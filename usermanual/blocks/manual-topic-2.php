<h2 id="manual-topic-1" class="heading">Create and Publish your Events</h2>

<div class="" data-spy="scroll" data-target="#manual-group-2" data-offset="0">
	<div class="text-block" id="manual-2-1">
		<h3 class="subheading">Create a New Event</h3>
		<div class="alert alert-warning">
			<strong>The Event/Ticket Checklist</strong> aims to speed up the process of creating your event and ticket, by providing the complete list of fields required for event/ticket creation. <a href="https://docs.google.com/spreadsheets/d/1Otwy9I2b6PPdBRVrXzvfSus8GwevpjbCpqKZ4pK77sQ/edit#gid=0" target="_blank">Please feel free to download or print the document.</a>
		</div>
		<ol>
			<li>Click on the <strong>Events</strong> tab.</li>
			<li>Select <strong>Create New Event</strong> from the dropdown menu.</li>
			<img class="img-faq img-fluid" src="img/createevent1-2.png" alt="">
		</ol>

		<p><strong>Basic Information:</strong></p>
		<img class="img-faq img-fluid" src="img/basic1.png" alt="">
		<ol>
			<li>Please enter <strong>Title of your Event.</strong> The globe icon in the upper right side will allow you to add different languages to the title of your event.</li>
			<li><strong>Subtitle</strong> allows you to give more information about the event than just the title. An example of this might be “Volume 2”, or "Sponsored by Company A". The globe icon in the upper right side will allow you to add different languages to the title of your event.
			</li>
			<li>Please choose the <strong>Event Type</strong> from the drop down menu.</li>
			<li>Please add the <strong>Venue</strong>. If the venue is not in our system, don’t worry. We can add a venue to the permanent list if it is used frequently.</li>
			<li>Please add the <strong>Event Date.</strong></li>
			<li>Please add the <strong>Event Start and End Times.</strong> If the event is set as a Live Music/Concert you will be asked to provide a <strong>Doors Open Time.</strong></li>
			<li>Please select an <strong>End Date</strong> for your event. </li>
		</ol>

		<p><strong>Add More Information:</strong></p>
		<img class="img-faq img-fluid" src="img/add1.png" alt="">	
		<ol>
			<li><strong>Flyer:</strong> please add your event flyer here. You can add up to 3 files, each at a maximum file size of 2MB. Accepted files types are JPEG, GIF, and PNG.</li>
			<li><strong>The upper left two icons</strong> allows you to edit the uploaded image.</li>
			<li>Click the <strong>X icon</strong> in the upper right corner once to bring up the remove file function. The <strong>globe icon</strong> in the upper right side will allow you to add different languages to the title of your event.</li>
		</ol>

		<p><strong>More Details</strong></p>
		<ol>
			<li><strong>Subcategory</strong> can be added as an even tag.</li>
			<li><strong>Event Introduction</strong> is where you describe your event.</li>
			<li><strong>Price</strong> is where you inform the users of the price.<br> 
				<div class="alert alert-warning">
					Setting up the event does<strong> NOT CREATE A TICKET</strong>, you will need to set up the ticket(s) for your event separately.
				</div>
			</li>
		</ol>

		<p><strong>Artists</strong></p>
		<ol>
			<li>If you are organizing a music event, this is where you add the artist(s). If they are not in the ZAIKO system, you can add them manually.</li>
		</ol>

		<p><strong>Advanced</strong></p>
		<ol>
			<li><strong>Late times</strong> (over 24hr clock) off.</li>
			<li><strong>Marketing tags</strong> can be added to your event for promotion purposes.</li>
		</ol>

		<p><strong>Finishing Up</strong></p>
		<ol>
			<li>Check to agree to <strong>Terms of Service</strong>.</li>
			<li>Click the <strong>Save Button</strong></li>
		</ol>


		<div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i></a>
		</div>
	</div>


	<div class="text-block" id="manual-2-2">
		<h3 class="subheading">Adding More Event Details</h3>
		<ol>
			<li>Click on the <strong>Home</strong> tab.</li>
			<li>Click on the <strong>Newly Created Event.</strong></li>
			<li>Click on the <strong>Event Details</strong> tab.</li>
			<img class="img-faq img-fluid" src="img/eventdetails3.png" alt="">	

			<li><strong>Information</strong> is where you update the information you added when you created the event.</li>
			<li><strong>Artists and Hosts</strong> is where you add new artists and hosts (organizers and promoters).</li>
			<li><strong>Images</strong> is where you can update your event images and include other sizes for social media.</li>
			<li><strong>Gallery</strong> is where you can add more images for the event page. A great way to make your event more interesting.</li>
			<li><strong>Embedded Media</strong> is where you can add video from YouTube. You can feature the video at the top of your event page by clicking on the <strong>Featured tab</strong>. You can add a maximum of 10 video links from this page.
			</li>
		</ol>
		<img class="img-faq img-fluid" src="img/eventdetails4-8.png" alt="">	
		<div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i>    </a>
		</div>
	</div>

	<div class="text-block" id="manual-2-3">
		<h3 class="subheading">Customize your Event Page (Microsite)</h3>
		<ol>
			<li>On the Main Menu, click on <strong>Profiles and select the profile</strong> you would like to edit.</li>
			<img class="img-faq img-fluid" src="img/design-1.png" alt="">
			<li>Scroll down the page and click on <strong>Edit Ticket Design</strong>.</li>
			<img class="img-faq img-fluid" src="img/design-2.png" alt="">
			<li><strong>Brand Name</strong>: Title of your event.</li>
			<li><strong>Highlight Color</strong>: The color of header bar.</li>
			<li><strong>Secondary Color</strong>: The color of link and purchase button.</li>
			<li><strong>Theme Color</strong>: Light and Dark Mode. Selecting light mode will set your microsite to white background and black text, while dark mode renders black background and white text.</li>  
			<li><strong>Theme Header Color</strong>: The color of all headings.</li>
			<li><strong>Logo Design</strong>: The image that will be displayed on the header bar. You can display it in square or in 200x75px format (light and dark background). JPG and PNG formats are accepted.</li>
			<li>Click <strong>Save</strong> to confirm.</li>
		</ol>
	</div>

	<div class="text-block" id="manual-2-4">
		<h3 class="subheading">Event Settings</h3>
		<ol>
			<li>Select your newly created event on the home screen.</li>
			<li>You will see all the tickets you have created and all the tabs: Event Details, Analytics, Communication, Payments, and Log.</li>
			<li>You will also see a summary of tickets sold and revenue on the left side of the screen.</li>
			<li>To see all the ticket purchasers click on the <strong>people icon</strong>.<br>
				<img class="img-faq img-fluid" src="img/eventsettings-0.png" alt="">	
				The Ticket Members page will list all of the purchasers for your event. To download the list in .CSV format click on the <strong>download icon</strong> on the top right corner.
				<img class="img-faq img-fluid" src="img/eventsettings-1.png" alt="">	
			</li>
			<strong>Event Details</strong>
			<ol>
				<li>This page shows the event's listing and publish status. To change the status click on the <strong>calendar icon.</strong>
					<img class="img-faq img-fluid" src="img/eventsettings-2.png" alt="">	
				</li>
				<li><strong>Information: </strong>this section will allow you to update your event information by clicking on the pencil icon on the top right corner.</li>
				<li><strong>Artists & Hosts: </strong>this section will allow you to add new artists to the event by clicking on the pencil icon on the top right corner.</li>
				<li><strong>Images: </strong>this section will allow you to add banners to your event by clicking on the pencil icon on the top right corner.</li>
				<li><strong>Gallery: </strong>this section will allow you to add images to your event by clicking on the pencil icon on the top right corner.</li>
				<li><strong>Media: </strong>this section will allow you to add embedded video/audio by clicking on the pencil icon on the top right corner.</li>
			</ol>
			<strong>Analytics</strong>
			<p>The <a href="#">Analytics tab</a> will provide a detailed breakdown of ticket purchasers data.</p>
			<strong>Communications</strong>
			<p>The Communication tab will allow you to send emails to your purchasers by clicking on the email customers tab. <strong>You are able to send messages to purchases up to and 30 days after the event date. After that time you will no longer be able to contact the users from the event.</strong></p>
			<strong>Payments</strong>
			<p>The Payments tab will allow you to see all the financial information from the event. This includes tickets sold, sales amount, amount returned minus fees and taxes, and estimated return date.<br>
				To enter your banking information for repayment please click on the <strong>yellow button.</strong>
				<img class="img-faq img-fluid" src="img/payments-1.png" alt="">	
			</p>
			<strong>Log</strong>
			<p>The Log tab will give you detailed information on changes made to the event and tickets, as well as every ticket purchase and event check-in.<br>
				You also have the ability to post messages by clicking on the <strong>POST button.</strong>
				<img class="img-faq img-fluid" src="img/log-1.png" alt="">	
			</p>
		</ol>

		<div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i>    </a>
		</div>

	</div>

	<div class="text-block" id="manual-2-5">
		<h3 class="subheading">Cancel your Event</h3>
		<div class="alert alert-warning">
			Note: As of June 1, 2019, our event cancellation fee for organisers has changed <strong>from 0% to 3%</strong>. Please see below for details.
		</div>
		<p>Generally after an event has been published online, there will be no cancellation. However in case of necessity, you can cancel the event with following procedure.
		</p>
		<ol>
			<li>To cancel an event, the organiser must always let all ticket purchasers know that why it's been cancelled in advance. To send an email to all ticket purchasers at once, please click on <strong>Communication Tab</strong>.
				<img class="img-faq img-fluid" src="img/cancel1.png" alt="">
			</li>
			<li>Please complete the cancellation process BEFORE the event date.</li>
			<li>Once cancellation process is complete, there will be an automatic mail out to all ticket purchasers and refund process begins respectively.</li>
			<li>Apart from free tickets, there will be a charge of 3% refund fee of all tickets sold.</li>
			<li>Please follow the above procedure and enter your credit card details.</li>

		</ol>
		<div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i>    </a>
		</div>
	</div>
</div>