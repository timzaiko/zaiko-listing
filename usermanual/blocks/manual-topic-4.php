<h2 id="manual-topic-1" class="heading">Monitor your Sales and Analytics</h2>

<div class="" data-spy="scroll" data-target="#manual-group-4" data-offset="0">
	<div class="text-block" id="manual-4-1">
		<h3 class="subheading">Members</h3>
		<ol>
			<li>Click on the <strong>Members</strong> tab.</li>
			<li><strong>The Members tab</strong> shows purchasing timeline, age, language, gender, and country data visualization.</li>
			<li>Members also shows what other artists or genre that purchasers are interested in, in addition to the current event.</li>

			<p><strong>Partners</strong></p>
			<ul>
				<li>The Partners tab shows the data of the media network selling your event tickets. There will be no data here if you are only using the white label solution.</li>
			</ul>

			<p><strong>Combined</strong></p>
			<ul>
				<li>The Combined tab shows a combination of your own white label tickets and media network tickets (if you sold on the media network).
				</li>
			</ul>
			<li>Click the download icon to download your users and their email addresses.
				<img class="img-faq img-fluid" src="img/member-1.png" alt="">
				<div class="alert alert-warning"><strong>Important</strong> - User emails are only available for tickets sold on your own media (white label). If a user has purchased a ticket from a media partner, the user information is theirs. As an organizer, you are able to contact all purchasers to your events through the Event Communication tool. Communication with purchasers will be available up to 30 days past your event date.
				</div>
			</li>
		</ol>


		<div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i></a>
		</div>
	</div>


	<div class="text-block" id="manual-4-2">
		<h3 class="subheading">Reports</h3>
		<ol>
			<li>Click on the <strong>Reports</strong> tab.</li>
			<p><strong>My Events</strong></p>
			<ul>
				<li>You will see the data of events currently on sale.</li>
				<li>You can choose a past month to see what has sold and not yet been paid.</li>
			</ul>
			<p><strong>Commissions</strong></p>
			<ul>
				<li>If you are a media company this is where you will see the money you have made from selling tickets on your platform.</li>
			</ul>
			<p><strong>Profiles</strong></p>
			<ul>
				<li>Clicking on the  Profiles tab will take you to the management page of your current profile. You can change the details of the current profile from here.</li>
				<li>If you are the manager of multiple profiles, you can switch profiles from here.</li>
			</ul>
		</ol>

		<div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i>    </a>
		</div>
	</div>

	<div class="text-block" id="manual-4-3">
		<h3 class="subheading">E-mail the Ticket Purchasers</h3>
		<ol>
			<li>On the home page, please select the event you want to email the members.</li>
			<li>On the Event Page, click on <strong>Communications</strong> tab.</li>
			<li>Click on the <strong>Email Customers</strong> button</li>
			<li><strong>Create an Email</strong>
				<ol>
					<li><strong>Name:</strong> Title to be displayed on the management screen. This is NOT the subject of the email. Ex. Lottery Application Email</li>
					<li><strong>Subject:</strong> To be displayed on the subject line of your email.</li>
					<li><Strong>PC Mail:</Strong> The header image of the email when viewed on a desktop. It can be a flyer or banner image.</li>
					<li><strong>Mobile Mail:</strong> The header image of the email when viewed on a smartphone. </li>
					<li><strong>Others:</strong> Files to be attached to the email. Ex. PDF file.</li>
					<li><strong>Contents:</strong> Your email content. There are tags on the top of the editor which you can use to automatically generate the event/ticket contents. For example, if you select “name”, the name of the email recipient will be displayed on the email.</li>
				</ol>
			</li>
			<li>Click “Save” to save the email as draft.</li>
			<li>You will need to enter an email as sender address. Click on the <strong>Verify Now</strong> button.</li>
			<li>Please enter your sender email address and click <strong>Save</strong>. You will receive a notification email of confirmation.</li>
			<li>Please add the members to whom you want to send email to. You can select the members of each ticket type to send email to. You can also send email to those who pre-register for the event, especially for Lottery tickets.</li>
			<li>Click <strong>Send All Email</strong> to send to all the members. If you want to send individually, you can do so by clicking on the green mark at the bottom right of each member’s name.</li>
		</ol>

	</div>
</div>


</div>
