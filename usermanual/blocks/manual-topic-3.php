<h2 id="manual-topic-1" class="heading">Set Up Tickets for your Events</h2>

<div class="" data-spy="scroll" data-target="#manual-group-3" data-offset="0">
	<div class="text-block" id="manual-3-1">
		<h3 class="subheading">Add a New Ticket to your Event</h3>
		<div class="alert alert-warning">
			<strong>The Event/Ticket Checklist</strong> aims to speed up the process of creating your event and ticket, by providing the complete list of fields required for event/ticket creation. <a href="https://docs.google.com/spreadsheets/d/1Otwy9I2b6PPdBRVrXzvfSus8GwevpjbCpqKZ4pK77sQ/edit#gid=0" target="_blank">Please feel free to download or print the document.</a>
		</div>
		<ol>
			<li>Click on the <strong>Home</strong> tab.</li>
			<li>Select the <strong>Event</strong> of which you would like to create the ticket.</li>
			<li>Click on the <strong>Make New Ticket</strong> button.</li>
			<img class="img-faq img-fluid" src="img/maketicket3.png" alt="">	

			<li>Select between <strong>Standard</strong> and <strong>Free</strong> Ticket.</li>
			<img class="img-faq img-fluid" src="img/maketicket4.png" alt="">	
		</ol>

		<div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i></a>
		</div>
	</div>


	<div class="text-block" id="manual-3-2">
		<h3 class="subheading">Create a Standard Ticket</h3>
		<img class="img-faq img-fluid" src="img/standard-1.png" alt="">	
		<ol>
			<li>Enter the <strong>Price</strong> of the ticket.</li>
			<li>Select <strong>Your Own Website</strong> or <strong>ZAIKO Media Network.</strong></li>

			<p><strong>Via Your Own Website</strong></p>
			<img class="img-faq img-fluid" src="img/standard-2.png" alt="">	
			<ol>
				<li>0%-2.89% + customer fee (Use slider to choose your rate. % under 2.89% adds fees to the user).
				</li>
				<li>4.98% + 98JPY</li>
				<ul>
					<li>Fees Off-No handling fees are added</li>
					<li>All On-Pass on all fees to the customer</li>
				</ul>
			</ol>

			<p><strong>Via ZAIKO Media Network</strong></p>
			<ol>
				<li>Gateway is 3% + a suggested 5% of ticket price to media partners (This can be adjusted)</li>
				<li>Media fees can be passed on to the customer using a slide bar as well.</li>
				<li>The default setting is to open tickets to all of the ZAIKO network.</li>
				<li>If you would like to sell to through only certain media companies choose the <strong>Restrict Network</strong> button and choose the media companies who can sell your tickets.</li>
			</ol>

			<p><strong>Basic Information</strong></p>
			<img class="img-faq img-fluid" src="img/basicinfo-1.png" alt="">	

			<ol>
				<li>Enter the <strong>Ticket Type</strong>.</li>
				<li>Under <strong>Availability</strong>, enter the sale start, sale end, and the maximum inventory.</li>
			</ol>

			<p><strong>Add More Information</strong></p>
			<img class="img-faq img-fluid" src="img/basicinfo-2.png" alt="">	
			<ol>
				<p><strong>More Details</strong></p>
				<ol>
					<li>Catch Text: Enter any catchy text you would like to use. Highlight the biggest selling point of this ticket type.</li>
					<li>Additional information: Space for additional information about what this ticket offers. VIP or Campsite details for example. <em>Note: This is not the space for terms and conditions.</em></li>
				</ol>

				<p><strong>Groups and Discounts</strong></p>
				<ol>
					<li>The <a href="https://zaiko.io/support/?cid=22#faq" target="_blank">Group Ticket</a> (maximum number a user can purchase at one time) is 4 people by default. You can add a discount for multiple ticket purchases here.</li>
				</ol>

				<p><strong>Reservation Numbers</strong></p>
				<ol>
					<li>Reservation numbers can be added to the tickets after the sale is final. Numbers will be assigned by the user purchasing time.</li>
				</ol>

				<p><strong>Advanced</strong></p>
				<ol>
					<li>Door Instructions: Leave door instructions here for the staff. For example: VIP ticket is GOLD.</li>
				</ol>
			</ol>
			<li>Read and agree to the <strong>Terms of Service.</strong></li>
			<li>Click on <strong>Make Ticket</strong> button to complete.</li>
		</ol>

		<div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i>    </a>
		</div>
	</div>

	<div class="text-block" id="manual-3-3">
		<h3 class="subheading">Create a Free Ticket</h3>
		<ol>
			<li><p>Click on <strong>Free Ticket</strong> option.</p>
				<div class="alert alert-warning">
					Note: there will be 50JPY charge per free ticket “purchased” to organiser.
				</div>
				<p><strong>Basic Information</strong></p>
				<img class="img-faq img-fluid" src="img/free-1.png" alt="">	
				<ol>
					<li>Enter the <strong>Ticket Type</strong>.</li>
					<li>Under <strong>Availability</strong>, enter the sale start, sale end, and the maximum inventory.</li>
				</ol>

				<p><strong>Add More Information</strong></p>
				<img class="img-faq img-fluid" src="img/free-2.png" alt="">	
				<ol>
					<p><strong>More Details</strong></p>
					<ol>
						<li>Catch Text: Enter any catchy text you would like to use. Highlight the biggest selling point of this ticket type.</li>
						<li>Additional information: Space for additional information about what this ticket offers. VIP or Campsite details for example. <em>Note: This is not the space for terms and conditions.</em></li>
					</ol>

					<p><strong>Reservation Numbers</strong></p>
					<ol>
						<li>Reservation numbers can be added to the tickets after the sale is final. Numbers will be assigned by the user purchasing time.</li>
					</ol>

					<p><strong>Advanced</strong></p>
					<ol>
						<li>Door Instructions: Leave door instructions here for the staff. For example: VIP ticket is GOLD.</li>
					</ol>
				</ol>
			</li>
			<li>Read and agree to the <strong>Terms of Service.</strong></li>
			<li>Click on <strong>Make Ticket</strong> button to complete.</li>
		</ol>

		<div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i>    </a>
		</div>
	</div>

	<div class="text-block" id="manual-3-4">
		<h3 class="subheading">Create a Lottery Ticket</h3>
		<ol>
			<li>Repeat all steps in <a href="#manual-3-1">Add a New Ticket for your Events.</a>	
			</li>
			<li>Go to the Ticket Page and under Lottery Options <strong>click on the edit (pencil) icon.</strong>
				<img class="img-faq img-fluid" src="img/maketicket5.png" alt="">	
			</li>
			<li><strong>Click Yes</strong> to turn on the feature.</li>
			<li>Set the <strong>Start, Close, and Announcement Dates</strong> (when organizer messages all participants regarding results).
				<div class="alert alert-warning">
					The ticket sales time period must start <strong>after the lottery time period or there will be an error when finalizing the lottery.</strong>
				</div>
			</li>
			<li>The ticket purchase and payment period will be displayed in the yellow box. To confirm click on <strong>Update Ticket</strong> button.</li>
			<li>Check your times and dates for the lottery. Please be sure to <a href="#manual-3-6">approve your ticket.</a></li>
		</ol>
	</div>

	<div class="text-block" id="manual-3-5">
		<h3 class="subheading">Set the Lottery Ticket Winner</h3>
		<ol>
			<li>When the Lottery Ticket is enabled, the edit icon will be replaced with a new 3-dot icon. <strong>Click on the new icon.</strong></li>
			<li>You will see the list of those who signed up for the lottery.</li>
			<img class="img-faq img-fluid" src="img/lottery1.png" alt="">	
			<li>To set a winner for the lottery ticket, click on the <strong>X button</strong> next to the name of the participant. You may choose more than 1 winner depending on the type of event.</li>
			<li>You can send emails to individuals by clicking on the <strong>mail icon</strong> on the top right corner of the section.</li>
			<li>You can send emails to the entire list by clicking on the <strong>3-circle icon</strong> on the top right corner of the section.</li>
			<li>Make your original mail to send to members.</li>
		</ol>


	</div>

	<div class="text-block" id="manual-3-6">
		<h3 class="subheading">Approve / Reject a Ticket</h3>
		<div class="alert alert-warning">
			You can check the ticket for any mistakes before approving it. To approve the ticket means that it will be made available during the sales period. Similarly, you may reject the ticket to prevent it from going live.
		</div>
		<ol>
			<li>Go to the Home tab and <strong>Select the Event</strong>.</li>
			<li>Click on the <strong>Lightning Bolt Button</strong> next to event description.</li>
			<img class="img-faq img-fluid" src="img/approve-1.png" alt="">	
			<li>To approve the ticket, click on <strong>Approve This Ticket</strong> button. To reject the ticket, <strong>click on the red X button.</strong></li>
			<img class="img-faq img-fluid" src="img/approve-2.png" alt="">	
			<li>A pop-up will appear asking to confirm your decision. For ticket rejection, please enter the reason as reference. Click on <strong>"OK"</strong> to confirm.</li>
			<li>If the ticket is approved, its status will be set to "Approved". Otherwise, it will be set to "Problem". After the problem has been fixed to the ticket, it is ready to be approved again <strong>(Repeat Steps 2-4)</strong>.</li>
		</ol>


	</div>

</div>
