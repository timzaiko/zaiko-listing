<h2 id="manual-topic-1" class="heading">Acquire a Profile on ZAIKO</h2>

<div class="" data-spy="scroll" data-target="#manual-group-1" data-offset="0">
	<div class="text-block" id="manual-1-1">
		<h3 class="subheading">What is a Profile?</h3>
		<p>A “Profile” is an invite-only business account specifically for event organizers, media partners and artists. Using a Profile they can set up events, sell tickets and track sales and user data. </p>

		<p><strong>A Profile is NOT a personal ZAIKO account, and cannot be combined with any personal accounts</strong>. A personal account can manage more than one profiles, and a profile can be assigned to multiple managers.</p>

		<p>To manage a Profile you must have a personal ZAIKO account.</p>

		<div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i></a>
		</div>
	</div>

	<div class="text-block" id="manual-1-2">
		<h3 class="subheading">Register / Login </h3>
		<ol>
			<li>Go to <a href="https://zaiko.io" target="_blank">https://zaiko.io</a> and click on <strong>Login</strong>.</li>
			<li>If you have not signed up for your ZAIKO account, please select <strong>Create Account.</strong></li>
			<img class="img-faq img-fluid" src="img/login-en.png" alt="">
			<li>Select the <strong>Email</strong> option.</li>
			<li>Enter the required fields and complete by clicking on <strong>Sign Up.</strong></li>
		</ol>
		<div class="alert alert-warning">You may also log in with your Facebook or Google account. We recommend that <strong>you register with a business email.</strong></div>
		<div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i></a>
		</div>
	</div>


	<div class="text-block" id="manual-1-3">
		<h3 class="subheading">Create a Profile</h3>
		<div class="alert alert-warning">
			For Country Manager Partners Only
		</div>
		<ol>
			<li>Click on the <strong>Profiles</strong> tab.</li>
			<li>Click on <strong>Create New Profile.</strong></li>
			<img class="img-faq img-fluid" src="img/createprofile1-2.png" alt="">
			<li>Inside the invite box please <strong>type in the code provided by ZAIKO.</strong></li>
			<img class="img-faq img-fluid" src="img/createprofile3.png" alt="">

			<li><strong>Information needed (Required)</strong></li>
			<img class="img-faq img-fluid" src="img/createprofile4a.png" alt="">

			<ol>
				<li>Name of media partner, organizer, venue, or artist</li>
				<li>ZAIKO microsite name <em>(will appear as yoursitename.zaiko.io)</em></li>
				<li>Default language for profile</li>
				<li>Location of profile</li>
			</ol>
			<li><strong>Add more information (optional)</strong></li>
			<img class="img-faq img-fluid" src="img/createprofile4b.png" alt="">

			<ol>
				<li>Images: Square image needed for profile image</li>
				<li>Details: Add website of new profile</li>
				<li>Category and Technology</li>
				<li>Profile: This information will be visible to other partners to learn about your company or event</li>
				<li>Company Information</li>
			</ol>
			<li>Please read Terms of Service and click <strong>Save Profile.</strong></li>
		</ol>

		<div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i>    </a>
		</div>
	</div>
	<div class="text-block" id="manual-1-4">
		<h3 class="subheading">Add a Manager to a Profile</h3>
		<ol>
			<li>Go to Profile tab and click on <strong>Select Profile.</strong></li>
			<img class="img-faq img-fluid" src="img/addmanager1a.png" alt="">
			<li>Go to the profile and click on <strong>Manage</strong> (or the profile name).</li>
			<img class="img-faq img-fluid" src="img/addmanager1b.png" alt="">
			<li>Managers can be added by <strong> clicking on the + people icon and adding an e-mail address of the person.</strong> If the user cannot be found, it is likely that they do not have a ZAIKO account and <a href="https://zaiko.io" target="_blank">must create one</a> before they can be added.
			</li>
			<img class="img-faq img-fluid" src="img/addmanager1c-1.png" alt="">
			<img class="img-faq img-fluid" src="img/addmanager1c-2.png" alt="">

		</ol>

		<div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i>    </a>
		</div>
	</div>

	<div class="text-block" id="manual-1-5">
		<h3 class="subheading">Linking an Event to a Profile</h3>
		<ol>
			<li>Go to Events tab and click on <strong>List Events.</strong></li>
			<img class="img-faq img-fluid" src="img/linkevent1-2.png" alt="">

			<li>Go to <strong>Find</strong> tab and search for events that has tickets you would like to sell on your media.</li>
			<img class="img-faq img-fluid" src="img/linkevent3-4.png" alt="">

			<li>Once the event is found, click on the <strong>Link Button.</strong></li>
			<img class="img-faq img-fluid" src="img/linkevent5.png" alt="">
			<li>To confirm, click on the <strong>Event Tab</strong> again and the linked events will be displayed.</li>

		</ol>
		<div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i>    </a>
		</div>
	</div>
	<div class="text-block" id="manual-1-6">
		<h3 class="subheading">Add a Linked Event to your Media</h3>
		<ol>
			<li>Events linked to your profile automatically are added to your microsite. You can choose to share the microsite link from your media as the fastest ticket integration. Some of our partners create a new “tickets” tab on their website that links to their ZAIKO microsite.
			</li>
			<li>Other integration options are available by clicking on the <strong>Share button.</strong>
				<img class="img-faq img-fluid" src="img/linkmedia2-a.png" alt="">
			</li>

			<ol>
				<li><strong>The share link</strong> is at the bottom of the page for social media.</li>
				<li><strong>Embed</strong> allows you to style and embed the ticket into your website using CSS that we have auto-generated for you. An example of when to use this feature would be when you are writing an article about an event and want to embed the ticket for the event into the article.</li>
				<li>There is a <strong>QR code</strong> which can be added to pages both online and offline which will open up the ticket purchase page.</li>
				<li><strong>Open Link</strong> will send you to the ticket purchase page.</li>
				<img class="img-faq img-fluid" src="img/linkmedia2-b.png" alt="">
			</ol>
		</ol>
		<div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i>    </a>
		</div>
	</div>
</div>