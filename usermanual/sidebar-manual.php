<div id="faq-groups">

	<div class="question"> 
		<a class="heading collapsed" data-toggle="collapse" data-target="#manual-group-1" aria-expanded="false" aria-controls="manual-group-1">Acquire a Profile on ZAIKO<i class="fas fa-chevron-right"></i></a>
		<div id="manual-group-1" class="collapse list-group">
			<a class="list-group-item list-group-item-action" href="#manual-1-1">What is a Profile?</a>
			<a class="list-group-item list-group-item-action" href="#manual-1-2">Register / Login</a>
			<a class="list-group-item list-group-item-action" href="#manual-1-3">Create a Profile</a>
			<a class="list-group-item list-group-item-action" href="#manual-1-4">Add a Manager to a Profile</a>
			<a class="list-group-item list-group-item-action" href="#manual-1-5">Linking an Event to a Profile</a>
			<a class="list-group-item list-group-item-action" href="#manual-1-6">Add a Linked Event to your Media</a>
		</div>
	</div>

	<div class="question">
		<a class="heading collapsed" data-toggle="collapse" data-target="#manual-group-2" aria-expanded="false" aria-controls="manual-group-2">Create and Publish your Events<i class="fas fa-chevron-right"></i></a>
		<div id="manual-group-2" class="collapse list-group">
			<a class="list-group-item list-group-item-action" href="#manual-2-1">Create a New Event</a>
			<a class="list-group-item list-group-item-action" href="#manual-2-2">Adding More Event Details</a>
			<a class="list-group-item list-group-item-action" href="#manual-2-3">Customize your Event Page (Microsite)</a>
			<a class="list-group-item list-group-item-action" href="#manual-2-4">Event Settings</a>
			<a class="list-group-item list-group-item-action" href="#manual-2-5">Cancel your Event</a>
		</div>
	</div>


	<div class="question">
		<a class="heading collapsed" data-toggle="collapse" data-target="#manual-group-3" aria-expanded="false" aria-controls="manual-group-3">Set Up Tickets for your Events<i class="fas fa-chevron-right"></i></a>
		<div id="manual-group-3" class="collapse list-group">
			<a class="list-group-item list-group-item-action" href="#manual-3-1">Add a New Ticket to your Event</a>
			<a class="list-group-item list-group-item-action" href="#manual-3-2">Create a Standard Ticket</a>
			<a class="list-group-item list-group-item-action" href="#manual-3-3">Create a Free Ticket</a>
			<a class="list-group-item list-group-item-action" href="#manual-3-4">Create a Lottery Ticket</a>
			<a class="list-group-item list-group-item-action" href="#manual-3-5">Set the Lottery Ticket Winner</a>
			<a class="list-group-item list-group-item-action" href="#manual-3-6">Approve/Reject a Ticket</a>
		</div>
	</div>

	<div class="question">
		<a class="heading collapsed" data-toggle="collapse" data-target="#manual-group-4" aria-expanded="false" aria-controls="manual-group-4">Monitor your Sales and Analytics<i class="fas fa-chevron-right"></i></a>
		<div id="manual-group-4" class="collapse list-group">
			<a class="list-group-item list-group-item-action" href="#manual-4-1">Members</a>
			<a class="list-group-item list-group-item-action" href="#manual-4-2">Reports</a>
		</div>
	</div>

</div>