<?php include('../header.php'); ?>

<section class="banner bg-purple">
	<div class="text-block banner-narrow text-center ">
		<h2>User Manual</h2>
		<p>For event organizers to set up events and tickets for sale and distribution</p>
	</div>
</section>

<?php include('cards-manual.php'); ?>

<div class="container-fluid">
	<div class="row faq">
		<div class="col-12 col-md-3 col-lg-3 faq__sidebar d-none d-lg-block">
			<?php include('sidebar-manual.php') ?>
		</div>    

		<div class="col-12 col-md-12 col-lg-9 faq__content" data-spy="scroll" data-target="fag-groups" data-offset="0">
			<div class="answers">
				<?php include('blocks/manual-topic-4.php') ?>
			</div>
		</div>
		<?php // include('../block-contact.php'); ?>
	</div>

<?php include('../footer.php'); ?>