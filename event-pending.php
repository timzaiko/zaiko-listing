<?php include('guestadmin-header.php'); ?>

<section class="container-fluid">
	<div class="text-center pt-5">
		<div class="text-muted">
			<i class="fas fa-hourglass-half h1"></i>
			<h4 class="fa-step--text">We are currently reviewing your event submission</h4>
		</div>
	</div>
	<p class="text-center text-dark-grey">You will be notified when they approve or reject your request.</p>
</section>

<section class="container p-4">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="text-center">
				<a href="guestadmin.php" class="btn btn-brand btn-lg">Return to Home Page</a>
			</div>
		</div>
	</div>		
</section>

<?php include('guestadmin-footer.php'); ?>