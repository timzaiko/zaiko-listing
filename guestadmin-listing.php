<?php include('guestadmin-header.php'); ?>

<div class="zaiko-listing">
	<div class="container pt-5">
		<div class="row">
			<div class="col-md-12 mx-auto">
				<form class="" action="" _lpchecked="1">
					<div class="input-group">
						<input type="text" class="form-control" id="" placeholder="Search for bands, concerts and any upcoming shows">
						<div class="input-group-append">
							<a href="listing-search.php" class="btn btn-brand btn-lg">Search</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<?php 
	include('listing-popular.php');
	include('listing-upcoming.php'); 
	?>

</div>

<?php include('guestadmin-footer.php'); ?>


<script>
	var mySwiper = new Swiper ('.swiper', {
		direction: 'horizontal',
		loop: true,
		speed: 700,
		autoplay: {
			delay: 2000,
		},

		slidesPerView: 6,
		spaceBetween: 15,
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		breakpoints: {
			640: {
				slidesPerView: 2
			},
			1080: {
				slidesPerView: 4
			}
		}
	})
</script>



