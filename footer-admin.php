<footer class="">
	<div class="container py-5">
		<div class="row">
			<div class="col-md-12 d-flex justify-content-around">
				<div class="flex-grow-2">
					<a href="/"><img class="img-header mb-3" src="/img/ZAIKO-logo-text-white.svg" alt=""></a>
				</div>
				<ul>
					<li class=""><a class="" href="faq.php">Support</a></li>
					<li class=""><a class="" href="faq.php">User Manual</a></li>
				</ul>
				<ul>
					<li class=""><a class="" href="about.php">What is ZAIKO?</a></li>
					<li class=""><a class="" href="https://zaiko.applytojob.com/">Careers</a></li>
				</ul>
				<ul>
					<li class=""><a class="" href="500.php">Terms</a></li>
					<li class=""><a class="" href="500.php">Privacy Policy</a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>

</div>

<!-- <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script> -->
<script src="https://code.jquery.com/jquery-3.4.1.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<script src="https://js.stripe.com/v3/"></script>

<script>
	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	})

	// Create a Stripe client.
var stripe = Stripe('pk_test_TYooMQauvdEDq54NiTphI7jx');

// Create an instance of Elements.
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element.
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');
</script>

</body>
</html>