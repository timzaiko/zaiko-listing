<div class="card card-dark p-2">
	<form action="listing-results.php">
		<div class="input-group p-3">
			<input type="text" class="form-control" id="" placeholder="Ex: event name, artist, venue, etc.">
			<div class="input-group-append">
				<button class="btn btn-default btn-lg"><i class="fas fa-search"></i></button>
			</div>
		</div>
	</form>
	<p class="d-xs-none px-3">
		Popular Searches: 
		<!-- foreach -->
		<button class="btn">{{ Search Word 1 }}</button>
		<button class="btn">{{ Search Word 2 }}</button>
		<button class="btn">{{ Search Word 3 }}</button>
	</p>
</div>