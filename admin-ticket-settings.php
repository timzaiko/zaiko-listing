<?php include('header-admin-profile.php'); ?>

<div class="container-fluid">
	<div class="row flex-lg-nowrap">
		<?php include('admin-aside-event.php') ?>

		<main class="admin-main">

			<?php include('admin-breadcrumbs.php') ?>

			<section class="container-fluid pt-4 px-5">
				<div class="row justify-content-between align-items-center my-3">
					<div>
						<h3 class="font-weight-normal mb-2">Ticket Setting</h3>
						<p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. </p>
					</div>

					<button class="btn btn-secondary" id="dropdownTicketBtn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Select Ticket<span class="typcn typcn-arrow-sorted-down"></span>
					</button>
					<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownTicketBtn">
						<a class="dropdown-item d-flex justify-content-between" href="#">General Admission <span class="ml-5">￥1,500</span></a>
						<a class="dropdown-item d-flex justify-content-between" href="#">Standing <span class="ml-5">￥2,500</span></a>
						<a class="dropdown-item d-flex justify-content-between" href="#">Lottery <span class="ml-5">￥3,500</span></a>
						<a class="dropdown-item d-flex justify-content-between" href="#">Pre-sale <span class="ml-5">￥2,000</span></a>
						<a class="dropdown-item d-flex justify-content-between" href="#">Fan Club限定 立ち見 <span class="ml-5">FREE</span></a>
					</div>



				</div>
			</section>

			<?php include('module-ticket-name.php') ?>

			<section class="container-fluid px-5">
				<div class="row" data-masonry='{ "itemSelector": ".grid-item" }'>
					<div class="col-md-6 grid-item pl-0">
						<div class="card">
							<div class="card-header bg-dark d-flex justify-content-between">
								<h5 class="font-weight-normal">Ticket Information</h5>
								<button class="p-0 typcn typcn-pencil text-white">
								</button>
							</div>
							<div class="row card-body p-4">
								<div class="col-md-12 d-flex justify-content-between">
									<label>Ticket Name</label>
									<strong class="mb-0">General Admission スタンダング</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>Date/Time Overrides</label>
									<strong class="mb-0">N/A</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>Max Tickets Per Group</label>
									<strong class="mb-0">4</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>Promotion Text</label>
									<strong class="mb-0">Yes</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>Additional Text</label>
									<strong class="mb-0">No</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>GEO Lock</label>
									<strong class="mb-0">No</strong>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-6 grid-item pr-0">
						<div class="position-absolute" style="top: 50%; left:50%; transform: translate(-50%, -50%); z-index: 1">
							<button class="btn btn-danger btn-lg" data-toggle="modal" data-target="#modal-addticket">Unlock Feature</button>
						</div>

						<div class="card" style="opacity: 0.4;">
							<div class="card-header bg-danger d-flex justify-content-between">
								<h5 class="font-weight-normal">Premium</h5>
								<button class="p-0 typcn typcn-pencil text-white">
								</button>
							</div>
							<div class="row card-body p-4">
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
							</div>
							<div class="card-footer">
								asdf
							</div>

						</div>
					</div>

					<div class="col-md-6 grid-item pl-0">
						<div class="position-absolute" style="top: 50%; left:50%; transform: translate(-50%, -50%); z-index: 1">
							<button class="btn btn-success btn-lg" data-toggle="modal" data-target="#modal-addticket">Start Trial</button>
						</div>

						<div class="card" style="opacity: 0.4;">
							<div class="card-header card-header-white d-flex justify-content-between">
								<h5 class="font-weight-normal">Advanced</h5>
								<button class="p-0 typcn typcn-pencil">
								</button>
							</div>
							<div class="row card-body p-4">
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>

							</div>
						</div>
					</div>

					<div class="col-md-6 grid-item pr-0">
						<div class="card">
							<div class="card-header card-header-white d-flex justify-content-between">
								<h5 class="font-weight-normal">General</h5>
								<button class="p-0 typcn typcn-pencil">
								</button>
							</div>
							<div class="row card-body p-4">
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
							</div>

						</div>
					</div>
				</div>
			</section>

		</main>
	</div>
</div>

<!-- modal -->
<div id="modal-addticket" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content queue p-5 text-center">
			<h4>Please select your ticket type</h4>
			<a href="register-1.php" class="btn btn-primary btn-lg btn-block">Paid Ticket</a>
			<a href="register-1.php" class="btn btn-default btn-lg btn-block">Free Ticket*</a>
			<div class="mt-2">*While these tickets are for free events, organizers will be charged 50¥ for each ticket “purchased”.</div>
		</div>


	</div>
</div>

<?php include('footer.php'); ?>


