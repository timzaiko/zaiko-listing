<?php include('header.php'); ?>

<style>
	header,footer {
		display: none;
	}
</style>

<section class="bg-overlay opacity-40 bg-zaiko full-h d-flex justify-content-center align-items-center flex-column">

	<div class="container-fluid">
		<div class="mx-auto card card-login">
			<div class="d-flex justify-content-between align-items-center card-header">
				<a href="/">
					<img class="img-header d-block mx-auto" src="https://d38fgd7fmrcuct.cloudfront.net/1_3srrgnchq4ywmry64ua57.png" alt="">
				</a>
				<p class="m-0">Reset your password</p>
			</div>

			<div class="card-body p-3">

				<form class="needs-validation" action="forgot-1.php" novalidate>
					<div class="form-group">
						<label>Enter old password</label>
						<input type="password" class="form-control" placeholder="min. 8 characters or letters" required>
						<div class="invalid-feedback">
							Please enter correct
						</div>
					</div>  

					<div class="form-group">
						<label>Create new password</label>
						<input type="password" class="form-control" placeholder="" required>
						<div class="invalid-feedback">
							Please enter correct
						</div>
					</div>  

					<div class="form-group">
						<label>Enter new password again</label>
						<input type="password" class="form-control" placeholder="" required>
						<div class="invalid-feedback">
							Please enter correct
						</div>
					</div>  
					
					<div class="d-block mt-3">
						<button class="btn btn-pink btn-xl btn-block" type="submit">Submit</button>
					</div>

				</form>
			</div>
			<div class="card-footer d-flex justify-content-between align-items-center">
				<p class="m-0">Already have another account?</p>
				<a href="/login.php" class="btn btn-outline-dark">Log In</a>
			</div>

		</div>
	</div>

</section>

<?php include('footer.php'); ?>