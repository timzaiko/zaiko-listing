<?php include('guestadmin-header.php'); ?>

<section class="container-fluid">
	<div class="row">
		<figure class="event-banner w-100">
			<img class="w-100" src="/img/guestadmin/guest-img-fyre.jpg" alt="" >
		</figure>
	</div>
</section>

<section class="container">
	<div class="row">
		<div class="col-md-4 order-md-last ticket-sticky pt-5">
			<div class="card p-4">
				<h3>Tickets</h3>
				<ul class="p-0">
					<li class="mb-3">
						<p>VIP (1 drink includ.)</p>
						<div class="price-info d-flex justify-content-between align-items-center">
							<h4 class="text-brand m-0">¥ 2,000</h4>
							<div class="input-group w-auto">
								<div class="input-group-prepend">
									<button type="button" class="btn btn-brand minus">-</button>
								</div>
								<input type="number" class="ticket-count pl-3 text-center" name="qty" value="0">
								<div class="input-group-append">
									<button type="button" class="btn btn-brand plus">+</button>
								</div>
							</div>
						</div>
					</li>
					<li class="mb-3">
						<p>ADV (1 drink includ.)</p>
						<div class="price-info d-flex justify-content-between align-items-center">
							<h4 class="text-brand m-0">¥ 1,500</h4>
							<div class="input-group w-auto">
								<div class="input-group-prepend">
									<button type="button" class="btn btn-brand minus">-</button>
								</div>
								<input type="number" class="ticket-count pl-3 text-center" name="qty" value="0">
								<div class="input-group-append">
									<button type="button" class="btn btn-brand plus">+</button>
								</div>
							</div>
						</div>
					</li>
					<li class="mb-3">
						<p>GA (1 drink includ.)</p>
						<div class="price-info d-flex justify-content-between align-items-center">
							<h4 class="text-brand m-0">¥ 1,000</h4>
							<div class="input-group w-auto">
								<div class="input-group-prepend">
									<button type="button" class="btn btn-brand minus">-</button>
								</div>
								<input type="number" class="ticket-count pl-3 text-center" name="qty" value="0">
								<div class="input-group-append">
									<button type="button" class="btn btn-brand plus">+</button>
								</div>
							</div>
						</div>
					</li>

					<li class="mb-3 coupon-price fade">
						<p>GA (Members Only)</p>
						<div class="price-info d-flex justify-content-between align-items-center">
							<h4 class="text-brand m-0">¥ 800</h4>
							<div class="input-group w-auto">
								<div class="input-group-prepend">
									<button type="button" class="btn btn-brand minus">-</button>
								</div>
								<input type="number" class="ticket-count pl-3 text-center" name="qty" value="0">
								<div class="input-group-append">
									<button type="button" class="btn btn-brand plus">+</button>
								</div>
							</div>
						</div>
					</li>

					<div class="input-group my-5">
						<input type="text" class="form-control" placeholder="Coupon Code" aria-label="Coupon Code">
						<div class="input-group-append">
							<button id="showDiscount" class="btn btn-secondary" type="button">Show</button>
						</div>
					</div>
				</ul>

				<div class="d-flex justify-content-between">
					<h3>Total:</h3>
					<h3 class="text-brand">¥ 15,000</h3>
				</div>

				<a href="#" class="btn btn-lg btn-brand">
					Buy Now
				</a>

				<div class="text-center mt-3">
					*Processing fee not included
				</div>
			</div>
		</div>

		<div class="col-md-8 order-md-first p-5">
			<figure>
				<h2>Test Festival</h2>
				<p><strong>
					<i class="fas fa-calendar-alt text-brand"></i>&nbsp;&nbsp;07/02(Tues) 06:00 - 07/02(Wed) 23:00<br>
					<i class="fas fa-map-marker-alt text-brand"></i>&nbsp;&nbsp;<a href="https://maps.google.com" target="_blank">The Exumas</a>
				</strong></p>

				<div class="event-desc">
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur quis blanditiis ducimus atque eaque quam provident eos ipsa minima neque facilis eligendi nostrum voluptate maxime, quo eum odit, dolorem nesciunt!
					</p>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur iusto perspiciatis consequuntur atque cumque in obcaecati voluptas porro dolores. Voluptatibus hic, veniam tempora delectus, cum a. Officia laudantium, suscipit alias.
					</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit architecto aliquam recusandae iusto illum illo numquam voluptas fuga. Facilis rem accusamus quisquam, aut tenetur! Repellat debitis ad doloribus sunt mollitia?</p>

					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur quis blanditiis ducimus atque eaque quam provident eos ipsa minima neque facilis eligendi nostrum voluptate maxime, quo eum odit, dolorem nesciunt!
					</p>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur iusto perspiciatis consequuntur atque cumque in obcaecati voluptas porro dolores. Voluptatibus hic, veniam tempora delectus, cum a. Officia laudantium, suscipit alias.
					</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit architecto aliquam recusandae iusto illum illo numquam voluptas fuga. Facilis rem accusamus quisquam, aut tenetur! Repellat debitis ad doloribus sunt mollitia?</p>

					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur quis blanditiis ducimus atque eaque quam provident eos ipsa minima neque facilis eligendi nostrum voluptate maxime, quo eum odit, dolorem nesciunt!
					</p>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur iusto perspiciatis consequuntur atque cumque in obcaecati voluptas porro dolores. Voluptatibus hic, veniam tempora delectus, cum a. Officia laudantium, suscipit alias.
					</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit architecto aliquam recusandae iusto illum illo numquam voluptas fuga. Facilis rem accusamus quisquam, aut tenetur! Repellat debitis ad doloribus sunt mollitia?</p>


					<div class="embed-responsive embed-responsive-16by9">
						<iframe width="100%" src="https://www.youtube.com/embed/mz5kY3RsmKo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div>
				</div>
			</figure>
		</div>

	</div>
</section>

<div class="preview alert-hidden">
	<div class="container p-3">
		<div class="row">
			<div class="col-md-12 d-flex justify-content-between align-items-center">
				<p class="m-0">
					This is the preview of your event page. Please note that It is currently NOT visible to public.
				</p>
				<div>
					<a href="guestadmin-submit.php" data-toggle="modal" data-target="#modal-verify" class="btn btn-brand btn-lg">Submit</a>
					<a href="editevent.php"><button class="btn btn-secondary btn-lg">Back to Edit</button></a>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="modal-verify" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg modal-dialog-centered">
		<div class="modal-content p-5">

			<div class="text-center">
				<h4>Please verify with your mobile phone</h4>
			</div>

			<form action="guestadmin-submit.php">

				<label>Enter your mobile phone</label>
				<div class="input-group">
					<div class="input-group-prepend">
						<button class="btn btn-brand dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Japan (+81)</button>
						<div class="dropdown-menu">
							<a class="dropdown-item" href="#">U.S. (+1)</a>
							<a class="dropdown-item" href="#">Country (+00)</a>
							<a class="dropdown-item" href="#">Country (+01)</a>
							<a class="dropdown-item" href="#">Country (+02)</a>
						</div>
					</div>
					<input type="tel" maxlength="12" class="form-control text-center" placeholder="">

				</div>

				<div class="text-center mt-2">
					<button class="btn btn-outline-brand" type="button" id="requestCode">Request SMS Code [Code Sent]</button>
				</div>

				<div class="fade form-control-verify">
					<label>Please enter the SMS code</label>
					<div class="form-group">
						<input type="text" class="form-control form-sms text-center" placeholder="4-digit max" maxlength="6">
					</div>
					<div class="text-center">
						<button class="btn btn-brand btn-lg btn-block disabled btn-submit" type="submit" disabled="true">Verify</button>
					</div>
				</div>

			</form>
		</div>
	</div>
</div>

<?php include('guestadmin-footer.php'); ?>

<script>
	$(document).ready(function(){
		$('.ticket-count').prop('disabled', true);
		$(document).on('click','.plus',function(){
			$('.ticket-count').val(parseInt($('.ticket-count').val()) + 1 );
		});
		$(document).on('click','.minus',function(){
			$('.ticket-count').val(parseInt($('.ticket-count').val()) - 1 );
			if ($('.ticket-count').val() == 0) {
				$('.ticket-count').val(1);
			}
		});

		$('#showDiscount').on('click', function(){
			$('.coupon-price').addClass('show');
		});

		$('#requestCode').on('click', function(){
			$(this).addClass('disabled');
			$('.form-control-verify').addClass('show');
			$('.btn-submit').removeClass('disabled').removeAttr('disabled');
		});
	});
</script>

