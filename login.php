<?php include('header.php'); ?>

<style>
	header,footer {
		display: none;
	}
</style>

<section class="bg-overlay opacity-40 bg-zaiko full-h d-flex justify-content-center align-items-center flex-column">

	<div class="container-fluid card-login">
		<div class="mx-auto card">
			<div class="card-body p-3 p-sm-5">
				<a href="/">
					<img class="img-header d-block mx-auto" src="https://d38fgd7fmrcuct.cloudfront.net/1_3srrgnchq4ywmry64ua57.png" alt="">
				</a>

				<form class="needs-validation py-3" action="" novalidate>
					<div class="form-group">
						<label>Your Email</label>
						<input type="email" class="form-control" placeholder="Email Address" required>
						<div class="invalid-feedback">
							Please enter correct email address
						</div>
					</div>  

					<div class="form-group">
						<label>Your Password</label>
						<input type="password" class="form-control" placeholder="Password" required>
						<div class="invalid-feedback">
							Please enter correct password
						</div>
					</div>  

					<div class="d-block text-right">
						<a href="forgot.php" class="text-muted">
							Forgot your password?
						</a>
					</div>

					<div class="d-block my-3">
						<button class="btn btn-pink btn-xl btn-block" type="submit">Login</button>
					</div>

					<div class="social-media">
						<p class="text-center text-muted mb-3">or</p>
						<div class="d-flex justify-content-around">
							<a href="#" class="btn btn-social btn-facebook">
								<svg viewBox="0 0 33.75 73.2"><path d="M70.2,58.29h-10V95H45V58.29H37.81V45.41H45V37.07c0-6,2.83-15.31,15.3-15.31l11.23,0V34.32H63.41c-1.34,0-3.22.67-3.22,3.51v7.59H71.53Z" transform="translate(-37.81 -21.76)" style="fill:#fff"/></svg>
								<span>Facebook</span>
								
							</a>
							<a href="#" class="btn btn-social btn-light">
								<svg viewBox="0 0 512 512"><path fill="#fbbb00" d="M113.47 309.408L95.648 375.94l-65.139 1.378C11.042 341.211 0 299.9 0 256c0-42.451 10.324-82.483 28.624-117.732h.014L86.63 148.9l25.404 57.644c-5.317 15.501-8.215 32.141-8.215 49.456.002 18.792 3.406 36.797 9.651 53.408z"></path><path fill="#518ef8" d="M507.527 208.176C510.467 223.662 512 239.655 512 256c0 18.328-1.927 36.206-5.598 53.451-12.462 58.683-45.025 109.925-90.134 146.187l-.014-.014-73.044-3.727-10.338-64.535c29.932-17.554 53.324-45.025 65.646-77.911h-136.89V208.176h245.899z"></path><path fill="#28b446" d="M416.253 455.624l.014.014C372.396 490.901 316.666 512 256 512c-97.491 0-182.252-54.491-225.491-134.681l82.961-67.91c21.619 57.698 77.278 98.771 142.53 98.771 28.047 0 54.323-7.582 76.87-20.818l83.383 68.262z"></path><path fill="#f14336" d="M419.404 58.936l-82.933 67.896C313.136 112.246 285.552 103.82 256 103.82c-66.729 0-123.429 42.957-143.965 102.724l-83.397-68.276h-.014C71.23 56.123 157.06 0 256 0c62.115 0 119.068 22.126 163.404 58.936z"></path></svg>
								<span class="text-dark">Google</span>
							</a>
							<a href="#" class="btn btn-social btn-success">
								<svg viewBox="0 0 87.38 83.26"><g><g><path d="M464.44,292.36c0-19.55-19.6-35.45-43.69-35.45s-43.69,15.9-43.69,35.45c0,17.53,15.54,32.21,36.54,35,1.42.31,3.36.94,3.85,2.16a8.93,8.93,0,0,1,.14,3.95l-.62,3.74c-.19,1.1-.88,4.32,3.78,2.35s25.16-14.81,34.33-25.36h0c6.33-6.95,9.36-14,9.36-21.82" transform="translate(-377.06 -256.91)" style="fill:#fff"/><path d="M411.87,282.92h-3.06a.85.85,0,0,0-.85.85v19a.85.85,0,0,0,.85.85h3.06a.85.85,0,0,0,.85-.85v-19a.85.85,0,0,0-.85-.85" transform="translate(-377.06 -256.91)" style="fill:#3aae37"/><path d="M433,282.92H429.9a.85.85,0,0,0-.85.85v11.31l-8.72-11.79-.07-.08h0l-.06-.05h0l-.05,0,0,0,0,0,0,0,0,0h0l-.05,0h0l0,0h-3.3a.85.85,0,0,0-.85.85v19a.85.85,0,0,0,.85.85h3.07a.85.85,0,0,0,.85-.85V291.5l8.74,11.79a.7.7,0,0,0,.21.21h0l.05,0h0l0,0,0,0h0l.06,0h0a.83.83,0,0,0,.22,0H433a.85.85,0,0,0,.85-.85v-19a.85.85,0,0,0-.85-.85" transform="translate(-377.06 -256.91)" style="fill:#3aae37"/><path d="M404.49,298.89h-8.33V283.77a.85.85,0,0,0-.85-.85h-3.07a.85.85,0,0,0-.85.85v19h0a.83.83,0,0,0,.24.59h0v0a.89.89,0,0,0,.59.23h12.25a.85.85,0,0,0,.85-.85v-3.06a.85.85,0,0,0-.85-.85" transform="translate(-377.06 -256.91)" style="fill:#3aae37"/><path d="M449.89,287.68a.85.85,0,0,0,.85-.85v-3.06a.85.85,0,0,0-.85-.85H437.65a.83.83,0,0,0-.59.24h0l0,0a.87.87,0,0,0-.23.59h0v19h0a.83.83,0,0,0,.24.59h0a.83.83,0,0,0,.59.24h12.24a.85.85,0,0,0,.85-.85v-3.06a.85.85,0,0,0-.85-.85h-8.33v-3.22h8.33a.85.85,0,0,0,.85-.85v-3.07a.85.85,0,0,0-.85-.85h-8.33v-3.22Z" transform="translate(-377.06 -256.91)" style="fill:#3aae37"/></g></g></svg>
								<span>LINE</span>
							</a>
						</div>
					</div>

				</form>
				<div class="d-flex justify-content-between align-items-center mt-3">
					<p class="m-0">Don't have a ZAIKO account?</p>
					<a href="#" class="btn btn-outline-dark" data-toggle="modal" data-target="#modal-queue">Sign Up</a>
				</div>
			</div>

			
		</div>
	</div>

</section>

<!-- modal -->
<div id="modal-queue" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content queue p-5 text-center">
			<h4>Create your account with</h4>
			<a href="register-1.php" class="btn btn-pink btn-lg btn-block">Email Address</a>
			<div class="social-media mt-3">
				<p class="text-center text-muted">or sign up with</p>
				<div class="d-flex justify-content-around">
					<a href="#" class="btn btn-social btn-facebook">
						<svg viewBox="0 0 33.75 73.2"><path d="M70.2,58.29h-10V95H45V58.29H37.81V45.41H45V37.07c0-6,2.83-15.31,15.3-15.31l11.23,0V34.32H63.41c-1.34,0-3.22.67-3.22,3.51v7.59H71.53Z" transform="translate(-37.81 -21.76)" style="fill:#fff"/></svg>
						<span>Facebook</span>

					</a>
					<a href="#" class="btn btn-social btn-light">
						<svg viewBox="0 0 512 512"><path fill="#fbbb00" d="M113.47 309.408L95.648 375.94l-65.139 1.378C11.042 341.211 0 299.9 0 256c0-42.451 10.324-82.483 28.624-117.732h.014L86.63 148.9l25.404 57.644c-5.317 15.501-8.215 32.141-8.215 49.456.002 18.792 3.406 36.797 9.651 53.408z"></path><path fill="#518ef8" d="M507.527 208.176C510.467 223.662 512 239.655 512 256c0 18.328-1.927 36.206-5.598 53.451-12.462 58.683-45.025 109.925-90.134 146.187l-.014-.014-73.044-3.727-10.338-64.535c29.932-17.554 53.324-45.025 65.646-77.911h-136.89V208.176h245.899z"></path><path fill="#28b446" d="M416.253 455.624l.014.014C372.396 490.901 316.666 512 256 512c-97.491 0-182.252-54.491-225.491-134.681l82.961-67.91c21.619 57.698 77.278 98.771 142.53 98.771 28.047 0 54.323-7.582 76.87-20.818l83.383 68.262z"></path><path fill="#f14336" d="M419.404 58.936l-82.933 67.896C313.136 112.246 285.552 103.82 256 103.82c-66.729 0-123.429 42.957-143.965 102.724l-83.397-68.276h-.014C71.23 56.123 157.06 0 256 0c62.115 0 119.068 22.126 163.404 58.936z"></path></svg>
						<span class="text-dark">Google</span>
					</a>
					<a href="#" class="btn btn-social btn-success">
						<svg viewBox="0 0 87.38 83.26"><g><g><path d="M464.44,292.36c0-19.55-19.6-35.45-43.69-35.45s-43.69,15.9-43.69,35.45c0,17.53,15.54,32.21,36.54,35,1.42.31,3.36.94,3.85,2.16a8.93,8.93,0,0,1,.14,3.95l-.62,3.74c-.19,1.1-.88,4.32,3.78,2.35s25.16-14.81,34.33-25.36h0c6.33-6.95,9.36-14,9.36-21.82" transform="translate(-377.06 -256.91)" style="fill:#fff"/><path d="M411.87,282.92h-3.06a.85.85,0,0,0-.85.85v19a.85.85,0,0,0,.85.85h3.06a.85.85,0,0,0,.85-.85v-19a.85.85,0,0,0-.85-.85" transform="translate(-377.06 -256.91)" style="fill:#3aae37"/><path d="M433,282.92H429.9a.85.85,0,0,0-.85.85v11.31l-8.72-11.79-.07-.08h0l-.06-.05h0l-.05,0,0,0,0,0,0,0,0,0h0l-.05,0h0l0,0h-3.3a.85.85,0,0,0-.85.85v19a.85.85,0,0,0,.85.85h3.07a.85.85,0,0,0,.85-.85V291.5l8.74,11.79a.7.7,0,0,0,.21.21h0l.05,0h0l0,0,0,0h0l.06,0h0a.83.83,0,0,0,.22,0H433a.85.85,0,0,0,.85-.85v-19a.85.85,0,0,0-.85-.85" transform="translate(-377.06 -256.91)" style="fill:#3aae37"/><path d="M404.49,298.89h-8.33V283.77a.85.85,0,0,0-.85-.85h-3.07a.85.85,0,0,0-.85.85v19h0a.83.83,0,0,0,.24.59h0v0a.89.89,0,0,0,.59.23h12.25a.85.85,0,0,0,.85-.85v-3.06a.85.85,0,0,0-.85-.85" transform="translate(-377.06 -256.91)" style="fill:#3aae37"/><path d="M449.89,287.68a.85.85,0,0,0,.85-.85v-3.06a.85.85,0,0,0-.85-.85H437.65a.83.83,0,0,0-.59.24h0l0,0a.87.87,0,0,0-.23.59h0v19h0a.83.83,0,0,0,.24.59h0a.83.83,0,0,0,.59.24h12.24a.85.85,0,0,0,.85-.85v-3.06a.85.85,0,0,0-.85-.85h-8.33v-3.22h8.33a.85.85,0,0,0,.85-.85v-3.07a.85.85,0,0,0-.85-.85h-8.33v-3.22Z" transform="translate(-377.06 -256.91)" style="fill:#3aae37"/></g></g></svg>
						<span>LINE</span>
					</a>
				</div>
			</div>		
		</div>
	</div>
</div>

<?php include('footer.php'); ?>