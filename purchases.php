<?php include('guestadmin-header.php'); ?>

	<section class="container p-5">
		<div class="row">
			<div class="col-md-12">
				<h3>Purchases - All (6)</h3>
				<div class="card">
					<table class="table table-hover">
						<thead class="thead-light text-center">
							<tr>
								<th scope="col">Name</th>
								<th scope="col">Event</th>
								<th scope="col">Ticket</th>
								<th scope="col"># of Tickets</th>
								<th scope="col">Time Purchased</th>
							</tr>
						</thead>
						<tbody class="text-center">
							<tr>
								<td>John Doe</td>
								<td><a href="purchases-1.php">Fyre Festival</a></td>
								<td>VIP</td>
								<td>1</td>
								<td>2019-07-02 20:32</td>
								
							</tr>
							<tr>
								<td>John Smith</td>
								<td><a href="purchases-1.php">Event #1</a></td>
								<td>GA</td>
								<td>3</td>
								<td>2019-07-01 12:12</td>
								
							</tr>
							<tr>
								<td>Jane Wilson</td>
								<td><a href="purchases-1.php">Fyre Festival</a></td>
								<td>ADV</td>
								<td>2</td>
								<td>2019-06-20 18:38</td>
								
							</tr>
							<tr>
								<td>Taro Yamada</td>
								<td><a href="purchases-1.php">Fyre Festival</a></td>
								<td>VIP</td>
								<td>1</td>
								<td>2019-06-19 20:22</td>
								
							</tr>
							<tr>
								<td>John A</td>
								<td><a href="purchases-1.php">Event #1</a></td>
								<td>GA</td>
								<td>1</td>
								<td>2019-06-18 12:32</td>
								
							</tr>
							<tr>
								<td>Gui Tamada</td>
								<td><a href="purchases-1.php">Fyre Festival</a></td>
								<td>GA</td>
								<td>2</td>
								<td>2019-06-17 18:31</td>
								
							</tr>


						</tbody>
					</table>
				</div>
				<div class="text-center">
					<a href="guestadmin.php" class="btn btn-outline-brand btn-lg">Return to Home Page</a>
				</div>
			</div>
		</div>
	</section>

<?php include('guestadmin-footer.php'); ?>