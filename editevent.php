<?php include('guestadmin-header.php'); ?>

<section class="container p-5">
	<form class="needs-validation" action="event-preview.php" novalidate>
		<div class="row justify-content-center">
			<div class="col-md-8">
				<h3>Edit Your Event</h3>

				<?php 
				include('guestadmin/add-1.php');
				include('guestadmin/add-3.php');

				?>
			</div>

			<div class="col-md-8 mt-5">
				<h3>Edit your Tickets</h3>
				<?php include('guestadmin/add-2.php'); ?>

				<div class="alert alert-warning my-5">
					By submitting the updated event <strong>you agree to have your event reviewed by the Media, who has the rights to reject your event regardless of its current status.</strong> If rejected, you have one more chance to revise and submit the event again; beyond that you cannot create or edit any more events, and your account will be under review.
				</div>

				<div class="text-center">
					<button class="btn btn-brand btn-lg" type="submit">Preview and Submit</button>
					<a href="addevent-1.php"><button class="btn btn-secondary btn-lg">Cancel</button></a>
				</div>
			</div>
		</div>
	</form>
</section>

<?php include('guestadmin-footer.php'); ?>

<script>
	$(function () {
		$('.add-one').click(function(){
			$('.ticket-single').first().clone().appendTo('.ticket-info').show();
			attach_delete();
		});

		function attach_delete(){
			$('.delete-one').off();
			$('.delete-one').click(function(){
				$(this).closest('.ticket-single').remove();
			});
		}

		$('[data-toggle="tooltip"]').tooltip();

		$('.form-control-price').attr('readonly', true);
	})
</script>