<?php include('header.php'); ?>

<section class="bg-overlay bg-overlay-zaiko full-h d-flex align-items-center p-x" style="background: url('/img/bg-500.jpg') no-repeat center center / cover">
	<div class="container-fluid">
		<div class="col-md-8 col-lg-8">
			<div class="text-block">
				<h4>500 Server Error</h4>
				<h1 class="hero-title">
					Sorry, the page you were looking for is temporarily not available.
				</h1>
				<a href="login.php" class="btn btn-xl btn-default d-xs-block"><i class="fas fa-chevron-left"></i>&nbsp;&nbsp;Return to Home Page</a>
			</div>
		</div>
	</div>
</section>

<?php include('footer.php'); ?>
