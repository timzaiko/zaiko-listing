<aside class="admin-aside collapse show" id="navbar-sidebar">
	
	<ul class="px-0 py-lg-5 py-3 mb-0" >

		<li data-toggle="collapse" data-target="#admin-submenu1" aria-expanded="false" aria-controls="admin-submenu1" class="">
			<span class="px-4 d-flex justify-content-between">
				<div class="">
					Overview
				</div>
				<span class="typcn typcn-chevron-right"></span>
			</span>
		</li>
		<li id="admin-submenu1" class="collapse show">
			<a href="#" target="" class="sub-menu">Dashboard</a>
			<a href="#" target="" class="sub-menu">Details</a>
			<a href="admin-account.php" target="" class="sub-menu">Recent Growth</a>
			<a href="admin-account-password.php" target="" class="sub-menu">Marketing Page</a>

			<a href="admin-account-payment.php" target="" class="sub-menu">Latest Posts</a>

			<a href="admin-account-receipts.php" target="" class="sub-menu">Latest Mails</a>

			<a href="admin-account-subscription.php" target="" class="sub-menu">Restricted Content</a>

		</li>

		<li>
			<a href="#" class="px-4">
				Contacts
			</a>
		</li>

		<li>
			<a href="#" class="px-4">
				Insights
			</a>
		</li>

		<li data-toggle="collapse" data-target="#admin-submenu4" aria-expanded="false" aria-controls="admin-submenu4" class="">
			<span class="px-4 d-flex justify-content-between">
				<div class="">
					Mails
				</div>
				<span class="typcn typcn-chevron-right"></span>
			</span>
		</li>
		<li id="admin-submenu4" class="collapse show">
			<a href="admin-account-password.php" target="" class="sub-menu">Overview</a>
			<a href="#" target="" class="sub-menu">Create Mail</a>
			<a href="admin-account.php" target="" class="sub-menu">Settings</a>
		</li>


	</li>


</ul>
</aside>