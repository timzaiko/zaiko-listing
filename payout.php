<?php include('guestadmin-header.php'); ?>

<section class="container-fluid">
	<div class="text-center pt-5">
		<div class="text-brand">
			<i class="fas fa-university h1"></i>
			<h4 class="fa-step--text">Please confirm your bank information below for payout</h4>
		</div>
	</div>
</section>

<section class="container py-4">
	<div class="row justify-content-center">
		<div class="col-md-6">
			<form class="needs-validation" action="guestadmin-payout.php" novalidate>

				<section class="py-4">
					<div class="status-details">
						<li class="d-flex">
							<p>Company Name</p>
							<strong>Magnises ltd.</strong>
						</li>
						<li class="d-flex">
							<p>Contact Name</p>
							<strong>Billy McFarland</strong>
						</li>
						<li class="d-flex">
							<p>Contact Email</p>
							<strong>billy@fyrefestival.com</strong>
						</li>
						<li class="d-flex">
							<p>Contact Phone Number</p>
							<strong>8001234567</strong>
						</li>
						<hr>
						<h4>Bank Details</h4>
						<li class="d-flex">
							<p>Bank Name</p>
							<strong>Bank of America</strong>
						</li>
						<li class="d-flex">
							<p>Branch Name</p>
							<strong>101 Broadway Avenue</strong>
						</li>
						<li class="d-flex">
							<p>Branch Code</p>
							<strong>50102</strong>
						</li>
						<li class="d-flex">
							<p>Account Number</p>
							<strong>100829128</strong>
						</li>
						<li class="d-flex">
							<p>Account Type</p>
							<strong>Savings</strong>
						</li>
						<li class="d-flex">
							<p>Account Name</p>
							<strong>Williamymcfarland</strong>
						</li>
						<hr>
						<li class="d-flex">
							<p>Payout Amount</p>
							<strong>29,000 Japanese Yen</strong>
						</li>
						<li class="d-flex">
							<p>Expected Transfer Date</p>
							<strong>2019-10-20</strong>
						</li>

					</div>

					<div class="form-check text-center my-4">
						<input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
						<label class="form-check-label" for="defaultCheck1">
							I agree to <a href="#" data-toggle="modal" data-target="#modal-terms">ZAIKO Terms and Conditions</a>
						</label>
					</div>

					<div class="text-center">
						<a href="addbank.php" class="btn btn-secondary btn-lg">Edit your bank info</a>
						<button class="btn btn-brand btn-lg" type="submit">Submit for Payout</button>
					</div>

				</form>
			</div>
		</div>
	</div>
</section>


<?php include('guestadmin-footer.php'); ?>


<script>
	// Example starter JavaScript for disabling form submissions if there are invalid fields
	(function() {
		'use strict';
		window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
    	form.addEventListener('submit', function(event) {
    		if (form.checkValidity() === false) {
    			event.preventDefault();
    			event.stopPropagation();
    		}
    		form.classList.add('was-validated');
    	}, false);
    });
}, false);
	})();
</script>