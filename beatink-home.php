<?php include('header-beatink-transparent.php'); ?>

<div class="microsite">

	<section class="swiper-container">
		<div class="swiper-wrapper">
			<div class="banner bg-overlay opacity-40 swiper-slide" style="background: url('https://www.beatink.com/upload/save_image/02041627_5e391cffd1bd8.jpg') no-repeat center center / cover">
				<div class="bg-gradient bg-gradient-top"></div>
				<div class="d-flex align-items-center position-absolute w-100">
					<div class="container">
						<div class="row">	
							<div class="col-md-12">
								<h1>Live Coverage</h1>
								<h4 class="font-weight-light">This Month's Best Event</h4>
								<a href="#"" class="btn btn-lg btn-default">Check it out</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="banner bg-overlay opacity-40 swiper-slide" style="background: url('https://d38fgd7fmrcuct.cloudfront.net/1_3t18rn1rdpna7qdk1of1l.jpg') no-repeat center center / cover">
				<div class="bg-gradient bg-gradient-top"></div>
				<div class="d-flex align-items-center position-absolute w-100">
					<div class="container">
						<div class="row">	
							<div class="col-md-12">
								<h1>Best Sellers</h1>
								<h4 class="font-weight-light">This Month's Best Event</h4>
								<a href="#"" class="btn btn-lg btn-default">Click Here</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="banner bg-overlay opacity-40 swiper-slide" style="background: url('https://www.beatink.com/upload/save_image/04171834_5cb6f3132940f.jpg') no-repeat center center / cover">
				<div class="bg-gradient bg-gradient-top"></div>
				<div class="d-flex align-items-center position-absolute w-100">
					<div class="container">
						<div class="row">	
							<div class="col-md-12">
								<h1>Monthly Shows</h1>
								<h4 class="font-weight-light">This Month's Best Event</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="swiper-button-next"></div>
		<div class="swiper-button-prev"></div>
	</section>

	<section class="container pt-5">
		<div class="row">
			<div class="col-md-12">
				<h3 class="font-weight-normal">Popular Events</h3>
				<h5 class="font-weight-light text-muted">Most visited events in the past 7 days</h5>
			</div>
		</div>
	</section>

	<?php include('listing-popular.php'); ?>


	<section class="container">
		<div class="row">
			<div class="col-md-8">
				<h3 class="font-weight-normal">Upcoming Events</h3>
				<h5 class="font-weight-light text-muted">Catch up to the hottest events before they sold out</h5>

				<?php
				include('listing-upcoming.php'); 
				?>

				<div class="text-center">
					<form action="listing-upcoming-single.php">
						<input type="submit" class="btn btn-lg btn-default" value="View All Upcoming Events">
					</form>
				</div>
			</div>
			<div class="col-md-4">
				<div class="card">
				<a class="twitter-timeline" data-height="500" href="https://twitter.com/beatink_jp?ref_src=twsrc%5Etfw">Tweets by beatink_jp</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
				</div>
			</div>
		</div>
	</section>

</div>

<div id="modal-login" class="modal fade show" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<?php include('modal-login.php') ?>
	</div>
</div>

<?php include('footer.php'); ?>

<script>

	var swiper = new Swiper('.swiper-container', {
		loop: true,
		speed: 700,
		effect: 'fade',
		autoplay: {
			delay: 4000,
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
	});

	var mySwiper = new Swiper ('.swiper', {
		direction: 'horizontal',
		loop: true,
		speed: 700,
		autoplay: {
			delay: 2000,
		},
		slidesPerView: 4,
		spaceBetween: 10,
		breakpoints: {
			640: {
				slidesPerView: 2
			},
			1080: {
				slidesPerView: 6
			}
		}
	})
</script>