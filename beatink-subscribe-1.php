<?php include('header-beatink.php'); ?>


<section class="container py-5">
	<div class="row">
		<div class="card-order col-lg-4 order-lg-last">
			<div class="card p-3 p-md-4">

				<h4 class="text-center text-md-left mb-3">Order Details</h4>
				<div class="" id="prices">

					<div class="ticket-price single d-flex justify-content-between align-items-center py-2">
						<span>						
							<p>VIP Membership</p>
							<h5 class="mb-1 text-primary">￥5000 <span class="small">/ year</span> </h5>

						</span>

						<button class="d-flex align-items-center p-0">
							<p class="text-muted mr-2">x 1</p>
							<span class="px-0 typcn typcn-pencil"></span>

						</button>
					</div>

					<div class="ticket-price single d-flex justify-content-between align-items-center py-2">
						<span>						
							<p>Handling Fee</p>
							<p class="small" data-toggle="tooltip" data-placement="bottom" title="this is extra charge on your ticket price">what is this?</p>
						</span>

						<a class="text-muted">￥800</a>		
					</div>

					<div class="ticket-price d-flex justify-content-between align-items-center pt-3">
						<h4>Total</h4>
						<h4 class="text-primary">￥5,800</h4>
					</div>

				</div>
			</div>


			<!-- <div class="alert alert-dark text-center">Checkout Time left 5:19</div> -->


		</div>

		<div class="col-lg-8">
			<div class="card p-4">
				<div class="row">
					<div class="col-md-12">
						<h4>Payment Method</h4>

						<ul class="d-flex flex-wrap payment-buttons p-0">
							<li class="selected">
								<a href="#ccDetails" class="showCardDetails">
									<h5 class="mb-0">Use Credit Card On File</h5>
									<svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 400 150"><title>master-card</title><g id="surface1"><path d="M250.67,54.94,229.85,34.08v83.59h27.87V71.87A23.88,23.88,0,0,0,250.67,54.94Z" style="fill:#ffb782"/><path d="M158.56,102.44,142.82,61.58a8,8,0,0,1,4.56-10.29l74.29-28.62A8,8,0,0,1,232,27.24L247.7,68.1a8,8,0,0,1-4.57,10.29L168.85,107A8,8,0,0,1,158.56,102.44Z" style="fill:#de4c3c"/><path d="M237.64,42,148.56,76.49l5.93,15.39,89.08-34.51Z" style="fill:#7a4930"/><path d="M142.28,73.88V30.1a8,8,0,0,1,8-8h79.6a8,8,0,0,1,8,8V73.88a8,8,0,0,1-8,8h-79.6A8,8,0,0,1,142.28,73.88Z" style="fill:#4398d1"/><path d="M229.85,22.14H221.4l-59.71,59.7h68.16a8,8,0,0,0,8-8V30.1A8,8,0,0,0,229.85,22.14Z" style="fill:#3e8cc7"/><path d="M150.25,54h8v4h-8Z" style="fill:#5eb3d1"/><path d="M150.25,65.92h8v4h-8Z" style="fill:#5eb3d1"/><path d="M186.07,65.92h8v4h-8Z" style="fill:#5eb3d1"/><path d="M162.19,54h8v4h-8Z" style="fill:#5eb3d1"/><path d="M174.13,54h8v4h-8Z" style="fill:#5eb3d1"/><path d="M186.07,54h8v4h-8Z" style="fill:#5eb3d1"/><path d="M223.88,30.1h4v6h-4Z" style="fill:#5eb3d1"/><path d="M215.92,30.1h4v6h-4Z" style="fill:#5eb3d1"/><path d="M208,30.1h4v6h-4Z" style="fill:#5eb3d1"/><path d="M200,30.1h4v6h-4Z" style="fill:#5eb3d1"/><path d="M221.89,117.67h35.83v19.9H221.89Z" style="fill:#a5a5a5"/><path d="M216.05,58.09A8.63,8.63,0,0,0,203.59,70L219.9,87.81a26.84,26.84,0,0,0,.55,27.54l1.44,2.32h25.88V89.8Z" style="fill:#ffb782"/><path d="M227.86,123.64h4v4h-4Z" style="fill:#34495e"/><path d="M150.25,42.84V33.28a3.18,3.18,0,0,1,3.18-3.18H163a3.18,3.18,0,0,1,3.19,3.18v9.56A3.19,3.19,0,0,1,163,46h-9.55A3.18,3.18,0,0,1,150.25,42.84Z" style="fill:#fdb62f"/><path d="M150.25,36.07h6v4h-6Z" style="fill:#fd7b2f"/><path d="M160.2,36.07h6v4h-6Z" style="fill:#fd7b2f"/><path d="M245.78,89.8a2,2,0,0,1-1.41-.58l-8-8a2,2,0,0,1,2.81-2.81l8,8a2,2,0,0,1,0,2.81A2,2,0,0,1,245.78,89.8Z" style="fill:#f2a46f"/></g><rect width="400" height="150" style="fill:none"/></svg>

									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
								</a>
							</li>

							<li>
								<a class="showCardDetails">
									<h5 class="mb-0">Use New Credit Card</h5>

									<svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 400 150"><title>master-card</title><rect width="400" height="150" style="fill:none"/><path d="M270.13,115.07a8.29,8.29,0,0,1-8.29,8.29H138.16a8.29,8.29,0,0,1-8.29-8.29V34.93a8.29,8.29,0,0,1,8.29-8.29H261.84a8.29,8.29,0,0,1,8.29,8.29v80.14Z" style="fill:#e7e8e3"/><rect x="129.87" y="45.45" width="140.26" height="19.73" style="fill:#34495e"/><rect x="129.87" y="65.18" width="140.26" height="12.21" style="fill:#fff"/><path d="M135.77,115.07V34.93a8.29,8.29,0,0,1,8.29-8.29h-5.9a8.29,8.29,0,0,0-8.29,8.29v80.14a8.29,8.29,0,0,0,8.29,8.29h5.9A8.29,8.29,0,0,1,135.77,115.07Z" style="fill:#202121;opacity:0.15000000596046448;isolation:isolate"/><path d="M232.59,100.25a13.05,13.05,0,0,1,4.86-10.18,13.12,13.12,0,1,0,0,20.37A13.06,13.06,0,0,1,232.59,100.25Z" style="fill:#fff"/><path d="M245.7,87.14a13,13,0,0,0-8.25,2.93,13.1,13.1,0,0,1,0,20.37,13.11,13.11,0,1,0,8.25-23.3Z" style="fill:#898989"/><path d="M242.31,100.25a13.08,13.08,0,0,0-4.86-10.18,13.1,13.1,0,0,0,0,20.37A13.09,13.09,0,0,0,242.31,100.25Z"/><path d="M173.72,93.28H145.06a2.29,2.29,0,1,1,0-4.58h28.66a2.29,2.29,0,0,1,0,4.58Z" style="fill:#fff"/><path d="M173.72,102.78H145.06a2.29,2.29,0,1,1,0-4.57h28.66a2.29,2.29,0,1,1,0,4.57Z" style="fill:#fff"/><path d="M173.72,112.29H145.06a2.29,2.29,0,1,1,0-4.58h28.66a2.29,2.29,0,0,1,0,4.58Z" style="fill:#fff"/></svg>


									<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit.             
									</p>
								</a>

							</li>

						</ul>
					</div>
				</div>
			</div>

			<h5>Use Credit Card On File</h5>

			<div class="table-responsive card p-0">
				<table class="table table-hover table-lang table-md">
					<thead class="thead-dark">
						<tr class="text-nowrap">
							<th>Select</th>
							<th>Card Number</th>
							<th>Name on Card</th>
							<th>Expires</th>

						</tr>
					</thead>
					<tbody>
						<tr>
							<td><input class="" type="radio" value="" id=""></td>
							<td class="text-nowrap">**** **** **** 6274</td>
							<td>Timothy Lee</td>
							<td>09/2022</td>
						</tr>

						<tr>
							<td><input class="" type="radio" value="" id=""></td>
							<td class="text-nowrap">**** **** **** 9991</td>
							<td>Taro Yamada</td>
							<td>01/2021</td>
						</tr>
					</tbody>
				</table>
			</div>

			<h5>Use New Credit Card</h5>
			<div class="card p-4 hover">
				<div class="credit_card_input">
					<div class="form-group">	
						<label class="mini_section_details">Card Number</label> 
						<div id="card-element" class="form-control StripeElement StripeElement--empty">
							<div class="__PrivateStripeElement" style="margin: 0px !important; padding: 0px !important; border: none !important; display: block !important; background: transparent !important; position: relative !important; opacity: 1 !important;">
								<iframe frameborder="0" allowtransparency="true" scrolling="no" name="__privateStripeFrame5" allowpaymentrequest="true" src="https://js.stripe.com/v3/elements-inner-card-1a817ef9682b56891b906e4f6b7c4a5a.html#componentName=card&amp;wait=false&amp;rtl=false&amp;keyMode=test&amp;apiKey=pk_test_9pFn9gHum4wiWhq9B3Oe5e9X&amp;origin=https%3A%2F%2Fzaiko.dev&amp;referrer=https%3A%2F%2Fzaiko.dev%2Ftest%2Fcheckout%3Fid%3D15919519%26type%3Duser&amp;controllerId=__privateStripeController1" title="Secure payment input frame" style="border: none !important; margin: 0px !important; padding: 0px !important; width: 1px !important; min-width: 100% !important; overflow: hidden !important; display: block !important; user-select: none !important; height: 16.8px;"></iframe>
								<input class="__PrivateStripeElement-input" aria-hidden="true" aria-label=" " autocomplete="false" maxlength="1" style="border: none !important; display: block !important; position: absolute !important; height: 1px !important; top: 0px !important; left: 0px !important; padding: 0px !important; margin: 0px !important; width: 100% !important; opacity: 0 !important; background: transparent !important; pointer-events: none !important; font-size: 16px !important;">
							</div>
						</div> 
						<div id="card-errors" role="alert"></div> 
					</div>
					<div class="form-group">
						<label class="mini_section_details">Name on Card</label> 
						<input type="text" placeholder="ex. John Smith" class="form-control">
					</div>

				</div>
			</div>

			<div class="text-center">
				<a href="payment-2.php" class="btn btn-lg btn-pink text-center btn-inline-block">Complete Order</a>
				<a href="listing-display.php" class="btn btn-lg btn-secondary text-center btn-inline-block">Cancel</a>
			</div>
		</div>
	</div>
</section>

<?php include('footer.php'); ?>

<script>

</script>
