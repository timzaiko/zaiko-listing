<!DOCTYPE html>
<html lang="en">
<head>
	<title>ZAIKO</title>
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
	<link rel="icon" href="/img/favicon.ico">

	<!-- swiper css -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css">

	<link type="text/css" rel="stylesheet" href="/css/vone/app.css">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css">
	<!-- <link type="text/css" rel="stylesheet" href="/vue/my-project/src/static/app.css"> -->
</head>

<body class>
	<div id="app">
		<header class="admin-header sticky-top">
			<nav class="navbar navbar-expand-lg admin-navbar--bg container-fluid d-flex justify-content-between p-4">
				<div class="d-flex align-items-center">
					<div class="admin-profile">
						<a class="navbar-toggler navbar-toggler-sidebar d-block" data-toggle="collapse" data-target="#navbar-sidebar" aria-expanded="false">
							<span></span>
							<span></span>
							<span></span>
						</a>
						<a href="admin-home.php" class="d-flex align-items-center ml-4">
							<img class="admin-profile-img" src="https://d38fgd7fmrcuct.cloudfront.net/bw_40/bh_40/pf_1/1_3qka8eovxrr0kifg8v30g.png">
							<span class="h5 admin-profile-name m-0 pl-2">BEATINK</span>
						</a>
					</div>
				</div>

				<div class="admin-navbar d-flex flex-grow-1 flex-lg-row-reverse justify-content-between align-items-center">
					<ul class="collapse navbar-collapse navbar-collapse-zaiko menu menu-right m-lg-0 p-lg-0 flex-lg-row-reverse" id="navbar-user">
						<li class="dropdown-user d-flex flex-column align-items-center">
							<div class="d-flex align-items-center justify-lg-content-end w-100">
								<img class="img-profile mr-2" src=" https://d38fgd7fmrcuct.cloudfront.net/bw_300/bh_300/pf_1/1_3r2xttwhvbo3aq0p0bkqp" alt="">
								<span class="welcome d-block pr-2">Taro Yamada</span>
							</div>
							<ul class="sub-menu sub-menu--user right w-100 py-3 px-0">
								<li>
									<a class="" href="https://zaiko.io/legacy_support/view_my_ticket">View My Tickets</a>
								</li>
								<li>
									<a class="" href="https://zaiko.io/legacy_support/settings">Account Settings</a>
								</li>

								<li class="mx-lg-3">
									<a class="btn btn-default d-block text-white" href="https://zaiko.io/legacy_support/admin">
										Business
									</a>
								</li>

								<li>
									<a dusk="logout_link" href="https://zaiko.io/legacy_support/logout_link" class="">Sign Out</a>
								</li>
							</ul>

						</li>
						<li class="dropdown-lang d-flex align-items-center">
							<div class="dropdown">
								<span class="dropdown-toggle" id="langButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="flag-icon flag-icon-en"></i>
									<span class="ml-2">English</span>
								</span>
								<ul class="dropdown-menu sub-menu sub-menu--user right w-100 py-3 px-0 " aria-labelledby="langButton">
									<li>
										<a href="https://zaiko.io/theflame/language/manager?lang=ja"><i class="mr-2 flag-icon flag-icon-ja"></i>
											<span>日本語</span>
										</a>
									</li>
									<li>
										<a href="https://zaiko.io/theflame/language/manager?lang=ko"><i class="mr-2 flag-icon flag-icon-ko"></i>
											<span>한국어</span>
										</a>
									</li>
									<li>
										<a href="https://zaiko.io/theflame/language/manager?lang=zh-hans"><i class="mr-2 flag-icon flag-icon-zh-hans"></i>
											<span>简体中文</span>
										</a>
									</li>
									<li>
										<a href="https://zaiko.io/theflame/language/manager?lang=zh-hant"><i class="mr-2 flag-icon flag-icon-zh-hant"></i>
											<span>繁體中文</span>
										</a>
									</li>
								</ul>
							</div>
						</li>
						<hr class="d-lg-none">                
						<nav class="admin-navbar">
							<ul class="m-0 p-0">

								<li>
									<a href="admin-home.php">
										<i class="material-icons z4mico-home"></i>
										Home
									</a>
								</li>



								<li>
									<a href="admin-event.php">
										<i class="material-icons z4mico-event"></i>
										Events
									</a>
									<ul class="sub-menu py-lg-3 px-0">
										<li class="text-lg-nowrap py-0 py-lg-2">
											<a class="" href="/_events/add.php">
												Create New Event
											</a>
										</li>
									</ul>
								</li>



								<li>
									<a href="/_members/">
										<i class="material-icons z4mico-people"></i>
										Members
									</a>
								</li>



								<li>
									<a href="/_reports/">
										<i class="material-icons z4mico-show_chart"></i>
										Reports
									</a>
								</li>



								<li>
									<a href="admin-account.php">
										<i class="material-icons z4mico-group_work"></i>
										Profiles
									</a>
									<ul class="sub-menu py-lg-3 px-0">
										<li class="text-lg-nowrap py-0 py-lg-2">
											<a class="" href="/i/">
												User Name
											</a>
										</li>
										<li class="text-lg-nowrap py-0 py-lg-2">
											<a class="" href="/admin/?set_id=154">
												TimTam Band
											</a>
										</li>
										<li class="text-lg-nowrap py-0 py-lg-2">
											<a class="" href="/admin/?set_id=1">
												ZAIKO
											</a>
										</li>
										<li class="text-lg-nowrap py-0 py-lg-2">
											<a class="" href="/_profiles/select.php">
												Select Profile
											</a>
										</li>
										<li class="text-lg-nowrap py-0 py-lg-2">
											<a class="" href="/_profiles/edit/info.php">
												Create New Profile
											</a>
										</li>
									</ul>
								</li>



				<!-- 			<li>
								<a href="/vpaprofile/">
									<i class="material-icons z4mico-change_history"></i>
									VPA + Accounts
								</a>
								<ul class="sub-menu py-lg-3 px-0">
									<li class="text-lg-nowrap py-0 py-lg-2">
										<a class="" href="/vpaprofile/?show=artists">
											Artists
										</a>
									</li>
									<li class="text-lg-nowrap py-0 py-lg-2">
										<a class="" href="/vpaprofile/?show=venues">
											Venues
										</a>
									</li>
									<li class="text-lg-nowrap py-0 py-lg-2">
										<a class="" href="/vpaprofile/?show=pages">
											Promoters
										</a>
									</li>
									<li class="text-lg-nowrap py-0 py-lg-2">
										<a class="" href="/user/">
											Accounts
										</a>
									</li>
								</ul>
							</li> -->


							
					<!-- 		<li>
								<a href="/theflame/">
									<i class="material-icons z4mico-apps"></i>
									ADMIN
								</a>
								<ul class="sub-menu py-lg-3 px-0">
									<li class="text-lg-nowrap py-0 py-lg-2">
										<a class="" href="/corkboard/">
											corkboard <span>Custom landing page</span>
										</a>
									</li>
									<li class="text-lg-nowrap py-0 py-lg-2">
										<a class="" href="/theflame/">
											the FLAME <span>general admin</span>
										</a>
									</li>
									<li class="text-lg-nowrap py-0 py-lg-2">
										<a class="" href="/support/support_manager.php">
											Support Manger <span>CRM support system</span>
										</a>
									</li>
								</ul>
							</li> -->


						</ul>
					</nav>
				</ul>
			</div>

			<a class="navbar-toggler navbar-toggler-zaiko" data-toggle="collapse" data-target="#navbar-user" aria-expanded="false">
				<span></span>
				<span></span>
				<span></span>
			</a>

		</nav>

	</header>

	<div class="container-fluid">
		<div class="row flex-lg-nowrap">