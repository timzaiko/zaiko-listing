<?php include('guestadmin-header.php'); ?>

<section class="call-to-action" style="background: url('/img/bg-business.jpg') no-repeat center center / cover">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-sm-12 mx-auto text-center sr-reveal">
				<h2>Set up your events in minutes</h2>
				<p class="lead">We make ticketing as simple as humanly possible, so you don’t have to. Get the most out of your ticket sales experience with our robust platform.</p>
				<a href="guestadmin.php" class="btn btn-xl btn-brand my-4">Get Started Now</a>
			</div>
		</div>
	</div>
</div>
</section>

<section class="container-fluid">
	<div class="row">
		<div class="col-md-6 col-sm-12 d-flex align-items-center">
			<div class="p-10rem pl-md-0">
				<h2>Your Events, Powered By Us</h2>
				<p class="lead">Bespoke your event any way you want. Concerts, conferences, meet-and-greets - if you can name it, we can get it done.</p>
				<div class="list-check d-flex flex-wrap mt-4">
					<div class="list-item col-md-6 p-0">
						<img src="/img/ico-check.svg" alt="" width="16" height="16">
						<span>Ticket Types like VIP, Pre-Sale</span>
					</div>
					<div class="list-item col-md-6 p-0">
						<img src="/img/ico-check.svg" alt="" width="16" height="16">
						<span>100% Self-Service</span>
					</div>
					<div class="list-item col-md-6 p-0">
						<img src="/img/ico-check.svg" alt="" width="16" height="16">
						<span>Discount Option Available</span>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6 order-md-first d-flex align-items-end pt-md-5 sr-reveal">
			<img class="img-fluid " src="/img/guestadmin/guest-events.png" alt="">
		</div>
	</div>
</section>

<section class="container-fluid bg-light-grey">
	<div class="row">
		<div class="col-md-5 d-flex align-items-center">
			<div class="p-10rem pr-md-0">
				<h2>Engage With Your Fanbase</h2>
				<p class="lead">Your biggest asset is your followers, your fans. Spend more time convert them into paying customers, and let us handle the rest of the ticket payment process.</p>
				
				<div class="list-check d-flex flex-wrap mt-4">
					<div class="list-item col-md-6 p-0">
						<img src="/img/ico-check.svg" alt="" width="16" height="16">
						<span>100% Paperless Tickets</span>
					</div>
					<div class="list-item col-md-6 p-0">
						<img src="/img/ico-check.svg" alt="" width="16" height="16">
						<span>Fraud-Proof Check-in System</span>
					</div>
					<div class="list-item col-md-6 p-0">
						<img src="/img/ico-check.svg" alt="" width="16" height="16">
						<span>PayPal &amp; CVS Accepted</span>
					</div>
					<div class="list-item col-md-6 p-0">
						<img src="/img/ico-check.svg" alt="" width="16" height="16">
						<span>Stripe Partner</span>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-7 d-flex align-items-end pt-md-5 sr-reveal">
			<img class="img-fluid" src="/img/guestadmin/guest-mogiri.png" alt="">
		</div>

	</div>
</section>

<section class="container-fluid">
	<div class="row">
		<div class="col-md-6 d-flex align-items-center">
			<div class="p-10rem pl-md-0">
				<h2>Data at your Service</h2>
				<p class="lead">We have automation tools to analyze and act on the data collected from your events. Harness the power of data for smarter sales strategy when planning your next events.</p>
				<div class="list-check d-flex flex-wrap mt-4">
					<div class="list-item col-md-6 p-0">
						<img src="/img/ico-check.svg" alt="" width="16" height="16">
						<span>Secure User Data</span>
					</div>
					<div class="list-item col-md-6 p-0">
						<img src="/img/ico-check.svg" alt="" width="16" height="16">
						<span>We Never Share Any Data</span>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6 order-md-first d-flex align-items-end pl-0 pt-md-5 sr-reveal">
			<img class="img-fluid" src="/img/business-3.png" alt="">
		</div>
	</div>
</section>

<section class="call-to-action bg-overlay" style="background: url('/img/bg-about.jpg') no-repeat center center / cover">
	<div class="container">
		<div class="row">
			<div class="col-md-10 mx-auto text-center sr-reveal">
				<h2>Boost Your Event Presence</h2>
				<p class="lead my-5">By upselling your events will headline across TimeOut Tokyo online publications. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti autem eos ad, aut alias! Quo, sed esse dolorum rem iusto aliquid illum veniam quisquam veritatis, deserunt hic, vitae quia impedit!</p>
				<a href="#" data-toggle="modal" data-target="#modal-promote" class="btn btn-lg btn-brand">Promote Your Events Now</a>
			</div>
		</div>
	</div>
</section>

<?php include('guestadmin-modal-promote.php'); ?>


<!-- Pricing -->
<section class="bg-light-grey">
	<div class="container p-5rem">
		<div class="row">
			<div class="col-md-8 mx-auto text-center">
				<h2>Pricing</h2>
				<p class="lead mb-5">Set your own ticket prices with absolutely no hidden fees.</p>
				<div class="card-deck">
					<div class="card p-5 m-0 hover sr-reveal">
						<h3>TimeOut Tokyo E-Ticket</h3>
						<h1 class="card-title pricing-card-title d-inline-block">10%<sup>*</sup></h1>
						<p>*Ticketed Events will be charged 10%* + system fee (4.98%+ 98JPY).</p>
						<p>Ticket purchasers never pay a user fee.</p>
						<div>
							<a href="guestadmin.php" class="btn btn-lg btn-brand mt-3">Get Started Now</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="banner call-to-action bg-overlay text-center">
	<div class="sr-reveal">
		<h3>Want to know how we can help sell your tickets?</h3>
		<a href="guestadmin-contact.php" class="btn btn-xl btn-brand">Contact Us</a>
	</div>
</section>

<?php include('guestadmin-footer.php'); ?>
