<?php include('header.php'); ?>

<style>
	header,footer {
		display: none;
	}
</style>

<section class="bg-overlay opacity-40 bg-zaiko">

	<div class="container-fluid card-register">
		<div class="mx-auto card">
			<div class="card-body p-3 p-sm-5">

				<div class="d-flex justify-content-between align-items-center">
					<a href="/">
						<img class="img-header d-block mx-auto" src="https://d38fgd7fmrcuct.cloudfront.net/1_3srrgnchq4ywmry64ua57.png" alt="">
					</a>
					<p class="m-0">Create An Account</p>
				</div>

				<form class="needs-validation py-3" action="register-2.php" novalidate>
					<div class="form-group">
						<label>First Name</label>
						<input type="email" class="form-control" placeholder="Your First Name" required>
						<div class="invalid-feedback">
							Please enter correct
						</div>
					</div>  

					<div class="form-group">
						<label>Last Name</label>
						<input type="email" class="form-control" placeholder="Your Last Name" required>
						<div class="invalid-feedback">
							Please enter correct
						</div>
					</div>  

					<div class="alert alert-secondary">Please enter your legal name</div>

					<div class="form-group">
						<label>E-mail</label>
						<input type="email" class="form-control" placeholder="Your E-mail Address" required>
						<div class="invalid-feedback">
							Please enter correct
						</div>
					</div>  

					<div class="row form-group">
						<div class="col-6 pr-0">
							<label>Birthday</label>
							<input type="date" class="form-control" placeholder="" required>
							<div class="invalid-feedback">
								Please enter correct
							</div>

						</div>
						<div class="col-6">
							<label>Gender</label>
							<select type="number" class="form-control" placeholder=""required>
								<option value="">Male</option>
								<option value="">Female</option>
								<option value="">Undefined</option>
								
							</select>  
							<div class="invalid-feedback">
								Please enter correct
							</div>
						</div>
					</div>

					<div class="form-group">
						<label>Where are you from?</label>
						<input type="number" class="form-control" placeholder="Enter your city, prefecture or country" required>
						<div class="invalid-feedback">
							Please enter correct
						</div>
					</div>  

					<div class="form-group">
						<label>Create Password</label>
						<input type="password" class="form-control" placeholder="min. 8 characters or letters" required>
						<div class="invalid-feedback">
							Please enter correct
						</div>
					</div>  

					<div class="field-wrap"><input type="checkbox" required="required" id="checkboxTos" name="checkboxTos"> <a href="#" target="_blank" class="text-muted" data-toggle="modal" data-target="#modal-tos">You agree on our Terms Of Service</a></div>

					<div class="d-block mt-3">
						<button class="btn btn-pink btn-xl btn-block" type="submit">Continue</button>
					</div>

				</form>
			</div>

			<div class="card-footer d-flex justify-content-between align-items-center">
				<p class="m-0">Already have a ZAIKO account?</p>
				<a href="/login.php" class="btn btn-outline-dark">Sign In</a>
			</div>
		</div>
	</div>

</section>

<div id="modal-tos" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<h3>ZAIKO Terms of Service</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit doloremque similique officiis laboriosam consectetur accusantium incidunt tenetur. Vel, minus tenetur vitae adipisci nihil velit soluta minima voluptates, doloremque quos voluptatem.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe vero inventore ducimus laboriosam sunt ipsa explicabo non placeat laudantium fuga quaerat veritatis dolorem magni debitis corrupti, corporis iste. Est, praesentium!</p>
		</div>
	</div>
</div>

<?php include('footer.php'); ?>