<div class="swiper mb-4">    
	<div class="swiper-wrapper">
		<?php for($i= 0; $i < 10; $i++) { ?>
			<a href="listing-display.php" class="swiper-slide listing-popular">
				<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRUEkUz6arPg_ECZa6NCkhtOE6swMKp3rF6QEhbYkDLePf-0bZU&s" alt="">
				<span class="event event-info">
					<p class="font-weight-bold">{{ Event Name }}</p>
					<p>{{ Event Date }}</p>
				</span>
				<small class="event event-location">{{City}}, {{Country}}</small>
			</a>
		<?php } ?>
	</div> 	
</div>
