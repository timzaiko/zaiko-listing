<?php include('header.php'); ?>

<style>
	header,footer {
		display: none;
	}
</style>

<section class="bg-overlay opacity-40 bg-zaiko full-h d-flex justify-content-center align-items-center flex-column">

	<div class="container-fluid card-register">
		<div class="mx-auto card">
			<div class="card-body p-3 p-sm-5">

				<div class="d-flex justify-content-center align-items-center">
					<a href="/">
						<img class="img-header d-block mx-auto" src="https://d38fgd7fmrcuct.cloudfront.net/1_3srrgnchq4ywmry64ua57.png" alt="">
					</a>
				</div>

				<div class="d-flex justify-content-between align-items-center mt-3">
					<p class="m-0">Already have a ZAIKO account?</p>
					<a href="/login.php" class="btn btn-outline-dark">Sign In</a>
				</div>

			</div>

			
		</div>
	</div>

</section>


<?php include('footer.php'); ?>