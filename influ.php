<?php include('header.php'); ?>

<div class="influencer">

  <section class="container-fluid full-h d-md-flex align-items-center position-relative py-5">
    <div class="col-md-2"></div>
    <div class="mock wow fadeIn col-md-3">
      <img class="img-fluid" src="https://d38fgd7fmrcuct.cloudfront.net/1_3sr7s2m3b9f4b2eg99hji.png" alt="">
    </div>
    <div class="text-white h5 wow fadeIn mx-5">
      <h2 class="mb-0">ZAIKO FOR</h2>
      <h2 class="box mb-0">INFLUENCER</h2>
      <p class="h4 my-4 mr-5">無数にあるイベントの中から関心のあるチケットを販売してみませんか？
      ZAIKOインフルエンサーは、たった1枚の販売からでも収入を得ることができます。</p>
      <p class="h4 mb-4">さあ、早速チケットを告知してみましょう。</p>
      <a class="btn btn-xl btn-black btn-rounded text-center text-sm-left mb-5" href="/i/?login=1">今すぐ始めよう！</a>

    </div>
    <div class="col-md-1"></div>
  </section>


  <section class="container-fluid bg-black">
    <div class="row">
      <div class="col-lg-7 d-flex align-items-center">
        <div class="p-lg-5 m-5">
          <h2>自分に合ったイベントだけを自由にプロモーション。
           世界中の人たちに対して手軽にチケット販売が可能に！
          </h2>
          <p class="lead">ZAIKOプラットフォーム上にある様々なジャンルの
            イベントの中から選んで
            SNSやブログなどで好きな時間に投稿するだけ！
            インフルエンサーを通じて販売されたチケットは、
            手数料として販売額の一部をインフルエンサーに
            還元するので、手間をかけずに自身のブランディングを
            保ちながら収益をあげることができます。
          </p>
          <div class="list-check d-flex flex-wrap mt-4">
            <div class="list-item col-md-6 p-0">
              <img src="/img/ico-check.svg" alt="" width="16" height="16">
              <span>初期費用・運営コスト一切なし</span>
            </div>
            <div class="list-item col-md-6 p-0">
              <img src="/img/ico-check.svg" alt="" width="16" height="16">
              <span>チケット購入者や主催者とのやりとりなし</span>
            </div>

          </div>
        </div>
      </div>
      <div class="col-lg-1"></div>
      <div class="col-lg-3 col-md-12 text-center">
       <video src="/img/influencer.mp4" autoplay="" playsinline="" loop="" muted=""></video>
     </div>
   </div>
 </section>

 <section class="py-5 text-white bg-overlay" style="background: url('https://d38fgd7fmrcuct.cloudfront.net/1_3tbow9i63kwuv12h5yncm.jpg') no-repeat center center / cover">
  <div class="container">
    <div class="row">
      <div class="col-md-10 mx-auto text-center p-5">
        <h2>ZAIKOの多種多様なイベント</h2>
        <p class="lead py-3">ライブやクラブイベント、大型フェスなどの
          音楽イベントの他にも、ファッションやeスポーツ、
          アート展覧会、旅行系/体験型イベントまで
          幅広いジャンルのイベントの中から選ぶことができます！
        </p>
        
      </div>

      <div class="col-12 d-flex justify-content-between flex-lg-nowrap flex-wrap img-clients">
        <img src="https://d38fgd7fmrcuct.cloudfront.net//1_3tbp0gbpcloopltqxliw2.png" alt="">
        <img src="https://d38fgd7fmrcuct.cloudfront.net/1_3tbp1d8go4m2cycl1ag9h.png" alt="">
        <img src="https://d38fgd7fmrcuct.cloudfront.net/1_3tbp1cdwr8m0mhdkyq0o3.png" alt="">
        <img src="https://d38fgd7fmrcuct.cloudfront.net/1_3tbqcj2yqhcwux9h22e4o.png" alt="">
        <img src="https://d38fgd7fmrcuct.cloudfront.net/1_3tbqcj45r6lclskgyrxhr.png" alt="">
        <img src="https://d38fgd7fmrcuct.cloudfront.net/1_3tbp1bbhbqkecy8e90y31.png" alt="">
      </div>
    </div>
  </div>
</section>

<section class="bg-black py-5 text-white">
  <div class="container py-5">
    <div class="row mx-5">
      <div class="col-md-10 col-sm-12 mx-auto text-center">
        <h2 class="mb-5">ZAIKO INFLUENCERに<br>なるための3ステップ</h2>
      </div>
      <div class="col-md-4 text-center img-influencer">
        <img class="img-icon" src="https://d38fgd7fmrcuct.cloudfront.net/1_3tbp9x4t117brye2mj9m6.png" alt="">
        <h4 class="font-weight-normal">ステップ 1</h4>
        <p class="lead">アカウントを使って簡単にインフルエンサー登録</p>
      </div>
      <div class="col-md-4 text-center img-influencer">
       <img class="img-icon" src="https://d38fgd7fmrcuct.cloudfront.net/1_3tbp9x2ox2e58jy99h374.png" alt="">

       <h4 class="font-weight-normal">ステップ 2</h4>
       <p class="lead">で興味のあるイベントをイベントを見つけてピックアップ</p>
     </div>
     <div class="col-md-4 text-center img-influencer">
       <img class="img-icon" src="https://d38fgd7fmrcuct.cloudfront.net/1_3tbp9x0cvohp0yycnklhu.png" alt="">
       <h4 class="font-weight-normal">ステップ 3</h4><p class="lead">やブログなどでイベント告知自身のオリジナルチケットページにて販売</p>
     </div>
   </div>
 </div>
</section>

<section class="text-white bg-overlay opacity-40" style="background: url('https://d38fgd7fmrcuct.cloudfront.net/1_3tboqmg4gtyaf7bukl9pc.jpg') no-repeat center center / cover">
  <div class="container pt-5">
    <div class="row">
      <div class="col-md-6 pt-5">
        <div class="mx-5">
          <h2 >ZAIKOを活用して
            新たな収益を生み出し
            インフルエンサーとしての
            さらなる可能性を一緒に
            広げていきましょう！
          </h2>
          <a class="btn btn-xl btn-black btn-rounded text-center text-sm-left mb-5" href="/i/?login=1">今すぐ始めよう！</a>
        </div>
      </div>

      <div class="col-md-6 pt-md-5">
        <img class="img-fluid" src="https://d38fgd7fmrcuct.cloudfront.net/1_3tbpgnwag8f6qdcuecb7b.png" alt="">
      </div>
    </div>
  </div>
</section>



</div>

<?php include('footer.php'); ?>

<script>
	var mySwiper = new Swiper ('.swiper', {
		direction: 'horizontal',
		loop: true,
		speed: 700,
		autoplay: {
			delay: 2000,
		},
		slidesPerView: 4,
		spaceBetween: 10,
		breakpoints: {
			640: {
				slidesPerView: 2
			},
			1080: {
				slidesPerView: 4
			}
		}
	})
</script>