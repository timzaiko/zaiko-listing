<?php include('header.php'); ?>

<section class="banner bg-zaiko">
    <div class="text-block banner-narrow text-center ">
        <h2>Frequently Asked Questions</h2>
        <p>Please browse the topics below to find what you are looking for</p>
    </section>

    <?php include('cards.php'); ?>

    <div class="container-fluid">
        <div class="row faq">
            <div class="col-12 col-md-3 col-lg-3 faq__sidebar d-md-none d-lg-block">
                <?php include('sidebar.php') ?>
            </div>    

            <div class="col-12 col-md-12 col-lg-9 faq__content" data-spy="scroll" data-target="fag-groups" data-offset="0">
                <div class="answers">
                    <?php include('blocks/faq-topic-1.php') ?>
                </div>
                <div class="card">
                    <div class="p-5 text-center">
                        <h4>Can't find what you are looking for?</h4>
                        <a href="#" class="btn btn-lg btn-success" data-toggle="modal" data-target="#contactModal">Contact Us</a>
                    </div>
                </div>
                
                <!-- <div id="contactModal" class="modal hide fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-dialog-centered modal-lg" style="transform: none;">
                        <div style="position: fixed; top: 1rem; right:1rem">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: white; opacity: 1; font-size: 3rem; font-weight:normal">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-content">
                            <div class="modal-body">
                                <iframe src="https://zaiko.io" width="100%" height="600" frameborder="0"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
 -->

        </div>
        <?php // include('block-contact.php'); ?>


    </div>


</div>    

</div>

<?php include('footer.php'); ?>