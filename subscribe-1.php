<?php include('header.php') ?>

<style>
	header, footer {
		display: none;
	}
</style>

<section class="bg-overlay-zaiko-rev">

<div id="modal-auto" class="modal d-block" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg modal-dialog-centered">
		<div class="modal-content p-5">

			<div class="text-center">
				<h3>Registration for Labyrinth Presale</h3>
				<p>Sign up now and secure your tickets when they go on sale</p>
			</div>

			<div class="progress my-3">
				<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 25%"></div>
			</div>					

			<form class="needs-validation" action="subscribe-2.php" novalidate>

				<label>Your Name</label>

				<div class="form-row">
					<div class="form-group col-md-6">
						<input type="text" class="form-control" placeholder="First name" required>
						<div class="invalid-feedback">
							Please enter your first name
						</div>
					</div>
					<div class="form-group col-md-6">
						<input type="text" class="form-control" placeholder="Last name" required>
						<div class="invalid-feedback">
							Please enter your last name
						</div>
					</div>
				</div>

				<div class="form-group">
					<label>Your Email</label>
					<input type="email" class="form-control" placeholder="Email Address" required>
					<div class="invalid-feedback">
						Please enter correct email address
					</div>
				</div>	

				<div class="alert alert-warning my-3" role="alert">
					ZAIKO often has problems delivering to <strong>@ezweb.ne.jp</strong> and <strong>@naver.com</strong> accounts. Please use a different email address.
				</div>
				
				<div class="text-center">
					<button class="btn btn-primary btn-lg btn-block" type="submit">Next Step</button>
				</div>

				<div class="text-center pt-3">      
					<a href="#" data-dismiss="modal" aria-label="Close">Cancel</a>
				</div>
			</form>
		</div>
	</div>
</div>

</section>
<?php include('footer.php') ?>

<script>
	// Example starter JavaScript for disabling form submissions if there are invalid fields
	(function() {
		'use strict';
		window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
    	form.addEventListener('submit', function(event) {
    		if (form.checkValidity() === false) {
    			event.preventDefault();
    			event.stopPropagation();
    		}
    		form.classList.add('was-validated');
    	}, false);
    });
}, false);
	})();
</script>