<div class="card-order col-lg-4 order-lg-last">
				<div class="card p-3 p-md-4 sticky-top">

					<h4 class="text-center text-md-left mb-3">Order Details</h4>

					<div class="d-flex align-items-center mb-3">
						<img src="https://d38fgd7fmrcuct.cloudfront.net/bw_300/bh_300/pf_1/1_3t4ahlk6y38uj1rv666wt.jpg" alt="" class="img-thumb" style="width:60px; height: 60px">
						<div class="meta ml-3">
							<strong>Squarepusher Japan Tour</strong>
							<p class="small">2020/06/01 - 2020/06/03</p>
						</div>

					</div>


					<div class="" id="prices">

						<div class="ticket-price single d-flex justify-content-between align-items-center py-2">
							<span>						
								<p>早割チケット</p>
								<h5 class="mb-1 text-primary">￥2,500</h5>
							</span>

							<span class="d-flex align-items-center">
								<p class="text-muted">x 1</p>
								<button class="typcn typcn-pencil"></button>

							</span>
						</div>

						<div class="ticket-price single d-flex justify-content-between align-items-center py-2">
							<span>						
								<p>		前売りチケット</p>
								<h5 class="mb-1 text-primary"> ￥3,000</h5>
							</span>

							<select class="form-control form-control-ticket" id="exampleFormControlSelect1">
								<option>1</option>
								<option selected>2</option>
								<option>3</option>
								<option>4</option>
								<option>5</option>
							</select>
						</div>

						<div class="ticket-price single d-flex justify-content-between align-items-center py-2">
							<span>						
								<p>Handling Fee</p>
								<p class="small" data-toggle="tooltip" data-placement="bottom" title="this is extra charge on your ticket price">what is this?</p>
							</span>

							<a class="text-muted small">At Checkout</a>		
						</div>

						<div class="ticket-price d-flex justify-content-between align-items-center pt-3">
							<h4>Total</h4>
							<h4 class="text-primary">￥8,500</h4>
						</div>

					</div>


				</div>
			</div>