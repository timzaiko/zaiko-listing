<?php include('header.php'); ?>

<div class="zaiko-listing">

	<section class="banner banner-narrow bg-zaiko">
		<div class="bg-gradient bg-gradient-top"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>Search Events</h2>
					<?php include('listing-search.php'); ?>
				</div>
			</div>
		</div>

	</section>

	<section class="container">
		<div class="row">
			<div class="col-md-12">
				<h4 class="font-weight-light">XX Results Found For "ABC"</h4>
				<?php include('listing-upcoming.php'); ?>

				<div class="text-center">
					<div class="spinner-border my-4" role="status">
						<span class="sr-only">Loading...</span>
					</div>

					<form action="listing.php">
						<input type="submit" class="btn btn-lg btn-default" value="Return to Home Page">
					</form>
				</div>
			</div>
		</div>
	</section>

</div>


<?php include('footer.php'); ?>

<script>

	// $('.navbar').addClass('position-relative bg-dark');

	var mySwiper = new Swiper ('.swiper', {
		direction: 'horizontal',
		loop: true,
		slidesPerView: 6,
		spaceBetween: 15,
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		breakpoints: {
			640: {
				slidesPerView: 2
			},
			1080: {
				slidesPerView: 4
			}
		}
	})
</script>