		</div>
	</div>

	<footer class="p-5">
		<div class="row align-items-center">
			<div class="col-12 col-md-8">
				<ul class="nav justify-content-center justify-content-md-start">
					<li class="nav-item">
						<a class="nav-link active" href="https://zaiko.dev">Home</a>
					</li>
					<li class="nav-item">
						<a dusk="support_link" class="nav-link active" href="https://zaiko.dev/support">Support</a>
					</li>
					<li class="nav-item">
						<a dusk="business_link" class="nav-link active" href="https://zaiko.dev/business">About Us</a>
					</li>
					<li class="nav-item">
						<a dusk="company_link" class="nav-link active" href="https://zaiko.dev/company">Our Company</a>
					</li>
					<li class="nav-item">
						<a dusk="career_link" class="nav-link active" href="https://zaiko.applytojob.com/">Careers</a>
					</li>
					<li class="nav-item">
						<a dusk="tos_link" class="nav-link active" href="https://zaiko.dev/terms">Terms</a>
					</li>
					<li class="nav-item">
						<a dusk="privacy_policy_link" class="nav-link active" href="https://zaiko.dev/privacy">Privacy Policy</a>
					</li>
				</ul>
			</div>

			<div class="col-12 col-md-4 mt-4 mt-md-0 text-center text-md-right">
				© ZAIKO PTE Ltd. / ZAIKO株式会社 All Rights Reserved.
			</div>
		</div>
	</footer>
</div>

<!-- <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script> -->
<script src="https://code.jquery.com/jquery-3.4.1.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.min.js"></script>
<script src="https://unpkg.com/swiper/js/swiper.min.js"></script>

<script src="https://js.stripe.com/v3/"></script>

<!-- 1. Link Vue Javascript -->
<script src='https://unpkg.com/vue/dist/vue.js'></script>

<!-- 2. Link VCalendar Javascript (Plugin automatically installed) -->
<!-- @next v1 Beta  -->
<script src='https://unpkg.com/v-calendar@next'></script>
<!-- Latest stable (Right now, this is very different from the v1 Beta)-->
<!-- <script src='https://unpkg.com/v-calendar'></script> -->
<!-- Hardcoded version -->
<!-- <script src='https://unpkg.com/v-calendar@1.0.0-beta.14/lib/v-calendar.umd.min.js'></script> -->

<!--3. Create the Vue instance-->
<script>
	new Vue({
		el: '#app',
		data: {
          // Data used by the date picker
          mode: 'single',
          selectedDate: null,
      }
  })
</script>

<script>
	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	})

	// Create a Stripe client.
	var stripe = Stripe('pk_test_TYooMQauvdEDq54NiTphI7jx');

// Create an instance of Elements.
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
	base: {
		color: '#32325d',
		fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
		fontSmoothing: 'antialiased',
		fontSize: '16px',
		'::placeholder': {
			color: '#aab7c4'
		}
	},
	invalid: {
		color: '#fa755a',
		iconColor: '#fa755a'
	}
};

// Create an instance of the card Element.
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');
</script>

</body>
</html>