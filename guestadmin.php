<?php include('guestadmin-header.php'); ?>

<section class="container pt-5">
	<div class="row">
		<div class="col-md-12 d-flex justify-content-between align-items-center mb-3">
			<h3 class="font-weight-normal m-0">Overview</h3>
			<span>
				<!-- <a href="guestadmin-intro.php" class="btn btn-brand">What is Lite?</a> -->
				<a href="#" class="btn btn-brand" data-toggle="modal" data-target="#modal-agree">+ Add your Event</a>

				<!-- <a href="#" class="btn btn-outline-brand" data-toggle="modal" data-target="#modal-youtube"><i class="fas fa-ticket-alt"></i>&nbsp;&nbsp;How to Check-in</a> -->
			</span>
		</div>
	</div>

	<div class="alert alert-success alert-hidden alert-submit fade show">
		You have submitted your event <strong>Fyre Festival</strong> to TimeOut Tokyo. You will be notified when they approve or reject your request.
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<div class="alert alert-success alert-hidden alert-addbank fade show">
		You have successfully added your bank account information.
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<div class="alert alert-success alert-hidden alert-payout fade show">
		You have successfully requested for payout on <strong>Fyre Festival</strong>. Please expect the bank transfer on the specified date.
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>


	<div class="row">
		<div class="col-md-4">
			<div class="card py-4">
				<div class="text-block text-center">
					<h5 class="mb-1 font-weight-normal">Active Tickets Sold</h5>
					<h2 class="text-brand mb-0">1,088</h2>
				</div>
			</div>

		</div>
		<div class="col-md-4">
			<div class="card py-4">
				<div class="text-block text-center">
					<h5 class="mb-1 font-weight-normal">Total Members</h5>
					<h2 class="text-brand mb-0">348</h2>
					<!-- <a href="purchases.php" class="btn btn-outline-brand">View All Members</a> -->
				</div>
			</div>

		</div>
		<div class="col-md-4">
			<div class="card py-4">
				<div class="text-block text-center">
					<h5 class="mb-1 font-weight-normal">Sales Proceeds</h5>
					<h2 class="text-brand mb-0" style="white-space:nowrap;">¥ 2,012,091</h2>
					<!-- <a href="guestadmin-all.php" class="btn btn-outline-brand">View Report</a> -->
				</div>
			</div>
		</div>
	</div>
</section>

<section class="container py-5">
	<div class="row">
		<div class="col-md-12">
			<div class="d-flex justify-content-between align-items-center mb-3">
				<h3 class="font-weight-normal m-0">Your Events</h3>
				<span class="ml-auto">
					Sort by:
					<button type="button" class="dropdown-toggle btn btn-outline-filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Most Recent
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="#">Most Recent</a>
						<a class="dropdown-item" href="#">Most Tickets Sold</a>
					</div>
				</span>
			</div>
			<div class="card table-responsive">
				<table class="table table-hover">
					<thead class="thead-dark">
						<tr>
							<th scope="col">Event Name</th>
							<th scope="col">Event Date</th>							
							<th scope="col">Tickets Sold</th>
							<th scope="col">Status</th>
							<!-- <th scope="col"></th> -->
						</tr>
					</thead>
					<tbody class="">
						<tr>
							<td><a href="event-display.php" target="_blank">Event #1</a></td>
							<td>2020/09/01</td>
							<td><a class="font-weight-bold" href="purchases-1.php">122</a> / 300</td>
							<td><a href="event-approved.php" class="badge badge-success">Active</a></td>

							<!-- <td>
								<a href="#" data-toggle="modal" data-target="#modal-promote" class="btn btn-sm btn-brand">Promote</a>
							</td> -->
						</tr>
						<tr>
							<td><a href="event-display.php" target="_blank">Fyre Festival</a></td>
							<td>2020/08/08</td>
							<td>N/A</td>
							<td><a href="editevent.php" class="badge badge-secondary">Draft</a></td>
							<!-- <td>
								<a href="#" data-toggle="modal" data-target="#modal-promote" class="btn btn-sm btn-brand">Promote</a>
							</td> -->
						</tr>
						<tr>
							<td><a href="event-display.php" target="_blank">Test Event</a></td>
							<td>2020/07/21</td>
							<td>N/A</td>
							<td><a href="event-pending.php" class="badge badge-secondary">Pending</a></td>
							<!-- <td>
								<a href="#" class="btn btn-sm btn-brand disabled" aria-disabled="true">Promote</a>
							</td> -->
						</tr>
						<tr>
							<td><a href="event-display.php" target="_blank">Tech Conf</a></td>
							<td>2020/06/15</td>
							<td>N/A</td>
							<td><a href="event-rejected.php" class="badge badge-danger">Rejected</a></td>
							<!-- <td>
								<a href="#" class="btn btn-sm btn-brand disabled" aria-disabled="true">Promote</a>
							</td> -->
						</tr>
						<tr>
							<td><a href="event-display.php" target="_blank">Test Event #2</a></td>
							<td>2020/06/15</td>
							<td>N/A</td>
							<td><a href="event-rejected.php" class="badge badge-dark">Inactive</a></td>
							<!-- <td>
								<a href="#" class="btn btn-sm btn-brand disabled" aria-disabled="true">Promote</a>
							</td> -->
						</tr>
						<tr>
							<td><a href="event-display.php" target="_blank">ZAIKO Office Party</a></td>
							<td>2020/05/01</td>
							<td><a class="font-weight-bold" href="purchases-1.php">50</a> / 50</td>
							<td><a href="event-over.php" class="badge badge-primary">Completed</a></td>
							<!-- <td><a href="#" class="btn btn-sm btn-brand disabled" aria-disabled="true">Promote</a></td> -->
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<nav aria-label="">
		<ul class="pagination justify-content-end">
			<li class="page-item disabled">
				<a class="page-link" href="#" tabindex="-1">Prev</a>
			</li>
			<li class="page-item active"><a class="page-link" href="#">1</a></li>
			<li class="page-item"><a class="page-link" href="#">2</a></li>
			<li class="page-item"><a class="page-link" href="#">3</a></li>
			<li class="page-item">
				<a class="page-link" href="#">Next</a>
			</li>
		</ul>
	</nav>
</section>

<!-- Modal -->
<div id="modal-youtube" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg modal-dialog-centered">
		<div class="modal-content">
			<div class="embed-responsive embed-responsive-16by9">
				<iframe width="100%" src="https://www.youtube.com/embed/nzjuiyJhY0o" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
		</div>
	</div>
</div>

<section class="container pb-5">
	<div class="row">
		<div class="col-md-12 d-flex justify-content-between align-items-center mb-3">
			<h3 id="gaNotification" class="font-weight-normal m-0">Notifications</h3>
			<span>
				<a href="#" class="btn btn-outline-brand">See All Notifications</a>
			</span>
		</div>
		<div class="col-md-12">
			<div class="card">
				<ul class="notification-list mb-0">
					<li class="p-4 d-flex align-items-center justify-content-between">
						<div>
							<p>Your event <strong>ZAIKO Office Party</strong> is now over. <strong>Please apply for payout.</strong><span class="badge badge-pill badge-danger">NEW</span></p>
							<div class="text-med-grey"><i class="fas fa-clock"></i>&nbsp;2 days ago</div>
						</div>
						<a href="event-over.php" class="btn btn-brand">See Details</a>
					</li>

					<li class="p-4 d-flex align-items-center justify-content-between">
						<div>
							<p>
								TimeOut Tokyo has approved your request to edit event <strong>Fyre Festival</strong>.<span class="badge badge-pill badge-danger">NEW</span>
							</p>
							<div class="text-med-grey"><i class="fas fa-clock"></i>&nbsp;22 hours ago</div>

						</div>
						<a href="event-approved.php" class="btn btn-brand">See Details</a>
					</li>

					<li class="p-4 d-flex align-items-center justify-content-between">
						<div>
							<p>
								Your event <strong>Tech Conf</strong> has been rejected.
							</p>
							<div class="text-med-grey"><i class="fas fa-clock"></i>&nbsp;2 days ago</div>
						</div>
						<a href="event-rejected.php" class="btn btn-brand">See Details</a>
					</li>
					<li class="p-4 d-flex align-items-center justify-content-between">
						<div>
							<p>
								Your event <strong>Fyre Festival</strong> has been approved.
							</p>
							<div class="text-med-grey"><i class="fas fa-clock"></i>&nbsp;2 days ago</div>
						</div>
						<a href="event-approved.php" class="btn btn-brand">See Details</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>


<div id="modal-agree" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg modal-dialog-centered">
		<div class="modal-content p-5 text-center">
			<h3>TimeOut Tokyo Ticket Rate</h3>
			<p>By accessing this admin you will be able to request that your events, tickets and items be listed on this service.</p>
			<h3 class="text-brand">10% + System Fee (4.98%+ 98 JPY)</h3>

			<div class="alert alert-warning">
				Please understand that the listing is at our discretion and even though an event or ticket is submitted we have the right to approve or reject it.
			</div>

			<span>
				<a href="addevent-2.php" class="btn btn-brand btn-lg">I Agree, Continue</a>
				<a href="#" class="btn btn-outline-brand btn-lg" data-dismiss="modal" aria-label="Close">Cancel</a>
			</span>
		</div>
		   <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
        </button>
	</div>
</div>

<?php include('guestadmin-modal-promote.php'); ?>

<?php include('guestadmin-footer.php'); ?>


