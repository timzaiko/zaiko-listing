<?php include('guestadmin-header.php'); ?>

<section class="container-fluid">
	<div class="text-center pt-5">
		<div class="text-info">
			<i class="fas fa-glass-cheers h1"></i>
			<h4 class="fa-step--text">Your Event is completed</h4>
		</div>
	</div>
	<p class="text-center text-dark-grey">Here is the summary of your event.</p>
</section>

<section class="container py-4">
	<div class="row justify-content-center">
		<div class="col-md-6">
			<div class="status-details">
				<li class="d-flex">
					<strong>Event Name</strong>
					<p>ZAIKO Office Party</p>
				</li>
				<li class="d-flex">
					<strong>Time Starts</strong>
					<p>2019-12-31 06:00</p>
				</li>
				<li class="d-flex">
					<strong>Time Ends</strong>
					<p>2020-01-02, 23:00</p>
				</li>
				<li class="d-flex">
					<strong>Tickets Purchased</strong>
					<div>
						<p>20 VIP @ ¥ 50,000</p>
						<p>10 GA @ ¥ 10,000</p>
						<p>10 Standing @ ¥ 5,000</p>
					</div>
				</li>
				<li class="d-flex">
					<strong>Sales</strong>
					<p class="font-weight-bold">¥ 34,000</p>
				</li>
				<li class="d-flex">
					<strong>Fees</strong>
					<p class="font-weight-bold">¥ 5,000&nbsp;<i class="fas fa-question-circle text-secondary" data-toggle="tooltip" data-placement="right" title="Fees = 10.98% of total sales"></i></p>
				</li>
				<li class="d-flex">
					<strong>Payout</strong>
					<p class="text-brand font-weight-bold">¥ 29,000</p>
				</li>
				<li class="d-flex">
					<strong>Payout Status</strong>
					<p><a href="payout.php" class="btn btn-brand">Apply for Payout</a> / Processing / Paid</p>
				</li>
			</div>
			<div class="alert alert-warning my-4">
				When the event has taken place and tickets purchased, <strong>you are required to submit your bank information for payouts.</strong> Please submit this information if you have not already done so.
			</div>
			<div class="text-center">
				<a href="guestadmin.php" class="btn btn-brand btn-lg">Return to Home Page</a>
			</div>
		</div>
	</div>		
</section>

<?php include('guestadmin-footer.php'); ?>

