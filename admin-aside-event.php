<aside class="admin-aside collapse show" id="navbar-sidebar">

	<ul class="px-0 py-lg-5 py-3 mb-0">
		<li data-toggle="collapse" data-target="#admin-submenu1" aria-expanded="false" aria-controls="admin-submenu1" class="collapsed">
			<span class="px-3 d-flex justify-content-between">
				<div class="">
					<span class="typcn pr-2 text-muted typcn-calendar-outline"></span>Event Details
				</div>
				<span class="typcn typcn-chevron-right"></span>
			</span>
			<li id="admin-submenu1" class="collapse">
				<a href="" target="" class="sub-menu">Information</a>
				<a href="" target="" class="sub-menu">Artists & Hosts</a>
				<a href="" target="" class="sub-menu">Media</a>
			</li>
		</li>

		<li data-toggle="collapse" data-target="#admin-submenu2" aria-expanded="false" aria-controls="admin-submenu2" class="collapsed">
			<span class="px-3 d-flex justify-content-between">
				<div class="">
					<span class="typcn pr-2 text-muted typcn-ticket"></span>All Tickets
				</div>
				<span class="typcn typcn-chevron-right"></span>
			</span>

			<li id="admin-submenu2" class="collapse">
				<a href="admin-event-settings.php" target="" class="sub-menu">Settings</a>


				<a href="" target="" class="sub-menu">Members</a>


				<a href="" target="" class="sub-menu">Add Ticket</a>
			</li>


		</li>

		<li class="">
			<a href="#" class="pl-3"><span class="typcn pr-2 text-muted typcn-chart-line-outline"></span>Analytics</a>

		</li>

		<li class="">
			<a href="#" class="pl-3"><span class="typcn pr-2 text-muted typcn-message"></span>Communication</a>
		</li>

		<li data-toggle="collapse" data-target="#admin-submenu3" aria-expanded="false" aria-controls="admin-submenu3" class="collapsed">
			<span class="px-3 d-flex justify-content-between">
				<div class="">
					<span class="typcn pr-2 text-muted typcn-credit-card"></span>Payments
				</div>
				<span class="typcn typcn-chevron-right"></span>
			</span>
			<li id="admin-submenu3" class="collapse">
				<a href="" target="" class="sub-menu">Payment Information</a>
			</li>
		</li>

		<li class="">
			<a href="#" class="pl-3"><span class="typcn pr-2 text-muted typcn-document-text"></span>Activity Log</a>

		</li>



	</ul>
</aside>