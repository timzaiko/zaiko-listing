<?php include('header-admin-profile.php'); ?>

		<?php include('admin-aside-home.php') ?>

		<main class="admin-main pb-5">
			<?php include('admin-breadcrumbs.php') ?>

			<section class="container-fluid px-5">

				<div class="progress-bar d-xs-none py-5">
					<ol class="clearfix">
						<li class="active">
							<div class="progress-status">
								<i class="fas fa-wrench"></i>
							</div>
							<h5 class="fa-step--text">1. Basic Information</h5>
						</li>
						<li class="">
							<div class="progress-status">
								<i class="fas fa-ticket-alt fa-step"></i>					
							</div>
							<h5 class="fa-step--text">2. Event Details</h5>
						</li>
					</ol>
				</div>

				<div class="row">
					<div class="col-md-12 mx-auto">
						<h3 class="font-weight-normal mb-2">Create Your Event</h3>
						<p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
						<form class="needs-validation" action="admin-create-event-finish.php" novalidate>

							<div class="card">
								<div class="card-body p-4">

									<?php include('guestadmin/add-1.php'); ?>
								</div>
								<div class="card-footer text-right mt-4">
									<button class="btn btn-default " type="submit">Next Step</button>
									<a href="addevent-1.php"><button class="btn btn-secondary ">Cancel</button></a>
								</div>

								
							</div>

							
						</form>
					</div>
				</div>
			</section>

		</main>


<!-- modal -->
<div id="modal-addticket" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content queue p-5 text-center">
			<h4>Please select your ticket type</h4>
			<a href="register-1.php" class="btn btn-primary btn-lg btn-block">Paid Ticket</a>
			<a href="register-1.php" class="btn btn-default btn-lg btn-block">Free Ticket*</a>
			<div class="mt-2">*While these tickets are for free events, organizers will be charged 50¥ for each ticket “purchased”.</div>
		</div>


	</div>
</div>

<?php include('footer.php'); ?>


