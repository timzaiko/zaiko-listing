<?php include('header-admin.php'); ?>

<div class="container-fluid flex-grow-1">
	<div class="row flex-lg-nowrap flex-grow-1">


		<main class="admin-main">
			<section class="container-fluid pt-5 px-5">
				<div class="row">
					<div class="col-md-12">
						<h3 class="font-weight-normal">Your Tickets</h3>
						<p class="text-muted">All purchased/transferred tickets
						</p>
					</div>
				</div>
			</section>


			<section class="container-fluid px-5">
				<div class="row">
					<div class="card-ticket col-lg-3 col-md-4 col-sm-6">
						<div class="card top mb-0 ">
							<div class="d-flex flex-column">
								<div class="image" style="background: url('https://d38fgd7fmrcuct.cloudfront.net/bw_400/bh_400/pf_1/cc_300.0.1000.1000/1_3snng5142owte2npxj0l9.jpg') center / cover">
									<div class="p-3 text-white card-ticket-name">
										<h5 class="mb-1">Event Name</h5>
										<p class="mb-0">Ticket Name</p>
									</div>
								</div>
								<div class="p-3">
									<h5 class="mb-1">2020-03-04</h5>
									<p class="mb-0">WOMB, Shibuya</p>
								</div>
							</div>
						</div>
						<div class="card-ticket__rip"></div>
						<div class="card bottom d-flex flex-row justify-content-between align-items-center px-3 pb-3 pt-1">
							<div class="card-ticket__barcode"><strong>1 PERSON</strong></div>
							<a href="#" class="btn btn-default">
								View Ticket
							</a>

						</div>
					</div>

					<div class="card-ticket col-lg-3 col-md-4 col-sm-6">
						<div class="card top mb-0 ">
							<div class="d-flex flex-column">
								<div class="image" style="background: url('https://d38fgd7fmrcuct.cloudfront.net/bw_400/bh_400/pf_1/cc_0.0.1200.1200/1_3sarfxsio64xq66nreagb.jpg') center / cover">
									<div class="p-3 text-white card-ticket-name">
										<h5 class="mb-1">Event Name</h5>
										<p class="mb-0">Ticket Name</p>
									</div>
								</div>
								<div class="p-3">
									<h5 class="mb-1">2020-03-04</h5>
									<p class="mb-0">WOMB, Shibuya</p>
								</div>
							</div>
						</div>
						<div class="card-ticket__rip"></div>
						<div class="card bottom d-flex flex-row justify-content-between align-items-center px-3 pb-3 pt-1">
							<div class="card-ticket__barcode"><strong>1 PERSON</strong></div>
							<a href="#" class="btn btn-default">
								View Ticket
							</a>

						</div>
					</div>

					<div class="card-ticket col-lg-3 col-md-4 col-sm-6">
						<div class="card top mb-0 ">
							<div class="d-flex flex-column">
								<div class="image" style="background: url('https://d38fgd7fmrcuct.cloudfront.net/bw_400/bh_400/pf_1/1_3qqxqoa8n44yhp5hwb5s3.jpg') center / cover">
									<div class="p-3 text-white card-ticket-name">
										<h5 class="mb-1">Event Name</h5>
										<p class="mb-0">Ticket Name</p>
									</div>
								</div>
								<div class="p-3">
									<h5 class="mb-1">2020-03-04</h5>
									<p class="mb-0">WOMB, Shibuya</p>
								</div>
							</div>
						</div>
						<div class="card-ticket__rip"></div>
						<div class="card bottom d-flex flex-row justify-content-between align-items-center px-3 pb-3 pt-1">
							<div class="card-ticket__barcode"><strong>1 PERSON</strong></div>
							<a href="#" class="btn btn-default">
								View Ticket
							</a>

						</div>
					</div>

					<div class="card-ticket col-lg-3 col-md-4 col-sm-6">
						<div class="card top mb-0 ">
							<div class="d-flex flex-column">
								<div class="image" style="background: url('https://d38fgd7fmrcuct.cloudfront.net/bw_400/bh_400/pf_1/1_3se741295wmtyyb1vlf3k.jpg') center / cover">
									<div class="p-3 text-white card-ticket-name">
										<h5 class="mb-1">Event Name</h5>
										<p class="mb-0">Ticket Name</p>
									</div>
								</div>
								<div class="p-3">
									<h5 class="mb-1">2020-03-04</h5>
									<p class="mb-0">WOMB, Shibuya</p>
								</div>
							</div>
						</div>
						<div class="card-ticket__rip"></div>
						<div class="card bottom d-flex flex-row justify-content-between align-items-center px-3 pb-3 pt-1">
							<div class="card-ticket__barcode"><strong>1 PERSON</strong></div>
							<a href="#" class="btn btn-default">
								View Ticket
							</a>

						</div>
					</div>





				</div>
			</section>


		</main>
	</div>
</div>

<?php include('footer.php'); ?>


