<?php include('header.php'); ?>

<section class="banner bg-overlay h-100" style="background: url('/img/bg-business.jpg') no-repeat center center / cover">
	<div class="container">
		<div class="row text-block banner-narrow">
			<div class="col-md-10 col-sm-12 mx-auto p-10rem py-0 text-center">
				<h2>티켓 판매를 위한 모든 것을 간소화 하였습니다</h2>
				<p class="lead">티켓 판매를 진행하기 위한 과정들을 최대한으로 줄였습니다. ZAIKO의 플랫폼과 함께 티켓 판매의 편리함을 경험해 보세요</p>
				<a href="login.php" class="btn btn-lg btn-default my-4">지금 바로 등록하기</a>
			</div>
		</div>
	</div>
</div>
</section>

<section class="container-fluid">
	<div class="row">
		<div class="col-md-6 col-sm-12 d-flex align-items-center">
			<div class="text-block p-10rem pl-md-0">
				<h5 class="zaiko-purple mb-2">화이트 라벨 티켓</h5>
				<h2>당신의 티켓, 당신의 브랜드</h2>
				<p class="lead">귀사가 원하는 스타일로 이벤트를  작성해주세요. 콘서트, 콘퍼런스, 팬미팅 등 어떤 이벤트도 가능합니다. 자사 사이트에서 직접 티켓 판매,  Microsite를 통해 귀사만의 티켓 구매 사이트를 만들어 보세요.</p>
				<div class="list-check d-flex flex-wrap mt-4">
					<div class="list-item col-md-6 p-0">
						<img src="/img/ico-check.svg" alt="" width="16" height="16">
						<span>Free or Paid Tickets Available</span>
					</div>
					<div class="list-item col-md-6 p-0">
						<img src="/img/ico-check.svg" alt="" width="16" height="16">
						<span>추천 티켓 작성 가능</span>
					</div>
					<div class="list-item col-md-6 p-0">
						<img src="/img/ico-check.svg" alt="" width="16" height="16">
						<span>VIP, Pre-sale 등 어떤 티켓 종류도 작성 가능</span>
					</div>
					<div class="list-item col-md-6 p-0">
						<img src="/img/ico-check.svg" alt="" width="16" height="16">
						<span>100% 자체 작성</span>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6 order-md-first d-flex align-items-end pt-md-5">
			<img class="img-fluid" src="/img/business-1.png" alt="">
		</div>
	</div>
</section>

<section class="container-fluid bg-light-grey">
	<div class="row">
		<div class="col-md-6 d-flex align-items-center">
			<div class="text-block p-10rem pr-md-0">
				<h5 class="zaiko-purple mb-2">Direct-To-Fan</h5>
				<h2>팬들을 사로잡다</h2>
				<p class="lead">귀사의 자산은 귀사의 팔로워와 팬들입니다.</p>
				
				<div class="list-check d-flex flex-wrap mt-4">
					<div class="list-item col-md-6 p-0">
						<img src="/img/ico-check.svg" alt="" width="16" height="16">
						<span>100% 전자 티켓</span>
					</div>
					<div class="list-item col-md-6 p-0">
						<img src="/img/ico-check.svg" alt="" width="16" height="16">
						<span>안전보안  체크인 시스템</span>
					</div>
					<div class="list-item col-md-6 p-0">
						<img src="/img/ico-check.svg" alt="" width="16" height="16">
						<span>PayPal 결제 가능</span>
					</div>
					<div class="list-item col-md-6 p-0">
						<img src="/img/ico-check.svg" alt="" width="16" height="16">
						<span>Stripe 과 협력중</span>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-6 d-flex align-items-end pt-md-5">
			<img class="img-fluid" src="/img/business-2.png" alt="">
		</div>

	</div>
</section>

<section class="container-fluid">
	<div class="row">
		<div class="col-md-6 d-flex align-items-center">
			<div class="text-block p-10rem pl-md-0">
				<h5 class="zaiko-purple mb-2">분석 대시보드</h5>
				<h2>당신의 서비스를 데이터로 확인하세요</h2>
				<p class="lead">분석을 하기 전 숫자는 많은 의미로 다가오지 않습니다. 어떤 고객들이 티켓을 구입했는지, 어떤 장르의 음악을 좋아지는지 저희는 모두 분석합니다. 빅데이터의 힘을 이용하여 다음 이벤트 계획을 좀 더 스마트하게 계획하세요.</p>
				<div class="list-check d-flex flex-wrap mt-4">
					<div class="list-item col-md-6 p-0">
						<img src="/img/ico-check.svg" alt="" width="16" height="16">
						<span>고객 데이터 보안</span>
					</div>
					<div class="list-item col-md-6 p-0">
						<img src="/img/ico-check.svg" alt="" width="16" height="16">
						<span>데이터 비공유</span>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6 order-md-first d-flex align-items-end pl-0 pt-md-5">
			<img class="img-fluid" src="/img/business-3.png" alt="">
		</div>
	</div>
</section>

<section class="call-to-action bg-overlay" style="background: url('/img/bg-about.jpg') no-repeat center center / cover">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-8 offset-md-2">
				<h2 class="text-center">ZAIKO는 티켓 판매의 그 이상을 구현합니다</h2>
				<p class="lead my-5 text-center">ZAIKO의 독점 판매 채널을 통해 미디어, 레코드 및 아티스트 관리 회사를 통해 이벤트를 홍보하세요</p>
			</div>

			<ul class="nav nav-tabs mx-auto" id="myTab" role="tablist">
				<li class="nav-item mx-3">
					<a class="nav-link btn btn-xl active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">미디아  파트너입니다</a>
				</li>
				<li class="nav-item mx-3">
					<a class="nav-link btn btn-xl" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">이벤트 주최자입니다</a>
				</li>
			</ul>

			<div class="col-md-12 tab-content mx-auto" id="myTabContent">
				<div class="tab-pane fade show active pt-5" id="home" role="tabpanel" aria-labelledby="home-tab">
					<div class="d-flex flex-wrap justify-content-between">
						<img class="img-fluid img-clients" src="img/partners/iflyer.png" alt="">
						<img class="img-fluid img-clients" src="img/partners/metropolis.png" alt="">
						<img class="img-fluid img-clients" src="img/partners/tokyogigguide.png" alt="">
						<img class="img-fluid img-clients" src="img/partners/clubberia.png" alt="">
						<img class="img-fluid img-clients" src="img/partners/fnmnl.png" alt="">
						<img class="img-fluid img-clients" src="img/partners/tokyocheap.png" alt="">
						<img class="img-fluid img-clients" src="img/partners/timeout.png" alt="">
						<img class="img-fluid img-clients" src="img/partners/spincoaster.png" alt="">
					</div>
					<p class="lead text-center pt-5">귀하의 브랜드 잠재력을 최대화하여 티켓 판매를 통한 브랜드 영향력과 수익을 높일 수 있습니다.</p>
				</div>
				<div class="tab-pane fade pt-5" id="profile" role="tabpanel" aria-labelledby="profile-tab">
					<div class="d-flex flex-wrap justify-content-between">
						<img class="img-fluid img-clients" src="img/partners/beatink.png" alt="">
						<img class="img-fluid img-clients" src="img/partners/fanicon.png" alt="">
						<img class="img-fluid img-clients" src="img/partners/hearst.png" alt="">
						<img class="img-fluid img-clients" src="img/partners/indieasia.png" alt="">
						<img class="img-fluid img-clients" src="img/partners/mutek.png" alt="">
						<img class="img-fluid img-clients" src="img/partners/soundmuseumvision.png" alt="">
						<img class="img-fluid img-clients" src="img/partners/www.png" alt="">
					</div>
					<p class="lead text-center pt-5">미디어의 영향력을 활용하여 이벤트를 홍보하고 티켓 판매를 지원하세요. 티켓의 수수료는 귀사가 직접 설정할 수 있습니다.</p>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Pricing -->
<section class="bg-light-grey">
	<div class="container p-5rem">
		<div class="row">
			<div class="col-md-12 text-center">
				<h2>가격 책정</h2>
				<p class="lead mb-5">숨기는 가격 없이 당신의 가격을 직접 정하세요.</p>
				<div class="card-deck">
					<div class="card p-5 m-0 hover">
						<h3>ZAIKO 전자 티켓</h3>
						<h1 class="card-title pricing-card-title d-inline-block">0%<sup>*</sup></h1>
						<p>*화이트 라벨 티켓의 최저 비율은 2.89%+고객 부담 비용입니다.</p>
						<div>
							<a href="login.php" class="btn btn-lg btn-default mt-3">지금 바로 등록하기</a>
						</div>
					</div>
					<div class="card p-5 m-0 nothover">
						<h4 class="font-weight-normal">타 티켓 회사</h4>
						<h1 class="card-title pricing-card-title"></h1>
						<li>티켓 발행 비용*</li>
						<li>티켓 발송 비용*</li>
						<li>특별 티켓 관련 추가 비용 발생 가능</li>
						<li>다언어 서비스 지원 비용**</li>
						<p class="pt-4">*ZAIKO에서는 모든 티켓이 전자 티켓입니다.</p>
						<p>**ZAIKO에서는 실시간으로 일본어, 영어, 한국어, 중국어를 지원하고 있으며, 이에 따른 서비스 비용이 추가 발생하지 않습니다.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="banner call-to-action bg-overlay-zaiko text-center">
	<h3>저희 ZAIKO에 대하여 좀 더 알고 싶으신가요?</h3>
	<a href="contact.php" class="btn btn-xl btn-outline-light">문의하기</a>
</section>

<?php include('footer.php'); ?>