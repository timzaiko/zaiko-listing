<div class="listing-category position-relative d-flex flex-row justify-space-between flex-wrap align-items-center">
	<div class="event">
		<figure>
			<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQU8j5oHfi40YvK7dhT_1jDlbKYN2Li4RLSYhgsPu_cIEMG31VeZg&s" alt="">
			<a href="listing-category-single.php">
				<div class="text-block">
					<h3>Music</h3>
				</div>
			</a>
		</figure>
	</div>
	<div class="event">
		<figure>
			<img src="https://ichef.bbci.co.uk/news/660/cpsprodpb/1038A/production/_100924466_shambala.jpg.jpg" alt="">
			<a href="listing-category-single.php">
				<div class="text-block">
					<h3>Festivals</h3>
				</div>
			</a>
		</figure>
	</div>
	<div class="event">
		<figure>
			<img src="https://www.eturbonews.com/wp-content/uploads/2018/12/JapanTourism.jpg" alt="">
			<a href="listing-category-single.php">
				<div class="text-block">
					<h3>Tourism</h3>
				</div>
			</a>
		</figure>
	</div>
	<div class="event">
		<figure>
			<img src="http://m-idols.jp/wp-content/uploads/2019/05/s_5%E4%BA%BA%E9%9B%86%E5%90%88.jpg" alt="">
			<a href="listing-category-single.php">
				<div class="text-block">
					<h3>Idol</h3>
				</div>
			</a>
		</figure>

	</div>
	<div class="event">
		<figure>
			<img src="https://o.aolcdn.com/images/dims?quality=85&image_uri=https%3A%2F%2Fs.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2Ff10fcd5c0630ecb553f69ead07e7c318%2F206538633%2Fesports-ed.jpg&client=amp-blogside-v2&signature=d126ae108524a7756f7d4f5f9c3a93004a800422" alt="">
			<a href="listing-category-single.php">
				<div class="text-block">
					<h3>eSports</h3>
				</div>
			</a>
		</figure>
	</div>
</div>

<div class="col-md-12 text-center mt-3">
	Or browse others:
	<button class=" text-dark-grey">Conference</button>
	<button class=" text-dark-grey">Meet-up</button>
	<button class=" text-dark-grey">Market</button>
	<button class=" text-dark-grey">Theatre</button>
	<button class=" text-dark-grey">Sports</button>
</div>