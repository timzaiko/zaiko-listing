<?php include('header.php'); ?>

<section class="banner bg-overlay" style="background: url('/img/bg-business.jpg') no-repeat center center / cover">
    <div class="text-block banner-narrow text-center">
        <h2>会社概要</h2>
        <p>About ZAIKO</p>
    </div>
</section>

<section class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 p-5">
            <p>ZAIKOはイベント主催者及びデジタルメディア企業向けのホワイトレーベル型チケット販売プラットフォー ムです。チケットの在庫をシステム上で一元管理することで、複数の販売先との取引を実現する製品開発をしている会社です。フェスをはじめ、クラブイベント、旅行パック、展示会、スボーツなど、幅広いイベントにおけるチケット販売の新型プラットフォームです。</p>
            <p>
            ZAIKOは会社の急激な成長に伴い、ご自身のキャリア面での成長だけでなく、お客様やクライアントの満足度に直接繋がるプロセスやプロダクトの開発に携わって共に成長できる会社です。</p>
            <p>
            情熱的で細部まで気配りができ、情報の整理に長け、かつグローバルで多様的な風通しの良いスタートアップ企業でのキャリアにご興味ございませんでしょうか。</p>
            <p>
            エンターテイメント業界、イベント企画、チケット販売やカスタマーサービスにご興味のある方はご応募をお待ちしております。</p>
        </div>
    </div>

    <div class="row">


    </div>
</section>

<section class="container-fluid p-0">
    <div class="row justify-content-center">
        <div class="col-md-6 p-5">
            <h3 class="text-center">日本 / Japan</h3>
            <table class="table text-center mt-5 d-lg-table d-md-table">
                <tbody>
                    <tr>
                        <th scope="row">商号</th>
                        <td>ZAIKO株式会社</td>

                    </tr>
                    <tr>
                        <th scope="row">本社所在地</th>
                        <td>150-0002 東京都港区西麻布2-13-6 K’S 西麻布 5F</td>

                    </tr>
                    <tr>
                        <th scope="row">電話番号</th>
                        <td>03-4577-5677</td>

                    </tr>
                    <tr>
                        <th scope="row">事業内容</th>
                        <td>*****</td>

                    </tr>
                    <tr>
                        <th scope="row">設立</th>
                        <td>2019年1月11日</td>

                    </tr>
                    <tr>
                        <th scope="row">代表者</th>
                        <td>代表取締役社長CEO　Malek Nasser </td>

                    </tr>
                    <tr>
                        <th scope="row">資本金</th>
                        <td>1,000,000円</td>

                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-12">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3241.6715387604245!2d139.72075211525825!3d35.660463380199175!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60188b7abe56e143%3A0x85eb4c2a4fdadbe7!2s2-ch%C5%8Dme-13-6+Nishiazabu%2C+Minato+City%2C+T%C5%8Dky%C5%8D-to+106-0031!5e0!3m2!1sen!2sjp!4v1553493784239" width="100%" height="450" frameborder="0" style="border:0"></iframe>
        </div>
        <div class="col-md-6 p-5">
            <h3 class="text-center">シンガポール / Singapore</h3>
            <table class="table text-center mt-5 d-lg-table d-md-table">
                <tbody>
                    <tr>
                        <th scope="row">商号</th>
                        <td>Zaiko Pte Ltd.</td>

                    </tr>
                    <tr>
                        <th scope="row">本社所在地</th>
                        <td>16 Raffles Quay #33-03 Hong Leong Building 048581 Singapore</td>

                    </tr>

                    <tr>
                        <th scope="row">設立</th>
                        <td>2019年1月11日</td>

                    </tr>
                    <tr>
                        <th scope="row">代表者</th>
                        <td>Malek Nasser, CEO</td>

                    </tr>

                </tbody>
            </table>
        </div>
    </div>
</section>

<section class="container">
    <div class="row justify-content-center">
      <div class="col-12 col-md-8 col-lg-5 text-center p-5">
        <img src="/v1/img/ZAIKO-logo-text.svg" height="100" alt="image">
        <p class="mt-4"><a href="https://zaiko.applytojob.com/" class="btn btn-primary btn-lg" target="_blank">採用情報</a></p>
    </div>
</div>
</section>

<?php include('footer.php'); ?>