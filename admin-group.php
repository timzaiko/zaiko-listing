<?php include('header-admin-beatink.php'); ?>

<div class="container-fluid">
	<div class="row flex-lg-nowrap">
		<?php include('admin-aside-group.php') ?>

		<main class="admin-main">
			<?php include('admin-breadcrumbs.php') ?>

			<section class="container-fluid pt-5 px-5">
				<div class="row flex-column mb-3">
					<h3 class="font-weight-normal mb-3">Members Connect</h3>
					<p class="text-muted">Connect with your members, get insights, communicate and lots more.</p>
				</div>
			</section>

			<section class="container-fluid px-5">
				<div class="row card">
					<div class="card-header bg-dark text-white">
						<h5 class="font-weight-normal mb-1">Group Name: VIP</h5>
					</div>
					<div class="p-4">
						<p class="h4">Edit Group</p>
					</div>

					<div class="col-md-9 mx-auto">

						<div class="d-md-flex flex-row align-items-center mb-3">
							<div class="col-md-3 text-md-right">
								<label><strong>Name</strong></label>

							</div>
							<div class="col-md-9 col-sm-12"> 
								<input id="" type="text" value="VIP" required="required" class="form-control">
							</div>
						</div>

						<div class="d-md-flex flex-row mb-3">
							<div class="col-md-3 text-md-right mt-3">
								<label><strong>Description</strong></label>

							</div>
							<div class="col-md-9 col-sm-12"> 
								<textarea id=""  required="required" rows="10" class="form-control">this is the VIP service, they will get frist round of tickets for the best events
								</textarea>
							</div>
						</div>

					</div>

				</div>
			</section>

			<section class="container-fluid px-5">
				<div class="row card">
					<div class="p-4">
						<p class="h4">Group Type</p>
					</div>

					<div class="row">
						<div class="col-md-9 mx-auto">
							<div class="alert alert-warning">
								You can not edit the following if there are members in this group.
							</div>



							<div class="form-group px-4">
								<div class="radio ">
									<input type="radio" name="radioGroupType" id="radio1" value="option1" checked="">
									<label for="radio1">
										<strong>Open</strong>
										<div class="text-muted font-italic">Any Members blah blah</div>
									</label>
								</div>
								<div class="radio ">
									<input type="radio" name="radioGroupType" id="radio2" value="option2">
									<label for="radio2">
										<strong>Closed</strong>
										<div class="text-muted font-italic">
											Only admins can add blah blah
										</div>
									</label>
								</div>
								<div class="radio ">
									<input type="radio" name="radioGroupType" id="radio3" value="option3">
									<label for="radio3">
										<strong>Subscription</strong>
										<div class="text-muted font-italic">
											This group is part blah blah blah
										</div>
									</label>
								</div>
							</div>
						</div>

					</div>
				</div>
			</section>


			<section class="container-fluid px-5">
				<div class="row card">
					<div class="p-4">
						<p class="h4">Publish Option</p>
					</div>

					<div class="row">
						<div class="col-md-9 mx-auto">
							<div class="form-group px-4">
								<div class="radio ">
									<input type="radio" name="radioPublishOption" id="radioPublishOption1" value="radioPublishOption1" checked="">
									<label for="radioPublishOption1">
										<strong>Draft</strong>
									</label>
								</div>
								<div class="radio">
									<input type="radio" name="radioPublishOption" id="radioPublishOption2" value="radioPublishOption2">
									<label for="radioPublishOption2">
										<strong>Published</strong>

									</label>
								</div>
								<div class="radio ">
									<input type="radio" name="radioPublishOption" id="radioPublishOption3" value="radioPublishOption3">
									<label for="radioPublishOption3">
										<strong>Paused</strong>

									</label>
								</div>

								<div class="radio ">
									<input type="radio" name="radioPublishOption" id="radioPublishOption4" value="radioPublishOption4">
									<label for="radioPublishOption4">
										<strong>Removed</strong>

									</label>

								</div>
							</div>
						</div>
					</div>

					<div class="text-center card-footer">
						<input type="submit" class="btn btn-default" value="Update">
						<input type="submit" class="btn btn-secondary" value="Cancel">
					</div>

				</div>
			</section>

		</main>
	</div>
</div>

<div id="modal-deactivate" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered">
		<div class="modal-content p-5 text-center">
			<h4>Are you sure you want to deactivate your account?</h4>
			<p >You can reactivate it any time by contacting our support.</p>
			<span><button class="btn btn-danger btn-lg">
				Deactivate
			</button> 
			<button class="btn btn-secondary btn-lg">
				Cancel
			</button></span>
		</div>
	</div>
</div>

<?php include('footer.php'); ?>


