<?php include('guestadmin-header.php'); ?>

<section class="container-fluid">
	<div class="progress-bar d-xs-none pt-5">
		<ol class="clearfix">
			<li class="through">
				<div class="progress-status">
					<i class="fas fa-wrench"></i>
				</div>
				<h5 class="fa-step--text">1. Event Information</h5>
			</li>
			<li class="active">
				<div class="progress-status">
					<i class="fas fa-ticket-alt fa-step"></i>					
				</div>
				<h5 class="fa-step--text">2. Add Tickets</h5>
			</li>
			<li class="">
				<div class="progress-status">
					<i class="fas fa-check fa-step"></i>					
				</div>
				<h5 class="fa-step--text">3. Confirmation</h5>
			</li>
		</ol>
	</div>
</section>

	<section class="container py-5">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<h4>Add Your Tickets</h4>
				<form class="needs-validation" action="addevent-4.php" novalidate>
					<?php include('guestadmin/add-2.php'); ?>
					<div class="text-center mt-4">
						<button class="btn btn-brand btn-lg" type="submit">Next Step</button>
						<button class="btn btn-secondary btn-lg">Cancel</button>
					</div>

				</form>
			</div>
		</div>
	</section>

<?php include('guestadmin-footer.php'); ?>


	<script>
		$('.add-one').click(function(){
			$('.ticket-single').first().clone().appendTo('.ticket-info').show();
			attach_delete();
		});

		function attach_delete(){
			$('.delete-one').off();
			$('.delete-one').click(function(){
				$(this).closest('.ticket-single').remove();
			});
		}
		$(function () {
			$('[data-toggle="tooltip"]').tooltip();
		})
	</script>