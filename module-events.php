<section class="container-fluid px-5">
	<div class="row">
		<div class="col-12 table-responsive card p-0">

			<table class="table table-hover table-striped table-md text-nowrap">
				<thead class="">
					<tr class="text-nowrap">
						<th colspan="2">Name</th>
						<th>Event Date</th>
						<th>Sold</th>
						<th>Revenue</th>
						<th>Status</th>
						<th>Action</th>

					</tr>
				</thead>
				<tbody>
					<tr>
						<td width="50">
							<img src="https://d38fgd7fmrcuct.cloudfront.net/bw_48/bh_48/pf_1/1_3sox63vrwc0wa9icf36wi.png" style="height:60px; width: 60px;">
						</td>
						<td class="text-nowrap">
							<div class=" ">
								<a href="admin-event-single.php"><h6 class="mb-1 ">OF MONSTERS AND MEN JAPAN TOUR 2020</h6></a>

								<p class="mb-1 small">@マイナビBLITZ赤坂 / TOKYO</p>
								<span class="badge badge-pill pill-lg pill-warning">Secret</span>	
							</div>
						</td>
						<td class="small text-nowrap">Jul 11th 12:00PM ~<br>Apr 30th 11:59PM (JST)</td>
						<td>25</td>
						<td>￥169,000</td>
						<td><span class="badge badge-info">
							Sales Over
						</span></td>
						<td class="text-nowrap">
							<a href="admin-edit-event.php" class="typcn px-1 typcn-edit" data-target="#modal-ccdelete" data-toggle="tooltip" data-placement="top" title="Edit Event Information"></a>
							<a href="admin-event-settings.php" class="typcn px-1 typcn-cog-outline" data-toggle="tooltip" data-placement="top" title="Edit Settings"></a>
							<a href="listing-display.php" class="typcn px-1 typcn-eye-outline" data-toggle="tooltip" data-placement="top" title="View Event Page"></a>


						</td>
					</tr>

					<tr>
						<td width="50">
							<img src="https://d38fgd7fmrcuct.cloudfront.net/bw_48/bh_48/pf_1/1_3sox63vrwc0wa9icf36wi.png" style="height:60px; width: 60px;">
						</td>
						<td class="text-nowrap">
							<div class=" ">
								<h6 class="mb-1 ">OF MONSTERS AND MEN JAPAN TOUR 2020</h6>

								<p class="mb-1 small">@マイナビBLITZ赤坂 / TOKYO</p>
								<span class="badge badge-pill pill-lg pill-warning">Secret</span>	
							</div>
						</td>

						<td class="small text-nowrap">Jul 11th 12:00PM ~<br>Apr 30th 11:59PM (JST)</td>
						<td>25</td>
						<td>￥169,000</td>
						<td><span class="badge badge-success">
							On Sale
						</span></td>
						<td class="text-nowrap">
							<a href="admin-edit-event.php" class="typcn px-1 typcn-edit" data-target="#modal-ccdelete" data-toggle="tooltip" data-placement="top" title="Edit Event Information"></a>
							<a href="admin-event-settings.php" class="typcn px-1 typcn-cog-outline"></a>
							<a href="listing-display.php" class="typcn px-1 typcn-eye-outline"></a>


						</td>
					</tr>

					<tr>
						<td width="50">
							<img src="https://d38fgd7fmrcuct.cloudfront.net/bw_48/bh_48/pf_1/1_3sox63vrwc0wa9icf36wi.png" style="height:60px; width: 60px;">
						</td>
						<td>
							<div class=" ">
								<h6 class="mb-1 ">OF MONSTERS AND MEN JAPAN TOUR 2020</h6>

								<p class="mb-1 small">@マイナビBLITZ赤坂 / TOKYO</p>
							</div>
						</td>
						<td class="small text-nowrap">Mar 20th 12:59AM ~<br>Apr 01st 03:59PM (JST)</td>
						<td>0</td>
						<td>￥169,000</td>
						<td><span class="badge badge-danger">
							Sold Out
						</span></td>
						<td class="text-nowrap">
							<a href="admin-edit-event.php" class="typcn px-1 typcn-edit" data-target="#modal-ccdelete" data-toggle="tooltip" data-placement="top" title="Edit Event Information"></a>
							<a href="admin-event-settings.php" class="typcn px-1 typcn-cog-outline"></a>
							<a href="listing-display.php" class="typcn px-1 typcn-eye-outline"></a>


						</td>
					</tr>

					<tr>
						<td width="50">
							<img src="https://d38fgd7fmrcuct.cloudfront.net/bw_48/bh_48/pf_1/1_3sox63vrwc0wa9icf36wi.png" style="height:60px; width: 60px;">
						</td>
						<td class="text-nowrap">
							<div class=" ">
								<h6 class="mb-1 ">OF MONSTERS AND MEN JAPAN TOUR 2020</h6>

								<p class="mb-1 small">@マイナビBLITZ赤坂 / TOKYO</p>

								<span class="badge badge-pill pill-lg pill-warning">Media Listing</span>	
							</div>
						</td>
						<td class="small text-nowrap">Mar 20th 12:59AM ~<br>Apr 01st 03:59PM (JST)</td>
						<td>0</td>
						<td>￥169,000</td>
						<td><span class="badge badge-danger">
							Sold Out
						</span></td>
						<td class="text-nowrap">
							<a href="admin-edit-event.php" class="typcn px-1 typcn-edit" data-target="#modal-ccdelete" data-toggle="tooltip" data-placement="top" title="Edit Event Information"></a>
							<a href="admin-event-settings.php" class="typcn px-1 typcn-cog-outline"></a>
							<a href="listing-display.php" class="typcn px-1 typcn-eye-outline"></a>


						</td>
					</tr>

					<tr>
						<td width="50">
							<img src="https://d38fgd7fmrcuct.cloudfront.net/bw_48/bh_48/pf_1/1_3sox63vrwc0wa9icf36wi.png" style="height:60px; width: 60px;">
						</td>
						<td class="text-nowrap">
							<div class=" ">
								<h6 class="mb-1 ">OF MONSTERS AND MEN JAPAN TOUR 2020</h6>

								<p class="mb-1 small">@マイナビBLITZ赤坂 / TOKYO</p>

								<span class="badge badge-pill pill-lg pill-warning">Draft</span>	
							</div>
						</td>
						<td class="small text-nowrap">Mar 20th 12:59AM ~<br>Apr 01st 03:59PM (JST)</td>
						<td>0</td>
						<td>￥169,000</td>
						<td><span class="badge badge-danger">
							Sold Out
						</span></td>
						<td class="text-nowrap">
							<a href="admin-edit-event.php" class="typcn px-1 typcn-edit" data-target="#modal-ccdelete" data-toggle="tooltip" data-placement="top" title="Edit Event Information"></a>
							<a href="admin-event-settings.php" class="typcn px-1 typcn-cog-outline"></a>
							<a href="listing-display.php" class="typcn px-1 typcn-eye-outline"></a>


						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

</section>