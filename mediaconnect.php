<?php include('header.php'); ?>

<style>
	header, footer {display:none;}
	li {
		margin-bottom: 5px;
	}
</style>

<div class="container-fluid my-5">
	<div class="row pt-5">
		<div class="col-md-10 mx-auto">
			<h3 class="text-center">Media Connect</h3>
			<div class="card-deck text-center">
				<div class="card mb-4 box-shadow">
					<div class="card-header">
						<h4 class="my-0 font-weight-normal">Entry Plan</h4>
					</div>
					<div class="card-body py-4">
						<h4 class="card-title pricing-card-title text-danger"><span class="font-weight-normal"></span> ¥0 / month</h5>
						<hr>

						<ul class="list-unstyled mt-3">
							<li>Email limit within plan: <strong>0</strong></li>
							<li>Send email rate: <strong>¥5/email</strong></li>
							<li>Segment: <strong>1</strong></li>
							<li>Tags: <strong>5</strong></li>
							<li>&nbsp;</li>
							<li>*Insights available for fee</li>
						</ul>
					</div>
				</div>
				<div class="card mb-4 box-shadow">
					<div class="card-header">
						<h4 class="my-0 font-weight-normal">Starter Plan</h4>
					</div>
					<div class="card-body py-4">
						<h4 class="card-title pricing-card-title text-danger"><span class="font-weight-normal"></span> ¥ 10,000 / month</h5>
						<hr>
						<ul class="list-unstyled mt-3">
							<li>Email limit per month: <strong>6,000</strong></li>
							<li>Send email rate: <strong>¥2/email*</strong></li>
							<li>Segments: <strong>3</strong></li>
							<li>Tags: <strong>10</strong></li>
							<li>&nbsp;</li>
							<li class="font-weight-bold">1 Free Basic Insight per month</li>
						</ul>
					</div>
				</div>
				<div class="card mb-4 box-shadow">
					<div class="card-header">
						<h4 class="my-0 font-weight-normal ">Business Plan</h4>
					</div>
					<div class="card-body py-4">
						<h4 class="card-title pricing-card-title text-danger"><span class="font-weight-normal"></span> ¥ 50,000 / month</h5>
						<hr>

						<ul class="list-unstyled mt-3">
							<li>Email limit per month: <strong>60,000</strong></li>
							<li>Send email rate: <strong>¥1/email*</strong></li>
							<li>Segments: <strong>15</strong></li>
							<li>Tags: <strong>40</strong></li>
							<li>&nbsp;</li>
							<li class="font-weight-bold">2 Detailed Insights per month</li>

						</ul>
					</div>
				</div>
				<div class="card mb-4 box-shadow">
					<div class="card-header">
						<h4 class="my-0 font-weight-normal">Professional Plan</h4>
					</div>
					<div class="card-body py-4">
						<h4 class="card-title pricing-card-title text-danger"><span class="font-weight-normal"></span> ¥ 100,000 / month</h5>
						<hr>

						<ul class="list-unstyled mt-3">
							<li>Email limit per month: <strong>250,000</strong></li>
							<li>Send email rate: <strong>¥0.05/email*</strong></li>
							<li>Segments: <strong>30</strong></li>
							<li>Tags: <strong>80</strong></li>
							<li>&nbsp;</li>
							<li class="font-weight-bold">4 Detailed Insights per month</li>
						</ul>
					</div>
				</div>
			</div>
			<p>*After reaching email limit</p>

		</div>
	</div>
</div>

<?php include('footer.php'); ?>
