<?php include('header.php'); ?>

<section class="top__content bg-overlay bg-overlay-zaiko full-h d-flex justify-content-center align-items-center flex-column">

	<div class="card card-login p-5">

		<h3 class="text-center">Thank you for contacting ZAIKO</h3>

		<div class="text-center mb-3">
			<p>We will respond to your inquiry within 2 business days.</p>
		</div>	

		<div class="text-center">
			<a href="/" class="btn btn-default btn-lg btn-block">Return to Home Page</a>
		</div>

	</div>

</section>

<?php include('footer.php'); ?>