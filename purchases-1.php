<?php include('guestadmin-header.php'); ?>

<section class="container p-5">
	<div class="row">
		<div class="col-md-12 d-flex justify-content-between">
			<h3>Purchases - Fyre Festival (105)</h3>
			<span>
				<a href="#" class="btn btn-outline-brand" data-toggle="modal" data-target="#modal-agree"><i class="fas fa-file-csv"></i>&nbsp;&nbsp;Download List</a>

				<a href="#" class="btn btn-brand" data-toggle="modal" data-target="#modal-agree"><i class="fas fa-envelope"></i>&nbsp;&nbsp;Email Attendees</a>
			</span>
		</div>

		<div class="col-md-12">
			<div class="card">
				<table class="table table-hover">
					<thead class="thead-light text-center">
						<tr>
							<th scope="col">Name</th>
							<th scope="col">Ticket</th>
							<th scope="col"># of Tickets</th>
							<th scope="col">Time Purchased</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<td>John Doe</td>
							<td>GA</td>
							<td>1</td>
							<td>2019-07-02 20:32</td>

						</tr>
						<tr>
							<td>John Smith</td>
							<td>GA</td>
							<td>3</td>
							<td>2019-07-01 12:12</td>

						</tr>
						<tr>
							<td>Steve Jobs</td>
							<td>VIP</td>
							<td>2</td>
							<td>2019-06-20 18:38</td>

						</tr>
						<tr>
							<td>John Doe</td>
							<td>GA</td>
							<td>1</td>
							<td>2019-07-02 20:32</td>
							
						</tr>
						<tr>
							<td>John Doe</td>
							<td>GA</td>
							<td>1</td>
							<td>2019-07-02 20:32</td>
							
						</tr>
						<tr>
							<td>John Doe</td>
							<td>GA</td>
							<td>1</td>
							<td>2019-07-02 20:32</td>
							
						</tr>
						<tr>
							<td>John Doe</td>
							<td>GA</td>
							<td>1</td>
							<td>2019-07-02 20:32</td>
							
						</tr>
						<tr>
							<td>John Doe</td>
							<td>GA</td>
							<td>1</td>
							<td>2019-07-02 20:32</td>
							
						</tr>
						<tr>
							<td>John Doe</td>
							<td>GA</td>
							<td>1</td>
							<td>2019-07-02 20:32</td>
							
						</tr>
					</tbody>
				</table>
			</div>
			<nav aria-label="">
				<ul class="pagination justify-content-end">
					<li class="page-item disabled">
						<a class="page-link" href="#" tabindex="-1">Prev</a>
					</li>
					<li class="page-item active"><a class="page-link" href="#">1</a></li>
					<li class="page-item"><a class="page-link" href="#">2</a></li>
					<li class="page-item"><a class="page-link" href="#">3</a></li>
					<li class="page-item">
						<a class="page-link" href="#">Next</a>
					</li>
				</ul>
			</nav>
			<div class="text-center">
				<a href="guestadmin.php" class="btn btn-brand btn-lg">Return to Home Page</a>
			</div>
		</div>
	</div>
</section>

<?php include('guestadmin-footer.php'); ?>
