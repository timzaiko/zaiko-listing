<?php include('header-admin-profile.php'); ?>

		<?php include('admin-aside-home.php') ?>

		<main class="admin-main pb-5">
			<?php include('admin-breadcrumbs.php') ?>

			<section class="container-fluid pt-4 px-5">
				<div class="row justify-content-between align-items-center my-3">
					<div>
						<h3 class="font-weight-normal mb-2">Your Dashboard</h3>
						<p class="text-muted">A quick overview of your events and statistics</p>
					</div>
					<div class="">
						<a href="listing-display.php" class="btn btn-secondary">Go To Microsite</a>
						<a href="admin-create-event.php" class="btn btn-default">Create An Event</a>
					</div>
				</div>
			</section>

			<div class="container-fluid px-5">
				<div class="row">
					<div class="col-md-4 p-0">
						<div class="card py-4 px-4 border-right">
							<div class="card-body">
								<h5 class="mb-3 font-weight-light">Daily Sales</h5>

								<div class="">
									<canvas id="chart_daily_sales" class="chartjs-render-monitor"></canvas>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4 p-0">
						<div class="card py-4 px-4 rounded-0">
							<div class="card-body">
								<h5 class="mb-3 font-weight-light">Members</h5> 
								<div class="">
									<canvas id="chart_profit_share"class="chartjs-render-monitor"></canvas>
								</div>
							</div>
							

						</div>
					</div>
					<div class="col-md-4 p-0">
						<div class="card py-4 px-4 border-left">
							<div class="card-body">
								<h5 class="mb-3 font-weight-light">Revenue</h5> 
								<div class="">
									<canvas id="chart_sales_stats" class="chartjs-render-monitor"></canvas>									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="container-fluid px-5">
					<div class="row d-flex justify-content-between align-content-center">
						<h5 class="mb-0">Events</h5>
						<ul class="nav nav-tabs mr-3">
							<li class="nav-item">
								<a class="nav-link active" href="#"><strong>Upcoming</strong></a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Past</a>
							</li>
						</ul>
					</div>
				</div>

				<?php include('module-events.php') ?>

				<div class="text-center">
					<a href="admin-event.php" class="btn btn-default">See All Events</a>
				</div>

			</main>

	<?php include('footer.php'); ?>

	<script src="dashboard.js"></script>

