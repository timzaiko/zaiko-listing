<?php include('guestadmin-header.php'); ?>

	<section class="container-fluid">
		<div class="text-center pt-5">
			<div class="text-success">
				<i class="fas fa-check-circle h1"></i>
				<h4 class="fa-step--text">Your Event is now Active!</h4>
			</div>
		</div>
		<p class="text-center text-dark-grey">You may now sell tickets to the general public. Here is your event details.</p>
	</section>

	<section class="container p-5">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<ul class="status-details">
					<li class="d-flex">
						<strong>Event Name</strong>
						<a href="event-display.php" target="_blank"><p>Fyre Festival</p></a>
					</li>
					<li class="d-flex">
						<strong>Time Starts</strong>
						<p>2019-12-31 06:00</p>
					</li>
					<li class="d-flex">
						<strong>Time Ends</strong>
						<p>2020-01-02, 23:00</p>
					</li>
					<li class="d-flex">
						<strong>Venue</strong>
						<p>Odaiba Fuji Building</p>
					</li>
					<li class="d-flex">
						<strong>Tickets</strong>
						<div>
							<p>100 VIP @ 50,000JPY</p>
							<p>50 GA @ 10,000JPY</p>
							<p>200 Standing @ 5,000JPY</p>
						</div>
					</li>
					<li class="d-flex">
						<strong>Event Description</strong>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A velit asperiores voluptatum sequi, nulla incidunt quos. Ab magnam obcaecati, corporis veritatis, hic dolor possimus, voluptatem eum officia dicta veniam et.</p>
					</li>
					<li class="d-flex">
						<strong>Event Status</strong>
						<p>Approved</p>
					</li>
					<li class="d-flex">
						<strong>Date Approved</strong>
						<p>2019-08-30 12:00</p>
					</li>
				</ul>
				<div class="text-center">
					<div class="alert alert-warning mb-5">
						<strong>Note: Timeout Tokyo has the rights to suspend your event at any given time without reason if your event is deemed malicious, harmful and/or unlawful.</strong>  
					</div>
					<a href="guestadmin.php" class="btn btn-brand btn-lg">Return to Home Page</a>
					<a href="editevent.php" class="btn btn-outline-brand btn-lg">Pause this Event </a>
				</div>
			</div>
		</div>		
	</section>

	<div id="modal-delete" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg modal-dialog-centered">
		<div class="modal-content queue p-5 text-center">
			<h3>Are you sure you want to remove this event?</h3>
			<p class="pb-3">Once removed you cannot restore the event again.</p>
			<span>
				<a href="guestadmin.php" class="btn btn-brand btn-lg">Remove this event</a>
				<a href="#" data-dismiss="modal" aria-label="Close" class="btn btn-secondary btn-lg">Cancel</a>
			</span>
		</div>
	</div>
</div>

<?php include('guestadmin-footer.php'); ?>


