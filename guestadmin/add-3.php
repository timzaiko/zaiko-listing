<script src="https://cdn.ckeditor.com/ckeditor5/12.3.0/classic/ckeditor.js"></script>

	<div class="form-group">
		<label>Cover Image <i class="fas fa-question-circle text-secondary" data-toggle="tooltip" data-placement="right" title="Main image to be displayed on the event page"></i></label>
		<div class="guest-img">
			<input type="file" class="custom-file-input guest-img__btn" id="customFile">
			<figure>
				<img class="w-100" src="/img/guestadmin/guest-img-placeholder.jpg" alt="">
				<div class="guest-img__desc">
					<i class="fas fa-camera"></i>
					<p class="pt-3">Upload Image<br>16 x 9 ratio recomm.</p>
				</div>
			</figure>
		</div>
	</div>

	<div class="form-group">
		<label>Event Description</label>
		<textarea name="content" id="editor" name="" rows="10"></textarea>
	</div>

	<style>
		.ck-editor__editable_inline {
			min-height: 500px;
		}

	</style>

	<div class="form-group">
		<label>Terms and Conditions  <i class="fas fa-question-circle text-secondary" data-toggle="tooltip" data-placement="right" title="To which customers are required to agree before purchasing"></i></label>
		<textarea class="form-control" placeholder="" name="" id="" rows="5"></textarea>
	</div>


	<!-- Modal -->
	<div id="modal-terms" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg modal-dialog-centered">
			<div class="modal-content p-5 text-center">
				<h4>Terms &amp; Conditions</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore accusamus facere veritatis vel quaerat consectetur aliquam, cum dolorum deserunt rem quo natus adipisci illum vitae debitis, eveniet aperiam nostrum a!</p>
			</div>
		</div>
	</div>

	<script>
		ClassicEditor
		.create( document.querySelector( '#editor' ) )
		.catch( error => {
			console.error( error );
		} );
	</script>