	<div class="form-group ticket-info">
		<div class="row ticket-single">
			<div class="col-md-6 col-sm-6">
				<label>Ticket Type</label>
				<input type="text" placeholder="ex. VIP, GA, Early Bird"class="form-control" required>
			</div>
			<div class="col-md-3 col-sm-3 pl-md-0">
				<label>Price</label>
				<input type="number" class="form-control form-control-price" required>
			</div>
			<div class="col-md-2 col-sm-2 pl-md-0">
				<label>Quantity</label>
				<input type="number" class="form-control" required>
			</div>
			<div class="col-md-1 col-sm-1 d-flex pt-5">
				<a href="#"><i class="add-one fas fa-plus-circle fa-add-ticket text-success"></i></a>&nbsp;&nbsp;
				<a href="#"><i class="delete-one fas fa-minus-circle fa-remove-ticket text-danger"></i></a>
			</div>
		</div>
	</div>

	<div class="form-group mt-5">
		<h4>Discounted Tickets<sup>&nbsp;<i class="fas fa-question-circle text-secondary" data-toggle="tooltip" data-placement="right" title="They can only be purchased with a coupon code"></i></sup></h4>
		<div class="row ticket-single">
			<div class="col-md-4 col-sm-4">
				<label>Ticket Type</label>
				<input type="text" placeholder="ex. VIP, GA, Early Bird"class="form-control" required>
			</div>
			<div class="col-md-2 col-sm-2 pl-md-0">
				<label>Price</label>
				<input type="number" class="form-control form-control-price" required>
			</div>
			<div class="col-md-2 col-sm-2 pl-md-0">
				<label>Quantity</label>
				<input type="number" class="form-control" required>
			</div>
			<div class="col-md-3 col-sm-3 pl-md-0">
				<label>Coupon Code</label>
				<input type="text" class="form-control" placeholder="Min. 4 characters" required>
			</div>

			<div class="col-md-1 col-sm-1 d-flex pt-5">
				<a href="#"><i class="add-one fas fa-plus-circle fa-add-ticket text-success"></i></a>&nbsp;&nbsp;
				<a href="#"><i class="delete-one fas fa-minus-circle fa-remove-ticket text-danger"></i></a>
			</div>
		</div>
	</div>
