	<label>Event Name</label>
	<div class="form-group">
		<input type="text" class="form-control" placeholder="Event name" required>
		<div class="invalid-feedback">
			Please enter the event name
		</div>
	</div>

	<div class="form-group">
		<label>Event Type</label>
		<select class="form-control" required>
			<option selected>Select from below</option>
			<option value="1">Concert/Live Show</option>
			<option value="1">Festival</option>
			<option value="2">Conference</option>
			<option value="3">Meet-and-Greet</option>
		</select>
	</div>  


	<div class="form-row">
		<div class="form-group col-md-6">
			<label>Time Starts</label>
			<div class="row">
				<div class="col-md-8">
					<input type="date" class="form-control" required>
				</div>
				<div class="col-md-4 pl-md-0">
					<input type="time" class="form-control" required>
				</div>
			</div>
		</div>
		<div class="form-group col-md-6">
			<label>Time Ends</label>
			<div class="row">
				<div class="col-md-8">
					<input type="date" class="form-control" required>
				</div>
				<div class="col-md-4 pl-md-0">
					<input type="time" class="form-control" required>
				</div>
			</div>
		</div>
		<div class="form-group col-md-12">
			<label>Venue</label>
			<input type="text" class="form-control" placeholder="Type in venue name to display available from the list" required>
			<div class="invalid-feedback">
				Please enter the event name
			</div>
		</div>
	</div>  