<?php include('header-admin-profile.php'); ?>

		<?php include('admin-aside-event.php') ?>

		<main class="admin-main">
			<?php include('admin-breadcrumbs.php') ?>

			<section class="container-fluid pt-4 px-5">
				<div class="row justify-content-between align-items-center my-3">
					<div>
						<h3 class="font-weight-normal mb-2">Event Details</h3>
						<p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. </p>
					</div>
					<div class="">
						<button class="btn btn-secondary">View Event</button>
						<button class="btn btn-default" data-toggle="modal" data-target="#modal-addticket">+ Add Ticket</button>
					</div>
				</div>
			</section>

			<?php include('module-event-name.php') ?>

			<div class="container-fluid px-5">
				<div class="row">
					<div class="col-md-4 p-0">
						<div class="card py-4 px-4 border-right ">
							<div class="card-body">
								<div class="d-flex justify-content-between mb-3">
									<h5 class="mb-1 font-weight-light">Tickets Sold</h5> 
									<h4 class="text-brand mb-0">0</h4>
								</div>
								<div class="progress">
									<div class="progress-bar bg-success" role="progressbar" style="width: 0" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4 p-0">
						<div class="card py-4 px-4 rounded-0">
							<div class="card-body">
								<div class="d-flex justify-content-between mb-3">
									<h5 class="mb-1 font-weight-light">Check-Ins</h5> 
									<h4 class="text-brand mb-0">0</h4>
								</div>
								<div class="progress">
									<div class="progress-bar bg-warning" role="progressbar" style="width: 0" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
							</div>
							

						</div>
					</div>
					<div class="col-md-4 p-0">
						<div class="card py-4 px-4 border-left">
							<div class="card-body">
								<div class="d-flex justify-content-between mb-3">
									<h5 class="mb-1 font-weight-light">Revenue</h5> 
									<h4 class="text-brand mb-0">￥0</h4>
								</div>
								<div class="progress">
									<div class="progress-bar bg-info" role="progressbar" style="width: 0" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="container-fluid px-5">
				<div class="row d-flex justify-content-between align-content-center">
					<h5 class="mb-0">Tickets</h5>
					<ul class="nav nav-tabs mr-3">
						<li class="nav-item">
							<a class="nav-link active" href="#"><strong>White Label</strong></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Public</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Selective</a>
						</li>
					</ul>
				</div>
			</div>

			<section class="container-fluid px-5">
				<div class="row">
					<div class="col-12 table-responsive card p-0">
						<table class="table table-hover table-striped table-md text-nowrap">
							<thead class="">
								<tr class="">
									<th>Name</th>
									<th>Price</th>
									<th>Sales Period</th>
									<th>Members</th>
									<th>Available</th>
									<th>Status</th>
									<th>Action</th>

								</tr>
							</thead>
							<tbody>
								<tr valign="middle">
									<td class="text-center py-5" colspan="7">
										<h4 class="font-weight-light text-muted">Make your first ticket for this event</h4>
										<button class="btn btn-default" data-toggle="modal" data-target="#modal-addticket">+ Add Ticket</button>
									</td>
								</tr>

							</tbody>
						</table>
					</div>
				</div>

			</section>

		</main>

<!-- modal -->
<div id="modal-addticket" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content queue p-5 text-center">
			<h4>Please select your ticket type</h4>
			<a href="admin-create-ticket.php" class="btn btn-primary btn-lg btn-block">Paid Ticket</a>
			<a href="admin-create-ticket.php" class="btn btn-default btn-lg btn-block">Free Ticket*</a>
			<div class="mt-2">*While these tickets are for free events, organizers will be charged 50¥ for each ticket “purchased”.</div>
		</div>


	</div>
</div>

<?php include('footer.php'); ?>


