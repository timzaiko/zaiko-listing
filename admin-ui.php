<?php include('header-admin-profile.php'); ?>

<div class="container-fluid">
	<div class="row flex-lg-nowrap">
		<?php include('admin-aside-account.php') ?>

		<main class="admin-main">
			<?php include('admin-breadcrumbs.php') ?>

			<section class="container-fluid pt-5 px-5">
				<div class="row flex-column mb-3">
					<h3 class="font-weight-normal mb-3">Personal Information</h3>
					<p class="text-muted">Manage your account settings</p>
				</div>
			</section>


			<section class="container-fluid px-5">
				<div class="row card">

					<div class="col-md-9 mx-auto">

						<div class="text-center my-4">
							<img class="img-profile" src="https://d38fgd7fmrcuct.cloudfront.net/bw_70/bh_70/1_3r2xttwhvbo3aq0p0bkqp.png" alt="" style="border-radius: 50%">
							<div class="d-block mt-2">
								<button class="btn btn-secondary">Change Photo</button>
							</div>
						</div>

						<div class="d-md-flex flex-row align-items-center mb-3">
							<div class="col-md-3 text-md-right">
								<label><strong>Name</strong></label>

							</div>
							<div class="col-md-9 col-sm-12"> 
								<input id="" type="text" value="Timothy Lee" required="required" class="form-control">
							</div>
						</div>

						<div class="d-md-flex flex-row align-items-center mb-3">
							<div class="col-md-3 text-md-right">
								<label><strong>E-mail</strong></label>

							</div>
							<div class="col-md-9 col-sm-12"> 
								<input id="" type="email" value="tim@zaiko.io" required="required" class="form-control">
							</div>
						</div>

						<div class="d-md-flex flex-row align-items-center mb-3">
							<div class="col-md-3 text-md-right">
								<label><strong>Phone</strong></label>

							</div>
							<div class="col-md-9 col-sm-12"> 
								<input id="" type="phone" value="+818072754385" required="required" class="form-control">
							</div>
						</div>

						<div class="d-md-flex flex-row align-items-center mb-3">
							<div class="col-md-3 text-md-right">
								<label><strong>Gender</strong></label>

							</div>
							<div class="col-md-9 col-sm-12"> 
								<select id="" placeholder="The official name of your event" required="required" class="form-control">
									<option value="" selected>Male</option>
									<option value="">Female</option>
									<option value="">Undefined</option>
								</select>
							</div>
						</div>

						<div class="d-md-flex flex-row align-items-center mb-3">
							<div class="col-md-3 text-md-right">
								<label><strong>Birthday</strong></label>

							</div>
							<div class="col-md-9 col-sm-12"> 
								<input id="" type="date" placeholder="The official name of your event" required="required" class="form-control">
							</div>
						</div>

						<div class="d-md-flex flex-row align-items-top mb-3">
							<div class="col-md-3 text-md-right mt-3">
								<label><strong>Location</strong></label>

							</div>
							<div class="col-md-9 col-sm-12"> 
								<select id="" type="" required="required" class="form-control mb-3"><option value="" selected>Japan</option>
								</select>
								<select id="" type="" required="required" class="form-control mb-3"><option value="" selected>Kanto</option>
								</select>
								<select id="" type="" required="required" class="form-control mb-3"><option value="" selected>Tokyo</option>
								</select>
							</div>
						</div>

					</div>

					<div class="text-center card-footer">
						<input type="submit" class="btn btn-default" value="Update">
						<input type="submit" class="btn btn-secondary" value="Cancel">
					</div>
				</div>

			</section>

			<section class="container-fluid px-5">
				<div class="row card">
					<div class="">
					</div>
					<div class="card-body p-4">
						<p class="h4">Facebook</p>
						<div class="d-flex justify-content-between align-items-center">
							<div class="d-flex align-items-center">

								<img src="https://graph.facebook.com/2264750540430381/picture?type=square" width="30" height="30" style="border-radius: 50%">
								<p class="mb-0 ml-2">Linked to <strong>Timothy Lee</strong></p>
							</div>
							<input type="submit" class="btn btn-dark" value="Unlink">
						</div>

						<hr> <!-- If no facebook -->

						<div class="d-flex justify-content-between align-items-center">
							<p class="mb-0">Link to Your Facebook Account</p>
							<input type="submit" class="btn btn-facebook" value="Link">
						</div>
					</div>
				</div>
			</section>

			<section class="container-fluid px-5">
				<div class="row card">
					<div class="card-body p-4">
						<p class="h4">Deactivate</p>
						<div class="d-flex justify-content-between align-items-center">
							<p class="mb-0">Clicking the button will deactivate your account.	</p>
							<button class="btn btn-danger" data-toggle="modal" data-target="#modal-deactivate">Deactivate</button>
						</div>
					</div>
				</div>
			</section>

			<div class="checkbox checkbox-default">
									<input id="checkbox1" type="checkbox">
									<label for="checkbox1">
										Default
									</label>
								</div>
								<div class="checkbox checkbox-primary">
									<input id="checkbox2" type="checkbox" checked="">
									<label for="checkbox2">
										Primary
									</label>
								</div>
								<div class="checkbox checkbox-success">
									<input id="checkbox3" type="checkbox">
									<label for="checkbox3">
										Success
									</label>
								</div>
								<div class="checkbox checkbox-info">
									<input id="checkbox4" type="checkbox">
									<label for="checkbox4">
										Info
									</label>
								</div>
								<div class="checkbox checkbox-warning">
									<input id="checkbox5" type="checkbox" checked="">
									<label for="checkbox5">
										Warning
									</label>
								</div>
								<div class="checkbox checkbox-danger">
									<input id="checkbox6" type="checkbox" checked="">
									<label for="checkbox6">
										Check me out
									</label>
								</div>
		</div>
	</main>
</div>

<div id="modal-deactivate" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered">
		<div class="modal-content p-5 text-center">
			<h4>Are you sure you want to deactivate your account?</h4>
			<p >You can reactivate it any time by contacting our support.</p>
			<span><button class="btn btn-danger btn-lg">
				Deactivate
			</button> 
			<button class="btn btn-secondary btn-lg">
				Cancel
			</button></span>
		</div>
	</div>
</div>

<?php include('footer.php'); ?>


