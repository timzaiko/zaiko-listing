<?php include('header-admin.php'); ?>

<div class="container-fluid">
	<div class="row flex-lg-nowrap">

		<?php include('admin-aside.php') ?>

		<main class="admin-main">
			<section class="container-fluid pt-5 px-5">
				<div class="row">
					<div class="col-md-12">
						<h3 class="font-weight-normal">Your Account</h3>
						<p class="text-muted">Manage your account settings
						</p>
					</div>
				</div>
			</section>


			<section class="container-fluid px-5">
				<div class="row card">

					<form action="" method="get">

						<div class="row card-body p-4">
							<div class="col-md-6 form-group">
								<label>First Name</label>
								<div class="d-flex justify-content-between align-items-center">
									<p class="mb-0">Timothy</p>
									<!-- <input class="form-control" type="text" value="Tim" name="namespace" id="namespace" placeholder="Namepsace"> -->
									<a href="#" data-toggle="modal" data-target="#modallang" class="btn btn-secondary" onclick="">Edit</a>
									
								</div>
							</div>
							<div class="col-md-6 form-group">
								<label>Last Name</label>
								<p>Lee</p>
								<!-- <input class="form-control" type="text" value="Lee" name="translation_text" id="translation_text" placeholder="Translated text"> -->
							</div>
							<div class="col-md-12 form-group">
								<label>Email Address</label>
								<input class="form-control" type="text" value="tim@zaiko.io">
							</div>
							<div class="col-md-4 form-group">
								<label>Gender</label>
								<select type="number" class="form-control" placeholder="" required>
									<option>Male</option>
									<option>Female</option>
									<option>Undefined</option>
								</select>  
							</div>
							<div class="col-md-4 form-group">
								<label>Age</label>
								<input class="form-control" type="number" value="">
							</div>
							<div class="col-md-4 form-group">
								<label>Phone Number</label>
								<input class="form-control" type="phone" value="08072754385">
							</div>
							<div class="col-md-12 form-group">
								<label>City/Country of Residence</label>
								<input class="form-control" type="text" value="Tokyo, Japan" >
							</div>
							<div class="col-md-12 form-group">
								<label>New Password</label>
								<input class="form-control" type="password" value="12345678" >
							</div>
							<div class="col-md-12 form-group">
								<label>Confirm New Password</label>
								<input class="form-control" type="password" value="">
							</div>
						</div>
						<div class="card-footer text-right">
							<input type="submit" class="btn btn-pink" value="Update">

							<a href="#" data-toggle="modal" data-target="#modallang" class="btn btn-secondary" onclick="">Cancel</a>
						</div>
					</form>
				</div>
			</section>

			<section class="container-fluid p-5">
				<div class="row">
					<div class="col-12 table-responsive card p-0">
						<table class="table table-striped table-hover table-lang">
							<thead class="thead-dark">
								<tr class="text-nowrap">
									<th>ID</th>
									<th>Namespace</th>
									<th>Original</th>
									<th>Description</th>
									<th>JA</th>
									<th>EN</th>
									<th>KO</th>
									<th>ZH-HANT</th>
									<th>ZH-HANS</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td> 174969 </td>
									<td class="text-nowrap"> influencer </td>
									<td class="font-weight-bold"> ZAIKOインフルエンサーのご招待  </td>
									<td class="font-italic"> alt tag for influencer banner image for influencer invite email </td>
									<td>  </td>
									<td> Inviting all ZAIKO Influencers </td>
									<td>  </td>
									<td>  </td>
									<td>  </td>
									<td class="edit_section">
										<button class="btn btn-default" data-toggle="modal" data-target="#modallang" onclick="showIframe('/theflame/lang_edit.php?id=174969')">EDIT
										</button>
									</td>
								</tr>

							</tbody>
						</table>
					</div>
				</div>
			</section>
		</main>
	</div>
</div>

<?php include('footer.php'); ?>


