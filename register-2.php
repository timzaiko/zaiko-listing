<?php include('header.php'); ?>

<style>
	header,footer {
		display: none;
	}
</style>

<section class="bg-overlay opacity-40 bg-zaiko full-h d-flex justify-content-center align-items-center flex-column">

	<div class="container-fluid">
		<div class="mx-auto card card-login">
			<div class="d-flex justify-content-between align-items-center card-header">
				<a href="/">
					<img class="img-header d-block mx-auto" src="https://d38fgd7fmrcuct.cloudfront.net/1_3srrgnchq4ywmry64ua57.png" alt="">
				</a>
				<p class="m-0">Confirmation</p>
			</div>

			<div class="card-body p-3">

				<div class="alert alert-warning">To finish the sign-up process you must complete e-mail verification. Please enter the verification code sent to your e-mail address.</div>
				<form class="needs-validation" action="" novalidate>
					<div class="form-group">
						<label>Enter the verification code</label>
						<input type="text" class="form-control" placeholder="" required>
						<div class="invalid-feedback">
							Please enter correct email address
						</div>
					</div>  

					<div class="d-block mt-3">
						<button class="btn btn-pink btn-xl btn-block" type="submit">Verify</button>
					</div>

				</form>
			</div>
			<div class="card-footer d-flex justify-content-between align-items-center">
				<p class="m-0">Did not receive email?</p>
				<a href="" class="btn btn-outline-dark">Send Again</a>
			</div>

		</div>
	</div>

</section>

<?php include('footer.php'); ?>