<aside class="admin-aside show" id="navbar-sidebar">
	<ul class="px-0 py-5">

		<li class="">
			<span class="pl-4">Account</span>

			<ul class="sub-menu p-0 active">
				<li>
					<a style="color:white;" href="" target="" class="d-block">Settings</a>
				</li>
			</ul>
			<ul class="sub-menu p-0">
				<li>
					<a href="" target="" class="d-block">Preferences</a>
				</li>
			</ul>
			<ul class="sub-menu p-0">
				<li>
					<a href="" target="" class="d-block">Purchases</a>
				</li>
			</ul>

		</li>

		<li class="">
			<span class="pl-4">Billing</span>
			<ul class="sub-menu p-0">
				<li>
					<a href="" target="" class="d-block">Payment Methods</a>
				</li>
			</ul>

			<ul class="sub-menu p-0">
				<li>
					<a href="" target="" class="d-block">Subscriptions</a>
				</li>
			</ul>

		</li>



	</ul>
</aside>