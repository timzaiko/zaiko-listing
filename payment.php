<?php include('header-beatink.php'); ?>

<div class="microsite">
	<?php include('admin-breadcrumbs.php') ?>

	<section class="container mt-3">
		<div class="row">

			<div class="card-order col-lg-4 order-lg-last">
				<div class="card p-3 p-md-4">

					<h4 class="text-center text-md-left mb-3">Order Details</h4>

					<div class="d-flex align-items-center mb-3">
						<img src="https://d38fgd7fmrcuct.cloudfront.net/bw_300/bh_300/pf_1/1_3t4ahlk6y38uj1rv666wt.jpg" alt="" class="img-thumb" style="width:60px; height: 60px">
						<div class="meta ml-3">
							<strong>Squarepusher Japan Tour</strong>
							<p class="small">2020/06/01 - 2020/06/03</p>
						</div>

					</div>


					<div class="" id="prices">

						<div class="ticket-price single d-flex justify-content-between align-items-center py-2">
							<span>						
								<p>早割チケット</p>
								<h5 class="mb-1 text-primary">￥2,500</h5>
							</span>

							<span class="d-flex align-items-center">
								<p class="text-muted">x 1</p>
								<button class="typcn typcn-pencil"></button>

							</span>
						</div>

						<div class="ticket-price single d-flex justify-content-between align-items-center py-2">
							<span>						
								<p>		前売りチケット</p>
								<h5 class="mb-1 text-primary"> ￥3,000</h5>
							</span>

							<select class="form-control form-control-ticket" id="exampleFormControlSelect1">
								<option>1</option>
								<option selected>2</option>
								<option>3</option>
								<option>4</option>
								<option>5</option>
							</select>
						</div>

						<div class="ticket-price single d-flex justify-content-between align-items-center py-2">
							<span>						
								<p>Handling Fee</p>
								<p class="small" data-toggle="tooltip" data-placement="bottom" title="this is extra charge on your ticket price">what is this?</p>
							</span>

							<a class="text-muted small">At Checkout</a>		
						</div>

						<div class="ticket-price d-flex justify-content-between align-items-center pt-3">
							<h4>Total</h4>
							<h4 class="text-primary">￥8,500</h4>
						</div>

					</div>


				</div>
			</div>

			<div class="col-lg-8">
				<div class="card p-4">
					<div class="row">
						<div class="col-md-12">
							<h4>Name on Ticket</h4>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-md-6 ">
							<label>First Name</label> 
							<input id="event-name" type="text" placeholder="Ex. Taro" required="required" class="form-control" value="Timothy">
						</div> 
						<div class="form-group col-md-6 ">
							<label>Last Name</label> 
							<input id="event-name" type="text" placeholder="Ex. Yamada" required="required" class="form-control" value="Lee">
						</div>
					</div>

					<div class="alert alert-warning">
						Please enter your legal name
					</div>
				</div>


				<div class="card p-4">
					<h4>Terms of Service</h4>
					<li> No changes, cancellations or refunds can be made to your ticket once the purchase is complete.<br>
					</li><li> This ticket cannot be refunded due to cancellations or changes in artists.<br>
					</li><li> This ticket is non-transferable.<br>
					</li><li> This ticket may carry system and other processing fees. Please understand that these fees cannot be refunded in the rare case the event is canceled.<br>
					</li><li> Many events have age and other restrictions. Please confirm that you qualify before completing your purchase.<br>
					</li><li> Please make sure to allow your email client (phone settings etc) to accept emails from @zaiko.io.</li>
					<div class="collapse" id="collapse-tos">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione asperiores quas id facere animi placeat rem veniam alias ut reiciendis ipsam eligendi repudiandae, perspiciatis distinctio consequatur tenetur dolor soluta consectetur.
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur dolorum maxime placeat odit, tempore. Sunt cum nulla dolorum, cupiditate a assumenda ipsam accusantium laudantium at amet ducimus voluptatem beatae, accusamus.
					</div>

					<p class="my-2 collapsed text-blue tos-more" data-toggle="collapse" data-target="#collapse-tos" aria-expanded="false" aria-controls="collapse-tos" >
						See More
					</p>

					<div class="text-center">
						<input type="checkbox" required="required" id="checkboxZaikoTos" name="checkboxZaikoTos">
						<strong>I agree to the Terms of Service</strong>
					</div>
				</div>

				<div class="text-center">
					<a href="payment-2.php" class="btn btn-lg btn-default text-center btn-inline-block">Continue</a>
					<a href="listing-display.php" class="btn btn-lg btn-secondary text-center btn-inline-block">Cancel</a>
				</div>
			</div> 

		</div>



	</div>
</section> 

</div>

<div id="modal-login" class="modal fade show" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<?php include('modal-login.php') ?>
	</div>
</div>

<?php include('footer.php'); ?>

<script>

</script>
