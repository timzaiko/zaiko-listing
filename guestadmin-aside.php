<aside class="col-12 col-md-3 col-lg-3 faq__sidebar d-md-none d-lg-block bg-secondary">
	<nav>
		<div class="navigation">
			<li><i class="fas fa-calendar"></i>Overview</li>
			<li><i class="fas fa-users"></i>Events</li>
			<li><i class="fas fa-receipt"></i>Orders</li>
			<li><i class="fas fa-user"></i>Settings</li>
			<li><i class="fas fa-cog"></i>Admin</li>
		</div>
	</nav>
</aside>