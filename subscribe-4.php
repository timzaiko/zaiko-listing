<?php include('subscribe-header.php') ?>

<div id="modal-auto" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg modal-dialog-centered">
		<div class="modal-content p-5">
			<div class="text-center">
				<h3>Registration Complete</h3>
				<p>Thank you for registering with us. You will receive notification by email when tickets go on sale.</p>
			</div>	

			<div class="text-center">
				<button class="btn btn-primary btn-lg btn-block" data-dismiss="modal" aria-label="Close">Close</button>
			</div>

		</div>
	</div>
</div>

<?php include('subscribe-footer.php') ?>
