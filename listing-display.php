<?php include('header-beatink.php'); ?>

<div class="microsite">
	<section class="container">
		<div class="row">


			<div class="col-md-8 col-sm-12 order-md-first py-3">
				<div class="swiper-container mb-3">
					<div class="swiper-wrapper">
						<div class="swiper-slide">
							<img class="w-100" src="https://d38fgd7fmrcuct.cloudfront.net/1_3t0wkgd2krfe4wuskvot3.jpg" alt="">
						</div>
						<!-- Add Pagination -->
						<div class="swiper-pagination"></div>
					</div>
				</div>
				<h2>Squarepusher Japan Tour</h2>
				<h4 class="font-weight-light">{{ Event Subtitle }}</h4>
				<ul class="event-meta p-0">
					<li>
						<i class="fas fa-calendar-alt"></i>
						<div class="event-meta-date">{{ Event Date }}</div>
					</li>
					<li>
						<i class="fas fa-map-marker-alt"></i>
						<div class="event-meta-venue">
							<a href="https://maps.google.com" target="_blank">{{ Venue }}</a>
						</div>
					</li>
					<li>
						<div class="event-meta-tag">
							<a href="#" data-toggle="modal" data-target="#modal-id000" class="badge badge-primary">{{ Category }}</a>

						</div>
					</li>
				</ul>

				<div class="event-desc">
					OF MONSTERS AND MEN
					Japan Tour 2020 Postponed
					20th Jan (Mon) @ Tokyo, Mynavi BLITZ
					21st Jan (Tue) @ Osaka, BIGCAT

					PLEASE TAKE NOTE;
					Due to the artist’s illness, we regret to inform you that both OF MONSTERS AND MEN’s tour (shown above) has been POSTPONED.
					We are currently working on rescheduling the shows. We will inform you once we have them confirmed.
					PLEASE hold on to your ticket as it will be VALID for the re-schedued show.

					【Refund】
					If you prefer to refund your ticket after we announce the re-schedued dates, we will inform you of how to collect your refund. So, in the meantime, please do not discard your ticket. You will need to present it.

					We sincerely apologize for any inconvenience to you.

					OF MONSTERS AND MEN
					Japan Tour 2020 Postponed
					20th Jan (Mon) @ Tokyo, Mynavi BLITZ
					21st Jan (Tue) @ Osaka, BIGCAT

					PLEASE TAKE NOTE;
					Due to the artist’s illness, we regret to inform you that both OF MONSTERS AND MEN’s tour (shown above) has been POSTPONED.
					We are currently working on rescheduling the shows. We will inform you once we have them confirmed.
					PLEASE hold on to your ticket as it will be VALID for the re-schedued show.

					【Refund】
					If you prefer to refund your ticket after we announce the re-schedued dates, we will inform you of how to collect your refund. So, in the meantime, please do not discard your ticket. You will need to present it.


					【Refund】
					If you prefer to refund your ticket after we announce the re-schedued dates, we will inform you of how to collect your refund. So, in the meantime, please do not discard your ticket. You will need to present it.

					We sincerely apologize for any inconvenience to you.
				</div>
			</div>

			<a class="btn btn-xl rounded-0 w-100 d-block d-sm-none text-center bg-info fixed-bottom" href="#checkoutPrices">Buy Tickets</a>


			<div class="col-md-4 col-sm-12 pt-3 ">

				<div id='app'>
					<v-calendar is-expanded></v-calendar>
					<br>

					<div class="card card-checkout p-3 p-md-4">

						<h4 class="text-center text-md-left mb-3">Buy Tickets</h4>
						<div class="card-checkout-content" id="checkoutPrices">
							<div class="">
								<a href="#">Enter Promo Code</a>
							</div>

							<div class="ticket-price single d-flex justify-content-between align-items-center py-3">
								<span>						
									<p>早割チケット</p>
									<h5 class="mb-1 text-primary">￥2500</h5>
									<p class="text-muted small font-italic">Sales End: 2020/02/29</p>
								</span>

								<select class="form-control form-control-ticket" id="exampleFormControlSelect1">
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</select>
							</div>
							<div class="ticket-price single d-flex justify-content-between align-items-center py-3">
								<span>						
									<p>	GA</p>
									<h5 class="mb-1 text-primary">￥3500</h5>
									<p class="text-muted small font-italic">Sales End: 2020/02/29</p>
								</span>

								<p class="text-muted"><strong>Sold Out</strong></p>							
							</div>
							<div class="ticket-price single d-flex justify-content-between align-items-center py-3">
								<span>						
									<p>		前売りチケット</p>
									<h5 class="mb-1 text-primary"> ￥3000</h5>
									<p class="text-muted small font-italic">Sales End: 2020/02/29</p>
								</span>

								<select class="form-control form-control-ticket" id="exampleFormControlSelect1">
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</select>
							</div>

							<div class="ticket-price d-flex justify-content-between align-items-center pt-3">
								<h4>Total</h4>
								<h4 class="text-primary">￥0</h4>
							</div>



							<div class="d-block pt-3">
								<button href="#prices" class="btn btn-lg btn-default btn-block" data-toggle="modal" data-target="#modal-login">
									Buy Now
								</button>
								<!-- if unavail -->
							<!-- <button class="btn btn-lg btn-secondary btn-block" disabled>
								Not Available
							</button> -->
							<p class="text-muted small font-italic text-center mt-3">Handling fees not included</p>
						</div>

						
					</div>


				</div>
			</div>
		</div>


	</div>
</section> 

</div>

<div id="modal-login" class="modal fade show" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<?php include('modal-login.php') ?>
	</div>
</div>

<?php include('footer.php'); ?>

<script>
	var swiper = new Swiper('.swiper-container', {
		loop: true,
		speed: 700,
		dynamicBullets: true,
		effect: 'fade',
		autoplay: {
			delay: 4000,
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
	});
</script>
