<?php include('header-beatink.php'); ?>


<section class="container py-5">
	<div class="row">
		<div class="col-lg-4 card p-5 card-pricing selected nothover">
			<div class="text-center">							
				<h3 class="mb-2">Free</h3>
				<div class="border-bottom pb-4 mb-4">
					<span class="h2 card-title mb-0 d-inline-block">$0</span><span class="small">/month</span>
				</div>
			</div>
			<ul class="pl-3">
				<li><span class="px-0 typcn typcn-tick">List item here please</span></li>
				<li><span class="px-0 typcn typcn-tick">List item here please</span></li>
				<li class="text-muted"><span class="px-0 typcn typcn-times">List item here please</span></li>
				<li class="text-muted"><span class="px-0 typcn typcn-times">List item here please</span></li>
				<li class="text-muted"><span class="px-0 typcn typcn-times">List item here please</span></li>
			</ul>	
			<button class="btn btn-lg btn-secondary">Keep</button>

			<h5 class="text-center text-info mt-3">This is your current plan</h5>

		</div>
		<div class="col-lg-4 card p-5 card-pricing hover">
			<div class="text-center">
				<h3 class="mb-2">Platinum</h3>
				<h5 class="text-center text-success">Best For Value</h5>

				<div class="border-bottom pb-4 mb-4">
					<span class="h2 card-title mb-0 d-inline-block">$XXX</span><span class="small">/year</span>
				</div>
			</div>
			<ul class="pl-3">
				<li><span class="px-0 typcn typcn-tick">List item here pleasList item here pleasList item here please</span></li>
				<li><span class="px-0 typcn typcn-tick">List item here please</span></li>
				<li><span class="px-0 typcn typcn-tick">List item here please</span></li>
				<li><span class="px-0 typcn typcn-tick">List item here please</span></li>
				<li><span class="px-0 typcn typcn-tick">List item here please</span></li>
			</ul>	
			<a href="#" class="btn btn-lg btn-default" data-toggle="modal" data-target="#modal-login">Upgrade Now</a>
		</div>

		<div class="col-lg-4 card p-5 card-pricing nothover">
			<div class="text-center">
				<h3 class="mb-2">Premium</h3>
				<div class="border-bottom pb-4 mb-4">
					<span class="h2 card-title mb-0 d-inline-block">$XX</span><span class="small">/month</span>
				</div>
			</div>
			<ul class="pl-3">
				<li ><span class="px-0 typcn typcn-tick">List item here please</span></li>
				<li ><span class="px-0 typcn typcn-tick">List item here please</span></li>
				<li ><span class="px-0 typcn typcn-tick">List item here please</span></li>
				<li class="text-muted"><span class="px-0 typcn typcn-times">List item here please</span></li>
				<li class="text-muted"><span class="px-0 typcn typcn-times">List item here please</span></li>
			</ul>	
			<button class="btn btn-lg btn-secondary">Buy Now</button>
		</div>
	</div>
</section>


<div id="modal-login" class="modal fade show" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<?php include('modal-login.php') ?>
	</div>
</div>

<?php include('footer.php'); ?>

<script>

</script>
