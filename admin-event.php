<?php include('header-admin-profile.php'); ?>

		<?php include('admin-aside-home.php') ?>

		<main class="admin-main">
			<?php include('admin-breadcrumbs.php') ?>

			<section class="container-fluid pt-4 px-5">
				<div class="row justify-content-between align-items-center my-3">
					<div>
						<h3 class="font-weight-normal mb-1">Events</h3>
						<p class="text-muted">A quick overview of your upcoming and past events</p>
					</div>
					<a href="admin-create-event.php" class="btn btn-default">Create Event</a>

				</div>
			</section>

			<div class="container-fluid px-5">
				<div class="row d-flex justify-content-between">
					<h5 class="mb-0">Event Type</h5>
					<ul class="nav nav-tabs mr-3">
						<li class="nav-item">
							<a class="nav-link" href="#">All</a>
						</li>
						<li class="nav-item">
							<a class="nav-link active" href="#"><strong>Private</strong></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Secret</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Media</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Draft</a>
						</li>
					</ul>
				</div>
			</div>
			<?php include('module-events.php') ?>
			<div class="text-center my-4">
				<div class="spinner-border"></div>
			</div>
		</main>


<?php include('footer.php'); ?>


