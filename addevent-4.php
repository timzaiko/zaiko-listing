<?php include('guestadmin-header.php'); ?>

<section class="container-fluid">
	<div class="progress-bar d-xs-none pt-5">
		<ol class="clearfix">
			<li class="through">
				<div class="progress-status">
					<i class="fas fa-wrench"></i>
				</div>
				<h5 class="fa-step--text">1. Event Information</h5>

			</li>
			<li class="through">
				<div class="progress-status">
					<i class="fas fa-ticket-alt fa-step"></i>					
				</div>
				<h5 class="fa-step--text">2. Add Tickets</h5>
			</li>
			<li class="active">
				<div class="progress-status">
					<i class="fas fa-check fa-step"></i>					
				</div>
				<h5 class="fa-step--text">3. Confirmation</h5>
			</li>
		</ol>
	</div>
</section>

	<section class="container py-5">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<h4>Confirm Your Event Details</h4>
				<form class="needs-validation" action="guestadmin.php" novalidate>

					<?php include('guestadmin/add-3.php'); ?>

					<div class="form-check text-center">
						<input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
						<label class="form-check-label" for="defaultCheck1">
							I agree to <a href="#" data-toggle="modal" data-target="#modal-terms">TimeOut Tokyo Terms and Conditions</a>
						</label>
					</div>

					<div class="text-center mt-4">
						<a href="event-preview.php" class="btn btn-brand btn-lg">Preview your event</a>
						<a href="addevent-1.php"><button class="btn btn-secondary btn-lg">Cancel</button></a>
					</div>

				</form>
			</div>
		</div>
	</section>

<?php include('guestadmin-footer.php'); ?>

