<!DOCTYPE html>
<html>
<head>
	<title>{{title}}</title>
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800|Noto+Sans+JP:300,400,500,700&display=swap" rel="stylesheet">
</head>
<body>

	<style>
		body {
			font-family: 'Noto Sans JP', sans-serif;
			overflow: hidden;
			font-size: 16px;
		}

		p, li {
			font-weight: 300;
		}

		h1, h2, h3, h4, h5, h6 {
			font-family: 'Montserrat', sans-serif;
			font-weight: 800;
		}

		.btn {
			border-radius: 50px;
			padding: 1em 2em;
			font-weight: bold;
			background: black;
		}

		.box {
			background: white;
			color: black;
			padding: 10px 15px;
			display: inline-block;
		}

		.full-h {
			background: linear-gradient(to left, #00cdcd, #00cdcd, #4a97a6);;
			position: absolute;
			width: 100%;
		}

		.full-h > div {
			z-index: 99;
			position: relative;
		}

		.full-h:after {
			content: "";
			position: absolute;
			bottom: 0;
			right: 0;
			left: 0;
			height: 0;
			border-right: 100vw solid #ef40b4;
			border-top: 40vw solid transparent;
			width: 0;
			z-index: 0;
		}

		a:hover {
			text-decoration: none;
		}

		ul {
			list-style: none;
			
		}

		ul > li {
			margin-bottom: 20px; 
			line-height: 1.5;
		}

		.mock img {
			display: block;
			margin: 0 auto;
		}

		@media (min-width: 768px) {
			.full-h {
				height: 100vh;	
			}
			.mock {
				margin-left: 15%;
			}
			.mock img {
				height: 80vh;
			}
			ul {
				padding-right: 100px;
			}
		}

		@media (max-width: 767px) {
			body {
				overflow-y:scroll;
			}
			h1 {
				font-size: 30px;
			}
			.full-h:after {
				position: fixed;
			}
			.mock img {
				width: 70%;
			}
			.btn {
				margin-bottom: 50px;
			}
		}

	</style>

	<header>
		<nav class=" fixed-top d-flex p-4 justify-content-between align-items-center">
			<a href="https://zaiko.io" target="_blank"><img width="150" class="img-header img-fluid" src="https://zaiko.io/img/ZAIKO-logo-text-white.svg"></a>

			
			<a href="https://zaiko.io/business" class="text-white font-weight-normal mr-3" target="_blank">ZAIKOとは</a>

		</div>
	</nav>
</header>

<section class="full-h d-md-flex align-items-center">

	<div class="mock wow fadeIn" data-wow-duration="1.5s">
		<img src="/wedding/top.jpg" alt="">
	</div>
	<div class="text-white px-3 h5 wow fadeIn p-5 my-0" data-wow-duration="2s" data-wow-delay="0.5s">
		<h1 class="mb-0" style="font-weight:700">ZAIKO FOR</h1>
		<h1 class="mb-4 box">INFLUENCERS</h1>
		<ul class="mb-4 pl-0">
			<li>リスクフリーであなたが好きなイベントのチケットを販売し、販売毎に報酬を得よう！</li>
			<li>初期費用、運営費無料でZaiko for Influencersを利用して、新しい収入ビジネススタイルを確率！</li>
			<li>インフルエンサーチケット販売プラットフォーム</li>
			<li>招待制ですので</li>
		</ul>
		<div class="btn btn-lg btn-dark mb-5 mb-md-0 mb-lg-0">SIGN UP FOR AN ACCOUNT</div>
		
		
		
	</div>
</section>


<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>

<script>
	new WOW().init();
</script>

</body>
</html>