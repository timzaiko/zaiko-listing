<?php include('header.php'); ?>

<section class="banner bg-overlay h-100" style="background: url('/img/bg-business.jpg') no-repeat center center / cover">
	<div class="container">
		<div class="row text-block banner-narrow">
			<div class="col-md-10 col-sm-12 mx-auto p-10rem py-0 text-center">
				<h2>We streamline everything you need to sell your tickets</h2>
				<p class="lead">At ZAIKO we make ticketing as simple as humanly possible, so you don’t have to. Get the most out of your ticket sales experience with our robust platform.</p>
				<a href="login.php" class="btn btn-lg btn-default my-4">Sign Up Now</a>
			</div>
		</div>
	</div>
</div>
</section>

<section class="container-fluid">
	<div class="row">
		<div class="col-md-6 col-sm-12 d-flex align-items-center">
			<div class="text-block p-10rem pl-md-0">
				<h5 class="zaiko-purple mb-2">White Label Ticketing</h5>
				<h2>Your Ticket, Your Brand</h2>
				<p class="lead">Bespoke your event any way you want. Concerts, conferences, meet-and-greets - if you can name it, we can get it done. Sell your tickets directly from your website, or take advantage of our customizable microsite.</p>
				<div class="list-check d-flex flex-wrap mt-4">
					<div class="list-item col-md-6 p-0">
						<img src="/img/ico-check.svg" alt="" width="16" height="16">
						<span>Free or Paid Tickets Available</span>
					</div>
					<div class="list-item col-md-6 p-0">
						<img src="/img/ico-check.svg" alt="" width="16" height="16">
						<span>Lottery Tickets Option</span>
					</div>
					<div class="list-item col-md-6 p-0">
						<img src="/img/ico-check.svg" alt="" width="16" height="16">
						<span>Any Ticket Types like VIP, Pre-Sale</span>
					</div>
					<div class="list-item col-md-6 p-0">
						<img src="/img/ico-check.svg" alt="" width="16" height="16">
						<span>100% Self-Service</span>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6 order-md-first d-flex align-items-end pt-md-5">
			<img class="img-fluid" src="/img/business-1.png" alt="">
		</div>
	</div>
</section>

<section class="container-fluid bg-light-grey">
	<div class="row">
		<div class="col-md-5 d-flex align-items-center">
			<div class="text-block p-10rem pr-md-0">
				<h5 class="zaiko-purple mb-2">Direct-To-Fan</h5>
				<h2>Engage With Your Fanbase</h2>
				<p class="lead">Your biggest asset is your followers, your fans. Spend more time convert them into paying customers, and let ZAIKO handle the rest of the ticket payment process.</p>
				
				<div class="list-check d-flex flex-wrap mt-4">
					<div class="list-item col-md-6 p-0">
						<img src="/img/ico-check.svg" alt="" width="16" height="16">
						<span>100% Paperless Tickets</span>
					</div>
					<div class="list-item col-md-6 p-0">
						<img src="/img/ico-check.svg" alt="" width="16" height="16">
						<span>Fraud-Proof Check-in System</span>
					</div>
					<div class="list-item col-md-6 p-0">
						<img src="/img/ico-check.svg" alt="" width="16" height="16">
						<span>PayPal &amp; CVS Accepted</span>
					</div>
					<div class="list-item col-md-6 p-0">
						<img src="/img/ico-check.svg" alt="" width="16" height="16">
						<span>Stripe Partner</span>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-7 d-flex align-items-end pt-md-5">
			<img class="img-fluid" src="/img/business-2.png" alt="">
		</div>

	</div>
</section>

<section class="container-fluid">
	<div class="row">
		<div class="col-md-6 d-flex align-items-center">
			<div class="text-block p-10rem pl-md-0">
				<h5 class="zaiko-purple mb-2">Dashboard Analytics</h5>
				<h2>Data at your Service</h2>
				<p class="lead">Numbers mean very little if you cannot interpret them. We break down everything from how user interacts with your event page, to what type of music they are interested in. Harness the power of big data for smarter sales strategy when planning your next events.</p>
				<div class="list-check d-flex flex-wrap mt-4">
					<div class="list-item col-md-6 p-0">
						<img src="/img/ico-check.svg" alt="" width="16" height="16">
						<span>Secure User Data</span>
					</div>
					<div class="list-item col-md-6 p-0">
						<img src="/img/ico-check.svg" alt="" width="16" height="16">
						<span>We Never Share Any Data</span>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6 order-md-first d-flex align-items-end pl-0 pt-md-5">
			<img class="img-fluid" src="/img/business-3.png" alt="">
		</div>
	</div>
</section>

<section class="call-to-action bg-overlay" style="background: url('/img/bg-about.jpg') no-repeat center center / cover">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-8 offset-md-2">
				<h2 class="text-center">We do more than just sell tickets</h2>
				<p class="lead my-5 text-center">ZAIKO unlocks your event’s exposure through unique sales channel consisting of news media, KOLs, and venues around the world.</p>
			</div>

			<ul class="nav nav-tabs mx-auto" id="myTab" role="tablist">
				<li class="nav-item mx-3">
					<a class="nav-link btn btn-xl active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">I am a Media Company</a>
				</li>
				<li class="nav-item mx-3">
					<a class="nav-link btn btn-xl" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">I am an Event Organizer</a>
				</li>
			</ul>

			<div class="col-md-12 tab-content mx-auto" id="myTabContent">
				<div class="tab-pane fade show active pt-5" id="home" role="tabpanel" aria-labelledby="home-tab">
					<div class="d-flex flex-wrap justify-content-between">
						<img class="img-fluid img-clients" src="img/partners/iflyer.png" alt="">
						<img class="img-fluid img-clients" src="img/partners/metropolis.png" alt="">
						<img class="img-fluid img-clients" src="img/partners/tokyogigguide.png" alt="">
						<img class="img-fluid img-clients" src="img/partners/clubberia.png" alt="">
						<img class="img-fluid img-clients" src="img/partners/fnmnl.png" alt="">
						<img class="img-fluid img-clients" src="img/partners/tokyocheap.png" alt="">
						<img class="img-fluid img-clients" src="img/partners/timeout.png" alt="">
						<img class="img-fluid img-clients" src="img/partners/spincoaster.png" alt="">
					</div>
					<p class="lead text-center pt-5">Maximize your brand potential and turn it into viable revenue stream through ticketing commission and upsell opportunities. You make money for every event you help promote.</p>
				</div>
				<div class="tab-pane fade pt-5" id="profile" role="tabpanel" aria-labelledby="profile-tab">
					<div class="d-flex flex-wrap justify-content-between">
						<img class="img-fluid img-clients" src="img/partners/beatink.png" alt="">
						<img class="img-fluid img-clients" src="img/partners/fanicon.png" alt="">
						<img class="img-fluid img-clients" src="img/partners/hearst.png" alt="">
						<img class="img-fluid img-clients" src="img/partners/indieasia.png" alt="">
						<img class="img-fluid img-clients" src="img/partners/mutek.png" alt="">
						<img class="img-fluid img-clients" src="img/partners/soundmuseumvision.png" alt="">
						<img class="img-fluid img-clients" src="img/partners/www.png" alt="">
					</div>
					<p class="lead text-center pt-5">Leverage the power of media to sell your tickets. Make your tickets available to them while setting your own terms, including sales and commission.</p>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Pricing -->
<section class="bg-light-grey">
	<div class="container p-5rem">
		<div class="row">
			<div class="col-md-12 text-center">
				<h2>Pricing</h2>
				<p class="lead mb-5">Set your own rates with absolutely no hidden fees.</p>
				<div class="card-deck">
					<div class="card p-5 m-0 hover">
						<h3>ZAIKO E-Ticket</h3>
						<h1 class="card-title pricing-card-title d-inline-block">0%<sup>*</sup></h1>
						<p>*The minimum rate for white-label event<br>Recommended rate is 2.89% plus customer fee</p>
						<div>
							<a href="login.php" class="btn btn-lg btn-default mt-3">Sign Up Now</a>
						</div>
					</div>
					<div class="card p-5 m-0 nothover">
						<h4 class="font-weight-normal">Other Ticketing Companies</h4>
						<h1 class="card-title pricing-card-title"></h1>
						<li>Processing Fee for Paper Ticket*</li>
						<li>Paper Ticket Delivery Charge*</li>
						<li>May Incur Fee for Pre-Sale Tickets</li>
						<li>Service Fee for Multi-language Support**</li>
						<p class="pt-4">*At ZAIKO, all processed tickets are paperless</p>
						<p>**ZAIKO provides real-time support in Japanese, English, Chinese and Korean - <strong>all free of charge</strong></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="banner call-to-action bg-overlay-zaiko text-center">
	<h3>Want to know how ZAIKO can help sell your tickets?</h3>
	<a href="contact.php" class="btn btn-xl btn-outline-light">Contact Us</a>
</section>

<?php include('footer.php'); ?>