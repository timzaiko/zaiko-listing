<?php include('guestadmin-header.php'); ?>

	<section class="container p-5">
		<div class="row">
			<div class="col-md-12">
				<h3>Report</h3>
				<div class="card">
					<table class="table table-hover">
						<thead class="thead-light text-center">
							<tr>
								<th scope="col">Event</th>
								<th scope="col">Tickets Sold</th>
								<th scope="col">Sales</th>
								<th scope="col">Payout Status</th>
							</tr>
						</thead>
						<tbody class="text-center">
							<tr>
								<td><a href="">Fyre Festival</a></td>
								<td>40</td>
								<td>¥ 89,098</td>
								<td>Event Ongoing</td>
								
							</tr>
							<tr>
								<td><a href="">Event #1</a></td>
								<td>101</td>
								<td>¥ 294,892</td>
								<td>Processing</td>
								
							</tr>
							<tr>
								<td><a href="">Burning Man</a></td>
								<td>51</td>
								<td>¥ 103,482</td>
								<td><a href="event-over.php">Pending</a></td>
								
							</tr>
							<tr>
								<td><a href="">Super Bowl Afterparty</a></td>
								<td>20</td>
								<td>¥ 54,082</td>
								<td><a href="event-over.php">Pending</a></td>
								
							</tr>
							<tr>
								<td><a href="">Fuji Rock Tailgate</a></td>
								<td>59</td>
								<td>¥ 100,028</td>
								<td><a href="event-over.php">Completed</a></td>
								
							</tr>


						</tbody>
					</table>
				</div>
				<div class="text-center">
					<a href="guestadmin.php" class="btn btn-outline-brand btn-lg">Return to Home Page</a>
				</div>
			</div>
		</div>
	</section>

<?php include('guestadmin-footer.php'); ?>