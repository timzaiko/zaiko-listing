<section class="container-fluid px-5">
	<div class="row card bg-dark text-white">
		<div class="d-flex justify-content-between align-items-center flex-wrap">
			<div class="d-flex align-items-center pr-5 p-3">
				<img class="mr-3" src="https://d38fgd7fmrcuct.cloudfront.net/bw_200/bh_200/pf_1/1_3qqxqoa8n44yhp5hwb5s3.png" alt="" style="width: 75px; height:75px;">
				<div class="">
					<h5 class="font-weight-normal mb-1">OF MONSTERS AND MEN JAPAN TOUR 2020</h5>
					<p class="text-light-grey mb-1"> 02.14 (Fri) @マイナビBLITZ赤坂 / TOKYO</p>
					<span class="badge badge-pill badge-warning">Secret</span>
				</div>
			</div>

			<div class="d-flex justify-content-around p-3">
				<div class="d-flex align-items-center">
					<img src="https://d38fgd7fmrcuct.cloudfront.net/bw_40/bh_40/pf_1/1_3s1ija3yw1aeyfm5x4nda.png" alt="" class="mr-3 rounded-sm">
					<p class="mb-0 text-nowrap">Event by<br>
					TimTam Band</p>

				</div>
				<div class="d-flex align-items-center px-4">
					<img src="https://d38fgd7fmrcuct.cloudfront.net/bw_40/bh_40/pf_1/1_3r2xttwhvbo3aq0p0bkqp.png" alt="" class="mr-3 rounded-circle">
					<p class="mb-0 text-nowrap">Event Owner<br>
					Timothy Lee</p>

				</div>
			</div>
		</div>
	</div>
</section>
