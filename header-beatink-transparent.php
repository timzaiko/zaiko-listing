<!DOCTYPE html>
<html lang="en">
<head>
	<title>ZAIKO</title>
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
	<link rel="icon" href="/img/favicon.ico">

	<!-- swiper css -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css">

	<link type="text/css" rel="stylesheet" href="/css/vone/app.css">
	<!-- <link type="text/css" rel="stylesheet" href="/vue/my-project/src/static/app.css"> -->
</head>

<body class>

	<header class="">
		<nav class="header--transparent navbar navbar-expand-lg navbar-light py-2">
			<a href="/">
				<img class="img-header img-fluid" src="https://d38fgd7fmrcuct.cloudfront.net/pf_1//bh_150/bw_400/1_3t4s8ikdhcuxt9q2acdv7.png">
			</a>

			<a class="navbar-toggler navbar-toggler-zaiko" data-toggle="collapse" data-target="#navbar-user" aria-controls="navbar-user" aria-expanded="false" aria-label="Toggle navigation">
				<span></span>
				<span></span>
				<span></span>
			</a>



			<div class="admin-navbar d-flex flex-lg-row-reverse justify-content-between align-items-center">

				<ul class="collapse navbar-collapse navbar-collapse-zaiko menu menu-right flex-lg-row-reverse mb-0" id="navbar-user">
					<li class="dropdown-user d-flex flex-column align-items-center">
						<div class="d-flex align-items-center justify-lg-content-end w-100">
							<img class="img-profile mr-2" src=" https://d38fgd7fmrcuct.cloudfront.net/bw_300/bh_300/pf_1/1_3r2xttwhvbo3aq0p0bkqp" alt="">
							<span class="welcome d-block pr-2">User Name</span>
						</div>
						<ul class="sub-menu sub-menu--user right w-100 py-3 px-0">
							<li>
								<a class="" href="https://zaiko.dev/legacy_support/view_my_ticket">View My Tickets</a>
							</li>
							<li>
								<a class="" href="https://zaiko.dev/legacy_support/settings">Account Settings</a>
							</li>

							<li class="mx-lg-3">
								<a class="btn btn-default d-block text-white" href="https://zaiko.dev/legacy_support/admin">
									Business
								</a>
							</li>
							
							<li>
								<a dusk="logout_link" href="https://zaiko.dev/legacy_support/logout_link" class="">Sign Out</a>
							</li>
						</ul>

					</li>
					<li class="dropdown-lang d-flex align-items-center">
						<div class="dropdown">
							<span class="dropdown-toggle" id="langButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="flag-icon flag-icon-en"></i>
								<span class="ml-2">English</span>
							</span>
							<ul class="dropdown-menu sub-menu sub-menu--user right w-100 py-3 px-0 " aria-labelledby="langButton">
								<li>
									<a href="https://iflyer.zaiko.dev/business/overview?profile_id=4&amp;profile%5Bincrementing%5D=1&amp;profile%5Bexists%5D=1&amp;profile%5BwasRecentlyCreated%5D=0&amp;profile%5Btimestamps%5D=1&amp;lang=ja"><i class="mr-2 flag-icon flag-icon-ja"></i>
										<span>日本語</span>
									</a>
								</li>
								<li>
									<a href="https://iflyer.zaiko.dev/business/overview?profile_id=4&amp;profile%5Bincrementing%5D=1&amp;profile%5Bexists%5D=1&amp;profile%5BwasRecentlyCreated%5D=0&amp;profile%5Btimestamps%5D=1&amp;lang=ko"><i class="mr-2 flag-icon flag-icon-ko"></i>
										<span>한국어</span>
									</a>
								</li>
								<li>
									<a href="https://iflyer.zaiko.dev/business/overview?profile_id=4&amp;profile%5Bincrementing%5D=1&amp;profile%5Bexists%5D=1&amp;profile%5BwasRecentlyCreated%5D=0&amp;profile%5Btimestamps%5D=1&amp;lang=zh-hans"><i class="mr-2 flag-icon flag-icon-zh-hans"></i>
										<span>简体中文</span>
									</a>
								</li>
								<li>
									<a href="https://iflyer.zaiko.dev/business/overview?profile_id=4&amp;profile%5Bincrementing%5D=1&amp;profile%5Bexists%5D=1&amp;profile%5BwasRecentlyCreated%5D=0&amp;profile%5Btimestamps%5D=1&amp;lang=zh-hant"><i class="mr-2 flag-icon flag-icon-zh-hant"></i>
										<span>繁體中文</span>
									</a>
								</li>
							</ul>
						</div>
					</li>
					<hr class="d-lg-none">  

					<nav class="admin-navbar">
						<ul class="m-0 p-0">
							<li>
								<a class="menu-item text-white" href="/admin/">
									<i class="material-icons z4mico-home"></i>
									Home
								</a>
							</li>

							<li>
								<a class="menu-item text-white" href="/_events/">
									<i class="material-icons z4mico-event"></i>
									Listing
								</a>
							</li>
							<li>
								<a class="menu-item text-white" href="/_members/">
									<i class="material-icons z4mico-people"></i>
									All Events
								</a>
							</li>



							<li>
								<a class="menu-item text-white" href="/_reports/">
									<i class="material-icons z4mico-show_chart"></i>
									Support
								</a>
							</li>

							<li>
								<a class="menu-item text-white" href="/_profiles/">
									<i class="material-icons z4mico-group_work"></i>
									Subscribe
								</a>
							</li>
						</ul>
					</nav>

				</ul>

			</div>
		</nav>
	</header>

