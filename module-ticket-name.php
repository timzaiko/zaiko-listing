<section class="container-fluid px-5">
	<div class="row card card-gradient text-white flex-row align-items-center">
		<div class="col-lg-6">
			<div class="d-flex py-3 border-sm-right">
				<img class="mr-3" src="https://d38fgd7fmrcuct.cloudfront.net/bw_200/bh_200/pf_1/1_3qqxqoa8n44yhp5hwb5s3.png" alt="" style="width: 75px; height:75px;">
				<div class="">
					<h5 class="font-weight-normal mb-1">OF MONSTERS AND MEN JAPAN TOUR 2020</h5>
					<p class="text-light-grey mb-1"> 02.14 (Fri) @マイナビBLITZ赤坂 / TOKYO</p>
					<span class="badge badge-pill badge-info">Lottery</span>
					<span class="badge badge-pill badge-danger">Password</span>
				</div>
			</div>
		</div>
		<div class="col-lg-6 p-3">
			<div class="d-flex justify-content-left">
				<span>
					<h5 class="font-weight-normal mb-3">
						<i class="typcn typcn-ticket"></i>General Admission スタンダング<br>￥1,500
					</h5>
					<a class="text-white" href="">https://timtamband.zaiko.dev/_buy/1jrM:3QM:77829</a>
				</span>
			</div>
		</div>

	</div>
</section>
