/*!
 *
 * Jquery Mapael - Dynamic maps jQuery plugin (based on raphael.js)
 * Requires jQuery and Mapael >=2.0.0
 *
 * Map of The Pink Osaka
 * 
 * @author author name
 */
(function (factory) {
    if (typeof exports === 'object') {
        // CommonJS
        module.exports = factory(require('jquery'), require('jquery-mapael'));
    } else if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery', 'mapael'], factory);
    } else {
        // Browser globals
        factory(jQuery, jQuery.mapael);
    }
}(function ($, Mapael) {

    "use strict";
    
    $.extend(true, Mapael,
        {
            maps :  {
                osaka_kohh : {
                    width : 1200,
                    height : 800,

                    // All Seatings
                    'elems': {
                 
                        "1": "M127,70.5h121.5v90.8H127V70.5z",
                        "2": "M127,296.6h121.5v-90.8H127V296.6z"
                    }
                }
            }
        }
    );

    return Mapael;

}));