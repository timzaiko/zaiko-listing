<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
</head>
<body>
	<div class="seating_container">
		<figure class="seatmap">
			<div class="map">
			</div>
		</figure>
	</div>

	<style>
		.seating_container {
			max-width: 900px;
		}

		figure {
			position: relative;
		}

		.mapael .map {
			position: relative;
		}

		.mapael image {
			pointer-events: none;
		}

		.mapael .mapTooltip {
			position: absolute;
			background-color: #000;
			moz-opacity: 1;
			opacity: 1;
			filter: alpha(opacity=100);
			border-radius: 10px;
			padding: 10px;
			z-index: 1000;
			max-width: 200px;
			display: none;
			color: #ffffff;
		}
	</style>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js" charset="utf-8"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.2.7/raphael.min.js"></script>
	<script src="jquery.mapael.min.js"></script>
	<script src="osaka_kohh.js"></script>

	<script>
		$(".seating_container").mapael({
			// map jpg
			plots: {
				'thepink': {
					type: "image",
					url: "tokyo.svg",
					width: 1200,
					height: 800,
					x: 600,
					y: 400,
					attrs: {
						opacity: 1
					}
				}
			},
			// all seatings
			map : {
				name : "osaka_kohh", 
				zoom: {
					enabled: true,
					maxLevel: 10
				}
			},

			// add style/attr to each seating block
			areas: {
				"1": {
					text: {
						content: "AVAILABLE"
					},
					attrs: {
						fill: "green",
						opacity: 0.8
					}
				},
				"2": {
					text: {
						content: "SOLD OUT", 
						attrs: {"font-size": 20}
					},
					attrs: {
						fill: "red",
						opacity: 0.8

					},
					attrsHover: {
						fill: "green"
					}
				},
				"3": {
					text: {
						content: "AVAILABLE"
					},
					attrs: {
						fill: "green",
						opacity: 0.8
					}
				},
				"4": {
					text: {
						content: "AVAILABLE"
					},
					attrs: {
						fill: "green",
						opacity: 0.8
					}
				},
				"5": {
					text: {
						content: "RESERVED", 
						attrs: {"font-size": 20}
					},
					attrs: {
						opacity: 0.8
					},
					tooltip: {content: "VIP (7000)"}
				}
			}
		});

		function cleanMap(){
     $(".map svg image").prependTo(".map svg");
 }
 cleanMap();

	</script>


</body>
</html>
