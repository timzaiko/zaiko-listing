<?php include('header-iflyer.php'); ?>

<div class="zaiko-listing">

		<section class="banner banner-narrow bg-overlay" style="background: url('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQU8j5oHfi40YvK7dhT_1jDlbKYN2Li4RLSYhgsPu_cIEMG31VeZg&s') no-repeat center center / cover">
		<div class="container">
			<div class="row">	
				<div class="col-md-12">
					<h2>Title</h2>
					<h4 class="font-weight-light">Subtitle</h4>
				</div>
			</div>
		</div>
	</section>

	<section class="container pt-3">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="font-weight-normal mb-2">Popular Events</h3>
                    <h5 class="font-weight-light text-muted">Most visited events in the past 7 days</h5>
                </div>
            </div>
        </section>

	<section class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<?php include('listing-popular.php'); ?>
			</div>
		</div>
	</section>

	<section class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="font-weight-normal mb-2">Upcoming Events</h3>
				<h5 class="font-weight-light text-muted">Catch up to the hottest events before they sold out</h5>

				<?php
				include('listing-upcoming.php'); 
				?>

				<div class="text-center">
					<form action="listing-upcoming-single.php">
						<input type="submit" class="btn btn-lg btn-default" value="View All Upcoming Events">
					</form>
				</div>
			</div>
		</div>
	</section>

	

</div>


<?php include('footer.php'); ?>

<script>
	var mySwiper = new Swiper ('.swiper', {
		direction: 'horizontal',
		loop: true,
		speed: 700,
		autoplay: {
			delay: 2000,
		},
		slidesPerView: 4,
		spaceBetween: 10,
		breakpoints: {
			640: {
				slidesPerView: 3
			},
			1080: {
				slidesPerView: 6
			}
		}
	})
</script>