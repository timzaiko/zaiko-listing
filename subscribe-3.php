<?php include('header.php') ?>

<style>
	header, footer {
		display: none;
	}
</style>

<section class="bg-overlay-zaiko">

	<div id="modal-auto" class="modal d-block" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg modal-dialog-centered">
			<div class="modal-content p-5">
				<div class="text-center">
					<h3>Registration for Labyrinth Presale</h3>
					<p>Please enter your credit card information for faster checkout process</p>
				</div>

				<div class="my-3">
					<!-- Form starts -->
					<form action="subscribe-4.php" method="post" id="payment-form">
						<div class="form-group">
							<label for="card-element">
								Card Information (VISA, MasterCard, JCB)
							</label>
							<div id="card-element" class="form-control">
								<!-- A Stripe Element will be inserted here. -->
							</div>

							<!-- Used to display form errors. -->
							<div id="card-errors" role="alert"></div>
						</div>

						<div class="form-group">
							<label>Name on card</label>
							<input type="text" class="form-control" placeholder="Example: Taro Yamada">
						</div>

						<div class="alert alert-warning mb-3" role="alert">
							Your card will not be charged at this time.
						</div>

						<div class="text-center">
							<button class="btn btn-primary btn-lg btn-block" type="submit">Complete your registration</button>
							<div class="text-center pt-3">      
								<a href="subscribe-4.php">I will add this later</a>
							</div>
						</div>
					</form>
				</div>

			</div>
		</div>
	</div>
</section>

<?php include('footer.php') ?>
<script type="text/javascript" src="https://js.stripe.com/v3/"></script>

<script>
	// Create a Stripe client.
	var stripe = Stripe('pk_test_TYooMQauvdEDq54NiTphI7jx');

// Create an instance of Elements.
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
	base: {
		// fontSize: '16px'

	},
	invalid: {
		// color: '#dc3545',
		// iconColor: '#dc3545'
	}
};

// Create an instance of the card Element.
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
	var displayError = document.getElementById('card-errors');
	if (event.error) {
		displayError.textContent = event.error.message;
	} else {
		displayError.textContent = '';
	}
});

// Handle form submission.
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
	event.preventDefault();

	stripe.createToken(card).then(function(result) {
		if (result.error) {
      // Inform the user if there was an error.
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
  } else {
      // Send the token to your server.
      stripeTokenHandler(result.token);
  }
});
});

// Submit the form with the token ID.
function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('payment-form');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);

  // Submit the form
  form.submit();
}
</script>
