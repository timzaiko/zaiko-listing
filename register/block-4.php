<h4 class="text-center my-4">You have successfully created a ZAIKO Account</h4>

<div class="text-center">
	<a href="/" class="btn btn-default btn-xl btn-block">View my account</a>
</div>