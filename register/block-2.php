<form action="register-3.php">
	<div class="form-group">
		<label>Please enter the code sent to your email</label>
		<input type="text" class="form-control form-code" placeholder="4-digit code" maxlength="4">
	</div>

	<button class="btn btn-default btn-xl btn-block btn-submit" type="submit">Verify Your Account</button>
</form>