<form class="needs-validation" action="register-2.php" novalidate>
	<label>Your Full Name</label>
	<div class="form-row">
		<div class="form-group col-md-6">
			<input type="text" class="form-control" placeholder="First name" required>
			<div class="invalid-feedback">
				Please enter your first name
			</div>
		</div>
		<div class="form-group col-md-6">
			<input type="text" class="form-control" placeholder="Last name" required>
			<div class="invalid-feedback">
				Please enter your last name
			</div>
		</div>
	</div>

	<div class="form-group">
		<label>Your Email</label>
		<input type="email" class="form-control" placeholder="Email Address" required>
		<div class="invalid-feedback">
			Please enter correct email address
		</div>
	</div>  

	<div class="form-group">
		<label>Your Email</label>
		<input type="email" class="form-control" placeholder="Email Address" required>
		<div class="invalid-feedback">
			Please enter correct email address
		</div>
	</div>  

	<div class="form-group">
		<label>Your Email</label>
		<input type="email" class="form-control" placeholder="Email Address" required>
		<div class="invalid-feedback">
			Please enter correct email address
		</div>
	</div>  

	<div class="form-group">
		<label>Your Email</label>
		<input type="email" class="form-control" placeholder="Email Address" required>
		<div class="invalid-feedback">
			Please enter correct email address
		</div>
	</div>  

	<div class="form-group">
		<label>Your Email</label>
		<input type="email" class="form-control" placeholder="Email Address" required>
		<div class="invalid-feedback">
			Please enter correct email address
		</div>
	</div>  

	<div class="form-group">
		<label>Your Email</label>
		<input type="email" class="form-control" placeholder="Email Address" required>
		<div class="invalid-feedback">
			Please enter correct email address
		</div>
	</div>  


	<div class="form-group">
		<label>Your Password</label>
		<input type="password" class="form-control" placeholder="Min. 8 characters, numbers" required>
		<div class="invalid-feedback">
			Please enter correct password
		</div>
	</div>  

	<div class="text-center">
		<button class="btn btn-pink btn-xl btn-block my-4" type="submit">Create Your Account</button>
	</div>

	<div class="text-center my-3">
		I agree to the <a href="#" data-toggle="modal" data-target="#modal-terms">Terms and Conditions</a>
	</div>
</form>


<!-- Modal -->
<div id="modal-terms" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg modal-dialog-centered">
		<div class="modal-content queue p-5 text-center">
			<h4>Terms &amp; Conditions</h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore accusamus facere veritatis vel quaerat consectetur aliquam, cum dolorum deserunt rem quo natus adipisci illum vitae debitis, eveniet aperiam nostrum a!</p>
		</div>
	</div>
</div>

<script>
	// Example starter JavaScript for disabling form submissions if there are invalid fields
	(function() {
		'use strict';
		window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
    	form.addEventListener('submit', function(event) {
    		if (form.checkValidity() === false) {
    			event.preventDefault();
    			event.stopPropagation();
    		}
    		form.classList.add('was-validated');
    	}, false);
    });
}, false);
	})();
</script>