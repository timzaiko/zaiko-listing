<style>
	header, footer {
		display: none;
	}

	/*For Stripe's Credit Card input CSS*/
	.StripeElement {
		padding: 10px 12px;
	}

	.StripeElement--invalid {
		border-color: #dc3545;
	}

	.StripeElement--webkit-autofill {
		background-color: #fefde5 !important;
	}

	.InputElement {
		color: #495057;
	}

	#card-errors {
		width: 100%;
		margin-top: .25rem;
		font-size: 80%;
		color: #dc3545;
	}

	.form-control {
		height: calc(1.5em + 0.75rem + 10px);
	}

</style>

<h4 class="text-center">Your Payment Information</h4>

<form action="register-4.php" method="post" id="payment-form">
	<div class="form-group">
		<label for="card-element">
			Enter your card information
		</label>
		<div id="card-element" class="form-control">
			<!-- A Stripe Element will be inserted here. -->
		</div>

		<!-- Used to display form errors. -->
		<div id="card-errors" role="alert"></div>
	</div>

	<div class="form-group">
		<label>Name on card</label>
		<input type="text" class="form-control" placeholder="Example: Taro Yamada">
	</div>

	<div class="alert alert-warning mb-3" role="alert">
		Entering your card will speed up checkout process. Your card will not be charged at all.
	</div>

	<div class="text-center">
		<button class="btn btn-pink btn-xl btn-block" type="submit">Complete your registration</button>
		<div class="text-center py-3">      
			<a href="register-4.php">I will add this later</a>
		</div>
	</div>
</form>