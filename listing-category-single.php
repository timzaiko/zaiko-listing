<?php include('header.php'); ?>

<div class="zaiko-listing">

	<section class="banner banner-narrow bg-overlay" style="background: url('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQU8j5oHfi40YvK7dhT_1jDlbKYN2Li4RLSYhgsPu_cIEMG31VeZg&s') no-repeat center center / cover">
		<div class="container">
			<div class="row">	
				<div class="col-md-12">
					<h2>{{ Category }}</h2>
					<h4 class="font-weight-light">{{ Category Description }}</h4>
				</div>
			</div>
		</div>
	</section>

	<section class="container">
		<div class="row">
			<div class="col-md-12">
				<?php include('listing-upcoming.php'); ?>


				<div class="text-center">
					<div class="spinner-border my-4" role="status">
						<span class="sr-only">Loading...</span>
					</div>
					
					<form action="listing.php">
						<input type="submit" class="btn btn-lg btn-default" value="Return to Home Page">
					</form>
				</div>

			</div>
		</div>
	</section>
</div>


<?php include('footer.php'); ?>

<script>

	// $('.navbar').addClass('position-relative bg-dark');

	var mySwiper = new Swiper ('.swiper', {
		direction: 'horizontal',
		loop: true,
		slidesPerView: 6,
		spaceBetween: 15,
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		breakpoints: {
			640: {
				slidesPerView: 2
			},
			1080: {
				slidesPerView: 4
			}
		}
	})
</script>