<?php include('header-admin-profile.php'); ?>

		<?php include('admin-aside-event.php') ?>

		<main class="admin-main">

			<?php include('admin-breadcrumbs.php') ?>

			<section class="container-fluid pt-4 px-5">
				<div class="row justify-content-between align-items-center my-3">
					<div>
						<h3 class="font-weight-normal mb-2">Event Settings</h3>
						<p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. </p>
					</div>

					<button class="btn btn-default" data-toggle="modal" data-target="#modal-addticket">+ Add Ticket</button>

				</div>
			</section>

			<?php include('module-event-name.php') ?>

			<section class="container-fluid px-5">
				<div class="row" data-masonry='{ "itemSelector": ".grid-item" }'>
					<div class="col-md-6 grid-item pl-0">
						<div class="card">
							<div class="card-header card-header-white d-flex justify-content-between">
								<h5 class="font-weight-normal">General</h5>
								<button class="p-0 typcn typcn-pencil">
								</button>
							</div>
							<div class="row card-body p-4">
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-6 grid-item pr-0">
						<div class="position-absolute" style="top: 50%; left:50%; transform: translate(-50%, -50%); z-index: 1">
							<button class="btn btn-danger btn-lg" data-toggle="modal" data-target="#modal-addticket">Unlock Feature</button>
						</div>

						<div class="card" style="opacity: 0.4;">
							<div class="card-header bg-danger d-flex justify-content-between">
								<h5 class="font-weight-normal">Premium</h5>
								<button class="p-0 typcn typcn-pencil text-white">
								</button>
							</div>
							<div class="row card-body p-4">
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
							</div>
							<div class="card-footer">
								asdf
							</div>

						</div>
					</div>

					<div class="col-md-6 grid-item pl-0">
						<div class="position-absolute" style="top: 50%; left:50%; transform: translate(-50%, -50%); z-index: 1">
							<button class="btn btn-success btn-lg" data-toggle="modal" data-target="#modal-addticket">Start Trial</button>
						</div>

						<div class="card" style="opacity: 0.4;">
							<div class="card-header card-header-white d-flex justify-content-between">
								<h5 class="font-weight-normal">Business</h5>
								<button class="p-0 typcn typcn-pencil">
								</button>
							</div>
							<div class="row card-body p-4">
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>

							</div>
						</div>
					</div>

					<div class="col-md-6 grid-item pr-0">
						<div class="card">
							<div class="card-header card-header-white d-flex justify-content-between">
								<h5 class="font-weight-normal">General</h5>
								<button class="p-0 typcn typcn-pencil">
								</button>
							</div>
							<div class="row card-body p-4">
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
								<div class="col-md-12 d-flex justify-content-between">
									<label>First Name</label>
									<strong class="mb-0">Timothy</strong>
								</div>
							</div>

						</div>
					</div>
				</div>
			</section>

		</main>



<!-- modal -->
<div id="modal-addticket" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content queue p-5 text-center">
			<h4>Please select your ticket type</h4>
			<a href="admin-create-ticket.php" class="btn btn-primary btn-lg btn-block">Paid Ticket</a>
			<a href="admin-create-ticket.php" class="btn btn-default btn-lg btn-block">Free Ticket*</a>
			<div class="mt-2">*While these tickets are for free events, organizers will be charged 50¥ for each ticket “purchased”.</div>
		</div>


	</div>
</div>

<?php include('footer.php'); ?>


