<?php include('header.php'); ?>

<div class="zaiko-listing">

	<section class="banner banner-narrow bg-overlay bg-home">
		<video src="https://zaiko.io/img/zaiko-bg.mp4" autoplay="" playsinline="" loop="" muted=""></video>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>Events to Explore</h2>
					<h5 class="font-weight-light">Your guide to complete event listing under ZAIKO media network</h5>
					
					<?php include('listing-search.php'); ?>
					<div class="">
						<a href="/my" class="btn btn-default btn-lg d-xs-block">Get Your Tickets</a><a href="/support" class="btn btn-lg btn-pink mx-md-3 my-3 d-xs-block">Support</a><a href="/business" class="btn btn-outline-light btn-lg d-xs-block">Media and Organizers</a>
					</div>

				</div>

			</div>
		</div>
	</section>

	<section class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="font-weight-normal">Popular Events</h3>
				<h5 class="font-weight-light text-muted">Most visited events in the past 7 days</h5>

				<?php include('listing-popular.php'); ?>
			</div>
		</div>
	</section>

	<section class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="font-weight-normal">Event by Categories</h3>
				<h5 class="font-weight-light text-muted">We don't just sell music events</h5>
				<?php include('listing-category.php'); ?>
			</div>
		</div>
	</section>

	<section class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="font-weight-normal">Upcoming Events</h3>
				<h5 class="font-weight-light text-muted">Catch up to the hottest events before they sold out</h5>

				<?php
				include('listing-upcoming.php'); 
				?>

				<div class="text-center">
					<form action="listing-upcoming-single.php">
						<input type="submit" class="btn btn-lg btn-default" value="View All Upcoming Events">
					</form>
				</div>
			</div>
		</div>
	</section>

	

</div>


<?php include('footer.php'); ?>

<script>
	var mySwiper = new Swiper ('.swiper', {
		direction: 'horizontal',
		loop: true,
		speed: 700,
		autoplay: {
			delay: 2000,
		},
		slidesPerView: 4,
		spaceBetween: 10,
		breakpoints: {
			640: {
				slidesPerView: 2
			},
			1080: {
				slidesPerView: 4
			}
		}
	})
</script>