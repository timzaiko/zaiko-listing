<section id="modal-promote" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-xl modal-dialog-centered">
			<div class="modal-content p-5 text-center">
				<div class="card-deck">
					<div class="pricing-header pb-3">
						<h2 class="text-brand">Upsell Your Events With Us</h2>
						<p>Quickly build an effective marketing campaign for potential customers with this. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore suscipit velit, error beatae est sint illo fugiat esse ullam excepturi fugit. Nulla soluta impedit, aut dolorem incidunt? Odio, deleniti, deserunt.</p>
					</div>

					<div class="card">
						<div class="card-header">
							<h4 class="my-0 font-weight-normal">Lite</h4>
						</div>
						<div class="card-body">
							<h2 class="card-title pricing-card-title">Free</h2>
							<ul class="list-unstyled mt-3 mb-4">
								<li>Feature</li>
								<li>Feature</li>
								<li>Feature</li>
								<li>Feature</li>
							</ul>
							<button type="button" class="btn btn-lg btn-block btn-outline-brand">Continue with Lite</button>
						</div>
					</div>
					<div class="card">
						<div class="card-header">
							<h4 class="my-0 font-weight-normal">Business</h4>
						</div>
						<div class="card-body">
							<h2 class="card-title pricing-card-title">$150</h2>
							<ul class="list-unstyled mt-3 mb-4">
								<li>Feature</li>
								<li>Feature</li>
								<li>Feature</li>
								<li>Feature</li>
							</ul>
							<a href="guestadmin-contact.php" class="btn btn-lg btn-block btn-brand">Use Business</a>
						</div>
					</div>
					<div class="card">
						<div class="card-header">
							<h4 class="my-0 font-weight-normal">Enterprise</h4>
						</div>
						<div class="card-body">
							<h2 class="card-title pricing-card-title">$250</h2>
							<ul class="list-unstyled mt-3 mb-4">
								<li>Feature</li>
								<li>Feature</li>
								<li>Feature</li>
								<li>Feature</li>
							</ul>
							<a href="guestadmin-contact.php" class="btn btn-lg btn-block btn-brand">Use ZAIKO Enterprise</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>