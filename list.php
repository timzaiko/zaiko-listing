<?php include('header.php'); ?>

<div class="p-5"></div>
<div class="p-5"></div>
<div class="container p-5">
  <div class="row d-flex align-items-center">
    <div class="col-md-4">
      <ul class="list-group card">
        <li class="list-group-item d-flex justify-content-between align-items-center">
          11月27日 
          <span class="badge badge-success badge-pill">19:00</span>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center">
          11月29日
          <span class="badge badge-success badge-pill">13:00</span>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center">
          11月30日
          <span class="badge badge-success badge-pill">17:30</span>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center">
          12月1日
          <span class="badge badge-success badge-pill">13:00</span>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center">
          12月2日
          <span class="badge badge-success badge-pill">19:00</span>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center">
          12月3日
          <span class="badge badge-success badge-pill">12:30</span>
        </li>
      </ul>
    </div>
    <div class="col-md-3">
      <ul class="list-group card">
        <li class="list-group-item d-flex justify-content-between align-items-center">
          SS席
          <span class="badge badge-danger badge-pill">7,800円</span>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center">
          S席
          <span class="badge badge-danger badge-pill">6,800円</span>
        </li>
      </ul>
    </div>
  </div>
</div>