<h2 id="faq-topic-4" class="heading">Ticket Confirmation</h2>

<div class="" data-spy="scroll" data-target="#faq-group-4" data-offset="0">
  <div class="text-block" id="faq-4-1">
    <h3 class="subheading">Where can I find my tickets on ZAIKO?</h3>
    <p>
      <a href="https://zaiko.io/" target="_blank">Please log into your ZAIKO account</a> and select the ticket. You can view the ticket details. Please note that ZAIKO does not issue paper tickets.
    </p>
    <img class="img-faq img-fluid d-none d-sm-block" src="img/screens/faq-4-1-1.jpg" alt="">
    <img class="img-faq img-fluid d-block d-sm-none" src="img/screens/faq-4-1-1-m.jpg" alt="">
    <div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i>    </a>
    </div>
  </div>
  <div class="text-block" id="faq-4-2">
    <h3 class="subheading">I cannot find my ticket on ZAIKO, what should I do?</h3>
    <p>There are some cases where you are not able to see your tickets under your account.</p>
    <p><strong>You may have purchased your tickets with a different account.</strong></p>
    <ol>
      <li>Please try your other e-mail address on ZAIKO. Also, consider that you may have used Facebook or another 3rd party service to create your account. In that case, the e-mail address you used with that service will be the one assigned to your ZAIKO account.
      </li>
    </ol>
    <p><strong>Payment has not been completed.</strong></p>
    <ol>
      <li>Please check your payment record whether or not it has been processed or not.</li>
    </ol>

    <div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i></a>
    </div>
  </div>