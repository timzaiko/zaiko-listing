<h2 id="faq-topic-8" class="heading">Others</h2>

<div class="" data-spy="scroll" data-target="#faq-group-8" data-offset="0">
    <div class="text-block" id="faq-8-1">
        <h3 class="subheading">I lost something at the event. Who should I contact?</h3>
        <p>Please note that ZAIKO is not responsible for the operations of any events. For lost and found inquiries, please contact the event promoter or the venue directly. We will not be able to respond to any inquiries regarding lost properties.</p>
        <p>For other inquiries <a href="/contact" target="_blank">please contact us.</a></p>
    <div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i>    </a>
    </div>
</div>
