<h2 id="faq-topic-1" class="heading">Group Ticketing</h2>

<div class="" data-spy="scroll" data-target="#faq-group-1" data-offset="0">
    <div class="text-block" id="faq-1-1">
        <h3 class="subheading">What is a Group Ticket?</h3>
        <p>A Group ticket is issued when you purchase multiple tickets for the same event at the same time. It is valid for the total number of people that you purchased. With the Group Ticket, you and your guests can enter the event at the same time.</p><p>Depending on the event, it may not be possible to purchase a Group Ticket.</p>
        <div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i>    </a>
        </div>
    </div>
    <div class="text-block" id="faq-1-2">
        <h3 class="subheading">I bought a Group Ticket, but can my guests and I enter the event separately?</h3>
        <p>Yes, you can. Please follow the steps below:</p>
        <ol>
            <li><a href="https://zaiko.io" target="_blank">Please log into your ZAIKO account</a>, and select the Group Ticket you had purchased.</li>
            <li>Under the “Ticket Details” section, click <strong>"Add name"</strong>.
            <img class="img-faq img-fluid d-none d-sm-block" src="img/screens/faq-1-2-1.jpg" alt="">
            <img class="img-faq img-fluid d-block d-sm-none" src="img/screens/faq-1-2-1-m.jpg" alt="">
            </li>
            <li>Please enter the guest’s full name and e-mail address, and click <strong>"Update"</strong>.
            <img class="img-faq img-fluid d-none d-sm-block" src="img/screens/faq-1-2-2.jpg" alt="">
            <img class="img-faq img-fluid d-block d-sm-none" src="img/screens/faq-1-2-2-m.jpg" alt="">
            </li>
        </ol>
        <p>Guest addition and name change is allowed until the day before the event.</p>
        <div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i>    </a>
        </div>
    </div>
    <div class="text-block" id="faq-1-3">
        <h3 class="subheading">If I cannot attend the event, how can my guests enter the show?</h3>
        <p>Please follow the steps below:</p>
        <ol>
            <li><a href="https://zaiko.io" target="_blank">Please log into your ZAIKO account</a>, and select the Group Ticket you had purchased.</li>
            <li>Under the "Ticket Details" section, click <strong>"Edit Name"</strong>.</li>
            <li>Select <strong>"It's for someone else"</strong>.</li>
            <li>The ticket link will be sent to the registered email address.</li>
        </ol>
        <p>Please make sure that the other guests successfully link the ticket to their own accounts.</p>

        <div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i>    </a>
        </div>
    </div>

    <div class="text-block" id="faq-1-4">
        <h3 class="subheading">Can I add more guests to my Group Ticket?</h3>
        <p>No, once the payment is confirmed you cannot add more guests to your Group Ticket.</p>
        <div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i>    </a>
        </div>
    </div>
    <div class="text-block" id="faq-1-5">
        <h3 class="subheading">My group is more than the maximum number of guests I can sign up for, what should I do?</h3>
        <p>Please purchase another Group Ticket for additional guests under your name. However, all of you will need to enter the event at the same time. Please show both Group Tickets at the door.</p>
        <div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i>    </a>
        </div>
    </div>