<h2 id="faq-topic-6" class="heading">Ticket Modification</h2>

<div class="" data-spy="scroll" data-target="#faq-group-6" data-offset="0">
    <div class="text-block" id="faq-6-1">
        <h3 class="subheading">I have mistakenly registered my first and last name in the wrong order. How can I change that?</h3>
        <p>Technically speaking there should not be any problems with having your first and last name filled in the wrong order. However, on the day of the event the door staff may carry out an ID check. Please follow the steps below:</p>
        <ol>
            <li><a href="https://zaiko.io" target="_blank">Please log into your ZAIKO account</a>, select your ticket and click <strong>"Edit Name"</strong>.</li>
            <li>Click <strong>"My name is wrong"</strong>.
                <img class="img-faq img-fluid d-none d-sm-block" src="img/screens/faq-6-1-1.jpg" alt="">
            </li>
            <li>Enter the correct name and click <strong>"Update"</strong>.</li>
        </ol>
        <p>Please note that you can update your ticket information only once, so consider updating your name under account settings as well. This will put your name in the correct order when you make your new ticket purchase.</p>

        <div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i>    </a>
        </div>
    </div>

    <div class="text-block" id="faq-6-2">
        <h3 class="subheading">How can I assign my ticket to another person?</h3>
        <ol>
            <li><a href="https://zaiko.io" target="_blank">Please log into your ZAIKO account</a>, select your ticket and click <strong>"Edit Name"</strong>.</li>
            <li>Click <strong>"It's for someone else"</strong>.</li>
            <img class="img-faq img-fluid d-none d-sm-block" src="img/screens/faq-6-2-1.jpg" alt=""></li>
            <li>Enter the person's name and e-mail address and click <strong>"Update"</strong>.
            </li>
        </ol>
        <p>An email notification will be sent. You can update your ticket information only once.</p>
        <div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i></a>
        </div>
    </div>
</div>
