<h2 id="faq-topic-3" class="heading">Payment</h2>

<div class="" data-spy="scroll" data-target="#faq-group-3" data-offset="0">
    <div class="text-block" id="faq-3-1">
        <h3 class="subheading">What payment methods do you accept?</h3>
        <p>We accept the following credit cards: <strong>VISA, MasterCard, JCB, AMEX, Diners.</strong></p>
        <ul>
            <li>For JCB, AMEX, Diners, you are subject to an additional 100 JPY processing fee. Please fill in your credit card details and select complete purchase. The security code is the 3 or 4-digit number on the back of your credit card. In certain cases, this number may appear on the front of the card.</li>
        </ul>

        <p><strong>Convenience Store Payment</strong></p>        
        <ul> 
            <li>An additional 300JPY processing fee is required for payment at convenience stores.</li>
            <li>Please enter your email address and phone number and click Next. The next page will show the details of payment and instructions will be sent to your email address.</li>
            <li>Please note that the convenience store will NOT issue a paper ticket. Instead, <a href="https://zaiko.io" target="_blank">please log into your ZAIKO account</a> to check the status of your ticket after payment. Please allow up to 30 minutes after payment before your E-Ticket be issued.</li>
            <li>Also note that sometimes this option will not be available depending on the sales time, remaining inventory and other factors.</li>
            <img class="img-faq img-fluid" src="img/screens/faq-3-1-1.jpg" alt="">
        </ul>

        <p><strong>Paypal</strong></p>
        <ul>
            <li>Paypal charges a processing fee of 200 JPY per transaction. </li>
            <li>Once you select Paypal, it will redirect you to the Paypal payment page where you can log in and proceed payment.</li>
        </ul>
    </p>
    <div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i>    </a>
    </div>
</div>
<div class="text-block" id="faq-3-2">
    <h3 class="subheading">Where can I find the ticket payment record on my credit card statement?
    </h3>
    <p>In most cases, the name <em>"Zaiko / iFLYER"</em> will appear in your credit card statement. Payments via PayPal will either say <em>"PayPal * IFLYER" or "PayPal * IFLYER / ZAIKO"</em></p>

    <p>If you had used your parents or friend's credit card please make sure to inform them.</p>
    <div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i></a>
    </div>
</div>

<div class="text-block" id="faq-3-3">
    <h3 class="subheading">Where can I find the payment receipt?</h3>
    <p><a href="https://zaiko.io" target="_blank">Please log into your ZAIKO account</a> and select the ticket. You will see "Purchase Details" in the middle of the page. Please click <strong>"View Receipt"</strong>.</p>
            <img class="img-faq img-fluid" src="img/screens/faq-3-1-2.jpg" alt="">
    <div class="back-to-top"><a href="#" class="btn">Back to Top     <i class="fas fa-chevron-up"></i>    </a>
    </div>
</div>

<div class="text-block" id="faq-3-4">
    <h3 class="subheading">Does ZAIKO store my credit card information?</h3>
    <p>Zaiko stores a reference to your credit card once it is entered in the system. When you click "delete the card" the system will remove the credit card's reference.</p>
    <p><strong>At no time will your credit card information be stored on ZAIKO server nor is it possible for any ZAIKO staff to see this information.</strong></p>
    <div class="back-to-top"><a href="#" class="btn">Back to Top     <i class="fas fa-chevron-up"></i>    </a>
    </div>
</div>
<div class="text-block" id="faq-3-5">
    <h3 class="subheading">For some reason I cannot buy a ticket on ZAIKO, what should I do?</h3>
    <p>We recommend using the latest version of internet browser to purchase ticket at ZAIKO. We do not guarantee that all versions will work, especially Microsoft Internet Explorer. <a href="https://www.google.com/chrome" target="_blank">We suggest using Google Chrome.</a></p>

    <p>You are only able to purchase tickets with personal computer or smartphone.</p>

    <p>It is possible that payments cannot be completed due to authorization problem. In that case, please verify whether or not your credit cards are valid.</p>
</p>
<div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i></a>
</div>
</div>
<div class="text-block" id="faq-3-6">
    <h3 class="subheading">I received a 1 JPY (or similar) charge to my credit card even though I did not buy any tickets recently. What should I do?</h3>
    <p>For security reasons, some credit card authorizations require a transaction of a very small sum. The sum will be returned automatically and your credit card will not be charged.
    </p>
    <div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i></a>
    </div>
</div>