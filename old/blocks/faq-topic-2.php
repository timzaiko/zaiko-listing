<h2 id="faq-topic-2" class="heading">Ticket Purchase</h2>

<div class="" data-spy="scroll" data-target="#faq-group-2" data-offset="0">
    <div class="text-block" id="faq-2-1">
        <h3 class="subheading">How can I buy tickets on ZAIKO?</h3>
        <ol>
            <li>Please choose an event and click <strong>"Buy Now"</strong>.
                <img class="img-faq img-fluid" src="img/screens/faq-2-1-1.jpg" alt="">
            </li>
            <li>Please enter your name, the number of tickets, and select <strong>"Get Ticket"</strong>.</li>
            <li>You will be required to create a ZAIKO account if this is your first time. You can sign in with your Facebook or Google account.
                <img class="img-faq img-fluid d-none d-sm-block" src="img/screens/faq-2-1-2.jpg" alt="">
                <img class="img-faq img-fluid d-block d-sm-none" src="img/screens/faq-2-1-2-m.jpg" alt="">
            </li>
            <li>You can pay by credit card, Convenience Store Payment*, or with Paypal.</li>
            <li>Tickets will be displayed inside your ZAIKO account after purchase. You will be required to show the e-TICKET at the entrance with your mobile phone. Printouts or screenshots are not valid.</li>
            <li><a href="https://d38fgd7fmrcuct.cloudfront.net/1_3qxk186b6lqfdu9g11jfa.jpg" target="_blank">Please follow these instructions on the day of the event.</a></li>
        </ol>
        <p>*Depending on the event, Convenience Store Payment method may not be available.</p>
        <p>Please purchase your tickets using personal computer or smartphone.</p>
        <p>Depending on the event, the door staff might ask for your ID card for verification purpose.</p>
        <div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i>    </a>
        </div>
    </div>
    <div class="text-block" id="faq-2-2">
        <h3 class="subheading">I am not receiving SMS verification code. What should I do?</h3>
        <p>If you have not received a SMS code, please click the <strong>"Call"</strong> button on the next page. You will receive a phone call on the registered phone number providing your SMS verification code.</p>
        Your account will be locked out if you repeat requesting the call.
        If neither of these methods work, <a href="/contact" target="_blank">please contact us.</a>

        <div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i></a>
        </div>
    </div>
    <div class="text-block" id="faq-2-3">
        <h3 class="subheading">Where can I find the seat number of my reserved seat?</h3>
        <p>When you purchase a reserved seat, the seat number will show up on your ticket inside your ZAIKO account.</p>

        <div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i></a>
        </div>
    </div>