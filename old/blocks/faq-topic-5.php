<h2 id="faq-topic-5" class="heading">Ticket Cancellation</h2>

<div class="" data-spy="scroll" data-target="#faq-group-5" data-offset="0">
  <div class="text-block" id="faq-5-1">
    <h3 class="subheading">I want to cancel my ticket. Is it possible to receive a refund?</h3>
    <p>As stated in the terms and conditions, <strong>all ticket sales are final</strong>. Policies defined by our clients prohibit ZAIKO from issuing exchanges or refunds after a ticket has been purchased without their consent, which is not routinely granted.</p>
    <p>Also, please note that tickets cannot be transferred in a commercial transaction. If a ticket has been found to be resold or "scalped" it will be voided and the event organizer will be notified.</p>
    <div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i>    </a>
    </div>
  </div>
  <div class="text-block" id="faq-5-2">
    <h3 class="subheading">My event was cancelled. How can I receive refund for my ticket?</h3>
    <p>If you have used credit card or PayPal, your purchase will be refunded automatically. No further actions are needed.</p>
    <p><strong>Refund Announcements</strong></p>
    <ol>
      <li>We will send an announcement to the registered e-mail address under your ZAIKO account.</li>
      <li>Another announcement will be sent to the same e-mail address when the process has been completed.</li>
    </ol>
    <p><strong>Refund Process</strong></p>
    <ul>
     <li>The credit card, or PayPal account, used at the time of purchase will be automatically refunded, minus any associated charges (see below for details). </li>
   </ul>
   <p><strong>Refundable Amount</strong></p>
   <ul>
    <li>There are typically some non-refundable processing fees used to complete your initial purchase. In some cases, there may be some additional charges required to refund your transaction. The formula usually works out as follows:<br>
    </li>
  </ul>
  <p><em>Total paid amount - payment processing fees - refund processing charges (if any) = amount to be refunded.</em></p>
  <p><strong>Refund Processing Period</strong></p>
  <ul>
    <img class="img-faq img-fluid" src="img/screens/faq-5-2-1.jpg" alt=""></li> 
    <li>Once the event organizer makes the official announcement it typically takes 30 to 60 days to complete the entire process.If you do not receive an email within 30 days of the official cancelation announcement, <a href="/contact" target="_blank">please contact us.</a></li>
    <li>Once you receive the 2nd, refund process completed email, please direct additional inquiries to your credit card company. Please be advised that it may take some time for the refund to appear on your credit card statement.</li>
    <li>If you have made your purchase using Convenient Store Payments, you need to <a href="https://zaiko.io" target="_blank">log into your ZAIKO account</a> and fill in your bank/PayPal information in order to get refunded. <strong>This has to be completed manually. Please note that there is deadline for refunds.</strong>
      <img class="img-faq img-fluid" src="img/screens/faq-5-2-2.jpg" alt=""></li> 
    </ul>

    <div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i></a>
    </div>
  </div>