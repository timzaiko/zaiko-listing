<h2 id="faq-topic-7" class="heading">Account</h2>

<div class="" data-spy="scroll" data-target="#faq-group-7" data-offset="0">
    <div class="text-block" id="faq-7-1">
        <h3 class="subheading">I forgot my password, what should I do?</h3>
        <ol>
            <li><a href="https://zaiko.io/?login" target="_blank">Please go to the Login Page</a> and select <strong>"Forgot Password"</strong>.
                <img class="img-faq img-fluid" src="img/screens/faq-7-1-1.jpg" alt="">
            </li>
            <li>Enter your e-mail address and select <strong>"Next"</strong>.
                <img class="img-faq img-fluid" src="img/screens/faq-7-1-2.jpg" alt="">
            </li>
            <li>Please check your inbox for a password reset email.</li>
            <li>Click the link provided in the email and enter a new password.</li>
        </ol>

        <p><strong>No E-mail received?</strong></p>
        <ol>
            <li>Please check your spam/junk folder.</li>
            <li>Please ensure that the email address <em>noreply@zaiko.io</em> is not blocked.</li>
            <li>If the problem persists please contact your email service provider. They are most likely blocking emails from ZAIKO from being delivered.</li>
        </ol>
        <div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i>    </a>
        </div>
    </div>
    <div class="text-block" id="faq-7-2">
        <h3 class="subheading">How can I change the email address on my account?</h3>
        <ol>
            <li><a href="https://zaiko.io/" target="_blank">Please log into your ZAIKO account</a> and select <strong>"Settings"</strong>.
                <img class="img-faq img-fluid" src="img/screens/faq-7-2-1.jpg" alt="">
            </li>
            <li>Click "Change of email" in the Authentication section.</li>
            <li>Enter a new email address and select <strong>"Send Verify Code"</strong>. You will receive a code to your e-mail address.</li>
        </ol>
        <p>Please Note that this <strong>will not automatically resend</strong> any e-mails which were sent to your previously registered email address.</p>
        <div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i>    </a>
        </div>
    </div>

    <div class="text-block" id="faq-7-3">
        <h3 class="subheading">How can I change my password?</h3>
        <ol>
            <li><a href="https://zaiko.io" target="_blank">Please log into your ZAIKO account</a> and select <strong>"Settings"</strong>.</li>
            <li>Click <strong>"Change of Password"</strong> in the Authentication section.</li>
            <li>Enter the old and new passwords and click <strong>"Save"</strong>.</li>
        </ol>
        <div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i>    </a>
        </div>
    </div>

    <div class="text-block" id="faq-7-4">
        <h3 class="subheading">How do I delete my account?</h3>
        <ol>
            <li><a href="https://zaiko.io" target="_blank">Please log into your ZAIKO account</a> and select <strong>"Settings"</strong>.</li>
            <li>Please scroll down to the end of the page, and select <strong>"Deactivate"</strong>.
                <img class="img-faq img-fluid" src="img/screens/faq-7-4-1.jpg" alt="">
            </li>
            <li>Select the reason for deactivation and confirm.</li>
        </ol>
        <p>Please note that this will not unregister your email address or phone number. Please contact us if you wish to make a new account with your phone number of email address.</p>
        <div class="back-to-top"><a href="#" class="btn">Back to Top<i class="fas fa-chevron-up"></i>    </a>
        </div>
    </div>
