<aside class="sidebar">
    <nav>
        <div class="profile text-center">
            <img src="./img/profile.jpg" width="75" alt="">
            <div class="welcome">Profile Name</div>
        </div>

        <div class="navigation">
            <a data-toggle="collapse" class="collapsed" data-target="#nav-1">
                <div class="navigation__first">
                    <i class="fas fa-home"></i>Home <i class="fas fa-chevron-right"></i>
                </div>
                <ul id="nav-1" class="collapse navigation__second">
                    <li><a href="#">sub-heading 1</a></li>
                    <li><a href="#">sub-heading 2</a></li>
                    <li><a href="#">sub-heading 3</a></li>
                    <li><a href="#">sub-heading 4</a></li>
                    <li><a href="#">sub-heading 5</a></li>
                </ul>
            </a>

            <li><div class="navigation__first"><i class="fas fa-calendar"></i>Events</div></li>
            <li><div class="navigation__first"><i class="fas fa-users"></i>Members</div></li>
            <li><div class="navigation__first"><i class="fas fa-receipt"></i>Reports</div></li>
            <li><div class="navigation__first"><i class="fas fa-user"></i>Profile</div></li>
            <li><div class="navigation__first"><i class="fas fa-cog"></i>Admin</div></li>
        </div>

        <div class="footer-logo text-center">
            <a href="#"><img src="./img/ZAIKO-logo-text-white.png" alt="" width="150"></a> 
        </div>
    </nav>

</aside><!-- end sidebar -->  