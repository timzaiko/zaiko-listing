<!DOCTYPE html>
<html lang="en">
<head>
  <title>ZAIKO - HOME</title>
  <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
  <link rel="icon" href="/img/favicon.ico">
  <link rel="stylesheet" href="/css/all.css">
  <link type="text/css" rel="stylesheet" href="/css/vone/style.css">
</head>
<body class>

    <div class="wrapper">
        <?php include('sidebar.php'); ?>

        <main>
            <div class="topbar p-4">
                <div class="topbar__left">                    
                    <form class action="">
                        <input type="text" class="form-control" id="faqSearch" placeholder="Search for bands, events, tickets, etc.">
                    </form>
                </div>


                <div class="topbar__right">

                    <div class="dropdown">
                        <button class="btn btn-clear dropdown-toggle" type="button" id="langButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            English
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="langButton">
                            <a class="dropdown-item" href="#">日本語</a>
                            <a class="dropdown-item" href="#">한국어</a>
                        </div>
                    </div>
                    <div class="user">
                        <span class="welcome">Hello, User</span>
                        <a href="#"><img src="./img/user.jpg" width="50" alt=""></a>
                    </div>
                </div>
            </div>

            <div class="breadcrumbs p-4">
                <div class="breadcrumbs__link">
                    <a href="">Home</a>
                    <span class="breadcrumbs__separator">/</span>
                    <a href="">Events</a>
                    <span class="breadcrumbs__separator">/</span>
                    <a href="">Tickets</a>
                    <span class="breadcrumbs__separator">/</span>
                    <a href="">Settings</a>
                </div>
            </div>

            <div class="content">

                <section class="card p-5">
                    <div class="heading flex-space-between card-title">
                        <div>
                            <h1>Events</h1>
                            <em>A quick overview of your upcoming events, go to the events section to list all events</em>
                        </div>
                        <div class="button-group">
                            <button type="button" class="btn btn-outline-primary">All Events</button>
                            <button type="button" class="btn btn-primary">+ Add Event</button>
                        </div>
                    </div>

                    <?php include('pricing.php'); ?>


                    <div class="">
                        <span class="badge badge-pill badge-primary">Primary</span>
                        <span class="badge badge-pill badge-secondary">Not On Sale</span>
                        <span class="badge badge-pill badge-success">On Sale</span>
                        <span class="badge badge-pill badge-danger">Danger</span>
                        <span class="badge badge-pill badge-warning">Sale Over</span>
                        <span class="badge badge-pill badge-info">Info</span>
                        <span class="badge badge-pill badge-light">Light</span>
                        <span class="badge badge-pill badge-dark">Dark</span>

                        <div class="p-5"></div>
                    </div>
                </section>

                <section class="card p-5">
                    <div class="heading flex-space-between card-title">
                        <h1>Members</h1>
                        <div class="button-group">
                            <button type="button" class="btn btn-outline-secondary">All Members</button>
                        </div>
                    </div>

                    <div class="">
                        <button type="button" class="btn btn-primary">Primary</button>
                        <button type="button" class="btn btn-secondary">Secondary</button>
                        <button type="button" class="btn btn-success">Success</button>
                        <button type="button" class="btn btn-danger">Danger</button>
                        <button type="button" class="btn btn-warning">Warning</button>
                        <button type="button" class="btn btn-info">Info</button>
                        <button type="button" class="btn btn-light">Light</button>
                        <a class="btn btn-zaiko btn-lg"><span class="helper"></span><span class="text">Zaiko</span></a>

                        <button type="button" class="btn btn-link">Link</button>

                        <div class="p-5"></div>

                        
                    </div>
                </section>

                <div class="p-5"></div>


            </div>
        </main>

    </div>

    <?php include('footer.php'); ?>
</body>
</html>