<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Seating Chart</title>
	<link rel="stylesheet" href="css/seating.css">
</head>
<body>

	<div id="plan-meta"></div>

	<div id="rows"></div>


	<h2>Sample Seating Plan</h2>
	<div class="row">
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--disabled"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--disabled"></div>
		<div class="row__seat row__seat--open"></div>
	</div>
	<div class="row">
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--disabled"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--reserved"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--disabled"></div>
		<div class="row__seat row__seat--open"></div>
	</div>
	<div class="row">
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--inactive"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--reserved"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--reserved"></div>
		<div class="row__seat row__seat--open"></div>
	</div>
	<div class="row">
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--inactive"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
	</div>
	<div class="row">
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
	</div>
	<div class="row">
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
	</div>
	<div class="row">
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
	</div>
	<div class="row">
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
	</div>
	<div class="row">
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
	</div>
	<div class="row">
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
		<div class="row__seat row__seat--open"></div>
	</div>

	<div class="legend">
		<div class="row__seat"></div> <span>--open</span>
		<div class="row__seat row__seat--selected"></div> <span>--selected</span>
		<div class="row__seat row__seat--inactive"></div> <span>--inactive</span>
		<div class="row__seat row__seat--reserved "></div> <span>--reserved</span>
		<p>*Empty space is defined as seat (--disabled) but has transparent background</p>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="/js/seatChart.js"></script>
	<script src="/js/script-seating.js"></script>


</body>
</html>