var seats = seatChart.seatmap;
var meta = seatChart.meta;

document.getElementById("plan-meta").innerHTML += "<h1>"+ meta.name + "</h1>";
document.getElementById("plan-meta").innerHTML += "<h2>Floor "+ meta.floor + "</h2>";
document.getElementById("plan-meta").innerHTML += "<h3>"+ meta.width + "x" + meta.height + "</h3>";

for (var seat in seats) { // loop for rows
    var seatRows = Object.keys(seats[seat]); // how many seats in row

    document.getElementById("rows").innerHTML += "<div class='row'>";

    for (var seatCount in seatRows) { // loop for seats in row
      var status = seats[seat][seatCount].status; 
      var seatName = seats[seat][seatCount].name;
      document.getElementById("rows").innerHTML += "<div class='tooltip row__seat row__seat--" +status+ "'data-tooltip='"+seatName+"'></div>";
  }
}

$('.row__seat--open').click(function(){
    $(this).toggleClass('row__seat--selected', 'row__seat--open');
});