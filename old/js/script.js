var seatChart = {
    "meta":{
        "name" :"section 1",
        "floor" : "1",
        "width" : 400,
        "height" : 300
    },
    "seatmap" :{ 
        "A" : [
        {
            "status" :"disabled",
            "name" : "&nbsp;"
        },
        {
            "status": "open",
            "name" : "A1"
        },
        {
            "status": "inactive",
            "name" : "A2"
        },
        {
            "status": "open",
            "name" : "A3"
        }    
        ],
        "B" : [
        {
            "status" : "reserved",
            "name" : "B1"
        },
        {
            "status": "open",
            "name" : "B2"
        },
        {
            "status": "open",
            "name" : "B3"
        },
        {
            "status": "open",
            "name" : "B4"
        }        
        ],

        "C" : [
        {
            "status" : "open",
            "name" : "C1"
        },
        {
            "status": "inactive",
            "name" : "C2"
        },
        {
            "status": "open",
            "name" : "C3"
        },    
        {
            "status": "inactive",
            "name" : "C4"
        }    
        ]
    }
};

