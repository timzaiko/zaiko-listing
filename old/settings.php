﻿<!DOCTYPE html><html class="mdl-js" style=""><head>
	<base href="https://zaiko.io/my/settings.php">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>ZAIKO  / Settings</title>
	<!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" id="viewport">
	<meta name="theme-color" content="#000000">
	<link rel="apple-touch-icon-precomposed" href="/img/zaiko_icon.png">
	<link rel="shortcut icon" data-savepage-href="/img/zaiko_icon.png" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAaEAAAGhCAYAAADIqAvCAAAABmJLR0QA8AABAAHHsrGDAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH4gUVCSAmcCeiJAAAHjBJREFUeNrt3e91Gsm6xeHyrPneygA5AsigcQSQAXIE4AjAEYAjgIlAKAJQBEIR0IoAFAH3wx3NGXtmbBAN3VX9/Nba6945d44v7n+7atdbb33I8/wQAAC4Mp1OJ/z++PjoSgAAKuE3lwAAwIQAAEwIAAAmBABgQgAAMCEAABMCAIAJAQCYEAAA7+V3lwD4OVmWhU6nE25ubkKn0/npv7vf78Nmswn7/T48Pz+7eAATAk4znG63G7rdbuh0On+Zz3spiiJsNpuwXq/Der1mTMAPfAghaGCKRtNut8Pd3d1fxnNJ9vt9WC6XYb1eh+VyGV5fX90ANJY8z5kQmkmr1Qp3d3fh7u4u3N7eVvIb3gxpsVgEjYTRVBMKf5oQUSOU5/lhPp8f6sZ2uz0MBgP3iBr3PjIhaszDvlqtDnWHGRETIkpI7XY7CvP5NzPq9XruISVvQvYJIUmyLAvT6TRsNpvQ7Xaj+/23t7dhuVyG1WoVWq2WG4pkYUJIjuFwGIqiCKPRKPq/S7fbDUVRhOl0GrIsc3ORHKrjkAx5nofFYnGxare3jah/pyiKf/z/u9TMa7/fh8lkEr59++ZmI5l3lgkhelqtVpjNZqHf75f2Z75tMN1sNqEoipNLqN+6LLztPep2u2dtev3xt41GI2XdSMKEQrA4RpEqy7LDeDwurRjg/v7+MBgMDq1W6yK/t9frHebz+WG325X2ey/1W4mC6jii/9ZgMCjlY77dbg/D4fCQZdnVf39ZVXvj8fjqv5+ICVFjH9qnp6dk9uOUtX9pt9vZX0RMiOhSarVapXQ6qOtm0LJmdqvV6u3FJmJCRGWt+5Txga57bFXmGtd8PrdeREyI6NzZwXa7bdwCfqvVKi2is15ETIioolY7T09PUUdTeZ6XYsJaABETIjoyjipj3Se1RfrhcFjaelG73fasERMi+lFlrftMp9Mk46csyw7T6bS09SIRHTEhohIjp9Vq1YiF+LKiyrf1Is8gMSFqbMl1GR/T7XbbyJLkXq9X2nqRkm5iQtSodZ8yYqXdbncYDodizJJizKbMJIkJUYNV1gK7NY3LbORNeU2NmBA1/EEro9WO6q5fX2ctgIgJEf1thH5/f2+fS6SbfGPfZ0VMiBq87lNG+xk7/qu/B46MICZE0Y3Cy1r38eGrz2zUkRHEhKgR6xG6QNd7Xa6uXciJCZHKLIvhDZqpGiwQE6JarDk04YgFe7XEpsSEKMHd+ha80+haoQUQMSG6ihyxkO6HwJERxISo1vFNWes+Wu3UV2W2ALKpmJgQlaKyWu1oB9OsAYd7TkyIahHRaIzZ7OjV7JeYEFWyWO2IAEUongdiQnT1sl2VUsrxzYyJCVElGxgdsWBjsr1hxITo6q1cVEM177lxZAQxITprRFvWEQs+ImbQ9o0RE6KTsn1HLFDdnikdNJgQNWDUWkaVk15hdMnZtQEOEyL5vciEKt9fJuplQmTnu8VjqrzThqIXJkQRyhELlNLeM+X/TIgatrvdAjHVsQuHjdBMiGqqsvp8aa1CMQySHBnBhCixuEOTSYoxLtYCiAlRAgu/2u1TzIUznmEmRJGWwKo6opSiZLN5JkSRLO7K0ynlzdTWNZkQ1bQtisoiiuVZLyNmVuHJhKik0aEjFqiJs35HRjAhqvjGlHXEgmiCmv4e6PrBhOjKI0B9t0gioP8hE6KrZ+E6EJO1UZ3gmRBVUhVkIZZUiRqwMSG6+v4IEQPZL2fLAhOiq+8Ut9hK5MgIJkQnqaw9ENqUEH0/sHNkBBOin6is7sEaNhJdPuK2sZsJWUTVioSo8kGf944JNT4e0JSRqPr4WwLBhBq5UCqbJqpPIZC1WCbUmJJRVTpEl3k/HRnBhJJd97m/v7dfgahBm8Ptz2NCybQRUYlDFOe7q1MJE6p0NFXWuo8HmCjuFMOREUwoulzZEQtE9XqvyzgyQvd6JnTREZMjFogkHAaZTKiS7LiMB9N0nag5e/zE7UyoNruuLVwSNbfbicIjJnSyHLFARGXv/7MFgwldbWe1zWxEOqHYjM6ETpIjFojoWgNVbbm+N6EPf/5DI8nzPCwWi3B7e3vWn7Ner8Pd3V14eXmp/O/T7XYD0CQWi8VV3712ux1ms9nZ79p+vw+TySR8+/atsfcuz/PQyJlQqkcslLULHIiJqt5BR0aUMxP6rUmum2VZmE6noSiKs0YxbyOYjx8/hsfHR0NRoIE8PDyETqcTJpNJ2O/37/5zbm9vw3q9DqvVKrRarUZeSxvREslyy2pBApgJVbOhvWl7ChsxE8rzPDw9PYXFYhFubm7OWvfpdDrh8+fP4fX1tZZ/13P+fkCsnDMLKYuXl5fw+fPn0O12w3q9PuvPmkwmoSiKMBgMGnH/ki1MaLVaYTabhX6/f9afUxRFGI1G4eHhofZ/59VqpTChZM79oMRITM/QbDYLX758qd3vGgwGYTabnT0w3Gw2YTQaJRv753mepgmNx+MwGo3OegD2+32YzWZhNpvVdubDhE4zkv1+HzabzT/+97fBRtXVjXVgOByG2WwWxW8tiiJ0Op3avp9ZloXRaBQmk8nZf9ZisQij0Siab9EpJhRCYt0OyuiGG2vPp6ayWq0O9/f3h/F4fBgOh4c8z20IfOe6Rhnrpk1aCzr2upaxXrvb7ZLrupDUZtUyCg9ib7WTOk9PT4f5fH4Yj8eHPM/15CtZZWxbuBbz+byxR0ZMp1MmFBLbxbzb7ZI4YiElttvtYT6f/zWzYRKXb0sTC7vdLurqsTJaAD09PSUxCIvehLIsO3tkkVI5ZMzsdrvD/f39YTAYmOGI4X5KCpFUGUdG7Ha76GPnqE2o3W6ftVs5tSMW8jyP0njm87nuwmK4k97b1AYA51z/2FOcaE2o3W6/e+SWaouMmEyI8YjhmhjDhQu2AIrViKI0ofcaUOpHLNTdhLbb7WE4HOocLIZrdAz3K53T0T9GI4rOhN5rQKvVKvl1hl6vV9vyabMeMZwY7rT1oveWdMdmRFGZUJZl7zKgphwwV7cO2qvVSlVbzbc0iOHqP7B8zzcvpvcuGhN6TxVcCpUjMZoQ80l3QCeGqyYyTfnbF40JnboP6OnpqXEjp6pNaLvdit0iUUzd1psUw5W5FzKWb2AUJnRq9U4TDSiEcPaeA3utmhPxiOHi1KlGFIOB196E2u02A6rxInMTCj7EcGK4mI2o7mvitTehU7LQJhtQFSbUlIIPMVx1a4vu2flGtNvtaj1QrLUJnbLG0XQDuqYJPT096VAthrt4DGeGXZ4R1dnQa2tCrVZLFdyJOme39SmdDmT0Yjiz7PiSorruH6qtCZ0yqpcbX6d5aQpdxsVwYrimDi7qWuRRSxM6pf1MSudq1NWEttutmaYYTgyXQLuu8XjMhMqcBW23W9HQhU3IWpsYTgyXzvaMOs6GamdCp7i6XfnvW0M7Zf3HtRXDieHiGGwcuyZct9lQ7Uzo2FmQXdTvN28GJIYTwzX3ftdtNlQrEzplY6oH9nImVMfcmMRwVN4gvk7Xu1YmdGzdu1H65UxIBZwYTgyX/ndgu90yoXNGbmZB5/fXU2kolhHDNXs2VJc19TzPD7+FGtDv98PNzc0v/731eh1eXl4CvueYa/czFotF+PLliwsZOVmWhcViEc3vnUwm3ueSmc1mR/17d3d3tfrd0cQHNqae3+JIkYcYrg48PT25ZxV2T9ntdmZCfx+99fv9X/57+/0+PDw8GOqUyGazqd2ICO+j1+sd9R7VBc/d5ThmNnxzcxN6vV4tfm/lJtTtdo/695bLpafrP+h0Oif/d/b7fej3++H19dUFFMNdPYZ7fn524yo0oVO+vUyICR01qnnPSFQen85H59x1wWvOvr9+/eqmXZCXl5ew2WyYUNkmJIorj9ls5nomghgO7x20dzqdkGUZEzomSlqv156qEkeik8nEhUgAMRzO/Wa+J8pPyoTyPC/1gjaVU6bVo9HIOlAiiOHwXzw+Ppb+7UjShI514WPyTfya2Wx29MOJeiOGQxmD98bPhI4dxRVF4Yk6k/1+L4ZLBDEcyhq812EmXakJHTsV9ACfjxguHcRwOHbgWdY3OFkTwvkcs65WFEX4448/XKwEEMPhWGJZS6/9mpCihPM5tp8U6k1sMdxsNpNiREC73W6uCcUSKcQ+JY/pw4X/ZjKZRPPOFEVhDbIG9yCG77A4LnJ+9QAtFgtrQQmQ53kYjUbR/N67uzvPXcXE0hGFCUXOryJNUVz8xBjD2QoAJgTnLyXCZDIJt7e3UfxWMRyYEP7CWlD8iOHAhFBrfrYmpPN43IjhwIRQe/5rTWi5XBqRRo4YDkwI0WIWFDdiODAhMCFUghgOTAjR8G9xzXq9NiqNGDEcmBCiNiGzoHgRw4EJIXr024uT2GK45XIphgMTwvfs93tNIyMlphhuv9/rkA0m1HRardY//jOn0MaJGA5MCNHxX0UJiIsYY7iHhwc3DkwIgQklgBgOTAjJII6LCzEcmBCi5ceWPUVR+EBEhBgOYEJR82Pz0mNPUkQ9EMMBTCgprAfFgxgOYELJsd/vXYQIEMMBTCgJvn79Gj58+BA+fPgQut2uQ+wiQQwH/I/fXYI00D4lDmKL4UajkRgOZkJACsQWw63X6/DHH3+4cWBCQAqI4QAmBFRCbDHcZDIJLy8vbhyYEBA7McZw3759c+PAhIAUGI1GYjiACQHXp91uR3X8tRgOTAhICDEcwISAShiPx/9oMltXxHBgQkBCiOEAJgRUhhgOYEJAJYjhACYEVEJsMdxsNhPDgQkBqRBTDLfZbMLXr1/dNDAhIAViiuFCCGI4MCEgFWKshnt+fnbjwISAFBDDAUwIqAQxHMCEgEoQwwFMCKgMMRzAhIBKEMMBTAioBDEcwISAyhDDAUwIqAQxHMCEgEqIsTecGA5MCEiEmGK4oiiiMkwwIQA/IcYY7vX11Y0DEwJiJ8YY7vHx0Y0DEwJSQAwHMCGgEsRwABMCKkEMBzAhoDLEcAATAipBDAcwIaASWq2WGA5gQkA1iOEAJgRUwnA4DN1uN5rfOxqNxHBgQkAKxBbDLZfL8PDw4MaBCQEpsFgsws3NTRS/db/f65ANJgSkQmwxnGo4MCEgEcRwABMCKkMMBzAhoBLEcAATAipBDAcwIaAyxHAAEwIqQQwHMCGgEsRwABMCKkMMBzAhoBJii+Emk4kYDkwISIHYYrj1eh2+ffvmxoEJASkghgOYEFAJMcZwLy8vbhyYEBA7YjiACQGVIYYDmBBQCWI4gAkBlSCGA5gQUBliOIAJAZUghgOYEFAJYjiACQGVEVMMF0IQw4EJAakghgOYEFAJscVwm80mfP361Y0DEwJSYDabieEAJgRcn16vF/r9fjS/dzKZhOfnZzcOTAiInSzLwmKxiOb3iuEAJoSEUA0HMCGgEsRwABMCKkEMBzAhoDLEcAATAipBDAcwIaASxHAAEwIqI7YYbjQauWkAE0IKxBbDzWaz8Pj46MYBTAixE1sMVxRFVL3sACYE/IQYq+FeX1/dOIAJIXbEcAATAipBDAcwIaAyxHAAEwIqQQwHMCGgEsRwABMCKkMMBzAhoBLEcAATAipBDAcwIaAyYuwNJ4YDmBASILYYbrlchoeHBzcOYEKIndhiuP1+76A6gAkhFVTDAUwIqAQxHMCEgEoQwwFMCKgMMRzAhIBKyPNcDAcwIeD6iOEAJgRUxmQyCbe3t9H8XjEcwISQCHmeh9FoFM3vFcMBTAiJEGMMF5NhAkwI+AmxxXCTySS8vLy4cQATQuzEFsOt1+vw7ds3Nw5gQogd1XAAmBAqQwwHgAmhEsRwAJgQKkEMB+Df+N0lwDUQw+GStFqt7wYNRVGEP/74w4WJhENVOobVanWo8jfS+crz/BATnrn4NBwOv7uH0+nUdTnyG5vneaXfBnEcxHBiuOjpdDr/uI+IAyYEMZwYLnq63a6LwISA71ENh2vNtn8c6JgJMSH4MEQVw4UQ9IZLaBa02WxcGCaEJhNjDPf8/OzGJWJCYEJoMLHFcJvNJnz9+tWNY0JgQoidGGM41XBxP28/Vsa9DSzAhNBAxHCowyzI6bdMCA1EDIdr0+/3XQQmBIjhUJ+ZkPJsJoQGIobDtWm32//6zFkPYkJoGGI4mMmCCaESxHCoCutBTAgQw6ES/iuKe5vpggmhAcQWwxVFIYZLhJ/NZhUmMCE0hNlslsyHC8G9BBNCPIzH43/dqV5nw3x8fHTjEqDX64WbmxszISaEptJut8NkMonm9xZFEdXvxc/5VQRsTYgJIXFirIbTxiUNWq2WhqVMCE1GDIcqMaNlQmgwYjhUPQs6piChKAoXiwkhRcRwiGEW9PLy4mIxIaSGGA4xzILAhJAgYjjEMgsCE0KCiOEQyyxovV67YEwIKSGGQx3uKZgQGkhsMdx+vxfbJEae57plMyE0FTEcqubUQYXybCaERIgthlsul+Hh4cGNS4jBYHBydwQmxISQADHGcMp30yLLMmtBTAhNRQyHqplMJj/tlI1f02q1mFAZxHRqZwqI4VA15xyWqEQ7vm9npSZ0zAPDhK6HGA51QAx3Xare0hBFHBfLtDJ2xHAwE0+HWI68qNSEjq1kMRvy8v+IGM5M/JxvShM4Zk2tDtcrChNyiFX9X/5rIoZLjyzLwnK5PPvP0UH7fxwzqGy8CR27iGh6flnEcKia2Wwm8SiZYwbvdSjkMBNqOGI4VM1gMDCzLZl2u13qN/jSHKrUdrs9HEO73T5U/VtTU7vdPsTEbrc7tFot984z+K+sVivX9E+Nx+OjrlnV71Oe54fKq+M2m81R/54mhmK4yWQi80+IstaB8L7v5X6/r8X7VLkJHfsQMqFmx3Dr9Tp8+/bNjUuI9XptHegCtFqto97tugwAfqvDg3gMnU7n6JwTP0c1HKpmPp+XPghSnn3agL1O3SUqzy+fnp6Oyi/n87m894rXuy4Mh0P3LSHN5/OLPCfj8dj1PWGdPcuyyn9rLdaEQjh+baLf74csywx1zmA4HIrhUBkq4S5LnudHRZzL5bI22xxqYULHZpM3NzfvbmyI/8+KxXCo0oBiK4aJjWPf77oVhNRiCrlarY4u063DNDJGHXuNxXBUtgaDwcWflzzPG32N8zyP7htamzjulEjObOj9MVxMm37FcGZAuNwsqG4dR6JbUDMbOk2tVuuw2+1sSqXabpo0EzpPvV7v6OtUp3frz3sW5wN7f3/vJRfDUQOr4GL4uF5TWZYdPYCvW1eJ2plQlmUnjdh7vZ6X/RcaDodRGdDT05P7lsBHsYqBT1Ov93Q6jXa2WDsTOnU2JJZLK4bTIzCNXnBV7UNTjBBfb71amtApa0OaFqYVw9lsGH8FXJWDnibOOE+53nWMK2trQqcssvl4ieGo+o/htdd/mj4YzbLspBnndDqt7UwupDKKHwwGPghiOKrgI3JKcsGErl/0Uedli1qb0Hs+pIxIDEfXG4mfsiDOhKqrOqxzAVetTei9kVLTjUgMR9eIy+sw+2lic+NTDajuW1lqb0IhhMP9/T0jEsNRTSrf6jrLbsJs+lQD2m63ta8ejsKETtmI1fQNj2I4utTgpurCgyY/S+/ddxXDAC8KE3obgb1nhD+fzxuzj0gMR5d47+puPqmbULvdftcgPJY0KBoTek/Z9t8/dqlHPmI4Kvtdi21WnWLfuPcOLOtajh29Cb1thjtnpJTqrEgMR2UMZMbjce0KDppoQq1W693vdGwFGtGZ0LlGtN1ukytaiC2Gi2GxtEll1oPBILpBTKomlGXZWV3HY6wQjNKEzjWitz0FKcRBMcZwTT94rA7PzGAweFfVqXi3vi2PYi1Rj9aEyjCiFAoXYhvBxpRVp9bVYDweV9ZY9BrEfG/OvS8x75GK2oTKapi42+2iXKMQw9GvTCeFmC1VEyqr7D32gV30JnROCeO/fSRjOZ9IDEd/f4mHw+FhPp8nPdNJxYTe1n3KGDynsL6dhAmds5nrv9aL6n5CoxiumWbT6/UO4/H4cH9/32jD+bdtGLEkN2UNmFPZ4pCMCV0ioppOp7WMj8Rw6evNaFarVXQz3iqoe/PSMtsdpbYBPzkTervhZY0Sd7tdrdr/iOGaIaRhQmWes7Tb7aJZLmi8Cf19xlDWB/vp6akWH9MYF5qZChO6NHXsFF3Guk/dUxkmdORIpMwzT+7v7ytbL4othmNCTOha1Km6tcxD/mJYny7jev0WEub19TV8+fIldDqdsF6vz/7z+v1+KIoijMfjkGXZVf8u/X4/AKgnrVYrrFarsF6vw+3t7Vl/VlEUod/vh0+fPoWXl5fkr91vTXhAnp+fw6dPn/4ykXOZTCahKIowGAy8fUCDybIsTKfTUBRF6Ha7Z/1Z+/0+TCaT8PHjx/Dw8NCo69jI6qOy8trVanWV9aJYNx6K18Rxl6aqBfsy152bdOzMj3Hchz//oZEjmNlsFu7u7kr58xaLRZhMJhebPk+n09DpdKK7zp8+fTJcPpHxeOwinPjuXTO2yvM8zGazUt7H9XodRqNReH5+buS9y/O8mTOhH524rFnGWwsg+2KI0mz+Wlbj1xQ7+gfVcfXYzfz2gKVY00/URJ17xIKBKhOq5GF7Wy9ygihRs5sk12GLBxNq8LS7yYuORGL6+mx2Z0INPevjx2m460qU/hELb++8dR8mVLsp+Xa7NSoiSvSIhb93cZB+MKFatwBqQksOohjU6/W02mFCcU3Xy9w4mnJzQqI6q8wjFiQcTCjqRoWyY6J4j1io03EvTKiBSvHICCLvqxSDCTV0ZGU/AVG9kwv7/5hQIzJmFTZE9VrD1QmFCTWy2kaPKaJqq1nt8WNC9h1c8cgIIvv6vu92IhpnQnZgeymIrtrhxKCPCSV5scs+MsJ1JXLEAhOiyqICC6Uk8h4rBGJCVPXLo2SUmjiYK6v4x5YIJiRG0AKI6Oqxts3hTIgusKCqjQjZEK5NFhOiyluKaKhIqajMrQ7SAiZEV9xkp7U82fTtPWBCVHm7EZU/FJMcscCEqGY3yZERJAWwl44JUTK5uKogSnk9dD6fm/UzIap7hZD9EZTaTN9+OSZEEWbmb7GFkSPFuuapcwgTogR2jeuZRbF1CzGAYkJUo5e6zCMjRBp0qUGTIxaICSUcb5R9ZIQRJtWtI4gjFpgQNeiFV+ZKdRkY2V7AhKjB0YeFX6oyIrbuw4TIIrDWJ3T1YhlbCJgQJRSLODKCYtk2YDM1EyIbAx0ZQVffQO2ZYkKkRYoGkXS0HLFATIjOGsGWeWSE/L45csQCMSGqZZavkkmrHRWXxISo8tGtPR1mzfaeEROiynN+u9utH+rCQUyIKq140udL5w39CIkJ0bsfEEdG2FOmMzsxIUpmF7wF6fS7axhwEBOii32kHBmR5iDDEQvEhKhxcY3F6nTiVq12iAlRtAvXynavP5BwxAIxIRLlaAEUdaRq3YeYECW3qK2Vi1Y7xISIKm/roqllOSqzLZOZKjEhiubB0gIonQ3HjlggJkSNb/mi+qqa6242SkyIjMgdGXH1Gah9XMSEyNqEyqyrr8XpaEFMiFRp6U129CzTEQvEhIjeIUdG1KvVjlklMSGyc1/fsqt2qnDeEzEhonCZIyNSNGxHLBATIrpwxOTIiMt1o3DEAjEhoit+dGMvNS7TlJW2ExMiqih+im3x3RELxISIElyIr3v7GUcsEBMiakArmrotzJd9xIJWO8SEiGq+ObMOZtRqtUrfL2Xdh5gQUURtat6iq+l0erUChl6vV+p6lyMWiAkRVfQAl1U99vcP+nw+P/R6vdIirXa7fRgOh6UajyMWKJV3+MOf/wBEy3g8DqPRKNzc3JT+ZxdFEYqiCOv1Ouz3+7DZbL77v282m9DpdL77z7rdbri5uQmdTid0Op2L/K7FYhFGo1F4fX31ACBa8jwPZkKUzHpRmS2A6oojFii1mdBvvBgp8Pr6Gj5//hw6nU5Yr9fJ/f2Kogj9fj98+vQpPD8/u+FIBiaEpHh+fg6fPn0K/X4/FEUR/d9nv9+HyWQSPn78GB4eHtxgJIlpISXdAqisEuhr04Su4CSOY0LEjGq47qPkmpgQUYJmNBwOSy/rLnPmo+iAmmZCSrTRSHq9Xuj3++Hu7q7S31EURZjNZmG5XIaXlxc3Bo0iz/PAhNBosiwL3W439Pv90O/3L7Kn50c2m01YLpdhuVyqdAMTYkLA/2i326Hb7f610fTHjain8rbBdb1e//U/bTAFmBBwNK1WK9ze3v6lY2Y6b+bDcAAmBACoqQnZrAoAqAwmBABgQgAAJgQAABMCADAhAACYEACACQEAwIQAAEwIAIB3839vw5khOfh/BAAAAABJRU5ErkJggg==">
	<meta property="fb:app_id" content="285617126486">
	<script type="text/javascript" async="" data-savepage-src="https://www.google-analytics.com/analytics.js" src=""></script><script type="text/javascript"></script>
	<script type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" data-savepage-src="/lib/shadowbox/shadowbox.js" src=""></script>

	<script type="text/javascript" data-savepage-src="/js/color_functions.js?vs=1" src=""></script>
	<script type="text/javascript" data-savepage-src="/js/swfobject.js" src=""></script>
	<script type="text/javascript" data-savepage-src="/js/swfobject2.js" src=""></script>
	<script data-savepage-src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js" src=""></script>
	<script type="text/javascript" data-savepage-src="/js/jquery-migrate-1.2.1.min.js" src=""></script>
	<script type="text/javascript" data-savepage-src="/js/jquery.textarea-expander.js" src=""></script>
	<script type="text/javascript" data-savepage-src="/js/jquery.ba-dotimeout.min.js" src=""></script>
	<script type="text/javascript" data-savepage-src="/js/jquery.lazyload.min.js" src=""></script>
	<script type="text/javascript" data-savepage-src="/lib/featherlight/featherlight.js" src=""></script>




        <style data-savepage-href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">/*! jQuery UI - v1.11.4 - 2015-03-11
* http://jqueryui.com
* Includes: core.css, accordion.css, autocomplete.css, button.css, datepicker.css, dialog.css, draggable.css, menu.css, progressbar.css, resizable.css, selectable.css, selectmenu.css, slider.css, sortable.css, spinner.css, tabs.css, tooltip.css, theme.css
* To view and modify this theme, visit http://jqueryui.com/themeroller/?ffDefault=Verdana%2CArial%2Csans-serif&fwDefault=normal&fsDefault=1.1em&cornerRadius=4px&bgColorHeader=cccccc&bgTextureHeader=highlight_soft&bgImgOpacityHeader=75&borderColorHeader=aaaaaa&fcHeader=222222&iconColorHeader=222222&bgColorContent=ffffff&bgTextureContent=flat&bgImgOpacityContent=75&borderColorContent=aaaaaa&fcContent=222222&iconColorContent=222222&bgColorDefault=e6e6e6&bgTextureDefault=glass&bgImgOpacityDefault=75&borderColorDefault=d3d3d3&fcDefault=555555&iconColorDefault=888888&bgColorHover=dadada&bgTextureHover=glass&bgImgOpacityHover=75&borderColorHover=999999&fcHover=212121&iconColorHover=454545&bgColorActive=ffffff&bgTextureActive=glass&bgImgOpacityActive=65&borderColorActive=aaaaaa&fcActive=212121&iconColorActive=454545&bgColorHighlight=fbf9ee&bgTextureHighlight=glass&bgImgOpacityHighlight=55&borderColorHighlight=fcefa1&fcHighlight=363636&iconColorHighlight=2e83ff&bgColorError=fef1ec&bgTextureError=glass&bgImgOpacityError=95&borderColorError=cd0a0a&fcError=cd0a0a&iconColorError=cd0a0a&bgColorOverlay=aaaaaa&bgTextureOverlay=flat&bgImgOpacityOverlay=0&opacityOverlay=30&bgColorShadow=aaaaaa&bgTextureShadow=flat&bgImgOpacityShadow=0&opacityShadow=30&thicknessShadow=8px&offsetTopShadow=-8px&offsetLeftShadow=-8px&cornerRadiusShadow=8px
* Copyright 2015 jQuery Foundation and other contributors; Licensed MIT */

/* Layout helpers
----------------------------------*/
.ui-helper-hidden {
	display: none;
}
.ui-helper-hidden-accessible {
	border: 0;
	clip: rect(0 0 0 0);
	height: 1px;
	margin: -1px;
	overflow: hidden;
	padding: 0;
	position: absolute;
	width: 1px;
}
.ui-helper-reset {
	margin: 0;
	padding: 0;
	border: 0;
	outline: 0;
	line-height: 1.3;
	text-decoration: none;
	font-size: 100%;
	list-style: none;
}
.ui-helper-clearfix:before,
.ui-helper-clearfix:after {
	content: "";
	display: table;
	border-collapse: collapse;
}
.ui-helper-clearfix:after {
	clear: both;
}
.ui-helper-clearfix {
	min-height: 0; /* support: IE7 */
}
.ui-helper-zfix {
	width: 100%;
	height: 100%;
	top: 0;
	left: 0;
	position: absolute;
	opacity: 0;
	filter:Alpha(Opacity=0); /* support: IE8 */
}

.ui-front {
	z-index: 100;
}


/* Interaction Cues
----------------------------------*/
.ui-state-disabled {
	cursor: default !important;
}


/* Icons
----------------------------------*/

/* states and images */
.ui-icon {
	display: block;
	text-indent: -99999px;
	overflow: hidden;
	background-repeat: no-repeat;
}


/* Misc visuals
----------------------------------*/

/* Overlays */
.ui-widget-overlay {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
}
.ui-accordion .ui-accordion-header {
	display: block;
	cursor: pointer;
	position: relative;
	margin: 2px 0 0 0;
	padding: .5em .5em .5em .7em;
	min-height: 0; /* support: IE7 */
	font-size: 100%;
}
.ui-accordion .ui-accordion-icons {
	padding-left: 2.2em;
}
.ui-accordion .ui-accordion-icons .ui-accordion-icons {
	padding-left: 2.2em;
}
.ui-accordion .ui-accordion-header .ui-accordion-header-icon {
	position: absolute;
	left: .5em;
	top: 50%;
	margin-top: -8px;
}
.ui-accordion .ui-accordion-content {
	padding: 1em 2.2em;
	border-top: 0;
	overflow: auto;
}
.ui-autocomplete {
	position: absolute;
	top: 0;
	left: 0;
	cursor: default;
}
.ui-button {
	display: inline-block;
	position: relative;
	padding: 0;
	line-height: normal;
	margin-right: .1em;
	cursor: pointer;
	vertical-align: middle;
	text-align: center;
	overflow: visible; /* removes extra width in IE */
}
.ui-button,
.ui-button:link,
.ui-button:visited,
.ui-button:hover,
.ui-button:active {
	text-decoration: none;
}
/* to make room for the icon, a width needs to be set here */
.ui-button-icon-only {
	width: 2.2em;
}
/* button elements seem to need a little more width */
button.ui-button-icon-only {
	width: 2.4em;
}
.ui-button-icons-only {
	width: 3.4em;
}
button.ui-button-icons-only {
	width: 3.7em;
}

/* button text element */
.ui-button .ui-button-text {
	display: block;
	line-height: normal;
}
.ui-button-text-only .ui-button-text {
	padding: .4em 1em;
}
.ui-button-icon-only .ui-button-text,
.ui-button-icons-only .ui-button-text {
	padding: .4em;
	text-indent: -9999999px;
}
.ui-button-text-icon-primary .ui-button-text,
.ui-button-text-icons .ui-button-text {
	padding: .4em 1em .4em 2.1em;
}
.ui-button-text-icon-secondary .ui-button-text,
.ui-button-text-icons .ui-button-text {
	padding: .4em 2.1em .4em 1em;
}
.ui-button-text-icons .ui-button-text {
	padding-left: 2.1em;
	padding-right: 2.1em;
}
/* no icon support for input elements, provide padding by default */
input.ui-button {
	padding: .4em 1em;
}

/* button icon element(s) */
.ui-button-icon-only .ui-icon,
.ui-button-text-icon-primary .ui-icon,
.ui-button-text-icon-secondary .ui-icon,
.ui-button-text-icons .ui-icon,
.ui-button-icons-only .ui-icon {
	position: absolute;
	top: 50%;
	margin-top: -8px;
}
.ui-button-icon-only .ui-icon {
	left: 50%;
	margin-left: -8px;
}
.ui-button-text-icon-primary .ui-button-icon-primary,
.ui-button-text-icons .ui-button-icon-primary,
.ui-button-icons-only .ui-button-icon-primary {
	left: .5em;
}
.ui-button-text-icon-secondary .ui-button-icon-secondary,
.ui-button-text-icons .ui-button-icon-secondary,
.ui-button-icons-only .ui-button-icon-secondary {
	right: .5em;
}

/* button sets */
.ui-buttonset {
	margin-right: 7px;
}
.ui-buttonset .ui-button {
	margin-left: 0;
	margin-right: -.3em;
}

/* workarounds */
/* reset extra padding in Firefox, see h5bp.com/l */
input.ui-button::-moz-focus-inner,
button.ui-button::-moz-focus-inner {
	border: 0;
	padding: 0;
}
.ui-datepicker {
	width: 17em;
	padding: .2em .2em 0;
	display: none;
}
.ui-datepicker .ui-datepicker-header {
	position: relative;
	padding: .2em 0;
}
.ui-datepicker .ui-datepicker-prev,
.ui-datepicker .ui-datepicker-next {
	position: absolute;
	top: 2px;
	width: 1.8em;
	height: 1.8em;
}
.ui-datepicker .ui-datepicker-prev-hover,
.ui-datepicker .ui-datepicker-next-hover {
	top: 1px;
}
.ui-datepicker .ui-datepicker-prev {
	left: 2px;
}
.ui-datepicker .ui-datepicker-next {
	right: 2px;
}
.ui-datepicker .ui-datepicker-prev-hover {
	left: 1px;
}
.ui-datepicker .ui-datepicker-next-hover {
	right: 1px;
}
.ui-datepicker .ui-datepicker-prev span,
.ui-datepicker .ui-datepicker-next span {
	display: block;
	position: absolute;
	left: 50%;
	margin-left: -8px;
	top: 50%;
	margin-top: -8px;
}
.ui-datepicker .ui-datepicker-title {
	margin: 0 2.3em;
	line-height: 1.8em;
	text-align: center;
}
.ui-datepicker .ui-datepicker-title select {
	font-size: 1em;
	margin: 1px 0;
}
.ui-datepicker select.ui-datepicker-month,
.ui-datepicker select.ui-datepicker-year {
	width: 45%;
}
.ui-datepicker table {
	width: 100%;
	font-size: .9em;
	border-collapse: collapse;
	margin: 0 0 .4em;
}
.ui-datepicker th {
	padding: .7em .3em;
	text-align: center;
	font-weight: bold;
	border: 0;
}
.ui-datepicker td {
	border: 0;
	padding: 1px;
}
.ui-datepicker td span,
.ui-datepicker td a {
	display: block;
	padding: .2em;
	text-align: right;
	text-decoration: none;
}
.ui-datepicker .ui-datepicker-buttonpane {
	background-image: none;
	margin: .7em 0 0 0;
	padding: 0 .2em;
	border-left: 0;
	border-right: 0;
	border-bottom: 0;
}
.ui-datepicker .ui-datepicker-buttonpane button {
	float: right;
	margin: .5em .2em .4em;
	cursor: pointer;
	padding: .2em .6em .3em .6em;
	width: auto;
	overflow: visible;
}
.ui-datepicker .ui-datepicker-buttonpane button.ui-datepicker-current {
	float: left;
}

/* with multiple calendars */
.ui-datepicker.ui-datepicker-multi {
	width: auto;
}
.ui-datepicker-multi .ui-datepicker-group {
	float: left;
}
.ui-datepicker-multi .ui-datepicker-group table {
	width: 95%;
	margin: 0 auto .4em;
}
.ui-datepicker-multi-2 .ui-datepicker-group {
	width: 50%;
}
.ui-datepicker-multi-3 .ui-datepicker-group {
	width: 33.3%;
}
.ui-datepicker-multi-4 .ui-datepicker-group {
	width: 25%;
}
.ui-datepicker-multi .ui-datepicker-group-last .ui-datepicker-header,
.ui-datepicker-multi .ui-datepicker-group-middle .ui-datepicker-header {
	border-left-width: 0;
}
.ui-datepicker-multi .ui-datepicker-buttonpane {
	clear: left;
}
.ui-datepicker-row-break {
	clear: both;
	width: 100%;
	font-size: 0;
}

/* RTL support */
.ui-datepicker-rtl {
	direction: rtl;
}
.ui-datepicker-rtl .ui-datepicker-prev {
	right: 2px;
	left: auto;
}
.ui-datepicker-rtl .ui-datepicker-next {
	left: 2px;
	right: auto;
}
.ui-datepicker-rtl .ui-datepicker-prev:hover {
	right: 1px;
	left: auto;
}
.ui-datepicker-rtl .ui-datepicker-next:hover {
	left: 1px;
	right: auto;
}
.ui-datepicker-rtl .ui-datepicker-buttonpane {
	clear: right;
}
.ui-datepicker-rtl .ui-datepicker-buttonpane button {
	float: left;
}
.ui-datepicker-rtl .ui-datepicker-buttonpane button.ui-datepicker-current,
.ui-datepicker-rtl .ui-datepicker-group {
	float: right;
}
.ui-datepicker-rtl .ui-datepicker-group-last .ui-datepicker-header,
.ui-datepicker-rtl .ui-datepicker-group-middle .ui-datepicker-header {
	border-right-width: 0;
	border-left-width: 1px;
}
.ui-dialog {
	overflow: hidden;
	position: absolute;
	top: 0;
	left: 0;
	padding: .2em;
	outline: 0;
}
.ui-dialog .ui-dialog-titlebar {
	padding: .4em 1em;
	position: relative;
}
.ui-dialog .ui-dialog-title {
	float: left;
	margin: .1em 0;
	white-space: nowrap;
	width: 90%;
	overflow: hidden;
	text-overflow: ellipsis;
}
.ui-dialog .ui-dialog-titlebar-close {
	position: absolute;
	right: .3em;
	top: 50%;
	width: 20px;
	margin: -10px 0 0 0;
	padding: 1px;
	height: 20px;
}
.ui-dialog .ui-dialog-content {
	position: relative;
	border: 0;
	padding: .5em 1em;
	background: none;
	overflow: auto;
}
.ui-dialog .ui-dialog-buttonpane {
	text-align: left;
	border-width: 1px 0 0 0;
	background-image: none;
	margin-top: .5em;
	padding: .3em 1em .5em .4em;
}
.ui-dialog .ui-dialog-buttonpane .ui-dialog-buttonset {
	float: right;
}
.ui-dialog .ui-dialog-buttonpane button {
	margin: .5em .4em .5em 0;
	cursor: pointer;
}
.ui-dialog .ui-resizable-se {
	width: 12px;
	height: 12px;
	right: -5px;
	bottom: -5px;
	background-position: 16px 16px;
}
.ui-draggable .ui-dialog-titlebar {
	cursor: move;
}
.ui-draggable-handle {
	-ms-touch-action: none;
	touch-action: none;
}
.ui-menu {
	list-style: none;
	padding: 0;
	margin: 0;
	display: block;
	outline: none;
}
.ui-menu .ui-menu {
	position: absolute;
}
.ui-menu .ui-menu-item {
	position: relative;
	margin: 0;
	padding: 3px 1em 3px .4em;
	cursor: pointer;
	min-height: 0; /* support: IE7 */
	/* support: IE10, see #8844 */
	list-style-image: url("data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7");
}
.ui-menu .ui-menu-divider {
	margin: 5px 0;
	height: 0;
	font-size: 0;
	line-height: 0;
	border-width: 1px 0 0 0;
}
.ui-menu .ui-state-focus,
.ui-menu .ui-state-active {
	margin: -1px;
}

/* icon support */
.ui-menu-icons {
	position: relative;
}
.ui-menu-icons .ui-menu-item {
	padding-left: 2em;
}

/* left-aligned */
.ui-menu .ui-icon {
	position: absolute;
	top: 0;
	bottom: 0;
	left: .2em;
	margin: auto 0;
}

/* right-aligned */
.ui-menu .ui-menu-icon {
	left: auto;
	right: 0;
}
.ui-progressbar {
	height: 2em;
	text-align: left;
	overflow: hidden;
}
.ui-progressbar .ui-progressbar-value {
	margin: -1px;
	height: 100%;
}
.ui-progressbar .ui-progressbar-overlay {
	background: url("data:image/gif;base64,R0lGODlhKAAoAIABAAAAAP///yH/C05FVFNDQVBFMi4wAwEAAAAh+QQJAQABACwAAAAAKAAoAAACkYwNqXrdC52DS06a7MFZI+4FHBCKoDeWKXqymPqGqxvJrXZbMx7Ttc+w9XgU2FB3lOyQRWET2IFGiU9m1frDVpxZZc6bfHwv4c1YXP6k1Vdy292Fb6UkuvFtXpvWSzA+HycXJHUXiGYIiMg2R6W459gnWGfHNdjIqDWVqemH2ekpObkpOlppWUqZiqr6edqqWQAAIfkECQEAAQAsAAAAACgAKAAAApSMgZnGfaqcg1E2uuzDmmHUBR8Qil95hiPKqWn3aqtLsS18y7G1SzNeowWBENtQd+T1JktP05nzPTdJZlR6vUxNWWjV+vUWhWNkWFwxl9VpZRedYcflIOLafaa28XdsH/ynlcc1uPVDZxQIR0K25+cICCmoqCe5mGhZOfeYSUh5yJcJyrkZWWpaR8doJ2o4NYq62lAAACH5BAkBAAEALAAAAAAoACgAAAKVDI4Yy22ZnINRNqosw0Bv7i1gyHUkFj7oSaWlu3ovC8GxNso5fluz3qLVhBVeT/Lz7ZTHyxL5dDalQWPVOsQWtRnuwXaFTj9jVVh8pma9JjZ4zYSj5ZOyma7uuolffh+IR5aW97cHuBUXKGKXlKjn+DiHWMcYJah4N0lYCMlJOXipGRr5qdgoSTrqWSq6WFl2ypoaUAAAIfkECQEAAQAsAAAAACgAKAAAApaEb6HLgd/iO7FNWtcFWe+ufODGjRfoiJ2akShbueb0wtI50zm02pbvwfWEMWBQ1zKGlLIhskiEPm9R6vRXxV4ZzWT2yHOGpWMyorblKlNp8HmHEb/lCXjcW7bmtXP8Xt229OVWR1fod2eWqNfHuMjXCPkIGNileOiImVmCOEmoSfn3yXlJWmoHGhqp6ilYuWYpmTqKUgAAIfkECQEAAQAsAAAAACgAKAAAApiEH6kb58biQ3FNWtMFWW3eNVcojuFGfqnZqSebuS06w5V80/X02pKe8zFwP6EFWOT1lDFk8rGERh1TTNOocQ61Hm4Xm2VexUHpzjymViHrFbiELsefVrn6XKfnt2Q9G/+Xdie499XHd2g4h7ioOGhXGJboGAnXSBnoBwKYyfioubZJ2Hn0RuRZaflZOil56Zp6iioKSXpUAAAh+QQJAQABACwAAAAAKAAoAAACkoQRqRvnxuI7kU1a1UU5bd5tnSeOZXhmn5lWK3qNTWvRdQxP8qvaC+/yaYQzXO7BMvaUEmJRd3TsiMAgswmNYrSgZdYrTX6tSHGZO73ezuAw2uxuQ+BbeZfMxsexY35+/Qe4J1inV0g4x3WHuMhIl2jXOKT2Q+VU5fgoSUI52VfZyfkJGkha6jmY+aaYdirq+lQAACH5BAkBAAEALAAAAAAoACgAAAKWBIKpYe0L3YNKToqswUlvznigd4wiR4KhZrKt9Upqip61i9E3vMvxRdHlbEFiEXfk9YARYxOZZD6VQ2pUunBmtRXo1Lf8hMVVcNl8JafV38aM2/Fu5V16Bn63r6xt97j09+MXSFi4BniGFae3hzbH9+hYBzkpuUh5aZmHuanZOZgIuvbGiNeomCnaxxap2upaCZsq+1kAACH5BAkBAAEALAAAAAAoACgAAAKXjI8By5zf4kOxTVrXNVlv1X0d8IGZGKLnNpYtm8Lr9cqVeuOSvfOW79D9aDHizNhDJidFZhNydEahOaDH6nomtJjp1tutKoNWkvA6JqfRVLHU/QUfau9l2x7G54d1fl995xcIGAdXqMfBNadoYrhH+Mg2KBlpVpbluCiXmMnZ2Sh4GBqJ+ckIOqqJ6LmKSllZmsoq6wpQAAAh+QQJAQABACwAAAAAKAAoAAAClYx/oLvoxuJDkU1a1YUZbJ59nSd2ZXhWqbRa2/gF8Gu2DY3iqs7yrq+xBYEkYvFSM8aSSObE+ZgRl1BHFZNr7pRCavZ5BW2142hY3AN/zWtsmf12p9XxxFl2lpLn1rseztfXZjdIWIf2s5dItwjYKBgo9yg5pHgzJXTEeGlZuenpyPmpGQoKOWkYmSpaSnqKileI2FAAACH5BAkBAAEALAAAAAAoACgAAAKVjB+gu+jG4kORTVrVhRlsnn2dJ3ZleFaptFrb+CXmO9OozeL5VfP99HvAWhpiUdcwkpBH3825AwYdU8xTqlLGhtCosArKMpvfa1mMRae9VvWZfeB2XfPkeLmm18lUcBj+p5dnN8jXZ3YIGEhYuOUn45aoCDkp16hl5IjYJvjWKcnoGQpqyPlpOhr3aElaqrq56Bq7VAAAOw==");
	height: 100%;
	filter: alpha(opacity=25); /* support: IE8 */
	opacity: 0.25;
}
.ui-progressbar-indeterminate .ui-progressbar-value {
	background-image: none;
}
.ui-resizable {
	position: relative;
}
.ui-resizable-handle {
	position: absolute;
	font-size: 0.1px;
	display: block;
	-ms-touch-action: none;
	touch-action: none;
}
.ui-resizable-disabled .ui-resizable-handle,
.ui-resizable-autohide .ui-resizable-handle {
	display: none;
}
.ui-resizable-n {
	cursor: n-resize;
	height: 7px;
	width: 100%;
	top: -5px;
	left: 0;
}
.ui-resizable-s {
	cursor: s-resize;
	height: 7px;
	width: 100%;
	bottom: -5px;
	left: 0;
}
.ui-resizable-e {
	cursor: e-resize;
	width: 7px;
	right: -5px;
	top: 0;
	height: 100%;
}
.ui-resizable-w {
	cursor: w-resize;
	width: 7px;
	left: -5px;
	top: 0;
	height: 100%;
}
.ui-resizable-se {
	cursor: se-resize;
	width: 12px;
	height: 12px;
	right: 1px;
	bottom: 1px;
}
.ui-resizable-sw {
	cursor: sw-resize;
	width: 9px;
	height: 9px;
	left: -5px;
	bottom: -5px;
}
.ui-resizable-nw {
	cursor: nw-resize;
	width: 9px;
	height: 9px;
	left: -5px;
	top: -5px;
}
.ui-resizable-ne {
	cursor: ne-resize;
	width: 9px;
	height: 9px;
	right: -5px;
	top: -5px;
}
.ui-selectable {
	-ms-touch-action: none;
	touch-action: none;
}
.ui-selectable-helper {
	position: absolute;
	z-index: 100;
	border: 1px dotted black;
}
.ui-selectmenu-menu {
	padding: 0;
	margin: 0;
	position: absolute;
	top: 0;
	left: 0;
	display: none;
}
.ui-selectmenu-menu .ui-menu {
	overflow: auto;
	/* Support: IE7 */
	overflow-x: hidden;
	padding-bottom: 1px;
}
.ui-selectmenu-menu .ui-menu .ui-selectmenu-optgroup {
	font-size: 1em;
	font-weight: bold;
	line-height: 1.5;
	padding: 2px 0.4em;
	margin: 0.5em 0 0 0;
	height: auto;
	border: 0;
}
.ui-selectmenu-open {
	display: block;
}
.ui-selectmenu-button {
	display: inline-block;
	overflow: hidden;
	position: relative;
	text-decoration: none;
	cursor: pointer;
}
.ui-selectmenu-button span.ui-icon {
	right: 0.5em;
	left: auto;
	margin-top: -8px;
	position: absolute;
	top: 50%;
}
.ui-selectmenu-button span.ui-selectmenu-text {
	text-align: left;
	padding: 0.4em 2.1em 0.4em 1em;
	display: block;
	line-height: 1.4;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
}
.ui-slider {
	position: relative;
	text-align: left;
}
.ui-slider .ui-slider-handle {
	position: absolute;
	z-index: 2;
	width: 1.2em;
	height: 1.2em;
	cursor: default;
	-ms-touch-action: none;
	touch-action: none;
}
.ui-slider .ui-slider-range {
	position: absolute;
	z-index: 1;
	font-size: .7em;
	display: block;
	border: 0;
	background-position: 0 0;
}

/* support: IE8 - See #6727 */
.ui-slider.ui-state-disabled .ui-slider-handle,
.ui-slider.ui-state-disabled .ui-slider-range {
	filter: inherit;
}

.ui-slider-horizontal {
	height: .8em;
}
.ui-slider-horizontal .ui-slider-handle {
	top: -.3em;
	margin-left: -.6em;
}
.ui-slider-horizontal .ui-slider-range {
	top: 0;
	height: 100%;
}
.ui-slider-horizontal .ui-slider-range-min {
	left: 0;
}
.ui-slider-horizontal .ui-slider-range-max {
	right: 0;
}

.ui-slider-vertical {
	width: .8em;
	height: 100px;
}
.ui-slider-vertical .ui-slider-handle {
	left: -.3em;
	margin-left: 0;
	margin-bottom: -.6em;
}
.ui-slider-vertical .ui-slider-range {
	left: 0;
	width: 100%;
}
.ui-slider-vertical .ui-slider-range-min {
	bottom: 0;
}
.ui-slider-vertical .ui-slider-range-max {
	top: 0;
}
.ui-sortable-handle {
	-ms-touch-action: none;
	touch-action: none;
}
.ui-spinner {
	position: relative;
	display: inline-block;
	overflow: hidden;
	padding: 0;
	vertical-align: middle;
}
.ui-spinner-input {
	border: none;
	background: none;
	color: inherit;
	padding: 0;
	margin: .2em 0;
	vertical-align: middle;
	margin-left: .4em;
	margin-right: 22px;
}
.ui-spinner-button {
	width: 16px;
	height: 50%;
	font-size: .5em;
	padding: 0;
	margin: 0;
	text-align: center;
	position: absolute;
	cursor: default;
	display: block;
	overflow: hidden;
	right: 0;
}
/* more specificity required here to override default borders */
.ui-spinner a.ui-spinner-button {
	border-top: none;
	border-bottom: none;
	border-right: none;
}
/* vertically center icon */
.ui-spinner .ui-icon {
	position: absolute;
	margin-top: -8px;
	top: 50%;
	left: 0;
}
.ui-spinner-up {
	top: 0;
}
.ui-spinner-down {
	bottom: 0;
}

/* TR overrides */
.ui-spinner .ui-icon-triangle-1-s {
	/* need to fix icons sprite */
	background-position: -65px -16px;
}
.ui-tabs {
	position: relative;/* position: relative prevents IE scroll bug (element with position: relative inside container with overflow: auto appear as "fixed") */
	padding: .2em;
}
.ui-tabs .ui-tabs-nav {
	margin: 0;
	padding: .2em .2em 0;
}
.ui-tabs .ui-tabs-nav li {
	list-style: none;
	float: left;
	position: relative;
	top: 0;
	margin: 1px .2em 0 0;
	border-bottom-width: 0;
	padding: 0;
	white-space: nowrap;
}
.ui-tabs .ui-tabs-nav .ui-tabs-anchor {
	float: left;
	padding: .5em 1em;
	text-decoration: none;
}
.ui-tabs .ui-tabs-nav li.ui-tabs-active {
	margin-bottom: -1px;
	padding-bottom: 1px;
}
.ui-tabs .ui-tabs-nav li.ui-tabs-active .ui-tabs-anchor,
.ui-tabs .ui-tabs-nav li.ui-state-disabled .ui-tabs-anchor,
.ui-tabs .ui-tabs-nav li.ui-tabs-loading .ui-tabs-anchor {
	cursor: text;
}
.ui-tabs-collapsible .ui-tabs-nav li.ui-tabs-active .ui-tabs-anchor {
	cursor: pointer;
}
.ui-tabs .ui-tabs-panel {
	display: block;
	border-width: 0;
	padding: 1em 1.4em;
	background: none;
}
.ui-tooltip {
	padding: 8px;
	position: absolute;
	z-index: 9999;
	max-width: 300px;
	-webkit-box-shadow: 0 0 5px #aaa;
	box-shadow: 0 0 5px #aaa;
}
body .ui-tooltip {
	border-width: 2px;
}

/* Component containers
----------------------------------*/
.ui-widget {
	font-family: Verdana,Arial,sans-serif;
	font-size: 1.1em;
}
.ui-widget .ui-widget {
	font-size: 1em;
}
.ui-widget input,
.ui-widget select,
.ui-widget textarea,
.ui-widget button {
	font-family: Verdana,Arial,sans-serif;
	font-size: 1em;
}
.ui-widget-content {
	border: 1px solid #aaaaaa;
	background: #ffffff /*savepage-url=images/ui-bg_flat_75_ffffff_40x100.png*/ url() 50% 50% repeat-x;
	color: #222222;
}
.ui-widget-content a {
	color: #222222;
}
.ui-widget-header {
	border: 1px solid #aaaaaa;
	background: #cccccc /*savepage-url=images/ui-bg_highlight-soft_75_cccccc_1x100.png*/ url() 50% 50% repeat-x;
	color: #222222;
	font-weight: bold;
}
.ui-widget-header a {
	color: #222222;
}

/* Interaction states
----------------------------------*/
.ui-state-default,
.ui-widget-content .ui-state-default,
.ui-widget-header .ui-state-default {
	border: 1px solid #d3d3d3;
	background: #e6e6e6 /*savepage-url=images/ui-bg_glass_75_e6e6e6_1x400.png*/ url() 50% 50% repeat-x;
	font-weight: normal;
	color: #555555;
}
.ui-state-default a,
.ui-state-default a:link,
.ui-state-default a:visited {
	color: #555555;
	text-decoration: none;
}
.ui-state-hover,
.ui-widget-content .ui-state-hover,
.ui-widget-header .ui-state-hover,
.ui-state-focus,
.ui-widget-content .ui-state-focus,
.ui-widget-header .ui-state-focus {
	border: 1px solid #999999;
	background: #dadada /*savepage-url=images/ui-bg_glass_75_dadada_1x400.png*/ url() 50% 50% repeat-x;
	font-weight: normal;
	color: #212121;
}
.ui-state-hover a,
.ui-state-hover a:hover,
.ui-state-hover a:link,
.ui-state-hover a:visited,
.ui-state-focus a,
.ui-state-focus a:hover,
.ui-state-focus a:link,
.ui-state-focus a:visited {
	color: #212121;
	text-decoration: none;
}
.ui-state-active,
.ui-widget-content .ui-state-active,
.ui-widget-header .ui-state-active {
	border: 1px solid #aaaaaa;
	background: #ffffff /*savepage-url=images/ui-bg_glass_65_ffffff_1x400.png*/ url() 50% 50% repeat-x;
	font-weight: normal;
	color: #212121;
}
.ui-state-active a,
.ui-state-active a:link,
.ui-state-active a:visited {
	color: #212121;
	text-decoration: none;
}

/* Interaction Cues
----------------------------------*/
.ui-state-highlight,
.ui-widget-content .ui-state-highlight,
.ui-widget-header .ui-state-highlight {
	border: 1px solid #fcefa1;
	background: #fbf9ee /*savepage-url=images/ui-bg_glass_55_fbf9ee_1x400.png*/ url() 50% 50% repeat-x;
	color: #363636;
}
.ui-state-highlight a,
.ui-widget-content .ui-state-highlight a,
.ui-widget-header .ui-state-highlight a {
	color: #363636;
}
.ui-state-error,
.ui-widget-content .ui-state-error,
.ui-widget-header .ui-state-error {
	border: 1px solid #cd0a0a;
	background: #fef1ec /*savepage-url=images/ui-bg_glass_95_fef1ec_1x400.png*/ url() 50% 50% repeat-x;
	color: #cd0a0a;
}
.ui-state-error a,
.ui-widget-content .ui-state-error a,
.ui-widget-header .ui-state-error a {
	color: #cd0a0a;
}
.ui-state-error-text,
.ui-widget-content .ui-state-error-text,
.ui-widget-header .ui-state-error-text {
	color: #cd0a0a;
}
.ui-priority-primary,
.ui-widget-content .ui-priority-primary,
.ui-widget-header .ui-priority-primary {
	font-weight: bold;
}
.ui-priority-secondary,
.ui-widget-content .ui-priority-secondary,
.ui-widget-header .ui-priority-secondary {
	opacity: .7;
	filter:Alpha(Opacity=70); /* support: IE8 */
	font-weight: normal;
}
.ui-state-disabled,
.ui-widget-content .ui-state-disabled,
.ui-widget-header .ui-state-disabled {
	opacity: .35;
	filter:Alpha(Opacity=35); /* support: IE8 */
	background-image: none;
}
.ui-state-disabled .ui-icon {
	filter:Alpha(Opacity=35); /* support: IE8 - See #6059 */
}

/* Icons
----------------------------------*/

/* states and images */
.ui-icon {
	width: 16px;
	height: 16px;
}
.ui-icon,
.ui-widget-content .ui-icon {
	background-image: /*savepage-url=images/ui-icons_222222_256x240.png*/ url();
}
.ui-widget-header .ui-icon {
	background-image: /*savepage-url=images/ui-icons_222222_256x240.png*/ url();
}
.ui-state-default .ui-icon {
	background-image: /*savepage-url=images/ui-icons_888888_256x240.png*/ url();
}
.ui-state-hover .ui-icon,
.ui-state-focus .ui-icon {
	background-image: /*savepage-url=images/ui-icons_454545_256x240.png*/ url();
}
.ui-state-active .ui-icon {
	background-image: /*savepage-url=images/ui-icons_454545_256x240.png*/ url();
}
.ui-state-highlight .ui-icon {
	background-image: /*savepage-url=images/ui-icons_2e83ff_256x240.png*/ url();
}
.ui-state-error .ui-icon,
.ui-state-error-text .ui-icon {
	background-image: /*savepage-url=images/ui-icons_cd0a0a_256x240.png*/ url();
}

/* positioning */
.ui-icon-blank { background-position: 16px 16px; }
.ui-icon-carat-1-n { background-position: 0 0; }
.ui-icon-carat-1-ne { background-position: -16px 0; }
.ui-icon-carat-1-e { background-position: -32px 0; }
.ui-icon-carat-1-se { background-position: -48px 0; }
.ui-icon-carat-1-s { background-position: -64px 0; }
.ui-icon-carat-1-sw { background-position: -80px 0; }
.ui-icon-carat-1-w { background-position: -96px 0; }
.ui-icon-carat-1-nw { background-position: -112px 0; }
.ui-icon-carat-2-n-s { background-position: -128px 0; }
.ui-icon-carat-2-e-w { background-position: -144px 0; }
.ui-icon-triangle-1-n { background-position: 0 -16px; }
.ui-icon-triangle-1-ne { background-position: -16px -16px; }
.ui-icon-triangle-1-e { background-position: -32px -16px; }
.ui-icon-triangle-1-se { background-position: -48px -16px; }
.ui-icon-triangle-1-s { background-position: -64px -16px; }
.ui-icon-triangle-1-sw { background-position: -80px -16px; }
.ui-icon-triangle-1-w { background-position: -96px -16px; }
.ui-icon-triangle-1-nw { background-position: -112px -16px; }
.ui-icon-triangle-2-n-s { background-position: -128px -16px; }
.ui-icon-triangle-2-e-w { background-position: -144px -16px; }
.ui-icon-arrow-1-n { background-position: 0 -32px; }
.ui-icon-arrow-1-ne { background-position: -16px -32px; }
.ui-icon-arrow-1-e { background-position: -32px -32px; }
.ui-icon-arrow-1-se { background-position: -48px -32px; }
.ui-icon-arrow-1-s { background-position: -64px -32px; }
.ui-icon-arrow-1-sw { background-position: -80px -32px; }
.ui-icon-arrow-1-w { background-position: -96px -32px; }
.ui-icon-arrow-1-nw { background-position: -112px -32px; }
.ui-icon-arrow-2-n-s { background-position: -128px -32px; }
.ui-icon-arrow-2-ne-sw { background-position: -144px -32px; }
.ui-icon-arrow-2-e-w { background-position: -160px -32px; }
.ui-icon-arrow-2-se-nw { background-position: -176px -32px; }
.ui-icon-arrowstop-1-n { background-position: -192px -32px; }
.ui-icon-arrowstop-1-e { background-position: -208px -32px; }
.ui-icon-arrowstop-1-s { background-position: -224px -32px; }
.ui-icon-arrowstop-1-w { background-position: -240px -32px; }
.ui-icon-arrowthick-1-n { background-position: 0 -48px; }
.ui-icon-arrowthick-1-ne { background-position: -16px -48px; }
.ui-icon-arrowthick-1-e { background-position: -32px -48px; }
.ui-icon-arrowthick-1-se { background-position: -48px -48px; }
.ui-icon-arrowthick-1-s { background-position: -64px -48px; }
.ui-icon-arrowthick-1-sw { background-position: -80px -48px; }
.ui-icon-arrowthick-1-w { background-position: -96px -48px; }
.ui-icon-arrowthick-1-nw { background-position: -112px -48px; }
.ui-icon-arrowthick-2-n-s { background-position: -128px -48px; }
.ui-icon-arrowthick-2-ne-sw { background-position: -144px -48px; }
.ui-icon-arrowthick-2-e-w { background-position: -160px -48px; }
.ui-icon-arrowthick-2-se-nw { background-position: -176px -48px; }
.ui-icon-arrowthickstop-1-n { background-position: -192px -48px; }
.ui-icon-arrowthickstop-1-e { background-position: -208px -48px; }
.ui-icon-arrowthickstop-1-s { background-position: -224px -48px; }
.ui-icon-arrowthickstop-1-w { background-position: -240px -48px; }
.ui-icon-arrowreturnthick-1-w { background-position: 0 -64px; }
.ui-icon-arrowreturnthick-1-n { background-position: -16px -64px; }
.ui-icon-arrowreturnthick-1-e { background-position: -32px -64px; }
.ui-icon-arrowreturnthick-1-s { background-position: -48px -64px; }
.ui-icon-arrowreturn-1-w { background-position: -64px -64px; }
.ui-icon-arrowreturn-1-n { background-position: -80px -64px; }
.ui-icon-arrowreturn-1-e { background-position: -96px -64px; }
.ui-icon-arrowreturn-1-s { background-position: -112px -64px; }
.ui-icon-arrowrefresh-1-w { background-position: -128px -64px; }
.ui-icon-arrowrefresh-1-n { background-position: -144px -64px; }
.ui-icon-arrowrefresh-1-e { background-position: -160px -64px; }
.ui-icon-arrowrefresh-1-s { background-position: -176px -64px; }
.ui-icon-arrow-4 { background-position: 0 -80px; }
.ui-icon-arrow-4-diag { background-position: -16px -80px; }
.ui-icon-extlink { background-position: -32px -80px; }
.ui-icon-newwin { background-position: -48px -80px; }
.ui-icon-refresh { background-position: -64px -80px; }
.ui-icon-shuffle { background-position: -80px -80px; }
.ui-icon-transfer-e-w { background-position: -96px -80px; }
.ui-icon-transferthick-e-w { background-position: -112px -80px; }
.ui-icon-folder-collapsed { background-position: 0 -96px; }
.ui-icon-folder-open { background-position: -16px -96px; }
.ui-icon-document { background-position: -32px -96px; }
.ui-icon-document-b { background-position: -48px -96px; }
.ui-icon-note { background-position: -64px -96px; }
.ui-icon-mail-closed { background-position: -80px -96px; }
.ui-icon-mail-open { background-position: -96px -96px; }
.ui-icon-suitcase { background-position: -112px -96px; }
.ui-icon-comment { background-position: -128px -96px; }
.ui-icon-person { background-position: -144px -96px; }
.ui-icon-print { background-position: -160px -96px; }
.ui-icon-trash { background-position: -176px -96px; }
.ui-icon-locked { background-position: -192px -96px; }
.ui-icon-unlocked { background-position: -208px -96px; }
.ui-icon-bookmark { background-position: -224px -96px; }
.ui-icon-tag { background-position: -240px -96px; }
.ui-icon-home { background-position: 0 -112px; }
.ui-icon-flag { background-position: -16px -112px; }
.ui-icon-calendar { background-position: -32px -112px; }
.ui-icon-cart { background-position: -48px -112px; }
.ui-icon-pencil { background-position: -64px -112px; }
.ui-icon-clock { background-position: -80px -112px; }
.ui-icon-disk { background-position: -96px -112px; }
.ui-icon-calculator { background-position: -112px -112px; }
.ui-icon-zoomin { background-position: -128px -112px; }
.ui-icon-zoomout { background-position: -144px -112px; }
.ui-icon-search { background-position: -160px -112px; }
.ui-icon-wrench { background-position: -176px -112px; }
.ui-icon-gear { background-position: -192px -112px; }
.ui-icon-heart { background-position: -208px -112px; }
.ui-icon-star { background-position: -224px -112px; }
.ui-icon-link { background-position: -240px -112px; }
.ui-icon-cancel { background-position: 0 -128px; }
.ui-icon-plus { background-position: -16px -128px; }
.ui-icon-plusthick { background-position: -32px -128px; }
.ui-icon-minus { background-position: -48px -128px; }
.ui-icon-minusthick { background-position: -64px -128px; }
.ui-icon-close { background-position: -80px -128px; }
.ui-icon-closethick { background-position: -96px -128px; }
.ui-icon-key { background-position: -112px -128px; }
.ui-icon-lightbulb { background-position: -128px -128px; }
.ui-icon-scissors { background-position: -144px -128px; }
.ui-icon-clipboard { background-position: -160px -128px; }
.ui-icon-copy { background-position: -176px -128px; }
.ui-icon-contact { background-position: -192px -128px; }
.ui-icon-image { background-position: -208px -128px; }
.ui-icon-video { background-position: -224px -128px; }
.ui-icon-script { background-position: -240px -128px; }
.ui-icon-alert { background-position: 0 -144px; }
.ui-icon-info { background-position: -16px -144px; }
.ui-icon-notice { background-position: -32px -144px; }
.ui-icon-help { background-position: -48px -144px; }
.ui-icon-check { background-position: -64px -144px; }
.ui-icon-bullet { background-position: -80px -144px; }
.ui-icon-radio-on { background-position: -96px -144px; }
.ui-icon-radio-off { background-position: -112px -144px; }
.ui-icon-pin-w { background-position: -128px -144px; }
.ui-icon-pin-s { background-position: -144px -144px; }
.ui-icon-play { background-position: 0 -160px; }
.ui-icon-pause { background-position: -16px -160px; }
.ui-icon-seek-next { background-position: -32px -160px; }
.ui-icon-seek-prev { background-position: -48px -160px; }
.ui-icon-seek-end { background-position: -64px -160px; }
.ui-icon-seek-start { background-position: -80px -160px; }
/* ui-icon-seek-first is deprecated, use ui-icon-seek-start instead */
.ui-icon-seek-first { background-position: -80px -160px; }
.ui-icon-stop { background-position: -96px -160px; }
.ui-icon-eject { background-position: -112px -160px; }
.ui-icon-volume-off { background-position: -128px -160px; }
.ui-icon-volume-on { background-position: -144px -160px; }
.ui-icon-power { background-position: 0 -176px; }
.ui-icon-signal-diag { background-position: -16px -176px; }
.ui-icon-signal { background-position: -32px -176px; }
.ui-icon-battery-0 { background-position: -48px -176px; }
.ui-icon-battery-1 { background-position: -64px -176px; }
.ui-icon-battery-2 { background-position: -80px -176px; }
.ui-icon-battery-3 { background-position: -96px -176px; }
.ui-icon-circle-plus { background-position: 0 -192px; }
.ui-icon-circle-minus { background-position: -16px -192px; }
.ui-icon-circle-close { background-position: -32px -192px; }
.ui-icon-circle-triangle-e { background-position: -48px -192px; }
.ui-icon-circle-triangle-s { background-position: -64px -192px; }
.ui-icon-circle-triangle-w { background-position: -80px -192px; }
.ui-icon-circle-triangle-n { background-position: -96px -192px; }
.ui-icon-circle-arrow-e { background-position: -112px -192px; }
.ui-icon-circle-arrow-s { background-position: -128px -192px; }
.ui-icon-circle-arrow-w { background-position: -144px -192px; }
.ui-icon-circle-arrow-n { background-position: -160px -192px; }
.ui-icon-circle-zoomin { background-position: -176px -192px; }
.ui-icon-circle-zoomout { background-position: -192px -192px; }
.ui-icon-circle-check { background-position: -208px -192px; }
.ui-icon-circlesmall-plus { background-position: 0 -208px; }
.ui-icon-circlesmall-minus { background-position: -16px -208px; }
.ui-icon-circlesmall-close { background-position: -32px -208px; }
.ui-icon-squaresmall-plus { background-position: -48px -208px; }
.ui-icon-squaresmall-minus { background-position: -64px -208px; }
.ui-icon-squaresmall-close { background-position: -80px -208px; }
.ui-icon-grip-dotted-vertical { background-position: 0 -224px; }
.ui-icon-grip-dotted-horizontal { background-position: -16px -224px; }
.ui-icon-grip-solid-vertical { background-position: -32px -224px; }
.ui-icon-grip-solid-horizontal { background-position: -48px -224px; }
.ui-icon-gripsmall-diagonal-se { background-position: -64px -224px; }
.ui-icon-grip-diagonal-se { background-position: -80px -224px; }


/* Misc visuals
----------------------------------*/

/* Corner radius */
.ui-corner-all,
.ui-corner-top,
.ui-corner-left,
.ui-corner-tl {
	border-top-left-radius: 4px;
}
.ui-corner-all,
.ui-corner-top,
.ui-corner-right,
.ui-corner-tr {
	border-top-right-radius: 4px;
}
.ui-corner-all,
.ui-corner-bottom,
.ui-corner-left,
.ui-corner-bl {
	border-bottom-left-radius: 4px;
}
.ui-corner-all,
.ui-corner-bottom,
.ui-corner-right,
.ui-corner-br {
	border-bottom-right-radius: 4px;
}

/* Overlays */
.ui-widget-overlay {
	background: #aaaaaa /*savepage-url=images/ui-bg_flat_0_aaaaaa_40x100.png*/ url() 50% 50% repeat-x;
	opacity: .3;
	filter: Alpha(Opacity=30); /* support: IE8 */
}
.ui-widget-shadow {
	margin: -8px 0 0 -8px;
	padding: 8px;
	background: #aaaaaa /*savepage-url=images/ui-bg_flat_0_aaaaaa_40x100.png*/ url() 50% 50% repeat-x;
	opacity: .3;
	filter: Alpha(Opacity=30); /* support: IE8 */
	border-radius: 8px;
}
</style>
<script data-savepage-src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" src=""></script>
<script type="text/javascript" data-savepage-src="/js/jquery.cookie.js?v=v1.4.1m" src=""></script>
<script type="text/javascript" data-savepage-src="/lib/embedly/jquery.embedly.js" src=""></script>
<style data-savepage-href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800,300" type="text/css">/* cyrillic-ext */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 300;
	src: local('Open Sans Light'), local('OpenSans-Light'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem5YaGs126MiZpBA-UN_r8OX-hpKKSTj5PW.woff2*/ url() format('woff2');
	unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}
/* cyrillic */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 300;
	src: local('Open Sans Light'), local('OpenSans-Light'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem5YaGs126MiZpBA-UN_r8OVuhpKKSTj5PW.woff2*/ url() format('woff2');
	unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* greek-ext */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 300;
	src: local('Open Sans Light'), local('OpenSans-Light'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem5YaGs126MiZpBA-UN_r8OXuhpKKSTj5PW.woff2*/ url() format('woff2');
	unicode-range: U+1F00-1FFF;
}
/* greek */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 300;
	src: local('Open Sans Light'), local('OpenSans-Light'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem5YaGs126MiZpBA-UN_r8OUehpKKSTj5PW.woff2*/ url() format('woff2');
	unicode-range: U+0370-03FF;
}
/* vietnamese */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 300;
	src: local('Open Sans Light'), local('OpenSans-Light'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem5YaGs126MiZpBA-UN_r8OXehpKKSTj5PW.woff2*/ url() format('woff2');
	unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
}
/* latin-ext */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 300;
	src: local('Open Sans Light'), local('OpenSans-Light'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem5YaGs126MiZpBA-UN_r8OXOhpKKSTj5PW.woff2*/ url() format('woff2');
	unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 300;
	src: local('Open Sans Light'), local('OpenSans-Light'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem5YaGs126MiZpBA-UN_r8OUuhpKKSTjw.woff2*/ url() format('woff2');
	unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* cyrillic-ext */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 400;
	src: local('Open Sans Regular'), local('OpenSans-Regular'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem8YaGs126MiZpBA-UFWJ0bf8pkAp6a.woff2*/ url() format('woff2');
	unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}
/* cyrillic */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 400;
	src: local('Open Sans Regular'), local('OpenSans-Regular'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem8YaGs126MiZpBA-UFUZ0bf8pkAp6a.woff2*/ url() format('woff2');
	unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* greek-ext */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 400;
	src: local('Open Sans Regular'), local('OpenSans-Regular'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem8YaGs126MiZpBA-UFWZ0bf8pkAp6a.woff2*/ url() format('woff2');
	unicode-range: U+1F00-1FFF;
}
/* greek */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 400;
	src: local('Open Sans Regular'), local('OpenSans-Regular'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem8YaGs126MiZpBA-UFVp0bf8pkAp6a.woff2*/ url() format('woff2');
	unicode-range: U+0370-03FF;
}
/* vietnamese */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 400;
	src: local('Open Sans Regular'), local('OpenSans-Regular'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem8YaGs126MiZpBA-UFWp0bf8pkAp6a.woff2*/ url() format('woff2');
	unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
}
/* latin-ext */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 400;
	src: local('Open Sans Regular'), local('OpenSans-Regular'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem8YaGs126MiZpBA-UFW50bf8pkAp6a.woff2*/ url() format('woff2');
	unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 400;
	src: local('Open Sans Regular'), local('OpenSans-Regular'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem8YaGs126MiZpBA-UFVZ0bf8pkAg.woff2*/ url() format('woff2');
	unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* cyrillic-ext */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 600;
	src: local('Open Sans SemiBold'), local('OpenSans-SemiBold'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem5YaGs126MiZpBA-UNirkOX-hpKKSTj5PW.woff2*/ url() format('woff2');
	unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}
/* cyrillic */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 600;
	src: local('Open Sans SemiBold'), local('OpenSans-SemiBold'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem5YaGs126MiZpBA-UNirkOVuhpKKSTj5PW.woff2*/ url() format('woff2');
	unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* greek-ext */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 600;
	src: local('Open Sans SemiBold'), local('OpenSans-SemiBold'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem5YaGs126MiZpBA-UNirkOXuhpKKSTj5PW.woff2*/ url() format('woff2');
	unicode-range: U+1F00-1FFF;
}
/* greek */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 600;
	src: local('Open Sans SemiBold'), local('OpenSans-SemiBold'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem5YaGs126MiZpBA-UNirkOUehpKKSTj5PW.woff2*/ url() format('woff2');
	unicode-range: U+0370-03FF;
}
/* vietnamese */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 600;
	src: local('Open Sans SemiBold'), local('OpenSans-SemiBold'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem5YaGs126MiZpBA-UNirkOXehpKKSTj5PW.woff2*/ url() format('woff2');
	unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
}
/* latin-ext */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 600;
	src: local('Open Sans SemiBold'), local('OpenSans-SemiBold'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem5YaGs126MiZpBA-UNirkOXOhpKKSTj5PW.woff2*/ url() format('woff2');
	unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 600;
	src: local('Open Sans SemiBold'), local('OpenSans-SemiBold'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem5YaGs126MiZpBA-UNirkOUuhpKKSTjw.woff2*/ url() format('woff2');
	unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* cyrillic-ext */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 700;
	src: local('Open Sans Bold'), local('OpenSans-Bold'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem5YaGs126MiZpBA-UN7rgOX-hpKKSTj5PW.woff2*/ url() format('woff2');
	unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}
/* cyrillic */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 700;
	src: local('Open Sans Bold'), local('OpenSans-Bold'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem5YaGs126MiZpBA-UN7rgOVuhpKKSTj5PW.woff2*/ url() format('woff2');
	unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* greek-ext */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 700;
	src: local('Open Sans Bold'), local('OpenSans-Bold'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem5YaGs126MiZpBA-UN7rgOXuhpKKSTj5PW.woff2*/ url() format('woff2');
	unicode-range: U+1F00-1FFF;
}
/* greek */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 700;
	src: local('Open Sans Bold'), local('OpenSans-Bold'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem5YaGs126MiZpBA-UN7rgOUehpKKSTj5PW.woff2*/ url() format('woff2');
	unicode-range: U+0370-03FF;
}
/* vietnamese */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 700;
	src: local('Open Sans Bold'), local('OpenSans-Bold'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem5YaGs126MiZpBA-UN7rgOXehpKKSTj5PW.woff2*/ url() format('woff2');
	unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
}
/* latin-ext */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 700;
	src: local('Open Sans Bold'), local('OpenSans-Bold'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem5YaGs126MiZpBA-UN7rgOXOhpKKSTj5PW.woff2*/ url() format('woff2');
	unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 700;
	src: local('Open Sans Bold'), local('OpenSans-Bold'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem5YaGs126MiZpBA-UN7rgOUuhpKKSTjw.woff2*/ url() format('woff2');
	unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* cyrillic-ext */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 800;
	src: local('Open Sans ExtraBold'), local('OpenSans-ExtraBold'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem5YaGs126MiZpBA-UN8rsOX-hpKKSTj5PW.woff2*/ url() format('woff2');
	unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}
/* cyrillic */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 800;
	src: local('Open Sans ExtraBold'), local('OpenSans-ExtraBold'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem5YaGs126MiZpBA-UN8rsOVuhpKKSTj5PW.woff2*/ url() format('woff2');
	unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* greek-ext */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 800;
	src: local('Open Sans ExtraBold'), local('OpenSans-ExtraBold'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem5YaGs126MiZpBA-UN8rsOXuhpKKSTj5PW.woff2*/ url() format('woff2');
	unicode-range: U+1F00-1FFF;
}
/* greek */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 800;
	src: local('Open Sans ExtraBold'), local('OpenSans-ExtraBold'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem5YaGs126MiZpBA-UN8rsOUehpKKSTj5PW.woff2*/ url() format('woff2');
	unicode-range: U+0370-03FF;
}
/* vietnamese */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 800;
	src: local('Open Sans ExtraBold'), local('OpenSans-ExtraBold'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem5YaGs126MiZpBA-UN8rsOXehpKKSTj5PW.woff2*/ url() format('woff2');
	unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
}
/* latin-ext */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 800;
	src: local('Open Sans ExtraBold'), local('OpenSans-ExtraBold'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem5YaGs126MiZpBA-UN8rsOXOhpKKSTj5PW.woff2*/ url() format('woff2');
	unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
	font-family: 'Open Sans';
	font-style: normal;
	font-weight: 800;
	src: local('Open Sans ExtraBold'), local('OpenSans-ExtraBold'), /*savepage-url=https://fonts.gstatic.com/s/opensans/v16/mem5YaGs126MiZpBA-UN8rsOUuhpKKSTjw.woff2*/ url() format('woff2');
	unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
</style>
<script type="text/javascript" data-savepage-src="/lib/magnific-popup/jquery.magnific-popup.min.js" src=""></script>
<style data-savepage-href="/lib/zaiko-icons/src-font/style.css?siteVersion=1.0.6" type="text/css">@font-face {
	font-family: 'zaiko-icons';
	src:  /*savepage-url=fonts/zaiko-icons.eot?v233xx*/ url();
	src:  /*savepage-url=fonts/zaiko-icons.eot?v233xx#iefix*/ url() format('embedded-opentype'),
	/*savepage-url=fonts/zaiko-icons.ttf?v233xx*/ url(data:application/octet-stream;base64,AAEAAAALAIAAAwAwT1MvMg8SBrgAAAC8AAAAYGNtYXDqn6ZfAAABHAAAAHRnYXNwAAAAEAAAAZAAAAAIZ2x5Zr6hw84AAAGYAACCJGhlYWQeKV7gAACDvAAAADZoaGVhERUNeAAAg/QAAAAkaG10eIxzCbIAAIQYAAABiGxvY2FN4iqcAACFoAAAAMZtYXhwAHUCTQAAhmgAAAAgbmFtZYOS8xEAAIaIAAABtnBvc3QAAwAAAACIQAAAACAAAwQiAZAABQAAApkCzAAAAI8CmQLMAAAB6wAzAQkAAAAAAAAAAAAAAAAAAAABEAAAAAAAAAAAAAAAAAAAAABAAADpbAPA/8AAQAPAAEAAAAABAAAAAAAAAAAAAAAgAAAAAAADAAAAAwAAABwAAQADAAAAHAADAAEAAAAcAAQAWAAAABIAEAADAAIAAQAg6TnpRuld6WTpbP/9//8AAAAAACDpAOk/6UjpYelr//3//wAB/+MXBBb/Fv4W+xb1AAMAAQAAAAAAAAAAAAAAAAAAAAAAAAABAAH//wAPAAEAAAAAAAAAAAACAAA3OQEAAAAAAQAAAAAAAAAAAAIAADc5AQAAAAABAAAAAAAAAAAAAgAANzkBAAAAAAUAFwAtA+ADTQAwAF4AbQCCAJgAADcGIicmNDcwNz4BNzYxNjQnJiIHMQYUFxYyNzE2MhcWFA8BBhQXFjI3MTY0JyYiBzEBMCcuAScmMS4BIyIGBzAHDgEHBjEGFBcwFhcGFhceATceATEeATcBPgE3NiYnJwMOAScuATcTPgEXHgEHNx4BFzAXHgEXFjEeARcWNjcBDgEXBTY3NjQnJicmJyYiBwYHDgEHAT4BN8EVPRUWFhESKxESJSUlaiUJCQgWCBU9FRYWcSUlJWomCAgJFwcCKxgZPBkZAgcDAwcCMTF2MTEFBgMCDAEMDSYPAgIHFQgBfgMDAQEDA639BAwEBAIE/QMMBAUCBCgEBgIZGTwZGQIFAxcuFf8ACQgBAU8cDg8PDhwdJCRMJCQcCQ4GAQ0KEwhUFhYVPRUSEioSEiVqJSUlCBYICQkVFRY8FnElaiYlJQkWCAgIAWcYGTwZGQIDAwM8PI88PAcVBwMBDiQNDgMMAgIHAQYBOgMGBAIGA0/+zAQCBAMLBQE0BAIEBAsEgAIEBBgZPBkZAgcDAggJAQEWLhd9HCUkSyUkHB0ODg4OHQgSCv7yBw4IAAABAN3/2AMJA60AEAAAEwEwNhcWBjEJATAWBwYmMQHdAfciExQU/kABqxASEif+JAG2AfcMExQd/kD+VSASEhIB3gAAAAIABP/EA/wDvAAbACAAAAEmBw4BBwYHBhceARcWFxY3PgE3Njc2Jy4BJyYBNyclAQLCYWRltEhJKCgBAktGRmFhZGW0SEkoKAECS0ZG/jhm9gJV/jsDlSgBAktGRmFhZGW0SEkoKAECS0ZGYWFkZbRISf1D9mYu/nYAAAANAAAAAAQAA4AACwAXACcAMwA/AFAAYABwAHwAiACYAKQAsAAAARQGIyImNTQ2MzIWFRQGIyImNTQ2MzIWEyEiBhURFBYzITI2NRE0JgMiJjU0NjMyFhUUBiciJjU0NjMyFhUUBiURFBY7ATI2NRE0JisBIgYVIREUFjsBMjY1ETQmKwEiBgEjIgYVERQWOwEyNjURNCYDIiY1NDYzMhYVFAYnIiY1NDYzMhYVFAYlIyIGFREUFjsBMjY1ETQmAyImNTQ2MzIWFRQGJyImNTQ2MzIWFRQGAisZEhIZGRISGRkSEhkZEhIZav7WCQ0NCQEqCQ0NniMyMiMjMjIjIzIyIyMyMv3dDAnWCA0NCNYJDAMADAnWCA0NCNYJDP3r1ggNDQjWCA0MdBIZGRISGRkSEhkZEhIZGQNZ1ggNDAnWCA0NcxIZGRISGRkSEhkZEhIZGQGrEhkZEhEZGecRGRkREhkZAW4MCf3VCQwMCQIrCQz+KzIjJDIyJCMy1TIkIzIyIyQyFv6qCQwMCQFWCA0NCP6qCQwMCQFWCA0NAg0MCf5VCQwMCQGrCA3+1RkSEhkZEhIZgBkSEhkZEhIZqwwJ/lUJDAwJAasIDf7VGRISGRkSEhmAGRISGRkSEhkAAAABAAAABwQAA3kAKgAAASIHDgEHBjEwJy4BJyYjIgcOAQcGFRQXHgEXFjEwNz4BNzY1NCcuAScmIwLpQS0tNgwMDAw2LS1BQTQ0SBMTUFDAUFBQUMBQUBMTSDQ0QQN5GBg6GRgYGToYGBwcWDY2M1ZlZa47Ojo6r2VlVjM2NlgcHAAAAAAEAAD/wAQAA8AAEQAfADwAWAAAASMiBhURFBY7ATI2NREuASMxAyIGFRQWMzI2NTQmIzE3IgcOAQcGFRQXHgEXFjMyNz4BNzY1NCcuAScmIxEiJy4BJyY1NDc+ATc2MzIXHgEXFhUUBw4BBwYCI0YUGxsURhQbARsTJCg4OScoOTgpAWpdXYspKCgpi11dampdXYspKCgpi11dalpPUHYiIyMidlBPWlpPT3YiIyMidk9PAfwcEv7UExscEgEsExsBDzkoKDg4KCg5tSgpi11dampdXYspKCgpi11dampdXYspKPxMIyJ2UE9aWk9PdiIjIyJ2T09aWk9QdiIjAAAABv/+/8wD/gO6AAcAKABJAFYAcAB9AAABMBQxOAExNQEUFjMyNj0BMzI2NTQmKwE1NCYjIgYdASMiBhUUFjsBFQE1OAExLgEnDgEjIiYnDgEHDgExBhYzITI2JzAmJzgBMREyNjU0JiMiBhUUFjM3DgEjIiYnHgEVFAYHHgEXMzI2JzAmJy4BJwUUFjMyNjU0JiMiBhUD/vx9GRIRGVISGRkSUhkREhlSEhkZElICPg9IPB1VMjJWHD1IDwYCAjQmAc8mNAEDBkhlZUhHZWVHpBxWMh44FwMEDQwvSRbZJjMBAwUPSD39eGVIR2VlR0hlAWQBAQEyEhkZElEaERIZUhIZGRJSGRIRGlH91AFJOh4kLCwkHjpKHCclNjYlKRoBX2VIR2VlR0hlKyQsEQ8NGw4bMhYXMS82JSoaSToeZkhlZUhHZWVHAAMARf/AA7sDwAAuADsAagAAAS4BJy4BJy4BIyIGBw4BBw4BBw4BFRQXHgEXFhceAR8BNz4BNzY3PgE3NjU0JicFIiY1NDYzMhYVFAYjFw4BBx4BFRQHDgEHBiMiJy4BJyY1NDY3LgEnDgEVFBceARcWMzI3PgE3NjU0JicDAgseExQsGRo3HBw3GhksFBMeCwsLCQgiGBkiMFIBDw8BUjAiGBkiCAkLC/7+LT8/LS0/Py3tChQLT1sbG2FDQ1FRQ0NhGxtbTwsUCl1xIiN3UVFdXVFRdyMicV0DFRktExMeCwsLCwsLHhMTLRkaNh0XHx9MLS00TG4BExMBbkw0LS1MHx8XHTYazUAtLT8/LS1AxREiERhMJx4eHTAPDw8PMB0eHidMGBEiESBtQjQsLEITExMTQiwsNEJtIAAFAAD/wAQAA8AAGwA3AEQAUQBeAAABIgcOAQcGFRQXHgEXFjMyNz4BNzY1NCcuAScmBzIXHgEXFhUUBw4BBwYjIicuAScmNTQ3PgE3NgMiBhUUFjMyNjU0JiMhIgYVFBYzMjY1NCYjISIGFRQWMzI2NTQmIwIAal1diykoKCmLXV1qal1diykoKCmLXV1qW09QdyIiIiJ3UE9bW09QdyIiIiJ3UE+rJDQ0JCQzMyQBBiQzMyQkMzMkAQYkMzMkJDQ0JAPAKCmLXV1qal1diykoKCmLXV1qal1diykoSyIid1BPW1tPUHciIiIid1BPW1tPUHciIv6iMyQkMzMkJDMzJCQzMyQkMzMkJDMzJCQzAAEA7v/WAxoDqwAQAAAJATAGJyY2MQkBMCY3NhYxAQMa/gkiExQUAcD+VRASEicB3AHN/gkMExQdAcABqyASEhL+IgAABAAD/8MD+wO7ACMAggCOAKgAACUCJT4BNz4BNz4BNz4BFx4BFx4BFx4BBw4BBw4BBw4BBwYUBwU3HgEXHgEXFjYnLgEnLgEnJicuAScmJy4BJy4BJyYGFx4BFx4BFx4BFwcwJiMuAScuAScuATU0NjU+ATcyNjMyFhceARcWFx4BFxYXHgEXHgEVHAEHDgEjIiYnLgEnNxQGIyImJyY2Fx4BASc+AScuAScmBgcuAScuASc+ARceARcWBgcC4K7+wxYnEh45HSFEJxgzGiM/HClDGR4GFhAtGhYvFg0VBAEB/uscChQLDBoMEREDAggFEC4aHh8fQyQkJxUuFwgRCRESAwIIBRM2Hg8dDSIBATNaIg8VAwEBAQQeHAMIBA4aDStRJTUwMFgnJyIWIwgCAgECIxkRHw8iPx8BRDAuQgEBQTYxPwH7IBAOBQQVEBAdDAYLBgYMBhA/ISQxBgYeG7IBPbABEg4XLxcbMBALCwEBFhMcRSszaTcmQiAcOBwRJhYDCAKpKgULBAUHAgIUEA4YDCREICQiI0AdHRsOGAwEAwEBEhIOGQwqSyQRIQ8kATZ1Qho4HgQKBQQIBBsbAwEEBA4qGiUpKVoyMTYjSSgJFAoEBwQZIgUECSETaTBDRC8qSwEBRgJRLgsfFBATAwQOEQQJBAMIBB0dBgYyJSA/DwAAAgAC/8ID8gO+ACYAQgAAJQE+ATU0Jy4BJyYjIgcOAQcGFRQXHgEXFjMyNjcBHgEzMjY3NjQnASInLgEnJjU0Nz4BNzYzMhceARcWFRQHDgEHBgPx/qQlKx0dZERDTE1DQ2UdHR0dZUNDTUF1LwFdBg8JCA8HDAz9gjs0NE0XFhYXTTQ0Ozo0NE4WFhYWTjQ0CgFeL3RBTERDZR0dHR1lQ0RMTUNEZB0eLSb+ogYGBgYNIwwBJBcWTjQ0Ozs0NE0XFhYXTTQ0Ozs0NE4WFwAAAAQAAP/ABAADwABkAPgBFAEhAAABMhYdAR4BFzc2Mh8BFhQPAR4BFzMyFh0BFAYrAQ4BBxcWFA8BBiIvAQ4BBxUUBisBIiY9AS4BJwcGIicjJzgBMSY0PwEuAScjIiY9ATQ2OwE+ATcnJjQ/ATYyHwE+ATc1NDY7ARUjIgYdARQGBw4BBwYmLwEmIgc4ATEHBhQfAR4BBw4BBw4BKwEiBh0BFBY7ATIWFx4BFxYGDwEGFB8BFjI/AT4BFx4BFx4BHQEUFjsBMjY9ATQ2Nz4BNzYWHwEWMj8BNjQvAS4BNz4BNz4BOwEyNj0BNCYrASImJy4BJyY2PwE2NC8BJiIPAQ4BJy4BJy4BPQE0JiMHMhceARcWFRQHDgEHBiMiJy4BJyY1NDc+ATc2FyIGFRQWMzI2NTQmIwJEIS8GDQYcF0MXYBcXHAMFAychLy8hJwMFAxwXF2AXQxccBg0GLyGIIS8GDQYcF0IXAWAXFxwDBQMnIS8vIScDBQMcFxdgF0MXHAYNBi8hiIgFBw0LEB8OChYILAQJA2EEBCwIBAUIDQUDEws+BgYGBj4LEwMFDQgFBAgsBARhAwkELAgWCg4fEAsNBwWIBQcNCxAfDgoXCCwDCQRgBAQsCAMFCAwFAxMLPgYGBgY+CxMDBQwIBQMILAQEYAQJAywIFwoOHxALDQcFRDEsK0ETExMTQSssMTEsK0ETExMTQSssMUdkZEdHZGRHA8AvIScDBQMcFxdgF0IXHAcNBi8hiCEvBg0GHBdDF2AXFxwDBQIoIS8vISgCBQMcFxdgF0MXHAYNBi8hiCEvBg0GHRdCF2AXFxwDBQMnIS9EBgY+CxIEBQwIBQQHLAQEYAQJAy0IFgoPHhALDQcFiAUHDQoQHw8KFggsBAkEYAQELAgDBQgMBQMTCz4GBgYGPgsTAwUMCAUDCCwEBGAECQQsBxcKDh8RCg0HBYgFBw0LEB8OChcILAMJBGAEBCwHBAUIDAUEEgs+BgbNExNBKywxMSwrQRMTExNBKywxMSwrQRMTRGRHR2RkR0dkAAACAAD/wAQAA8AAKABFAAATIgYVERQWMyEyNjURNCYnMSYGFREUBiMhIiY1ETQ2MyEyNicxLgEjIQUXFhQHAQYUFzEWMjcBNjIfARY2NRE2JiMhIgYXq0dkZEcCqkZlCQgbRiEY/VYYISEYAQQnHBsIFAz+/AHsYgcH/pUTExQ4FAFrBxEGYgsbAQ0J/r0PCwoDwGRH/VZGZWVGAQQLFQgaHCb+/BghIRgCqhghRhsICSZhBxIG/pQUOBQTEwFrBwdiCgsPAUMJDRsLAAADAAD/wAQAA8AAGwAfACMAAAE3FzcnBiInJjQ3MScHFwcnARc2MhcWFAcXAScnNxcHFzcXBwLgPTyncB9YIB8fcKc9PTz+WnAfWCAfH28Bpz3RPDw8LTw8PAGiPT2ncB8fIFgfcKc8PT3+WXAgIB9YH3ABpjzSPDw9LDw8PAAAAAQAAP/ABAADwABIAFQAiACsAAAlJz4BNTQmJzc+AScuAQ8BLgEnNTQmIyIGHQEOAQcnJgYHBhYfAQ4BFRQWFwcOARceAT8BHgEXFRQWMzI2PQE+ATcXFjY3NiYnJyImNTQ2MzIWFRQGASIGBy4BIyIGBy4BIyIHDgEHBhUUFx4BFxYzMjY3HgEzMjY3HgEzMjc+ATc2NTQnLgEnJgMiJicOASMiJicOASMiJjU0NjMyFhc+ATMyFhc+ATMyFhUUBgKMJAEBAQEkDQcHBx0MJQcQChQPDxQKEAclDB0HBwcNJAEBAQEkDQcHBx0MJQcQChQPDxQKEAclDB0HBwcNjA8UFA8PFBQBEQgQCCyHTU2HLAgQCC4pKT0REhIRPSkpLhUnEyxrOjprLBMnFS4pKT0REhIRPSkpLhouFCNmOztmIxQuGkJeXkIQHw4geEtLeCAOHxBCXl5JFAUKBQUJBRUHHA0NBwcVBgoDKg8UFA8qAwoGFQcHDQ0cBxUFCQUFCgUUCBwNDAgHFQYKAyoOFRUOKgMKBhUHCAwNHAgEFQ8OFRUODxUC8wEBPUVFPQEBEhE9KSkuLikpPRESBwgmKSkmCAcSET0pKS4uKSk9ERL+gBANKjMzKg0QXkJCXgYGPk5OPgYGXkJCXgAAAAMAAAAABAADgAA0AFkAZQAAASIGBy4BIyIGBy4BIyIHDgEHBhUUFx4BFxYzMjY3HgEzMjY3HgEzMjc+ATc2NTQnLgEnJiMRIiYnDgEjIiYnDgEjIiY1NDYzMhYXPgEzMhYXPgEzMhYVFAYjARQWMzI2NTQmMTAGAyAIEAgsh01NhywIEAguKSk9ERISET0pKS4VJxMsazo6aywTJxUuKSk9ERISET0pKS4aLhQjZjs7ZiMULhpCXl5CEB8OIHhLS3ggDh8QQl5eQv6gJRsbJUBAAwABAT1FRT0BARIRPSkpLi4pKT0REgcIJikpJggHEhE9KSkuLikpPRES/oAQDSozMyoNEF5CQl4GBj5OTj4GBl5CQl7+wBslJRsbZWUAAAYAAP/ABAADwAAzAD8ASwBXAGMAbwAAASIGBy4BIyIGBy4BIyIHDgEHBhUUFx4BFxYzMjY3HgEzMjY3HgEzMjc+ATc2NTQnLgEnJgEiBhUUFjMyNjU0JgUiBhUUFjMyNjU0JiUiBhUUFjMyNjU0JgMiBhUUFjMyNjU0JiEiBhUUFjMyNjU0JgMgCBAILIdNTYcsCBAILikpPRESEhE9KSkuFScTLGs6OmssEycVLikpPRESEhE9KSn9chslJRsbJSUBJRslJRsbJSUBJRslJRsbJSWbGyUlGxslJf5lGyUlGxslJQNAAQE9RUU9AQESET0pKS4uKSk9ERIHCCYpKSYIBxIRPSkpLi4pKT0REv4AJRsbJSUbGyVAJRsbJSUbGyVAJRsbJSUbGyX/ACUbGyUlGxslJRsbJSUbGyUAAAAABAAA/8AEAAPAAEgAVACIAKwAACUnPgE1NCYnNz4BJy4BDwEuASc1NCYjIgYdAQ4BBycmBgcGFh8BDgEVFBYXBw4BFx4BPwEeARcVFBYzMjY9AT4BNxcWNjc2JicnIiY1NDYzMhYVFAYBIgYHLgEjIgYHLgEjIgcOAQcGFRQXHgEXFjMyNjceATMyNjceATMyNz4BNzY1NCcuAScmAyImJw4BIyImJw4BIyImNTQ2MzIWFz4BMzIWFz4BMzIWFRQGAowkAQEBASQNBwcHHQwlBxAKFA8PFAoQByUMHQcHBw0kAQEBASQNBwcHHQwlBxAKFA8PFAoQByUMHQcHBw2MDxQUDw8UFAERCBAILIdNTYcsCBAILikpPRESEhE9KSkuFScTLGs6OmssEycVLikpPRESEhE9KSkuGi4UI2Y7O2YjFC4aQl5eQhAfDiB4S0t4IA4fEEJeXkkUBQoFBQkFFQccDQ0HBxUGCgMqDxQUDyoDCgYVBwcNDRwHFQUJBQUKBRQIHA0MCAcVBgoDKg4VFQ4qAwoGFQcIDA0cCAQVDw4VFQ4PFQLzAQE9RUU9AQESET0pKS4uKSk9ERIHCCYpKSYIBxIRPSkpLi4pKT0REv6AEA0qMzMqDRBeQkJeBgY+Tk4+BgZeQkJeAAAAAQAA/8AEAAPAAEgAAAE0JiMiBiMuASMiBgciJiMiBhUUFhcOARUuASMiBw4BBwYVFBceARcWMzI2Nx4BFwcXBzcnNz4BNx4BMzI3PgE3NjU0Jic+ATUEAGlJBAcEImQ5OWQiBAcESWkCAQECCBAILikpPRESEhE9KSkuFScTHEAjLkBAwEAKN2YqEycVLikpPRESGRYWGQKySmkBKzExKwFpSgcPCAECAQEBERI9KSgvLikpPBISCAcYIQkfQICAQBMCKCQHCBISPCkpLidFHBg+IgAKAAD/wAQAA8AAGwA3AEUAUwBhAHAAfgCMAJsAqQAAASIHDgEHBhUUFx4BFxYzMjc+ATc2NTQnLgEnJgMiJy4BJyY1NDc+ATc2MzIXHgEXFhUUBw4BBwYDMjY9ATQmIyIGHQEUFhMiBh0BFBYzMjY9ATQmATc2NCcmIg8BBhQXFjIBBwYUFxYyPwE2NCcmIgcnNCYrASIGFRQWOwEyNiUjIgYVFBY7ATI2NTQmJRYyNzY0LwEmIgcGFB8BASYiBwYUHwEWMjc2NCcCADkyMksWFhYWSzIyOTkyMksWFhYWSzIyOSkkJDUPEBAPNSQkKSkkJDUPEBAPNSQkKRAXFxAQFxcQEBcXEBAXFwEiOAwMDCAMNwwMCyH9pzgMDAwgDDcMDAshCzAYEE8QFxcQTxAYAztPEBgYEE8QFxf85QshCwwMNwwhCwwMOAJkCyELDAw3DCELDAwC1BYWSzIyOTkyMksWFhYWSzIyOTkyMksWFv4nEA81JCQpKSQkNQ8QEA81JCQpKSQkNQ8QAicYEE8QFxcQTxAY/TwYEE8QFxcQTxAYAl03DCELDAw4CyELDP4WNwwhCwwMOAshCwwM+xAXFxAQFxc3FxAQFxcQEBfUDAwLIQs4DAwLIQw3/goMDAshCzgMDAwgDAAAAAACAAAAgAQAAwAANABZAAABIgYHLgEjIgYHLgEjIgcOAQcGFRQXHgEXFjMyNjceATMyNjceATMyNz4BNzY1NCcuAScmIxEiJicOASMiJicOASMiJjU0NjMyFhc+ATMyFhc+ATMyFhUUBiMDIAgQCCyHTU2HLAgQCC4pKT0REhIRPSkpLhUnEyxrOjprLBMnFS4pKT0REhIRPSkpLhouFCNmOztmIxQuGkJeXkIQHw4geEtLeCAOHxBCXl5CAoABAT1FRT0BARIRPSkpLi4pKT0REgcIJikpJggHEhE9KSkuLikpPRES/oAQDSozMyoNEF5CQl4GBj5OTj4GBl5CQl4ABAAA/8AEAAPAAEgAVACIAKwAACUnPgE1NCYnNz4BJy4BDwEuASc1NCYjIgYdAQ4BBycmBgcGFh8BDgEVFBYXBw4BFx4BPwEeARcVFBYzMjY9AT4BNxcWNjc2JicnIiY1NDYzMhYVFAYBIgYHLgEjIgYHLgEjIgcOAQcGFRQXHgEXFjMyNjceATMyNjceATMyNz4BNzY1NCcuAScmAyImJw4BIyImJw4BIyImNTQ2MzIWFz4BMzIWFz4BMzIWFRQGAowkAQEBASQNBwcHHQwlBxAKFA8PFAoQByUMHQcHBw0kAQEBASQNBwcHHQwlBxAKFA8PFAoQByUMHQcHBw2MDxQUDw8UFAERCBAILIdNTYcsCBAILikpPRESEhE9KSkuFScTLGs6OmssEycVLikpPRESEhE9KSkuGi4UI2Y7O2YjFC4aQl5eQhAfDiB4S0t4IA4fEEJeXkkUBQoFBQkFFQccDQ0HBxUGCgMqDxQUDyoDCgYVBwcNDRwHFQUJBQUKBRQIHA0MCAcVBgoDKg4VFQ4qAwoGFQcIDA0cCAQVDw4VFQ4PFQLzAQE9RUU9AQESET0pKS4uKSk9ERIHCCYpKSYIBxIRPSkpLi4pKT0REv6AEA0qMzMqDRBeQkJeBgY+Tk4+BgZeQkJeAAAABABbAMQDpQMQAA0AGwApADcAABMhMjY1NCYjISIGFRQWBSEiBhUUFjMhMjY1NCYHISIGFRQWMyEyNjU0JgchIgYVFBYzITI2NTQmhQL2ERkZEf0KERkZAwf9ChEZGREC9hEZGRH9ChEZGREC9hEZGRH9ChEZGREC9hEZGQK8GRESGBgSERlUGBIRGRkREhioGRERGRkRERmoGRESGBgSERkAAAAEAFsAxAOlAxAADQAbACkANwAAEyEyNjU0JiMhIgYVFBYFISIGFRQWMyEyNjU0JgchIgYVFBYzITI2NTQmByEiBhUUFjMhMjY1NCaFAvYRGRkR/QoRGRkDB/0KERkZEQL2ERkZEf0KERkZEQL2ERkZEf0KERkZEQL2ERkZArwZERIYGBIRGVQYEhEZGRESGKgZEREZGRERGagZERIYGBIRGQAAAAgAAP/ABAADwAANABsAKQA3AEUAhQCVAMYAAAEyNj0BNCYjIgYdARQWBTc2NCcmIg8BBhQXFjIFMzI2NTQmKwEiBhUUFiUUFjsBMjY1NCYrASIGJRYyNzY0LwEmIgcGFBcBIgYHLgEnNCcuAScmIyIHDgEHBhUUFhcGBw4BBwYVFBceARcWMzI2Nx4BMzI2Nx4BMzI3PgE3NjU0Jy4BJyYjATIWFy4BIyIGBy4BNTQ2MwEiJicOASMiJicOASMiJjU0NjMyFhceARc+ATc+ATMyFhceARceARc+ATMyFhUUBiMBoA0TEw0NExMBBi0JCQkbCS0KCgka/ZFADRMTDUANExMCrRMNQA0TEw1ADRP95wkbCQoKLQkbCQoKAqYIEAgZQScSEj0oKC4uKSk9ERIMCy0nJzoRERIRPSkpLhUnEyxrOjprLBMnFS4pKT0REhIRPSkpLv6APFgJDx4QRXstCQpeQgGAGi4UI2Y7O2YjFC4aQl5eQgwWCgQJBAYRCSNlOw8dDw8eDiM4Eg4fEEJeXkIDQBMNQA0TEw1ADRNULQkbCQoKLQkbCQriEw0NExMNDRMgDRMTDQ0TE78KCgkbCS0KCgkbCf6nAQEiNBEtKCg8ERESET0pKS4aMRYCEhI8KCgtLikpPRESBwgmKSkmCAcSET0pKS4uKSk9ERIBAE06AwQ4MxEmFEJe/YAQDSozMyoNEF5CQl4EAwECAg0XCysyBAMECwgTOCMGBl5CQl4AAAgAAP/ABAADwAANABsAKQA3AEUAhQCVAMYAAAEyNj0BNCYjIgYdARQWBTc2NCcmIg8BBhQXFjIFMzI2NTQmKwEiBhUUFiUUFjsBMjY1NCYrASIGJRYyNzY0LwEmIgcGFBcBIgYHLgEnNCcuAScmIyIHDgEHBhUUFhcGBw4BBwYVFBceARcWMzI2Nx4BMzI2Nx4BMzI3PgE3NjU0Jy4BJyYjATIWFy4BIyIGBy4BNTQ2MwEiJicOASMiJicOASMiJjU0NjMyFhceARc+ATc+ATMyFhceARceARc+ATMyFhUUBiMBoA0TEw0NExMBBi0JCQkbCS0KCgka/ZFADRMTDUANExMCrRMNQA0TEw1ADRP95wkbCQoKLQkbCQoKAqYIEAgZQScSEj0oKC4uKSk9ERIMCy0nJzoRERIRPSkpLhUnEyxrOjprLBMnFS4pKT0REhIRPSkpLv6APFgJDx4QRXstCQpeQgGAGi4UI2Y7O2YjFC4aQl5eQgwWCgQJBAYRCSNlOw8dDw8eDiM4Eg4fEEJeXkIDQBMNQA0TEw1ADRNULQkbCQoKLQkbCQriEw0NExMNDRMgDRMTDQ0TE78KCgkbCS0KCgkbCf6nAQEiNBEtKCg8ERESET0pKS4aMRYCEhI8KCgtLikpPRESBwgmKSkmCAcSET0pKS4uKSk9ERIBAE06AwQ4MxEmFEJe/YAQDSozMyoNEF5CQl4EAwECAg0XCysyBAMECwgTOCMGBl5CQl4AAAgAAP/ABAADwAANABsAKQA3AEUAhQCVAMYAAAEyNj0BNCYjIgYdARQWBTc2NCcmIg8BBhQXFjIFMzI2NTQmKwEiBhUUFiUUFjsBMjY1NCYrASIGJRYyNzY0LwEmIgcGFBcBIgYHLgEnNCcuAScmIyIHDgEHBhUUFhcGBw4BBwYVFBceARcWMzI2Nx4BMzI2Nx4BMzI3PgE3NjU0Jy4BJyYjATIWFy4BIyIGBy4BNTQ2MwEiJicOASMiJicOASMiJjU0NjMyFhceARc+ATc+ATMyFhceARceARc+ATMyFhUUBiMBoA0TEw0NExMBBi0JCQkbCS0KCgka/ZFADRMTDUANExMCrRMNQA0TEw1ADRP95wkbCQoKLQkbCQoKAqYIEAgZQScSEj0oKC4uKSk9ERIMCy0nJzoRERIRPSkpLhUnEyxrOjprLBMnFS4pKT0REhIRPSkpLv6APFgJDx4QRXstCQpeQgGAGi4UI2Y7O2YjFC4aQl5eQgwWCgQJBAYRCSNlOw8dDw8eDiM4Eg4fEEJeXkIDQBMNQA0TEw1ADRNULQkbCQoKLQkbCQriEw0NExMNDRMgDRMTDQ0TE78KCgkbCS0KCgkbCf6nAQEiNBEtKCg8ERESET0pKS4aMRYCEhI8KCgtLikpPRESBwgmKSkmCAcSET0pKS4uKSk9ERIBAE06AwQ4MxEmFEJe/YAQDSozMyoNEF5CQl4EAwECAg0XCysyBAMECwgTOCMGBl5CQl4AAAgAAP/ABAADwAANABsAKQA3AEUAhQCVAMYAAAEyNj0BNCYjIgYdARQWBTc2NCcmIg8BBhQXFjIFMzI2NTQmKwEiBhUUFiUUFjsBMjY1NCYrASIGJRYyNzY0LwEmIgcGFBcBIgYHLgEnNCcuAScmIyIHDgEHBhUUFhcGBw4BBwYVFBceARcWMzI2Nx4BMzI2Nx4BMzI3PgE3NjU0Jy4BJyYjATIWFy4BIyIGBy4BNTQ2MwEiJicOASMiJicOASMiJjU0NjMyFhceARc+ATc+ATMyFhceARceARc+ATMyFhUUBiMBoA0TEw0NExMBBi0JCQkbCS0KCgka/ZFADRMTDUANExMCrRMNQA0TEw1ADRP95wkbCQoKLQkbCQoKAqYIEAgZQScSEj0oKC4uKSk9ERIMCy0nJzoRERIRPSkpLhUnEyxrOjprLBMnFS4pKT0REhIRPSkpLv6APFgJDx4QRXstCQpeQgGAGi4UI2Y7O2YjFC4aQl5eQgwWCgQJBAYRCSNlOw8dDw8eDiM4Eg4fEEJeXkIDQBMNQA0TEw1ADRNULQkbCQoKLQkbCQriEw0NExMNDRMgDRMTDQ0TE78KCgkbCS0KCgkbCf6nAQEiNBEtKCg8ERESET0pKS4aMRYCEhI8KCgtLikpPRESBwgmKSkmCAcSET0pKS4uKSk9ERIBAE06AwQ4MxEmFEJe/YAQDSozMyoNEF5CQl4EAwECAg0XCysyBAMECwgTOCMGBl5CQl4AAAUAAP/ABAADwAAzAFcAYwBvAHsAAAEiBgcuASMiBgcuASMiBw4BBwYVFBceARcWMzI2Nx4BMzI2Nx4BMzI3PgE3NjU0Jy4BJyYDIiYnDgEjIiYnDgEjIiY1NDYzMhYXPgEzMhYXPgEzMhYVFAYBFBYzMjY1NCYxMAY3FBYzMjY1NCYxMAYlFBYzMjY1NCYxMAYDIAgQCCyHTU2HLAgQCC4pKT0REhIRPSkpLhUnEyxrOjprLBMnFS4pKT0REhIRPSkpLhouFCNmOztmIxQuGkJeXkIQHw4geEtLeCAOHxBCXl7+YCUbGyVAQP4lGxslQED+ACUbGyVAQANAAQE9RUU9AQESET0pKS4uKSk9ERIHCCYpKSYIBxIRPSkpLi4pKT0REv6AEA0qMzMqDRBeQkJeBgY+Tk4+BgZeQkJe/kAbJSUbG2VlJRslJRsbZWVlGyUlGxtlZQAABgAA/8AEAAPAADMAPwBLAFcAYwBvAAABIgYHLgEjIgYHLgEjIgcOAQcGFRQXHgEXFjMyNjceATMyNjceATMyNz4BNzY1NCcuAScmASIGFRQWMzI2NTQmBSIGFRQWMzI2NTQmJSIGFRQWMzI2NTQmAyIGFRQWMzI2NTQmISIGFRQWMzI2NTQmAyAIEAgsh01NhywIEAguKSk9ERISET0pKS4VJxMsazo6aywTJxUuKSk9ERISET0pKf1yGyUlGxslJQElGyUlGxslJQElGyUlGxslJZsbJSUbGyUl/mUbJSUbGyUlA0ABAT1FRT0BARIRPSkpLi4pKT0REgcIJikpJggHEhE9KSkuLikpPRES/gAlGxslJRsbJUAlGxslJRsbJUAlGxslJRsbJf8AJRsbJSUbGyUlGxslJRsbJQAAAAAIAAD/wAQAA8AASABUAJ0AqQDyAP4BMgFWAAAlJz4BNTQmJzc+AScuAQ8BLgEnNTQmIyIGHQEOAQcnJgYHBhYfAQ4BFRQWFwcOARceAT8BHgEXFRQWMzI2PQE+ATcXFjY3NiYnJyImNTQ2MzIWFRQGJSc0Jic3NjQnJiIPAS4BIycuAQcOAR8BDgEHDgEHJyYGBwYWHwEeARcHBhQXFjI/AR4BHwEeATc+AS8BPgE3PgE3FxY2NzYmJwcGIicmNDc2MhcWFCUHLgEnLgEnNzYmJyYGDwEiBgcnJiIHBhQfAQ4BFQcOARceAT8BHgEXHgEXBwYWFxY2PwE+ATcXFjY3NjQvAT4BPwE+AScuAQcHBiInJjQ3NjIXFhQBNCcuAScmIyIGBy4BIyIGBy4BIyIHDgEHBhUUFx4BFxYzMjY3HgEzMjY3HgEzMjc+ATc2BQ4BIyImJw4BIyImNTQ2MzIWFz4BMzIWFz4BMzIWFRQGIyImAowkAQEBASQNBwcHHQwlBxAKFA8PFAoQByUMHQcHBw0kAQEBASQNBwcHHQwlBxAKFA8PFAoQByUMHQcHBw2MDxQUDw8UFAGlGAMDEgYGBhIGEQYLBQcCDwgJCAIGAgUCAgMCGAgPAwIJCBgBAwISBgcGEQYSBQsGBgIPCQgJAgcDBAICBAIXCQ8CAggJRwcRBgYGBhEHBv17GAIDAgIFAgYDCQgJDwIHBQsGEQYSBgYGEgMDGAkIAgIPCRcCBAICBAMHAgkICQ4DBgYLBRIGEQcGBhICAwEYCAkCAw8IPQYRBwYGBxEGBgNJEhE9KSkuCBAILIdNTYcsCBAILikpPRESEhE9KSkuFScTLGs6OmssEycVLikpPRES/sQjZjs7ZiMULhpCXl5CEB8OIHhLS3ggDh8QQl5eQhouSRQFCgUFCQUVBxwNDQcHFQYKAyoPFBQPKgMKBhUHBw0NHAcVBQkFBQoFFAgcDQwIBxUGCgMqDhUVDioDCgYVBwgMDRwIBBUPDhUVDg8VWgcGCwURBxEGBgYRAgMYCQkDAg8IGAIDAwIEAgYCCQgIDwMGBgsFEgYRBgYGEQIDARgICQIDDwgYAgMCAgUCBgIICQgPAg0GBgYSBgYGBhJzBgIEAgMDAhgIDwIDCQkYAwIRBgYGEQcRBQsGBgMPCAkIAgYCBQICAwIYCA8CAwkIGAEDAhEHAQYGEQYSBQsGBgMPCAgJAjkGBgYSBgYGBhIBgC4pKT0REgEBPUVFPQEBEhE9KSkuLikpPRESBwgmKSkmCAcSET0pKVUqMzMqDRBeQkJeBgY+Tk4+BgZeQkJeEAAAAAoAAP/ABAADwAAbADcARQBTAGEAcAB+AIwAmwCpAAABIgcOAQcGFRQXHgEXFjMyNz4BNzY1NCcuAScmAyInLgEnJjU0Nz4BNzYzMhceARcWFRQHDgEHBgMyNj0BNCYjIgYdARQWEyIGHQEUFjMyNj0BNCYBNzY0JyYiDwEGFBcWMgEHBhQXFjI/ATY0JyYiByc0JisBIgYVFBY7ATI2JSMiBhUUFjsBMjY1NCYlFjI3NjQvASYiBwYUHwEBJiIHBhQfARYyNzY0JwIAOTIySxYWFhZLMjI5OTIySxYWFhZLMjI5KSQkNQ8QEA81JCQpKSQkNQ8QEA81JCQpEBcXEBAXFxAQFxcQEBcXASI4DAwMIAw3DAwLIf2nOAwMDCAMNwwMCyELMBgQTxAXFxBPEBgDO08QGBgQTxAXF/zlCyELDAw3DCELDAw4AmQLIQsMDDcMIQsMDALUFhZLMjI5OTIySxYWFhZLMjI5OTIySxYW/icQDzUkJCkpJCQ1DxAQDzUkJCkpJCQ1DxACJxgQTxAXFxBPEBj9PBgQTxAXFxBPEBgCXTcMIQsMDDgLIQsM/hY3DCELDAw4CyELDAz7EBcXEBAXFzcXEBAXFxAQF9QMDAshCzgMDAshDDf+CgwMCyELOAwMDCAMAAAAAAEAAP/ABAADwABIAAABNCYjIgYjLgEjIgYHIiYjIgYVFBYXDgEVLgEjIgcOAQcGFRQXHgEXFjMyNjceARcHFwc3Jzc+ATceATMyNz4BNzY1NCYnPgE1BABpSQQHBCJkOTlkIgQHBElpAgEBAggQCC4pKT0REhIRPSkpLhUnExxAIy5AQMBACjdmKhMnFS4pKT0REhkWFhkCskppASsxMSsBaUoHDwgBAgEBARESPSkoLy4pKTwSEggHGCEJH0CAgEATAigkBwgSEjwpKS4nRRwYPiIAAQAA/8AEAAPAAEgAAAE0JiMiBiMuASMiBgciJiMiBhUUFhcOARUuASMiBw4BBwYVFBceARcWMzI2Nx4BFwcXBzcnNz4BNx4BMzI3PgE3NjU0Jic+ATUEAGlJBAcEImQ5OWQiBAcESWkCAQECCBAILikpPRESEhE9KSkuFScTHEAjLkBAwEAKN2YqEycVLikpPRESGRYWGQKySmkBKzExKwFpSgcPCAECAQEBERI9KSgvLikpPBISCAcYIQkfQICAQBMCKCQHCBISPCkpLidFHBg+IgACAAD/wAQAA8AAGwAvAAABIgcOAQcGFRQXHgEXFjMyNz4BNzY1NCcuAScmEwUGJi8BJjQ3PgEfATc2FhcWFAcCAGpdXosoKCgoi15dampdXosoKCgoi15dlP79DycOhA4ODykMYuARJw0LCwPAKCiLXl1qal1eiygoKCiLXl1qal1eiygo/j30DQEPhg4pDQ0CD2TSCwYPDSEPAAADAAD/wAQAA8AAGwApADcAAAEiBw4BBwYVFBceARcWMzI3PgE3NjU0Jy4BJyYDFAYjIiY1ETQ2MzIWFRMUBiMiJjURNDYzMhYVAgBqXV6LKCgoKIteXWpqXV6LKCgoKIteXZcaEhIbGxISGrMbEhIaGhISGwPAKCiLXl1qal1eiygoKCiLXl1qal1eiygo/W8SGhoSASISGhoS/t4SGhoSASISGhoSAAADAAD/wAQAA8AAGwB4AJAAAAEiBw4BBwYVFBceARcWMzI3PgE3NjU0Jy4BJyYDDgEjIiYnLgE1NDY3PgE/AT4BNz4BNTQmJy4BIyIGBw4BIyImJy4BNTQ2Nz4BNz4BNz4BMzIWFx4BFRQGBw4BDwEOAQcOARUUFhcWMjMyNjc+ATMyFhceARUUBgcTDgEjIiYnLgE1NDY3PgEzMhYXHgEVFAYCAGpdXosoKCgoi15dampdXosoKCgoi15dUSE7GhIbCAgIAgICBQNSAwUCAQICAQEEAgUWEBETAwMHBAMECAkIFQwLGxAQHAwTHgkKCgECAQYEQgQFAgIBAQEBAwEKFg0NEAMDBwMEAyAhbQwbERAcCwwMDAwLHBARGwwLDAwDwCgoi15dampdXosoKCgoi15dampdXosoKP0AGhkJCQoVCwgNBgcMB6oIDAQFCAMEBQECAREREREFBAUGAQUOCQoVCwkTCQgJCwsLGw8HDgcHDwiRCQ4GBgcDAgMBAQ0ODg0EBQQHAQchGQHHCwwMCwwcEBAbDAwLCwwMGxAQHAAAAAAJAGr/zAO7A6cAOQCsAL0A7wFQAV0BpQGqAa4AAAE2JjEuATEmJy4BJyYxMCYHDgEXJgYHDgEXDgEXFgYxMBY3HgEXHgE7ATI2Nz4BNz4BNzYnLgEnJjEHFDAVFAYxHAEjFAYVMBQHFAYVFDAVDgEVMBQxDgEVFCIHFAYVFAYxFAYHMAYxBhQHMBQxDgEHHAEjBhQHFAYxDgEVFAYxDgEHMAYVFAYHOAExDgEHFAYxDgEVMAYVFAYVFCIVDgEVLgE3PgEXOAEVFAYVBSMnPAE1LgEnMw4BBxwBBzE3JgYHIy4BBw4BBzQ2NTQ2NzgBNT4BNz4BNx4BFx4BFzAWFzA2JzI2Mx4BFRQWFS4BJwUuASc0JjEuATUwJjUuATU0JjU0JicwJjU0JicwNCMmNCc0JjEmNCcwJjU0JjU0JjEuATUwNDEuASc0MDU0JjU0MCc0JjUwNCcwJjU0MDU0JjUwNDEyFhceAQc2JicyIjEXLgEnHgEXMAYxFAYxASoBMTAiIyImJy4BJz4BNzYmJzQ2NR4BNz4BNzMeARcWNjcOARceARcOAQcOARUwBjEiBgcwBjEUBgc4ARUOAQc4ATkBDgEjLwE3FwcXMxUjA3URPAVbAiIiUCEhTlM1GQMdQiEuBg8gLzg/J3UqCBIJIkYjZiRGIQcOBoIGPR4DAx8SEhkBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQIBAQEBAQEBAQIBAQECAQQKDhcB/rwrAQIIB1AICQEBgxQkEJwQKBYkOgwBKAIFCAQFCwYPJBQccmQpDCEMBAkEBw8BETMd/jABAQEBAQEBAQECAQEBAgEBAQEBAQEBAQEBAQEBAQEBAQEBAQ4MCQMBAQEBAQEBRQECAQICAgEBASUlDQwlJlghAwUCAgUCChoOARI2Hys8CDAIPysbLxIPHAkECgUBBAIBAwEBAgEBAgIBAgEgTR9lBHoDeSMwMAJjRFMwFiQVFRQDAhwcETsWCg0nN1wjIIdqdVE2HQkYDC5OTi4KEwcOcnk+LzBAERCkAQEBAgEBAQICAQEBAgEBAQIDAQECAwEBAQEDAQEBAgICAgEDAgECBAEBAQIDAQEBAQMBAQECAgIBAQEEAgIDAQEBAQMBAQEBAgEBAQEBARQlDioSAwEDAwEpAQMFAw4UCAgWDwMEAk0BAQMEAQIDFx8gPRUeYwUBBg0GBgsEDRcJGDgaFhYpLgEPKRISMBoTDQOGAQQCAQECAgICAQECAgEBAQICAgEBAQQCAQIDAQEBAgIBAgEBAgEBAQICAQIBAwIBAQECAQEBAQICAQECAQEBAQMBARYjCxoPAQMCLwMEAgICAQEBAv62fSUDCAMDBQIMTCkEBwMOCgMEPyYnPwMBBworUg0GBwMCBQICAgIBAwEBAgIBAQIDAidhqxQUExUVDQALAHH/vwOPA78ABAAJAE4ArAEPAVIBmgGlAfwCBwJKAAAlMxUjNRczFSM1AS8DLgEjMCIjIgYPAi8BMCIxIgYHBhQfAg8BHAEXFBYzMjAzMDIxPwEfARYyMzIwMzoBNT4BNT8DPgE1NCYnNy4BIzAiMT4BNzY3NiYnJicuASMqAQciBw4BBwYHDgEHDgEXDgEHDgEXHgEXHgEXHgEXBhYXHgEzOAExOgEzHgEXHgE7ATI2Nz4BNzoBMzI2NzQmJz4BNz4BNzYmJw8CHAEVMBQxHAEjMBQxFAYVMBQjFAYVMAYxMAYxMAYxBjAHOAE5AQ4BBwYiIyoBJy4BLwEHBiIjKgEnLgEnLgE/AS8CJjQ3PgEzOgExFzc+ATM6ARceAR8CHgEXFgYPAQUqATUwJjEuAS8DLgE3PgE/Aj4BNzYyMzIWHwE3OgExMhYXFhQPARcWBgcOAQciBiMqAS8BBw4BIw4BIyImJzEDPgE3NhYXHgEzMjY3HgEXFhQHJy4BJy4BIyIGDwEnKgEjIgYHBhYfAS4BIyIGBzc+AScuASMqASMHJy4BIyIGBw4BDwEmNjcDLgEnFjIfARYGBwUOASMqASMiJicuASc+AT8BJzAmJzIwMzoBNz4BPwEXMhYzOgE3PgE3PgEvAT4BMzIWFwcGFhceARcyFjMyNj8BFx4BFxYyMzI2Nw4BBwYUFx4BFw4BBzcuASc3PgE3DgEHBTI2MT8BHwEwMjE6ATMyNjc2NC8CPwE2NCcuASMwIjEHLwE0JiMqATEiBg8EDgEVFBYzHwIUFhcwMhU6ATEBtpOTLjg4AV9JDAQRAQMCAQECAwEwCA5NAQIDAQEBKgcGHwIDAQEBAUsNCjsBAgEBAQEBAgMEAQxDAgEDAUMIGRABAQIBCQcIChgXNl9/GAUGAQYdHU4pKRwJEwkuOygLEQYFBwIHEQkGCwcCBwIDGwgDBQIBAgEOIxVJUyIyHFZMFSQNAQIBExwCAQIMEAMJEQcDCAQ+NwUBAQEBAQIBAQECBAIDBgMCBAIECAI7TQIFAQIEAgUJAwYEBB8HASIGBgQPCQEBTjAFDgYCBAIKDQITSAkLAQEJCAz9vAECAggLAQUsFwgJAQELCUgTAg0KAQQDBw0FME4BAQgQBAUFKh8DAwYDCQUBBAMBBQJLOwMHAwIEAgIFARkfTxoJHhMbRCgfOhwcQgUDAQYEFg8CBgINFggqQQEDAQ8ZBwgBCBAGDgcKEwcRCAEICBkOAQIBRCoJFg0DBgMPFwMOAQUIKwwXCQECASgFAwICBkZRFRUDGhlNRRYfCwgJAQMBAwIBAQMGAwcMBTJAAwcEAwYDCQ8GCgUGGAUfEg4WCRgGBQoGDwkDBgMDBwQ/MgUMBwQFBAUJBAEDAQMGAQUDDiAUhwICARYECAMGEQn9wgECPAoMSwEBAQEBAgECAR4FByoBAQEDAgFbCDEEAQEBAwIBEgQMSQIDAgJOAQUDAgIBAcQYGD8PDwFVHQQNTAIDAQE8CgEHAgIBBQJCCw1HAgQCAQETAwkzAQEBAwNNDgcnAQUBAwMBKQkKBAcEHiMkUCoqKUchAQMDEhMSHwkRCSpyjwIICAYWECZAFw4WCAMHAjUgAQEBL0QURD88RxRDLxYRCBsSDCAHGEAlEhcGUCBOAQIBAQECAQEBAQEBAQEBAQEBAQICAQIBAQMDMxcBAQEGAwcTCUcKAjUIEwgICAc8BgcBAw0JTB0DEAkKEQQGjAEBAxAJThoNBREJCQ8EHUwJDQMBBgY9BwgIBxMJQkcJEwcEBQEBARQ0AgQBAQEBAXxQVAIBAwIEBQQFHlMVC0EtHQ8WAwEBCwozBg8LDiAMGQEBAwEbDR8OCw8FMwoKAQEDFg85KT4S/pkOOiUBARghKArlQjo5QxNCKAYOAQYHDQwBAgUFLBICAQIJBgwfDzoCCQYGOA4fDAcJAgECAREsBAYCAQIBCQ0DBxEGAwUCJ0ET+BInEg4DBwQgNRIUATMJAxMBAQIEAkgMC0ICBQECAggKPAEBBAFMDQQdAQMDAgQvDk0DAwEBAAAAAAUAhf/LA3cDsgBVAKEArQC5ANUAAAEuASMqAQc0Jic2NzYmJyYnMCcuAScmBwYHDgEHBgcGFhciJiMiBgcOARceATMwMjEeARUeARceARceATsBMjY3PgE3PgE3PgE3MDIxMjY3PgE3NiYnBw4BIyImJw4BBw4BIyoBIyImJy4BJw4BIyImJyY2MzoBFx4BMRczMCcmNjc2NzAmNzAXHgE3NjcwFjcwFgcwFgczNzA2Nz4BMx4BBwUUBiMiJjU0NjMyFhcUBiMiJjU0NjMyFgcuAQcmBgcOAQcwFjcwNjMyFhceATEWNjEuAScDcgYWDgMIBQEBCAYHBA4NIBMTSjc2Rkc3Nk0VFQQDCAUDBgMOFgYECAMLMyYBAgIFERsUHw0hKhhkDiIXEjMnHBMGAQMBAREgDgwSBgUGBAsMLRcECgQLDCBETRUVNhkZOUMhCAsECQUXLQwDEhADCAMDAxASAgICBQUPCgouLoBCQigvIx8OFg4cDwMDCBAHEBID/jwSDA0REQ0MEvYRDQwSEgwNEXcMGAgPKRIfQQgBFFouHTkSIC4UAQtkGwH0CQgBBxAIJCwsUB0eBhgYNRMTCQkVFTwlJis0bicBCAkFEw8+dgsYCjBWGRMfDSEiEBIOLiQaWTEKEwoeHRg+IhETBSNEYAUEP30fPz09Px98QAUEYEQRDgERE2cZGkQkIxNmHAoKDQMEGiQGLjWSP3MMCgEDAQ8RNgwSEgwNERENDBISDA0REc0DAQEBAwcGHx4cBCAFAgcSAxsoHgMAAAcAC/+/A/UDvwALABcAHAAgAO0BQAGFAAABFAYjIiY1NDYzMhYXFAYjIiY1NDYzMhYHMxUjNRczFSMBNCYnPgE1NCYjMCIxNiYnPgE1NCYjKgEjOAExNCYnPgE1NCYjIgYHLgEjKgEjNjQ1NCYjIgYHPAE1NCYjIgYHLgEjIgYHLgEjIgYHLgEjIgYVFBYXDgEHLgEHDgEHDgEVFBYXDgEHDgEHDgEHJiIjIgYVFBYzOgEzHgEXDgEHBhYXDgEXDgEVBhYXDgEHBhYXHgEXHgEzMjY3NR4BOwEyNjc+ATcVHgEzMjY3PgE3PgE3HgE3PgEnPgE3PgEnPgE3PgE3PgE1NCYnPgE1ASoBIyImJzEuAScuASc+ATc0NjU+ATc8ATU+ATc+ATceARc3IiYnPgE3OgEzHgEzMjY3FhQXIgYHFz4BFxQGFRQWFw4BFRQWFw4BBw4BBzEOASMDLwM0IjE4ASMiMBUPASMnOAExIhQxHAEVHwEPARQwFTAWMTgBMzgBMT8BHwEwMjEwMjM4ATMyNDU3NT8BMDY1BiYjAZwRCwwREQwLEcgRCwwREQwLEbV5eSUuLgIhCwoGBxIMAgMHCAUFLh8CAwIDBAgJEgwGCgQRKxkCBAICHBUFCAUSDAcNBA8vGxswEg4gESA2EQMJBBQdBAQBAQEGFgoHCQEcJwUEAwYDBAUDDRYGAQUCDBISDAECAQECAQ8UAgMRDw0DCwQGAg8MAQIBAx4ZATssCEQtDxsNDxkPXAwfFQQGBAwYDiY+DgMIAxMbBgcWCQoGBAQHAxoPDQQHBQMFAhIXBwYQEf4ZEzIXDRkRDiccEA8EAgsKAQYHAhIcCwkRCA8bCQkBCwoQFAQCAwIQOCIJEQkBASM7AQkPPx0CLSQEBgMEBRAUFiQPHCYMiBMDAQUBAQEOAgQUAQsBAQkBARUDAhABAQEBAQEDEwEBAQEBgAsREQsMEREMCxERCwwREcMUFDQMARQPHAoEDgcNEhImEQgUCyEtChQLAw4KDRIEAw8TAwgFFB0CAgECAQwSBgUQFRYSBwoeGQIBHBQHDgYBBAEIBgUEDQcELB0KEgkBAgECBAIFFA0BEgwNEgMFAgkfEhUlDRg5GwMKBgkTAgMHBBwuCS1ECCo5BwZEDg0PEQIFA2UFBiohAgQCDCMUBwQHBhUKAgQCETkZAQQCAgUCCiQWDBYICyAT/nsREQ4oGg8zHhYrEgIDAggTCwIFAwgZEQEDBAMGAx0DAw0jFRsfAwEBBAEOAR0FCwMDCAQoPw4EDAUGCgQlQRIVIA0YGAEQCAEDFQEBEAMDAQEBARICBBMBAQEFAgMNAQEUBAILAgEBAQAJAIf/xAN1A7EAAwAHAJ4AsADAAMwA3QElAYwAACUzFSMXMxUjJT4BNz4BNw4BJz4BNzYmJy4BIyoBBzc+ATMyFhcuASMwIjE3PgE1LgEnLgEnNCYnJgYPASYGBz4BFx4BFw4BBw4BFxQWHwEiMCMiBgc+ATMyFh8BIiYjIgYHDgEXHgEXBiYnHgEzHgEXFAYHHgEXFjY3MAYHHgEXNSceARceATsBMjY3PgE3FT4BNy4BMRY2Nz4BNy4BNTc1NjIzMhYHDgEHLgEnLgEnNwE+ARceARcqASMiBgc+ATEDFAYjIiY1NDYzMhYnJjYzOgEfAQ4BBw4BBy4BJxMmNicOAQcwBjEOARUwIjEiBjEwIjEwIjEwIjUwJjEwJiMiNCMiJjUiJicuASc+ATcXNTAnJjY3NhcGBw4BBwYXNzYGBy4BJyUqAQcwIjEqASMwIjEiJiMwIjUiJjEwJjUwJicOAQcOAQcOARUOAQcUBgcOAQcGMBUOASMqASMiJicxOAExLgEnLgE1LgEnNjc+ATc2NwcwNz4BNzY3PgEXHgEHFTceARcOAQcUIiMBxnR0IywsAQoVHQoSJBAPIhEGBwEBBQMFEQwDBwQCBQ0GDhUCAiIXAQgNDQFXMh9AIgwMEzAPHidUFg4yGAEDARwzGBAfAQcJDQEBFyICAhUNCQ4FAwQFBAsRBgMFAQEFBRElDhAlEwkbExUqBQwGECELBwgeQSQBAwcDFh4UPBEfFgICAiVDIAsKEjsRAgMBNhgWBAgDDA0CAwUEBAcFCAgCCP7XCh0MBAcCBQsFDBsNBAUwDwsLDw8LCw/aAw8LAwcEBAEJCgIHAwMEAo0dBSABAgEBAQEBAQIBAgQCAgEBAQEBAQEBDBMIDRsNFQcICBcYPwwPDhkJCAFMBiIDBAgDAYsBAQEBAQEBAQEBAQEBAQICARcFHAUHAwECAwUDAgEDBAIBJCUOEhQXEiIiAgQDAQECBAINGBg8ICAdfwoKJxsbIx0SDRYkCBUMGg4KGAwBAZETHQuaBTUdAQ4MCwUFFB8FCxAEBgYBEQUFEg0WH30nQxUjSBgKDQEOGQkOBhQoExklFQ4LAQEBCRoPFisXFEAmeB8WDRIHBhcBBgYEDwsFGREGBgwODhs3CiSAEwUJBQMEChsQEh0JThQEBwQXHBsYAgMCYQgZEhIjEQgFAgICB5cksAYBCgwOGgwCBgMIFQkSAeoMBAkDCAUBAQUG/d0LDw8LCw8PFwwKAQcLGAoCBQMJFQv+6hs8NgEBAQEBAQEBAQECAQEBAQEIJRkCCgkuMSwsaCorBCAsLWExMiwdA11jAgcDjgEBAQEBAQIBGWEcBAYEAQEBAwUCAQIBAgUDAQElKCclAgQDAQEBAgQCFygoWy4tISEUE0EpKSsJGgkRa31QLAcJAhwlBAEAAAAABACG/80DtgOZAD0AYwCSALkAAAE+AScuAScmJy4BBwYHDgEHBhceARcWFx4BFxYGBw4BByEuATMeATc+ATc2Jjc+ATc2Jjc+ATc2Jy4BJyY3BTQmNSc0JicuATc+ATcwMjEyFhc3Fyc3LgEjDgEHBhYXHgEXMQcFDgEHDgEHDgEjIiYnLgEnLgEnLgEvAQcnFwcXHgEXHgEXHgEXPgE3PgE3FyIGFTcOAQcUBjEXBzcXPgE3NiYnLgEjDgEHDgExJz4BNzIwMzIWFx4BBwNpCwkDBDEhKUhHnEtKMS9TAgEKCiEVFhUNHwMHDxYPHQ4BzREGFyQ4IxsgBQYSAgQTBggNDREfDg0NDiYQEAf97wICAgETFAsLOykBER8NDhBKDQYPCCMcBAgPDQEBASIBJA0aDQ4ZCgMJBAQIAw0UCQoUDAMGAwQRCEoPAQMGAwsSCgYOCAkSCgwXDCQBAV4EEA4BDksODwgKAgMFEQocEAsTBwEGHRkdEAEBISwMFhAGAhYaJCg/bSYvGBcCFRUqKX9SNCsrTCIhIRQxGTRVIRcoEh9PAQoGBBMYHBMOFBoXHiUGCwcPCxoaPB0cEQ8BAQECAQIBGlAoJy4DBwgRSQIQAwICJQ4cOxIBAQEiOAsUCgsUCQMDAwILDwcHDwsCBQMEEUsLDgEDBQMJDgcECwYHDggIEwogAQGLFSYSAQERBUoQDBgOFC8RCwoBBQUBBCYQCQEXDRY/JwAAAAMAAP/ABAADwAAbADgAPAAAASIHDgEHBhUUFx4BFxYzMjc+ATc2NTQnLgEnJgcyFx4BFxYVFAcOAQcGIyInLgEnJjU0Nz4BNzYzBxEtAQIAal1diykoKCmLXV1qal1diykoKCmLXV1qZVhZgycmJieDWVhlZVhZgycmJieDWVhlkAGJ/ncDwCgpi11dampdXYspKCgpi11dampdXYspKBomJ4NZWGVlWFmDJyYmJ4NZWGVlWFmDJyb6/ijs7AAAAAEAKP/oA9gDmAAfAAAJATY0JyYGBwkBJiIHBhYXCQEGFBcWNjcJARYyNzYmJwKRAUYfHh5WHv66/rofVR4eAR4BRv66Hx4eVh4BRgFGH1UeHgEeAcABRh9VHh4BHv66AUYfHh5WHv66/rofVR4eAR4BRv66Hx4eVh4AAAABACX/wAPbA8AAMwAAASEyNz4BNzY1NCcuAScmIyE1DQE1ITIWFRQGIyEiBw4BBwYVFBceARcWMyE1ISImNTQ2MwFJAZI1Ly9FFBQUFEUvLzX+bv7cASQBki5AQC7+bjUuL0YUFBQURi8uNQJu/ZItQUEtAS4UFEYuLzU1Li9FFRSS29ySQC0uQBQURi4vNTUvLkYUFJJBLS1BAAAEAAD/wAQAA8AAGwA6AF0AfQAAASIHDgEHBhUUFx4BFxYzMjc+ATc2NTQnLgEnJhMOASMiJicuAQcOATEGJicmNjcyNjc2FhceARceAQc3DgEjIiYnJicuAScmBw4BIwYmJyY2NzI2NzYWFx4BFx4BBzciJicmJyYGBwYHBiYnJjY3MjY3NhYXHgEXHgEHDgEjAgBqXV6LKCgoKIteXWpqXV6LKCgoKIteXXUFEAkFCQRFlDY9UA4ZBAQODgJXQydLIy1RIw0HB0EGEwsFCwUpKytUJycgSF4BEB4FBBEQA2ZPLlkqNWAqDwkJKAcNBmFpaa86OQEVJAYFFBUDfmA5bTNBdjQSCwsHGA0DwCgoi15dampdXosoKCgoi15dampdXosoKP0ZCAkCAikVAQIRBA8ODRoEEgMBBgYJHRUHHQyHCgoCAxgPDg8CAwEDEwURERAeBRYCAgcHCiMZCSEPewMEORARBQwMAQUVFBQlBhsDAggKDCseCykSDA0AAAACAAD/wAQAA8AAEwAjAAABFSMiBh0BMxUjESMRIzUzNTQ2MzchIgYVERQWMyEyNjURNCYDZmYVHpmZmmZmaUq0/MwqPDwqAzQqPDwDWpoeFWeZ/pkBZ5mAS2lmPCr8zCo8PCoDNCo8ABAAAP/ABAADwAADAAcACwAPACYAMwA7AEcAVABlAHYAhwCYAJwAoACkAAAlIzUzESMVMyUjFTMVIxUzJRUzNTMVIxUzFTM1IzUzNSM1MzUjFSM3FTMVMxUzNSM1IzUjBTMVMzUjNSM3IxUzNTM1MzUjFSMnFTM1MzUzNSMVIzUjBTU0NjsBNSMiBw4BBwYdATMlMzIWHQEzNTQnLgEnJisBFRMVFAYrARUzMjc+ATc2PQEjBSMiJj0BIxUUFx4BFxY7ATUBIxUzBSMVMwMjFTMBjnJycnIBVnJycnL+wTkiFzk5ORciOTlythc5OTkXOf7BOTk5Oc0iOSI5OTlEOTk5OTk5/u9NO3JyOzQ0TRYWlAHecjtNlBYWTTQ0O3L6TTtycjs0NE0WFpT+InI7TZQWFk00NDtyAVY5Of76OTk5OTnGcQFWcnJy5HHMcTk5OSI5IjkWORdQOS0uOS4tUCI5Ihc5OSI5InJyFyI5IiJbcjtNlBYWTTQ0O3L6TTtycjs0NE0WFpT+InI7TZQWFk00NDty+k07cnI7NDRNFhaUAbA5zDkBPjkAAAAAAgAdALQD4wLMABIAJQAAATYyFzEWFAcxAQYiJzEmNDcxAQUmNDcxNjIXMQEWFAcxBiInMQEDfBU9FRYW/lEWPBYVFQGw/KEWFhU9FQGwFRUWPBb+UQLMFRUWPRX+UBUVFj0VAbBoFT0WFRX+UBU9FhUVAbAAAAAAAgAdALQD4wLMABIAJQAANwYiJzEmNDcxATYyFzEWFAcxASUWFAcxBiInMQEmNDcxNjIXMQGEFT0VFhYBrxY8FhUV/lADXxYWFT0V/lAVFRY8FgGvtBUVFj0VAbAVFRY9Ff5QaBU9FhUVAbAVPRYVFf5QAAAEAA//zQPrA6kAUwCaALgAxAAAJT4BJz4BNzY3PgE3Njc2JjEuAScwJgcGBw4BBwYHDgEHJgYHDgExBhYXMBYfAQYWHwEOAQcGBw4BFxYxMBcWNjc2Nz4BNxceATcXHgExHgE3MDY3Jw4BDwEOAQcGJi8BPgE3PgEnJgYHDgEHJy4BNz4BNzY0Nz4BNzY3PgE3Njc2Nz4BNzY3MjYzHAEVBgcOAQcGBwYHDgEHBgcFDgEHBiIjPAE3PgE3PgE3Fw4BMQYWNzA2NxcOAQcBJiIHBhQXFjI3NjQDEBoFBhkwFR0TEhcFBQECBAIgEyBAKC8vZjQ1MyZCHRVEMFZZDQoUOw4EFwgcBxAeDSEMCwEFBRoaVDY1OBYoEwcdWCEFDQoDGhFnL/ICBgQEBgsEBREDZRgzGE8sFhZoTxkpEGQDAgMDBwUBAQMFAiYjI0UlJComKSlXLi4uEBoJAgUGFRAQFhcbG0IpKDP+8h9BIggPBwEDFBMJFg0qBwYNFRsQDCsQHw8B+h1RHR0dHVEdHXwxQxUdQyUzNTRlLy8oQR8VHwIEAgEFBRYTEx0VMBkGBRouaBAbAwkOBSFYHQYUKBU5NTZUGhoFBQELDCENHhAGHQgXBA47FAoNWVYbAgUCAwQIAgQBBGUQKBlPaRUWK1AZMhdkAxIEBAsGAQEBBQcCMikoQhsbGBUQEBUGBgEBChoQLy0uVykpJiokJUUjIyY+ExQDAQcPCCJCHg8fECoNDxwVDgYHKw0WCQJxHR0dUR0dHR1RAAAAAAIAAP/uBAADkgA1AFgAAAEhOAExIgYPAScuASM4ATEhIgYdARQWOwEROAExFBYXHgEzBR4BMzI2NTgBMREzMjY9ATQmIwMjIgYVESUuASMiBhUOASMiJicRNCYrATUzFx4BMzI2PwEzA5/++QsSBnV1BhIL/vkoOTkoWUExAgEBAdUDCAQUHlkoOTkoA4gVHf70BQ4HFR0BHxYWHwEdFYjvigYTCgoTBorvA5IIB3R0Bwg5KLApOf7VNVINAQFuAQEdFQH/OSmwKDn+8h4V/imlAwQdFRUeHhUBXRUeqokHCAgHiQAAAAADAAD/wAQAA8AAHAAwAEUAAAEjNTQmIyEiBhURFBYXFTceATsBBTU+ATURNCYjARE0NjMhMhYVERQGIyEHNSMiJjUFFAYrARUnISImJzczMjY9ATMyFhUDZjNYQf4AQlg4LnsTSCrxAUMuOFhC/QAdFwIAFxwcF/7wvTMXHQM0HRczvf7wDxsEcfFBWDMXHQLzM0JYWEL+zTJOD9dNIyrN1w9OMgE0QVj/AAEzFx0dF/7NFxx7eyATzRcce3sQD0hYQZocFwAAAAgALv/AA9IDvwANAFwAawB7AIkAlgCjALAAABMGFhcWNj8BJgYnJgYHBS4BDwEnPgE3PgEnAwUXJQMGFhceARcHJyYGBwYWHwE4ATEwMjEXMhYzMjY3NiYvATcyFjMyNj8BFx4BMzI2NxcHDgEXHgEzMjY/AT4BJyUOASc5AS4BJy4BNxMFAzcDNxcWBgcOAQcOASMiJiclLgEHBiYHFx4BNz4BJwE+ATUuAQciBhUeATMXPgE1LgEjDgEXFBYzBxQWNz4BJy4BByIGF5cJNTAwUwsTMBRUOSkDAzsEFgtQMyAxEBMGDoT+ohf+1WMJDxcTNyAiUgwUAgMNDG4BbgIDAQoQAgMNDFIhBQgESnYQFw8Wb0UKEwoyUAsLBAMPCQIFAtcMCgP90g1tPh4zEBELBlIBASSXQ/puCgUODi4dDBkMMlEPAQUELjhRIi0nD1kvLiwO/g8NEgEUDg0SARQOfg0SARQODRIBFA6QFQ0OEgEBFA4NEgEBji9SCwo2MLMbghIMMArXCwsEG5gPMCAmVCoBMXVfQf7AKVMkHSsLnBEDDQwMFAIYGAENCgwUAhKcAV9Lrz5CUAMBlxoEFgsJCwEBRwQVDKM9Rw0HIxoaOx0BCjj+73cBDlP9Hj0bHCgKBAQ6LlMKKxMbgCCwLywPEFcuAVoBFA4NEgEUDg0SOQEUDg0SARQODRIbDhIBARQNDhIBFQ0ABwAA/8AEAAPAABMAHQAiACYAKgAuADMAAAEhESEyFhUUBiMhFSEyNjURNCYjEy4BIyERITIWFSkBESERByM1MzchFSEVIRUhBSEVITUDZvyaA2YXHR0X/s0BM0JYWEI0DBgQ/QADABcd/jP+zQEzZ2ZmzQEz/s0BM/7N/mcCzP00A8D8zRwXFx1mWEICzEJY/SkEBgJnHRf+zQEzzGZmZmZnZmdnAAAAAQAU/8UD6QOrACYAAAkBNjQnJiIHCQEmIgcGFBcJAQYUFx4BMzI2NwkBHgEzMjY3NjQnAQJLAZ4REQ8tEP5i/mIQLRAQEAGe/mIQEAgUCwoUCAGeAZ4IFAoLFAcREf5iAcABnhAtEBAQ/mIBnhAQEC0Q/mL+YhAtEAgICAgBnv5iCAgICBAtEAGeAAABAA8AQQPxAzAAFgAAJS4BJwMmNDc2MhcTATYyFxYUBwEOAQcBVRcmCv8LCw4lDv4CZQ4jDQsL/akLJBZBAhkUARoNIw0MDP7nAoIMDA4kDv1/ExkCAAAAAwAA/8EDygOIABAAGgBBAAABBwE3Njc+AScmJyYnJgYHBgEHMzcuAScuAScFBiInJjQ3AScBBiInJjQ3AScBBiInJjQ3AScBBx4BFx4BFzcBJwECIhQBcxUgExMBExMsLDIyYSws/cgKAa4CGxYWOyEBfgkYCAkJAYg9/nYIGQcJCQGHPf53CRgICQkBiD3+PAsmRBoaIAK/AcI9/ngDVBT+jBUgLCxhMjEsLBMUAhMT/PuuCSE6FxYbAh8JCQkYCAGIPf56CQkIGQcBiD3+eQkJCBkHAYg9/j6/Ah8aGkMoDAHCPf57AAIAAP/ABAADwAADAAcAABMRIREDIREhAAQAgP0AAwADwPwABAD8gAMAAAAAAwAA/8AEAAPAAAMABwANAAATESERAyERIQEnNxc3FwAEAID9AAMA/kDAWmbmWgPA/AAEAPyAAwD9wcBbZuZbAAIAAP/ABAADwAADAAkAABMRIREBJzcXNxcABAD9wMBaZuZaA8D8AAQA/UHAW2bmWwAAAAABAAD/wAPDA8AAOQAAASYnLgEnJiMiBw4BBwYVFBceARcWMzI3PgE3NjcnBgcOAQcGIyInLgEnJjU0Nz4BNzYzMhYXByERBwMwISQlUSssLWdaW4cnJycnh1taZzMxMFkoKCE5HSEhSikoK1ZLTHEgISEgcUxLVkqFNo8BXJMDLRsWFR4ICCcnh1pbZmdaW4cnJwoKJhsbITocFhcfCQghIHFLTFZVTEtxISAxK48BXJMAAAAFAAAADgR0A3IAGABiAIEAkACeAAABERQGKwE+ATU8ATUzESERBxE0NjMhMhYVASM3PgE1NCYnLgEjMSIGBwUjDgEjIiYnLgE1NDA9ASImIzgBMSIGDwEOARURFBY7ATI2Nx4BMzI2Nx4BMzI2Nx4BMzI2NzU0JiM3NTQmJz4BNTQmIyIGFRQWFw4BHQEUFjsBOAExMjY1JyMiBhUUFjsBMjY1NCYjBzI2NTQmKwEiBhUUFjMEdDQl5gwNxf3aYDUlAjQlNP6En4oQFQMCBycZBw4H/vkBBQsGDhgJBwgECAUcMxXCBgcQC4JJhjgFIxgRGwoHIhYWIwcJHBAbJwEOC+AhGQwPJRsaJQ8MGiAIBaMFCOOvDBERDK8MEBAMIAwQEAyPDBAQDAMZ/mwlNBEqFwQHAwGG/rcmAXYlNDQl/ctYCiMUBw0FFhwCA2cCAgsKCBYLAQHzARIQkAQNCP5oDA8qJhceDwwUGRkTDA4kGmMLD/wgHS8KCRwQGiYmGhAcCQovHSAFCAgF6xEMDBERDAwRohAMDBERDAwQAAAAAAMAAAAmA/8DWgA/AF8AgQAAJSEiJicuATU0JzwBNTQ1NDY3NhYzMjY3PgE3PgEzMjMyFhceARceATM6ATc2FgcGFQYUFxQXFAYjJiMqASMiIxMwIjEiBw4BBwYVFBceARcWMzI3PgE3Njc0Jy4BJyYjFx4BFxQGIyImJyY2MzgBMTIWFy4BIyIGFRQWMzI2NTQmJwH//mkeLw4GBgE5LRo1Gh4YCQYNBgYXEqmpExcFBw4HBxcXGzUaMT0BAQEBATovMzMzZTMzMwEBNS4vRhQUFBRGLi81NS8uRhQUARQVRS8uNX8CAQEdEhQaAQEdEwYMBRU3H0BaWkA/Wg4MJxkaCxoNOjo5czo6OS05AQEBERwTJRMRERASFCkUFRIBAUIsODk5cTk5OSZCAQJmFBRGLi81NS8uRhQUFBRFLy41NS8vRhQUqwQIBBMcHBMTHAMDFBdZQD9aWj8YKxIAAAAFABP/wAPtA8AAKgA2AEIATwBoAAABIzU0JisBIgYdASE1NCYrASIGHQEjIgYVERQWMyE1PgE/AT4BNTMRNCYjBzUzFRQGKwEiJj0BITUzFRQGKwEiJj0BASMiBh0BISImNREhEQUWMjcBPgEvASYiDwEGIi8BJgYPAQYWHwEDizIkGQUaJP5QJBoFGSQ0KTk5KQK2BQgDpQQFBDkpsoAkGQUaJP3QgCQaBRkkAv9hGib9bgwRA1D+IAUPBQEvBQEGMwYPBuAGDwVvBhAFNgUBBr4DSjkZJCQZOTkZJCQZOTop/TwpOgIBBASrBAsFAl0pOiMZPRkkJBkkGT0ZJCQZJP2NJhpuEQwCKP5pNwUFAScGDwYzBgbZBQVoBgEHQQYPBacACQAW/8AD6gPAACsANwBDAFEAXQBqAHYAggCOAAATIyIGHQEjIgYVERQWMyEyNjc+AT8BPgE1ETQmKwE1NCYrASIGHQEhNTQmIwc1MxUUBisBIiY9ASE1MxUUBisBIiY9ARMRIyIGHQIhIiY1ESEFMjY1NCYjIgYVFBYhMjY1NCYjIgYVFBYzIzI2NTQmIyIGFRQWBzI2NTQmIyIGFRQWITI2NTQmIyIGFRQW5gYZJTQkNDQkAnoEBwIDBQPYAhA0JDQlGQYZJf5IJRlEgiUZBhklAjqCJRkGGSXfkR0r/YwRGAN2/VcXHx8XFiAgAfIWICAWFx8fF+4WICAWFiAg2BcfHxcWICABBBYgIBYWICADwCUZQDQl/S8kNAICAQMC2gMWBwIlJTRAGSUlGUBAGSWWFD4aJSUaKhQ+GiUlGir+7f65Kx2YARgRAf+7IBYXHx8XFiAgFhcfHxcWICAWFx8fFxYg6yAWFiAgFhYgIBYWICAWFiAAAAMAAP/ABAADwAAbACcAMwAAASIHDgEHBhUUFx4BFxYzMjc+ATc2NTQnLgEnJhMhERceATMyNj8BEREHDgEnBiYvATUhFQIAal1eiygoKCiLXl1qal1eiygoKCiLXl3B/arRFC4YGC4U0fgJGw8PGwn4AlYDwCgoi15dampdXosoKCgoi15dampdXosoKP0rARyRDQ0NDZH+5AFvqwcIAQEIB6s7OwAAAAMAr//AA1EDwAAaACoAPwAAATQ2MzIWHQEzNTQnLgEnJiMiBw4BBwYdATM1BSEiBhURFBYzITI2NRE0JgEVFAYjIiY9AS4BNTQ2MzIWFRQGBwF/SzY2S0oQEDclJSoqJSU3EBBKAZj90hgiIhgCLhgiIv75FxERFw0PKBwcKA8NAvU2TEw2aWkqJSU3EBAQEDclJSppaZIiGP3SGSIiGQIuGCL+rWoQGBgQagkdERwoKBwRHQkAAAAJAD0AOQMrA0cAOgB1AHoAfgCaALwAzgDgAOwAADcxIiYnLgE3PgE3PgE3PgE3PgE3PgEzMhYXFgYHDgEXHgEzMjY3PgEzMhYXFgYHDgEHDgEHDgEHDgEjEyIGBw4BBw4BBw4BBw4BBwYWFx4BMzEyNjc+ATc+ATc+ATc+AScuASMiBgcOASMiJicmNjc+AScuASMlFwcnNycXByclIiYnLgE3PgE3PgE3PgEzMhYXHgEHDgEHDgEjNyIGBw4BBw4BBwYWFx4BMzI2Nz4BMzgBMTI2NzY0Jy4BIwEiJi8BJjQ3NjIfARYUBw4BIzciJi8BJjY3NjIfARYUBw4BIwcWFAcGIicmNDc2Mv85XQMpLi4PKRUVJQ0HCQUFDAgSKRQQHAsNBgUGBgwKEwgFCgYFCwYOEAQPLhoNGQgFBgIFFyIPJBVPDBsNBwoEBgwKEisXEyIMISgjEUslDhgLHRMFAwgJChwPFxsEAgEDAggFBg4HFSEMEQgHBQMGBg4IARgY5RfkMhfLGAEGDBYJEA0BAykgCA8HHS8WCxMIDgoGBSMgET8lhQ8oGgcPCBQnAQEJCwQKBRY2EQIIBRcYAwIIBQkF/gIDBgMzBAUEDgUzBAUCBgMzAwcCMwUBBAUOBTIFBQIGAxoGBgUPBQUFBQ85TAMqlC8QEggHEQ4HGg4PHg0YGxAPFCEPDyQbFQoCAQECDgcdLBEIEgoGFg4aRSMPEAIHExIKGg4RHwsSFAgHDwwieiQSMgoLHjsZER0LCxUJEBcJAgEBAQEDGRokMRINEAgJCEcW7BfrNBbRF30KCRAfDxYmEwUKBBIaBwYKIBIQIAIeRr4WEAUJBQ0dDgcSCgUEOSIEBRIKAw4GAwP9pAIDNAUNBQUFNAUOBAMCNAMCNAUOBAUFNAUOBAMCWAUQBQYGBRAFBQASAAAAdgMzAwEACwAYACUAKgA2AEIATwBbAGcAcwB/AIsA7ADwAPQA+AD8AYgAADcUBiMiJjU0NjMyFgcyNjU0JiMiBhUUFjM1MhYVFAYjIiY1NDYzBzMRIxETMhYVFAYjIiY1NDYTMhYVFAYjIiY1NDYDMhYVFAYjIiY1NDYzFTIWFRQGIyImNTQ2JzIWFRQGIyImNTQ2EzIWFRQGIyImNTQ2ExQGIyImNTQ2MzIWJQ4BFx4BNz4BJy4BASc0JicuAQcOAQcxFRwBMTAUMRcHJiIHJz4BFxY2Jy4BJy4BBwYWMzIWFxY2JyY2NxcHFzceARceARcWNjc2NCcwJic3PgE1JzoBMzAWMR4BFQcOARUHMz8BFxQWHwE3JwEnNxcXJzcXFyc3FxcnNxc3PgE3PgE3PgE3PgE3PgE3PgE3PgE3PgE3PgE3PgE3PgE3OgEXMhYXHgEXMhYXHgEXHgEXHgEXHgEXHgEXHgEXHgExMCYnLgEnLgEnLgEnLgEnLgEnLgEnLgEnLgEnLgEnLgEnBiIHIgYHDgEHDgEHIgYHDgEHDgEHDgEHDgEHDgEHDgEHDgExFzA2N44QCwsQEAsLEBseKioeHioqHholJRoaJSUac+bm1wMEBAMDBAQDAwQEAwMEBGEOFBQODhQUDiQ0NCQlMzM/AwQEAwMEBAMDBAQDAwQEcwcFBQgIBQUHARwWDQ4NMhcWDA0OMgEqDA4jJ0AKFRwBATICBQOvBBINEQUSFDwFBEI6Ol0REhgKCxYNCg4EwQYLKAECAQECAgQKAwIBAgIwCQwBAQEBFxMJOgICBEIEKAkFBGAvWP20DRUMBAQVAwIDFAMCAxQDTwIDAgEEAgIEAgIFAwMFAwMHAwEEAQIEAgMHBAgQCAkRCQgSCAkRCAQIBAEEAgIDAgQHAwMGAwYLBAIFAgEEAQYHBgYBAwICBAIECgYDBgMDBwQBBAICAwIECAQIEQoIEwkKEgkKEggFCAQCBAICBAIDCAMEBgMEBgMCBgIDBAICBAEGBxcGBfoLEBALCw8PUikeHSkpHR4phCQZGiQkGhkkwQFX/qkBTAQCAwQEAwIE/swEAwMEBAMDBAEjFA0OExMODRRhMyMkMzMkIzNyBAIDBAQDAgT+zAQDAwQEAwMEAQIFCAgFBQcH/Q0yFhcMDg0yFhcM/nt/CT4sMhQBAR4UAgEBAWcLAQGMBw4GCRgOEBEREU5LSywlDAwIDwwXBI8BNAkCBAMCBQEDAQQDCAMDAgsCDglLGxgcAWYECASso0hZBQoEYy5bAVcKGgktAhsDKwIaAiwDGgJXAgMCAQMBAgMCAgMCAgMCAgMCAQEBAQEBAgICAgUBAgIBAgQBAQMBAQEBAQECAgIBBAEDBwMBAwIBAwEFBgYGAQMCAgMCAwgDAgQCAgMCAQIBAQEBAQQBAgYBAgIBAQEEAgEDAQEBAQIBAQMCAgMCAgMCAQQCAQQBAgICBQUZBgUAAAAEAAD/wAQAA8AAAwATACQAMAAAJSERITUhIgYVERQWMyEyNjURNCYDNCcuAScmIyIHDgEHBh0BIQEyNjU0JiMiBhUUFgOO/OQDHPzkL0NDLwMcL0NDvRwcUCwsICAsLFAcHAIA/wA1S0s1NUtLMgMcckMv/OQvQ0MvAxwwQv0OIBgYIAgICAggGBggKgEOSzU1S0s1NUsAAAMAAABGBJIDOgBPAJgA5QAAJSYnLgEnJic1NDY3PgE3PgE3PgE3PgE1NCYnLgEnLgEnLgEjIgYHDgEHDgEHDgEVFBYXHgEXHgEXHgEXHgEdAQYHDgEHBgcOAR0BITU0Jic3LgEnNTQ2NzQ2Nz4BNz4BNz4BNTQmJy4BJy4BJy4BIyIGBw4BBw4BBw4BFRQWFx4BFx4BFx4BFx4BFxUOAQceARc6ATE1NCYnJS4BJzU0Njc+ATc+ATc+ATc+ATU0JicuAScuAScuASMiBgcOAQcOAQcOARUUFhceARceARceARcWFB0BBgcOAQcGBw4BHQEwMjM+ATcDlwMkI1YkJQQBAQIDAQIEAggLBQQFBgYFEAoJFw0NGw8PHA0MFwoJEAUGBQQEBQwHAwMCAQMBAQIEISBOICEFCVUDJjsLtgWIBgEBAgIBAwEFCQMDAwQEBAsHBxAJCRMKCxMJCg8HBwsEBAQEAwMIBgEDAQEBAQEBAQZPBChrV1pxPwb9awVQBgEBAQIBAQICBQkDAwMEBAQLBgcQCQkUCgsTCQkQBwcLBAQEAwMDCQUCAgIBAgEBAhoaPBkaAQgqcVpXbyf1ARAQJhARAkECBwUFCgUGDQcPIRMTJxUcMBMUIAwMEQUGBQUGBRIMDCATFC8cFSkTEyEPBwwFBQkFBAcCPwIPDyMPDwIENjdJSTcqBZQDPQMuAQUEAwcEBAkFCxgNDRwOFCINDhYJCA0DBAQEBAQMCQgWDg4hFA8cDg0YCgUIBAMHAwMFAiwDOwElLUE8JCoCBwE6AywCBAMEBgQDCQQLGA0NHQ8TIg0OFgkIDQQEAwMEBAwJCBYODiEUDxwNDRgKBQkEBAcEAwUCLQEMDBsLCwEDISQ7QS4kAAACAAAARAMyAzwACQAQAAAlIREhNSERIREjAxcBFwEXEQKQ/eQBDv5+AwNzrn7+xFIBP324AeFz/TgBZAGUdv7EUgE/dQE6AAADADr/wAPGA8AABQAJABUAAAEnByc3FyUzESMBIREzFSMRIREjNTMCpqamTfPz/tVwcAH+/HT0gwKqg/QCiZ2dUebmmf1p/uQCpHH+PQHDcQAAAAAFAAAABwQAA3kACwAqAEcAUgBeAAABFAYjIiY1NDYzMhYDIicuAScmLwE3PgE3PgEzMhYXHgEfAQcGBw4BBwYjJRYXHgEXFjMyNz4BNzY3JicuAScmIyIHDgEHBgcTNxceARcLAT4BNwEXITcuAScHIScOAQJpPiwsPj4sLD5pTkREZR4fAhUTAToyQ4pCQopDMjoBExUCHx5lRERO/tASHx9PLy8zMy8vUB4fEhIeH08wLzMzLzBPHx4S9Dw8FzAYm5sYMBcBgz/89D8PHg1/BAB/DR4Bayw9PSwsPj7++BscQx0dAxYYAUEnNTY2NSdBARgWAx0dQxwb0REYFywPEBAPLBcYEBMcHDMSExMSMxwcEgElZ2cDDAkBDP70CQwD/jVqagoVCtzcChUAAAAAAQAAACYDWgNaAAcAABMBNSERITUBAAGtAa3+U/5TAcD+ZsMBrsP+ZgAAAAADAD//4gPeA4EAJABBAE8AAAE2NzYmJyYnJicmIgcGBwYHBhQXFhcWFx4BNzY3ARYyNzY0JwEFJicmNDc2NzY3NjIXFhcWFxYUBwYHBgcGIicmJyUyNjU0JiMhIgYVFBYzAtEhDQ0OGhsuNkREjkRENzYbGxsbNi84OXg9PDUBDRpIGRkZ/vP9/SoUFRUUKik0NWw0NCopFRQUFSkqNDRsNTQpAUsSGRkS/vsTGRkTAWo1PD14OTgvNhsbGxs2NkVEjkRENi4bGg4NDSH+8xkZGUkZAQ0IKjQ0bTQ0KSoUFRUUKik0NG00NCopFRQUFSmcGhISGRkSEhoAAAADAD//4gPeA4EAHwBEAGEAAAEzMjY1NCYrATU0JiMiBh0BIyIGFRQWOwEVFBYzMjY1BTY3NiYnJicmJyYiBwYHBgcGFBcWFxYXHgE3NjcBFjI3NjQnAQUmJyY0NzY3Njc2MhcWFxYXFhQHBgcGBwYiJyYnAcJXEhkZElcaEhIZVxMZGRNXGRISGgEPIQ0NDhobLjZERI5ERDc2GxsbGzYvODl4PTw1AQ0aSBkZGf7z/f0qFBUVFCopNDVsNDQqKRUUFBUpKjQ0bDU0KQH+GhISGVgSGRkSWBkSEhpXEhkZEj01PD14OTgvNhsbGxs2NkVEjkRENi4bGg4NDSH+8xkZGUkZAQ0IKjQ0bTQ0KSoUFRUUKik0NG00NCopFRQUFSkAAAAAAwAAAAYEAAN6AAMABwALAAABIzUzESM1MwUhCQECL15eXl790QQA/gD+AAFMuv6LXukDdPyMAAAJAAD/wAQAA8AACwAXACMALwA7AEcAUwBfAGsAABMUBiMiJjU0NjMyFhEUBiMiJjU0NjMyFgEUBiMiJjU0NjMyFhEUBiMiJjU0NjMyFgEUBiMiJjU0NjMyFhEUBiMiJjU0NjMyFgEUBiMiJjU0NjMyFgUUBiMiJjU0NjMyFgUUBiMiJjU0NjMyFvRIMjNHRzMySEgyM0dHMzJIAYZIMjJISDIySEgyMkhIMjJIAYZHMzJISDIzR0czMkhIMjNH/PRIMjNHRzMySAGGSDIySEgyMkgBhkczMkhIMjNHA0YySEgyM0dH/kcySEgyMkhIAVQySEgyM0dH/kcySEgyMkhIAVQySEgyM0dH/kcySEgyMkhI/kgzR0czMkhIMjNHRzMySEgyM0dHMzJISAAEAAD/wAQAA8AAAwAHAAsADwAAAREhESERIREDESERIREhEQGr/lUEAP5Vqv5VBAD+VQIVAav+VQGr/lX9qwGr/lUBq/5VAAAGABD/9AQAA4wARgBdAGsAfACKAJsAAAEiBhUUFjMyFx4BFxYVFAcOAQcGIyInLgEnJicXHgEzMjY3NjQvASYiDwEGFBcWMj8BFhceARcWMzI3PgE3NjU0Jy4BJyYjAyImJyY0PwERNDYzMhYVERQGDwEOASMlIyImNTQ2OwEyFhUUBgciJi8BJjQ3NjIfARYUBw4BByImPQE0NjMyFh0BFAYTIiYnJjQ/ATYyFxYUDwEOAQI0EBgYEE9FRWcdHh4dZ0VFT0hAQGQhIQkcBg8ICA8FDAxkDCILZQwMDCEMIggnKHpQT1lfVFR9JCQkJH1UVF+YCBAGDQ2GGRIRGQYGkwYQCAHKLQsREQstCxERdQULBB8ICAgYCB8ICAQK4wwQEAwMEBCyBgoECAgfCBgICAgfBAsDjBgREBgeHmdFRU5ORUVnHh4ZGlg8PEUcBgYGBgwhDGUMDGUMIQwMDCJXTEtwISAlJH1TVF9fVFN9JCX9VAcGDCQMhgEPEhkZEv7gCRAGkgYHxhAMDBAQDAwQ3gQFHwgYCAgIIAgXCAUEWxAMLAwQEAwsDBAB9wQECBgIHwgICBcJHwQEAAAGAAD/ywQAA7UADAAZADcAXwB+AIwAABMUFjMyNjU0JiMiBhUzFAYjIiY1NDYzMhYVEyERIRUUFjMyNj0BNCYjISIGFREUFjMhMjY1NCYjPwEXFjI3NjQvASYiDwEnLgEjIgYjBwYUFx4BMzI2PwEXFjI3NjQvASU0JiMiBh0BJyYiBwYUHwEeATMyNj8BNjQnJiIPATUTISIGFRQWMyEyNjU0Jq4/Li4/Py4uP68kHhonJxoaKCv+zwKOGBMUGCYX/T4XJiMaAUsUGBgUaVNFBxEHBgZXBBMEZCcDCAYECwOuBwcDCAYECgSdUgcRBwYGGgFLGBQTGB8NIw0NDWkGDwkKDgdoDQ0NIw0emP54ExgYEwGIFBgYApouP0IrLj8/LhcqJxoaJyca/jICktoUFxcU9BcmJhf9OhojGBMUGNVTRQcHBhIGVwcHZCcDAgWyBxEHAwEBA6FTBgYHEQceHxQYGBTjHw0NDSMNZAcGBgdkDSMNDQ0f4/5iGBQTGBgTFBgABQBR/8ADmgN6ACgAVQBpAG0AcQAAASc2NzYmJyYnJicmBgcGBwYUFwcmJyY0NzY3Njc+ARcWFxYXHgEHBgcFJicmNDc2NzY3PgEXFhcWFx4BBwYHJzY3NiYnJicmJyYGBwYHBgcGFBcWFwclEyMnIQcjEy4BJyY2NzYWFxYGBxcnBzMXJyMHAtc9GgkJDBUVJCMoKU8kJBooKD0bDg4ODhsjMTBtNzcwMBwdEQwNI/25KhUVFRUqNUtKplRVSUksLBoTEzU9LA8QFSUkPTxGRok9PiwjERERESM9AYjLURn+8BlRyg8ZBxAbJCRKEBEcJBQ7PHc8K5kqASktIygpUCMkGhkKCQ0VFSM3iTctJisrWSsrJTAdHRENDCMjMTBtNzcwWTpCQodCQjlJLSwZExM1NUtKplRVSS08RkaJPT4sLBAQFiQlPDA2N3A2Ny8tq/5FNzcBuwcYECRJERAbJCRKEN2CgoNeXgAAAAYAAAAmAzMDWgANABsALQA7AEwAeAAAATQmIyEiBhUUFjMhMjYHITI2NTQmIyEiBhUUFic0JisBIgYVFBY7ATgBMTI2NQUhIgYVFBYzITI2NTQmEyEOARURFBYXIT4BNRE0JicDISImNREhMjY1NCYjITUhMjY1NCYjITU0NjMhMhYVESEiBhUUFjMhFRQGIwH0Dwz+4wsQEAsBHQwPhQE/CxAQC/7BCxAQSBALYAsQEAtgCxABE/7jCxAQCwEdCxAQk/2ZKjw8KgJnKzs7KgH9mRQcAVELEBAL/q8BOQsQEAv+xxwUAmcUHP7+CxAQCwECHBQCYAsQEAsLEBDkDwwLEBALDA8bCxAQCwwPDwy6EAsLEBALCxACiAE7Kv2YKjsBATsqAmgqOwH9AxsUAU8QCwsQnhAMCxAOFBsbFP5JEAsMEHoUGwACACP/8QPcA48AJwBDAAAlFTwBNS4BNTQmJzgBOQEuAScOASMiJicOAQcOATEGFjMhMjYnMCYnJTI3PgE3NjU0Jy4BJyYjIgcOAQcGFRQXHgEXFgPQAQEBARhqVyt+Skp+KlttFggFAU04ArI4TQIECP4wNS8vRRQVFRRFLy81NS8uRhQUFBRGLi/fAgEBAQEEAQIEAmRSKzZAQDYtV24qOjhQUDg/J68UFEYvLjY1Li9GFBQUFEYvLjU2Li9GFBQAAAAAAwBB/8wDvwOkABsAMQA/AAABJSYiBwUOARURFBYXBR4BMzI2NyU+ATURNCYnARUjNQEjIgYHDgEHDgEHDgEHJzchFQMjNT4BNz4BNz4BNxcHA4f+sRo8Gv6xGh4eGgFPDRwPDxwNAU8aHh4a/ed3AZKEJDgVFCUQDhQHBwkDCgoB7QHHIzcTGCUPBgkDCg4C48EQEMEQNB3+fB01D8EICAgIwRA0HQGEHTUP/b0OCgJcBQUFEg0LFw0NHxEBqAv9lhIFEAwPLB4MHhEBxgAAAAQATP/cA7MDlQAbADsAUgBgAAAFIiYnJS4BNRE0NjclNjIXBR4BFREUBgcFDgEjESIGBwUOARURFBYXBR4BMzI2NyU+ATURNCYnMSUuASMDMQE1IQcXPgE3PgE3PgE3PgE7AQEVMyUOAQcOAQcOAQcVMzcjAgAPHw3+whsgIBsBPhs/GwE+GyAgG/7CDR4PDBgL/sIWGRoVAT4LGAwMGAsBPhYZGhX+wgsYDIoBef4tCwsCCQYHEw0PIxQTNSJ9/oRxAXwDCQYOIxYTMyG9DQokBwi3EDcgAW4gNxC3EBC3EDcg/pIgNxC3CAcDsgYGuA0rGv6SGiwMtwcGBge3DSsaAW0aLAy4Bgf9FgI8Cp8BER0NDBYKDBEEBQb9wwq8DxwMHCoOCxAEEbsAAAUAAP/ABAADwAAeADUAQwBHAGMAAAElLgEjIgYHBQ4BFREUFhcFHgEzMjY3JT4BNRE0JicBFSM1ASMiBgcOAQcOAQcOAQcnNyEVAQUjNT4BNz4BNz4BNzMHAREhEQMUBgcFDgEjIiYnJS4BNRE0NjclNjIXBR4BFREDbf7CCxgMDBgL/sIWGRoVAT4LGAwMGAsBPhYZGhX+CXEBfH0iNRMUIw8NEwcGCQILCwHT/ocBeb0hMxMWIw4GCQMKDf0RBABNIBv+wg0eDw8fDf7CGyAgGwE+Gz8bAT4bIALJuAYHBga4DSsa/pIaLAy3BwYGB7cNKxoBbRosDP3bDgoCPQYFBBEMChYMDR0RAZ8K/cQNEQQQCw4qHAwcD7sDKfwABAD9SSA3ELcIBwcItxA3IAFuIDcQtxAQtxA3IP6SAAAACwAq/+gMBAOYACMATQBQAGoAhACgAPIBRAF1AZMBowAAJQEjLgE1MTQmNTwBNzQ2NRUhFwEhHgEXMRwBFRwBFQ4BBzUhJSMHDgEjBiIjKgEnIiYnMxM+ATc6ATM6ATMeARcTDgEjBiIjKgEnIiYnJzMDJT4BNzoBMzoBMx4BFxEOASMGIiMqASciJicTPgE3OgEzOgEzHgEXEQ4BIwYiIyoBJyImJxsBPgE3OgEzOgEzHgEXAxMOAQciBiMiJiMuAScFKgEjIiYnFy4BLwEuAS8BLgE1PAE1MTwBNTQ2Nwc+ATcHPgE3Mz4BMzIWFyceAR8BHgEXFR4BFRwBFTUcARUUBgc1DgEHNw4BDwEOASMqASMxNToBMzI2NyM+ATcxPgE3NT4BNTwBNRU8ATU0JicVLgEnFS4BJzUuASMiBgc3DgEHMQ4BBxUOARUcARUxHAEVFBYXNR4BFzUeARczHgEzOgEzMQElLgEjIgYHMQUOARU4ATkBETgBMRQWFzMFHgEzMjY3FSU+ATU4ATkBETgBMTQmJyMBMRUjNQEjKgEjIgYHMw4BBzEOAQcVDgEHFSc3IRUDIzU+ATcHPgE3NT4BNzUzA9EBHPkBAQEBAgFuBf7kAQgBAQEBAQH+gwL+1icFCgUFCgUFCwYFCgUBtwULBgYMBwYKBgYMBbYFCgYGCwYFCgYFCgXnrFcBOQUKBQUKBQULBQUKBAQKBQUKBgUKBQYJBdsECgUFCwUFCgUFCgUFCgUFCgUFCwUGCQRntQYKBgUKBAQMBgYMBrPKBwwGBQwGBQoFBgsGAWwBAwIbMxcBFiUOAQ4XBgEGCAgHAQcXDwEPJRUBGDUdHTYYARYlDgEOFgcHBwcHBhcPAQ8lFQEWMxsCAwEBAgERIQ8BDhgJCg4EBQUFBQQOCgkYDg8jEhMiEAEOGAkKDgQFBQUFBA4KCRcOAQ4gEgECAfgs/sQMGw4PGwz+xBgeHRgBAT0LGw8OGwwBPBgeHRgB/gVxAXx9AgYDGjAYAxQjDwsUBwcJAgoKAdIBvB00FwIYJAwGCAMKpQHnAwgFBQkEBAoEBQkEAQf+GgQIBQUJBAUJBAUJBAGCggEBAQEBAQIzAQEBAQEB/c0BAQEBAQHIAR9MAQEBAQEB/c0BAQEBAQECMwEBAQEBAf3NAQEBAQEBAR4BFQEBAQEBAf7x/twBAQEBAQEBAQkLCwELHxIBFC0ZAhk6HgECAQECAR46GwMbLhQBFB4LCwsMCwELHhMBEy4ZAhk5HgEDAQEBAgEeOhwDGi8UARMfCgEKC0UIBwgWDg8jEwEULBcCAgIBAQIBGC0WAxQjEAEOFgcBBwgICAEIFg4PIxMBFC0XAQIBAQICFy0WAxQjEAEOFggIBwH5twcHBwe3DjEd/pIdMQ63BwcIBwG3DjEdAW4dMQ793Q0KAjoFBQURDAkWDAENHQ8BAZ4K/bkRAxANAQ8qGgEMGw8BAAAAAAUAZ//AA5oDwAADAAcADAAQABQAAAERARclEQEXJTcXBycFBxchJRchJwN6/Y2CAYv+k4L+PoODg4MCYoNOAQb9bU4BBNACuwEF/Y2DTAEG/pGDg4ODg4MegU5OTs8AAAwAAAAKDVUDdgADAAcADAAQABQAGAAdACgALgA4AGEAagAAExcBNQMBFzcFNxcHJwUXMycFFzMnBTMRIzUzNSMVEzM1MzUjNSE1JREBIxEhNSEBCwEjARUzNQEjAS4BJzU+ATU0Jy4BJyYjIREhNTM1IzUhNSEHESERMzIWFx4BFzMuAScnIzUzMhYVFAaKbwGqV/7GcMr9tHBwcHABm0Lgs/6AQt+yAiG9vb29573s7AEQ/jMCsLwBxP74AhuRkNcBCL0BCNUEbgQnMzMzFBRAKSkr/vH+0OjoAQv+onACjE08JgMBBwyoEQgC8WBZJDIoAV5wAanf/pj+x3DJWXBwcHCJQrJwQrKyAc4sp6f+BvuoV6cF/VoCoP1gpgH6/u8BEf5b+/sBpf33MHMNAhRPNzMkJC0LCv4GVahXp7H+EQEDZTMOTRARcBX0iR8mJh4AAAABAAAAAQAAsk34g18PPPUACwQAAAAAANjbjTIAAAAA2NuNMv/+/78NVQPAAAAACAACAAAAAAAAAAEAAAPA/8AAAA1V//4AAA1VAAEAAAAAAAAAAAAAAAAAAABiBAAAAAAAAAAAAAAAAgAAAAQAABcEAADdBAAABAQAAAAEAAAABAAAAAQA//4EAABFBAAAAAQAAO4EAAADBAAAAgQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAWwQAAFsEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAagQAAHEEAACFBAAACwQAAIcEAACGBAAAAAQAACgEAAAlBAAAAAQAAAAEAAAABAAAHQQAAB0EAAAPBAAAAAQAAAAEAAAuBAAAAAQAABQEAAAPBAAAAAQAAAAEAAAABAAAAAPDAAAEdAAABAAAAAQAABMEAAAWBAAAAAQAAK8DMwA9AzMAAAQAAAAEkgAAAzIAAAQAADoEAAAAA1oAAAQAAD8EAAA/BAAAAAQAAAAEAAAABAAAEAQAAAAEAABRAzMAAAQAACMEAABBBAAATAQAAAAMLwAqBAAAZw1VAAAAAAAAAAoAFAAeAP4BIAFeAk4CkAMQA7QEUgTaBPwF/gZmB+4IVAiUCYoKGgq6C7AMGg0QDZAOhg7YDyoQRBFeEngTkhQ+FN4WzhfEGC4YmBjmGToaDBwoHywgUiJMJGQleiXaJhgmZicmJ1ooMChsKKYpyio+KqIrriwCLEgsdCzoLP4tHi04LZIuYi8SL6QwYDC0MRAybDSaNOY2MjZWNoA3GDcuN6w4PjhaOPA5FDnyOrQ7ajwOPHA82D1uPg5AOEBoQRIAAAABAAAAYgJLABIAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAADgCuAAEAAAAAAAEACwAAAAEAAAAAAAIABwCEAAEAAAAAAAMACwBCAAEAAAAAAAQACwCZAAEAAAAAAAUACwAhAAEAAAAAAAYACwBjAAEAAAAAAAoAGgC6AAMAAQQJAAEAFgALAAMAAQQJAAIADgCLAAMAAQQJAAMAFgBNAAMAAQQJAAQAFgCkAAMAAQQJAAUAFgAsAAMAAQQJAAYAFgBuAAMAAQQJAAoANADUemFpa28taWNvbnMAegBhAGkAawBvAC0AaQBjAG8AbgBzVmVyc2lvbiAxLjAAVgBlAHIAcwBpAG8AbgAgADEALgAwemFpa28taWNvbnMAegBhAGkAawBvAC0AaQBjAG8AbgBzemFpa28taWNvbnMAegBhAGkAawBvAC0AaQBjAG8AbgBzUmVndWxhcgBSAGUAZwB1AGwAYQByemFpa28taWNvbnMAegBhAGkAawBvAC0AaQBjAG8AbgBzRm9udCBnZW5lcmF0ZWQgYnkgSWNvTW9vbi4ARgBvAG4AdAAgAGcAZQBuAGUAcgBhAHQAZQBkACAAYgB5ACAASQBjAG8ATQBvAG8AbgAuAAAAAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==) format('truetype'),
	/*savepage-url=fonts/zaiko-icons.woff?v233xx*/ url(data:application/font-woff;base64,d09GRgABAAAAAIisAAsAAAAAiGAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAABPUy8yAAABCAAAAGAAAABgDxIGuGNtYXAAAAFoAAAAdAAAAHTqn6ZfZ2FzcAAAAdwAAAAIAAAACAAAABBnbHlmAAAB5AAAgiQAAIIkvqHDzmhlYWQAAIQIAAAANgAAADYeKV7gaGhlYQAAhEAAAAAkAAAAJBEVDXhobXR4AACEZAAAAYgAAAGIjHMJsmxvY2EAAIXsAAAAxgAAAMZN4iqcbWF4cAAAhrQAAAAgAAAAIAB1Ak1uYW1lAACG1AAAAbYAAAG2g5LzEXBvc3QAAIiMAAAAIAAAACAAAwAAAAMEIgGQAAUAAAKZAswAAACPApkCzAAAAesAMwEJAAAAAAAAAAAAAAAAAAAAARAAAAAAAAAAAAAAAAAAAAAAQAAA6WwDwP/AAEADwABAAAAAAQAAAAAAAAAAAAAAIAAAAAAAAwAAAAMAAAAcAAEAAwAAABwAAwABAAAAHAAEAFgAAAASABAAAwACAAEAIOk56UbpXelk6Wz//f//AAAAAAAg6QDpP+lI6WHpa//9//8AAf/jFwQW/xb+FvsW9QADAAEAAAAAAAAAAAAAAAAAAAAAAAAAAQAB//8ADwABAAAAAAAAAAAAAgAANzkBAAAAAAEAAAAAAAAAAAACAAA3OQEAAAAAAQAAAAAAAAAAAAIAADc5AQAAAAAFABcALQPgA00AMABeAG0AggCYAAA3BiInJjQ3MDc+ATc2MTY0JyYiBzEGFBcWMjcxNjIXFhQPAQYUFxYyNzE2NCcmIgcxATAnLgEnJjEuASMiBgcwBw4BBwYxBhQXMBYXBhYXHgE3HgExHgE3AT4BNzYmJycDDgEnLgE3Ez4BFx4BBzceARcwFx4BFxYxHgEXFjY3AQ4BFwU2NzY0JyYnJicmIgcGBw4BBwE+ATfBFT0VFhYREisREiUlJWolCQkIFggVPRUWFnElJSVqJggICRcHAisYGTwZGQIHAwMHAjExdjExBQYDAgwBDA0mDwICBxUIAX4DAwEBAwOt/QQMBAQCBP0DDAQFAgQoBAYCGRk8GRkCBQMXLhX/AAkIAQFPHA4PDw4cHSQkTCQkHAkOBgENChMIVBYWFT0VEhIqEhIlaiUlJQgWCAkJFRUWPBZxJWomJSUJFggICAFnGBk8GRkCAwMDPDyPPDwHFQcDAQ4kDQ4DDAICBwEGAToDBgQCBgNP/swEAgQDCwUBNAQCBAQLBIACBAQYGTwZGQIHAwIICQEBFi4XfRwlJEslJBwdDg4ODh0IEgr+8gcOCAAAAQDd/9gDCQOtABAAABMBMDYXFgYxCQEwFgcGJjEB3QH3IhMUFP5AAasQEhIn/iQBtgH3DBMUHf5A/lUgEhISAd4AAAACAAT/xAP8A7wAGwAgAAABJgcOAQcGBwYXHgEXFhcWNz4BNzY3NicuAScmATcnJQECwmFkZbRISSgoAQJLRkZhYWRltEhJKCgBAktGRv44ZvYCVf47A5UoAQJLRkZhYWRltEhJKCgBAktGRmFhZGW0SEn9Q/ZmLv52AAAADQAAAAAEAAOAAAsAFwAnADMAPwBQAGAAcAB8AIgAmACkALAAAAEUBiMiJjU0NjMyFhUUBiMiJjU0NjMyFhMhIgYVERQWMyEyNjURNCYDIiY1NDYzMhYVFAYnIiY1NDYzMhYVFAYlERQWOwEyNjURNCYrASIGFSERFBY7ATI2NRE0JisBIgYBIyIGFREUFjsBMjY1ETQmAyImNTQ2MzIWFRQGJyImNTQ2MzIWFRQGJSMiBhURFBY7ATI2NRE0JgMiJjU0NjMyFhUUBiciJjU0NjMyFhUUBgIrGRISGRkSEhkZEhIZGRISGWr+1gkNDQkBKgkNDZ4jMjIjIzIyIyMyMiMjMjL93QwJ1ggNDQjWCQwDAAwJ1ggNDQjWCQz969YIDQ0I1ggNDHQSGRkSEhkZEhIZGRISGRkDWdYIDQwJ1ggNDXMSGRkSEhkZEhIZGRISGRkBqxIZGRIRGRnnERkZERIZGQFuDAn91QkMDAkCKwkM/isyIyQyMiQjMtUyJCMyMiMkMhb+qgkMDAkBVggNDQj+qgkMDAkBVggNDQINDAn+VQkMDAkBqwgN/tUZEhIZGRISGYAZEhIZGRISGasMCf5VCQwMCQGrCA3+1RkSEhkZEhIZgBkSEhkZEhIZAAAAAQAAAAcEAAN5ACoAAAEiBw4BBwYxMCcuAScmIyIHDgEHBhUUFx4BFxYxMDc+ATc2NTQnLgEnJiMC6UEtLTYMDAwMNi0tQUE0NEgTE1BQwFBQUFDAUFATE0g0NEEDeRgYOhkYGBk6GBgcHFg2NjNWZWWuOzo6Oq9lZVYzNjZYHBwAAAAABAAA/8AEAAPAABEAHwA8AFgAAAEjIgYVERQWOwEyNjURLgEjMQMiBhUUFjMyNjU0JiMxNyIHDgEHBhUUFx4BFxYzMjc+ATc2NTQnLgEnJiMRIicuAScmNTQ3PgE3NjMyFx4BFxYVFAcOAQcGAiNGFBsbFEYUGwEbEyQoODknKDk4KQFqXV2LKSgoKYtdXWpqXV2LKSgoKYtdXWpaT1B2IiMjInZQT1paT092IiMjInZPTwH8HBL+1BMbHBIBLBMbAQ85KCg4OCgoObUoKYtdXWpqXV2LKSgoKYtdXWpqXV2LKSj8TCMidlBPWlpPT3YiIyMidk9PWlpPUHYiIwAAAAb//v/MA/4DugAHACgASQBWAHAAfQAAATAUMTgBMTUBFBYzMjY9ATMyNjU0JisBNTQmIyIGHQEjIgYVFBY7ARUBNTgBMS4BJw4BIyImJw4BBw4BMQYWMyEyNicwJic4ATERMjY1NCYjIgYVFBYzNw4BIyImJx4BFRQGBx4BFzMyNicwJicuAScFFBYzMjY1NCYjIgYVA/78fRkSERlSEhkZElIZERIZUhIZGRJSAj4PSDwdVTIyVhw9SA8GAgI0JgHPJjQBAwZIZWVIR2VlR6QcVjIeOBcDBA0ML0kW2SYzAQMFD0g9/XhlSEdlZUdIZQFkAQEBMhIZGRJRGhESGVISGRkSUhkSERpR/dQBSToeJCwsJB46ShwnJTY2JSkaAV9lSEdlZUdIZSskLBEPDRsOGzIWFzEvNiUqGkk6HmZIZWVIR2VlRwADAEX/wAO7A8AALgA7AGoAAAEuAScuAScuASMiBgcOAQcOAQcOARUUFx4BFxYXHgEfATc+ATc2Nz4BNzY1NCYnBSImNTQ2MzIWFRQGIxcOAQceARUUBw4BBwYjIicuAScmNTQ2Ny4BJw4BFRQXHgEXFjMyNz4BNzY1NCYnAwILHhMULBkaNxwcNxoZLBQTHgsLCwkIIhgZIjBSAQ8PAVIwIhgZIggJCwv+/i0/Py0tPz8t7QoUC09bGxthQ0NRUUNDYRsbW08LFApdcSIjd1FRXV1RUXcjInFdAxUZLRMTHgsLCwsLCx4TEy0ZGjYdFx8fTC0tNExuARMTAW5MNC0tTB8fFx02Gs1ALS0/Py0tQMURIhEYTCceHh0wDw8PDzAdHh4nTBgRIhEgbUI0LCxCExMTE0IsLDRCbSAABQAA/8AEAAPAABsANwBEAFEAXgAAASIHDgEHBhUUFx4BFxYzMjc+ATc2NTQnLgEnJgcyFx4BFxYVFAcOAQcGIyInLgEnJjU0Nz4BNzYDIgYVFBYzMjY1NCYjISIGFRQWMzI2NTQmIyEiBhUUFjMyNjU0JiMCAGpdXYspKCgpi11dampdXYspKCgpi11daltPUHciIiIid1BPW1tPUHciIiIid1BPqyQ0NCQkMzMkAQYkMzMkJDMzJAEGJDMzJCQ0NCQDwCgpi11dampdXYspKCgpi11dampdXYspKEsiIndQT1tbT1B3IiIiIndQT1tbT1B3IiL+ojMkJDMzJCQzMyQkMzMkJDMzJCQzMyQkMwABAO7/1gMaA6sAEAAACQEwBicmNjEJATAmNzYWMQEDGv4JIhMUFAHA/lUQEhInAdwBzf4JDBMUHQHAAasgEhIS/iIAAAQAA//DA/sDuwAjAIIAjgCoAAAlAiU+ATc+ATc+ATc+ARceARceARceAQcOAQcOAQcOAQcGFAcFNx4BFx4BFxY2Jy4BJy4BJyYnLgEnJicuAScuAScmBhceARceARceARcHMCYjLgEnLgEnLgE1NDY1PgE3MjYzMhYXHgEXFhceARcWFx4BFx4BFRwBBw4BIyImJy4BJzcUBiMiJicmNhceAQEnPgEnLgEnJgYHLgEnLgEnPgEXHgEXFgYHAuCu/sMWJxIeOR0hRCcYMxojPxwpQxkeBhYQLRoWLxYNFQQBAf7rHAoUCwwaDBERAwIIBRAuGh4fH0MkJCcVLhcIEQkREgMCCAUTNh4PHQ0iAQEzWiIPFQMBAQEEHhwDCAQOGg0rUSU1MDBYJyciFiMIAgIBAiMZER8PIj8fAUQwLkIBAUE2MT8B+yAQDgUEFRAQHQwGCwYGDAYQPyEkMQYGHhuyAT2wARIOFy8XGzAQCwsBARYTHEUrM2k3JkIgHDgcESYWAwgCqSoFCwQFBwICFBAOGAwkRCAkIiNAHR0bDhgMBAMBARISDhkMKkskESEPJAE2dUIaOB4ECgUECAQbGwMBBAQOKholKSlaMjE2I0koCRQKBAcEGSIFBAkhE2kwQ0QvKksBAUYCUS4LHxQQEwMEDhEECQQDCAQdHQYGMiUgPw8AAAIAAv/CA/IDvgAmAEIAACUBPgE1NCcuAScmIyIHDgEHBhUUFx4BFxYzMjY3AR4BMzI2NzY0JwEiJy4BJyY1NDc+ATc2MzIXHgEXFhUUBw4BBwYD8f6kJSsdHWREQ0xNQ0NlHR0dHWVDQ01BdS8BXQYPCQgPBwwM/YI7NDRNFxYWF000NDs6NDROFhYWFk40NAoBXi90QUxEQ2UdHR0dZUNETE1DRGQdHi0m/qIGBgYGDSMMASQXFk40NDs7NDRNFxYWF000NDs7NDROFhcAAAAEAAD/wAQAA8AAZAD4ARQBIQAAATIWHQEeARc3NjIfARYUDwEeARczMhYdARQGKwEOAQcXFhQPAQYiLwEOAQcVFAYrASImPQEuAScHBiInIyc4ATEmND8BLgEnIyImPQE0NjsBPgE3JyY0PwE2Mh8BPgE3NTQ2OwEVIyIGHQEUBgcOAQcGJi8BJiIHOAExBwYUHwEeAQcOAQcOASsBIgYdARQWOwEyFhceARcWBg8BBhQfARYyPwE+ARceARceAR0BFBY7ATI2PQE0Njc+ATc2Fh8BFjI/ATY0LwEuATc+ATc+ATsBMjY9ATQmKwEiJicuAScmNj8BNjQvASYiDwEOAScuAScuAT0BNCYjBzIXHgEXFhUUBw4BBwYjIicuAScmNTQ3PgE3NhciBhUUFjMyNjU0JiMCRCEvBg0GHBdDF2AXFxwDBQMnIS8vIScDBQMcFxdgF0MXHAYNBi8hiCEvBg0GHBdCFwFgFxccAwUDJyEvLyEnAwUDHBcXYBdDFxwGDQYvIYiIBQcNCxAfDgoWCCwECQNhBAQsCAQFCA0FAxMLPgYGBgY+CxMDBQ0IBQQILAQEYQMJBCwIFgoOHxALDQcFiAUHDQsQHw4KFwgsAwkEYAQELAgDBQgMBQMTCz4GBgYGPgsTAwUMCAUDCCwEBGAECQMsCBcKDh8QCw0HBUQxLCtBExMTE0ErLDExLCtBExMTE0ErLDFHZGRHR2RkRwPALyEnAwUDHBcXYBdCFxwHDQYvIYghLwYNBhwXQxdgFxccAwUCKCEvLyEoAgUDHBcXYBdDFxwGDQYvIYghLwYNBh0XQhdgFxccAwUDJyEvRAYGPgsSBAUMCAUEBywEBGAECQMtCBYKDx4QCw0HBYgFBw0KEB8PChYILAQJBGAEBCwIAwUIDAUDEws+BgYGBj4LEwMFDAgFAwgsBARgBAkELAcXCg4fEQoNBwWIBQcNCxAfDgoXCCwDCQRgBAQsBwQFCAwFBBILPgYGzRMTQSssMTEsK0ETExMTQSssMTEsK0ETE0RkR0dkZEdHZAAAAgAA/8AEAAPAACgARQAAEyIGFREUFjMhMjY1ETQmJzEmBhURFAYjISImNRE0NjMhMjYnMS4BIyEFFxYUBwEGFBcxFjI3ATYyHwEWNjURNiYjISIGF6tHZGRHAqpGZQkIG0YhGP1WGCEhGAEEJxwbCBQM/vwB7GIHB/6VExMUOBQBawcRBmILGwENCf69DwsKA8BkR/1WRmVlRgEECxUIGhwm/vwYISEYAqoYIUYbCAkmYQcSBv6UFDgUExMBawcHYgoLDwFDCQ0bCwAAAwAA/8AEAAPAABsAHwAjAAABNxc3JwYiJyY0NzEnBxcHJwEXNjIXFhQHFwEnJzcXBxc3FwcC4D08p3AfWCAfH3CnPT08/lpwH1ggHx9vAac90Tw8PC08PDwBoj09p3AfHyBYH3CnPD09/llwICAfWB9wAaY80jw8PSw8PDwAAAAEAAD/wAQAA8AASABUAIgArAAAJSc+ATU0Jic3PgEnLgEPAS4BJzU0JiMiBh0BDgEHJyYGBwYWHwEOARUUFhcHDgEXHgE/AR4BFxUUFjMyNj0BPgE3FxY2NzYmJyciJjU0NjMyFhUUBgEiBgcuASMiBgcuASMiBw4BBwYVFBceARcWMzI2Nx4BMzI2Nx4BMzI3PgE3NjU0Jy4BJyYDIiYnDgEjIiYnDgEjIiY1NDYzMhYXPgEzMhYXPgEzMhYVFAYCjCQBAQEBJA0HBwcdDCUHEAoUDw8UChAHJQwdBwcHDSQBAQEBJA0HBwcdDCUHEAoUDw8UChAHJQwdBwcHDYwPFBQPDxQUAREIEAgsh01NhywIEAguKSk9ERISET0pKS4VJxMsazo6aywTJxUuKSk9ERISET0pKS4aLhQjZjs7ZiMULhpCXl5CEB8OIHhLS3ggDh8QQl5eSRQFCgUFCQUVBxwNDQcHFQYKAyoPFBQPKgMKBhUHBw0NHAcVBQkFBQoFFAgcDQwIBxUGCgMqDhUVDioDCgYVBwgMDRwIBBUPDhUVDg8VAvMBAT1FRT0BARIRPSkpLi4pKT0REgcIJikpJggHEhE9KSkuLikpPRES/oAQDSozMyoNEF5CQl4GBj5OTj4GBl5CQl4AAAADAAAAAAQAA4AANABZAGUAAAEiBgcuASMiBgcuASMiBw4BBwYVFBceARcWMzI2Nx4BMzI2Nx4BMzI3PgE3NjU0Jy4BJyYjESImJw4BIyImJw4BIyImNTQ2MzIWFz4BMzIWFz4BMzIWFRQGIwEUFjMyNjU0JjEwBgMgCBAILIdNTYcsCBAILikpPRESEhE9KSkuFScTLGs6OmssEycVLikpPRESEhE9KSkuGi4UI2Y7O2YjFC4aQl5eQhAfDiB4S0t4IA4fEEJeXkL+oCUbGyVAQAMAAQE9RUU9AQESET0pKS4uKSk9ERIHCCYpKSYIBxIRPSkpLi4pKT0REv6AEA0qMzMqDRBeQkJeBgY+Tk4+BgZeQkJe/sAbJSUbG2VlAAAGAAD/wAQAA8AAMwA/AEsAVwBjAG8AAAEiBgcuASMiBgcuASMiBw4BBwYVFBceARcWMzI2Nx4BMzI2Nx4BMzI3PgE3NjU0Jy4BJyYBIgYVFBYzMjY1NCYFIgYVFBYzMjY1NCYlIgYVFBYzMjY1NCYDIgYVFBYzMjY1NCYhIgYVFBYzMjY1NCYDIAgQCCyHTU2HLAgQCC4pKT0REhIRPSkpLhUnEyxrOjprLBMnFS4pKT0REhIRPSkp/XIbJSUbGyUlASUbJSUbGyUlASUbJSUbGyUlmxslJRsbJSX+ZRslJRsbJSUDQAEBPUVFPQEBEhE9KSkuLikpPRESBwgmKSkmCAcSET0pKS4uKSk9ERL+ACUbGyUlGxslQCUbGyUlGxslQCUbGyUlGxsl/wAlGxslJRsbJSUbGyUlGxslAAAAAAQAAP/ABAADwABIAFQAiACsAAAlJz4BNTQmJzc+AScuAQ8BLgEnNTQmIyIGHQEOAQcnJgYHBhYfAQ4BFRQWFwcOARceAT8BHgEXFRQWMzI2PQE+ATcXFjY3NiYnJyImNTQ2MzIWFRQGASIGBy4BIyIGBy4BIyIHDgEHBhUUFx4BFxYzMjY3HgEzMjY3HgEzMjc+ATc2NTQnLgEnJgMiJicOASMiJicOASMiJjU0NjMyFhc+ATMyFhc+ATMyFhUUBgKMJAEBAQEkDQcHBx0MJQcQChQPDxQKEAclDB0HBwcNJAEBAQEkDQcHBx0MJQcQChQPDxQKEAclDB0HBwcNjA8UFA8PFBQBEQgQCCyHTU2HLAgQCC4pKT0REhIRPSkpLhUnEyxrOjprLBMnFS4pKT0REhIRPSkpLhouFCNmOztmIxQuGkJeXkIQHw4geEtLeCAOHxBCXl5JFAUKBQUJBRUHHA0NBwcVBgoDKg8UFA8qAwoGFQcHDQ0cBxUFCQUFCgUUCBwNDAgHFQYKAyoOFRUOKgMKBhUHCAwNHAgEFQ8OFRUODxUC8wEBPUVFPQEBEhE9KSkuLikpPRESBwgmKSkmCAcSET0pKS4uKSk9ERL+gBANKjMzKg0QXkJCXgYGPk5OPgYGXkJCXgAAAAEAAP/ABAADwABIAAABNCYjIgYjLgEjIgYHIiYjIgYVFBYXDgEVLgEjIgcOAQcGFRQXHgEXFjMyNjceARcHFwc3Jzc+ATceATMyNz4BNzY1NCYnPgE1BABpSQQHBCJkOTlkIgQHBElpAgEBAggQCC4pKT0REhIRPSkpLhUnExxAIy5AQMBACjdmKhMnFS4pKT0REhkWFhkCskppASsxMSsBaUoHDwgBAgEBARESPSkoLy4pKTwSEggHGCEJH0CAgEATAigkBwgSEjwpKS4nRRwYPiIACgAA/8AEAAPAABsANwBFAFMAYQBwAH4AjACbAKkAAAEiBw4BBwYVFBceARcWMzI3PgE3NjU0Jy4BJyYDIicuAScmNTQ3PgE3NjMyFx4BFxYVFAcOAQcGAzI2PQE0JiMiBh0BFBYTIgYdARQWMzI2PQE0JgE3NjQnJiIPAQYUFxYyAQcGFBcWMj8BNjQnJiIHJzQmKwEiBhUUFjsBMjYlIyIGFRQWOwEyNjU0JiUWMjc2NC8BJiIHBhQfAQEmIgcGFB8BFjI3NjQnAgA5MjJLFhYWFksyMjk5MjJLFhYWFksyMjkpJCQ1DxAQDzUkJCkpJCQ1DxAQDzUkJCkQFxcQEBcXEBAXFxAQFxcBIjgMDAwgDDcMDAsh/ac4DAwMIAw3DAwLIQswGBBPEBcXEE8QGAM7TxAYGBBPEBcX/OULIQsMDDcMIQsMDDgCZAshCwwMNwwhCwwMAtQWFksyMjk5MjJLFhYWFksyMjk5MjJLFhb+JxAPNSQkKSkkJDUPEBAPNSQkKSkkJDUPEAInGBBPEBcXEE8QGP08GBBPEBcXEE8QGAJdNwwhCwwMOAshCwz+FjcMIQsMDDgLIQsMDPsQFxcQEBcXNxcQEBcXEBAX1AwMCyELOAwMCyEMN/4KDAwLIQs4DAwMIAwAAAAAAgAAAIAEAAMAADQAWQAAASIGBy4BIyIGBy4BIyIHDgEHBhUUFx4BFxYzMjY3HgEzMjY3HgEzMjc+ATc2NTQnLgEnJiMRIiYnDgEjIiYnDgEjIiY1NDYzMhYXPgEzMhYXPgEzMhYVFAYjAyAIEAgsh01NhywIEAguKSk9ERISET0pKS4VJxMsazo6aywTJxUuKSk9ERISET0pKS4aLhQjZjs7ZiMULhpCXl5CEB8OIHhLS3ggDh8QQl5eQgKAAQE9RUU9AQESET0pKS4uKSk9ERIHCCYpKSYIBxIRPSkpLi4pKT0REv6AEA0qMzMqDRBeQkJeBgY+Tk4+BgZeQkJeAAQAAP/ABAADwABIAFQAiACsAAAlJz4BNTQmJzc+AScuAQ8BLgEnNTQmIyIGHQEOAQcnJgYHBhYfAQ4BFRQWFwcOARceAT8BHgEXFRQWMzI2PQE+ATcXFjY3NiYnJyImNTQ2MzIWFRQGASIGBy4BIyIGBy4BIyIHDgEHBhUUFx4BFxYzMjY3HgEzMjY3HgEzMjc+ATc2NTQnLgEnJgMiJicOASMiJicOASMiJjU0NjMyFhc+ATMyFhc+ATMyFhUUBgKMJAEBAQEkDQcHBx0MJQcQChQPDxQKEAclDB0HBwcNJAEBAQEkDQcHBx0MJQcQChQPDxQKEAclDB0HBwcNjA8UFA8PFBQBEQgQCCyHTU2HLAgQCC4pKT0REhIRPSkpLhUnEyxrOjprLBMnFS4pKT0REhIRPSkpLhouFCNmOztmIxQuGkJeXkIQHw4geEtLeCAOHxBCXl5JFAUKBQUJBRUHHA0NBwcVBgoDKg8UFA8qAwoGFQcHDQ0cBxUFCQUFCgUUCBwNDAgHFQYKAyoOFRUOKgMKBhUHCAwNHAgEFQ8OFRUODxUC8wEBPUVFPQEBEhE9KSkuLikpPRESBwgmKSkmCAcSET0pKS4uKSk9ERL+gBANKjMzKg0QXkJCXgYGPk5OPgYGXkJCXgAAAAQAWwDEA6UDEAANABsAKQA3AAATITI2NTQmIyEiBhUUFgUhIgYVFBYzITI2NTQmByEiBhUUFjMhMjY1NCYHISIGFRQWMyEyNjU0JoUC9hEZGRH9ChEZGQMH/QoRGRkRAvYRGRkR/QoRGRkRAvYRGRkR/QoRGRkRAvYRGRkCvBkREhgYEhEZVBgSERkZERIYqBkRERkZEREZqBkREhgYEhEZAAAABABbAMQDpQMQAA0AGwApADcAABMhMjY1NCYjISIGFRQWBSEiBhUUFjMhMjY1NCYHISIGFRQWMyEyNjU0JgchIgYVFBYzITI2NTQmhQL2ERkZEf0KERkZAwf9ChEZGREC9hEZGRH9ChEZGREC9hEZGRH9ChEZGREC9hEZGQK8GRESGBgSERlUGBIRGRkREhioGRERGRkRERmoGRESGBgSERkAAAAIAAD/wAQAA8AADQAbACkANwBFAIUAlQDGAAABMjY9ATQmIyIGHQEUFgU3NjQnJiIPAQYUFxYyBTMyNjU0JisBIgYVFBYlFBY7ATI2NTQmKwEiBiUWMjc2NC8BJiIHBhQXASIGBy4BJzQnLgEnJiMiBw4BBwYVFBYXBgcOAQcGFRQXHgEXFjMyNjceATMyNjceATMyNz4BNzY1NCcuAScmIwEyFhcuASMiBgcuATU0NjMBIiYnDgEjIiYnDgEjIiY1NDYzMhYXHgEXPgE3PgEzMhYXHgEXHgEXPgEzMhYVFAYjAaANExMNDRMTAQYtCQkJGwktCgoJGv2RQA0TEw1ADRMTAq0TDUANExMNQA0T/ecJGwkKCi0JGwkKCgKmCBAIGUEnEhI9KCguLikpPRESDAstJyc6ERESET0pKS4VJxMsazo6aywTJxUuKSk9ERISET0pKS7+gDxYCQ8eEEV7LQkKXkIBgBouFCNmOztmIxQuGkJeXkIMFgoECQQGEQkjZTsPHQ8PHg4jOBIOHxBCXl5CA0ATDUANExMNQA0TVC0JGwkKCi0JGwkK4hMNDRMTDQ0TIA0TEw0NExO/CgoJGwktCgoJGwn+pwEBIjQRLSgoPBEREhE9KSkuGjEWAhISPCgoLS4pKT0REgcIJikpJggHEhE9KSkuLikpPRESAQBNOgMEODMRJhRCXv2AEA0qMzMqDRBeQkJeBAMBAgINFwsrMgQDBAsIEzgjBgZeQkJeAAAIAAD/wAQAA8AADQAbACkANwBFAIUAlQDGAAABMjY9ATQmIyIGHQEUFgU3NjQnJiIPAQYUFxYyBTMyNjU0JisBIgYVFBYlFBY7ATI2NTQmKwEiBiUWMjc2NC8BJiIHBhQXASIGBy4BJzQnLgEnJiMiBw4BBwYVFBYXBgcOAQcGFRQXHgEXFjMyNjceATMyNjceATMyNz4BNzY1NCcuAScmIwEyFhcuASMiBgcuATU0NjMBIiYnDgEjIiYnDgEjIiY1NDYzMhYXHgEXPgE3PgEzMhYXHgEXHgEXPgEzMhYVFAYjAaANExMNDRMTAQYtCQkJGwktCgoJGv2RQA0TEw1ADRMTAq0TDUANExMNQA0T/ecJGwkKCi0JGwkKCgKmCBAIGUEnEhI9KCguLikpPRESDAstJyc6ERESET0pKS4VJxMsazo6aywTJxUuKSk9ERISET0pKS7+gDxYCQ8eEEV7LQkKXkIBgBouFCNmOztmIxQuGkJeXkIMFgoECQQGEQkjZTsPHQ8PHg4jOBIOHxBCXl5CA0ATDUANExMNQA0TVC0JGwkKCi0JGwkK4hMNDRMTDQ0TIA0TEw0NExO/CgoJGwktCgoJGwn+pwEBIjQRLSgoPBEREhE9KSkuGjEWAhISPCgoLS4pKT0REgcIJikpJggHEhE9KSkuLikpPRESAQBNOgMEODMRJhRCXv2AEA0qMzMqDRBeQkJeBAMBAgINFwsrMgQDBAsIEzgjBgZeQkJeAAAIAAD/wAQAA8AADQAbACkANwBFAIUAlQDGAAABMjY9ATQmIyIGHQEUFgU3NjQnJiIPAQYUFxYyBTMyNjU0JisBIgYVFBYlFBY7ATI2NTQmKwEiBiUWMjc2NC8BJiIHBhQXASIGBy4BJzQnLgEnJiMiBw4BBwYVFBYXBgcOAQcGFRQXHgEXFjMyNjceATMyNjceATMyNz4BNzY1NCcuAScmIwEyFhcuASMiBgcuATU0NjMBIiYnDgEjIiYnDgEjIiY1NDYzMhYXHgEXPgE3PgEzMhYXHgEXHgEXPgEzMhYVFAYjAaANExMNDRMTAQYtCQkJGwktCgoJGv2RQA0TEw1ADRMTAq0TDUANExMNQA0T/ecJGwkKCi0JGwkKCgKmCBAIGUEnEhI9KCguLikpPRESDAstJyc6ERESET0pKS4VJxMsazo6aywTJxUuKSk9ERISET0pKS7+gDxYCQ8eEEV7LQkKXkIBgBouFCNmOztmIxQuGkJeXkIMFgoECQQGEQkjZTsPHQ8PHg4jOBIOHxBCXl5CA0ATDUANExMNQA0TVC0JGwkKCi0JGwkK4hMNDRMTDQ0TIA0TEw0NExO/CgoJGwktCgoJGwn+pwEBIjQRLSgoPBEREhE9KSkuGjEWAhISPCgoLS4pKT0REgcIJikpJggHEhE9KSkuLikpPRESAQBNOgMEODMRJhRCXv2AEA0qMzMqDRBeQkJeBAMBAgINFwsrMgQDBAsIEzgjBgZeQkJeAAAIAAD/wAQAA8AADQAbACkANwBFAIUAlQDGAAABMjY9ATQmIyIGHQEUFgU3NjQnJiIPAQYUFxYyBTMyNjU0JisBIgYVFBYlFBY7ATI2NTQmKwEiBiUWMjc2NC8BJiIHBhQXASIGBy4BJzQnLgEnJiMiBw4BBwYVFBYXBgcOAQcGFRQXHgEXFjMyNjceATMyNjceATMyNz4BNzY1NCcuAScmIwEyFhcuASMiBgcuATU0NjMBIiYnDgEjIiYnDgEjIiY1NDYzMhYXHgEXPgE3PgEzMhYXHgEXHgEXPgEzMhYVFAYjAaANExMNDRMTAQYtCQkJGwktCgoJGv2RQA0TEw1ADRMTAq0TDUANExMNQA0T/ecJGwkKCi0JGwkKCgKmCBAIGUEnEhI9KCguLikpPRESDAstJyc6ERESET0pKS4VJxMsazo6aywTJxUuKSk9ERISET0pKS7+gDxYCQ8eEEV7LQkKXkIBgBouFCNmOztmIxQuGkJeXkIMFgoECQQGEQkjZTsPHQ8PHg4jOBIOHxBCXl5CA0ATDUANExMNQA0TVC0JGwkKCi0JGwkK4hMNDRMTDQ0TIA0TEw0NExO/CgoJGwktCgoJGwn+pwEBIjQRLSgoPBEREhE9KSkuGjEWAhISPCgoLS4pKT0REgcIJikpJggHEhE9KSkuLikpPRESAQBNOgMEODMRJhRCXv2AEA0qMzMqDRBeQkJeBAMBAgINFwsrMgQDBAsIEzgjBgZeQkJeAAAFAAD/wAQAA8AAMwBXAGMAbwB7AAABIgYHLgEjIgYHLgEjIgcOAQcGFRQXHgEXFjMyNjceATMyNjceATMyNz4BNzY1NCcuAScmAyImJw4BIyImJw4BIyImNTQ2MzIWFz4BMzIWFz4BMzIWFRQGARQWMzI2NTQmMTAGNxQWMzI2NTQmMTAGJRQWMzI2NTQmMTAGAyAIEAgsh01NhywIEAguKSk9ERISET0pKS4VJxMsazo6aywTJxUuKSk9ERISET0pKS4aLhQjZjs7ZiMULhpCXl5CEB8OIHhLS3ggDh8QQl5e/mAlGxslQED+JRsbJUBA/gAlGxslQEADQAEBPUVFPQEBEhE9KSkuLikpPRESBwgmKSkmCAcSET0pKS4uKSk9ERL+gBANKjMzKg0QXkJCXgYGPk5OPgYGXkJCXv5AGyUlGxtlZSUbJSUbG2VlZRslJRsbZWUAAAYAAP/ABAADwAAzAD8ASwBXAGMAbwAAASIGBy4BIyIGBy4BIyIHDgEHBhUUFx4BFxYzMjY3HgEzMjY3HgEzMjc+ATc2NTQnLgEnJgEiBhUUFjMyNjU0JgUiBhUUFjMyNjU0JiUiBhUUFjMyNjU0JgMiBhUUFjMyNjU0JiEiBhUUFjMyNjU0JgMgCBAILIdNTYcsCBAILikpPRESEhE9KSkuFScTLGs6OmssEycVLikpPRESEhE9KSn9chslJRsbJSUBJRslJRsbJSUBJRslJRsbJSWbGyUlGxslJf5lGyUlGxslJQNAAQE9RUU9AQESET0pKS4uKSk9ERIHCCYpKSYIBxIRPSkpLi4pKT0REv4AJRsbJSUbGyVAJRsbJSUbGyVAJRsbJSUbGyX/ACUbGyUlGxslJRsbJSUbGyUAAAAACAAA/8AEAAPAAEgAVACdAKkA8gD+ATIBVgAAJSc+ATU0Jic3PgEnLgEPAS4BJzU0JiMiBh0BDgEHJyYGBwYWHwEOARUUFhcHDgEXHgE/AR4BFxUUFjMyNj0BPgE3FxY2NzYmJyciJjU0NjMyFhUUBiUnNCYnNzY0JyYiDwEuASMnLgEHDgEfAQ4BBw4BBycmBgcGFh8BHgEXBwYUFxYyPwEeAR8BHgE3PgEvAT4BNz4BNxcWNjc2JicHBiInJjQ3NjIXFhQlBy4BJy4BJzc2JicmBg8BIgYHJyYiBwYUHwEOARUHDgEXHgE/AR4BFx4BFwcGFhcWNj8BPgE3FxY2NzY0LwE+AT8BPgEnLgEHBwYiJyY0NzYyFxYUATQnLgEnJiMiBgcuASMiBgcuASMiBw4BBwYVFBceARcWMzI2Nx4BMzI2Nx4BMzI3PgE3NgUOASMiJicOASMiJjU0NjMyFhc+ATMyFhc+ATMyFhUUBiMiJgKMJAEBAQEkDQcHBx0MJQcQChQPDxQKEAclDB0HBwcNJAEBAQEkDQcHBx0MJQcQChQPDxQKEAclDB0HBwcNjA8UFA8PFBQBpRgDAxIGBgYSBhEGCwUHAg8ICQgCBgIFAgIDAhgIDwMCCQgYAQMCEgYHBhEGEgULBgYCDwkICQIHAwQCAgQCFwkPAgIICUcHEQYGBgYRBwb9exgCAwICBQIGAwkICQ8CBwULBhEGEgYGBhIDAxgJCAICDwkXAgQCAgQDBwIJCAkOAwYGCwUSBhEHBgYSAgMBGAgJAgMPCD0GEQcGBgcRBgYDSRIRPSkpLggQCCyHTU2HLAgQCC4pKT0REhIRPSkpLhUnEyxrOjprLBMnFS4pKT0REv7EI2Y7O2YjFC4aQl5eQhAfDiB4S0t4IA4fEEJeXkIaLkkUBQoFBQkFFQccDQ0HBxUGCgMqDxQUDyoDCgYVBwcNDRwHFQUJBQUKBRQIHA0MCAcVBgoDKg4VFQ4qAwoGFQcIDA0cCAQVDw4VFQ4PFVoHBgsFEQcRBgYGEQIDGAkJAwIPCBgCAwMCBAIGAgkICA8DBgYLBRIGEQYGBhECAwEYCAkCAw8IGAIDAgIFAgYCCAkIDwINBgYGEgYGBgYScwYCBAIDAwIYCA8CAwkJGAMCEQYGBhEHEQULBgYDDwgJCAIGAgUCAgMCGAgPAgMJCBgBAwIRBwEGBhEGEgULBgYDDwgICQI5BgYGEgYGBgYSAYAuKSk9ERIBAT1FRT0BARIRPSkpLi4pKT0REgcIJikpJggHEhE9KSlVKjMzKg0QXkJCXgYGPk5OPgYGXkJCXhAAAAAKAAD/wAQAA8AAGwA3AEUAUwBhAHAAfgCMAJsAqQAAASIHDgEHBhUUFx4BFxYzMjc+ATc2NTQnLgEnJgMiJy4BJyY1NDc+ATc2MzIXHgEXFhUUBw4BBwYDMjY9ATQmIyIGHQEUFhMiBh0BFBYzMjY9ATQmATc2NCcmIg8BBhQXFjIBBwYUFxYyPwE2NCcmIgcnNCYrASIGFRQWOwEyNiUjIgYVFBY7ATI2NTQmJRYyNzY0LwEmIgcGFB8BASYiBwYUHwEWMjc2NCcCADkyMksWFhYWSzIyOTkyMksWFhYWSzIyOSkkJDUPEBAPNSQkKSkkJDUPEBAPNSQkKRAXFxAQFxcQEBcXEBAXFwEiOAwMDCAMNwwMCyH9pzgMDAwgDDcMDAshCzAYEE8QFxcQTxAYAztPEBgYEE8QFxf85QshCwwMNwwhCwwMOAJkCyELDAw3DCELDAwC1BYWSzIyOTkyMksWFhYWSzIyOTkyMksWFv4nEA81JCQpKSQkNQ8QEA81JCQpKSQkNQ8QAicYEE8QFxcQTxAY/TwYEE8QFxcQTxAYAl03DCELDAw4CyELDP4WNwwhCwwMOAshCwwM+xAXFxAQFxc3FxAQFxcQEBfUDAwLIQs4DAwLIQw3/goMDAshCzgMDAwgDAAAAAABAAD/wAQAA8AASAAAATQmIyIGIy4BIyIGByImIyIGFRQWFw4BFS4BIyIHDgEHBhUUFx4BFxYzMjY3HgEXBxcHNyc3PgE3HgEzMjc+ATc2NTQmJz4BNQQAaUkEBwQiZDk5ZCIEBwRJaQIBAQIIEAguKSk9ERISET0pKS4VJxMcQCMuQEDAQAo3ZioTJxUuKSk9ERIZFhYZArJKaQErMTErAWlKBw8IAQIBAQEREj0pKC8uKSk8EhIIBxghCR9AgIBAEwIoJAcIEhI8KSkuJ0UcGD4iAAEAAP/ABAADwABIAAABNCYjIgYjLgEjIgYHIiYjIgYVFBYXDgEVLgEjIgcOAQcGFRQXHgEXFjMyNjceARcHFwc3Jzc+ATceATMyNz4BNzY1NCYnPgE1BABpSQQHBCJkOTlkIgQHBElpAgEBAggQCC4pKT0REhIRPSkpLhUnExxAIy5AQMBACjdmKhMnFS4pKT0REhkWFhkCskppASsxMSsBaUoHDwgBAgEBARESPSkoLy4pKTwSEggHGCEJH0CAgEATAigkBwgSEjwpKS4nRRwYPiIAAgAA/8AEAAPAABsALwAAASIHDgEHBhUUFx4BFxYzMjc+ATc2NTQnLgEnJhMFBiYvASY0Nz4BHwE3NhYXFhQHAgBqXV6LKCgoKIteXWpqXV6LKCgoKIteXZT+/Q8nDoQODg8pDGLgEScNCwsDwCgoi15dampdXosoKCgoi15dampdXosoKP499A0BD4YOKQ0NAg9k0gsGDw0hDwAAAwAA/8AEAAPAABsAKQA3AAABIgcOAQcGFRQXHgEXFjMyNz4BNzY1NCcuAScmAxQGIyImNRE0NjMyFhUTFAYjIiY1ETQ2MzIWFQIAal1eiygoKCiLXl1qal1eiygoKCiLXl2XGhISGxsSEhqzGxISGhoSEhsDwCgoi15dampdXosoKCgoi15dampdXosoKP1vEhoaEgEiEhoaEv7eEhoaEgEiEhoaEgAAAwAA/8AEAAPAABsAeACQAAABIgcOAQcGFRQXHgEXFjMyNz4BNzY1NCcuAScmAw4BIyImJy4BNTQ2Nz4BPwE+ATc+ATU0JicuASMiBgcOASMiJicuATU0Njc+ATc+ATc+ATMyFhceARUUBgcOAQ8BDgEHDgEVFBYXFjIzMjY3PgEzMhYXHgEVFAYHEw4BIyImJy4BNTQ2Nz4BMzIWFx4BFRQGAgBqXV6LKCgoKIteXWpqXV6LKCgoKIteXVEhOxoSGwgICAICAgUDUgMFAgECAgEBBAIFFhAREwMDBwQDBAgJCBUMCxsQEBwMEx4JCgoBAgEGBEIEBQICAQEBAQMBChYNDRADAwcDBAMgIW0MGxEQHAsMDAwMCxwQERsMCwwMA8AoKIteXWpqXV6LKCgoKIteXWpqXV6LKCj9ABoZCQkKFQsIDQYHDAeqCAwEBQgDBAUBAgERERERBQQFBgEFDgkKFQsJEwkICQsLCxsPBw4HBw8IkQkOBgYHAwIDAQENDg4NBAUEBwEHIRkBxwsMDAsMHBAQGwwMCwsMDBsQEBwAAAAACQBq/8wDuwOnADkArAC9AO8BUAFdAaUBqgGuAAABNiYxLgExJicuAScmMTAmBw4BFyYGBw4BFw4BFxYGMTAWNx4BFx4BOwEyNjc+ATc+ATc2Jy4BJyYxBxQwFRQGMRwBIxQGFTAUBxQGFRQwFQ4BFTAUMQ4BFRQiBxQGFRQGMRQGBzAGMQYUBzAUMQ4BBxwBIwYUBxQGMQ4BFRQGMQ4BBzAGFRQGBzgBMQ4BBxQGMQ4BFTAGFRQGFRQiFQ4BFS4BNz4BFzgBFRQGFQUjJzwBNS4BJzMOAQccAQcxNyYGByMuAQcOAQc0NjU0Njc4ATU+ATc+ATceARceARcwFhcwNicyNjMeARUUFhUuAScFLgEnNCYxLgE1MCY1LgE1NCY1NCYnMCY1NCYnMDQjJjQnNCYxJjQnMCY1NCY1NCYxLgE1MDQxLgEnNDA1NCY1NDAnNCY1MDQnMCY1NDA1NCY1MDQxMhYXHgEHNiYnMiIxFy4BJx4BFzAGMRQGMQEqATEwIiMiJicuASc+ATc2Jic0NjUeATc+ATczHgEXFjY3DgEXHgEXDgEHDgEVMAYxIgYHMAYxFAYHOAEVDgEHOAE5AQ4BIy8BNxcHFzMVIwN1ETwFWwIiIlAhIU5TNRkDHUIhLgYPIC84Pyd1KggSCSJGI2YkRiEHDgaCBj0eAwMfEhIZAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQECAQEBAQEBAQECAQEBAgEECg4XAf68KwECCAdQCAkBAYMUJBCcECgWJDoMASgCBQgEBQsGDyQUHHJkKQwhDAQJBAcPAREzHf4wAQEBAQEBAQEBAgEBAQIBAQEBAQEBAQEBAQEBAQEBAQEBAQEODAkDAQEBAQEBAUUBAgECAgIBAQElJQ0MJSZYIQMFAgIFAgoaDgESNh8rPAgwCD8rGy8SDxwJBAoFAQQCAQMBAQIBAQICAQIBIE0fZQR6A3kjMDACY0RTMBYkFRUUAwIcHBE7FgoNJzdcIyCHanVRNh0JGAwuTk4uChMHDnJ5Pi8wQBEQpAEBAQIBAQECAgEBAQIBAQECAwEBAgMBAQEBAwEBAQICAgIBAwIBAgQBAQECAwEBAQEDAQEBAgICAQEBBAICAwEBAQEDAQEBAQIBAQEBAQEUJQ4qEgMBAwMBKQEDBQMOFAgIFg8DBAJNAQEDBAECAxcfID0VHmMFAQYNBgYLBA0XCRg4GhYWKS4BDykSEjAaEw0DhgEEAgEBAgICAgEBAgIBAQECAgIBAQEEAgECAwEBAQICAQIBAQIBAQECAgECAQMCAQEBAgEBAQECAgEBAgEBAQEDAQEWIwsaDwEDAi8DBAICAgEBAQL+tn0lAwgDAwUCDEwpBAcDDgoDBD8mJz8DAQcKK1INBgcDAgUCAgICAQMBAQICAQECAwInYasUFBMVFQ0ACwBx/78DjwO/AAQACQBOAKwBDwFSAZoBpQH8AgcCSgAAJTMVIzUXMxUjNQEvAy4BIzAiIyIGDwIvATAiMSIGBwYUHwIPARwBFxQWMzIwMzAyMT8BHwEWMjMyMDM6ATU+ATU/Az4BNTQmJzcuASMwIjE+ATc2NzYmJyYnLgEjKgEHIgcOAQcGBw4BBw4BFw4BBw4BFx4BFx4BFx4BFwYWFx4BMzgBMToBMx4BFx4BOwEyNjc+ATc6ATMyNjc0Jic+ATc+ATc2JicPAhwBFTAUMRwBIzAUMRQGFTAUIxQGFTAGMTAGMTAGMQYwBzgBOQEOAQcGIiMqAScuAS8BBwYiIyoBJy4BJy4BPwEvAiY0Nz4BMzoBMRc3PgEzOgEXHgEfAh4BFxYGDwEFKgE1MCYxLgEvAy4BNz4BPwI+ATc2MjMyFh8BNzoBMTIWFxYUDwEXFgYHDgEHIgYjKgEvAQcOASMOASMiJicxAz4BNzYWFx4BMzI2Nx4BFxYUBycuAScuASMiBg8BJyoBIyIGBwYWHwEuASMiBgc3PgEnLgEjKgEjBycuASMiBgcOAQ8BJjY3Ay4BJxYyHwEWBgcFDgEjKgEjIiYnLgEnPgE/AScwJicyMDM6ATc+AT8BFzIWMzoBNz4BNz4BLwE+ATMyFhcHBhYXHgEXMhYzMjY/ARceARcWMjMyNjcOAQcGFBceARcOAQc3LgEnNz4BNw4BBwUyNjE/AR8BMDIxOgEzMjY3NjQvAj8BNjQnLgEjMCIxBy8BNCYjKgExIgYPBA4BFRQWMx8CFBYXMDIVOgExAbaTky44OAFfSQwEEQEDAgEBAgMBMAgOTQECAwEBASoHBh8CAwEBAQFLDQo7AQIBAQEBAQIDBAEMQwIBAwFDCBkQAQECAQkHCAoYFzZffxgFBgEGHR1OKSkcCRMJLjsoCxEGBQcCBxEJBgsHAgcCAxsIAwUCAQIBDiMVSVMiMhxWTBUkDQECARMcAgECDBADCREHAwgEPjcFAQEBAQECAQEBAgQCAwYDAgQCBAgCO00CBQECBAIFCQMGBAQfBwEiBgYEDwkBAU4wBQ4GAgQCCg0CE0gJCwEBCQgM/bwBAgIICwEFLBcICQEBCwlIEwINCgEEAwcNBTBOAQEIEAQFBSofAwMGAwkFAQQDAQUCSzsDBwMCBAICBQEZH08aCR4TG0QoHzocHEIFAwEGBBYPAgYCDRYIKkEBAwEPGQcIAQgQBg4HChMHEQgBCAgZDgECAUQqCRYNAwYDDxcDDgEFCCsMFwkBAgEoBQMCAgZGURUVAxoZTUUWHwsICQEDAQMCAQEDBgMHDAUyQAMHBAMGAwkPBgoFBhgFHxIOFgkYBgUKBg8JAwYDAwcEPzIFDAcEBQQFCQQBAwEDBgEFAw4gFIcCAgEWBAgDBhEJ/cIBAjwKDEsBAQEBAQIBAgEeBQcqAQEBAwIBWwgxBAEBAQMCARIEDEkCAwICTgEFAwICAQHEGBg/Dw8BVR0EDUwCAwEBPAoBBwICAQUCQgsNRwIEAgEBEwMJMwEBAQMDTQ4HJwEFAQMDASkJCgQHBB4jJFAqKilHIQEDAxITEh8JEQkqco8CCAgGFhAmQBcOFggDBwI1IAEBAS9EFEQ/PEcUQy8WEQgbEgwgBxhAJRIXBlAgTgECAQEBAgEBAQEBAQEBAQEBAQECAgECAQEDAzMXAQEBBgMHEwlHCgI1CBMICAgHPAYHAQMNCUwdAxAJChEEBowBAQMQCU4aDQURCQkPBB1MCQ0DAQYGPQcICAcTCUJHCRMHBAUBAQEUNAIEAQEBAQF8UFQCAQMCBAUEBR5TFQtBLR0PFgMBAQsKMwYPCw4gDBkBAQMBGw0fDgsPBTMKCgEBAxYPOSk+Ev6ZDjolAQEYISgK5UI6OUMTQigGDgEGBw0MAQIFBSwSAgECCQYMHw86AgkGBjgOHwwHCQIBAgERLAQGAgECAQkNAwcRBgMFAidBE/gSJxIOAwcEIDUSFAEzCQMTAQECBAJIDAtCAgUBAgIICjwBAQQBTA0EHQEDAwIELw5NAwMBAQAAAAAFAIX/ywN3A7IAVQChAK0AuQDVAAABLgEjKgEHNCYnNjc2JicmJzAnLgEnJgcGBw4BBwYHBhYXIiYjIgYHDgEXHgEzMDIxHgEVHgEXHgEXHgE7ATI2Nz4BNz4BNz4BNzAyMTI2Nz4BNzYmJwcOASMiJicOAQcOASMqASMiJicuAScOASMiJicmNjM6ARceATEXMzAnJjY3NjcwJjcwFx4BNzY3MBY3MBYHMBYHMzcwNjc+ATMeAQcFFAYjIiY1NDYzMhYXFAYjIiY1NDYzMhYHLgEHJgYHDgEHMBY3MDYzMhYXHgExFjYxLgEnA3IGFg4DCAUBAQgGBwQODSATE0o3NkZHNzZNFRUEAwgFAwYDDhYGBAgDCzMmAQICBREbFB8NISoYZA4iFxIzJxwTBgEDAQERIA4MEgYFBgQLDC0XBAoECwwgRE0VFTYZGTlDIQgLBAkFFy0MAxIQAwgDAwMQEgICAgUFDwoKLi6AQkIoLyMfDhYOHA8DAwgQBxASA/48EgwNERENDBL2EQ0MEhIMDRF3DBgIDykSH0EIARRaLh05EiAuFAELZBsB9AkIAQcQCCQsLFAdHgYYGDUTEwkJFRU8JSYrNG4nAQgJBRMPPnYLGAowVhkTHw0hIhASDi4kGlkxChMKHh0YPiIREwUjRGAFBD99Hz89PT8ffEAFBGBEEQ4BERNnGRpEJCMTZhwKCg0DBBokBi41kj9zDAoBAwEPETYMEhIMDRERDQwSEgwNERHNAwEBAQMHBh8eHAQgBQIHEgMbKB4DAAAHAAv/vwP1A78ACwAXABwAIADtAUABhQAAARQGIyImNTQ2MzIWFxQGIyImNTQ2MzIWBzMVIzUXMxUjATQmJz4BNTQmIzAiMTYmJz4BNTQmIyoBIzgBMTQmJz4BNTQmIyIGBy4BIyoBIzY0NTQmIyIGBzwBNTQmIyIGBy4BIyIGBy4BIyIGBy4BIyIGFRQWFw4BBy4BBw4BBw4BFRQWFw4BBw4BBw4BByYiIyIGFRQWMzoBMx4BFw4BBwYWFw4BFw4BFQYWFw4BBwYWFx4BFx4BMzI2NzUeATsBMjY3PgE3FR4BMzI2Nz4BNz4BNx4BNz4BJz4BNz4BJz4BNz4BNz4BNTQmJz4BNQEqASMiJicxLgEnLgEnPgE3NDY1PgE3PAE1PgE3PgE3HgEXNyImJz4BNzoBMx4BMzI2NxYUFyIGBxc+ARcUBhUUFhcOARUUFhcOAQcOAQcxDgEjAy8DNCIxOAEjIjAVDwEjJzgBMSIUMRwBFR8BDwEUMBUwFjE4ATM4ATE/AR8BMDIxMDIzOAEzMjQ1NzU/ATA2NQYmIwGcEQsMEREMCxHIEQsMEREMCxG1eXklLi4CIQsKBgcSDAIDBwgFBS4fAgMCAwQICRIMBgoEESsZAgQCAhwVBQgFEgwHDQQPLxsbMBIOIBEgNhEDCQQUHQQEAQEBBhYKBwkBHCcFBAMGAwQFAw0WBgEFAgwSEgwBAgEBAgEPFAIDEQ8NAwsEBgIPDAECAQMeGQE7LAhELQ8bDQ8ZD1wMHxUEBgQMGA4mPg4DCAMTGwYHFgkKBgQEBwMaDw0EBwUDBQISFwcGEBH+GRMyFw0ZEQ4nHBAPBAILCgEGBwISHAsJEQgPGwkJAQsKEBQEAgMCEDgiCREJAQEjOwEJDz8dAi0kBAYDBAUQFBYkDxwmDIgTAwEFAQEBDgIEFAELAQEJAQEVAwIQAQEBAQEBAxMBAQEBAYALERELDBERDAsREQsMERHDFBQ0DAEUDxwKBA4HDRISJhEIFAshLQoUCwMOCg0SBAMPEwMIBRQdAgIBAgEMEgYFEBUWEgcKHhkCARwUBw4GAQQBCAYFBA0HBCwdChIJAQIBAgQCBRQNARIMDRIDBQIJHxIVJQ0YORsDCgYJEwIDBwQcLgktRAgqOQcGRA4NDxECBQNlBQYqIQIEAgwjFAcEBwYVCgIEAhE5GQEEAgIFAgokFgwWCAsgE/57EREOKBoPMx4WKxICAwIIEwsCBQMIGREBAwQDBgMdAwMNIxUbHwMBAQQBDgEdBQsDAwgEKD8OBAwFBgoEJUESFSANGBgBEAgBAxUBARADAwEBAQESAgQTAQEBBQIDDQEBFAQCCwIBAQEACQCH/8QDdQOxAAMABwCeALAAwADMAN0BJQGMAAAlMxUjFzMVIyU+ATc+ATcOASc+ATc2JicuASMqAQc3PgEzMhYXLgEjMCIxNz4BNS4BJy4BJzQmJyYGDwEmBgc+ARceARcOAQcOARcUFh8BIjAjIgYHPgEzMhYfASImIyIGBw4BFx4BFwYmJx4BMx4BFxQGBx4BFxY2NzAGBx4BFzUnHgEXHgE7ATI2Nz4BNxU+ATcuATEWNjc+ATcuATU3NTYyMzIWBw4BBy4BJy4BJzcBPgEXHgEXKgEjIgYHPgExAxQGIyImNTQ2MzIWJyY2MzoBHwEOAQcOAQcuAScTJjYnDgEHMAYxDgEVMCIxIgYxMCIxMCIxMCI1MCYxMCYjIjQjIiY1IiYnLgEnPgE3FzUwJyY2NzYXBgcOAQcGFzc2BgcuASclKgEHMCIxKgEjMCIxIiYjMCI1IiYxMCY1MCYnDgEHDgEHDgEVDgEHFAYHDgEHBjAVDgEjKgEjIiYnMTgBMS4BJy4BNS4BJzY3PgE3NjcHMDc+ATc2Nz4BFx4BBxU3HgEXDgEHFCIjAcZ0dCMsLAEKFR0KEiQQDyIRBgcBAQUDBREMAwcEAgUNBg4VAgIiFwEIDQ0BVzIfQCIMDBMwDx4nVBYOMhgBAwEcMxgQHwEHCQ0BARciAgIVDQkOBQMEBQQLEQYDBQEBBQURJQ4QJRMJGxMVKgUMBhAhCwcIHkEkAQMHAxYeFDwRHxYCAgIlQyALChI7EQIDATYYFgQIAwwNAgMFBAQHBQgIAgj+1wodDAQHAgULBQwbDQQFMA8LCw8PCwsP2gMPCwMHBAQBCQoCBwMDBAKNHQUgAQIBAQEBAQECAQIEAgIBAQEBAQEBAQwTCA0bDRUHCAgXGD8MDw4ZCQgBTAYiAwQIAwGLAQEBAQEBAQEBAQEBAQECAgEXBRwFBwMBAgMFAwIBAwQCASQlDhIUFxIiIgIEAwEBAgQCDRgYPCAgHX8KCicbGyMdEg0WJAgVDBoOChgMAQGREx0LmgU1HQEODAsFBRQfBQsQBAYGAREFBRINFh99J0MVI0gYCg0BDhkJDgYUKBMZJRUOCwEBAQkaDxYrFxRAJngfFg0SBwYXAQYGBA8LBRkRBgYMDg4bNwokgBMFCQUDBAobEBIdCU4UBAcEFxwbGAIDAmEIGRISIxEIBQICAgeXJLAGAQoMDhoMAgYDCBUJEgHqDAQJAwgFAQEFBv3dCw8PCwsPDxcMCgEHCxgKAgUDCRUL/uobPDYBAQEBAQEBAQEBAgEBAQEBCCUZAgoJLjEsLGgqKwQgLC1hMTIsHQNdYwIHA44BAQEBAQECARlhHAQGBAEBAQMFAgECAQIFAwEBJSgnJQIEAwEBAQIEAhcoKFsuLSEhFBNBKSkrCRoJEWt9UCwHCQIcJQQBAAAAAAQAhv/NA7YDmQA9AGMAkgC5AAABPgEnLgEnJicuAQcGBw4BBwYXHgEXFhceARcWBgcOAQchLgEzHgE3PgE3NiY3PgE3NiY3PgE3NicuAScmNwU0JjUnNCYnLgE3PgE3MDIxMhYXNxcnNy4BIw4BBwYWFx4BFzEHBQ4BBw4BBw4BIyImJy4BJy4BJy4BLwEHJxcHFx4BFx4BFx4BFz4BNz4BNxciBhU3DgEHFAYxFwc3Fz4BNzYmJy4BIw4BBw4BMSc+ATcyMDMyFhceAQcDaQsJAwQxISlIR5xLSjEvUwIBCgohFRYVDR8DBw8WDx0OAc0RBhckOCMbIAUGEgIEEwYIDQ0RHw4NDQ4mEBAH/e8CAgIBExQLCzspAREfDQ4QSg0GDwgjHAQIDw0BAQEiASQNGg0OGQoDCQQECAMNFAkKFAwDBgMEEQhKDwEDBgMLEgoGDggJEgoMFwwkAQFeBBAOAQ5LDg8ICgIDBREKHBALEwcBBh0ZHRABASEsDBYQBgIWGiQoP20mLxgXAhUVKil/UjQrK0wiISEUMRk0VSEXKBIfTwEKBgQTGBwTDhQaFx4lBgsHDwsaGjwdHBEPAQEBAgECARpQKCcuAwcIEUkCEAMCAiUOHDsSAQEBIjgLFAoLFAkDAwMCCw8HBw8LAgUDBBFLCw4BAwUDCQ4HBAsGBw4ICBMKIAEBixUmEgEBEQVKEAwYDhQvEQsKAQUFAQQmEAkBFw0WPycAAAADAAD/wAQAA8AAGwA4ADwAAAEiBw4BBwYVFBceARcWMzI3PgE3NjU0Jy4BJyYHMhceARcWFRQHDgEHBiMiJy4BJyY1NDc+ATc2MwcRLQECAGpdXYspKCgpi11dampdXYspKCgpi11damVYWYMnJiYng1lYZWVYWYMnJiYng1lYZZABif53A8AoKYtdXWpqXV2LKSgoKYtdXWpqXV2LKSgaJieDWVhlZVhZgycmJieDWVhlZVhZgycm+v4o7OwAAAABACj/6APYA5gAHwAACQE2NCcmBgcJASYiBwYWFwkBBhQXFjY3CQEWMjc2JicCkQFGHx4eVh7+uv66H1UeHgEeAUb+uh8eHlYeAUYBRh9VHh4BHgHAAUYfVR4eAR7+ugFGHx4eVh7+uv66H1UeHgEeAUb+uh8eHlYeAAAAAQAl/8AD2wPAADMAAAEhMjc+ATc2NTQnLgEnJiMhNQ0BNSEyFhUUBiMhIgcOAQcGFRQXHgEXFjMhNSEiJjU0NjMBSQGSNS8vRRQUFBRFLy81/m7+3AEkAZIuQEAu/m41Li9GFBQUFEYvLjUCbv2SLUFBLQEuFBRGLi81NS4vRRUUktvckkAtLkAUFEYuLzU1Ly5GFBSSQS0tQQAABAAA/8AEAAPAABsAOgBdAH0AAAEiBw4BBwYVFBceARcWMzI3PgE3NjU0Jy4BJyYTDgEjIiYnLgEHDgExBiYnJjY3MjY3NhYXHgEXHgEHNw4BIyImJyYnLgEnJgcOASMGJicmNjcyNjc2FhceARceAQc3IiYnJicmBgcGBwYmJyY2NzI2NzYWFx4BFx4BBw4BIwIAal1eiygoKCiLXl1qal1eiygoKCiLXl11BRAJBQkERZQ2PVAOGQQEDg4CV0MnSyMtUSMNBwdBBhMLBQsFKSsrVCcnIEheARAeBQQREANmTy5ZKjVgKg8JCSgHDQZhaWmvOjkBFSQGBRQVA35gOW0zQXY0EgsLBxgNA8AoKIteXWpqXV6LKCgoKIteXWpqXV6LKCj9GQgJAgIpFQECEQQPDg0aBBIDAQYGCR0VBx0MhwoKAgMYDw4PAgMBAxMFEREQHgUWAgIHBwojGQkhD3sDBDkQEQUMDAEFFRQUJQYbAwIICgwrHgspEgwNAAAAAgAA/8AEAAPAABMAIwAAARUjIgYdATMVIxEjESM1MzU0NjM3ISIGFREUFjMhMjY1ETQmA2ZmFR6ZmZpmZmlKtPzMKjw8KgM0Kjw8A1qaHhVnmf6ZAWeZgEtpZjwq/MwqPDwqAzQqPAAQAAD/wAQAA8AAAwAHAAsADwAmADMAOwBHAFQAZQB2AIcAmACcAKAApAAAJSM1MxEjFTMlIxUzFSMVMyUVMzUzFSMVMxUzNSM1MzUjNTM1IxUjNxUzFTMVMzUjNSM1IwUzFTM1IzUjNyMVMzUzNTM1IxUjJxUzNTM1MzUjFSM1IwU1NDY7ATUjIgcOAQcGHQEzJTMyFh0BMzU0Jy4BJyYrARUTFRQGKwEVMzI3PgE3Nj0BIwUjIiY9ASMVFBceARcWOwE1ASMVMwUjFTMDIxUzAY5ycnJyAVZycnJy/sE5Ihc5OTkXIjk5crYXOTk5Fzn+wTk5OTnNIjkiOTk5RDk5OTk5Of7vTTtycjs0NE0WFpQB3nI7TZQWFk00NDty+k07cnI7NDRNFhaU/iJyO02UFhZNNDQ7cgFWOTn++jk5OTk5xnEBVnJycuRxzHE5OTkiOSI5FjkXUDktLjkuLVAiOSIXOTkiOSJychciOSIiW3I7TZQWFk00NDty+k07cnI7NDRNFhaU/iJyO02UFhZNNDQ7cvpNO3JyOzQ0TRYWlAGwOcw5AT45AAAAAAIAHQC0A+MCzAASACUAAAE2MhcxFhQHMQEGIicxJjQ3MQEFJjQ3MTYyFzEBFhQHMQYiJzEBA3wVPRUWFv5RFjwWFRUBsPyhFhYVPRUBsBUVFjwW/lECzBUVFj0V/lAVFRY9FQGwaBU9FhUV/lAVPRYVFQGwAAAAAAIAHQC0A+MCzAASACUAADcGIicxJjQ3MQE2MhcxFhQHMQElFhQHMQYiJzEBJjQ3MTYyFzEBhBU9FRYWAa8WPBYVFf5QA18WFhU9Ff5QFRUWPBYBr7QVFRY9FQGwFRUWPRX+UGgVPRYVFQGwFT0WFRX+UAAABAAP/80D6wOpAFMAmgC4AMQAACU+ASc+ATc2Nz4BNzY3NiYxLgEnMCYHBgcOAQcGBw4BByYGBw4BMQYWFzAWHwEGFh8BDgEHBgcOARcWMTAXFjY3Njc+ATcXHgE3Fx4BMR4BNzA2NycOAQ8BDgEHBiYvAT4BNz4BJyYGBw4BBycuATc+ATc2NDc+ATc2Nz4BNzY3Njc+ATc2NzI2MxwBFQYHDgEHBgcGBw4BBwYHBQ4BBwYiIzwBNz4BNz4BNxcOATEGFjcwNjcXDgEHASYiBwYUFxYyNzY0AxAaBQYZMBUdExIXBQUBAgQCIBMgQCgvL2Y0NTMmQh0VRDBWWQ0KFDsOBBcIHAcQHg0hDAsBBQUaGlQ2NTgWKBMHHVghBQ0KAxoRZy/yAgYEBAYLBAURA2UYMxhPLBYWaE8ZKRBkAwIDAwcFAQEDBQImIyNFJSQqJikpVy4uLhAaCQIFBhUQEBYXGxtCKSgz/vIfQSIIDwcBAxQTCRYNKgcGDRUbEAwrEB8PAfodUR0dHR1RHR18MUMVHUMlMzU0ZS8vKEEfFR8CBAIBBQUWExMdFTAZBgUaLmgQGwMJDgUhWB0GFCgVOTU2VBoaBQUBCwwhDR4QBh0IFwQOOxQKDVlWGwIFAgMECAIEAQRlECgZT2kVFitQGTIXZAMSBAQLBgEBAQUHAjIpKEIbGxgVEBAVBgYBAQoaEC8tLlcpKSYqJCVFIyMmPhMUAwEHDwgiQh4PHxAqDQ8cFQ4GBysNFgkCcR0dHVEdHR0dUQAAAAACAAD/7gQAA5IANQBYAAABITgBMSIGDwEnLgEjOAExISIGHQEUFjsBETgBMRQWFx4BMwUeATMyNjU4ATERMzI2PQE0JiMDIyIGFRElLgEjIgYVDgEjIiYnETQmKwE1MxceATMyNj8BMwOf/vkLEgZ1dQYSC/75KDk5KFlBMQIBAQHVAwgEFB5ZKDk5KAOIFR3+9AUOBxUdAR8WFh8BHRWI74oGEwoKEwaK7wOSCAd0dAcIOSiwKTn+1TVSDQEBbgEBHRUB/zkpsCg5/vIeFf4ppQMEHRUVHh4VAV0VHqqJBwgIB4kAAAAAAwAA/8AEAAPAABwAMABFAAABIzU0JiMhIgYVERQWFxU3HgE7AQU1PgE1ETQmIwERNDYzITIWFREUBiMhBzUjIiY1BRQGKwEVJyEiJic3MzI2PQEzMhYVA2YzWEH+AEJYOC57E0gq8QFDLjhYQv0AHRcCABccHBf+8L0zFx0DNB0XM73+8A8bBHHxQVgzFx0C8zNCWFhC/s0yTg/XTSMqzdcPTjIBNEFY/wABMxcdHRf+zRcce3sgE80XHHt7EA9IWEGaHBcAAAAIAC7/wAPSA78ADQBcAGsAewCJAJYAowCwAAATBhYXFjY/ASYGJyYGBwUuAQ8BJz4BNz4BJwMFFyUDBhYXHgEXBycmBgcGFh8BOAExMDIxFzIWMzI2NzYmLwE3MhYzMjY/ARceATMyNjcXBw4BFx4BMzI2PwE+ASclDgEnOQEuAScuATcTBQM3AzcXFgYHDgEHDgEjIiYnJS4BBwYmBxceATc+AScBPgE1LgEHIgYVHgEzFz4BNS4BIw4BFxQWMwcUFjc+AScuAQciBheXCTUwMFMLEzAUVDkpAwM7BBYLUDMgMRATBg6E/qIX/tVjCQ8XEzcgIlIMFAIDDQxuAW4CAwEKEAIDDQxSIQUIBEp2EBcPFm9FChMKMlALCwQDDwkCBQLXDAoD/dINbT4eMxARCwZSAQEkl0P6bgoFDg4uHQwZDDJRDwEFBC44USItJw9ZLy4sDv4PDRIBFA4NEgEUDn4NEgEUDg0SARQOkBUNDhIBARQODRIBAY4vUgsKNjCzG4ISDDAK1wsLBBuYDzAgJlQqATF1X0H+wClTJB0rC5wRAw0MDBQCGBgBDQoMFAISnAFfS68+QlADAZcaBBYLCQsBAUcEFQyjPUcNByMaGjsdAQo4/u93AQ5T/R49GxwoCgQEOi5TCisTG4AgsC8sDxBXLgFaARQODRIBFA4NEjkBFA4NEgEUDg0SGw4SAQEUDQ4SARUNAAcAAP/ABAADwAATAB0AIgAmACoALgAzAAABIREhMhYVFAYjIRUhMjY1ETQmIxMuASMhESEyFhUpAREhEQcjNTM3IRUhFSEVIQUhFSE1A2b8mgNmFx0dF/7NATNCWFhCNAwYEP0AAwAXHf4z/s0BM2dmZs0BM/7NATP+zf5nAsz9NAPA/M0cFxcdZlhCAsxCWP0pBAYCZx0X/s0BM8xmZmZmZ2ZnZwAAAAEAFP/FA+kDqwAmAAAJATY0JyYiBwkBJiIHBhQXCQEGFBceATMyNjcJAR4BMzI2NzY0JwECSwGeEREPLRD+Yv5iEC0QEBABnv5iEBAIFAsKFAgBngGeCBQKCxQHERH+YgHAAZ4QLRAQEP5iAZ4QEBAtEP5i/mIQLRAICAgIAZ7+YggICAgQLRABngAAAQAPAEED8QMwABYAACUuAScDJjQ3NjIXEwE2MhcWFAcBDgEHAVUXJgr/CwsOJQ7+AmUOIw0LC/2pCyQWQQIZFAEaDSMNDAz+5wKCDAwOJA79fxMZAgAAAAMAAP/BA8oDiAAQABoAQQAAAQcBNzY3PgEnJicmJyYGBwYBBzM3LgEnLgEnBQYiJyY0NwEnAQYiJyY0NwEnAQYiJyY0NwEnAQceARceARc3AScBAiIUAXMVIBMTARMTLCwyMmEsLP3ICgGuAhsWFjshAX4JGAgJCQGIPf52CBkHCQkBhz3+dwkYCAkJAYg9/jwLJkQaGiACvwHCPf54A1QU/owVICwsYTIxLCwTFAITE/z7rgkhOhcWGwIfCQkJGAgBiD3+egkJCBkHAYg9/nkJCQgZBwGIPf4+vwIfGhpDKAwBwj3+ewACAAD/wAQAA8AAAwAHAAATESERAyERIQAEAID9AAMAA8D8AAQA/IADAAAAAAMAAP/ABAADwAADAAcADQAAExEhEQMhESEBJzcXNxcABACA/QADAP5AwFpm5loDwPwABAD8gAMA/cHAW2bmWwACAAD/wAQAA8AAAwAJAAATESERASc3FzcXAAQA/cDAWmbmWgPA/AAEAP1BwFtm5lsAAAAAAQAA/8ADwwPAADkAAAEmJy4BJyYjIgcOAQcGFRQXHgEXFjMyNz4BNzY3JwYHDgEHBiMiJy4BJyY1NDc+ATc2MzIWFwchEQcDMCEkJVErLC1nWluHJycnJ4dbWmczMTBZKCghOR0hIUopKCtWS0xxICEhIHFMS1ZKhTaPAVyTAy0bFhUeCAgnJ4daW2ZnWluHJycKCiYbGyE6HBYXHwkIISBxS0xWVUxLcSEgMSuPAVyTAAAABQAAAA4EdANyABgAYgCBAJAAngAAAREUBisBPgE1PAE1MxEhEQcRNDYzITIWFQEjNz4BNTQmJy4BIzEiBgcFIw4BIyImJy4BNTQwPQEiJiM4ATEiBg8BDgEVERQWOwEyNjceATMyNjceATMyNjceATMyNjc1NCYjNzU0Jic+ATU0JiMiBhUUFhcOAR0BFBY7ATgBMTI2NScjIgYVFBY7ATI2NTQmIwcyNjU0JisBIgYVFBYzBHQ0JeYMDcX92mA1JQI0JTT+hJ+KEBUDAgcnGQcOB/75AQULBg4YCQcIBAgFHDMVwgYHEAuCSYY4BSMYERsKByIWFiMHCRwQGycBDgvgIRkMDyUbGiUPDBogCAWjBQjjrwwREQyvDBAQDCAMEBAMjwwQEAwDGf5sJTQRKhcEBwMBhv63JgF2JTQ0Jf3LWAojFAcNBRYcAgNnAgILCggWCwEB8wESEJAEDQj+aAwPKiYXHg8MFBkZEwwOJBpjCw/8IB0vCgkcEBomJhoQHAkKLx0gBQgIBesRDAwREQwMEaIQDAwREQwMEAAAAAADAAAAJgP/A1oAPwBfAIEAACUhIiYnLgE1NCc8ATU0NTQ2NzYWMzI2Nz4BNz4BMzIzMhYXHgEXHgEzOgE3NhYHBhUGFBcUFxQGIyYjKgEjIiMTMCIxIgcOAQcGFRQXHgEXFjMyNz4BNzY3NCcuAScmIxceARcUBiMiJicmNjM4ATEyFhcuASMiBhUUFjMyNjU0JicB//5pHi8OBgYBOS0aNRoeGAkGDQYGFxKpqRMXBQcOBwcXFxs1GjE9AQEBAQE6LzMzM2UzMzMBATUuL0YUFBQURi4vNTUvLkYUFAEUFUUvLjV/AgEBHRIUGgEBHRMGDAUVNx9AWlpAP1oODCcZGgsaDTo6OXM6OjktOQEBAREcEyUTEREQEhQpFBUSAQFCLDg5OXE5OTkmQgECZhQURi4vNTUvLkYUFBQURS8uNTUvL0YUFKsECAQTHBwTExwDAxQXWUA/Wlo/GCsSAAAABQAT/8AD7QPAACoANgBCAE8AaAAAASM1NCYrASIGHQEhNTQmKwEiBh0BIyIGFREUFjMhNT4BPwE+ATUzETQmIwc1MxUUBisBIiY9ASE1MxUUBisBIiY9AQEjIgYdASEiJjURIREFFjI3AT4BLwEmIg8BBiIvASYGDwEGFh8BA4syJBkFGiT+UCQaBRkkNCk5OSkCtgUIA6UEBQQ5KbKAJBkFGiT90IAkGgUZJAL/YRom/W4MEQNQ/iAFDwUBLwUBBjMGDwbgBg8FbwYQBTYFAQa+A0o5GSQkGTk5GSQkGTk6Kf08KToCAQQEqwQLBQJdKTojGT0ZJCQZJBk9GSQkGST9jSYabhEMAij+aTcFBQEnBg8GMwYG2QUFaAYBB0EGDwWnAAkAFv/AA+oDwAArADcAQwBRAF0AagB2AIIAjgAAEyMiBh0BIyIGFREUFjMhMjY3PgE/AT4BNRE0JisBNTQmKwEiBh0BITU0JiMHNTMVFAYrASImPQEhNTMVFAYrASImPQETESMiBh0CISImNREhBTI2NTQmIyIGFRQWITI2NTQmIyIGFRQWMyMyNjU0JiMiBhUUFgcyNjU0JiMiBhUUFiEyNjU0JiMiBhUUFuYGGSU0JDQ0JAJ6BAcCAwUD2AIQNCQ0JRkGGSX+SCUZRIIlGQYZJQI6giUZBhkl35EdK/2MERgDdv1XFx8fFxYgIAHyFiAgFhcfHxfuFiAgFhYgINgXHx8XFiAgAQQWICAWFiAgA8AlGUA0Jf0vJDQCAgEDAtoDFgcCJSU0QBklJRlAQBkllhQ+GiUlGioUPholJRoq/u3+uSsdmAEYEQH/uyAWFx8fFxYgIBYXHx8XFiAgFhcfHxcWIOsgFhYgIBYWICAWFiAgFhYgAAADAAD/wAQAA8AAGwAnADMAAAEiBw4BBwYVFBceARcWMzI3PgE3NjU0Jy4BJyYTIREXHgEzMjY/ARERBw4BJwYmLwE1IRUCAGpdXosoKCgoi15dampdXosoKCgoi15dwf2q0RQuGBguFNH4CRsPDxsJ+AJWA8AoKIteXWpqXV6LKCgoKIteXWpqXV6LKCj9KwEckQ0NDQ2R/uQBb6sHCAEBCAerOzsAAAADAK//wANRA8AAGgAqAD8AAAE0NjMyFh0BMzU0Jy4BJyYjIgcOAQcGHQEzNQUhIgYVERQWMyEyNjURNCYBFRQGIyImPQEuATU0NjMyFhUUBgcBf0s2NktKEBA3JSUqKiUlNxAQSgGY/dIYIiIYAi4YIiL++RcRERcNDygcHCgPDQL1NkxMNmlpKiUlNxAQEBA3JSUqaWmSIhj90hkiIhkCLhgi/q1qEBgYEGoJHREcKCgcER0JAAAACQA9ADkDKwNHADoAdQB6AH4AmgC8AM4A4ADsAAA3MSImJy4BNz4BNz4BNz4BNz4BNz4BMzIWFxYGBw4BFx4BMzI2Nz4BMzIWFxYGBw4BBw4BBw4BBw4BIxMiBgcOAQcOAQcOAQcOAQcGFhceATMxMjY3PgE3PgE3PgE3PgEnLgEjIgYHDgEjIiYnJjY3PgEnLgEjJRcHJzcnFwcnJSImJy4BNz4BNz4BNz4BMzIWFx4BBw4BBw4BIzciBgcOAQcOAQcGFhceATMyNjc+ATM4ATEyNjc2NCcuASMBIiYvASY0NzYyHwEWFAcOASM3IiYvASY2NzYyHwEWFAcOASMHFhQHBiInJjQ3NjL/OV0DKS4uDykVFSUNBwkFBQwIEikUEBwLDQYFBgYMChMIBQoGBQsGDhAEDy4aDRkIBQYCBRciDyQVTwwbDQcKBAYMChIrFxMiDCEoIxFLJQ4YCx0TBQMICQocDxcbBAIBAwIIBQYOBxUhDBEIBwUDBgYOCAEYGOUX5DIXyxgBBgwWCRANAQMpIAgPBx0vFgsTCA4KBgUjIBE/JYUPKBoHDwgUJwEBCQsECgUWNhECCAUXGAMCCAUJBf4CAwYDMwQFBA4FMwQFAgYDMwMHAjMFAQQFDgUyBQUCBgMaBgYFDwUFBQUPOUwDKpQvEBIIBxEOBxoODx4NGBsQDxQhDw8kGxUKAgEBAg4HHSwRCBIKBhYOGkUjDxACBxMSChoOER8LEhQIBw8MInokEjIKCx47GREdCwsVCRAXCQIBAQEBAxkaJDESDRAICQhHFuwX6zQW0Rd9CgkQHw8WJhMFCgQSGgcGCiASECACHka+FhAFCQUNHQ4HEgoFBDkiBAUSCgMOBgMD/aQCAzQFDQUFBTQFDgQDAjQDAjQFDgQFBTQFDgQDAlgFEAUGBgUQBQUAEgAAAHYDMwMBAAsAGAAlACoANgBCAE8AWwBnAHMAfwCLAOwA8AD0APgA/AGIAAA3FAYjIiY1NDYzMhYHMjY1NCYjIgYVFBYzNTIWFRQGIyImNTQ2MwczESMREzIWFRQGIyImNTQ2EzIWFRQGIyImNTQ2AzIWFRQGIyImNTQ2MxUyFhUUBiMiJjU0NicyFhUUBiMiJjU0NhMyFhUUBiMiJjU0NhMUBiMiJjU0NjMyFiUOARceATc+AScuAQEnNCYnLgEHDgEHMRUcATEwFDEXByYiByc+ARcWNicuAScuAQcGFjMyFhcWNicmNjcXBxc3HgEXHgEXFjY3NjQnMCYnNz4BNSc6ATMwFjEeARUHDgEVBzM/ARcUFh8BNycBJzcXFyc3FxcnNxcXJzcXNz4BNz4BNz4BNz4BNz4BNz4BNz4BNz4BNz4BNz4BNz4BNzoBFzIWFx4BFzIWFx4BFx4BFx4BFx4BFx4BFx4BFx4BMTAmJy4BJy4BJy4BJy4BJy4BJy4BJy4BJy4BJy4BJy4BJwYiByIGBw4BBw4BByIGBw4BBw4BBw4BBw4BBw4BBw4BBw4BMRcwNjeOEAsLEBALCxAbHioqHh4qKh4aJSUaGiUlGnPm5tcDBAQDAwQEAwMEBAMDBARhDhQUDg4UFA4kNDQkJTMzPwMEBAMDBAQDAwQEAwMEBHMHBQUICAUFBwEcFg0ODTIXFgwNDjIBKgwOIydAChUcAQEyAgUDrwQSDREFEhQ8BQRCOjpdERIYCgsWDQoOBMEGCygBAgEBAgIECgMCAQICMAkMAQEBARcTCToCAgRCBCgJBQRgL1j9tA0VDAQEFQMCAxQDAgMUA08CAwIBBAICBAICBQMDBQMDBwMBBAECBAIDBwQIEAgJEQkIEggJEQgECAQBBAICAwIEBwMDBgMGCwQCBQIBBAEGBwYGAQMCAgQCBAoGAwYDAwcEAQQCAgMCBAgECBEKCBMJChIJChIIBQgEAgQCAgQCAwgDBAYDBAYDAgYCAwQCAgQBBgcXBgX6CxAQCwsPD1IpHh0pKR0eKYQkGRokJBoZJMEBV/6pAUwEAgMEBAMCBP7MBAMDBAQDAwQBIxQNDhMTDg0UYTMjJDMzJCMzcgQCAwQEAwIE/swEAwMEBAMDBAECBQgIBQUHB/0NMhYXDA4NMhYXDP57fwk+LDIUAQEeFAIBAQFnCwEBjAcOBgkYDhARERFOS0ssJQwMCA8MFwSPATQJAgQDAgUBAwEEAwgDAwILAg4JSxsYHAFmBAgErKNIWQUKBGMuWwFXChoJLQIbAysCGgIsAxoCVwIDAgEDAQIDAgIDAgIDAgIDAgEBAQEBAQICAgIFAQICAQIEAQEDAQEBAQEBAgICAQQBAwcDAQMCAQMBBQYGBgEDAgIDAgMIAwIEAgIDAgECAQEBAQEEAQIGAQICAQEBBAIBAwEBAQECAQEDAgIDAgIDAgEEAgEEAQICAgUFGQYFAAAABAAA/8AEAAPAAAMAEwAkADAAACUhESE1ISIGFREUFjMhMjY1ETQmAzQnLgEnJiMiBw4BBwYdASEBMjY1NCYjIgYVFBYDjvzkAxz85C9DQy8DHC9DQ70cHFAsLCAgLCxQHBwCAP8ANUtLNTVLSzIDHHJDL/zkL0NDLwMcMEL9DiAYGCAICAgIIBgYICoBDks1NUtLNTVLAAADAAAARgSSAzoATwCYAOUAACUmJy4BJyYnNTQ2Nz4BNz4BNz4BNz4BNTQmJy4BJy4BJy4BIyIGBw4BBw4BBw4BFRQWFx4BFx4BFx4BFx4BHQEGBw4BBwYHDgEdASE1NCYnNy4BJzU0Njc0Njc+ATc+ATc+ATU0JicuAScuAScuASMiBgcOAQcOAQcOARUUFhceARceARceARceARcVDgEHHgEXOgExNTQmJyUuASc1NDY3PgE3PgE3PgE3PgE1NCYnLgEnLgEnLgEjIgYHDgEHDgEHDgEVFBYXHgEXHgEXHgEXFhQdAQYHDgEHBgcOAR0BMDIzPgE3A5cDJCNWJCUEAQECAwECBAIICwUEBQYGBRAKCRcNDRsPDxwNDBcKCRAFBgUEBAUMBwMDAgEDAQECBCEgTiAhBQlVAyY7C7YFiAYBAQICAQMBBQkDAwMEBAQLBwcQCQkTCgsTCQoPBwcLBAQEBAMDCAYBAwEBAQEBAQEGTwQoa1dacT8G/WsFUAYBAQECAQECAgUJAwMDBAQECwYHEAkJFAoLEwkJEAcHCwQEBAMDAwkFAgICAQIBAQIaGjwZGgEIKnFaV28n9QEQECYQEQJBAgcFBQoFBg0HDyETEycVHDATFCAMDBEFBgUFBgUSDAwgExQvHBUpExMhDwcMBQUJBQQHAj8CDw8jDw8CBDY3SUk3KgWUAz0DLgEFBAMHBAQJBQsYDQ0cDhQiDQ4WCQgNAwQEBAQEDAkIFg4OIRQPHA4NGAoFCAQDBwMDBQIsAzsBJS1BPCQqAgcBOgMsAgQDBAYEAwkECxgNDR0PEyINDhYJCA0EBAMDBAQMCQgWDg4hFA8cDQ0YCgUJBAQHBAMFAi0BDAwbCwsBAyEkO0EuJAAAAgAAAEQDMgM8AAkAEAAAJSERITUhESERIwMXARcBFxECkP3kAQ7+fgMDc65+/sRSAT99uAHhc/04AWQBlHb+xFIBP3UBOgAAAwA6/8ADxgPAAAUACQAVAAABJwcnNxclMxEjASERMxUjESERIzUzAqampk3z8/7VcHAB/vx09IMCqoP0AomdnVHm5pn9af7kAqRx/j0Bw3EAAAAABQAAAAcEAAN5AAsAKgBHAFIAXgAAARQGIyImNTQ2MzIWAyInLgEnJi8BNz4BNz4BMzIWFx4BHwEHBgcOAQcGIyUWFx4BFxYzMjc+ATc2NyYnLgEnJiMiBw4BBwYHEzcXHgEXCwE+ATcBFyE3LgEnByEnDgECaT4sLD4+LCw+aU5ERGUeHwIVEwE6MkOKQkKKQzI6ARMVAh8eZURETv7QEh8fTy8vMzMvL1AeHxISHh9PMC8zMy8wTx8eEvQ8PBcwGJubGDAXAYM//PQ/Dx4NfwQAfw0eAWssPT0sLD4+/vgbHEMdHQMWGAFBJzU2NjUnQQEYFgMdHUMcG9ERGBcsDxAQDywXGBATHBwzEhMTEjMcHBIBJWdnAwwJAQz+9AkMA/41amoKFQrc3AoVAAAAAAEAAAAmA1oDWgAHAAATATUhESE1AQABrQGt/lP+UwHA/mbDAa7D/mYAAAAAAwA//+ID3gOBACQAQQBPAAABNjc2JicmJyYnJiIHBgcGBwYUFxYXFhceATc2NwEWMjc2NCcBBSYnJjQ3Njc2NzYyFxYXFhcWFAcGBwYHBiInJiclMjY1NCYjISIGFRQWMwLRIQ0NDhobLjZERI5ERDc2GxsbGzYvODl4PTw1AQ0aSBkZGf7z/f0qFBUVFCopNDVsNDQqKRUUFBUpKjQ0bDU0KQFLEhkZEv77ExkZEwFqNTw9eDk4LzYbGxsbNjZFRI5ERDYuGxoODQ0h/vMZGRlJGQENCCo0NG00NCkqFBUVFCopNDRtNDQqKRUUFBUpnBoSEhkZEhIaAAAAAwA//+ID3gOBAB8ARABhAAABMzI2NTQmKwE1NCYjIgYdASMiBhUUFjsBFRQWMzI2NQU2NzYmJyYnJicmIgcGBwYHBhQXFhcWFx4BNzY3ARYyNzY0JwEFJicmNDc2NzY3NjIXFhcWFxYUBwYHBgcGIicmJwHCVxIZGRJXGhISGVcTGRkTVxkSEhoBDyENDQ4aGy42RESOREQ3NhsbGxs2Lzg5eD08NQENGkgZGRn+8/39KhQVFRQqKTQ1bDQ0KikVFBQVKSo0NGw1NCkB/hoSEhlYEhkZElgZEhIaVxIZGRI9NTw9eDk4LzYbGxsbNjZFRI5ERDYuGxoODQ0h/vMZGRlJGQENCCo0NG00NCkqFBUVFCopNDRtNDQqKRUUFBUpAAAAAAMAAAAGBAADegADAAcACwAAASM1MxEjNTMFIQkBAi9eXl5e/dEEAP4A/gABTLr+i17pA3T8jAAACQAA/8AEAAPAAAsAFwAjAC8AOwBHAFMAXwBrAAATFAYjIiY1NDYzMhYRFAYjIiY1NDYzMhYBFAYjIiY1NDYzMhYRFAYjIiY1NDYzMhYBFAYjIiY1NDYzMhYRFAYjIiY1NDYzMhYBFAYjIiY1NDYzMhYFFAYjIiY1NDYzMhYFFAYjIiY1NDYzMhb0SDIzR0czMkhIMjNHRzMySAGGSDIySEgyMkhIMjJISDIySAGGRzMySEgyM0dHMzJISDIzR/z0SDIzR0czMkgBhkgyMkhIMjJIAYZHMzJISDIzRwNGMkhIMjNHR/5HMkhIMjJISAFUMkhIMjNHR/5HMkhIMjJISAFUMkhIMjNHR/5HMkhIMjJISP5IM0dHMzJISDIzR0czMkhIMjNHRzMySEgABAAA/8AEAAPAAAMABwALAA8AAAERIREhESERAxEhESERIREBq/5VBAD+Var+VQQA/lUCFQGr/lUBq/5V/asBq/5VAav+VQAABgAQ//QEAAOMAEYAXQBrAHwAigCbAAABIgYVFBYzMhceARcWFRQHDgEHBiMiJy4BJyYnFx4BMzI2NzY0LwEmIg8BBhQXFjI/ARYXHgEXFjMyNz4BNzY1NCcuAScmIwMiJicmND8BETQ2MzIWFREUBg8BDgEjJSMiJjU0NjsBMhYVFAYHIiYvASY0NzYyHwEWFAcOAQciJj0BNDYzMhYdARQGEyImJyY0PwE2MhcWFA8BDgECNBAYGBBPRUVnHR4eHWdFRU9IQEBkISEJHAYPCAgPBQwMZAwiC2UMDAwhDCIIJyh6UE9ZX1RUfSQkJCR9VFRfmAgQBg0NhhkSERkGBpMGEAgByi0LERELLQsREXUFCwQfCAgIGAgfCAgECuMMEBAMDBAQsgYKBAgIHwgYCAgIHwQLA4wYERAYHh5nRUVOTkVFZx4eGRpYPDxFHAYGBgYMIQxlDAxlDCEMDAwiV0xLcCEgJSR9U1RfX1RTfSQl/VQHBgwkDIYBDxIZGRL+4AkQBpIGB8YQDAwQEAwMEN4EBR8IGAgICCAIFwgFBFsQDCwMEBAMLAwQAfcEBAgYCB8ICAgXCR8EBAAABgAA/8sEAAO1AAwAGQA3AF8AfgCMAAATFBYzMjY1NCYjIgYVMxQGIyImNTQ2MzIWFRMhESEVFBYzMjY9ATQmIyEiBhURFBYzITI2NTQmIz8BFxYyNzY0LwEmIg8BJy4BIyIGIwcGFBceATMyNj8BFxYyNzY0LwElNCYjIgYdAScmIgcGFB8BHgEzMjY/ATY0JyYiDwE1EyEiBhUUFjMhMjY1NCauPy4uPz8uLj+vJB4aJycaGigr/s8CjhgTFBgmF/0+FyYjGgFLFBgYFGlTRQcRBwYGVwQTBGQnAwgGBAsDrgcHAwgGBAoEnVIHEQcGBhoBSxgUExgfDSMNDQ1pBg8JCg4HaA0NDSMNHpj+eBMYGBMBiBQYGAKaLj9CKy4/Py4XKicaGicnGv4yApLaFBcXFPQXJiYX/ToaIxgTFBjVU0UHBwYSBlcHB2QnAwIFsgcRBwMBAQOhUwYGBxEHHh8UGBgU4x8NDQ0jDWQHBgYHZA0jDQ0NH+P+YhgUExgYExQYAAUAUf/AA5oDegAoAFUAaQBtAHEAAAEnNjc2JicmJyYnJgYHBgcGFBcHJicmNDc2NzY3PgEXFhcWFx4BBwYHBSYnJjQ3Njc2Nz4BFxYXFhceAQcGByc2NzYmJyYnJicmBgcGBwYHBhQXFhcHJRMjJyEHIxMuAScmNjc2FhcWBgcXJwczFycjBwLXPRoJCQwVFSQjKClPJCQaKCg9Gw4ODg4bIzEwbTc3MDAcHREMDSP9uSoVFRUVKjVLSqZUVUlJLCwaExM1PSwPEBUlJD08RkaJPT4sIxEREREjPQGIy1EZ/vAZUcoPGQcQGyQkShARHCQUOzx3PCuZKgEpLSMoKVAjJBoZCgkNFRUjN4k3LSYrK1krKyUwHR0RDQwjIzEwbTc3MFk6QkKHQkI5SS0sGRMTNTVLSqZUVUktPEZGiT0+LCwQEBYkJTwwNjdwNjcvLav+RTc3AbsHGBAkSREQGyQkShDdgoKDXl4AAAAGAAAAJgMzA1oADQAbAC0AOwBMAHgAAAE0JiMhIgYVFBYzITI2ByEyNjU0JiMhIgYVFBYnNCYrASIGFRQWOwE4ATEyNjUFISIGFRQWMyEyNjU0JhMhDgEVERQWFyE+ATURNCYnAyEiJjURITI2NTQmIyE1ITI2NTQmIyE1NDYzITIWFREhIgYVFBYzIRUUBiMB9A8M/uMLEBALAR0MD4UBPwsQEAv+wQsQEEgQC2ALEBALYAsQARP+4wsQEAsBHQsQEJP9mSo8PCoCZys7OyoB/ZkUHAFRCxAQC/6vATkLEBAL/sccFAJnFBz+/gsQEAsBAhwUAmALEBALCxAQ5A8MCxAQCwwPGwsQEAsMDw8MuhALCxAQCwsQAogBOyr9mCo7AQE7KgJoKjsB/QMbFAFPEAsLEJ4QDAsQDhQbGxT+SRALDBB6FBsAAgAj//ED3AOPACcAQwAAJRU8ATUuATU0Jic4ATkBLgEnDgEjIiYnDgEHDgExBhYzITI2JzAmJyUyNz4BNzY1NCcuAScmIyIHDgEHBhUUFx4BFxYD0AEBAQEYalcrfkpKfipbbRYIBQFNOAKyOE0CBAj+MDUvL0UUFRUURS8vNTUvLkYUFBQURi4v3wIBAQEBBAECBAJkUis2QEA2LVduKjo4UFA4PyevFBRGLy42NS4vRhQUFBRGLy41Ni4vRhQUAAAAAAMAQf/MA78DpAAbADEAPwAAASUmIgcFDgEVERQWFwUeATMyNjclPgE1ETQmJwEVIzUBIyIGBw4BBw4BBw4BByc3IRUDIzU+ATc+ATc+ATcXBwOH/rEaPBr+sRoeHhoBTw0cDw8cDQFPGh4eGv3ndwGShCQ4FRQlEA4UBwcJAwoKAe0BxyM3ExglDwYJAwoOAuPBEBDBEDQd/nwdNQ/BCAgICMEQNB0BhB01D/29DgoCXAUFBRINCxcNDR8RAagL/ZYSBRAMDyweDB4RAcYAAAAEAEz/3AOzA5UAGwA7AFIAYAAABSImJyUuATURNDY3JTYyFwUeARURFAYHBQ4BIxEiBgcFDgEVERQWFwUeATMyNjclPgE1ETQmJzElLgEjAzEBNSEHFz4BNz4BNz4BNz4BOwEBFTMlDgEHDgEHDgEHFTM3IwIADx8N/sIbICAbAT4bPxsBPhsgIBv+wg0eDwwYC/7CFhkaFQE+CxgMDBgLAT4WGRoV/sILGAyKAXn+LQsLAgkGBxMNDyMUEzUiff6EcQF8AwkGDiMWEzMhvQ0KJAcItxA3IAFuIDcQtxAQtxA3IP6SIDcQtwgHA7IGBrgNKxr+khosDLcHBgYHtw0rGgFtGiwMuAYH/RYCPAqfAREdDQwWCgwRBAUG/cMKvA8cDBwqDgsQBBG7AAAFAAD/wAQAA8AAHgA1AEMARwBjAAABJS4BIyIGBwUOARURFBYXBR4BMzI2NyU+ATURNCYnARUjNQEjIgYHDgEHDgEHDgEHJzchFQEFIzU+ATc+ATc+ATczBwERIREDFAYHBQ4BIyImJyUuATURNDY3JTYyFwUeARURA23+wgsYDAwYC/7CFhkaFQE+CxgMDBgLAT4WGRoV/glxAXx9IjUTFCMPDRMHBgkCCwsB0/6HAXm9ITMTFiMOBgkDCg39EQQATSAb/sINHg8PHw3+whsgIBsBPhs/GwE+GyACybgGBwYGuA0rGv6SGiwMtwcGBge3DSsaAW0aLAz92w4KAj0GBQQRDAoWDA0dEQGfCv3EDREEEAsOKhwMHA+7Ayn8AAQA/UkgNxC3CAcHCLcQNyABbiA3ELcQELcQNyD+kgAAAAsAKv/oDAQDmAAjAE0AUABqAIQAoADyAUQBdQGTAaMAACUBIy4BNTE0JjU8ATc0NjUVIRcBIR4BFzEcARUcARUOAQc1ISUjBw4BIwYiIyoBJyImJzMTPgE3OgEzOgEzHgEXEw4BIwYiIyoBJyImJyczAyU+ATc6ATM6ATMeARcRDgEjBiIjKgEnIiYnEz4BNzoBMzoBMx4BFxEOASMGIiMqASciJicbAT4BNzoBMzoBMx4BFwMTDgEHIgYjIiYjLgEnBSoBIyImJxcuAS8BLgEvAS4BNTwBNTE8ATU0NjcHPgE3Bz4BNzM+ATMyFhcnHgEfAR4BFxUeARUcARU1HAEVFAYHNQ4BBzcOAQ8BDgEjKgEjMTU6ATMyNjcjPgE3MT4BNzU+ATU8ATUVPAE1NCYnFS4BJxUuASc1LgEjIgYHNw4BBzEOAQcVDgEVHAEVMRwBFRQWFzUeARc1HgEXMx4BMzoBMzEBJS4BIyIGBzEFDgEVOAE5ARE4ATEUFhczBR4BMzI2NxUlPgE1OAE5ARE4ATE0JicjATEVIzUBIyoBIyIGBzMOAQcxDgEHFQ4BBxUnNyEVAyM1PgE3Bz4BNzU+ATc1MwPRARz5AQEBAQIBbgX+5AEIAQEBAQEB/oMC/tYnBQoFBQoFBQsGBQoFAbcFCwYGDAcGCgYGDAW2BQoGBgsGBQoGBQoF56xXATkFCgUFCgUFCwUFCgQECgUFCgYFCgUGCQXbBAoFBQsFBQoFBQoFBQoFBQoFBQsFBgkEZ7UGCgYFCgQEDAYGDAazygcMBgUMBgUKBQYLBgFsAQMCGzMXARYlDgEOFwYBBggIBwEHFw8BDyUVARg1HR02GAEWJQ4BDhYHBwcHBwYXDwEPJRUBFjMbAgMBAQIBESEPAQ4YCQoOBAUFBQUEDgoJGA4PIxITIhABDhgJCg4EBQUFBQQOCgkXDgEOIBIBAgH4LP7EDBsODxsM/sQYHh0YAQE9CxsPDhsMATwYHh0YAf4FcQF8fQIGAxowGAMUIw8LFAcHCQIKCgHSAbwdNBcCGCQMBggDCqUB5wMIBQUJBAQKBAUJBAEH/hoECAUFCQQFCQQFCQQBgoIBAQEBAQECMwEBAQEBAf3NAQEBAQEByAEfTAEBAQEBAf3NAQEBAQEBAjMBAQEBAQH9zQEBAQEBAQEeARUBAQEBAQH+8f7cAQEBAQEBAQEJCwsBCx8SARQtGQIZOh4BAgEBAgEeOhsDGy4UARQeCwsLDAsBCx4TARMuGQIZOR4BAwEBAQIBHjocAxovFAETHwoBCgtFCAcIFg4PIxMBFCwXAgICAQECARgtFgMUIxABDhYHAQcICAgBCBYODyMTARQtFwECAQECAhctFgMUIxABDhYICAcB+bcHBwcHtw4xHf6SHTEOtwcHCAcBtw4xHQFuHTEO/d0NCgI6BQUFEQwJFgwBDR0PAQGeCv25EQMQDQEPKhoBDBsPAQAAAAAFAGf/wAOaA8AAAwAHAAwAEAAUAAABEQEXJREBFyU3FwcnBQcXISUXIScDev2NggGL/pOC/j6Dg4ODAmKDTgEG/W1OAQTQArsBBf2Ng0wBBv6Rg4ODg4ODHoFOTk7PAAAMAAAACg1VA3YAAwAHAAwAEAAUABgAHQAoAC4AOABhAGoAABMXATUDARc3BTcXBycFFzMnBRczJwUzESM1MzUjFRMzNTM1IzUhNSURASMRITUhAQsBIwEVMzUBIwEuASc1PgE1NCcuAScmIyERITUzNSM1ITUhBxEhETMyFhceARczLgEnJyM1MzIWFRQGim8Bqlf+xnDK/bRwcHBwAZtC4LP+gELfsgIhvb29vee97OwBEP4zArC8AcT++AIbkZDXAQi9AQjVBG4EJzMzMxQUQCkpK/7x/tDo6AEL/qJwAoxNPCYDAQcMqBEIAvFgWSQyKAFecAGp3/6Y/sdwyVlwcHBwiUKycEKysgHOLKen/gb7qFenBf1aAqD9YKYB+v7vARH+W/v7AaX99zBzDQIUTzczJCQtCwr+BlWoV6ex/hEBA2UzDk0QEXAV9IkfJiYeAAAAAQAAAAEAALJN+INfDzz1AAsEAAAAAADY240yAAAAANjbjTL//v+/DVUDwAAAAAgAAgAAAAAAAAABAAADwP/AAAANVf/+AAANVQABAAAAAAAAAAAAAAAAAAAAYgQAAAAAAAAAAAAAAAIAAAAEAAAXBAAA3QQAAAQEAAAABAAAAAQAAAAEAP/+BAAARQQAAAAEAADuBAAAAwQAAAIEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAFsEAABbBAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAABAAAAAQAAGoEAABxBAAAhQQAAAsEAACHBAAAhgQAAAAEAAAoBAAAJQQAAAAEAAAABAAAAAQAAB0EAAAdBAAADwQAAAAEAAAABAAALgQAAAAEAAAUBAAADwQAAAAEAAAABAAAAAQAAAADwwAABHQAAAQAAAAEAAATBAAAFgQAAAAEAACvAzMAPQMzAAAEAAAABJIAAAMyAAAEAAA6BAAAAANaAAAEAAA/BAAAPwQAAAAEAAAABAAAAAQAABAEAAAABAAAUQMzAAAEAAAjBAAAQQQAAEwEAAAADC8AKgQAAGcNVQAAAAAAAAAKABQAHgD+ASABXgJOApADEAO0BFIE2gT8Bf4GZgfuCFQIlAmKChoKuguwDBoNEA2QDoYO2A8qEEQRXhJ4E5IUPhTeFs4XxBguGJgY5hk6GgwcKB8sIFIiTCRkJXol2iYYJmYnJidaKDAobCimKcoqPiqiK64sAixILHQs6Cz+LR4tOC2SLmIvEi+kMGAwtDEQMmw0mjTmNjI2VjaANxg3LjesOD44WjjwORQ58jq0O2o8DjxwPNg9bj4OQDhAaEESAAAAAQAAAGICSwASAAAAAAACAAAAAAAAAAAAAAAAAAAAAAAAAA4ArgABAAAAAAABAAsAAAABAAAAAAACAAcAhAABAAAAAAADAAsAQgABAAAAAAAEAAsAmQABAAAAAAAFAAsAIQABAAAAAAAGAAsAYwABAAAAAAAKABoAugADAAEECQABABYACwADAAEECQACAA4AiwADAAEECQADABYATQADAAEECQAEABYApAADAAEECQAFABYALAADAAEECQAGABYAbgADAAEECQAKADQA1HphaWtvLWljb25zAHoAYQBpAGsAbwAtAGkAYwBvAG4Ac1ZlcnNpb24gMS4wAFYAZQByAHMAaQBvAG4AIAAxAC4AMHphaWtvLWljb25zAHoAYQBpAGsAbwAtAGkAYwBvAG4Ac3phaWtvLWljb25zAHoAYQBpAGsAbwAtAGkAYwBvAG4Ac1JlZ3VsYXIAUgBlAGcAdQBsAGEAcnphaWtvLWljb25zAHoAYQBpAGsAbwAtAGkAYwBvAG4Ac0ZvbnQgZ2VuZXJhdGVkIGJ5IEljb01vb24uAEYAbwBuAHQAIABnAGUAbgBlAHIAYQB0AGUAZAAgAGIAeQAgAEkAYwBvAE0AbwBvAG4ALgAAAAMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=) format('woff'),
	/*savepage-url=fonts/zaiko-icons.svg?v233xx#zaiko-icons*/ url() format('svg');
	font-weight: normal;
	font-style: normal;
}

[class^="icon-"], [class*=" icon-"] {
	/* use !important to prevent issues with browser extensions that change fonts */
	font-family: 'zaiko-icons' !important;
	speak: none;
	font-style: normal;
	font-weight: normal;
	font-variant: normal;
	text-transform: none;
	line-height: 1;

	/* Better Font Rendering =========== */
	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;
}

.icon-zaiko_square:before {
	content: "\e961";
}
.icon-zaiko_square_outline:before {
	content: "\e962";
}
.icon-zaiko_square_cut:before {
	content: "\e963";
}
.icon-zaiko_wide:before {
	content: "\e964";
}
.icon-iFLYER2_shape:before {
	content: "\e96b";
}
.icon-iFLYER2_wide:before {
	content: "\e96c";
}
.icon-chanceflurries:before {
	content: "\e90f";
}
.icon-chancerain:before {
	content: "\e910";
}
.icon-chancesleet:before {
	content: "\e911";
}
.icon-chancesnow:before {
	content: "\e912";
}
.icon-chancetstorms:before {
	content: "\e913";
}
.icon-clear:before {
	content: "\e914";
}
.icon-cloudy:before {
	content: "\e915";
}
.icon-flurries:before {
	content: "\e916";
}
.icon-fog:before {
	content: "\e917";
}
.icon-hazy:before {
	content: "\e918";
}
.icon-mostlycloudy:before {
	content: "\e919";
}
.icon-mostlysunny:before {
	content: "\e91a";
}
.icon-partlycloudy:before {
	content: "\e91b";
}
.icon-partlysunny:before {
	content: "\e91c";
}
.icon-rain:before {
	content: "\e91d";
}
.icon-sleet:before {
	content: "\e91e";
}
.icon-snow:before {
	content: "\e91f";
}
.icon-sunny:before {
	content: "\e920";
}
.icon-tstorms:before {
	content: "\e921";
}
.icon-unknown:before {
	content: "\e922";
}
.icon-on:before {
	content: "\e923";
}
.icon-off:before {
	content: "\e924";
}
.icon-info2:before {
	content: "\e925";
}
.icon-send:before {
	content: "\e94a";
}
.icon-lock:before {
	content: "\e94b";
}
.icon-rock_star5:before {
	content: "\e926";
}
.icon-rock_star4:before {
	content: "\e927";
}
.icon-rock_star3:before {
	content: "\e928";
}
.icon-rock_star2:before {
	content: "\e929";
}
.icon-rock_star1:before {
	content: "\e92a";
}
.icon-guitar:before {
	content: "\e94c";
}
.icon-rocknroll:before {
	content: "\e94d";
}
.icon-calculate:before {
	content: "\e92b";
}
.icon-play:before {
	content: "\e92c";
}
.icon-cross:before {
	content: "\e92d";
}
.icon-spinleft:before {
	content: "\e92e";
}
.icon-spotify:before {
	content: "\e92f";
}
.icon-facebook:before {
	content: "\e930";
}
.icon-scan:before {
	content: "\e931";
}
.icon-scrolldown:before {
	content: "\e932";
}
.icon-scrollup:before {
	content: "\e933";
}
.icon-mission:before {
	content: "\e934";
}
.icon-store:before {
	content: "\e935";
}
.icon-chat:before {
	content: "\e936";
}
.icon-meet:before {
	content: "\e937";
}
.icon-news:before {
	content: "\e938";
}
.icon-artists:before {
	content: "\e900";
}
.icon-close:before {
	content: "\e939";
}
.icon-back:before {
	content: "\e901";
}
.icon-next:before {
	content: "\e909";
}
.icon-reload:before {
	content: "\e944";
}
.icon-directions:before {
	content: "\e902";
}
.icon-events:before {
	content: "\e903";
}
.icon-heart:before {
	content: "\e904";
}
.icon-info:before {
	content: "\e905";
}
.icon-account:before {
	content: "\e94e";
}
.icon-group:before {
	content: "\e94f";
}
.icon-invite:before {
	content: "\e906";
}
.icon-user:before {
	content: "\e95d";
}
.icon-location:before {
	content: "\e907";
}
.icon-more:before {
	content: "\e908";
}
.icon-notifications:before {
	content: "\e90a";
}
.icon-search:before {
	content: "\e90b";
}
.icon-settings:before {
	content: "\e90c";
}
.icon-share:before {
	content: "\e90d";
}
.icon-share2:before {
	content: "\e950";
}
.icon-open_up:before {
	content: "\e951";
}
.icon-tickets:before {
	content: "\e90e";
}
.icon-tick:before {
	content: "\e93f";
}
.icon-edit:before {
	content: "\e940";
}
.icon-checkbox_empty:before {
	content: "\e941";
}
.icon-checkbox_checked:before {
	content: "\e942";
}
.icon-checkbox_checked_solid:before {
	content: "\e943";
}
.icon-mine:before {
	content: "\e952";
}
.icon-ID_card:before {
	content: "\e945";
}
.icon-camera:before {
	content: "\e946";
}
.icon-calendar_add:before {
	content: "\e948";
}
.icon-calendar:before {
	content: "\e949";
}
.icon-back_arrow:before {
	content: "\e953";
}
.icon-zoom_out:before {
	content: "\e954";
}
.icon-zoom_in:before {
	content: "\e955";
}
.icon-alert:before {
	content: "\e956";
}
.icon-loading:before {
	content: "\e957";
}
.icon-menu:before {
	content: "\e958";
}
.icon-time:before {
	content: "\e959";
}
.icon-save_image:before {
	content: "\e95a";
}
.icon-news2:before {
	content: "\e95b";
}
.icon-timetable:before {
	content: "\e95c";
}
</style>
        <style data-savepage-href="https://use.fontawesome.com/releases/v5.2.0/css/all.css">/*!
 * Font Awesome Free 5.2.0 by @fontawesome - https://fontawesome.com
 * License - https://fontawesome.com/license (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License)
 */
 .fa,.fab,.fal,.far,.fas{-moz-osx-font-smoothing:grayscale;-webkit-font-smoothing:antialiased;display:inline-block;font-style:normal;font-variant:normal;text-rendering:auto;line-height:1}.fa-lg{font-size:1.33333em;line-height:.75em;vertical-align:-.0667em}.fa-xs{font-size:.75em}.fa-sm{font-size:.875em}.fa-1x{font-size:1em}.fa-2x{font-size:2em}.fa-3x{font-size:3em}.fa-4x{font-size:4em}.fa-5x{font-size:5em}.fa-6x{font-size:6em}.fa-7x{font-size:7em}.fa-8x{font-size:8em}.fa-9x{font-size:9em}.fa-10x{font-size:10em}.fa-fw{text-align:center;width:1.25em}.fa-ul{list-style-type:none;margin-left:2.5em;padding-left:0}.fa-ul>li{position:relative}.fa-li{left:-2em;position:absolute;text-align:center;width:2em;line-height:inherit}.fa-border{border:.08em solid #eee;border-radius:.1em;padding:.2em .25em .15em}.fa-pull-left{float:left}.fa-pull-right{float:right}.fa.fa-pull-left,.fab.fa-pull-left,.fal.fa-pull-left,.far.fa-pull-left,.fas.fa-pull-left{margin-right:.3em}.fa.fa-pull-right,.fab.fa-pull-right,.fal.fa-pull-right,.far.fa-pull-right,.fas.fa-pull-right{margin-left:.3em}.fa-spin{animation:a 2s infinite linear}.fa-pulse{animation:a 1s infinite steps(8)}@keyframes a{0%{transform:rotate(0deg)}to{transform:rotate(1turn)}}.fa-rotate-90{-ms-filter:"progid:DXImageTransform.Microsoft.BasicImage(rotation=1)";transform:rotate(90deg)}.fa-rotate-180{-ms-filter:"progid:DXImageTransform.Microsoft.BasicImage(rotation=2)";transform:rotate(180deg)}.fa-rotate-270{-ms-filter:"progid:DXImageTransform.Microsoft.BasicImage(rotation=3)";transform:rotate(270deg)}.fa-flip-horizontal{-ms-filter:"progid:DXImageTransform.Microsoft.BasicImage(rotation=0, mirror=1)";transform:scaleX(-1)}.fa-flip-vertical{transform:scaleY(-1)}.fa-flip-horizontal.fa-flip-vertical,.fa-flip-vertical{-ms-filter:"progid:DXImageTransform.Microsoft.BasicImage(rotation=2, mirror=1)"}.fa-flip-horizontal.fa-flip-vertical{transform:scale(-1)}:root .fa-flip-horizontal,:root .fa-flip-vertical,:root .fa-rotate-90,:root .fa-rotate-180,:root .fa-rotate-270{-webkit-filter:none;filter:none}.fa-stack{display:inline-block;height:2em;line-height:2em;position:relative;vertical-align:middle;width:2em}.fa-stack-1x,.fa-stack-2x{left:0;position:absolute;text-align:center;width:100%}.fa-stack-1x{line-height:inherit}.fa-stack-2x{font-size:2em}.fa-inverse{color:#fff}.fa-500px:before{content:"\f26e"}.fa-accessible-icon:before{content:"\f368"}.fa-accusoft:before{content:"\f369"}.fa-address-book:before{content:"\f2b9"}.fa-address-card:before{content:"\f2bb"}.fa-adjust:before{content:"\f042"}.fa-adn:before{content:"\f170"}.fa-adversal:before{content:"\f36a"}.fa-affiliatetheme:before{content:"\f36b"}.fa-air-freshener:before{content:"\f5d0"}.fa-algolia:before{content:"\f36c"}.fa-align-center:before{content:"\f037"}.fa-align-justify:before{content:"\f039"}.fa-align-left:before{content:"\f036"}.fa-align-right:before{content:"\f038"}.fa-allergies:before{content:"\f461"}.fa-amazon:before{content:"\f270"}.fa-amazon-pay:before{content:"\f42c"}.fa-ambulance:before{content:"\f0f9"}.fa-american-sign-language-interpreting:before{content:"\f2a3"}.fa-amilia:before{content:"\f36d"}.fa-anchor:before{content:"\f13d"}.fa-android:before{content:"\f17b"}.fa-angellist:before{content:"\f209"}.fa-angle-double-down:before{content:"\f103"}.fa-angle-double-left:before{content:"\f100"}.fa-angle-double-right:before{content:"\f101"}.fa-angle-double-up:before{content:"\f102"}.fa-angle-down:before{content:"\f107"}.fa-angle-left:before{content:"\f104"}.fa-angle-right:before{content:"\f105"}.fa-angle-up:before{content:"\f106"}.fa-angry:before{content:"\f556"}.fa-angrycreative:before{content:"\f36e"}.fa-angular:before{content:"\f420"}.fa-app-store:before{content:"\f36f"}.fa-app-store-ios:before{content:"\f370"}.fa-apper:before{content:"\f371"}.fa-apple:before{content:"\f179"}.fa-apple-alt:before{content:"\f5d1"}.fa-apple-pay:before{content:"\f415"}.fa-archive:before{content:"\f187"}.fa-archway:before{content:"\f557"}.fa-arrow-alt-circle-down:before{content:"\f358"}.fa-arrow-alt-circle-left:before{content:"\f359"}.fa-arrow-alt-circle-right:before{content:"\f35a"}.fa-arrow-alt-circle-up:before{content:"\f35b"}.fa-arrow-circle-down:before{content:"\f0ab"}.fa-arrow-circle-left:before{content:"\f0a8"}.fa-arrow-circle-right:before{content:"\f0a9"}.fa-arrow-circle-up:before{content:"\f0aa"}.fa-arrow-down:before{content:"\f063"}.fa-arrow-left:before{content:"\f060"}.fa-arrow-right:before{content:"\f061"}.fa-arrow-up:before{content:"\f062"}.fa-arrows-alt:before{content:"\f0b2"}.fa-arrows-alt-h:before{content:"\f337"}.fa-arrows-alt-v:before{content:"\f338"}.fa-assistive-listening-systems:before{content:"\f2a2"}.fa-asterisk:before{content:"\f069"}.fa-asymmetrik:before{content:"\f372"}.fa-at:before{content:"\f1fa"}.fa-atlas:before{content:"\f558"}.fa-atom:before{content:"\f5d2"}.fa-audible:before{content:"\f373"}.fa-audio-description:before{content:"\f29e"}.fa-autoprefixer:before{content:"\f41c"}.fa-avianex:before{content:"\f374"}.fa-aviato:before{content:"\f421"}.fa-award:before{content:"\f559"}.fa-aws:before{content:"\f375"}.fa-backspace:before{content:"\f55a"}.fa-backward:before{content:"\f04a"}.fa-balance-scale:before{content:"\f24e"}.fa-ban:before{content:"\f05e"}.fa-band-aid:before{content:"\f462"}.fa-bandcamp:before{content:"\f2d5"}.fa-barcode:before{content:"\f02a"}.fa-bars:before{content:"\f0c9"}.fa-baseball-ball:before{content:"\f433"}.fa-basketball-ball:before{content:"\f434"}.fa-bath:before{content:"\f2cd"}.fa-battery-empty:before{content:"\f244"}.fa-battery-full:before{content:"\f240"}.fa-battery-half:before{content:"\f242"}.fa-battery-quarter:before{content:"\f243"}.fa-battery-three-quarters:before{content:"\f241"}.fa-bed:before{content:"\f236"}.fa-beer:before{content:"\f0fc"}.fa-behance:before{content:"\f1b4"}.fa-behance-square:before{content:"\f1b5"}.fa-bell:before{content:"\f0f3"}.fa-bell-slash:before{content:"\f1f6"}.fa-bezier-curve:before{content:"\f55b"}.fa-bicycle:before{content:"\f206"}.fa-bimobject:before{content:"\f378"}.fa-binoculars:before{content:"\f1e5"}.fa-birthday-cake:before{content:"\f1fd"}.fa-bitbucket:before{content:"\f171"}.fa-bitcoin:before{content:"\f379"}.fa-bity:before{content:"\f37a"}.fa-black-tie:before{content:"\f27e"}.fa-blackberry:before{content:"\f37b"}.fa-blender:before{content:"\f517"}.fa-blind:before{content:"\f29d"}.fa-blogger:before{content:"\f37c"}.fa-blogger-b:before{content:"\f37d"}.fa-bluetooth:before{content:"\f293"}.fa-bluetooth-b:before{content:"\f294"}.fa-bold:before{content:"\f032"}.fa-bolt:before{content:"\f0e7"}.fa-bomb:before{content:"\f1e2"}.fa-bone:before{content:"\f5d7"}.fa-bong:before{content:"\f55c"}.fa-book:before{content:"\f02d"}.fa-book-open:before{content:"\f518"}.fa-book-reader:before{content:"\f5da"}.fa-bookmark:before{content:"\f02e"}.fa-bowling-ball:before{content:"\f436"}.fa-box:before{content:"\f466"}.fa-box-open:before{content:"\f49e"}.fa-boxes:before{content:"\f468"}.fa-braille:before{content:"\f2a1"}.fa-brain:before{content:"\f5dc"}.fa-briefcase:before{content:"\f0b1"}.fa-briefcase-medical:before{content:"\f469"}.fa-broadcast-tower:before{content:"\f519"}.fa-broom:before{content:"\f51a"}.fa-brush:before{content:"\f55d"}.fa-btc:before{content:"\f15a"}.fa-bug:before{content:"\f188"}.fa-building:before{content:"\f1ad"}.fa-bullhorn:before{content:"\f0a1"}.fa-bullseye:before{content:"\f140"}.fa-burn:before{content:"\f46a"}.fa-buromobelexperte:before{content:"\f37f"}.fa-bus:before{content:"\f207"}.fa-bus-alt:before{content:"\f55e"}.fa-buysellads:before{content:"\f20d"}.fa-calculator:before{content:"\f1ec"}.fa-calendar:before{content:"\f133"}.fa-calendar-alt:before{content:"\f073"}.fa-calendar-check:before{content:"\f274"}.fa-calendar-minus:before{content:"\f272"}.fa-calendar-plus:before{content:"\f271"}.fa-calendar-times:before{content:"\f273"}.fa-camera:before{content:"\f030"}.fa-camera-retro:before{content:"\f083"}.fa-cannabis:before{content:"\f55f"}.fa-capsules:before{content:"\f46b"}.fa-car:before{content:"\f1b9"}.fa-car-alt:before{content:"\f5de"}.fa-car-battery:before{content:"\f5df"}.fa-car-crash:before{content:"\f5e1"}.fa-car-side:before{content:"\f5e4"}.fa-caret-down:before{content:"\f0d7"}.fa-caret-left:before{content:"\f0d9"}.fa-caret-right:before{content:"\f0da"}.fa-caret-square-down:before{content:"\f150"}.fa-caret-square-left:before{content:"\f191"}.fa-caret-square-right:before{content:"\f152"}.fa-caret-square-up:before{content:"\f151"}.fa-caret-up:before{content:"\f0d8"}.fa-cart-arrow-down:before{content:"\f218"}.fa-cart-plus:before{content:"\f217"}.fa-cc-amazon-pay:before{content:"\f42d"}.fa-cc-amex:before{content:"\f1f3"}.fa-cc-apple-pay:before{content:"\f416"}.fa-cc-diners-club:before{content:"\f24c"}.fa-cc-discover:before{content:"\f1f2"}.fa-cc-jcb:before{content:"\f24b"}.fa-cc-mastercard:before{content:"\f1f1"}.fa-cc-paypal:before{content:"\f1f4"}.fa-cc-stripe:before{content:"\f1f5"}.fa-cc-visa:before{content:"\f1f0"}.fa-centercode:before{content:"\f380"}.fa-certificate:before{content:"\f0a3"}.fa-chalkboard:before{content:"\f51b"}.fa-chalkboard-teacher:before{content:"\f51c"}.fa-charging-station:before{content:"\f5e7"}.fa-chart-area:before{content:"\f1fe"}.fa-chart-bar:before{content:"\f080"}.fa-chart-line:before{content:"\f201"}.fa-chart-pie:before{content:"\f200"}.fa-check:before{content:"\f00c"}.fa-check-circle:before{content:"\f058"}.fa-check-double:before{content:"\f560"}.fa-check-square:before{content:"\f14a"}.fa-chess:before{content:"\f439"}.fa-chess-bishop:before{content:"\f43a"}.fa-chess-board:before{content:"\f43c"}.fa-chess-king:before{content:"\f43f"}.fa-chess-knight:before{content:"\f441"}.fa-chess-pawn:before{content:"\f443"}.fa-chess-queen:before{content:"\f445"}.fa-chess-rook:before{content:"\f447"}.fa-chevron-circle-down:before{content:"\f13a"}.fa-chevron-circle-left:before{content:"\f137"}.fa-chevron-circle-right:before{content:"\f138"}.fa-chevron-circle-up:before{content:"\f139"}.fa-chevron-down:before{content:"\f078"}.fa-chevron-left:before{content:"\f053"}.fa-chevron-right:before{content:"\f054"}.fa-chevron-up:before{content:"\f077"}.fa-child:before{content:"\f1ae"}.fa-chrome:before{content:"\f268"}.fa-church:before{content:"\f51d"}.fa-circle:before{content:"\f111"}.fa-circle-notch:before{content:"\f1ce"}.fa-clipboard:before{content:"\f328"}.fa-clipboard-check:before{content:"\f46c"}.fa-clipboard-list:before{content:"\f46d"}.fa-clock:before{content:"\f017"}.fa-clone:before{content:"\f24d"}.fa-closed-captioning:before{content:"\f20a"}.fa-cloud:before{content:"\f0c2"}.fa-cloud-download-alt:before{content:"\f381"}.fa-cloud-upload-alt:before{content:"\f382"}.fa-cloudscale:before{content:"\f383"}.fa-cloudsmith:before{content:"\f384"}.fa-cloudversify:before{content:"\f385"}.fa-cocktail:before{content:"\f561"}.fa-code:before{content:"\f121"}.fa-code-branch:before{content:"\f126"}.fa-codepen:before{content:"\f1cb"}.fa-codiepie:before{content:"\f284"}.fa-coffee:before{content:"\f0f4"}.fa-cog:before{content:"\f013"}.fa-cogs:before{content:"\f085"}.fa-coins:before{content:"\f51e"}.fa-columns:before{content:"\f0db"}.fa-comment:before{content:"\f075"}.fa-comment-alt:before{content:"\f27a"}.fa-comment-dots:before{content:"\f4ad"}.fa-comment-slash:before{content:"\f4b3"}.fa-comments:before{content:"\f086"}.fa-compact-disc:before{content:"\f51f"}.fa-compass:before{content:"\f14e"}.fa-compress:before{content:"\f066"}.fa-concierge-bell:before{content:"\f562"}.fa-connectdevelop:before{content:"\f20e"}.fa-contao:before{content:"\f26d"}.fa-cookie:before{content:"\f563"}.fa-cookie-bite:before{content:"\f564"}.fa-copy:before{content:"\f0c5"}.fa-copyright:before{content:"\f1f9"}.fa-couch:before{content:"\f4b8"}.fa-cpanel:before{content:"\f388"}.fa-creative-commons:before{content:"\f25e"}.fa-creative-commons-by:before{content:"\f4e7"}.fa-creative-commons-nc:before{content:"\f4e8"}.fa-creative-commons-nc-eu:before{content:"\f4e9"}.fa-creative-commons-nc-jp:before{content:"\f4ea"}.fa-creative-commons-nd:before{content:"\f4eb"}.fa-creative-commons-pd:before{content:"\f4ec"}.fa-creative-commons-pd-alt:before{content:"\f4ed"}.fa-creative-commons-remix:before{content:"\f4ee"}.fa-creative-commons-sa:before{content:"\f4ef"}.fa-creative-commons-sampling:before{content:"\f4f0"}.fa-creative-commons-sampling-plus:before{content:"\f4f1"}.fa-creative-commons-share:before{content:"\f4f2"}.fa-credit-card:before{content:"\f09d"}.fa-crop:before{content:"\f125"}.fa-crop-alt:before{content:"\f565"}.fa-crosshairs:before{content:"\f05b"}.fa-crow:before{content:"\f520"}.fa-crown:before{content:"\f521"}.fa-css3:before{content:"\f13c"}.fa-css3-alt:before{content:"\f38b"}.fa-cube:before{content:"\f1b2"}.fa-cubes:before{content:"\f1b3"}.fa-cut:before{content:"\f0c4"}.fa-cuttlefish:before{content:"\f38c"}.fa-d-and-d:before{content:"\f38d"}.fa-dashcube:before{content:"\f210"}.fa-database:before{content:"\f1c0"}.fa-deaf:before{content:"\f2a4"}.fa-delicious:before{content:"\f1a5"}.fa-deploydog:before{content:"\f38e"}.fa-deskpro:before{content:"\f38f"}.fa-desktop:before{content:"\f108"}.fa-deviantart:before{content:"\f1bd"}.fa-diagnoses:before{content:"\f470"}.fa-dice:before{content:"\f522"}.fa-dice-five:before{content:"\f523"}.fa-dice-four:before{content:"\f524"}.fa-dice-one:before{content:"\f525"}.fa-dice-six:before{content:"\f526"}.fa-dice-three:before{content:"\f527"}.fa-dice-two:before{content:"\f528"}.fa-digg:before{content:"\f1a6"}.fa-digital-ocean:before{content:"\f391"}.fa-digital-tachograph:before{content:"\f566"}.fa-directions:before{content:"\f5eb"}.fa-discord:before{content:"\f392"}.fa-discourse:before{content:"\f393"}.fa-divide:before{content:"\f529"}.fa-dizzy:before{content:"\f567"}.fa-dna:before{content:"\f471"}.fa-dochub:before{content:"\f394"}.fa-docker:before{content:"\f395"}.fa-dollar-sign:before{content:"\f155"}.fa-dolly:before{content:"\f472"}.fa-dolly-flatbed:before{content:"\f474"}.fa-donate:before{content:"\f4b9"}.fa-door-closed:before{content:"\f52a"}.fa-door-open:before{content:"\f52b"}.fa-dot-circle:before{content:"\f192"}.fa-dove:before{content:"\f4ba"}.fa-download:before{content:"\f019"}.fa-draft2digital:before{content:"\f396"}.fa-drafting-compass:before{content:"\f568"}.fa-draw-polygon:before{content:"\f5ee"}.fa-dribbble:before{content:"\f17d"}.fa-dribbble-square:before{content:"\f397"}.fa-dropbox:before{content:"\f16b"}.fa-drum:before{content:"\f569"}.fa-drum-steelpan:before{content:"\f56a"}.fa-drupal:before{content:"\f1a9"}.fa-dumbbell:before{content:"\f44b"}.fa-dyalog:before{content:"\f399"}.fa-earlybirds:before{content:"\f39a"}.fa-ebay:before{content:"\f4f4"}.fa-edge:before{content:"\f282"}.fa-edit:before{content:"\f044"}.fa-eject:before{content:"\f052"}.fa-elementor:before{content:"\f430"}.fa-ellipsis-h:before{content:"\f141"}.fa-ellipsis-v:before{content:"\f142"}.fa-ello:before{content:"\f5f1"}.fa-ember:before{content:"\f423"}.fa-empire:before{content:"\f1d1"}.fa-envelope:before{content:"\f0e0"}.fa-envelope-open:before{content:"\f2b6"}.fa-envelope-square:before{content:"\f199"}.fa-envira:before{content:"\f299"}.fa-equals:before{content:"\f52c"}.fa-eraser:before{content:"\f12d"}.fa-erlang:before{content:"\f39d"}.fa-ethereum:before{content:"\f42e"}.fa-etsy:before{content:"\f2d7"}.fa-euro-sign:before{content:"\f153"}.fa-exchange-alt:before{content:"\f362"}.fa-exclamation:before{content:"\f12a"}.fa-exclamation-circle:before{content:"\f06a"}.fa-exclamation-triangle:before{content:"\f071"}.fa-expand:before{content:"\f065"}.fa-expand-arrows-alt:before{content:"\f31e"}.fa-expeditedssl:before{content:"\f23e"}.fa-external-link-alt:before{content:"\f35d"}.fa-external-link-square-alt:before{content:"\f360"}.fa-eye:before{content:"\f06e"}.fa-eye-dropper:before{content:"\f1fb"}.fa-eye-slash:before{content:"\f070"}.fa-facebook:before{content:"\f09a"}.fa-facebook-f:before{content:"\f39e"}.fa-facebook-messenger:before{content:"\f39f"}.fa-facebook-square:before{content:"\f082"}.fa-fast-backward:before{content:"\f049"}.fa-fast-forward:before{content:"\f050"}.fa-fax:before{content:"\f1ac"}.fa-feather:before{content:"\f52d"}.fa-feather-alt:before{content:"\f56b"}.fa-female:before{content:"\f182"}.fa-fighter-jet:before{content:"\f0fb"}.fa-file:before{content:"\f15b"}.fa-file-alt:before{content:"\f15c"}.fa-file-archive:before{content:"\f1c6"}.fa-file-audio:before{content:"\f1c7"}.fa-file-code:before{content:"\f1c9"}.fa-file-contract:before{content:"\f56c"}.fa-file-download:before{content:"\f56d"}.fa-file-excel:before{content:"\f1c3"}.fa-file-export:before{content:"\f56e"}.fa-file-image:before{content:"\f1c5"}.fa-file-import:before{content:"\f56f"}.fa-file-invoice:before{content:"\f570"}.fa-file-invoice-dollar:before{content:"\f571"}.fa-file-medical:before{content:"\f477"}.fa-file-medical-alt:before{content:"\f478"}.fa-file-pdf:before{content:"\f1c1"}.fa-file-powerpoint:before{content:"\f1c4"}.fa-file-prescription:before{content:"\f572"}.fa-file-signature:before{content:"\f573"}.fa-file-upload:before{content:"\f574"}.fa-file-video:before{content:"\f1c8"}.fa-file-word:before{content:"\f1c2"}.fa-fill:before{content:"\f575"}.fa-fill-drip:before{content:"\f576"}.fa-film:before{content:"\f008"}.fa-filter:before{content:"\f0b0"}.fa-fingerprint:before{content:"\f577"}.fa-fire:before{content:"\f06d"}.fa-fire-extinguisher:before{content:"\f134"}.fa-firefox:before{content:"\f269"}.fa-first-aid:before{content:"\f479"}.fa-first-order:before{content:"\f2b0"}.fa-first-order-alt:before{content:"\f50a"}.fa-firstdraft:before{content:"\f3a1"}.fa-fish:before{content:"\f578"}.fa-flag:before{content:"\f024"}.fa-flag-checkered:before{content:"\f11e"}.fa-flask:before{content:"\f0c3"}.fa-flickr:before{content:"\f16e"}.fa-flipboard:before{content:"\f44d"}.fa-flushed:before{content:"\f579"}.fa-fly:before{content:"\f417"}.fa-folder:before{content:"\f07b"}.fa-folder-open:before{content:"\f07c"}.fa-font:before{content:"\f031"}.fa-font-awesome:before{content:"\f2b4"}.fa-font-awesome-alt:before{content:"\f35c"}.fa-font-awesome-flag:before{content:"\f425"}.fa-font-awesome-logo-full:before{content:"\f4e6"}.fa-fonticons:before{content:"\f280"}.fa-fonticons-fi:before{content:"\f3a2"}.fa-football-ball:before{content:"\f44e"}.fa-fort-awesome:before{content:"\f286"}.fa-fort-awesome-alt:before{content:"\f3a3"}.fa-forumbee:before{content:"\f211"}.fa-forward:before{content:"\f04e"}.fa-foursquare:before{content:"\f180"}.fa-free-code-camp:before{content:"\f2c5"}.fa-freebsd:before{content:"\f3a4"}.fa-frog:before{content:"\f52e"}.fa-frown:before{content:"\f119"}.fa-frown-open:before{content:"\f57a"}.fa-fulcrum:before{content:"\f50b"}.fa-futbol:before{content:"\f1e3"}.fa-galactic-republic:before{content:"\f50c"}.fa-galactic-senate:before{content:"\f50d"}.fa-gamepad:before{content:"\f11b"}.fa-gas-pump:before{content:"\f52f"}.fa-gavel:before{content:"\f0e3"}.fa-gem:before{content:"\f3a5"}.fa-genderless:before{content:"\f22d"}.fa-get-pocket:before{content:"\f265"}.fa-gg:before{content:"\f260"}.fa-gg-circle:before{content:"\f261"}.fa-gift:before{content:"\f06b"}.fa-git:before{content:"\f1d3"}.fa-git-square:before{content:"\f1d2"}.fa-github:before{content:"\f09b"}.fa-github-alt:before{content:"\f113"}.fa-github-square:before{content:"\f092"}.fa-gitkraken:before{content:"\f3a6"}.fa-gitlab:before{content:"\f296"}.fa-gitter:before{content:"\f426"}.fa-glass-martini:before{content:"\f000"}.fa-glass-martini-alt:before{content:"\f57b"}.fa-glasses:before{content:"\f530"}.fa-glide:before{content:"\f2a5"}.fa-glide-g:before{content:"\f2a6"}.fa-globe:before{content:"\f0ac"}.fa-globe-africa:before{content:"\f57c"}.fa-globe-americas:before{content:"\f57d"}.fa-globe-asia:before{content:"\f57e"}.fa-gofore:before{content:"\f3a7"}.fa-golf-ball:before{content:"\f450"}.fa-goodreads:before{content:"\f3a8"}.fa-goodreads-g:before{content:"\f3a9"}.fa-google:before{content:"\f1a0"}.fa-google-drive:before{content:"\f3aa"}.fa-google-play:before{content:"\f3ab"}.fa-google-plus:before{content:"\f2b3"}.fa-google-plus-g:before{content:"\f0d5"}.fa-google-plus-square:before{content:"\f0d4"}.fa-google-wallet:before{content:"\f1ee"}.fa-graduation-cap:before{content:"\f19d"}.fa-gratipay:before{content:"\f184"}.fa-grav:before{content:"\f2d6"}.fa-greater-than:before{content:"\f531"}.fa-greater-than-equal:before{content:"\f532"}.fa-grimace:before{content:"\f57f"}.fa-grin:before{content:"\f580"}.fa-grin-alt:before{content:"\f581"}.fa-grin-beam:before{content:"\f582"}.fa-grin-beam-sweat:before{content:"\f583"}.fa-grin-hearts:before{content:"\f584"}.fa-grin-squint:before{content:"\f585"}.fa-grin-squint-tears:before{content:"\f586"}.fa-grin-stars:before{content:"\f587"}.fa-grin-tears:before{content:"\f588"}.fa-grin-tongue:before{content:"\f589"}.fa-grin-tongue-squint:before{content:"\f58a"}.fa-grin-tongue-wink:before{content:"\f58b"}.fa-grin-wink:before{content:"\f58c"}.fa-grip-horizontal:before{content:"\f58d"}.fa-grip-vertical:before{content:"\f58e"}.fa-gripfire:before{content:"\f3ac"}.fa-grunt:before{content:"\f3ad"}.fa-gulp:before{content:"\f3ae"}.fa-h-square:before{content:"\f0fd"}.fa-hacker-news:before{content:"\f1d4"}.fa-hacker-news-square:before{content:"\f3af"}.fa-hackerrank:before{content:"\f5f7"}.fa-hand-holding:before{content:"\f4bd"}.fa-hand-holding-heart:before{content:"\f4be"}.fa-hand-holding-usd:before{content:"\f4c0"}.fa-hand-lizard:before{content:"\f258"}.fa-hand-paper:before{content:"\f256"}.fa-hand-peace:before{content:"\f25b"}.fa-hand-point-down:before{content:"\f0a7"}.fa-hand-point-left:before{content:"\f0a5"}.fa-hand-point-right:before{content:"\f0a4"}.fa-hand-point-up:before{content:"\f0a6"}.fa-hand-pointer:before{content:"\f25a"}.fa-hand-rock:before{content:"\f255"}.fa-hand-scissors:before{content:"\f257"}.fa-hand-spock:before{content:"\f259"}.fa-hands:before{content:"\f4c2"}.fa-hands-helping:before{content:"\f4c4"}.fa-handshake:before{content:"\f2b5"}.fa-hashtag:before{content:"\f292"}.fa-hdd:before{content:"\f0a0"}.fa-heading:before{content:"\f1dc"}.fa-headphones:before{content:"\f025"}.fa-headphones-alt:before{content:"\f58f"}.fa-headset:before{content:"\f590"}.fa-heart:before{content:"\f004"}.fa-heartbeat:before{content:"\f21e"}.fa-helicopter:before{content:"\f533"}.fa-highlighter:before{content:"\f591"}.fa-hips:before{content:"\f452"}.fa-hire-a-helper:before{content:"\f3b0"}.fa-history:before{content:"\f1da"}.fa-hockey-puck:before{content:"\f453"}.fa-home:before{content:"\f015"}.fa-hooli:before{content:"\f427"}.fa-hornbill:before{content:"\f592"}.fa-hospital:before{content:"\f0f8"}.fa-hospital-alt:before{content:"\f47d"}.fa-hospital-symbol:before{content:"\f47e"}.fa-hot-tub:before{content:"\f593"}.fa-hotel:before{content:"\f594"}.fa-hotjar:before{content:"\f3b1"}.fa-hourglass:before{content:"\f254"}.fa-hourglass-end:before{content:"\f253"}.fa-hourglass-half:before{content:"\f252"}.fa-hourglass-start:before{content:"\f251"}.fa-houzz:before{content:"\f27c"}.fa-html5:before{content:"\f13b"}.fa-hubspot:before{content:"\f3b2"}.fa-i-cursor:before{content:"\f246"}.fa-id-badge:before{content:"\f2c1"}.fa-id-card:before{content:"\f2c2"}.fa-id-card-alt:before{content:"\f47f"}.fa-image:before{content:"\f03e"}.fa-images:before{content:"\f302"}.fa-imdb:before{content:"\f2d8"}.fa-inbox:before{content:"\f01c"}.fa-indent:before{content:"\f03c"}.fa-industry:before{content:"\f275"}.fa-infinity:before{content:"\f534"}.fa-info:before{content:"\f129"}.fa-info-circle:before{content:"\f05a"}.fa-instagram:before{content:"\f16d"}.fa-internet-explorer:before{content:"\f26b"}.fa-ioxhost:before{content:"\f208"}.fa-italic:before{content:"\f033"}.fa-itunes:before{content:"\f3b4"}.fa-itunes-note:before{content:"\f3b5"}.fa-java:before{content:"\f4e4"}.fa-jedi-order:before{content:"\f50e"}.fa-jenkins:before{content:"\f3b6"}.fa-joget:before{content:"\f3b7"}.fa-joint:before{content:"\f595"}.fa-joomla:before{content:"\f1aa"}.fa-js:before{content:"\f3b8"}.fa-js-square:before{content:"\f3b9"}.fa-jsfiddle:before{content:"\f1cc"}.fa-kaggle:before{content:"\f5fa"}.fa-key:before{content:"\f084"}.fa-keybase:before{content:"\f4f5"}.fa-keyboard:before{content:"\f11c"}.fa-keycdn:before{content:"\f3ba"}.fa-kickstarter:before{content:"\f3bb"}.fa-kickstarter-k:before{content:"\f3bc"}.fa-kiss:before{content:"\f596"}.fa-kiss-beam:before{content:"\f597"}.fa-kiss-wink-heart:before{content:"\f598"}.fa-kiwi-bird:before{content:"\f535"}.fa-korvue:before{content:"\f42f"}.fa-language:before{content:"\f1ab"}.fa-laptop:before{content:"\f109"}.fa-laptop-code:before{content:"\f5fc"}.fa-laravel:before{content:"\f3bd"}.fa-lastfm:before{content:"\f202"}.fa-lastfm-square:before{content:"\f203"}.fa-laugh:before{content:"\f599"}.fa-laugh-beam:before{content:"\f59a"}.fa-laugh-squint:before{content:"\f59b"}.fa-laugh-wink:before{content:"\f59c"}.fa-layer-group:before{content:"\f5fd"}.fa-leaf:before{content:"\f06c"}.fa-leanpub:before{content:"\f212"}.fa-lemon:before{content:"\f094"}.fa-less:before{content:"\f41d"}.fa-less-than:before{content:"\f536"}.fa-less-than-equal:before{content:"\f537"}.fa-level-down-alt:before{content:"\f3be"}.fa-level-up-alt:before{content:"\f3bf"}.fa-life-ring:before{content:"\f1cd"}.fa-lightbulb:before{content:"\f0eb"}.fa-line:before{content:"\f3c0"}.fa-link:before{content:"\f0c1"}.fa-linkedin:before{content:"\f08c"}.fa-linkedin-in:before{content:"\f0e1"}.fa-linode:before{content:"\f2b8"}.fa-linux:before{content:"\f17c"}.fa-lira-sign:before{content:"\f195"}.fa-list:before{content:"\f03a"}.fa-list-alt:before{content:"\f022"}.fa-list-ol:before{content:"\f0cb"}.fa-list-ul:before{content:"\f0ca"}.fa-location-arrow:before{content:"\f124"}.fa-lock:before{content:"\f023"}.fa-lock-open:before{content:"\f3c1"}.fa-long-arrow-alt-down:before{content:"\f309"}.fa-long-arrow-alt-left:before{content:"\f30a"}.fa-long-arrow-alt-right:before{content:"\f30b"}.fa-long-arrow-alt-up:before{content:"\f30c"}.fa-low-vision:before{content:"\f2a8"}.fa-luggage-cart:before{content:"\f59d"}.fa-lyft:before{content:"\f3c3"}.fa-magento:before{content:"\f3c4"}.fa-magic:before{content:"\f0d0"}.fa-magnet:before{content:"\f076"}.fa-mailchimp:before{content:"\f59e"}.fa-male:before{content:"\f183"}.fa-mandalorian:before{content:"\f50f"}.fa-map:before{content:"\f279"}.fa-map-marked:before{content:"\f59f"}.fa-map-marked-alt:before{content:"\f5a0"}.fa-map-marker:before{content:"\f041"}.fa-map-marker-alt:before{content:"\f3c5"}.fa-map-pin:before{content:"\f276"}.fa-map-signs:before{content:"\f277"}.fa-markdown:before{content:"\f60f"}.fa-marker:before{content:"\f5a1"}.fa-mars:before{content:"\f222"}.fa-mars-double:before{content:"\f227"}.fa-mars-stroke:before{content:"\f229"}.fa-mars-stroke-h:before{content:"\f22b"}.fa-mars-stroke-v:before{content:"\f22a"}.fa-mastodon:before{content:"\f4f6"}.fa-maxcdn:before{content:"\f136"}.fa-medal:before{content:"\f5a2"}.fa-medapps:before{content:"\f3c6"}.fa-medium:before{content:"\f23a"}.fa-medium-m:before{content:"\f3c7"}.fa-medkit:before{content:"\f0fa"}.fa-medrt:before{content:"\f3c8"}.fa-meetup:before{content:"\f2e0"}.fa-megaport:before{content:"\f5a3"}.fa-meh:before{content:"\f11a"}.fa-meh-blank:before{content:"\f5a4"}.fa-meh-rolling-eyes:before{content:"\f5a5"}.fa-memory:before{content:"\f538"}.fa-mercury:before{content:"\f223"}.fa-microchip:before{content:"\f2db"}.fa-microphone:before{content:"\f130"}.fa-microphone-alt:before{content:"\f3c9"}.fa-microphone-alt-slash:before{content:"\f539"}.fa-microphone-slash:before{content:"\f131"}.fa-microscope:before{content:"\f610"}.fa-microsoft:before{content:"\f3ca"}.fa-minus:before{content:"\f068"}.fa-minus-circle:before{content:"\f056"}.fa-minus-square:before{content:"\f146"}.fa-mix:before{content:"\f3cb"}.fa-mixcloud:before{content:"\f289"}.fa-mizuni:before{content:"\f3cc"}.fa-mobile:before{content:"\f10b"}.fa-mobile-alt:before{content:"\f3cd"}.fa-modx:before{content:"\f285"}.fa-monero:before{content:"\f3d0"}.fa-money-bill:before{content:"\f0d6"}.fa-money-bill-alt:before{content:"\f3d1"}.fa-money-bill-wave:before{content:"\f53a"}.fa-money-bill-wave-alt:before{content:"\f53b"}.fa-money-check:before{content:"\f53c"}.fa-money-check-alt:before{content:"\f53d"}.fa-monument:before{content:"\f5a6"}.fa-moon:before{content:"\f186"}.fa-mortar-pestle:before{content:"\f5a7"}.fa-motorcycle:before{content:"\f21c"}.fa-mouse-pointer:before{content:"\f245"}.fa-music:before{content:"\f001"}.fa-napster:before{content:"\f3d2"}.fa-neos:before{content:"\f612"}.fa-neuter:before{content:"\f22c"}.fa-newspaper:before{content:"\f1ea"}.fa-nimblr:before{content:"\f5a8"}.fa-nintendo-switch:before{content:"\f418"}.fa-node:before{content:"\f419"}.fa-node-js:before{content:"\f3d3"}.fa-not-equal:before{content:"\f53e"}.fa-notes-medical:before{content:"\f481"}.fa-npm:before{content:"\f3d4"}.fa-ns8:before{content:"\f3d5"}.fa-nutritionix:before{content:"\f3d6"}.fa-object-group:before{content:"\f247"}.fa-object-ungroup:before{content:"\f248"}.fa-odnoklassniki:before{content:"\f263"}.fa-odnoklassniki-square:before{content:"\f264"}.fa-oil-can:before{content:"\f613"}.fa-old-republic:before{content:"\f510"}.fa-opencart:before{content:"\f23d"}.fa-openid:before{content:"\f19b"}.fa-opera:before{content:"\f26a"}.fa-optin-monster:before{content:"\f23c"}.fa-osi:before{content:"\f41a"}.fa-outdent:before{content:"\f03b"}.fa-page4:before{content:"\f3d7"}.fa-pagelines:before{content:"\f18c"}.fa-paint-brush:before{content:"\f1fc"}.fa-paint-roller:before{content:"\f5aa"}.fa-palette:before{content:"\f53f"}.fa-palfed:before{content:"\f3d8"}.fa-pallet:before{content:"\f482"}.fa-paper-plane:before{content:"\f1d8"}.fa-paperclip:before{content:"\f0c6"}.fa-parachute-box:before{content:"\f4cd"}.fa-paragraph:before{content:"\f1dd"}.fa-parking:before{content:"\f540"}.fa-passport:before{content:"\f5ab"}.fa-paste:before{content:"\f0ea"}.fa-patreon:before{content:"\f3d9"}.fa-pause:before{content:"\f04c"}.fa-pause-circle:before{content:"\f28b"}.fa-paw:before{content:"\f1b0"}.fa-paypal:before{content:"\f1ed"}.fa-pen:before{content:"\f304"}.fa-pen-alt:before{content:"\f305"}.fa-pen-fancy:before{content:"\f5ac"}.fa-pen-nib:before{content:"\f5ad"}.fa-pen-square:before{content:"\f14b"}.fa-pencil-alt:before{content:"\f303"}.fa-pencil-ruler:before{content:"\f5ae"}.fa-people-carry:before{content:"\f4ce"}.fa-percent:before{content:"\f295"}.fa-percentage:before{content:"\f541"}.fa-periscope:before{content:"\f3da"}.fa-phabricator:before{content:"\f3db"}.fa-phoenix-framework:before{content:"\f3dc"}.fa-phoenix-squadron:before{content:"\f511"}.fa-phone:before{content:"\f095"}.fa-phone-slash:before{content:"\f3dd"}.fa-phone-square:before{content:"\f098"}.fa-phone-volume:before{content:"\f2a0"}.fa-php:before{content:"\f457"}.fa-pied-piper:before{content:"\f2ae"}.fa-pied-piper-alt:before{content:"\f1a8"}.fa-pied-piper-hat:before{content:"\f4e5"}.fa-pied-piper-pp:before{content:"\f1a7"}.fa-piggy-bank:before{content:"\f4d3"}.fa-pills:before{content:"\f484"}.fa-pinterest:before{content:"\f0d2"}.fa-pinterest-p:before{content:"\f231"}.fa-pinterest-square:before{content:"\f0d3"}.fa-plane:before{content:"\f072"}.fa-plane-arrival:before{content:"\f5af"}.fa-plane-departure:before{content:"\f5b0"}.fa-play:before{content:"\f04b"}.fa-play-circle:before{content:"\f144"}.fa-playstation:before{content:"\f3df"}.fa-plug:before{content:"\f1e6"}.fa-plus:before{content:"\f067"}.fa-plus-circle:before{content:"\f055"}.fa-plus-square:before{content:"\f0fe"}.fa-podcast:before{content:"\f2ce"}.fa-poo:before{content:"\f2fe"}.fa-poop:before{content:"\f619"}.fa-portrait:before{content:"\f3e0"}.fa-pound-sign:before{content:"\f154"}.fa-power-off:before{content:"\f011"}.fa-prescription:before{content:"\f5b1"}.fa-prescription-bottle:before{content:"\f485"}.fa-prescription-bottle-alt:before{content:"\f486"}.fa-print:before{content:"\f02f"}.fa-procedures:before{content:"\f487"}.fa-product-hunt:before{content:"\f288"}.fa-project-diagram:before{content:"\f542"}.fa-pushed:before{content:"\f3e1"}.fa-puzzle-piece:before{content:"\f12e"}.fa-python:before{content:"\f3e2"}.fa-qq:before{content:"\f1d6"}.fa-qrcode:before{content:"\f029"}.fa-question:before{content:"\f128"}.fa-question-circle:before{content:"\f059"}.fa-quidditch:before{content:"\f458"}.fa-quinscape:before{content:"\f459"}.fa-quora:before{content:"\f2c4"}.fa-quote-left:before{content:"\f10d"}.fa-quote-right:before{content:"\f10e"}.fa-r-project:before{content:"\f4f7"}.fa-random:before{content:"\f074"}.fa-ravelry:before{content:"\f2d9"}.fa-react:before{content:"\f41b"}.fa-readme:before{content:"\f4d5"}.fa-rebel:before{content:"\f1d0"}.fa-receipt:before{content:"\f543"}.fa-recycle:before{content:"\f1b8"}.fa-red-river:before{content:"\f3e3"}.fa-reddit:before{content:"\f1a1"}.fa-reddit-alien:before{content:"\f281"}.fa-reddit-square:before{content:"\f1a2"}.fa-redo:before{content:"\f01e"}.fa-redo-alt:before{content:"\f2f9"}.fa-registered:before{content:"\f25d"}.fa-rendact:before{content:"\f3e4"}.fa-renren:before{content:"\f18b"}.fa-reply:before{content:"\f3e5"}.fa-reply-all:before{content:"\f122"}.fa-replyd:before{content:"\f3e6"}.fa-researchgate:before{content:"\f4f8"}.fa-resolving:before{content:"\f3e7"}.fa-retweet:before{content:"\f079"}.fa-rev:before{content:"\f5b2"}.fa-ribbon:before{content:"\f4d6"}.fa-road:before{content:"\f018"}.fa-robot:before{content:"\f544"}.fa-rocket:before{content:"\f135"}.fa-rocketchat:before{content:"\f3e8"}.fa-rockrms:before{content:"\f3e9"}.fa-route:before{content:"\f4d7"}.fa-rss:before{content:"\f09e"}.fa-rss-square:before{content:"\f143"}.fa-ruble-sign:before{content:"\f158"}.fa-ruler:before{content:"\f545"}.fa-ruler-combined:before{content:"\f546"}.fa-ruler-horizontal:before{content:"\f547"}.fa-ruler-vertical:before{content:"\f548"}.fa-rupee-sign:before{content:"\f156"}.fa-sad-cry:before{content:"\f5b3"}.fa-sad-tear:before{content:"\f5b4"}.fa-safari:before{content:"\f267"}.fa-sass:before{content:"\f41e"}.fa-save:before{content:"\f0c7"}.fa-schlix:before{content:"\f3ea"}.fa-school:before{content:"\f549"}.fa-screwdriver:before{content:"\f54a"}.fa-scribd:before{content:"\f28a"}.fa-search:before{content:"\f002"}.fa-search-minus:before{content:"\f010"}.fa-search-plus:before{content:"\f00e"}.fa-searchengin:before{content:"\f3eb"}.fa-seedling:before{content:"\f4d8"}.fa-sellcast:before{content:"\f2da"}.fa-sellsy:before{content:"\f213"}.fa-server:before{content:"\f233"}.fa-servicestack:before{content:"\f3ec"}.fa-shapes:before{content:"\f61f"}.fa-share:before{content:"\f064"}.fa-share-alt:before{content:"\f1e0"}.fa-share-alt-square:before{content:"\f1e1"}.fa-share-square:before{content:"\f14d"}.fa-shekel-sign:before{content:"\f20b"}.fa-shield-alt:before{content:"\f3ed"}.fa-ship:before{content:"\f21a"}.fa-shipping-fast:before{content:"\f48b"}.fa-shirtsinbulk:before{content:"\f214"}.fa-shoe-prints:before{content:"\f54b"}.fa-shopping-bag:before{content:"\f290"}.fa-shopping-basket:before{content:"\f291"}.fa-shopping-cart:before{content:"\f07a"}.fa-shopware:before{content:"\f5b5"}.fa-shower:before{content:"\f2cc"}.fa-shuttle-van:before{content:"\f5b6"}.fa-sign:before{content:"\f4d9"}.fa-sign-in-alt:before{content:"\f2f6"}.fa-sign-language:before{content:"\f2a7"}.fa-sign-out-alt:before{content:"\f2f5"}.fa-signal:before{content:"\f012"}.fa-signature:before{content:"\f5b7"}.fa-simplybuilt:before{content:"\f215"}.fa-sistrix:before{content:"\f3ee"}.fa-sitemap:before{content:"\f0e8"}.fa-sith:before{content:"\f512"}.fa-skull:before{content:"\f54c"}.fa-skyatlas:before{content:"\f216"}.fa-skype:before{content:"\f17e"}.fa-slack:before{content:"\f198"}.fa-slack-hash:before{content:"\f3ef"}.fa-sliders-h:before{content:"\f1de"}.fa-slideshare:before{content:"\f1e7"}.fa-smile:before{content:"\f118"}.fa-smile-beam:before{content:"\f5b8"}.fa-smile-wink:before{content:"\f4da"}.fa-smoking:before{content:"\f48d"}.fa-smoking-ban:before{content:"\f54d"}.fa-snapchat:before{content:"\f2ab"}.fa-snapchat-ghost:before{content:"\f2ac"}.fa-snapchat-square:before{content:"\f2ad"}.fa-snowflake:before{content:"\f2dc"}.fa-solar-panel:before{content:"\f5ba"}.fa-sort:before{content:"\f0dc"}.fa-sort-alpha-down:before{content:"\f15d"}.fa-sort-alpha-up:before{content:"\f15e"}.fa-sort-amount-down:before{content:"\f160"}.fa-sort-amount-up:before{content:"\f161"}.fa-sort-down:before{content:"\f0dd"}.fa-sort-numeric-down:before{content:"\f162"}.fa-sort-numeric-up:before{content:"\f163"}.fa-sort-up:before{content:"\f0de"}.fa-soundcloud:before{content:"\f1be"}.fa-spa:before{content:"\f5bb"}.fa-space-shuttle:before{content:"\f197"}.fa-speakap:before{content:"\f3f3"}.fa-spinner:before{content:"\f110"}.fa-splotch:before{content:"\f5bc"}.fa-spotify:before{content:"\f1bc"}.fa-spray-can:before{content:"\f5bd"}.fa-square:before{content:"\f0c8"}.fa-square-full:before{content:"\f45c"}.fa-squarespace:before{content:"\f5be"}.fa-stack-exchange:before{content:"\f18d"}.fa-stack-overflow:before{content:"\f16c"}.fa-stamp:before{content:"\f5bf"}.fa-star:before{content:"\f005"}.fa-star-half:before{content:"\f089"}.fa-star-half-alt:before{content:"\f5c0"}.fa-star-of-life:before{content:"\f621"}.fa-staylinked:before{content:"\f3f5"}.fa-steam:before{content:"\f1b6"}.fa-steam-square:before{content:"\f1b7"}.fa-steam-symbol:before{content:"\f3f6"}.fa-step-backward:before{content:"\f048"}.fa-step-forward:before{content:"\f051"}.fa-stethoscope:before{content:"\f0f1"}.fa-sticker-mule:before{content:"\f3f7"}.fa-sticky-note:before{content:"\f249"}.fa-stop:before{content:"\f04d"}.fa-stop-circle:before{content:"\f28d"}.fa-stopwatch:before{content:"\f2f2"}.fa-store:before{content:"\f54e"}.fa-store-alt:before{content:"\f54f"}.fa-strava:before{content:"\f428"}.fa-stream:before{content:"\f550"}.fa-street-view:before{content:"\f21d"}.fa-strikethrough:before{content:"\f0cc"}.fa-stripe:before{content:"\f429"}.fa-stripe-s:before{content:"\f42a"}.fa-stroopwafel:before{content:"\f551"}.fa-studiovinari:before{content:"\f3f8"}.fa-stumbleupon:before{content:"\f1a4"}.fa-stumbleupon-circle:before{content:"\f1a3"}.fa-subscript:before{content:"\f12c"}.fa-subway:before{content:"\f239"}.fa-suitcase:before{content:"\f0f2"}.fa-suitcase-rolling:before{content:"\f5c1"}.fa-sun:before{content:"\f185"}.fa-superpowers:before{content:"\f2dd"}.fa-superscript:before{content:"\f12b"}.fa-supple:before{content:"\f3f9"}.fa-surprise:before{content:"\f5c2"}.fa-swatchbook:before{content:"\f5c3"}.fa-swimmer:before{content:"\f5c4"}.fa-swimming-pool:before{content:"\f5c5"}.fa-sync:before{content:"\f021"}.fa-sync-alt:before{content:"\f2f1"}.fa-syringe:before{content:"\f48e"}.fa-table:before{content:"\f0ce"}.fa-table-tennis:before{content:"\f45d"}.fa-tablet:before{content:"\f10a"}.fa-tablet-alt:before{content:"\f3fa"}.fa-tablets:before{content:"\f490"}.fa-tachometer-alt:before{content:"\f3fd"}.fa-tag:before{content:"\f02b"}.fa-tags:before{content:"\f02c"}.fa-tape:before{content:"\f4db"}.fa-tasks:before{content:"\f0ae"}.fa-taxi:before{content:"\f1ba"}.fa-teamspeak:before{content:"\f4f9"}.fa-teeth:before{content:"\f62e"}.fa-teeth-open:before{content:"\f62f"}.fa-telegram:before{content:"\f2c6"}.fa-telegram-plane:before{content:"\f3fe"}.fa-tencent-weibo:before{content:"\f1d5"}.fa-terminal:before{content:"\f120"}.fa-text-height:before{content:"\f034"}.fa-text-width:before{content:"\f035"}.fa-th:before{content:"\f00a"}.fa-th-large:before{content:"\f009"}.fa-th-list:before{content:"\f00b"}.fa-theater-masks:before{content:"\f630"}.fa-themeco:before{content:"\f5c6"}.fa-themeisle:before{content:"\f2b2"}.fa-thermometer:before{content:"\f491"}.fa-thermometer-empty:before{content:"\f2cb"}.fa-thermometer-full:before{content:"\f2c7"}.fa-thermometer-half:before{content:"\f2c9"}.fa-thermometer-quarter:before{content:"\f2ca"}.fa-thermometer-three-quarters:before{content:"\f2c8"}.fa-thumbs-down:before{content:"\f165"}.fa-thumbs-up:before{content:"\f164"}.fa-thumbtack:before{content:"\f08d"}.fa-ticket-alt:before{content:"\f3ff"}.fa-times:before{content:"\f00d"}.fa-times-circle:before{content:"\f057"}.fa-tint:before{content:"\f043"}.fa-tint-slash:before{content:"\f5c7"}.fa-tired:before{content:"\f5c8"}.fa-toggle-off:before{content:"\f204"}.fa-toggle-on:before{content:"\f205"}.fa-toolbox:before{content:"\f552"}.fa-tooth:before{content:"\f5c9"}.fa-trade-federation:before{content:"\f513"}.fa-trademark:before{content:"\f25c"}.fa-traffic-light:before{content:"\f637"}.fa-train:before{content:"\f238"}.fa-transgender:before{content:"\f224"}.fa-transgender-alt:before{content:"\f225"}.fa-trash:before{content:"\f1f8"}.fa-trash-alt:before{content:"\f2ed"}.fa-tree:before{content:"\f1bb"}.fa-trello:before{content:"\f181"}.fa-tripadvisor:before{content:"\f262"}.fa-trophy:before{content:"\f091"}.fa-truck:before{content:"\f0d1"}.fa-truck-loading:before{content:"\f4de"}.fa-truck-monster:before{content:"\f63b"}.fa-truck-moving:before{content:"\f4df"}.fa-truck-pickup:before{content:"\f63c"}.fa-tshirt:before{content:"\f553"}.fa-tty:before{content:"\f1e4"}.fa-tumblr:before{content:"\f173"}.fa-tumblr-square:before{content:"\f174"}.fa-tv:before{content:"\f26c"}.fa-twitch:before{content:"\f1e8"}.fa-twitter:before{content:"\f099"}.fa-twitter-square:before{content:"\f081"}.fa-typo3:before{content:"\f42b"}.fa-uber:before{content:"\f402"}.fa-uikit:before{content:"\f403"}.fa-umbrella:before{content:"\f0e9"}.fa-umbrella-beach:before{content:"\f5ca"}.fa-underline:before{content:"\f0cd"}.fa-undo:before{content:"\f0e2"}.fa-undo-alt:before{content:"\f2ea"}.fa-uniregistry:before{content:"\f404"}.fa-universal-access:before{content:"\f29a"}.fa-university:before{content:"\f19c"}.fa-unlink:before{content:"\f127"}.fa-unlock:before{content:"\f09c"}.fa-unlock-alt:before{content:"\f13e"}.fa-untappd:before{content:"\f405"}.fa-upload:before{content:"\f093"}.fa-usb:before{content:"\f287"}.fa-user:before{content:"\f007"}.fa-user-alt:before{content:"\f406"}.fa-user-alt-slash:before{content:"\f4fa"}.fa-user-astronaut:before{content:"\f4fb"}.fa-user-check:before{content:"\f4fc"}.fa-user-circle:before{content:"\f2bd"}.fa-user-clock:before{content:"\f4fd"}.fa-user-cog:before{content:"\f4fe"}.fa-user-edit:before{content:"\f4ff"}.fa-user-friends:before{content:"\f500"}.fa-user-graduate:before{content:"\f501"}.fa-user-lock:before{content:"\f502"}.fa-user-md:before{content:"\f0f0"}.fa-user-minus:before{content:"\f503"}.fa-user-ninja:before{content:"\f504"}.fa-user-plus:before{content:"\f234"}.fa-user-secret:before{content:"\f21b"}.fa-user-shield:before{content:"\f505"}.fa-user-slash:before{content:"\f506"}.fa-user-tag:before{content:"\f507"}.fa-user-tie:before{content:"\f508"}.fa-user-times:before{content:"\f235"}.fa-users:before{content:"\f0c0"}.fa-users-cog:before{content:"\f509"}.fa-ussunnah:before{content:"\f407"}.fa-utensil-spoon:before{content:"\f2e5"}.fa-utensils:before{content:"\f2e7"}.fa-vaadin:before{content:"\f408"}.fa-vector-square:before{content:"\f5cb"}.fa-venus:before{content:"\f221"}.fa-venus-double:before{content:"\f226"}.fa-venus-mars:before{content:"\f228"}.fa-viacoin:before{content:"\f237"}.fa-viadeo:before{content:"\f2a9"}.fa-viadeo-square:before{content:"\f2aa"}.fa-vial:before{content:"\f492"}.fa-vials:before{content:"\f493"}.fa-viber:before{content:"\f409"}.fa-video:before{content:"\f03d"}.fa-video-slash:before{content:"\f4e2"}.fa-vimeo:before{content:"\f40a"}.fa-vimeo-square:before{content:"\f194"}.fa-vimeo-v:before{content:"\f27d"}.fa-vine:before{content:"\f1ca"}.fa-vk:before{content:"\f189"}.fa-vnv:before{content:"\f40b"}.fa-volleyball-ball:before{content:"\f45f"}.fa-volume-down:before{content:"\f027"}.fa-volume-off:before{content:"\f026"}.fa-volume-up:before{content:"\f028"}.fa-vuejs:before{content:"\f41f"}.fa-walking:before{content:"\f554"}.fa-wallet:before{content:"\f555"}.fa-warehouse:before{content:"\f494"}.fa-weebly:before{content:"\f5cc"}.fa-weibo:before{content:"\f18a"}.fa-weight:before{content:"\f496"}.fa-weight-hanging:before{content:"\f5cd"}.fa-weixin:before{content:"\f1d7"}.fa-whatsapp:before{content:"\f232"}.fa-whatsapp-square:before{content:"\f40c"}.fa-wheelchair:before{content:"\f193"}.fa-whmcs:before{content:"\f40d"}.fa-wifi:before{content:"\f1eb"}.fa-wikipedia-w:before{content:"\f266"}.fa-window-close:before{content:"\f410"}.fa-window-maximize:before{content:"\f2d0"}.fa-window-minimize:before{content:"\f2d1"}.fa-window-restore:before{content:"\f2d2"}.fa-windows:before{content:"\f17a"}.fa-wine-glass:before{content:"\f4e3"}.fa-wine-glass-alt:before{content:"\f5ce"}.fa-wix:before{content:"\f5cf"}.fa-wolf-pack-battalion:before{content:"\f514"}.fa-won-sign:before{content:"\f159"}.fa-wordpress:before{content:"\f19a"}.fa-wordpress-simple:before{content:"\f411"}.fa-wpbeginner:before{content:"\f297"}.fa-wpexplorer:before{content:"\f2de"}.fa-wpforms:before{content:"\f298"}.fa-wrench:before{content:"\f0ad"}.fa-x-ray:before{content:"\f497"}.fa-xbox:before{content:"\f412"}.fa-xing:before{content:"\f168"}.fa-xing-square:before{content:"\f169"}.fa-y-combinator:before{content:"\f23b"}.fa-yahoo:before{content:"\f19e"}.fa-yandex:before{content:"\f413"}.fa-yandex-international:before{content:"\f414"}.fa-yelp:before{content:"\f1e9"}.fa-yen-sign:before{content:"\f157"}.fa-yoast:before{content:"\f2b1"}.fa-youtube:before{content:"\f167"}.fa-youtube-square:before{content:"\f431"}.fa-zhihu:before{content:"\f63f"}.sr-only{border:0;clip:rect(0,0,0,0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px}.sr-only-focusable:active,.sr-only-focusable:focus{clip:auto;height:auto;margin:0;overflow:visible;position:static;width:auto}@font-face{font-family:"Font Awesome 5 Brands";font-style:normal;font-weight:normal;src:/*savepage-url=../webfonts/fa-brands-400.eot*/url();src:/*savepage-url=../webfonts/fa-brands-400.eot?#iefix*/url() format("embedded-opentype"),/*savepage-url=../webfonts/fa-brands-400.woff2*/url() format("woff2"),/*savepage-url=../webfonts/fa-brands-400.woff*/url() format("woff"),/*savepage-url=../webfonts/fa-brands-400.ttf*/url() format("truetype"),/*savepage-url=../webfonts/fa-brands-400.svg#fontawesome*/url() format("svg")}.fab{font-family:"Font Awesome 5 Brands"}@font-face{font-family:"Font Awesome 5 Free";font-style:normal;font-weight:400;src:/*savepage-url=../webfonts/fa-regular-400.eot*/url();src:/*savepage-url=../webfonts/fa-regular-400.eot?#iefix*/url() format("embedded-opentype"),/*savepage-url=../webfonts/fa-regular-400.woff2*/url() format("woff2"),/*savepage-url=../webfonts/fa-regular-400.woff*/url() format("woff"),/*savepage-url=../webfonts/fa-regular-400.ttf*/url() format("truetype"),/*savepage-url=../webfonts/fa-regular-400.svg#fontawesome*/url() format("svg")}.far{font-weight:400}@font-face{font-family:"Font Awesome 5 Free";font-style:normal;font-weight:900;src:/*savepage-url=../webfonts/fa-solid-900.eot*/url();src:/*savepage-url=../webfonts/fa-solid-900.eot?#iefix*/url() format("embedded-opentype"),/*savepage-url=../webfonts/fa-solid-900.woff2*/url() format("woff2"),/*savepage-url=../webfonts/fa-solid-900.woff*/url() format("woff"),/*savepage-url=../webfonts/fa-solid-900.ttf*/url() format("truetype"),/*savepage-url=../webfonts/fa-solid-900.svg#fontawesome*/url() format("svg")}.fa,.far,.fas{font-family:"Font Awesome 5 Free"}.fa,.fas{font-weight:900}</style>
 <style data-savepage-href="https://fonts.googleapis.com/icon?family=Material+Icons">/* fallback */
 @font-face {
 	font-family: 'Material Icons';
 	font-style: normal;
 	font-weight: 400;
 	src: /*savepage-url=https://fonts.gstatic.com/s/materialicons/v47/flUhRq6tzZclQEJ-Vdg-IuiaDsNcIhQ8tQ.woff2*/ url(data:font/woff2;base64,d09GMgABAAAAAO2oAA4AAAACtrwAAO1QAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGhwbEByCkCoGYACRNBEICoiIaIaLbAunZAABNgIkA5N2BCAFgnoHIFtnI3Ki8HY/oyh1VxVym/zz40XB2LXhdgTtzZ2Pr2DjKjPoDpAI13bfZP//f2rSkMOSK0mvDANm2+8hmGsmmoYhR2X0cE7JmVmTMzNTM1prrWHBEyut2Ar2/TjOM2no3TLv0GVXcaVXIDaLbvMb+3CJqiqSK1v6rJiKxvDUa4quwkrq/hveGNojqOXZ1ThHlwdJXBVYVig7sk2CbLIEizX7+xdeDo8e3yVjdLsvP/6x8NKbbUms+L8PY8EWCFupoEhQV1vQEuuGH63YWqjgJvFS4Wcs7DCf8SqgHJtTI1k5efIlh2ibxwPCAyqgYBQ2gmJjTrGwEqt6U7e2Vq5KF2Zt2kNXbo21aJfhjzh7TXIJ24JnSWdR9CT79AG5HRYQB8DcdYC45N/X/7ujJJPBwwr9qb2ivfoqCJXFCmQl+P1So94Mn+JTlFAJIRm7Zbc7xNkQwuV627c8//x+T1t7n/tmBt4XxCSJV/EQvYtVojXRapFmKZMSlXE2E5Fh+fzqy2RSpQ25kAs1Qo1Qo06NXAgvouv0v9PMr/qaUqeMQ7Ytw57MmbfafF3XxGGMMEaIw2DFhdnkZh5KJS44vkP552XPnTsv268mGVHRpW5C0bukyUIRipAU8/OUmz2uOxMyA4QjIRyS/wNyBRWQDBCuBOSaP1wqyKVJ9Obw7MxaK6Dd1tqECm3V2pNJz63WrtiG2mO3lbZ2S+ixjbb3gmvco8e2u2sgojWdnNyPZOsPggU0dip86moBnUNqjQwD9QFwBMvZplSP4huo6l7roZ5uKhZqgbmgPimqk4IH6pOAB9GB+lV0K3q18Cy0Hl5q+zS8pKKjN8Biz+SHB4cFek5LJha6yB+AU5fWVkL9pA6H2FDb+3CBZttSiq9zHmAJ+qN7fIbrJfp9gzIu0YHtOsPVmZ7/ufxfLs/du5H0KhQOYzYv7IS2ZN7EWI49x1OaQ31nS1N06MH9tAzC1JkKLYEgL/l0xYHehnKqp/Z31/4MRZBOhhLI8hBS52CZR4na+fO1v8n3wBAYWAYscKgMhiGEhs6wWfoA8+aXePBOrZ+RH85ID9sskiy1XSJbLtxxwbEHkuinjjarn1/51/6lwC4HCgdUWD7qHlEcH3AP5sul/sw/zlxHowVDYVmGgoFbdgo8o0msSxz9VperdJVebwtx3PaT3Swk3PKC7X5IeNce+MD+YN+fyTv7D80BFEsE/7OgnxcFOEbRmMbTFq3EBdJ6galWWVmpIg/7NV80vx/kL56g8dXv+TUeFWUmOjKdVdFRaUBtzRimtrppCzTadjAP36ezqvesurBLA7BDs+5Gk9YPhgbMDF9Bo61Eo3R2GrVXPPmk02k6FYEqqUin6rQTz/j/q2pfS0hDj//fpNmo6TbGopvV5qreMgr3PbxHvIcLiA8kJQGiZZGOVCYleUwn4gGkQFB2nrNOE3KSJ8obUijKrUJIRbNVt+3yfP0903vsryUJ7AQg9W1nXvCaCSwLSEO2/r5UrSuaND2iI+RJ8kZ5I7VRdtXcN6fTHG5b963qfmg0un/jg+zf/BDQYFMgQMoAKEoIhAw0SZkAQZiCOLMgCMp01kiTglaTEtAQvQAkeQnIdIiyNCElaeRNIR/3OMc57vWyp71e5nDc03n92E///88gJeaYbIaFIiJRCMRb/u5Xqa/shURp97Wwx+3d/69aVaNiRMSIiIgRY+z/v9/5/f+n95559/2aeV/HFi1aRESJiIgSpSyl9GTTLHJZtiXBrbA2RSBhBJJQqS5z39y8TNc/6ujp3cYzrYqUEUZhh5FAxrz+f4hu/x/dbyzEoshnlqGg7K6kWXfJXQoJgNABBIAa0lcLltG1/H4oKdc4Q9KD8/2c1XTfe3doOxh9HQCiI/bJe5bNYLJt1V89hPEX+OpfaOS4d13nNP1Z7gJi1KP8+HfNbFhfFrta3OjoDutd//eB/S/H343Q0c/eF8Nqw5MHdfm/K+MKf6/urImuqQfmWM1W9multQ56GWKkyeZaOppGakoeTQKPoa9g8Kehb3aAzinUOYEax1DlCCocQpkDKLEPRS74NAvv18fUUzMGJG5n7WaDz6iLbnWstmVZrd7g3eotn2mg0LKZ3RxeLZnhlAKOCBFROOyx6Tqj6tmDYZ9YWIYo24LxdIZnDFQmR+4PkMyUefpMJQtt6W6RrLS1y5FstZ1rI9lrB3L/wV3sYpPus8nMb1Z7WYe2AZ/32Y7u6zbL9n5XSziGjgslvAJB3MefsSGhwzIzDtrLtwphSZka6Spv99uFkYUvXXdidMwIUsRKNiaFzPITd5KPk684eOzUmQcXnTlX6Oi7VFfDzdep3noXfWtZ/h9Le/bx7hd/9OARafVfLYqARmEBSghAREoKC0fRoOAkCE0JSSgi4DM1dbWO+V6JsE3uMAYSqI/Z+y/Hp6qrIkv3Aob4SX1RY4JnxZIlWg+fE6qfDCwRCXt9nr/9Z1jxsa2qKCr0EZFHLtBCyKmNyKNWCz+2PV0iS8JuYwbdi67bDJYnLI5QVQhNGJW0Z8I9ifxNvqu/PQAMFRzQdpdgxA9ugPOQgW0FKlmMJfVrwv93MwH9VoHFAPtJJBEIlGMv9HcpdUj70dylBUeJbJ97uvUIR849MpTEoRgtQBuTY2PGKnaEFDKUD/3VwOVNKYVNon53pgawVESqRteY4RF2emiWBQNIleJIZULT/vYAd1RUurR8Ozfqr4SuiPZ5qozw5loxmbMg39zj0YwQRves2D+RsSjKdiQb+2ItfxMUPHBUPPctvPwpJSQmWIN7xlfHjtCyIXlpvw5HRgWR0FBPEnXdtB7eGtB/c2xUCNVWWFwddcyif+ebe2HrSd6f6zNEqBGfQ/u5SsBhhsmRG6GeSGobrMZAWIcmvZJnsJHSe966agZd1r/Ya5s9jVv5rQl/Ltgwrv04CEi4d9dZNjFw4D5mlbFMNPnnh5ayUzQ/QntYHkzu2B9OU2EOal4lF2MPcnrFXcQDbHgXTLNXsuzuz0nA77vWFY2usyiLdElst9BkBaveunNkhdfodf1TsbyZ9WmY2ezvdiyNadZIXqFsXzgjToWCufKxRi+pRDje8FE+hoTd8h+COq6TRJWZvyCWBCac1+9QYiUfsrL7qPuNDqN16BAscc+MKnHJB2F4bug5jMHft14owdmC+7V6mVAS1+AGT0BHbMjA1GNZ+gUmcktPy0QPkoMY/pZMTaRGuxBs69cNYqW4FkQUOA7PCTTZlcGZserK2gssP/OvhrTNjauHW26tc4U32R5nYvF1sbqatvH1wLJW1xoVbxf7f2BsTUgn9bkxUU/iScNC33YPc72ZgH6j101fRAfgSebDtJpdGRiH6RjlwW8NuOgDueHW3Hv3lIT5N7aef591UTiLJFEP1jCYdqh0nKZBO4ceJ1jeR96vFCt17N2mbSKpvpocMNFeD5p074tLEjlPbx6kxTJjzDkmO9fTm/Pw1vC92aIVe7n1TRY3YkBGA4wKZAAIHWfxoMqGNvM09DnilkiRyV2yEnkUVlZt8XJV10iRPpQ6sz9yiNjf6tkAXvTTvCJJKf/r0JK6sDibq3Rw22KFIVzCByCuz0NyQtV7xC/JzhdOeMxDDPB5Nf9vSMItrSSJXOdDAd5b1folLneCkTLpdPuThtjnrENwWcY+jsbJbjH13Cdqqs6HtNa1/A6zwDj4+lmWqQb3ku9HIw/DurapzYMA5dmV19UZoxy1cV9XmftJhqjtEv9vcpwXy5N4mv0PrzebuqixHX+uBdpcwGbzkIBQx7o2vbVg5W7TIGVbGkuJYRGJw67DrI8Y10PcanLSvH9gtoik9BZOWhdLqC4fZ1nenERHwcKwWF8unnaxq037JBtDWmSc0fVjGWXkYxlm+GMZZKjd759+guZvhHW6aw/SS2CsEqXbiwhgVx/t6XrhoWfXSFe7aOz2yQGS6Wb/xaqAYLhohnA8zs2gCKeQiZr0VlJM7PJh+mboybP/RtTM6zFfPzIOe5D1mtUyxeDuIJbdWsVIYZN7qktBAhoS4kUzqiajPsufJ/DHM5H/As+9uXRwF+iUnlennsTOaj7C6ThH/PEQNsC0Y/tliweFofLdejK0f0D+PYC2GLOKr9e1ifhTIwmawRHMhzYzQ1qTil9RQ/aW+w2bIAW6E3mPyawRiYFKdEvJJeIISYnvAc9cnzi9+oMGp69drnEvQLATHpXutOt71j/+ao6pTGG2E/ZYWnvbyzZZwhrvSIBQgY4R7hDuVDmQsE2XMxqKur4qhCR0qboSoUZ3v504aGbHrkc+esiCR+QUgWFRMdNJljz+mI/DJec7cqUzZx8c3s/Rja16L0o9KCxpvu3ytVvtoNi3pu3K/f9LEThibirvGfTDaLKQV+M9yUgmVDam5QoW9fEsShuXugaV7JB2sz4f2djetrhCUeEhHQ/7L0Mdf9jf02pQN0EZzdfMJbRLf0w2QKqeBqicdHk9tlPXljeBOGsVBpVWPadVIchDwTOlSYOoEGHlM4mpOWmntKOfMrd0UWrCavmSDp7D1I0cJFSUDkKrzduDNsQ7XhEfFalHsQqDH3hdmhTVawHZy/bRl4RJCWcFjgZERno8InVCME7i652bBysTa010UwluV3WvXWxelKLb5pTBYQAPErEHZwZVSHT5K2RfClLJZXUriLJZBlnYhuOw6/w8h9p24grmlse+F5x47QCMjP87uAcYxlVnPkVJHsMquKxVnicP7w2HL5pWq4GOqk80eblttx0JU4jOxZB5D+RcSnQ8XByQuZmwZ8oYdVbp3ZMzNiZyI65ZcB1E30HWHIoRnO3cOPrMfEKAqEVl1mm4R1gmK3yHW6TCzHOxQhiIyTcL3cCBlHqBvgAa9wTTKH3E7vUE2IBppvke8lAHp5Wg/pOPbwOsVfAxNI+NIiK0TOT5h8mGWkFc7W1aQAvW2aLP+zhLI4zRM7Ln2aaQFfR5cMjk5nZZNmt5YJ5bAS+yvjPR2vNlTdlR7LnNrqNQXb0VRK06e136DBSDq5tFjXcD4sWbKXG26ecjnsFEqLBHegXfuaZhj0sxQtmle/eQBJiFMbeeTyC5KaYvfTgK/GF8pDebNW4wJS48htEDFvjQHKh8CtZ/JERW7KGP472G/nfVO3A36UCOI2Vnwt0ouMHuLA/DZBkpHff4MPlnlG68hpuCYmMS33s0GAXsdUfvPuTsVjO0s7u1fnDfRKlInm0TE5A3loF9R334nQ8FarpK9K7jy9BtE8Da5E3SxHBLDKWMh4/67YsZ7gxSyQHtMvjdk101ZErTzP2oK6Lqzu5huPHq7O5eZEpFqJY1R1q6hdXrIfsm8FjGDTdwi3THyI50VsPNP2ZhFac+RfXLpU1SGi31LNXn03t14qycu6wWm2Gy7OY01Ki0foz7dljWboJ7zoq7d96kZBLX/xg5EBblRNgVDyZGi4JLzJffeBTh8dT/N7s1sDssWXTo8SdP+tJVbBME+b7/5fo5zSTmdGDOE3Zffbfle7mdPa9HA9CBQKEABHLCqHNglJI6OyQ2jNr1ViSfqKRElTXThxoFS9rNWzZ5zz4jEzMLUE1AJSHqMb2Fb1jY0gnH5YHK4WKmchVQE0CydevhqWTYrJjmrF6wFBtZqCyO1X9GI9L8NnY4G/FUgS231gDJN7e7YEDRZ0s9if4A2qg5aVPUzmeJy+Pgzd+mEurrJ/YaAN9/RL2hokF949Q4xJGEBCTbXO4nMhLsGvz8eL6IkdVxcGT51zlFF7yg1F8QiFR8En1v44oxhcRmvfmVClv1exr4xNfdvN7WRKlNdjbeBCi7btW8ibT9ot/JmE+mL7U4IeMDtZhZxwkUYH/9raq2XflEKeW9sK6RomoOOV8IEO4YCa6iD8fsRRJo7Y0JbevEePp212xhmhD/wsGz71tRu4DFyud9snnDX9bLfXuuvyuMw+1dGuTxdd15eZcxJZUSZ+9zd2veUyLw4c2zDAwtGSJmf8Iy2UVaoPAHsllF8ESHi5pjjL9FSQVv87tN+mLKh8lhPxJJqXJUoXxmqkLrdWxQsF7nJE1Dk5pOoE7QiIm6Gg0mXq3kPs4iLR4nqs5O3qhig15Q4GEv4diYhDEjG7pgyI8kEGIuBurHUtjlToKIjaFir1ePothitrc2BxCxx+cH6EgXosly7e0eslEuoDE5PG/iMGBkhuLxdKvkx2vcTfHWUo6U0umenHIJyRHy6RTQJqoHeNeIPj3IhRiFZNtB19xZaaaLsGG3IVxIlMr/063hPDMd9ixA0OOUaFRaM5+uAKmU02CzPW9754uNo4Y13IHDR8/pGZyb04wugqItvWW7XAstuvKruepXyzo7N5UPkmX/tgNdx33Lv9WdyiJiZhF1x8Nf+G/V+ielIPaiuq9V1pGs96obUnLlqYxs+NPy3tYOskThMQzFeEDTRPrdrN5+9n7W+Z5Hb61O+WaEKIjGDDY0hx7iRwMl34ELBU2Ai+OeYl8frAucHAIfuGT9VrlKMqgI3u907eazD/IM6zK7+xVWVNygOUhtxXZcDZR5NO9Dz+AcyULKjkOfv7Zw5Eq6OPK3xIeeZmmxiCNgFq41qEAzxf/MUz0LWXnHdwIdAHAiMeWhqFrVU97tKvkPdKvBlZt5MVYJt9g2c8C2kNt993M/mBRIKr/f490DNWlDUa43AyvB4psFpwOst83k7h2Ai4r1TDZd1TvIfki+16WtVrFH8g2Mf2LRdktlW/lA2wenYNxQR8iMB03U8skbVtxbzqA7hrmRhfWfQAoRvRtke4+2TKQ+uhaC6MMbUip0/D/jcldaIMyTmvvMx3deh6Z62wgXQ3u69v63ueXdr+AAwUYbZRAcneDABDXLTNgJ5FWRoPK6VFMkMqRnu96csLLria/mGF/jZgmJjBpIFoyen3juTRe+0p2ef4OjKy9U9VY0qqu42aqqrXbWt2LD0y2Gl94Zu/HO3V27tZOd75W4Ahm5ByxqWM5zabjQI39Rei53XtW/OCxZhWrmFArFdj7SpG3r2SDaMaoAgIJSTFvjO7aOpP/CYxGurBBDLeRIx0VzjlKcilJfqly6pBqrfdTbq+6imR48EiLoclADCQw38+ykUdApoFIxU1KTmIpkofJ9eCPSoPifQnElADCyfdonOzdwv0L+lutilP5EmduWBBGTlburzRyUBtlleGGSLTaUkENpavp6ST5FxaQpnj8bB/qyjRFsndyLg2IaUw5AKmjeVWaGFw6Z86K5XWvqvRHYsuCzfFWWw1Qvpmf1wpit7rvYGI1ftNQBuJRfQEjzab42wlDV1nHHy4MQ4KgMJVP0xhvlOwmI4TwfPnmIMSyfHFoDqHabeXZv5fYxII7ZH9xN0xjb4V3ZvVNAOF8pUDL/3lzL6S6VdMM5y4kdAAJj9brbg9arnH6UVEFPavpYkKdj9bDUFlCh91MR4vqVVog7f92i8ATqStXmdvTfVwAq7pgvKIQz2Ra1cbYl01JjVdfYSnH1yMWTq+lDulrBAlfnyB75nHTOgzkE1W/vN2P+LB2A8lb3mc2ivPCHZPzY6e5EZ9Hx05BvkMUB9dtXk0V2YCXZNeVoBm2y2EjRLWWCzeq2mCXq0tdFcwp1ke0sdx91DPS31ZUgklmuuGn3DdWtdp/SolADiHD7rRzYBKkCHoTeJmbF0zcINZd+Bo1d5sCWS9GgbZOX4BSYdwnEzP/uoYV8hUlj2bLj/YIS1wgodTEX397qf/3a3xQxeJnYicNGL3MVWAMx3yu/e+5gwm7px/sNMgPlkF/fMxo61jouBaXMjt09PYPuk1d/Mu45+5qped4oLz8+0jAvPtIgz9v99URh/UszbZSHmHg+FLN/mFrm2W2kmFfk/cdHmuVdezre4XTet+WgzG6nvWhvb8yb5M375tjn6MEg2R+Xeysm9PeyQdQQ5E8kbxEe6H/Cm23YX54I8tn/dgmRF7f8v6diLSfpYri5Njvumle+N0RQoxZ2uPEEhSafZurTEhwoeVC2M5UsiU/6fvS06gDGj54L/UQCweFEwPSz8EgZ6wSYF/yNbrIYStU8rAeDSU7kGoYAkygLD6cX8enVAqJXf5MvXiry7zREFco8DTqY68IWKPPYXW6fzx+QJx/CaujJ33kdDg9Gpdnk318Q+QhaJbXDIZsZLAz9f72rsAFbx9H9IMTdQY+Uas3/Oz6sh6m2uuyV/6mwwg0Mj5OT6c18TGxiCSRfVi3q1mCj5a11dw/0ZC+2d1ExRDTUJAeFCn2EfuIp53SQQRF8SxOhN1lxwvjn5CK5K4huc0NxakyPYivFZ73cbSmd3hJImz1gxL3m2zXXbVZjelGhutFyWMouOhDcwmzVdUJzXYhl1enfwNRMwokQCaaLM8R31i4a1DW4vaAE9PBAFBjfdLBLdOdgiOGsGZoNHitG3ExtUJpu+corqRCij5e1QVB614ezYTSsIUdkR2cZ6ENgGPmWLYqc4tjH8SmDHRn0EDD4u0T9+40QohlH3SUijwon6hxcJMHZvtgXKMExbOUK8x31rBlCLU6hMWnPzdhuIbVyCsqqe7aD5fenWNd/zW3KGbYDqOvKtNTMtOI8TT5XGEAM8P/SjnnEL1oClIYgNwCFzlz1mAgWf6utLCvzEXNSepo5geCT9pQAyJ6OWway8UXzSnEa1yPlydfbTbjLjM6cxeOE176dooUsobQo7OfidXyhuB0q3LOe3BkaX1Z5Ej57wNKXKxWHy1qdL5sQ0vIV5sE2dUl3tjlBE9Ni4udXOBxvlocScehhzR9BWd6OyHUBgwkdjwxfIG0gxwqBQeb9YkOMIBwVTeT9DWLBApzi7zfnuEGWOu2VMP/+DsGppSWXM5770iez4nsqMfdceTulUdid3/mOE1vN+u2RHM0U3F14t5iIltWF23sNfCxBsHaD7cIrwLvWrZ4/Q6vk+jVyJ817USV+5H24K3fQ9Wtgw9TNj+maYuwaQhfEnO1MXHofZqfGifPV23K2g950Hk7C0enMvWjntS5uu63l184hkjUuE532nJ3JKScuEwp0TKBQ8LFACmHjmxkt5nnOKpEpg6q8y9cmqqZUNRUplbemm2VnBqaumTfzxHs3liiPfOOnCwxNKTpqWHrBfCTPEOHtZT2YAFMvozVGuWSvmnLX6aL+MmzEZHYfTsIiNRYybVSzq1za6CfLU/M2FqefgbXTy066JROkcWzsppB9IX+8VoY6Q6XxZtV4MdFcAiX3bIRrfFQo3cetiUNmZZpmqrz2Y0WAlgq1jDNYRm+uWex66Kk1htc6MPm8ueWyHA8uYgn60DwBG+iIUfi8+ZcauGlMfuYGZRa/UXMpuHneiPunENPiRvP4EtxPVz4soF2ybCJ3V2SdiRdHzpt5oX6ui8n+MO8AR2MlGjEZfY/+MUFb+GMy4gJGdmaUZyx8QeaD9XrFFNzJYbvn7aVsvxobw1zGnjBQ9GdjECjboKyQybBcE8SGfd64sFbLstO+YQmY9tJhnMy4w4CI1rUZB155QH0BnfXS2DsAoET1lfw0mLQdgC13NxvKasanR6Wgc+YnCq1nLkdzvZ5pccP0YkULIaqguVttxrrDU3OUQukrJqHecwx2I7uZjXEmfhNoqPeeM6ancGk7HHEyKwR4OeutgMTMH1x7E09DRc6zZqc4UCzjWdLYkPLRu15bBA+Jmu8Ntf1wWHiIzpoTB0F+2BtYHExR50x9O+zjvFEBw6JKzAsHk6NvFDMTMWr1VotzkBqIBJaVeq9Wi3RreR+Yv+dKTsW0psJHiNwPeQpgy125mxxfDBgN9EK9oFxeJbNg5CfbAm4NDUY+F+Qnoh1Mu0hkkBFNwl6BA+POcb00TTIqfXZS4tcPH1d0Z9RXqOFmpFH4Kh9O+ktzOffyLUlDLAzfopYhGML9kr64YdlbE6Ev0osTkbpi7I4VAYMMmeI735Z/rXiQV3yRqF23GaPaXoIAvONT4JQimSDt/bZyg8VOetECR/n8SXQuZzG2e4hRh9ot9bqd5X/7fMaBoY/F+Ex0KtM452fY/Mc3gisw3ABEcMEGQw0cdDB0sMADvpCDwPgfhbEH3irFaSBlzh+B5D4oQzIJTXKGF8D01s6kD2AHjahnjJizMykq2qRb8KHnwj7PvMEpICeX7qP9PTCFrHpxZBMxkNIJu3tohPV774+zmLLFG2IEEjQvmz5kpbXpUVRgyxT6G9SWGlda7C7PjhHoU5E1omVT8B5eAmfPZfYWtEV+UJwbhwlnxx/On/UBCffQJZ3YbKECW7yebXMpu8u/s92SxI0AvkO9GxCJobr1nFIkIzGZ7VL8BJizj+cHUf2Uct4abyRcWiAhrWmAaqybS74MO1eSqSyjox4w7AQuEdBQS/P1zB3/BBfteYDcEMT6AYBSa3VI7fg0AUbudBGHsUm+1+IZZf2ehbJyug3zAT8EGC8shzVnB01XfLzGDhDCGfTpIqwFAVfYyajkoxum5KqOCWwqnnIASHdQ9FJjAqgfkrbMMVdBbxeDnhk4T1SbfEnSyrwy73ZSSK7nuy1ReuCdCz9kQLZTQYeXpGQD/rc5aJDlLvrSadjxTzbX6c1SXAuIXttaFb0E8/7rKKvDRmWMx2nAhIdPbXZqgrJMVvLtq4QFIrIBXZ2ePDiq8BAac+7+yRqAGKeDUwuRO1YyNPBKYQCPwGJdHAvYmdv71r0036rHdVVp1BtLYcO3yO+h17k7VphFJcyJi/1ctV7BGglUnfox/xgAIg9qvXX6Us9Q9oN6b0k+7I2xjSwttWshhSuFBiURIvOC+h6CdC32KYhIAh/W+LE6oqIImHEunmhQ2L3HemJmVamyTxOU159GrG1k935Otgy6kx3xI1CR5cXXgfru8T7RJTyAMpk5iAXid8je3PDiWRS+t6WWlDSk4O4y5Ood5D78/d9hJAsqGFOKrZxmkJm2Ou+Rrw2RWfyMhN6dUMtnp3H6mnMqwDklrGDOz7DMPIIhZjcTvsdxB4serEyklxyTVHcCprJySFCEoY23Y0/Ck3fXlbJR+U5RHZYDQtJyiFz2XrNKyBb6fzZXuBYgjBOf68xGmutV0c8uNsx8uEPnfAZndcUI+BkmcNxigj8hfrMhpWGTe7KaZA2icsQUqfEiPZsMLXzrno92MX4miRWR60Ao3c3ycfoNF73ayVc/PHNc6q1ys4KSlmpqF4r9aVd5lvH31z/5IwzvC8u+fO7VLzz85KN/ww1Rq25Q4kCHMl5G3xXGxaA1PA61VhEB5KDkdDOUdaqn1VZ5tMddSvmna2alx4z4n6VenEWRcg0kVA1scwGrgXm8e5tSgOkftq7shfkB2KMkIKTrfIGtp0wboK9lRuD+IdkCGd/Q37rUz0GjFjkoTReqfWtUVJ2YWRPg8JhduuFHsA47ZyrkwYyjg/xusjdBs0LOaoeen+JvCWp8reo1buKMWLpVkuNTVi5pXlrljPIthtQH4sr0aNn3R7KNS+yQd0gaMKYTwdROkVY3n0pSrpOGU1XuFJRdltOpVUXRkIA2fQ7sF1zYdQZGMV0oDhGdjYSE91K+FKloN/KnAhngyPj0PEVFwrg+RQXCCKXK/1NdhP2p90p+ikDJisuEvOKXy+fTQ+JzCvcdF+5WOGG2gNNfk1pRk3ns/cm9sRU7DenuZWvUp0W5vRAGhKVUDlD0htA1lDLcuearIXOJ+eoNThXxXmnSFq+uqEeDKKllb29kx7o1KtdLL+q+amL8lwpelRSMMnotYUBCuwD1in64EFEpfzOtMVP2MpJWkdgvxgV5BWqzMDvnK5ZqR/25+odxwjJoOVM1zOWFG/15F+8YxeO4q6l7yEPhegnow/nLvGMA5be4pmTGQaa8u6NvUAoxr3+LDZfsK/WbZ2jtP+uNI2T859dy1FR791U5QEUwRtExUT2dz+ocH8oPW6vTFE6Udryssll0Qvz4lu48HBMs4ctEwXRp9Q9D8qHTnrM4DkxR8VIC9QSMnVklcXXjO/eY9Bso10kE5khZU9b6C6y0LJID1IiASwfFwt10jerG/iDpnXsHZM6RM56G2Xz/0JI75brcQ8W8Jd6b59FUWFnoaIG0HZPnaKrsleoFcB+qoJPY6Dxcyjvh7n7ZHUXwfqqCkRMBqUsRwLBJw8QMszxiVZGz8Ni3JJHHKhk0fGgFCWT2DpC9PAwHBsCKPjuWOFPxkahZyP9t6dfuPgtER20Q+1nH+fGf3s1lJtrorAdWyEnFN9arx5nYYdxc+HnSOMimcnOnCvyBwR3dYHXrVQ2lC0ZJmII3q8ZvSJ/Wi6vz0T+B0gaKW/84Gfry3M4t1rridyku4bnPeLAA0evQcJs93WtNq6NGb1wGT2UOTefMTzm62FJb5stj2Su7nHV8da/p9bNo6TZFS2+MzXjjbq4NbYmttDnbge30dnnrHxoFEaorlrqHq3I6lQc5Yerrcj3p7TBI1egGJwIUhQUWoULSVvTszdsr6ol36RnbSr9k2cf1nV5MkagqrCg9uBCo9sDwvUE+KUP/FjFryTW8XtI+HoaVoR0Qf18gRMNKgvxHqjiyVLokb7CK18opyop6iyqi6W9Y6chPjPI7Xpj1CYEsLDzE1YgtXWrvAY9iB1i7ANuGr43THekVhtWL5naG2n/k5HedWVKVteI6Xg72fAg9J9f0m20bIPUppqL3QmORf+Br1GSN7tufqKGlwMc0wywRQKVlK1FyTOs7PB/Atx1wT5OyrVweogY2pqlfQEIhQl9yR0jWJnfDxs9eDsbnWUMLLvs0/Ve+CRJzfblHoD3sc3EybThL1zkSrr0CUMrgihk+yD/dbLiCyfh47M0MSCswwsU2utUdG37a7a8wA6XVHWhpaucRV987g/ghX6WWdozyskyrCdNJl9N3oMqN7/RGWfqtg+E++ZxA25b1v0FS1D+JHKX7XyHAK2v70joxF650uL7Wf37maQw/sesb2fNON0we35HTOavrEGW+Etf1ml68zTenjW8Zv5IoelYjrmdFch4mh60NvjvmOVmi0WnmHL3bRYevfeyysz3UX7QrrW61VPv78v3F45dv/+zTHyn7UB8MDh4HcmgfTh/GD+YgR3HUObocw0d5PH4cPj448qkQUTtxTuDETvp09jR6uncazjNRyTPrbHsG58S5eW45d55nz+nZX4Ej3kFkE51YKMtk29ghdpldY0vsBfvJcaRbSCwSh3vwcJ7Oy/lefpy381HO54/4K/4dUOSryPJkBplNtgY3CIRYSIdCqIbdcABOwkXohuGujFU2OkMg18J5tsqLjsX4UNxW24PJSAUfJs3uEvDNL/MTYS1G4n95hWLR86XU0kKBZPHc7d4+uwZeZmqOBiuGtseolCiO9qfSFO3KT5M5xqykvIaWfbMJaslVF1kNaT6tvcqjMf/Wt0d5BTXqY0ppw/k8993Ky68sGtCCvhi18hnbUaPMVrkXnuWFkcJmo4b0Va9WjWOTr6SrjN0naRn6MxRm0Gsju9jTTJbq5poV/vcONUtbULXQVrZGd3FAkex6gsavaWWFka0kysges+s/Q7vlsJA0axVrkRfJiS53S7zypEkqQgIXeYm83eUpWx0Yu301lFyWJ04iFWhTZV8su1XGKbzxUysfiWGtsrYFol95y+LVXpm7/BmsJxolqpzSj0eOUGtf+XNLqYbuypNXJYMQKfvPMUkXaRu4RFUfU+mc/mcYIyeswEnXwsbuljimOBJa315JxnKdYk3nJEPEck/ZoyjFG0UgbUwoZ95BMRcY/0v8lewU8OmRXkWehOwmLhQ6V0d9hCrBL1Vj3yzGzbSJq4oqZLl4VHy5TBLvBmqFpu6ap+9iHkqtuLTc67SGuulrIhfy7cH8GVWERjCREcU8RiJXvVpGXWMC2TS7hh1hm7t/gmckFmwTqiZ+urZWYVjL5uWceqEad0LQWyiY93+gnth013nKEqDr/A1cwdnKHa2Oh5ttCe17kxDowqqH9JqupNaq391hWsqqAy66FeNFVfUpdtTZy1zqCRi81DuTqWYcRsOdbf5hLi/dYwnl1EMXEIuaLuVkN68uI+SiG7yIapFQ4Wvvqsm3VojSAdfTtyluIXdVgt/fqyMFYbsf6oofnIJTgN10FjqBJYrsvnR5c41r3UC1lYWGgq3ctS5VvsR272QYKFa+Hqf3V64fUwNmc1OB1xPmupUpPHJjilOoe9oh6P1cl1mY4igvODDwYRObOl5G8vlCuZN4Uly8M2EvbLT9C/EGndD9lrF8GYNN2Dysl/jC4zirr143W2IcBpwSr6qjaA4m1C9XMMVpxIuKpAdHVIlwEr1BVK+VdCnBmHwDgUY/jHeAteY2cHPJ6rWjgrvC2jxMxayowIFjag75EI/MiuFOQV8VtC0u0PW0jeI/hZbpyqBv2/KhvVlINlNq6H1+ZXBYGADBQ2jyL8AnVt0avgPBBTrTSRpZYbix3g4ae3sieBIN7yBkcvDZWHedkEXQm+qyiwF64HOw85Cdp94lhmcrVcy6ve6lgNmHFYQG9xmOeQ0cil9LGa0QmelhxV3AYEN4Pl4QsLSBD+SngYGkbrae1mu7ANbanppH1X3ZRWpTq9Hy7Xhb0H9rdpKZ2IM7xnSq4wtzdjBDEvWeZ5vD5de1ixky/aW7AHhHuhPXfUYyYZAAVON5V3eaHvGZO9PdcrjjdJz1rUxPWiDy4oEuPauROD+vGSWjLlm6Xqrxe1eZJXGT33MfwdP1kFOo2SPOv2N6JzViUWmWSxSF93nihgHWM4ZXTi6A3hmy+9JOfzekjVG2ZRuuStqWH8BSqctiX5OFh1/yU4kUD31K+j8V3yVqKG4jxLviHtG0N75N0D7oKGF6uvdVaSe93iXcM74Hc4U/hPWPH/Wh81gkOirtWUrQ/3Ls+jRV08yrkw0thOf191q+Ztku9m8ZimfYGOhLQcDARQ8ZcngyHvySeESbnE3y9Nkh8oMlLVUiTYxEQbYibYnCj3qu0mmBoRbAXlZtayT2uKMTBosDUsGBAkzGGUef/kSAjCXB8zICEHGnAeARbctV+nvBpOq4EjYXQxwzr9q1+E5FI5B2xcOia/IZSk8k58yU1b9O8LhXObufqhFKpnrLy339EbIMwTNNVld9nOvg7ib4M56uNLLlhCCpwVcT26G9v845KjWmB8CGO8exjMao/mddumAVK8DGaDrrY+wYQD5d2AGZ24fJQiWn9IfxZOqcIfxALZp5azA9I6aHOnuOIkcfrW/vix8xOM2RUzTFUfaDi0NJNXl+C+gRxM3CJyLWN4BKJnJPtiNt7dI2lwl6flgiApQmb88JaG9DV2CD06uF+mcEwgDQ2nm0DK69cwcBcd/dMyAptxWg1l0dSJdbddeYigUsjNC+iclY2CIHGpk0HALGXfU/VRu+p6Dee3jitfjCHI7F9Y2NwR5eXt7guhu0ltW5zAzNM7B2pp1vqmdEJ0Ag/7KvgjuAbDt8cP6eNjTKcDJsDR221tvf4XdKQ/SHnKWOZRvcO2u9f4vpck+e9BJILnGnbkU+3YX/djNog4QF2TbVp1klW+0DbVHnroN1cZegaZVmyf/nVnJQG0NMttDq2YEcq0Y9IvSd/gbBnnFCgd0hdp8PfE0nu144foUfAiqTBKa/UiKAfM/n4Ec4bspfGLWC04y10y2/MzFPcPImh/ptp4/dVFVP/kO3SzNDSg5rbOmfk5jmtEznTM/f2DVDURd9mS6XBQOPYEGM6cj45AuJCx/pO7pliabIN+nnEpMw9c3VFyZJxnLW7BmCNaz8+ZQjzTD+iqnfUGGK0f5DA+6r2FFN2UVdMG2ux8qe7YWMYKTKdXJcGElUUOr6c4czgLEKyKvVGEqmT2eXWUZ3LczD6bkfog7DaCx2lMjqvbPYXLyNY4rMM1cQ1PXNWGocBaEFty33C8o2hMqqT8Ya41h2fSTUJwSk3DsHJaQDD3Y0nB3CAqtPp6gh413iXHiA+t5OGAn29ZzAfaGh8E6jcVCKaIXR9lfe+GUFsOHKnueYb2baZ9O5qYDOoZrHsQlwOHAtOm9C+XZh9l4OssDp18/cg4Wj5ZPSR8bmXGWe71rDmUzzB9ox42SAvNPM8qQX7W//Q4Q/WNzoeGJ98fuYbb9bvgbRiUvCkTmewdxOEy2BlMquQ0FJSzU93vaO96WmXbn/mT6qWTGH3wkd5Vj6pY1iRAjVTF8Xn/WvflRseLv4FaZVZOBnE1k+EurHC5tJgvNtpXk+fflTVH1VRF6sR+mYwQzZM/1M2dg6Va3Ex0mDB48yuXdEJQmLGfJ7jOSXunC1xRVVz8rG+TF6Ivckmo5s4Yyh7v1ZE3lVsomB6nbuBvrobKxrYfLhsYeJFvNUTKjgHXTE7WZQWSjHa1GNv3NHi8V4YsZFs6lU/cEkYR8e55Rpd25ktxi9uB/jzI/pgbAeELpCzMgwVWMlo5ZxJj7bRhLi/c4xbhF9T4DH1QWipOB8pdV/ZN8pwejqz6KRG5a+1g/pXOCpFLnw9OZlm90F6sAtziKfwLfEBA92h/6AQo/t6esrdLoNZBbZjuMvbhmBfkH3W1IH8XqwArD/R357tmpDATIY4CwdQVzl+6N843It/joQKu8sqdJYVdlAOiqnIvJHI7vJFy2Y3iYmfdCv/sBYyllacqHh6zwoNIqooIkuJniTzbAZM5PmSwWrSS2aUF9znWRtYl+3J+3Ajl2u6zbtVff2mod1OMOeklkLWtfxTgWX4BYoJIqLaFGm8lSWeqNFYhYy9SHbucRlrqSOHeyhnkaaOcBhjnCKM5ynlUt00k0fI0zQYcaaAxeERwC8+PDLd37xl/93KG1M4piAJCT7uTz3rwA6uNQVgBQyKEOHhT5m8LhiRs2E6aq99qpFpzSrifYy+rdqe9hO27HL9rRNH3Hr7tujHJ3RGpOZCCq3uZODejl8R0VxGchV3spRf3SThMfm32jv1uKQPYH2sJfBqM+CrTanxfP6w7e/SgICJ4cTj4Qn/Zk9t6/3r1VtZ4x0FFghIEWHjBYp3HTTAr58vdbjtVinRqWsMJgfvueuC9776PRczGHO58y3Prnb6+hKhm7uvHNWWm7HNoj9ejmfjof9bruhLMTlfDY1asSwAf169ejSod0Vl5x3zhmnnXLcUS0OO2i/Zo0a7LPHbrvU2KZcoWEGtLVz098pSovgjb4DSKbDBPIZtiIsQmFEalVmA3pYG7CBFmXbVnegA1jZt9UD6ADDE2aOiZFlY9FlTp8JDjPe7PFkC3fFsHKjxuw2YdJe0z6mvq3ZlbUCI/Zq8lmLFj+dMuHXml1thDRZcJOMBx5TsGqdcv7XOjRq4VxNssA0bFKmaXOKFh80RJmnmMGScjiSKkkztc8Rqzqt2/A9n45nYg1CPqNgUaOD86B6rjVs4slEiSAT1nJ8vxChoZExyhzdNmnKLwICf6xY8XetgRvon5ev7+R/NuoVIpv1Ch+RQoohkSDBgCl5rjyp8paFIUcvZ/0WnRm/T0MtmTxRSwc5lNvh2vhcO9f5et0AdiBNbjILg5AGc8yxziKLNVpiiWb3uU+LBz2oFWOcOnWI5ZbjrbRKmzXW6LDeRp0222yDbbbZSIhuAC+5JFy7ThG6dYvWq0+MQSPiTJiQZMqUZLNuSHHLkhz3PFRAQKDEqjWlnnmm3AsvVHjttUrvvFflgw+2+uyLbb75rtYvv2z311871n/QUDvfgIHILjAVe6kxcRGHo3EeAswLl2BJuuwst7dm5a2Lp9y4f78t9w9coH58AGJTg5kX6uf8F/wR8Xe6mTastCrqx0Oer4zLvcUuCb0OUoCzNDGkAkK2LbwY1atCvCoqTwwMFZwVj23R220FpeSwmN5ETUyLOwo7nLX+4oWI/AVhuBcx36paARsGouFtKj5cZF75WDiQnRio97jhh6LA0Pi3MRzyGTsk7YKqFWOWkzdFFRdK9IlzAh6D4pTxgibAejn10tag/f3z/2yOGmZu6ZdNUKcXVavegUIKgcHQ3gFgCzIiA058WCEwgd4AWHXYEgATLD4LHvW7kldPaSXvpDG4AAakB/bOmr+8xrMG8AEx4Yg/4Q+8eEFFeAN1LKlGYPioZXV53F+X+YuAaEDSPFU5ZgFV4BIWDPdc7XxPbzBjBip5ZAj0aKLhQlM0qVrv+vo2rsGDheGRlznbGDbqglMaUoEbaqHoFAiTYXAOQoRoAZptGxw1vADeJsrBJ2z1VdZFjEAMHyo/wnG846I41AB361XPrpZDHl2A1bBow1y1gfcr0D2B2IhzL4hPSI9I8XLZOuyg4si0m4GHBfWOTTSJCPRC1MtL9sFVAAfmtMrAqghf/CVhrQ6odmmvq8ryiPbRSn0bz/HYRuD6IEKSqA3UhupfHO5YlAHxqjylN08+tYsC89WlJiKpqgfhxcGOrgAy1z9IDLzoIyNFTaXRdhp8oEyL9sXFCmwCX084eNGsGcdf6oT4g4SMyt7HFV0imSI0/zYayMHNE2/0iNGIMemvNbgXBx+Zkz7ByvTyhxId3O5q4022fdRiO1sw2OOyHYUMfOOD+dSYG+Cnb01rqSNABlcN1iUBnT0eOIZjvXlgInvc7B3dfJ49arIL5Rw2zwA85+pilzYMsQSJEKQWYE1JrjlDbGSHJ+dkrD2loRCghk/9TiOJwZh3p4gEt2QiV9uDmmtgCCP0p9R7v//nt/Yg7rLN3rkbek3qQDP1ACD6jWKMFR4MNK8idgK6JUlZxBXmOPm1WajW4aV66a1yVWpIo+X6mDMKpLRz1ku59uAaTxAD0yjxOqXUc5Vlx9oXLik1VkvXFyeA5rC5ROAOV/JvzmdUDTvbMO4Dnjj4j1RveA3R2WOdUUjle7vVrgadTZUNVGHQokA5PE+iE2pTs4kb0gyp25zzWtNzBHkld41wVTTlv6s7XNg5j7w3ZwkU7KMc1/yckfhAzPRA5fTUBZe2UzVafyv6UFZVRxgoWiXULMwL+s0GZDGHYnvXUWzrgJDW7aN72YAuvfcNPYSKla/kUNvjgVSbLr5aFanwqVP+zkG3XtedDbafWtCz+BkyY67gTlcjd+56VrR2uWvWNUS/pRBcYjy4F8Fbs59zt7Ezj9lllID91vr8AvhxNOKH2vDDJdBIE9KEsBgDJzhsGHMrlDtOxe4p8diDNDOBFzEerKGKLKHBuDrRM6rGBN7PSAOnRldgqyIA6FCfoePv5BcyaYJVzNDcasUdafT7ulLW5L02ugFUsQ2NbCakWBHTvB0dO8YxcMBhZoovt74h18mnyZBwGSnIbkTYCDbs+BUQGfuSQIdP60huQFfFK49sYjCFVM9k1n2ZiMfQWc82yFsrTTw6fJ+tGmsOuKoz9QnBLNtREfGWN/jNSmyFjPAXLsmI1YJAgz0w1qpTjolR/TdyEJmfdh/Zwo4sx3Z7bGg1VIuRycaHLHljk3ku4mhPwKD21GgBnqrek9nr3uGStyb5DC3lh6a5naRupAQNuCVwLnmBO5dDu5GxIq2F/iSJjpNj/CMA4drOJDkZB0vl7RLIhcAaLcTYkSKc+RW1ZcxQ46ylMlbqZ+w0JKo5I1jcfxF5FJ7dTL15d4hvTrOjn2vjTYQewJSooXl0i1Ic/Z05inCGqkkoTtADmCU9xgwP3YVDtGsXdFXEDsWt9tv0u3eLncYJOMUIFxDmE3Ez6NngKayewF7iiZ6dEtMKZ/y3feZM5y5qQ2TJZnms/crVhsCZXH3VnRcmiZEL4mKssYvLbE7uNJJuCF/ivWOH2hJ2gQu32e0YIjb4Xa+UR61Sm6UUNUNRfosQ3zxoPg92iieqyQ4rKrWseb2SUa/iIGoT9tbQ/tFQOaFxUKATF+Q3LUdGR0Wi2S8eaIvGRZRLsiF24XfPgJ3MzkTzDD5eaKwNtTlDxyJk5MdVLCMpk14IFyykiTkEhmz60yvAgk7KFxuglDJyRgwJvAPvKWQth+CD+zLHEAwDBrRFQQvP3KeXB3lJ1mrT/eVe8AJUl/xWnCdyFkISEnxjxMF3q33W8sbdmTpz4WbdbD8sn6zrJteAU0BH3/Tao5ETGqoSmPeahk+maZwa+NQwxYaUrzEKg1w+x+AxLNJQiw6w1y4uQxbbGnXmskassKakoJt6rb16gMqpEeK5LAY9fhQm+TDxsGuabJUcyPqG8rC4oykRTcfZqxqjVA9dSO+Ub/aGCOpCnneFk012fGYXa2oP0VRAVKW9ou2JS4tUnue0Ri+xAL867YIhCKvJsQHP2lENQPXzEo4bBkYm/JXyatt5u04uW8wIWmRb2EMM2MtpKN07NjM5lxhV8Qg/09NINseZ8qsJrILMK1IoZm6pgwvbCFa7SF4FmFQPSYi9mmLy37YAuONjy/EJkAxST3jBT/rrdIGwTEY+A0HOXBgaXvmnv/mHEMXYKYIkxoLds9lixZbg+2zjJSO2VvAk0D7ePqVbUW9Ad8mF4cyDrmtMApWGRyoX5ZSCuBVya8NPnqTjdMIYkMb3M/pj4KWkUmIRddKOY7apxUE2dF+8p6HNgAsrL2Gv9i4HIBnSqkFANFNYp9gGS7pjAM7A4BLJ+5/7w5n9ZG6MjdcH+NW7hejHCGHYIWvklMjpIfnPqMGQTi9r94Hx93IX/vxrxIdOTdu5toDbq3Tv7FMRwzqaeHLij9WkLIWNpQKXiAK/yylhrhdrA2OGVV7IIVPQ1Q0pnTSSC4qznPWKNdMp8jbrbHPrydkrdGYBnqU0QfcibhjA5jg6N9umszGV7WA2ZoMT06AI2d5SS1i9opQuYe2F9btS8rkld2SLY8RnfGvTKqKeJftK3+b2Yqurp4aJRVhq/w2Rn0W9y0MhnHPNtrFID1vNaIg2Hxbrn8IfRJxCB3OTN4p4jz3lUodAez7PnD5yV0lTkmtYhafl2lJKsMgE3JiOEwFFmnP2BOPKNzyra4pO8F1O/IlI0sr2EOsZhw5ziLPIz5ilWkz7AwtoiTME3SYtlypzwkoOC4EJwBmV65I169BJIzx/KW8pWcnGV3bg2VP65mOnlvGJCk4sRnDekVXxgMdLi/cMPFISrCEwGbMhYEvHXBMTLrrnlJtGlogxdI8SI3pk462cKh4P3GxqjfPcXzM0tUX7NUtJtxkyA5f6UErhO7YHFi4iC/nXn0+a81TRuYDceQf5aR/i5k+tmELuq+ivpUJLN0x1mgK5EG6T+xoXX601ZNWYyTh8JhQ/w3C9/Rfjp+QHEXS537305HgAzTl7lMFH1Ybxm185wskDqHU7fu3/8EP2PKP2Zcc5rnFgXKMXFeECCRTH/22iQhnryCQm0rEM/5INCwYCEmC8ki9vKlLOvWbUJoTYwpxZNHKdpAW4oUDmqhm3xaANJyyaFORKlit3/J7Uwmpcj2RyEh3Hw9DcXo0aubIpYVO/y1fXBCFQY3sfqBIDoV8HqqqJwVePOvJBFcTVcAwbkDgLzofyGvuwBVLVRNOgDTf9Dh6DFBKwDZVdZdGB2WHMVKim7gGZwzfvSHcRaGWxTVi0O8i/6xnbe99j9+fhB/FBJJaHHRjVEJFefa5YuTrTgTCJAjHUXyqwXFi+Mj6Tlz4yPERkIjDysMEIvSkCJjZSUQXZ4Z218rc0b6iAU7j0RVAiy6SSrwLhxxoGo86qjPvQ+oOkB6jyJfdnA7ocsvbrcVVVuoa3Ftv23DwOq+gKVDZgauVyaW/B4E6NhLKt5pZSkpX04BxEcMKldMA0twqufH/2e0gwQWyGraQDEbo6nlBbRYw3M+90zV0DKsVHbEnkndzY8H5RgAR5NwQcENU4aMiQkAc8KbSvCxI/vmRyFXc70JaNHNXRpjQyiVAMG4SoyAMroJZJ1Y370QDL+goN94BVuMAQovzM/5wMon7cDbeKu4RREB+7MQCYU0eMJFNjqkzdpIF3NQ5yDmhQJiGYAwJhJEWo6OaNv8j0qk2oqIzlhJesNgptAbXMGbKTSSmxpfTjlUKyGMmRQRTP5bTrMjX9I5aKJJ5HQcgpO733hsyQz7RIbIsVfwWtryvmInBtYCb6nlh63fACfgZuVJUzWlGmG2VHHAyowVggD2tAxKddxESnB8ZnRyZnD4yK6gzu3Dy1n0PG1uLs8pxRdRgYIVuvrxeMvt7FxEpvViTVlx7MVCgZXjPnMm3YeQGGyhijWckFbhADDbTYvJHOy52lYL/u+DzbMab0BO0hyu2cugi5SlItDBioeuuLa1Fh2WENbRLVEUlAAMar2aAgugBVdTpw7q2CJfosqVnUBJnwQTzMpG+wodleUtpcEVwcKC67wB8/UzGsehLyUQyg/vU2wAiISErvLxHVLq4PGIc4prVujj4CA7iZAd06hGjAQMs/+Tsja/fCwqBtxzeNOoO4enBc4/L/DaRFLBmJ6ZMsGWRIUqU5a4DWwkDYjExymHesY7+NxPZDj3JsjzmJkdqVqw07vigbBYvYAmlF3XTadcG2JGEfmdq00VWM7dQ0hIDVfrKROByqhhtWiOaQI9uyNUrI88gMFYMbs/OqguIfl9JYWzRsL+td9Ea+AeL8lMtg1z+sF2JU1sVSXTXVQ/qcknQ7qfkA0GMz3T8MIqCXSVvp0VL8NyPG937tr+PsbPrOPNjV7qvT5o2n+BfGYMlk+TeW8NNNPfNj5hn0pHKWAUvZCNhoxhb0OyvPi8WIzYCNVQyql4cHcAp5/s15ufB/cZY4vg+Nk6eogfNS0+oiJnqzX0/EGCoWr3gOWFaE3dhtNniUaas5SjxrjVJrZIoXZtWEVNQAuLXmQJttdg7CGhfOQlUoFiviSG2OS+L0eUt3Ry94g7lPJ+WSiGlqEtnDT/k4JCPZRGOFPtU+9hgIJXHKXQnWBi5bqrJEw9fxzReezXneVnkzayMzdNpJt8QQ1HPeIZLCn4kIxJdjBIYUgq6qRWBsArnVhgpaWtrnLu77VwhxYLI6l405nc+01fIcRl0eE3LVgTkhWQAdmQNHtcsyWVfnmYCw2f3nVLfL/+kFBygG6hDXVVXkUuOxKBFP0Ug3LMKCpSFkU4kx0GnKTyeuH6gpBmhfAN60FvnhOPU+ou5riNvZVPr34DKMzOWOlCFi1sBW7BC92ObtDqnB6Z3x2Na7JKDaWmuILEvVByl2+T6WAjLbg2j/JelE7bgZanM6Ig9SayAzRyVz7PYcmefSa2HQ+rm9rcWZMHDhTgHWNciTNVYXxpyCWV+63uw1OiTNL2Z54HKkcQTh4BCYTese6HGvZDpYwl22PoO7NTXPphDHiMSt5vzc/maURfGsfPBZ+PlhPhkjknNFN+wlgxkMz1MC5bXW02fxzLn8gNpU6bZK4orBFApdVRsNS6HEPjzsis2KO6fK5ov4/WTOPkrWzUZcyoQQrUq4C1jrq3uNgvGzv+cHxqgE1TA+hsZHFSBR4/onyQigufiQIip3r2wjiX+z6lgCdLrTEDSJDWzcBFhCDcjPD4Fn8L4i0Tvo6tygETbhFO6AWMeLxiaJz7ZJOfa/jPcAaJf2+uRLQ8Li+fkx+R603DN2sdJ6MSzhZoV/EK4tZUWVK7uO63rq6mMSEXSXIGuyyAfwyoTuEDSmGradq0cN+GRix7W1U9vrSeLvdDCV1m5tRWWVmNlaWBXkSkC7bDO8ingGfoS1NVxeka0KzyyYM2iJ7QnV4eY9fPqqz3se9lAMstZ2g332HVwY/gt5p3agdwXx5c3sgrsWd8JIc9s9VeXYiwajmLAUQGqH81RdeaBKLqzrGhlwJg5/BLFtkGnRZwCouoiVWnw8I2ycz1coHEDaYG3Is+9NhzyTrpp59TqdpR0J6u3FGPPfaCddXElo14sBBrKmzxSt5k4496ChILtICYFqW4g83xgN/cib3k/uWivIxGpilR32q+SDlXV218LyZvxoArWTWTTNFWOFvQUAkxijP1ffs/CjnyW8hkHknJG7taIV9Gi1rNeZGOgwcNDBrQR/t2R631784k+Nfg754xx/CMvkQtTOjM6Jjy8SQLkl5FESjjl8cTEuhdjxDUezPKM7DhGr1F37PS7DfAtXSdXt38qQD9zqZpp/iCgF4bnB8p1xIY5VCAOSTSVLtp9EsVxz2IBEvgZk7TFemhqdYFmjnRGafqr+Sn3VKfhdXA1rSQPzemKrc/jNSWca1pom1olxeN5VyN2kx96+zxW57giMYDWCUJGhZKC7CVCNc1RBwmJ4fbJ8BVhzs6ndxPoeONUay+9gZZuzN7EOxxGg/SFiH8JsTn61jcguVJvk/COIX1zf3bWbVPkYAu8OMyjbDMNROESOvMy4EBJk+GsXLCe4s4EcKIJcInwONALGJ96aGyw/RHq0LJgrcmAZDcwUF32Ek1dEb5BHmyUdvJsSegpChnUI8wSBo0yGWQWuuWRglrC/W4RWzVMEVoAwRoIPbBTAQBh0F2taM+t3cDqFxSr2UMIYA39N5sJyhDgrHZMIwnKo+Ce+phHXKKH+yucVWuInGSMEi2tJG5UH2vsw6VUqysi9uRfL3eLFdq0m4eSG91CB98LIyESx3QSEGWkXQgV+poGDGAUpqAY4Zavz/BIQCAgDvnlkEH0mw3CEAxfbSAMMc5UC6RgBPuo8NjtRyu4oeLK2LdvuggljhPipSIEyXx+jZ5K963BS4TkSL+exNUItSxXgYQr5Riy6ddm5iKzcYI0huva2mA0CvWtsNT982BVdScEFzbiox3dLm7x+sFojIVsk3O+JlWGogQpiyM/CJpHNTVQXq11jl5vkRC4l/5ks+zgNViqIucuvYWTD4nyJvc+PR4M3f5lv/v0E4LJuNIuH1OxcH1tmXaDy9gCHeTgCe3uBb5F9EvTmnuTdGVPoSGYYK0BvNTRzsN4RSxM0zse5WJZwWb9ew+mgUckkkjBXfkJ4Hpjgi0jQXcFsZvbtR9HPFVR6SVcUMFzyS4/I7F6UEwp75Jkow0ExhQSkQwTh97EpPyiDGrQQUSHcTF4cctlsuCSqILKW9FoZ82M9TylkYkQliUv7a9GvAl9MvhzNkO4GMOWxkfA9zFVArTOH5rhrZXH9XTLag1ANbYYfzxHavx0RgPrxIqh4ofPhPgXUMAK+KzUcQ7Ohj8TrytIOqrTVY/O6aH1xlTWmkpZYXiF4C0xkCPtTBuf4sQeSKGF4w0FRhW9oTwpP+VF+22zuhxtKd7wrmIZv6zeAIeW57n+SrsXZQmKwdTvL5pReSelwnhXmcHUxfVcNgD28JjUOpFW2BtuwQxGW5WGxvs3mCS9lGhLkbwHsALG7BlE0FMlxjHq9AgMSsIeQEuYDP084C5vM4BqmpzRcGZf5gFLlW9WAtkLYGkH7hAw91gKzkMaEY04ikvAuWy8lhAOYd0uQ8HFvUj000C6/qXyia0MFlNEpcUbUs5THr+Nu4mswt3z9l8Lisd0yVJze0oJgZDe9VvVi6MTCGB07KKIuVi+NOIupZ2Xg9gMFvOfHcfvjb2th+VJ08WeyilsRmK/ME16ArNwybVy2uW93J1/n9k+uWcFXJohDzrnytRAZgmA5/i8teoOV91bdcbFrWMwKsH01JDjJ+llBmDcwCpFJISHmpoXMmwOMfB3se8mrO1QHQ4Ccn3/KZcLQz8szx94zPBNXSuzdrg5KJ1QiQ6UWIDKjlAZJITeD3knCp3pNgSx4VpT4OyJ8Cav6S9V8W99KkbFpUGcLR0wjEzYbfmuiVSznshY4oJLV5LgR+5xmEcBwW9KCX2OqRskJbDv7rRawOwcIr/Yn0AxWB+L1g5EfNRBcmYazqPNCBrxQbSUKQswkYCwIQVMMVxbnsRVjCq3JZivy1owmBVqymOTrioGS3pXD3gAOoX8aJy/hf8N/cRD3mRB63ZTiEq/nACOQoK7Ux7iSnywXdXuIgpAUdRzdtSmZhrwObVWmlxXzG3JtNKpWNZXnvBvnaxw4G17VXs2bR4oJ6ntTKDW3nsOp56c54MIBMFqn8g3pcvNIoLoTUCdVEpRBvWi8au7o5e6mXukNUhEqmJABdNG3pYKnNABNq/kThneDQOv+WxiIEEisG4UVgs6ngwnMqtN3d/LZJJc3bypRcK4SaXzJGZaj6Brfp4ejsPdmJNk9MPWpw2uxCblNLfwAyyxctyfYqGIXkeapTpq3+Z3rTMJNn2e9ijAfHyrB9gSPGUYPkP+G/xp4d7V5rvcnvyQUzKsARvOLbk5VJ2+0vVKXpPIDshbpOv4VD2nV3CiSAF2GiEZp92k9YUA0C/SBlDK8CJ/1+hDp0A69Qn3JHFwfAQVUI8EcPguq/2ghGJbRqG5rafkemfQHeEQ4jzWZArEPwJcHQCrLIbqhQS7OKKSXXHsWucRmrIdcIwqjC01r5ZBWeQTi/wPlDPyFKHF2K3StBFgnJDLRnEwI05hUosYPveSD6AiECc+EPlQQGbRXQNQLk3X5/o4RUFWzCkC2rX+uFwtXYe0jpgTQkhbM1eF7CEdipupgWB3nrFP5m7NUupEL9XibqtDpLoW9ExYADPb7dG2f2CbLDuBWBwTr49AOIF7gSIFxQ97J/Y/jVKRlJMgMpN6yGyFXU9j7YJFMRDJzYtmqOxNp0+TLW7RrRKWbQQbmfYGQ1cBKo5KHwtTK5JVok7C5mEO4H0Oo5nH03etIwHEH/AYoZcME/JYDO9V43rwlLCNRDciQsqSsIxCoqKVMdMTQ0/RijQwmWlN4U9eHfwZ5ebKPYgKuxhSM0wicgxdxgk+eouPKpzglql1cQfGBvg5XOKgxk5PZEIiqgowsHTpopx9i9JoH7M7aueDKCNTlJOL4i13jBj2A2SXopkJQvNeoyD3Igyc9dRH1SPSUJYRvkPCi2uylCZfwvNLD4hrkfKXeIFR6MJYp9ko/KDZhYJjwtxRDTmnAct6UCUV1yllFeCKa5n79e/EdU6Q+MTIh9AlMSj+c7OOQ60UdYihDF/cZ52yv04G+pt9Eyf1kOUQYaBFoC6xi7BM39xQPEmHvyw6z0YpmLCP6/iJ1KnFTs45snQcb5PMOmCHe9tAyQUBVVOtmPokzXyQyJgFoTeMEPgEMRXuHVWGfBD4OIn1umGrKPMuC+xZ+Pz5/h8Q1thJCuAC0gJDnR8uNvZLK4OJE5GoG11GJq26TWjomRhtUK6PoRi2aJw3BwA+3YOkVtfAd5WTgA3Ga8ArU8f0SIHhvW7sY+ASlhhA2shWob85iJDw0vBsKBFdLVUpGz+gafTXAUTiT+mtnQ3cyDreM8Tu8L9Cf9Zdn/13XgsGX+42HWbTba7ltsqJ/SfElxBJuYr32IeIaXsZa92B/4Hhn57x6T65QSXagoSn8yKjDBHzTGnoMqpTL037CWcdy9sw0hcoCUHg4gpDSLAHlNsc3nLni4sX0uwy/161WI6emcBQFt05jh1wJQt2liNlWJhOvtMet7CuUwrNQHZt5JUuEjLiHrDMLP9+E+pzB3fZEHr/k54DVpuUDgxI75sw89A8VcBFYAoSXIByDWQNWM9xMZnDpN0mqdswBS4mDqqeGIPbB5Q6ENrdYHaaloLgM7QJmUbPMqWDo6TTGsnsbWOef9q/ZCYP44txMRNUSo9MsRZjbmVeGyCBWzv/tZK6enbLQuSBEgbgdhRzYLmycsJkH3w1mMhjLQ+WhAfYp7EbK0ocNfBBhhSUDlYC9O5SJFoXMAWAFKydPzkvbXKnEsNrn58MQMdJheRrI74st1bY/Bt9AuxCkRnv9dhC7dirCOV9E5l+up2tFq4ohElDbIvHfRTapU0MrjkcQWf/nhlgt0ARpNazHaFvyhxkKcAjU+Vj58rmK02dbLmuoXzYS/3JKmbiVo3KZZaDw/s4hrjz8Xn7zvcBnCsOSuyepfrcxd+EwPJjzhXy7O2l5qjT2NC26OnTc2RGFFuCAix3/kn+syznEjSn5RymmZpZekIguy7taKG1CmO0NoCyxBSd/R7FDdtlg5YcE9MLc5LdwxiT7HmCucg9jbf8fbhE2hvFx9s8IczxyKS2R0l6eXKWlAyA56cQhXriYwTagVye7IkN2ReWM80FkJnEWPF2er5wzuYnlHDmRzOssCYi1rT9ECjuxUw3g/e2EZ1KMCWnGWBYWJGlilDpoTqU1ddVjJEcOtoHEjgdZN/VVK1il5sEPw+PMW3BdGtwGFlLyWGX9+RHQJ4qXnyBTbO6elOiOAXaXh9w1j/64NgGV8+BKfsIGSL+/0KuOlTN+0IAXlKuvRktCPRb2IBJcjS76kj83E34Gnc/PYxNGb3v0y1kNQYuIILj6cSWmXab350mtlLZP3EM639OaNacUEceqtqvHXGqFIJl+RlVM+WsheUCVl1jZEjTpYz1bAFBVEkQ4iV+sAXuaIqsCq5slFpPaQiGlQmOH05uhCXLgOZLBfPLyBvbGI/Gz6I1iwpoi11Z77duAoIsqQXODMlcgNWtqF+2jErPkyVBo2gq2XtnPrq/hlkZs1h4rfzcZ7hMGEdoqxdhjX4akhfEaebVlF6lRhqrw72jkrslehNv31y6Iu4k4fnCSfMHNGjSh9vwND9dSQ9cXbWI4bo/Crv8k4tqq9ehV//IkysvUQMTrY4D2m4y/NmNff5LF7y4YvmLodlZRtYn3sMrZg5zXwntJyz8EV4grEhxEySX087cvI1ZyZnFX5QjuYS0riJ+c+iHQxHtSq+7wtGkOto353NYbSZIBocSZ/aO5ZHtU6W4j2peHLCzN5XFDFtNEg5nLyUdgAdjh5nZwo8I29iPcuJ9LAdO9g6ZU8F+J7UtF0Kpi0QzkjJvpbL2B4Znrl3XJaQwh1DxNk8/Jc0aoynUWblUNlitW3Ifw3n7WexeZgVfmMoSZ8iDJzmDg9b0YTwE5A+C4yZdwAIgtk1NrnCDLlSnJeZ43F4Ux8rLR7GnkuexecHgKBxRV2rIeT8vGrNNgUtSTFn03mSPhUCr72E7slXJ+luL2BY6+4ZbgEnVkTgGoeGkh9vhPF7ec4VAW0TKoDKyj1V6xzAS1SAJFrw9wojeBBAuVsj48gZF8uYFedYvlz8pzSzTbu4stuEYTnNO1uFNPq93O4msz+k4qrXIwjGejw6uUq6Gowbx2ENrBsAN/xfiJmM0+CNNRKjxZzZnpAAaxnUCzvLiQWoHC00OkauHJxLIRv2y+2mQBpDKJrLJetKLHlU68goKUGGql+V3KVChKKwj17/aQosWJgFda8yxXwzv0NDDi40KUOLfxL9lT+AYrGOORpDtJMBXBhQae62Pv5c/441IrX94pGIJUIdIjHOlmy52xrH4ips/0shEX5lPc+b6zoBlwewxE0wDCHkbZoAnnIZAsZoo/dNs4xW59cJx2H769c+qOR/rTm38jfLKfiLejp9bBbscnPmnw+c/u4e7rJMml9cuOkatHauFDymBeOwQtcRhUvOzigBaHqeSshPVUITOt+L76X+LO/99ToSw9Q1yxfaz7g9vRrcoSSubAQ6ZuqGKBZ2bx924pvZgHV92NtNmZ4BdScG7Wu5xF01Nkz7Euz3HarNhtV/AbUuI3Ph68KQzOFhXdXVIdnqqI/mbaTtil8mIxp76kLbkLYjm0b52cXIohFF4xPpgi++HfOWHSg0Cr/KYHDr+GcBA/FWv+MsuqmfMLLHbSJ3czx+mOp62b4Q2yEG9wHPrYz0jUqgycbq07ia133f4D0caoPhCllSXb4OXYmBOIM96Nct+SN64PfUMVrcTiou5v80H0C+qCq2GNz5SDPTBf9v7NLS24yyi0jmJXgKVa3G+YtyRW5J4EoX9cD6UKq3pWvRCNJS7mAJZnx4GyLdNAg8CHq+FIQkHe7e9P5I4cu2bly7UeXwf+jZRfDiI/naPK3tSGQLf8+8BeERrAE8q8kXnM7tnad6WHu/RKS1jKcxJwu8OG6XXdRQqiUjGy9bW0ysUh9AzAlRIBS+BAitoc2pEmmEOemOVfhGkszGDgsciPlYUveLes/oDVWVfvcVYmyyYZ2sXD7QWiXGAg+QLspwnosc8ICpstF9y0UAcbNYgz+6TY2uc1jZAXZXd8GRfYJLOJbpPHQP9pf7zaNRjnzQ/odjEMjNfCWgbI/NH6cDKLORg5LuFwnGWAEJEYsTlI5kYXEvjRol2ptqRlfZo2zVMC6wmEnNcbmFlu9Sr9LnVWQGkf9GZsw8bZVAdVhGKDAmRD1++j9Gvx+gIMWbwA3pZcLOXxj4Uv8ZMt9kDEz+YQ7jtF82vTely1/tOUL74Qixw5GiWf/9wTj73z2cfzirN6gk+2/2nKz37msSfe+/yTeuyF4/Ho0SPRNakDBdFO0dIafIVzQCvtpdh7WNxYIz56w1X6nwi3uet1REG+02Oy28/0syFsf1It6TbAZcCf9ympwD8DLw5qbNu+Fs/PyE+4N+dR66nTSkGm8gKwmIsEocHcW4Tu14M5YrshS0AAQpH2DIFIbywfMi7oCtfeSCRSMSqMvqFEHafBBv4hBZ4WuaB8ZM1kBmdBHJmlKrkOPHp5wkteZJR60tdJXnglHB7X4oErvrQqBCLKrVxHPeSnCA02FpxC61Vx5TKR4rG4eoVmA6SVSrs95qxcfAwhpg7KVJiKrTYf26Nl7OwtsTeBMXvgOrbBc7GiL/3SDIzP1p3iod5hORwFbpMTP2TcV1l37IViole4XpdP8OfFXiSSMJeaetIY9ddsvQtv27nlJLUWl8xI4qIlh2msOHRyBgpzfiyLz8PWEaps6JLxkEVy/cDjVrhaBM/AMLLFR3UUVFp85I6h+T52tLNgd7g9WL121AGkdWgAP6lYiVumHaPSQ50lA4+otet6wK1oCVBZpv+7/OusX6xgSNDaSP4UQ0qlL9cixVbao/aAD3ShtO7E4Hfy/9KMZHZIBCng45ghB54sQKYcOT32L7r230px8AkfFVi2l5B15EcOZXFAOH7R3s/oN7eJQ8mSHBn8t6VUbuUPYVjB4hdzBvz/LIKpeEV2cLONES5mmkIaX4BMLoWRvCeRWo75Roc21rQPzBvZ2y5cLM1S2crA8qS7O2D66NedAVFQ2EOP/LMSasyU8lCHrZdn4CjhSyuHbSJfrvtCiH/ZvcJd+w7VnfYN8Ij96P5Vb952mqYnS7XfMrsUObQuwepWjxEE/OeDn21x6NA1O2OQH9Lcr2D4BKeoY5mq/8X/2dEQZ9Xu4JobPhrk6leDEJNDD4+IzdX9DJq85UMmC5la5r6msC5reSWnIuqoBpks6Pwv7yM1CPmUuoqQ22RbkchH0jQNuSFCkJie+4GrHCegR2oQogmDrUaR7wW/AF3wkd6qWnVzgD3ffiO7fchb7MCik1vc7lF/SmD+KRtY6egZnAYv39sEhFnxHXEwxwTdh1OKGVleuEU8EZhPOnaJzNCH97NopF33MRtRAQXhbiOEi91625ADc9GpGfy+OfHMQgjVAYZPHslDPDSI1cDOzHt51bp0dbNMJoJFqKKTo2CB/Qwrbyoj9E8C3TaqHn4EgXS/RDaS4WDrigIprPXN4nzCGOhNfjWa8OMfkK1pqThv9RYpI4cqz/te/8Mf/CmWRHUFxcNtrd1rodlMRd8HuPWGAlWtupe57Sctec6Xz0DAQBKYCs+x4BusDXZH0AGGX4Xpcb4J9m4pFVtnwNQ8sLnXhBVl/3bnjsCZWxnrnKe++J4PoT/IIP1WXpWMJAzSm9nHRz4Lo6/itR/tH1TjO+DD+bBYeI/1whckO+QhAEHsk73GFDaFYXInhn1jlDqPUc25dRv7H1568/5iAypEVgi40ildG9wpMZ1MVT6UX/jFi6gWZtEKGMigeVB6mCTHunP4fxbB+zl268icxZ0OVPWy5pirPvN4ZrUvGkoRMVxFApxl/kp5cmSpYFJ1Udh9AAA9MuqzvDQMPkInArfO/y7TUIjoEp4TYDfgFwKo7mMm0TfUCkLWSS35LV1vx3FqMHAooJrkRrCL1tzbRZGUoIGlxSyAnYeqlQLfOvBov4QSzFFz0YZ53lLJXC5BmfjiJ2DXD3kvS0/sJfnR+kvYr26971JDjZ8TfgHcrpMTL3OxcdO/fpOQ9a6H4n0cLQ/HsymRINbEE+USXYHsUhfIQnlPuiTQjys7Jyhc44ycA3mSaijDCTaSmEUzfE/8bn2RzDdTYQmlBIw35wLvp11I3b9istDt3LKIvNhojhDOQp3DotAu7x+OQMcrcZp5rq0Hq36aaebT8ZlV8AJbAxzLkHS5NAjjSRroxkTQ6IiEPCmcCZJfdUt0WWlf7J+5RR2HR/N2ztA2W6f3aSQBdS/N18JHBFeLxIwz7V8A7SudXn7iVb9kNuEb8hv+EsIcT9TB+l/ztRm0kmC3gB6mcGHEH708ueI+Bpf82Thv2ESBxiDtxgHB4VgbAZXgMcgVSOmE82perooApnnq6WUIcQwd0S4gVhpBDGWZRikoIPTeLni9o/yBKnGduU58byBtusLu6d1pGfaYO1ddd3pAVei8ENWgFn98kRADBCoG8/IyfWS6f8Enf5xyBRFg+PgrWoCHtPfU2jhV2m+3zzzc9zU2GvyQYZR7pY5Nntd2dHETvvGu6lw7TzDWje/xafuLzreW/Lzpp3X7dZ+/YLR+F6fBu8DyIaX32pC2iHJ1nT5oKi/XXKpY/f8Ghd2iflhKQd68RABRGCTFYdogxO4/telMfgJJSE7FxiMsSueRAAz/eQT+q+SDxqV78Z1SopmUR2z+KW8cxEytCw1YasrijgLGcPSJ/iv+2PYdghTd7peTS2/OO6ryqpeGP/axXPOGYAYjMhxiH/1yD7K5ExxJr44tdP/HcSIe9hVdOwMMg+3Iq0OX61swRZnRH0a1ADcMBKo7Pz6g57OpHhmU9HHKFGPmDgIhs+C+a7zZB7Ed2mAbbJaqHgprMwe6XyRxatNd0liKjMw/wtpVyuZbicAlf9mDOyXXWN7Mvr/mLq+Ox3ZDNA5YZ6442bZ8Xgn+LXu8L1ltpjhZcKAj43/PPA0HoXqt9l0B/JYWqvAvshzKdZdavwqEQscE8i1Ob80QAH2LHyC+nzjlbN+DtCJKJdjsrlPXEU9kA8vBQbKsF7GK3mSrd7Ac26oPku2n6ddPjD/aNMq7QD0Ewni4ouPXXFh+bE51T+k/pBoa+lfupkP9rXV9kjajiv6EaZf6aCvF+/XbextwD2B30cL08pjk4TicMykG+fzUTqaw/qH52D+KDwAuH8b/Ffo2bdgoPGzgkS1bF9UHcTZL3UmlNwcvImAhN16f1RW7z1cvyDE4LB8DFsD/JFwlirXb7bLLfoD4ClPy4UkKKZSBH+oD4UEdWkgdGwM/tOh1vz+ZkQ5l/1hacmmzPCd3dl0CiiOAVEd73viReWORnuBpf4QoE2ejU2YFGFQWliWiZdLr5ww8IPwECC95yCKRhAeslIkOKz/71kVjoa6bEVOdEStcHaJfkIxfo9TMqsw3Is2grRUyDgnd29QfNtFi3wX2vF8oQFpuZgLrO9t/Tfkgp+B7Wq4+oGiSygHIghVck3QzSTI+raBYt6rru6dtf1lwGrwsnXgKzqlUL5PgVx7Ej1KUPPeOB6MpENqWbI5Tq/ggT9j9P6bSwfEyuFNLhKStnMfckttBkofcAU2Ydkto+9nhEQhxzu+iyggax5xK+7/Vn5NJKQa93C7ma/9gqE1quIVogksXgkzFDk8uPECG4/Zd82Zqx9ZMe4wWohyQnjtHTCUVnskUewHwaLZCVl8BhTv7iNcXA8M49TCvSxP1jYfojmWbJca4yOvpUQ32jZC/OC1VGg3to/ptLfvmOQLhsV1w73TPo1N/MNk9pmYGWFseRuhW3HsdbGLAIlpkkDs2j8X4oWNU8c6cvS97R1a3+Fn9BPT7t4tYNSyripW3dY30dQ5yfKX/3XO4SQW9nyf+oK4p0dcxEqG2HhaDQp4SkQy7IFvjjKtsVyVPiT9LRtyUn2R5BIOx9fliHE5d19UUDyORTnwzn/kiUMJZwNigMoyoY1/a7EBO7YSBoeiBfWJOcbRtf18KPO9nFf0+oDEIrLPJfPyFB/GjCjR40ir2bdp+y3HthE6spOXU07WMn/PIiLJv6EpDz9KuvafKdTXGWYxLnw9bNiuI0leqJETneAM9Xw5zifIhduDBysRCM7l7P/bTyO4B6Zg56MEwhJ6WAwcZhREaxlnPvrgWBbMB2+6OhNFzy41lYuAsAVs5GWVptAqppjo3OJm687PvHGoe3ShA9sOXLiF3UpXCRYpbHkBDw8WVwxPVwFIpxArVZ0xw3Kgt5wfOqWVEPdNjI1B847+WhTppEclP+/qW1FBaXxb5W4as+GhwQtp2U36XgsUfELvAq6PeetAscOIN2X07TgbHeo4EBgVGBb2f22WIeT4oh462sgqtS/66riMf3Ks4BmpfxDqZRNPuYDgGvzdYd8t6BaZJ7QDAR9j8iJSGjlUthqwEP8JAoK67Aqw1i9CW48LLIYLZMfseCa7mJVzWTUcbyn62OZPwGSdt8iW9TTGqcs778cDUuK9iL+Ipm0EnC8b9AgWdWpnEetltUlmJ3IRe/oLlEcFtBknqmWxPcREsSRMctLK6FPUDNag1qXpK4her/Jovoo+8VFlnhX9BvhtBMbgDb0UZWAjZ3uMsnHbf/OzzYZp+eRLOuK/Plh+kwMbDfGeC8WaULX6k+MIYmuWbjwrt8cGGZlzeF15G3AozX8vVdgfdSk82C5y0+tF0PicmMl0AOb27Ued5umAatS1hfkjBR6/Tor+1DsflFwEBg+bTy4YETX7ltCMj5T73OezwRU28sZyCbcUvZD/7AhWORSlwsEg7WydtZjEPZrJNkvyXG4HhdOFrBcZA6dxQwnGWfaQSdUFjzc3WqZu0yNcCA3B//Uaof7sXjIFmtwSkVH1SC2ZkYhRk+UeXtaPHicjadsgM1VSzYZ2nxUBtJXive4xUFoEdf+BcTjJWaBxp2m1JL7FoXtZ+loxj1WTUJlExEnbjyo9ncEzK8X6fc7V6+vfLVuC4zyOp2Q7pxHMHVLAXTtERwcU0NIn+NPEnsJZXD4b1ZbRbmjSC1U2umMzxnuCTMZgCIMoO4ML7iBt8zw0S5JjeTjTHhIzoe9tki7n250y3VQrvjCib2EkV2a6y+dR6d8/evusOWVv9/Pt7m359qgxPI2pruk+RA4puUpcOkQ2CHBLb7BFAyxKhZXaP2ivtw9LaKC8esjdr+6B4SPs1xVnK+8tnvsMVUzKyv1g7FuPmMjiRphuprXL4xP+8DJPUQGkdgboJKE5oUJhS8dywULKQPI+JPmfupRBw0LLMc366fh20AEWOyFnncYupA2ziKf6qwMF4e05Wd9siJfQi3/8PLAGKa+R2+QQbfF0242b9uzb1ij6bRDcomQPlzogHmv1NPH7S4Gxx5rSdIFdP553hwp2IuME6T2x8dPPZo4Ib7L9zh5ySf1n9EH9HKjcLu5n3jMHfUYbPZ0/PiTxgEBp2WgbGJt+zDQPDhzEPbrU9OxANenM3vzL9dPUzmco7BuXk8xLPnp4BwrSgKzzR1ri0qNwNHwdsZOMitYyD4k7zeOgMkxZsTKSwbxrm7wGooL9CJUqbyk2ablBSCr63Sl6pVSgyR5BjIVhcXGHrGYHYQnhyyjjUY7xAOfrbIrB2P2HBs2DgzgrYHJdEVzpofsQlnp7h1Yhj3TIxicRaxvRJ1S08dg79H1S5Av0SEPylytRTH35jq3x97G1980uCsi+nx/NCGY2SwSW2Vnu/C8q4IHzDeFEJfoEvTgdZJvBiiZGZcE/FXjyFLls0blWP+zy2Jxv2Crgc0bsc+eFzqF2++byBpnVdlgJe19S5inY53/aXDB0nQVKlmir4njOzZJU1xqy1H2vUODsMrJnEoaPWoM2Ao1Q7D1BXRDAb6dX+TJN/kwQQ0O3uM3zEJPK8WWXhkRX9Y3lCfx3hVeoCva83inCoYDuCgskxJRV8X3frJ0f1+SDnXsIX/K+8LXRutiAREUwNDjqYePpGdE3YE+X1i+MOPmItSDZBFgJFY3QHsWLqIF8egSnEEjBqaQ3v5OsC5P1Dffzdz+yjJsPfJSMqmO/Xvz5IKWf63d4ZmWGL+6IO7O0tzsbBl8yfQq/srYUi9PNwUa+wgZJADcv4ChN+z4oD9xnc+TjFp1XHO0Ts0a8iCP+5c5H0mDw1cR95ntuz6tMRvnM2pTHuJG+zmZLggypMQXt97ETHiKY1llP2NijgeEgT3Wafq/7iG8YIIaN8T/ujhQ8AFY4oKkoWCA9a6FX0De9rfLq60dBVJYj9FAiopvIVeMsenYXIauEsKwbstDbX6/OSCPewn2skNSvVECtPGGkW2YA2Mo2VzhjD++npQrkutkIAm7urD5M3fjCtpOxMOytX9dLlKeEP6APEM6MQgImHuGb2Hfa0bY6GdNfaq4YLxuNbvZnfictMFskDeNF0WnGQhv4SOZ9+/OzpJ8/yk/HUdCRrg3bAt9V9ByCjvXyKslQeVA0XfdSMxd5ViUsubRZ/n9mDzJP6Iti+4rz5oB/d/o95BMaWtQgSoO9NnN6WNcT3LaAUJTZGNwgnpHatuGYmrcS+v9aUpOIWtqVeA2txXRR72eH+ZuHmq7evhFfz1LvKYJ5nRtue92KyIUd7Ln4K+TXEieUI5RFiBFafvvKAU9POPt1R+yLqbzB5X/wC+P4FqU0Boa/Ki/Gfn3t1h0zPCY18IOnFieL1jqMHdrOosTA1bg74ZdlzF2XpJC/RP4XJoVs5vzruJw9m4fvZd6INpx2W3FKU6TkKGebzQACEkQVAukZuGytZxGvLuCKBKu94GyHbue/Edf5oV3LxQo7yb4W6lrOLJMk/hegqJeNSe9ZOjfpIcEKJT+4PSE5Mr6hq98ek1P1Qr9VuhDd7d5P7zjKM6v2fxx/+MvrosklUebRIDQnELxnAQ8MKu7yx+CEZn7T7wxcSzxD1cwZwYvqWXw9eEg7kb3UKARqF1lzbUH6tQWFUaaH5jTUamx6PrNh4vN6dJKJHIwMBr+cjtFzy7QbjI6c09J7Qf7iPqxsVYfJwwDi7n9+OO+tIoiljKBkHlnrRfSLiMxhwNv0RxfAAFVh4qBeysoXIYY7tBItgylnknykjUZSD9adLmTQbVjJhu0nsuMRAzgAw+/ld32UOtyc8eP2eWVJkSl/05su5DBjnXxnoS+bq1VxMWFpNUGg2t6LRCJRFB10Fnyvqrp/tm69lOd35zGYX6P6rKSLhXU4yEbnwgI+te7GjNEJd0+RuPExrJC0rimsr6UIHwbwPzWwD4N0Yu/3Vyv7ePzOa/9nQgyrxXHCoHywyrTYLyjMOz4UgKe9Ia+Df+Md5j9IxihAvKLVUU1v0FyV5lD2X8BISkngiXTum8QvkaDxHAP60F9PCMaRFm2i4YxmZN1GuT/IY73uEoqfV1k85758upnIPBAzhMwhzYgBSqBItURHeD17GfiePqi2Anz0XZwLOtTtpKfNQxJQRXQ/BTVBwhibH4IaLSEHZ5uxGquFBDztFBn6GlwNBkaItngCrVBnX0A72vpWWn2ATRDalBoxiNLGGmv/POq0OeevLroCiGO5BOCQHIImwO2GInj0lZAtPHgQk1YbZAebz1lrDeNkh21JipPuRljaa1dRFSjfYJTE01prlKYV7s4cVIxjDHZvIj/FEyyHz9Xxuo3uYtszw8ONCuCDPH2ibf6oeX0XjlG7SEuhK8Hk2Zykpm73MuNM2bYcdNp9eQnRW5AayjHxs0iYfRUaXn0zB4uZrU6sVSGw13ukNyd/xEsHjzdK9l7jpuEnhLULi6VncQGMlNFvVrfejQAmIRd5rYYtFMZCubL5ibTZSvZ5tLsLo2Mdj6oUoZH6uFnSgC8nY/O7qWZz4zxlp1+J/9BDd/em+5SgHY1tNw6eG32dOaKO3p12D+FqDCqCF2DwucsMQTya3AFFSk9f9rgotW2PdxOlzUxSxmcw39IKyHDaenaxgE/WhkxaxNMwEOva4LgAtXI99qwGJyE6ilJkF9LYuB1G43pMuS52kr+BoApOMAlJd+yMZwk60b0pI7/qpKdxufZr4lIJNO4KYs8GwLVI1o8uBvj0IamT9oqHLPEMmHUhhP+vJzDEpamH9c47jj51SNiFkFFp0bLPNgHpCtslG3G8xOexDO3XvalcLqLfUkXIqNTJdBJ5+j29ctmc8p5ZX7JavR9jiBHSMFIldznwHd43tA0AMM5mFXq3o+YT6S7aVt5JHDWrA3I7YC8bI6OdaiWLpRcJGsRY5NUJia4dbR5yE80wNe+2WpHXFNfblLWa8Msx8bopJq37xTCNLHOvOsOqJAGvyHOHcNhDg+RQyQNhI7xSynIEp4FG1t1cEODiPB7h4BoOQq4C54P9zUz788MPGmR02vsYhSDV13q1OaLYVJnkV2JCAiFFA5PhxbdtuQsNu8xM/13geJxveW1NurUVvaqHn7qTNil1+k0jIaYnvpe2cJjE2k61lJVnSL/y0qr4i51xr4YjUnIcf8sIyljMPjfdvy54aUmZKCCtX3qCKKsQ0gRNBMtBkAD8zE6lmbX302E7W+ZYAVUkQhCxKtbVKPGFNbXEGCA1x2QEygUaJnbHXLfw0QIgSDepEpBLjhRSrZWhXLR6DQLp/rRywDaaGm3jmdLlyYmxXvIHInZZS6+dU4fb3pmqfM4l6VroDNucHZ4fG6bkzHw8YDGv4AGvze+KiGiG9sVvm8y95nGgnKzgWyfbE/uNtshsKPJk9Z/3rMku2alFbiWox1ZHJ5ayr7DkvpP6gr19Or4a+GYFEIPqsuQN5cEOGlYcQGL8DfRcQk1H97MYi61EHI7pMn5kFjRVrbTK2UJDD10wabQuTuUAAjMifI/PAw7yUhZ34EnRXjudyEXoeaDRHWmdQIIN9uN6xT0NkxA45m+VTnSk4OWuJ11c/sWwbr8BLW5kV/GaUP7dttmbpc6XWPXy0inAvvhg3CIELqeTpPQcMOlX4O9KTYftToFhfkudfKKCg87KNQuIGJm5zsxYVy/T14IrDaH124k7bHYpYG2w0Vpov1csmFqZ6ZxP1yavJSGzg7AnQwe9Al1+YP3K1amyaXt2FmMlrh4Em+RV2Rjs3Oxdf0tQOkguS4KaxQcUvLF5g6WH0ozY/f7pD0AJqXdKxLCWg0dR5952zvUTu7hLdAddxqBqxqTHbG8sa0P4cBreoovohGCVtB6kUfH87coFKKm0f6sPAlCg0oCDD7Tgc3SwKK0LngL8H7yg64hc5PTdbmxMilKx3Ua+2u3vZBq8Hzzs/rzj6eaCjU+u20vMSiWAuotpy+GFJUKyZSje3QFUNCKLdcJRjjixpQvMxAMAJNxZ5LezjvBsCpkyCPbZQIhtx0ZQsjwcG9roGvW4SxbV3WToK6MLXv4Dw3ZUjdA84N439s0MAL8F7ThB0wx48v+YjFKZZaWsyFCxyhniGNNdinmlqvIlixD8neakCuKHmXX/narXlxpdZEEpUQyFHs6g1D9sVWX/uH0tCWVdW2KpUYgQuRCTC8eECPuCtffIFtP9xdORx14Yd8AgvBoG3a24ji9/9yzlptdO8C43E4XV1hzJ/SseVI21ND8+UAuVdsHBp0qPXyJCv4mq0Uc/Ju+Mw4948htuIhuNjxIcjRqRDV8zw8IcFVKwzbnet/UP8XfNKeLX1PTCe5qWhuzsqLLjJhxfDPNhFXDLmN+Zxz+YjvTsD/CIe9LHbOyPJJuQtazA8J0DGvMEQWp8eM7zpJab94OCU9efnDmQvw76SLrg7b9luKi+ujCNAr5YCa/3VRc7KDe3tlybVtWzpRyxrZ7jpDvaMlGev2MjtaATCMbHr57dv2+t3VbIlHFNJeq2pBhqD4Rq1qxpGjDV3WPwpUe3KJjtkCjFAVt4pGNZU8XrMApMtmNK2PaQLSSBoi+BnHpMV5qWCFbIU2sAlOMQERbID5w5kptOfuV6sm0jT+REe5AEBCLsDYXysGNAuxBdUR7ZjQkiJ3wCief9nOjjweBLOZCleVjeQ4BpbAypoNw3n8t0caAHDUzlz8bQGa8qprQXHDSQAP+71pBSIYSLF7oeohPRYX01fiNfNon7+WnZ1pWkEvzFVOTr/padQ/kC8qso1mTPYayObioAJdwY+nNHbs/4h3/qp2H7y/4N9kaUaGzEGsC0Oj9Lth7eS9Nt167beRheRujDQGmzltbo1wA03/4Izdu13tV8xT8R1XzKbG6bf+H3913EOqahdbaMzyiVsJXkdmHiIMilFMg9pCn1lsJUp3MIJrNNdm7NyIB3zqB4ZoRk/U8r5VAbAe7VSC6CM3AI22T9be6sGM1lEyGwoassK+5mYq5rZn9j5C3THn4t9yebuPtsyzkuxLy4qubYgESsNbV5YNlj+RxB1+OBBUj/rJqp33A7X3OYnbmoPP5itAlU5vf0gtXYnRwZ95I7io8KFVbBmuPgZc0DOLZups9UqpKaHc5MqkKlQNDb5h9GylgPDmfq56z6ip6+k2JEzcqnCT+PeIgH5EkThbkM1QYSCwvNQRAcjY486ltIouVa7NE4Te1X++oQkPm3sFqOodd5ZDibwHQqkHPIPlAfiigp5wV94DL9fH+rZoy21PNf5tvV586u29wG+PtgIjTSBMAFJ/Il+vQ4/20DR1XJRtbOl4qbsqYKjoPYpIshdoILtt7JN+MyrSo1D1gDCy862Wq5SuTsbb87TjN3sfWQ2P70ut/GOK9+b1R67dCu67SMdP0n4X+eUnCPDytZ3tXbhPm9rC5aZhQwiK3H4KkH6cFI73+9GkFS54GnaDEKGxZDIM19sdNpcjTzP7w873EgEXCeUdMMJXDEvyUeeInY17+2F5ifvlZ3zRjD8kBsLGMiRufOyotISeSU9HhRJbc8NdjPpgR+necWfhsnsMeUXWbizSSz13ztGvpQJ5aLdxu5up/NjUVy8Wb4CLCFgF+9nS30qLFmrEdGss/k5U+R5i4LjAVfkwFIqo8/dSZtCkArfNbZyGthGQ74b4/xbapG9WCzEzywWlm5ANvLPb8T6DRVs0cYkyqfrOyn4ypWlxqCdgZGqkDQqsN27GuZKzq0tp2LwkpC3OoCJUEDEy0G2Jlmt70RG5ADjiFzciU+wGB+2E22Z9seQ3PnTU1Rq+Vn84Xsm0isvkw+uS/Gt8MWrOjIXrqhQzt2IvMml3cuWvl/UnaRl3V7XWGl5kRTClNy5uEcF2QtjIyKacZuqep1G4P4JFV9e2rWL0WIemPbThZS0+Cb+bKpEp8VPvl4sQWQqmWK2s5CPM+en6oX8NJFYo89GwJq6Tzapl74nIUq2WTyu9/lUiqO0sULGz4WUbTcfjo6/MM0G35KXUEUFHwBrcVkJ4FsuDpflZ1Jj7bIn/p6xUelYHFralHBDE/POOJQvWI7HNOZ2rgE1MzdEaGxNMWHbD2vLYUulIWlWOxi30jjdWpNCoAEq5UusJV9RBf3pe/a0sQGnCHj/2kfQthQ1Qm8I0eiEDywHZ7s7QJW09B43wt1r2VKnWO2BeD9Ry/c9N9CBdC8605KYyFicXVnh4Q/IZ1Bz/FiiC7QWdtluDc2qRWvi3W2CzKvl2baIPIeaB6SMbGgHXl9A4qS+EwwKnOxp1Ar88K1oBtFLWlvPHqWoRbJoLLzDFsi1FBBMWyXPAOEP9QQ8v11IYbIzPgjlNKKunagDSyRR7GOQMN33mnYwO//CkB3MBVTi9ypoRli66slcGdhCl6FRN2nkBw8EerQP3pXSPFp7ExluNbneA/Z/iL+LUxuPyQXVqHRWwjy3OVKHfLcPcb1Ym/oZjPtXgd0cMfVJndbiirAgE7VkQ9adHTn/zK0H05dxwi0H3bWl/hFCi8BY278O8dyOM+Ezh+rtTTvawNKxDCZ38MMxWKn1KVm8m5wLx/4R0CsfhNUUjo8SO0e4FERKDojzAPrs2sLkFvzqaiqwn8iIIIIqDGwcO2mrPhJ20iaNJCyBeJds+E4D+YpI9B0I2VJl/A7I4HUBvl/JRjTN6X3iyVjSVfm/8gHCqTq2rbroMF/qlUbeB9QD+1TdOoIbBoP3dIbOaPlLfeaa+kCW3ZOn79T+ZfRRHx/xEaS7CECa5P18V4G2/DJhfHRZimVrmaUTbqTXc8Y765wZ9uSfBGCyje6HvutYjo0WxrK5gb/5691c5fE/z4eXJir9jIrs0qDYdZ4m71wc93tpGf6bnJMziOUugiHhEc1AjQJZZb8CUfbGlg4+pag3LBM7GD25SZLipNVKWk8nUa/XpcZKgSt3AxG2bQLXTDrHDt4ZC8DwVmVpMowC62X6wVschm6zxhqbQN+Ckeqd14ks4nwWjKeVQPB/GsGinh3Pk0vIhnfL9ALVnqwS58R27jIQFUY80X6PnTpyc9K4oXg4N5ugTqL9CDtshdZMGjTNj9iT4sO6c/gSHiODZIMqJrpKOjXVTnKZqs9njs4CE2zr6ZgR9s5O4tenOitzxumfH6qXvkNvfSLbzlUKAx3YmRAJkQ5pZoPsJfM46WYStUmwkcHNwey1TYEu3Cpium4X0UrvRm6XxAMrvXSOL2vcJ9Ed37gTy1deKNp1SzZocWGSKjuHf/TSstMXJFnrXHMUGScjssGCQeDL6fEUSUSilmCEKwwvHjyYfCN4Du7t7y9qUV6NccmRiDCngY084rVc8rsNy3aKBvgesyxSto/3iENN83Fi3PzB3PLW5Zho/aSC6BicZVPB5QWOmtBMgDr5mJynTgUs8BDRcgghh0TztGTNz697LyaKnCSPipnfEvLhJ0uenX6xfEvTaHHI3y4q9NO5dFXQf8vtE8clumnHjrPLe9XUU7/a78Rez57doYZlPH+Q58tW17W2adEN+WXRYFYOZJmwWZgNuw7zkNg0lVFHYBZHDDNZzJfZ82LebaKmKMAVSD4uh/sI4PmbPOyodCKe58F7LC6KmGCCCJ4RKk0HQ2Dpgu6bA/JCsyr0ynkl6O+HxWFfd7fPF4hK8XaBQhULzWdr3tHc4NRdl0fPjnqMxivqL/93J6gJCqdRabE/VzMDb/++cAsy8OQuQgw4+/1OT7Y6+nPTQ4+9bdsDJY0sZn/z7V1IplarZfkvZ2OESNNjvVpUY4zfXR7Mb5ESy3EcYav7+6vzn/EuK+pNpkL0LesLK8JYm15kaqqpRtU1S1RR+sqK0vVuAi8rJEpRST5yKP6lHXvxSLgXH/4eD3Nb3hH/r8+HfsW9Qn+/1N6+ITA5ek7Cfs2uYGmilAmC76RwV2DFhpyQoZ6+Ow5ICKYmKB4M+BktYeUGhAhrwd+UBbaYkKDVRY1XkaHPacspwmGM8MnvohgYkRsJEhbxAofdeLmaG+ho5BSIDH8uZ3x8OhAzYrXUmK5dlXzW4uB19bIVlbUp7F9RUU9lVxeXBDdcWCvrZRFYUFdaSulw/ELOljyv3r6dLaYEmW/omCDiRASBFwlLCMVx2jJkc8q4h+ivYn+2MgTCS7wRYr3QvVTdkN57Zi7OdWngZJKoJYjwmN/EcRyGrM7yROcYJCtm534JIbl26MeMDOREnICjtijhCWSzOFAyIIuGMX7CKR+/UP0bOV8O7afIeVSAE0zGTmTKE0plAiPb/Xbm/AQ1SrsMq86rXoMWWTMtRhiQeSSC5C7j2MXjuQA2qImaeYImUlkF5Zq+vuqa/r573derwXqTyerV81adClf+vaJidmRkdlaY71XNiAgBPqK0wrN9uweGE7sH8gQFnVatiiFrgsViZn1HAxZX1LebXAf5Nt3B9zD+//iOnafho5LdfHZTywvvqTY1v0AtABEYACniRezmMI/RKomB+iSJxzwYhSanmVqIj++OVuMnl1Rd6SWFRcZDQVwKoOoM9lwvmZZWl2hBFsqV6viWeCLHLXOcmbGDs/Aik3e/Ie7bJ1VWtgbnpMmm7MZvvRvVlbbNgJ5YrbzVSlSGks5nO/8F2PMW67IVUOmcHy3v75dmI97TPLqps/NES5848HIfuCcKihtu6dgo7y3GlR3/aK0PQ+qv7PwK3POJOD742SjNW2+lwTFUeF9p3bPnVJV5A1MQmuPnFFZykyyvSxHUS94yaqZuMYHVAkEwiIyvCzHoTQR3bJ0jVWp6bck7SpQ6bNSDvTVSy8W3/7xCP2WTen+K4npJl1Pggw9KhVfyOmPNXJLA0WvZ+EB7sqDyHeHQpMfFIObVqOoilR5Thph3Z5azb1we2FNRWPcKb1atRfOL3zv9RKI2mFHw/o+Lc4/EBObsJu6lfTXCFuILnU+yOsuWfj1THm8utP6UlHpHolU4uOqBEfPjCjM6VhSvet6b8ZmWcR5HkRYKj9OHol66fBGRkbGeT0UHxYpCQbafk43QMMkWNj1QRbEkNy/VjFzZDh4JYVHwz/ceYubo9pio6lUQhKAFhcIqOdXUNUyy7ZtHx9+NL16qCelyYRTpuQoSFnt+A3T+0MeZ6fedqu8KXLjwtQbDJyB3Fk5Ga98RHoV3fMQCZbePRZ/fmb/K1vunS+gfBb8j5+f5s+7ETzLvPnTFKrLgorQEyQHVhuT7SNpunLJhCdvkY1HAGy1k6Ggq2iyqcYJSrcv6gz5DUNO1ilp1ulCuYqMVo8yiOUpRcUtBsLg7aaozmMUKA6Yx5KNN2RNvTjTH9yTawq7DJGyosT2RiCRRH6dHery+X1Sa9SalCRaZot9YdmYqvwuBixYD3wO34ERON2SNM1ljbgRf4wuN5wWRmqLQNbHpFJrFEl58BTOChCSA5xxYNOCmQzVdI67HIZ0ur9q0XEdeL1mjbC4kypCPq22YnVRyhV4FEpHAKT8Wd9UBRDAe9007qXGKh9NpRx4W9yYC5wQe81DI+OyOnm4fa0A0khBZ8GREKa5jzbJjJpbqZyZx8nG5hCURyhymuAIum69iROrGiie27WsKYf5AekTS8wN+ntr36Z6Sn5gnKxL1Z9a9eb1yY/2h8xsxSqz4957Un8wEWd+hJ7xeTi4wuZFbizAz1kuTICaaC02J825601FRLSITz58bjpYvP8sRojrG1cMMKM6JLgZxZ0+eFVnhrCuiOeQieA4LUv5hqmPOIbhAxNeMDP/EaDcBc73ToIRY+pUhFPqgww2yLC4LClxtUPD5Mrpz1aI6t7tTpaZZ9ULsUawuOXMjRv/VE4PVCj17lVcUxW3cGFf0t8h28NeRiXfHcTJcYTKPT5ADmE0T49l4oIAxRs8BAmTG7fSjMN2AkA/Z4kJVy60JQpfcqUxghRqtSj43lkQ3yDNhs7ApgXa43U10gml371a8XDp+d3Z3f24YCUMTS0B94fWKmO7/Y0ROjPBs6dNI+6kul3o+ukIFgd1uxu9/+cVD4d9DPXTryuZxQfDs2T0qhDjUeKgVWI8el8tu78VOJ+4dGQFZ2udEdVYBUWDs2PPoXH8/VO8VRfhyIICEfCURsBPbMGSdPXQIYI4Tjx5GPFg5CVt9BPFV3WpYR8Atclz87gTNSgFkdXxtstElxKvv+jxrdlZqP+++I07MY1gpkv/OmEcmlxaaj/fIT+Ol0nGvvqWl1TsYitQtWXGDYRgTy2JM+2YLY8EspjIE2RgbNKV4iSYM7EtOJVycP3Dx7JuuEvB9Jky5phpEgQCWrPxcsWSRXOHHlVHDrsJuH4IlelM4gx5iOe6mHofpOQAOPYOTyG4Hb9NJFvTBGI/bur17CWXdOD318eaBgQcw0+ojf1dulqa+K02uTppKqkpO6P5Xq4QQlJ1WkJQmr5anIYqQ1PnpY6kRSHMsvr8dhGc46hSFqSmK+HxEPi7HtqXlXzNbbZWVUk31L1sOTfNFBcZMVtWsitAf799LL4UMxv3TtlL+m7QFz5flphnrG6IlGS98n9F5FyRUur++liqlEVc9+9U/GhYs43Up5xONkhJ/HJ5AZVZC2mvpt+0c2jO790Lo4i319ugm/1+f/kk1p88KLD165fr1K71T50uypKAv62X5vgM3FKr6+6UzZ/1yqSkqgPqdIu+UcQ6lZZZdjyrKyV43aIww7i02vXp5IRBXelPWth882F769R2utrq/P/dzTUCYMpUWlqtpV975PmglzIxDuqG0Xw8lp/5R98fU5Olfhzg6uS29qS09mZ7y2QO6NGBJeiWL8oJqtSSKUKu1W3/9xfz+DYvOq6uTXTebt41ZkfWTzofoSZT7nyZ3//H9Ute3aHrDB0ofKAYPTtF5cCHcg/sQ8v6d2XVcqSure8W8J+/0SwRjeMXQXbO3vIHeB96qi+guNzVOidQpahGMSACvXei+i5ngnWw+IU1JOMI60qhqKs0hff2UQ5qRQyAdPwcArSm0bUrRrhGvXBXKnGVS2aWyJVw2u3BbKOfB3spK5zfmMXkGJjWNlXn4hZ4XcB7MjTp3zAnNuRIPjwkhG65U52Xo79XEDKEDJ4/AuNNZjN28G2M2EW7qiYZkRDdn7OaMLXJQsOrIlcqVkequ8M0Ic7goPif8YzhH/uPUTGOOgXn+AqYQOOAXd9jBBIEZbaHZwsL8qvi48vJh2xD5oPEq0PfsczT+StqN3PRQRoy6n/+ScOrRoIgCORdbPjP0RElQkGZwkNkTQzPlsUY4GAYWCaHQUIQmoOhJg4Kc8z69lBfLRYRAVEfxs4QIv8zqRJ1AQrVVQ5j4M4zKDT9ZWq3IVNWfWRU4GIgk3rP2EHjBVPRzuPk7JCbQ5AWz6YUXQK1k5YnPLVkki/txym8LpJHXkLMlmrpthjj2EQzZnHyv3B05iO737yjg8KcI6k7KN088IU3djYl8VL4Y+1d4sTiha3hYRdZly0JkO7ZT8lF5jK8EU+Bv9Tz22MiIE3rj2O0AZ7UfONC+tqy9vezCxJUrdaW7Tp/e9VWslDWVJcUqJPg66FOzOO+LBw+2q9RO1QrB1+5d7K5TRaUp06JU1WFfpNqGbO0vO36l235dmRPb0RyuUmmoLOsgYrwjEGbZZRnoPRtzR96crg253vE3S+AS8AwOglKuxyKR6gKvE3VgAUuYm/GL+xSF0Xqi3YTsdRxEF9yviAdf0njqeaXOq/MPbkAwhIkgZU7xt47ZMqgp9x3NOeqbnW+6LMHRR8GiOofnD55Lmgc0A8WE4+C0GtMzc2kTvRNpc09ufL0TnKxQagA9LN8T4LBlnYglMViJIF4hikgUm78hhAxiO0OGMJ2hbZjO2Njyv9cERTKFpUhmz5kgxmz1dtdrsz5CxluecT/NZ2I9DxK3Yvp1Z6wnFsGr0XnEmu/owrOwiMl5/3Ur49bSIv147gOc2uWX3wQwIZ4vKSw8rGcWLXXr0tOtoiV55bXxcBdn6qH6xKEhE+axCd3/cEw4en94zOGJTIQ5BPgJcz2LHiXWm856bk83IJgQq/jL7md59C8KUHiZN77h69sfJlKlHfz9ni33EZ6cFvCsdwX2eBkv+WkwXBJOaEavVIetcePA0naHpDIxDHPPBc2p9FOgCd/vq+2VBYdQWLud2q74NRiSnYrvKer7sxWBgUfsIov4R4Xn7DFCBOkUxQOjXf2O3l8YPyLhuZGDjY2DRE4o6iHcuPnWvTHFAzS9sO2FQa0fCXCbgZA7oWRuJz0CJUAJgfN2EYv2BcxjuIj2uif37XUg2xwo8JL8AOWdEpw5Ibrb8JOfhEMOjP2n8uFRSRWbrUq5+MrjTTWh8Brv8f+uYRxN1esLg4ye1KSRxNSSGksI2mVgITypuDZwMescUqXHPE767Tlo+XgkcHBjN/9sGPmkAFg1JN8rR6gLiLrr7dd1vJZPHCwaTAyJFgFJyG/lqZfPzzleyx3cl0XHm+vn5lzrf7HQ31j5DwVK5RCpI8AJISSJt3gRySWwFUvFIlqE6DdHp4tUZ/3W0212WwrsD1UweFE48s+XY8K6GOjYhlWb6j8++OUU8J64yf4Fk/gcUg7HgYZ4ghetVZIW3QyflhItd2AkuDRQUzIJYbSgzoQTYngGIUJVouU2ioCKHVK31ocESIJmQAgerTUgjjhcLvBy0KGaG4v3wxVWWJvWZ5Tp+pfVLqAre7eDQm+MDFe3hzCVqoB2dnXZL671c3PO9d9Dt4nZW+msrXS2sHe76t5bWPnv+pGZozcCOI4cErE8a7Fyn9dBB7Crc9ekMfbaw7JwNgyHyx6+Akia6PjjOJ8vHPEvO4kcjS2+eSt0c6wRmjclpk5ROZkCrYNzioXMnMyYRTNF2cIh2RbY1GcRS4FjBK6sihBJ4bb8swriO2FET4cDOUSCCbhdu5ua1piROWu0aTQrAvl8OGLN80jC0tnVGxFJQFG0mWyi2OkU3ZN1NgeySeA2Sf39EuEyMv5hyoiZrVQkJIpxNV1Kx6UfGlDglA3ZJZQ/6jW+HJevQ/O9IhL3c5pU36CZmWga6qVLOxTNzYMZ4+CDJw1g/5vreU00xTPyARJc2MFE0RFDCDiydp3RZlUILKr6RIaC6atNTnTbsM1aAZwODw01CoKbgHJZDdcjeUt5XZ1ndus2zzv06tRqTbW8WnjEN4czh3KKYpqgzQe8MLyhxULkZOyo77Zsafp1tNm8xWIy3eW3ycvH5HhUVu+TT+NOZNJOw21di2UHYlGy7BFK/oiseFknp8LJKPaArLQh68ChjYW0BaqWOknpc/lrXtMSOQ+eVl+UF7c+ryjvnIGv4o57Pdu2X/bozgtBy1hZ9U+pOCXR2ZJfjjqiVYz43woEWZ5ddmARL/vanqLK483/JPXHoJhXSf90ji+nTsngVdAd1HpPuZPL+zW6RKzofSI1zbqErGkdWKLyewm7uPywvrVi+abeJuprqvG/GnpbXZCx7uxe/VtapZhLY6rlYH1rlpy1bMb4K+Xvkxobpf37Gl9QmxR1CvWZ4ZlCdFejVSlZUaXiqafWRSkpNVEQRCocjvfNWIcjJw5LVrDRhmzqkiq9KBKOrIkUJbRAsk/V0hCPIAF3Bo/Qor034vbRbRIRvOA/GxRSgtQm6V9J5XhBQZxGRkX3aLO0m7Sp06/auLc51HPv2i47bt9e+tymTVs5G87BczSwk6Tp7K5Eq3VhemWwM0b3PuZZtcoDvXs9ptx7ryu0atpGN49jWY8c2lfw7Zau+mrfV2tLA8cd2+sIgeVHJZyCC0RrdCFn0C4Zx3dVVVfRItzCyunv/jdfjW9gHb4AIxFRnTNw8X/KQQcyLIcCQexJJTredVASvsqS3jkYMblSnbnTwG8Vu+u1bt/bFMJpq2tdXcbzLVsGNo9rzWV2Pqw03QOnSRFhy2MJQoYmzyYb8FRUvvLCGm7fT9vjA25ot+ihBYg8SSatrrBZ2A1fhkV4PqRchWl0c5GDGYjSDhEDK82qajmGaxQtI4VLCcs9pnl2k44Gr+Is5/+kt7bokG57X9/3i+X7PbcH6OcwnHlKb4Bei69dGxen1SwBujxMncgvm31rIF2Ymf6qpS9Eqv+3WSkQL+oUyXfbmDFfzdPYYdMvgSyva8C8MCMhSfhY0Sw4aWYGnuwI+UaOBlPZbS93R/EmTD/JImkKsrldCftjfEJXLjVBOVwnShFhb48FSJOTaDLqk4Lj3occwiN36iYHeZXdBcO6ZwwcOJfCD4M+32utoxucQ+tGR9ct3g2SRh4DvnZMIRPupXplE2Gz7h5J8rH0MTkTOLSFXkSLnE+xMWdRuVFIEOWX0OQqaEUYWoioixC1TeFkU1pGEkG1EsARgX8M4V+Hmpqu9Z8cC+lVG00W632nEqfGjk4op3y+qajIg9vGxpPGCzxX80f/+Wl0r9cD6jtfhU2ps1yNGavx+RD16Gkwp465xIz1BDPeKrq2Zr/oqN1SkJGuym4gBGHap5LA2iPwEnS2AGGkaiCrGNq2QkmU8YG7Ap1ykuzFfoxX52VAts9IL0GRILxWbp4TOVVzDzTGVkmoiI9PgKH123dI0wcuXz46NjSw45VXQDv/YjAtTkN5YbVpw8+GPq1/3nD8TnHCWzlvWYrv7Njw51buYrMse36iXk34q6j50AUsCeVjbZBIeoTVcZ4cLwM37JtVJtTUSdXVLYO/GODebJvv3HPii7/OaQoAFmHEGXiJ5xx63RnJrXV9jgOPhKfZv+ps+aNJsc4sZ2zSo+VsXlf3aKLiUuYlRdK9OlbCBsK7YYlFNERpBAkb1mYpsA0SSe8ANePV3aEaWnQe3VpTr4/bwal3cKvNTbA1vHjwQEfx6t4In8GcdTnVfX339fVVr8xuUDlO7ODa09Cc8VSelIxXSvKkcvhEuOaUCjkqhoaRUY97idQjh0PQyouiIbGIMD2Bf9Swy+TL3B+eX6W71X5Lt+p5af86t6B236FD0tSppiZJ/7HcalPkt6YeTllsmjDtHrz24WLAiakpyagaloXbzjCZIHn2JisIrMj6CMuz0J/UP8nhahiNeeFpmmp52pf9kHJdfTXsKnk2LBu+rfMxkScdfu/Iu/qjKKM6SuewOcEzxJWlJaqnYtJ9fUx58qmfczgrQ3DkyqRC5Xfy5EcFtmmtsdIt69xNU4EKCU4TayRKzGTyH4wQm6/ILzLNGbtAgvwMgo23i1YqMRBXM19OeB7hokYig2RIl8k+VFLxiBYDJRiebuVG1GgjcyOhlbEcxioix2ue2/59v7+c9Irnmfl8EIB9Fn5dt1pUt8XkvmgxcN9RKq0VQEA6EjKOoT64MwT+QOzNP9IGyQjX8+AUQgbRPScjO/vEEKsVRktUxUW+5Zua0kFki4EscNOWe88jCFayjXLJiIQhFcbRVWPYLJLP07+USEoC5aIyGz7ZxasKDxsFbelohpscLR3cICKtk1KGQoZab7rrprbKnhvy5oZ8Ofpp84POIXp1NvK7SkaTjZIZxQs0GOGxjXDrUJ9kDuYt5ENIEjsmJYsK4ENdjpOjx4xpuVZk7Gmk7STjmZ5N0XkYKmJxZRT1bJh770R6y+1CTQn3iflouVIebWbnWBbvvU+s6lwo1HygeQgfTferBWLMHkxtvPqN/uyDXEGhtHevuoEt2P/gg5AzOcOZ52WpKc0EFVn1sFL3/uDA8cYp5amhoa2tlZyh8eio2pRtmlsw5r5Vq6SxxpXT6dRrLlRXo9MxulcurjHdeLJ6P7yvw/KP1qIhlLvjqJA7ZICcMcuJ4UZWem5B6EyMi+MkQr9mTQhKPux+0DjOdjc5ipMtrZFWsq8qRNkEEr5V1IIzub36ujpTnV6wxFpYQirCKJSEgY4WUhwxcrkyNSkVVL7tpAMnCN7aLNZCGFZhW+HvS1cMh2YQr3UxLM8yKJxm6HAgYZ3OOUMfB+3mXR7GToLhiHmYGJnjwLGWsLjPR26iUwQR8IoC0cGmBolmrVBmqoyPq0FzI1ZU9FotvnkjBOK4UWfKhOwpfifCN1lE0ED40s4cRzjtZXhkCRicAEbvcnS66OSw6lLZJVVA+kZ6HBar6OFgIVsj2OzoREwbF9EinizlQ8bzmfV90zW8twvWjwwP2VVOQQDmQ8yVePn0AT8fP9Ijgy7nxb9wn2fAuIyWl8owpqW/IWpCTmO7QaJdtvxI/aLzjXXdqFvwpRYKu5KLibxEFK11LupRP0a7gz8FiSiKnBthBOSxyIuYgPnTt+yyvHTZ+yF+Vnn+7zNF3OqnAgPar62QVyXIvi4q25r/3Gh2QfqTGaExv38tRPrp+R2My/j1pYH+85saAMhj3idxhCOsG7kBLI943wzHcRzLI96iPXsBQtelRFDRojwBY58P51LgCYmICIjHVUYYt61olXRbHbw3M3+sX5rxsamuJb6ktvZldazbe0/8lKz8ZYMeBQiYd0G8K/DPA8aJzelbzUtpta6UGpemewmzbZtGaPFP7/TeBq6LBcSX+wUjJ8x7FsyzPfPLmQJikdVlgyq1ilaoL6E4w+kfln1Yiz3TA4kAaZG5KNrfyH5NJLKs28qwtuNfneNnVUSy13YwhxJjHpfMjkPxz6M16O2KxNXt9L2pUf1J+lUoqyKasWbYtnZzFMQOQ5mZ1ADnbDdZaso6QE1QrCP6KZ9w8wT4ymsvJS3bAC3m3WBLuRxTweMsshfxCHGY4JKuzMdhHm3E4QzD0JNHq2fpI0mniw8ciQNOekFAJNK/TBB+9eDVRbKih9hGUc2+kP6Cu3F2CM1YHM6YBfEGXsfqwvF/NAw6rgfEufEkhp6pRCwZ1Zai0pFr7Ecs+IREJKhpLUDc63rWU/plmryaSrvA3NHcZM7ic2F02EXSKc0U9aVqtuyS6iq+mgerzz78CNJ5dTT5PsrKCtO31eaV2UXrzdBfkDUhIk2elvhTopoJ3cPfVflciXzvvZ7t22Y9VF32cwUTS3jCWq2PzCT6NrmM+mnXYmJ4XTLz211MYU1dairqbmfVh15EppIo1RF5lZV5SY/c2bLySWWqCR8RzLrxREz2eSWVH3KJtVr4+WJ5fpe9loiYBmXZbtcRkQ8Oc0QkxwhEkZwzJ1OjcFjk5t3Ed6kFUQJmJvapwqdimQFJM6U5xYsZOiDmRle+YiIZWyUAV/3DtG7TxMQm3LRzRyMua+PLsM6rw0qiZCVBqtsvsrT/IiFc2ILsuc9UsC7uLdvY/U0acZRDADogg8Z3evPASw5xsXxR8MlMkh1naIk+47CUXQ9B3HR2DLA3WxHC31PUEpEIbps9tmAlLBEMNt7m0yKMhAV2CI5cIyw5CQHhhhoQQHBL7pkSJQxGgovgGpjjv1rEP5rO7tjZONkS3UnwzSIsKU72QCcb45jF5yliB03iEa9x6QxwN0jWEixh2hEo9GtCjMNAjVOLwDzwwZqb5Vg2V2H15jUH0ZMvg6fXXLp0zdPjgkU55vpnNlgAmDU8n6q6B9RyRDIsYYTPAHcDouXqg+2BQr8lcCITbTa3LV001lfABi0BhEZyP/nVp8NaiVbCWP0XD8vaPdfeBJLrzFAkFBPcljRPgzgxxkrwE5ohkx3Jy/wyTdxiIFfXMrPcDSzT5RWk60yQzVlOwaKEu8HrLN+RxIl0cEeG9N2FyXK+m1cTntgLeFzbf8iN5ZMwhvCXeLq2MYKkJEoeAgBr/5E53MIxLDHDzzK3GSuwE+RBBUkTJBkCN3MmyQ6p46l6+nffEWuamhCmJGp/m0nMw7HvlCdIRvA7rFeNEQIYK6xDxBFGoH03wST3Hrj+ZLURvjxLG3m3bu7d8H5p2TxDK7tbfsjio+e+jxqGrxeJt/4AmlxE7Nh2sjmW2bl+h+iyu841bwmxdly2l+8HrKiYCHYxAkEEcVnQzSEs6GxTGCBH0XmInCYn2yDX185Pwdgj7d7f8VZGPp4+LqdH3B3aRWEHbwUgJm7slo/LyZkLOp/uts6r+7CaqpZXa6oHb2lOpY/JqbH0nErZoKxANiB7WUbCVF4GTOAMKG+5nxVO4MetRFJE3h7DbmmBEV0AEJMZepIcYi58wniZSWAMzEA112jsqTLm3gTf21gFUTcTAquMi6hEXZ56Q0jLaHhCsU9yVjbvGf5coQBQiM1WznWwWB7GEcIZMwUdRIAWMo1Ul8+GwW8dsUVwYAcEchKnZSH0C6wb68GK2PfDyDx25N3mr61uU2ZrYlJrJsmsSE6qyBQEyGVJPySKGB7QJOo3LW8WY+b+x82WKuyrD4vAD/vzmfZ3auRXHEmVsEuRSUeuDyCI8lJIjJrqwudS0mPC+Z9JkuQ5HMaSN98SyUT48vdNjtWz1dXLCl3NrQBD046djQ9u2VKGyvi2fyKi/ujpU073MnfgP3N/2wrmlLMNFNsNLo92W4osdgkTg4MlN/0KmjIMsYnRNJyc5JdvLRzaEI7HuZuBjzFdSTVo4rgG7AMVNBAi8G9TteWz8svyefl/L57vRB0N34KG9ojwg8Kp65Gu9ioRmxZp/hMdjufKlcha4ThCYVMNfAQ1IpwaRnImGwyJWEQ/FkjacJk4416/llMV7HviTSFMEf+8WQRnSERPGjhiW5IGJY2F6uHlhkjVsrYSq2OJUagRT75Utc4vjeVv8tUG1zoWLAV2Xmps8kPBt2M2CAskzipVkVFKUZGfTswWbP94GQlreuLf4YWQLaovfEfx5ZO9OfSV0y1Vb/DFqz/VQcvmOiRGz+z1hQUJN1OPCg27sg7Hl9fd0UVaVh/tDnkDJ8t/L4P61RV93hn3WlcjcO3VmYgszaLnmDuaNe7JG8rpD7cMi9NeS3kl4IZb+oMv/OJWS7++tf/mp6jiBuC2ssZqDDhHdxT/F7+6d8glRPwBIbaHU9o1n5bHtuy8j9hlzqdGti+Alc7o+1zui3ZOvmO0y6upcIRabm7aZibd6+MIIXYPYEENbGQqPLeayMnrr0uOC2+F1G8LiTKNSisMjzkrpoRd5xc1jFU0HN3TmKiKf9n+4bqj1yqqVXeSLp1dFatRtx4x5cfLcZaBFzqnFTH6v92otCMc7OZd5rkhqB5pdN4ZNWreWT36ewQ4ful87vqSqZw66fNhK7JiNjh0QNJMDN4pjbLgByUqecY1p6ZyDgqpiUNKRcRvuE7Xy3qEtY6PcRgThcQyqnDTA+RBjDyIIeNpmpoCRowdJJjjXncSghdQmC6QN/qIHR06NEUndBoy8HkfLF3MPwKOEI49E5dk4CA3smzd68qbWrmiDsMVp5yvo7z8RHWbL98Z+3ONO/tqdLd0GdHyqIgvhGhS9O3ZDx0eQqdz3Olc9++NDeuKcY9n9LWBgdde93hqKyqInAALIRWscxaBtmJiEINqMUinsfVyPhz5cJE5M6vVQyWucUVe48qcPTV9+13inpxN2bVa5bXh8Z4Glhtde9syG0sBZSeGz7YuDcPhqoBekedNjrLd3Nu7cuDwiFZxFxWHW4PZxooEIRjl5lph3mCKrs83+/w2d2ISXemv+Wxp0K9O/ctrzCefrCiZsmjIzVVcBNRgK32JEf+buBoJS+xKJMFCGl3QT2UVEl1EBna4RJAauRGsxn8d3Qqk1Sp8apt6poXthL75SSUqLdhw05cC5wbz+0RXC+mvqtt7qLdtsijFVFJOXk766Dj2LykxKaKLe/XhgyCEs1wR4tYY93MElTN7ePc7RbzPwRji848oaI8Ubw55vqUwfuZXuF4e/2uX8cY4uDOzzuodfUyfQ382a83q+/oiKGdfWl2SJ3p38FB7eVRCks6v5K4bC9/o35ZWCg0QtCMCG2FjSWzabMl5UuRWLKYAJiy6QiP8PwOA3RLccHGTSCI5izmShevZQDBBws8hdZQmyrnRRIpFJV8BjkblVO5rpGynue9msemBVOskpIhoPAh2o4WmxBFRRDzuwFKQUAvVWmMJyCLgxm0hC4elCKjtIRsQJ+lnSEScG6rXmt5AAmLvIZ9aPnpbi+PjbKuTPXmWuAsucdgBZS+AfFQu+bKBNSVv9pMRqiT0zJbSQyTaEgNnbB7CLE0HwXcvG4qApf/kHKJdmmKX7DMO+Nqzu/ti4Xk8ninjii0uO1Zo1OFx5uMXpbXkZ1UggD1Gsj11JqttwkTshsZjvgMKzw2PPnDk8i0KUrcQX9I003SY2usNKESx+ljEd2a3sr7bZX1zHdZ+o9Fnv0yJK5z3Yth1V/fLnnz4fo/5u5ls3ow6fCEV2X0+eR/O0VxTdZ+W9bWo7z5yult1TZODk4PT0NwcIy04eUI4Y2W1Mr745f+kaZWmQrIiEh8RPVY5k+3dVX+Cx0qBpe6kuxSf3tideeKtB8Y7EcGwPvnxDXVGl3bZuKw1urCbcxG7b44FdKHnu3MWycwH9nWNUlt7/eE5ttFiBY5HwjvGb2GPonULM/bShAVhES0Kx4aT7Dv7A6sF2x8BXkXhOIInxmHv3Rj2GQ4OrLUlahhz6QXjmCwwH3BRh9clNCawvUQzLrlAl9crCqLMXk54t7EDc8qRrsm4BgBU3tOmWROvFToeIvDWourr6UGDT2PenWuVTNQ4ZZK4lJIPTtzep0yr1iKeT3wgAPgRnsrT+t7ODAz6zEoKm3r1+SOPPVZb57ME7rccOjT0c2OoLytKE6X1dYdSMI26Q3Mt5rwRHZepU6N4sI0sYrXdRmKx0yT+hmm+/4ohrFszK7RTcWcwiw96xg3jEKH2S61Wr5d0B+EmGfEtztFzqhsvDLrNjtXXNyTvUwVctbUL7P0fKaufpDdc0oxqsb+A4wZlg13rSRiO18pxHJGT9es9t/lbSThsOrsuaOUsXVxLzrar5y4ElcZbh0rCUbRlgTjco5Ftapu2LtO0YT9W0fZ/JX8M+5V52hPnAWQuZAZ23jQU79ixowkJQ3vFQSSObxtuOrzCEq90lJdTUgqKHBc3xHs7VrqUZvu/tPNIg2jF3K3TRpyubrcay1fxOLk6vtl7Xz7SnGtrS5EoFAlbo3Kk+RPV5m4V85+GC5D77xpNRUeTyHNGYWGttbV0ZTXkRmCqoRFveq5wf1vb+ontJh6VgedFXDkdWCS84EwBcp9ukQj+kgdDeEaYopd4V+X204Y6eXXA8SKPsdP+If0WVzsyJkpyil03d3lybrKv3C5qFm7/fXRB2NEx+4tOT3Zm0mjxSNJER84ccNNSmCXmbVfPWFWn2EmrY9UgaotpqQY4QxrdBXZrq+g+bJFbVUv0ZW1tB/m2slX1BEbZJLlSDqU9dj9lsJVFxErz+Lj9eZp1Ti3Bv387bmmkFmeuw3oGzTefGB0+RsSQ5KY8VIZ3JDLaNDoS0u3WvOMW5vn3VXulIW5De0vc35rjOpZoWAJ/i2t5UbCj2JzrgYmxU/rFNN3vDBl9leAJmV4fo9RClR+lvhbNM+Mq5bbyXhL1On3J80nO2fkOCaHiLkPzouGWs7Hbadci+YSrvNRzZ6/iJbsMS7PLQ1P9NYLtJoZmbtBqVlQjtVrNNLJR5Gi9qI+WW4uYKE2UXlQjOkpz4VmjXU51mpEfpYlzLD4xZkPW5czL4EWcTMNwdOLV00JbYg1Toa17j2D+Io96XYW6cXBzc97Td75QSPis4HT+o9RsWVWH/fqdVX6KP7zW0F06kXpsAuy2H/juw6bHD/iT0uqNb4lPWns29vQc2uXtWcA/Ge+q/0XQvYHdmFCM178Nw58KbsAbzu+iF3q8JT81vIYbgt9P+FU149b5MF7OfCq+kt9itXIMMHa+XJ/euVFJWDCnRs3sNVb3kirS1N80QA0rnswIj3x56hG2JZyQarL4staen36UPRwaEWrSN/y8klZwcA583mQHisSi55JW117eYREtYKe4rM5Ul1zHEYYjZtgJhiTf0IuoN/2vv9YOW9uXhQBTznNJeXzt2Wss7NJRfuOGwcGT6yM3hfK16ip9VVbS0SEy0P7rovQT2+PCd28r1MfHB/p0Qlfe/uptrdoVXWu021tatycWpsRFZzRf+V3G7k+HTHURqq+/LF8TbA7XiO6WpcYFt7e4s5p3X9TFZT76MpWWa1AiOvYPr6zpTw+G7Ac/gUknRSVG+BHYva30pxf2d3bHrn4cParnm/eW/7unNDH+r7Ai/Q9nkx/VZezqM32nwYqn36VQS44yNUF9VBGRmal8MCMMZf8LJct8sNarIgGdD4o3JpUtEPxg0pRYndB0/X83Z9ukOJn2fWoDVhRevaqrLePbfkf275eIarXuu1p8zhXp7fYUVIeud5i5zZ7rjRV+ktlXvKne0HxZVuwdbaOkqvgm2BRplZ3ZYXgxXWDK/r1iMcfYVfT9BkZIf3HHWkXZjbqGlqNyQYDuHAMiAT3OAOFF2nEfhHrMDZRrXyKc8T710JxkNhxSv5Noq054c7ecBU4yLTFe98F6FYw5wneS2GULT3iXS0nWLEZh5Xl+TkIbcQNx4NkhOtQXpbZ2NcDnklXpwpU+CSkhTerYlaH6HVfIjDc1F+TV9PXfqzavaVkpSziqOqp5CWij4hviGgAGayluq4UxM78gHsW7LGatd89p03SWraZp5zaLyy7MYvcdS4bhzy28h4cuE2ZFLMyL6yzmSkl1Lbp49AviSTZBJpMUx8eXfpXu3+shHhB12pyN+vOsWDIX0rHd4fNESYYAWxoSc/DoruwsNG7rHsDumdyEA6xCsTsok4rtZirR4jS5fhWjF84Tt/YtlsR9CMVlkOBlh6R5e/vhlTwi0VZDZVCQLIoLS2JmLhC4Dq4XoRej4Vm6r89kdfBY0vojZUqLI3PXiacyzUM85cECX8/efciAxI175UhnJMLSYeATgdWSJTYbuCYz7tBgbr9VvAshzyfUXm1qCkjf+O5zb8AwLwA1TUxKnkpOEhXtt3L5QfekNdiOAb/RKrLvNXowaX/XS9JKb8AVU1aPFo+shoZnIvuZZZTN5O++h663o9+xdtBhNe4bDmgRwSD8i4UR5sknS0qQ0p8wI0nT0gwVQCJOyhn2DHqBfQFjUzjmmOnpmZkBq7ZkluU2NCdIanN4DqY6ld2VHfrKDedn+eYwFKhQIcik5vVvs2OHrenLNZfg8JZPSrZuSA7xo+S/92iVdSA5VJG67krpFl1LKDNm7MIeZG5/dq8VmdqPDxdbixOSZclLPmNEyI00cGdmOFY6LPeTWPYnPWa7CEq8zqu7rfPpjiaVl3sqyu9TZm1usm9pStPnBqBDrjHZgluT6u13c8QeZMMYHBpndogknevvl/btyxD1ooLHxiCKj/c89pjZ7NFqHkMKOvvZPezLphX1g75CndVMepv+OgWx/OPRRE5aM59ON0wqL7tUNlvmLEua6PR0ftE532nc8OURZDWWXGTks7MHmNib4z4dNszDB4eVSV7uYnKX9L5mH8GuP2tO1WSnAFkbVO0SJEz6nLwbdc5d0Jb6lBsrcCe1ZjKseBmJmSV6up3MVkCLVN3LX1hI6aK8G856AwvOOmLaFDq+VHYyBS5hOQGIiKCdzAhWrPi9X5DI8/N98KrLWZFlh/qeqe478+fV1fU4kkdU702zj+zp3HMkOy7ngHW99cCqstZV2lK4rdn5FNHpfqZfVV1S+SKgpS+scLUAyFkBRztU8SghaFW/TdPyIzQFX+Z5gQ+64ZoSXXamdEnEZ9/7cMkelb3Z/C0d8Bhye04/rt3YXVvjn0UQnoExTZM2h9nTaxc855wsEQBrzVWOnti53mV32dffv+P0I/vRazUEN6T77pcamyzNhj0JmIo9b0zzq96o7+iw3//Ec08/feMphffwrXcU6rLZWKl2c9AMRoRhg3BKMai/EnRZwfBVazoCaa1cszOQu4bhzblSlWYCVcLwoJudS72tRcFavNIRbS+5rK520br2YxF0SYiCBEKyNiPWuFiTTZOniWmaNDdgOp7wfH2ELlLdvqwrwiqFXeag01D8IkrCMxC/zHekVWqiwv4W7PuRItwuT5L3itrko6IA1EZjQRhWtxNmWQbRM5ef+gUDy0g8z0sMyyJYAKE8dSjZulRIlLG/fv6XwR4rhUoRHVENyRZlQyoorsFNcKbya1WpXq8i42NillDMa/g2E/irSGb7OXqHSvpzIGTPE/gpUZDab4QTaFsamSYdJjt3ZQ1rRKJozSj2xl2ddsymPrtIyg/FQSulJ6zjWGHTVZGMl0lPnh+AnqSxk7WItlevKQ27DPvRzRMXEs5o/IPQP5hA7hZe91b9Y3g7MjTtoMgT8q+UZVnCzvX4KypA1U6Tw93nWJcQe1GtmgOGqxUrS0b3jqenUdVwNMydatjZaNq5vKPJzDV2LO+E3cn/lg5rUoNYd2QS1kS5hKIWI2wHb0S4IZAPB1k4I711+kpwhEc4XnQYjrlP7J6h2fRZh8MHXa5ZuT9bRD4LR1vq5DGqijdIss3PwIpEBAD/EckFPgf/CHh+6aKYFCKPq6BHcjcFeKH02Ar3YdX39ZyL833K5M6RkEyBQK7lGK5BQWd7yNIS6kYiJTmHvYm66ghv/UmP48zvdz680RczvmEOFWDrveO+p5UdHePV4w573ZaOm1j8JRue1eu72471bTfNGzjbXnSK+1K6+dznmC8P1qrHItYnwBfTqutW4Ii2jHVt6ZH4aOjhQ+5v1DFFVzQNXANzbV5FRSN4/H+Z7v9fV+AjrEMzRrPSj9yF1XkPKY3WeOYe7wkDa15A/Jpq0YYgAohqJUmGDUM/fr1+qvoAbe2s6boqRzws9ehPnhwdISODSwn03vdZ751+JFn4l7dOVca3lWHddZ2tXMkrl7RZ94+DEhGJgaNFLBJxUaSxwsorrAoJ81jHGPODTx9PH+MJ6G5YdMo4FCHFRmCkWfim8Zjc6UopvRjPppRcTAi7CktIS7hYksLGXyxV8IKZYYiZ/GaJh78Nsift36JIfXZ7HerfnhPwGJL+tvyKAdcsfDh3HIvv1r6n757j5Kibw7bMbLS6pf04UkAFBJK8XvfG6mqvuzkqv4V09yOEswK96oEHVtGgkVFeiX2m8JlYT607ffxveVsq5kkTRfiiaWHRLTyXD5qeuH3Q3z4ASmGLkKSXiixWB3JorlFz+jnqmgbNy2FFEOLs2MadwVdNOfhT6fm8e0Izxe+Fhgb+fhuZ/r/KNQvG11QXsanLwjwnazgG0M4HM8sTykqMCZkPxkGnKaEqvlKfOnQ0Dlpkv5BdmR0vxWcvtV44e+6S3W1zn5TP5gXKeCSB/luHCj5Jo9FhG7FpOeiEcvx/q6BrlWS24+nm6VxpZsXElxurAKNt0ba+O9UHw/nopLeV/r5wPNU8lRtO9f9boltbmfJYzKB6UV3t+4pNRjD9mmJddrr0uYgfm/3wHXuUHPmD7uMJYb07svtRTXdPrXhZQarNMFZlDqsLOyANbwQCU181m7qHNqvNdI/aliUsW6qRunEYt9r+X9EmqnN71D254Budli5kM9jXqp8Zci1D+/2KXfEeNnImcO5cISCDIyjrMzIzUPpeX6G/dL35q0u2jXEpOjTztS85xbX1ufcMIrX0tolia1lbW9nqupa2isTWTGkD8COy1OGy95PM1kR3QZumNZAYqGNhK754sEyXWl14XGfxRkUWNzO9zczBSNHOl8Ucx5Gq8nCvonQdrHivo8NjNm32xm7J3N6+PfMfLUD1rt7NGbDfLz1g3wzFzCLWwJXnlZdviAmNoQD7Nl0ucnP4LUyKaLPM877YbG1Dbba0jUmpUfijYk5jpjgRPUu5RFks7e0HD3S0WayzD75S7apcQzUA8VC5UuIlg8AzPBJ4ojCb7J0bcQ2CJATgaOp0M6RDcC3+RGg040sHsShwZNQjWdETfgnAbtFqBY0ckfxnAo/EeAb3WKZCgXPogE8QQWU8FT9hmOccClu++kqwqSMjHl13LlRQVGHS1R4YeDMIvJpjqxWxrMHNolV1Ofou4FkfAAy5UKtVZ5ZaF0iIgA+LQ9ZAj8O1YhFDk6O+dH3XvVFoEoLLS0oqX9HUqdTkpNFICjn2kq7oPumqKQlyTkipToKBS9dYqrRbnlz16gGd4Z2et643X1SAbem9NTIie4yOQPKe0bcWmXAAgu4DDo/h25Dd+9ZhIci0PAR+y+HJGwZB/6VffkUc7kcSuUN+2fmnm/zlCjBz6xcLSU/OPw2XeBcc3RPRbftZft7MIMY8P08Evd4Hx0enE0TKeGdFPnk4XK/TE1LtrIaL30SRRAOyISucnSoscU2j1/s1Q3ouZPJj9g0E7Gt0tRDem25RLffA4VGS4rzrrsrKwfnNm6cKv4oGCwpcB/8aBhfRJPJZpq6nsONFDLNZVr2jJki2gtlZTEs38a31bW+XfoxCRamfjgkkdGHj53PxX3OgnUr6Swvf/0h3GcPIpgahc9Gj1MTfUrICerK6EvzVVNidNZoVXJHMMg7HG/49FLP7xwaER99H+b9IL4jdgV9POvZ6nU4vVGT4AgEf1zwn7vZxigWvSqLEBiG9r81lumSFafazBSskPIaXUhWDC+QmkMN98J9yWMDSfl1FMdqs4m2zWhTV5u5uQTDaHVrJZ9iB6eFFk7hwCWQ2TBSjNlmR1SJdym4YPRu2UGh2TCZzlXD5hucpskl9EU1lcnXyyuk7t+SYVp9XXFaU1jVK1KFh3e4jTucQLE5hHEhpdty+j4PEwpYkueRUdQnVuCGDqfTdETk6RLyAdQgR5dwmNo/M521FViKERyj4/UGyoUo2CiunF8Vj/o+e1bIsbxof70WNO3c2OtJdk9L1dXHCIygPYs5z0aRoFnguifuRcQ8vKsLJu+1IsSKZAhSdXKXRaW+sSri0Yb2xDC2NmcEBA1NOev7fi4OjAw4ebEd3R7umCiuqX8WhsMswRn4vpiw9jQotHzCKLP85FbU7WAsFZEFoWT6c7mf+IHcziSYnTRKTlVLSlZFW9RVKJCRiCYszFkSLc2lyWHW6Q04ptnfIqfLvX9ZtiF7rsZU/2B/4pCdVI/WsDPgBs1G390L55pQEU8FziIuLzEkzwf355j7b1IV1abV2h5IonaJojgipq7HvPz92vD/c67ii7rpuDkGeohSTUqN6HNMgG86PaoXxFq+GAbOtj2DS9xmUdeRkxvSwCk5M+5+61p4SmQM9w/5uX5PhJNOgTzrkh0aQDvwL4cfxI72LfjRh/XKLwvdyeDLzY6K9hrTvFpXFl8lfbinJ/OzzyBKQbl6TaauSKA/Ezsf6xfqu+Y9Jryy7q9H0umIl8LfltAa23DHp6n462NIZakJxXycf57ZrQ3B2Yud8iH5eYPjlUvhcVZWXs6I3vEYBrJ1z+txNB8yTR1fKoWxaNmwysVbWS8MJ+UEzxwiCcaTe0U/ju3XOb2NFVv9Yya4oSDijZE1iw8vOqocDWu/psPosYPriyvw0tVS4REmSSfItL0f0xPb0SI3rvnTLAURli/AL+AgOAHe/UYWHtScqjXU2b2MMmHy9/Yet472LrUuv+UZnBgPa5quwcVGazlkXVTszkjUvFyJw+nK/7D0x5HF5RN32X9TBS6QcrHlSC36ZQFOnqD988dfs90jwS5oa3RX8UCnBdmkKMPoUOzlZ4WYJWysYoba78xGSDBBcV5idZHl0k2+atUyTaUbVf+nMmUtOUsmL9Rkk0VU/D5LTpqmp/puK7FOxSybEwEPl8M13xPF0WDH077cUpY0bZ7Z27SCIWyNVGr+fnn7vPcOtjoiTigTeOSat9mt++jZTUVXtLZhEb2ji0ap8PhWqdZ4+I02b1snbJvMvjbCl/QsWyeL+zjU5mfAZNeU3uP0OckQa/JleX2g23Xcge7qV7Au2a5KdzoaHHzt6TwcjinTXxD3Uk7qoCsXfu4LNNps5uJXYhd1SXeh1qSkkdd9Twgu2Uv9mDthd5DDp/sXrhy0hjY2Wlpan8r92tS+P4p38LH+JD0lT0l5+uyx0rR5NThopHklKhljF1pqC7XHL70kPRwTBd+YMR0qrio2k7nFPjaFzsHpdaSWUTxq16skZz/mrHAHySqJMpcY1sx7PUNrs+35IM06lHleyYYTr7SSFKe1ldPo/i15dXAoZHDSWGQdNr1FTVIS1rMwaQR5vAqAepKTymKw4vajXPdrzx9XJEpWA9XF7RtfkY9uqCo9kFGhr25882jTJGLLHR6VU3RWHWaw5UTPG8GT4YYYRHQ4/6oq9NLfxIHBYhb2R1VpIZt4GFmZEd7jRvvb5wAPqZzH1D43ilsVJmonrF5aak/usA7CoXVy3qBUX6Ftt+5JGkvjMgd56/c9qu8yvxc/Tf5y2bSndEPwn8e41e9/70F+SMmjJpo251AbtXPo4ReTkI+sS603G4sa3PF6GwMH88bd0jVlf4KA0Qsl5mdbMkuLaTvn3FK8yj+qvLdpoQmgbQTSxif3EYrjnfyGpxVYY2gyUdV5dWGL22pBq5obh+u63i+UaECB5O2To2ImOHxLRRbjOmZK/OgyY3zirDK9OTFd2PRulVL+KtWPFcCXqxt2ZquLFYtXSHK8MCgLvxuShnEzM48z3O5yGHG+BIwaLFUfcsu6vrWMbNCaOmVpSsjyrXKr9SEgUNxQvtBN1ZFxgXOT2LzOnMk/xSlAyq5uh0ded3X7wfujMipnEOGvph5X6YvIv2GCfgMOuwggbCgJ0ylirYfKw6IGBSnk5dCJVw7LgxHbXlsVP2u3NbOXtTUcH2qYsTFAa843K8IBmP3VCHp6UoHbwCUFNmz3Y+DxHW0J4wkNtKvgVsYR1f/xbZdSwYdJIxdF/m+xcDflPna3G5lDF11LV92oUe8pA65liJ5jpJ9I1f5Gl+6H1JqbggiuzjANpM8vp6hzqFmkRVJjiDM0bae6F4hpHOWRGyLQClfg+SeDnADz0y8zU0drTcR2pT+O57Vcffiv+OXqeflo+Pz42MbstMzi2d4SaoOLCZ47eRGFX4Tl4HoZd+9P82AqP4keEzRzbHtnlum6vxi50ptB2BBs5WrZm43LbnLkNpc/INaKVQe+7u1Hj2NN/KrLkXmZnNrJZM9CnG1cqOqvXbghRJgsV/2cbMu4zx7KGFWw8qKIz/eWfE7X/5NGQXrXJiK34p779NxNtxYHY9AcYKqddn2NmQjONJC1Pym25lBRe11V5MyH+k7jEiFuEpCKVjJEl5ysT4pIo7/+okn+KS6hZ5BUEP4Y4GotIxFp5es1y6rcEzWk+ETuwlmMnWa2FdbDLvqgIDu+Gm2rfcYXVklsymY4d2xaylDS3ShLGgJiGRjCoa88d/cw+DUazJMOXQ42mFTpnyuN1R4OLHxrY/CWWv+EwbIaq1xTXptXbJ01j24zNdpciddVYG+lsgia/WPiMsjafBLp557s1k+dSbuV81P3RR1J6Jc208KcCszk+zih5t9e4U82SE+syYMJXu+dfyp4jfndb4t6YBsbIkG5FzJjs81cBD/vejbcMCkJlJYk+8WELNh9awRjPDM3w+Et5rLSqHI79vHt29P+xvkdVSo9vsmcze0qpiXKIpCpTox3CvPxNhHvDDfZYtmTz/6gZJSJz7AdWIjiQiERXfTsX2sh0TmCRzYpdBr8kli2QVfSxDcNoETnIW7oHmd1KCBGpBDJ6nTClGZXHpCylaP8M9CcGVhLtfMidjX1uHobpAhAI4Xp5Rqxs6OYSFaUOMNosZc9CqkdRZEny3Ht80DP+X+C+Xs/kayXiWI5wHHsbSLm1szJZrr7pHT5rRpoySPDBNqSoh9gNjMj3V45EL9LWHKtsxD13inlMOMQj9wzKTg6lMcZ0SHI2Lo08xz0XWYp1Z4gl9B2FSv+2U+O5tS7/9S2Bkvgp/BP7/HOhqenqVYsmmcfXpk2/ocvX1dXcFrXpkTV9A6QsxxHOFHPZMe/zRQrVL/V19e8QqCwQt3/g2ENZ0b2gdEGfHkF6jnbtRobGYhKzcKNZPXkMDJ3bJth+yZreTb3md3xca2zpJQJf+cALIi/CTCqCDU7RCXt2uEVx7ZYkAgzPm2zirNx7BjAA6OU5zojhRpN0EudLCBzZ2QgFc31+pBoG3BTeyzAPSUw0h7M7UcPgAOYnjkLY/NXXtoPQ+3D4V4Klrqcj+ZZPR0dutbcfffPomMfNmhNtekVnD3DrR8irIdEWGfcB+mAz/Xs0nNfQ9PzzTShawW3p4HusYv6HEfKWuaP5n0rD6U1uVsfLcmXx6ube4/xy/Qiv1uLwk518dflLqmYsXGJOf6Fd3bhOExTJLTljwlqYO9RNpjuiu6sBvexm4/9NbfjkvIrK/OL164vzQ+t5Kwpa3zeuhaI9vz0OamDUM+oE53DWsDPBGJiGrr/+RgprmKYvmf/I/3/3m8tt4vvNeMXMOZsHaWjISeLeqm9uTlSRp0LDMTtf2rDx6eidBqx1b4JBLw5u9m4ePJjrl/AZNSuKxJud/tJubPz9E+kf9eLh1rrVowbBM6Pjdp8WDG7kz7knHExQ7PgYXIjzCbFdU2GBdnKrfJziqRwE9nSmfYLb0qVN9lbJfQ3abEF4FwRLcs1QYmYnpZpZKAleXaO709y0ys1hA8Tba6ePPRbroRMtWNZGP4k+LiOqmCVVOnFfaoVFOCqO8G8apy4oqDtNul8Di9YsnN6546yr3Zi6Tj14fXZtWRt/sI1gqS5LiLylhW2CCFYnu+Di5qzTKvKgGY4zS+YZF8d1AwsCVN86xYcbAYr9vmsA8//pONfluCpLelILJFvy0aeRRCQ2i85zktLO5Z9ZH6UrrWaQhF1EiyzHCdIlxqaz8TzmRZ3IU67cS+2r7+gg+kKz+dn/ULv6+mqSq5OM1O+pvXthypz1rL/BLK9M9DIDwGYrHXh5kXBr+Cr6xNqulK7adF3WsbH1i4aH4ps3Jb0freXd9erVlqur3ZdkY1vfkd7ZOlbwUKO2R0rvJdKro1/pKF5cPya79Gxc6ZTgcOD6a9cEzuNi0W5zHeIblzlRM369TqbVPTni1fxt28MjWIS9quOcV2Z2aqdwdSyCT1l+KdLdZf72jHnO92pg4ry+A14Jpvv0d3Y+GwGtcgvx9gmFgUd0EkeRmIfPDdMilqS1v03UcYQVkcguZUEneHMOuJE7P0XQnt2GDbjAsjkuO3fTBnDf6i4DP+jmFGMmw9AZHs8r6axR6O6N3Xf4H3X94IRVd4ceKfcSzOcSIX60dRyG/ITJ7D+trV4ZGuxJwys3/U/Zk5sEGYkYFyFqvK6LEz6Jw6LnhoVULKd5u184FQBFxvS8ac33pYHBjOBA8z2O6XKF0Q+0oHr8MRAGEaGJNcaWT19sKZlkNcv7lIkNFPmTNt3+KnGbqdiwoSJU7pdWCX7axSjlFtxNredJgxDFAZu6Gx29YS1I+dMTlopmQHoInbqPQgwa5hyiT9LHmtQgQUErLVw6lxSK8dFO3xpf8XCHhJzGiZIILN0sEvXsq3qfjiK98TDjgco17U3nMzA1fQW56OlVVSbficnbS3VTbb0buRF1s4zlMbt4lMt/E9+fjDeq6sembvsRkAZXMhLFbVezDPz+/bidrKvbRGJBPNrDF0T3RHZjSLJ8r3wZmE/klcar2NSS55+3+aJj6ZC155mQt7JiljC32W4RuiFCdkI3VlksPneiFGkqLG5qHGpqGmpsKqYLPySwL182e/On/lLYlehaVFd02Ljuu9H4UD5UcnRFpPFwEaqr7b8SnBQ84d7DmB/FUb/Y6D9Ob1737nmeDioNDvL7edu6cv1foEUQ8RfUt4MR0QHg/rnpK39RXdRluO+2F4P00XA+DImJlUyTpzPvyLFvJA/HcdK9XDbdMa76cLQpxGN0bVdnV9e+fUCGrtEtBh+zuAWNgoAY5g6GYCkptvuaivOBemTlIbfb9T6fvr3oDTYTDMH0mj2TNXz9DnX7kbAJMHjDE0k4MgSFj/PB2uS9az8dWEr1PVBdW+vxqWem1f9jhiW7X/1iDo9eghJ21UzI7MSdpwlPsCiKmLBQEWlDz4IsfG+YpNGhZJ+WI5mCpEgtod02royg759H8wtKFGERYlA7lX2BFg0CK7FKF2mWtFKH0qBRcZXGuih64hP2mrGNxVcmJv7Rk2dTTZA+Vsqj2Whyx1U5Sy5FzzGXFU8+yQm8qRCZTMKFJ3nxcol0dx7njjt2HEhly1jYWceCdkNmcgZq/tYz5QAGunaK4bSuxY3c7kNiu/ZVGD1nNLFyaPnQwVnP/FWVN+ZNz+KPkbUmxAAjMjAO2BWZmtklz2/WV+ai46w4cJlnkkiyxR8n0BnrGU5hxTyeFunCZZAW8YUjmGsY4oYMNkywZFuW0CD3ka3h1hxGDMJNQhoyeBOaslDLihOiYcP2DYNh1+GH1E9U3Kdm1Pfp3/YLzF/WB9cfb48UdgC2xwB2YsngLVanjZMfv9DOHid31e8f7pmfT9rqaVg3+Wo7HuZu3L9XyN6qyHL1oCfFDTYat4TSu7N6toRENtuvQ9+WkoflDrFFwFqEnUzOD9YtoUUEITFv45xchz6urzymmWJqVClzl3OVsu4SnwwX1w44o0PwsYlVt/Dcx3GvXcG95I5gvCJMDHqCid5kOvNed4GurvaO86R8WHwAvSMnf+WQQcFx7ByaYyGg20ncDbOXRO8R9xOnpga4kNHi9hmtvOP2EdtanaCKmcwfv3dwPCpEfNPY15kauoJ1TCVDci+F5ya/Mdz/BZXG3HDR8qjjIuMJW2BYRuvx4B1hQ3BiQFzuzrsVjdy3wie+CYTzdpFcFuVwx5fwsLPZsn0c6koDf1pE/F5dhPyiUzcRmUVrbpSIu0Bdnaej/XFrO/1i4z3B7IslKarg1tRjHgggXhkc7SynTn3bVicnVRuhK1dp1rd3+7AwIOBuX70twqrcavFstR4YwjWv/2XjbTyJEJrEhjPZTRYwHMHw4Rz3KA9uLY675x6oNlGBE8C5ikcU4Yg4PCT7to/juvKgzXKY30oIPn5THSToV7BN30KtC11sG1oRhOg5WqFgaRa7TWkgIjdJ5Zt17CILwDbaBsVGogGi/gjrEBD+FNsS7hp7xp9sDO8hMyh2E65NCAw5sycHObXwUIlWwhOy8MTuCc8vVxSZU4rCbvDNNrK+8UBkSW4KN8j2zlzgp2UcIih7QGIXixcTzI7qutnF5sHv/uh/eKSSKANP612M03l1cQfg/EyaBMdNTkr79knGSs9j+Y3TNZjxNs06PypSrPBsz2m86yW0P0yQqWj/nBEPibjaDRTzcUQhH5XffPRYOS0j6wzSzVf4IMwG2DrZX4dnNWARW9GSEVvcSyIg7AB01dkmufjKYy0YMFost61WGfmfDZ319R0jsdqYqamNa+b0a5qb1xQ3yYaWGxHl3y8kBYJy0VSanpKkqenqxnZvu1npuSGoRiP1xu8+RviCqqm0xP9IMOJy79JHU6c0pyiUFv0fnQP6pASOeBaRmH8TYQbP28HtUIcx/W9p661C7EuPvY22RfHSxBUyNKafoNPhkI6xWhxSqXvUkvZMJsVcOKHONsC0SHTgMVhYa7UY2DiDJPIie9K8Ld17zcwBVcL2H/IsiS8eK3btTH7zHc3SriubKybxJOy+uuPtN4vUzfTO5ARbbusPZyyJbaeOnRzflfzm2FdjfRRx3iNclknwR0aeOVrDKPCJLE1fQ8Qem0ZyhBOL1Gp8oDP88LBaVANnSvoWnBG/6M/j7vs8O3eOe9yH8SKWcxXVjpKFlZWb/l15W5hf28E2cpDs12YzkLvctF//e/hImVUcXJwVkxuVmdBSnpJ1BgVri/ZnpmqMSZkhGXXUFJS6gL+CU45hE5VcUb+SGst8iCrtJGyOOSfz1bSsBa0+6z9M9Vv1vL6wbrC3StlX1KqkTmWOU6tHZ6fFRNYS5oeXYlZAty67XdK/vTma/n0yxeNxxr/3lf74b5yBNvdQf3XlShz1cvWfT2lW11Gcft1Inqaui8dBYv//vXyNeXc1hPn25IOnLdrA/WdfGjOt3/X6s9X5FQ4hjPUv9nLPct+RWj1UdB7dOxuXDJ6P2tExm509O54AWWNKv7thJHYb4x7LTsrj87b8RxIvZ91/G3XmhdIFEpT8HAgRa8Un+Je3qlpbVL22qrzRxclH5TZkE6FY5hx1IowdmpVxC+UuYdLGqo3nclpIThwe2JmlCIxJj5CPfEB4PMECwzIIml9t9HOH1gSRFLBrZ56zkOTXp59uist2iqAd6Xo/Std5dOnJmbrMnbSSKOkQ/bX4h9f7ISLJ60X5aHV8HTHnbsPIaqOmqM/gVf0+NWw/hhwCYV93BYab6KaWEQ2hXAWqXSeK6LNV4UAMXW8kBLbqZ4eaHWyI++YtjcFlSztqa362agyJ2xzivnRXXAPZ2kDiygFQ7FIf2Nee/vBUSP0W27pg9SXXxVMLu7H7V7C0IvgB+gPBp9LYMhhakDAlETY5pldUz/heyhJBNvQTIxFc4Ke2XcRFxMGIjJBHjkI/1t02AadWBM7R1p5fwXjzMUb23axQ3MX6B0HzHttSp9VvznpuqgFvJMQPUIRLcx0bOtaT/9hT1S5grE4q1nGGa5KTqozj22Pca9R8wfvprwXdr8amzWzuaQmTDUxqCQvfPJO2xo9Mh0xTMvICmiIInADdDPpYnH+T7vYa0LuGDNP0tKpL79IPjwJIPGtlFU0qAg85+J7G6fSx9NH0vemwI65lnZ7UP9ztSv+Xv6/gL9l/uTgDQXGT40F0aQGs2KokyvlqnVdgEcYZDW1l0XjKWvHQpm48aO/zqScne6JYJ/bKr/k6+MVeVRxQFpjNebnz99yjbrKqEJW6g3jYVOcDGqoZPH4htKpKBxXuaUH0YHO4Z0/PHq79p6hQ9bTBH0uaYHWiP08tU5tNo08iZLjOOt1SZ5F1QfeAc3Y/PN/ukfxHCj7LX6KktbhxZPTs6Mijo4Pr1BXujAYfhp6bI7rX7FCLFdjA6FRTlJaa9vCt1tPSjN9AOMarq+COf3WtScrVaCqJir5KeaVfe4U6knwSgp9YsIk0mcp6a1aZcXmQfFA5Nq/qrSlLMSe1kk0FaTX4ThGeSI5OnsAH7thjKJSjsHn4DZkmVWJi4qYk7Verqqryy8vpcsexsPmOv/rFffWn25k5Ezl+6f0ZBrkvOWlzUvLmpER4/t9a3fV2D+/Dd+pzjc6XMUc3zKVa044vj7hcqtpV+B20VcofI85PSPNvoC+HmaQvztS0/vjl9G7YIIGSztt+S1f8jzZCKGFzrOpS2axqRXfG5QqzO85xKRINpwKzBduNM9ipPXQoy2XA7H3M/qCBAWnfvjmOs0pQLiaHn1+87JlnTh1iQbCw4HXB+UfbkjqbNJEgJTcEoynY9fmTkxoUH5QNn0FRkKdVslg9ZvPiDzUhuCX3wtJi/ZsbOjqq3pdf7rBH+mfPP//Z0f7F6GiMaCNeViA8M2N+hM0f1h61fGiq/1XNwjXKF33vTHdBnU4HKDcWbGuz2eJO2TP5ldeOu2eSd8Q/ZE9riDPZwjAGNlHDL9hKAkKj7yZCCLA9CTMY4UXwYUY+JNo6ISjHQTsPLDNvuDIGwMX43dsILy1/NZ1SGJyppw8U9Hzpyk8Kk9l4RVvTira04SMxiZXyL937uxRy5EFfoHm0MUBp9C8dPs2KLXc8rxJokw+Ho3BsfaTJuuLMcr/uG+s3uoUtnderclt/0wrrr9FLujJrmY7uI5zvOz2XL9+avezRXBrN2fzAxr97WSbzN7ffhcDTl/1mPFF/TBvdvDL+1XD7G2fzWt+GW0ua+QMuytOFj1jPWx5ZhvVCHk4fjQi/bC15T/fb3dXJbcPBdUJRQoXB0k5l8LG4YkEwmHhCW4NsSrbWyhXYWPhKSZQMxnMOm+3swwzD7SUnBiBnSx2L34PJkq5N59N+kYVC5KMyXjYq1/GyvYpV0uw6MfYlFj3VCkKybbwPLfPLAlJK5nle4FfJ2ZLzJLmYCBnCKNgbQzV8wWTlzS7UWa126M8Y9r0PLBMLcR7xmpC+GRS7JW6m4rrW97z+fsmr0AZhvonw1iLOPf/T0FNdXfXxaVRa/FujC1va2wO37RASghhGRFi4Hc0sS/wi0/ykjIvSUxCtP5Qbh0wYY33hpNVirttwtD6djXC2WCYBlMe8LbZWpyPIItYSbR9e0qHXAj4E5sMIgSDf/Y/UCSWcxot5p8B6cePzz7JPweOQBo6nCSeN2r6mJhrvV0bCsLzNBMmeI4Pj6/JxqL84GTKgEEux/pK92HAWZ7MjyjlqdY/Cxy6WckbNqqE8qi1RWD18cJHHj6WIbN+z29UD6mGHmkhApJlEHBRJPZtoE68Sw6pZuue8Cd2l5zk1zN1YtQjbp2Y3Er65dLyHFfGXYbOaZXCPpJ7GfW8ydcvRSwFXAo1pVm3+fR2BhdrhfwXnejvWTZZ4odpvZjukwGo8qzBjUk236kkREkBo4+z1wBa4+nhBrf4NKpyk9KEXI8zq1UndVQJpCjrju3+kXdeC58Do/BUWqZuV3q5rQEw0Wk006avZmCy0zKgPiGnOjhFN1KvORrVxpxZ9mwvRUN/M0NT0YNt4Ogszu8/LlH0z5seZutk9fOw51GHzsEm2ybsqYiT9mZPo7j7UeIwFc95VQFXlHuiigkmJ9v2x9yR07jE8j0NhV/faM4in/hnkvVr1fByi0X+TY1nf+xuG1Yy2Xu9nFGgz/z0k5Z7Czyxq7odbx9LnXS659B48EoGxLznqzRCxwr/VfD5Q2PzciALNzIufNkL64A4F/44Y+yiasvIv/Hsyh9OOvfTXZOZmInov9Bg/pRQpnz0BBRr/h07MuzPdfC57o4WKC9D7FF2gGO423NgkS88cRC+dmcxQkwmbEczgH6438tM4y91YhGghl04Tw3uWP1fCHc1z/C2rQHaWLzc49A7KabJ4S5Rz9EGe8EYZw/j55URKn/aNCJN6dXL3bR/d59xTg4mZ0YWCr7twLvz67XToVK+eToiY79BgV8+SslnbfMy5W1VcwyKepLffasKEwUT8MKjd4RYiZijdT3nz+EncxFBze9HryXtBtA4WS5h1OMPnEndB1M8JhFZshZV4kM8JWeNyPh+jmtuA3WDmF0qQZMcOfFk/3XvHLzEYkMEHYPFSkSyTjZFXIFwXE9c/ilkWWI+G7ApL1jW9hnueIELOTsUQDAeihIeGQa1huCHW0n5LZ3WIWlCZ79S3OnTLDIas2fR/z8MDU+/+EVpNqH13bNuJt8bH/2l0kf58SirreRYRhoYuPNr6Pj/bqDm1pwAhoIFnJKLoWBQDb3xaBQ2dsqOefOCdE/EZs2GGI5j5KbrGt63V15GaRFSdAeBvnq0JuGbStAY61INhQ5viMZ6xzmDC9ckwZjcBheZWpwV8j0NT/NLCwl8g01Zz9Hp9kjyKjTKq9wJ02jOyLKg+g0tqpwzN+r3UTNmZThoKVSzDAsC50jVRFTc3FafqbulUqnmVZi3LtNLN93nx0UZbQHraA190zTJK7PGGaOYfJFwz0isGLx//5fa/f7ENnVa/Y/vwqO//FZX9fbM6KU+gJRbKtn8UUY/xf43cotpxu9/siWZi70UCjoh8CXr11amLryqTA+aV5v/1mW+77VWKGsj3KuxrxEN1oA7qK/PsZhjE33CzOImFJXTz+2KnAGt/F7f/QGvXAaED7vzdJoSWGLGHwUDv2VBe6W5eCY4ZcDPUPdqQegMMU7yyCOIjilYbvHtT32PEW5C6Hw8Gke3L/7OD3Cd1BscT90/5MNtTNWn+7znSW07IjG3A4/S7YVQ/Bi1xNZJ5HaQRBDuq/D6DEAs45rgr900RD3k3MPLoo9NaH8IIXk5W0bqLdIgdWyUr6ezlLKTDhhTM5Jro7lVaKv/ow0fzEbUquns1MBJC0FZ3C76XRbiZwdoAwoqs4bEUbLRpzHNrulNZ8AmKQBGGd94yy347fGU4upvT473BQYMkQFWt6LCBkaKIMSaLNuQUnZh1OwmCqJ5bWmpl3sgvt1qkaYBq8Tx1yZhn8bp07F7P1m2znmU24WQARk7szLnYSKzaRtfv4PrgZ+izzeAtRpjTUsr2Sk19MGQQzSxcRkEUyKHpx6oZHuFC3Jg5iYE0l9L+RzHeZzhW+/54pKfpkMc9zjb9DcITl3pjMNcXCDSmrQ3nm1oxjaFc1Wq1Ty2RykV5c/EZIV/mOa9maPwgmQR2GPEWD+zkktUFtxsRe24lf5t/Mqr80ySDATyufp3GW56M2B15pnFVU2NZl4zZOdT04JonIl+jT5s8OLbFKLqTPPbg1U5SUDG2YmKFvqJlQ1pjZtexFUdXFFYk3dXz7sY/bezx0/VryByub0yGyQ91vZGrH7f4VaRf4r39i58M5GI1u3cby07ju1zGcQ+7XpvluAWcwKusCuFQfne2xvcSN9Wune8R7s4u9AtjX2PJ+SnvDdZ/fnlqxjwFzltZf/bWlr6cNWG/u2D80f2O8sYx0/D6leDcSV+lWTG4QhOvoW8xGp9kEg/YbloVNxHWhmxziREEn7FYyk4EnmPVL+IOIWJC4QNbrAI393+J5iNfUez4G665RGOimM8/+OCmPDaYRueShgx8/cWk01Bb9C73wCr5ima5WvoM5s/1/Ds+d+DhJ1eGJx6WaL2Jr16lmuWSfSRmVV8WtKW2BsqdpT5LWNveDnQQOewy3BthHwTzbm2yHI3a+nyFo/Kdui2xnywW36HZ2NDgcScaeX7j2ITnT6Yei8WzZfP4yIinvV36+1rGqzsHG9e8+mp391CBi4hrlU9LtbWrK2JKWoMkFQwP//7e0YZ++Q3zbFDt3EIPoYNtrtj++QRsEOknGDUZRqkHqWBlhJHY/8LsuF9wL6dNh0N+oZ+tguDnncRiVoTqKuctf8mZcx0lFlILPb10ScULUVuL6kwXpVlKKwCrs8flHYYr/+45/9D+uohzD7UO5f5Z2k4l/8v/LEjxhG3K58+P7j0/e/ZRd2ylsuT1t+Oz4t9+vYSgpS1lReyGfXlZ1f19NTmDWYM52lhIdvo0mp1kLQ4Wrx6eXTgwZGyRhbjc8tn0YtAtUnnFqGfzPoaqKB+99RTatWvDhlu62q4UzwDHe3g/764m4PMIXrmisvfVXRrvOKiwc+eo1Tob1Jy8xW0eNJtoHT2wPnVnrZi0wJi0xJjIfYQpuFMQ7RDz3B0FdttKPSuzQESd1JbWNGxJUkPRyzQ0ErQTF/ndGCKj6xmedZDgJ/y06KaPfY3nZgYh/y4yvLqNfyYk5BL7UNxI7POVAH6OH2jYCjo9rK9KH7kEBXmx44Y7h31rA53fHLHJt7xaxYP9JlOl3nOseBPbanowUeVURY68sfbxXLV83dWaxPIziFl74pPs7PPsYbGbI1sWH5lfzutUO2qXvVJ/02Zz8JJg+5KR5BKZIqXBk2TKtriFF5d2TOhdW5Lb60JGLpfLkyp9tM/kdvGv5Br5r9mVv7b5X/dij2wPsGN51uBiCUusLMsuC+wkayDHdzaEyGOedgsCXnQuIp+b9RmkGUk/NAxdKj3zggwRscjlknipRfhfSXXpxurSiMp5Dx/EwIgicZyf0Ipn5Yn0dcsDPd/ZnMRp+xLO8u2+IKf6ls5fZ3mjrK1tf3v72oJ1pR9OlztcyIUche9V+X77jUDyDG03yw6lU/nttWIvNdoV+oOJYNZuwArrHBlbLmbt7QHzjGnmMc4OfVI4AW0I4SygDecGggDOhkY52nHGywSXn6DH0fFwJvw4Vu7MbJIbDKl0urRWCxOC6FgYHQiRMuj7CUulA8H5b8n3T0wmQChs1S5EGVUxqkK5+DUsZskCpdGYX2ssrGpWrKym6dU3FNcyWYzPyUPEYlBKbEFSYmtBYkpBbHKQoj77mCWKsjpyENX2RqzhIz/i+1FEt73r/REfcVGQbER3K1QfORi2jIssOJY9sbuO6GLbs5O63fSG7F3R9OhNa4EJ38Tc8pMJvcguAqhjDssoaQ5BwTt5SdqQKIPaL3RG5ayTLFMZVugLekzfJ2+6YK/pwtMmajF355+nnbXlQ/tsobw+q9yjVMuTpt5If2lKl46n/iBXP1ZeUVpL1eZGwQnHSfCU83WPBxM/UJ5XF2pKC9Ob2k4U9fq6G+BJ27U6e6akDuuky+lbSHxWX6MEVXcl4ABuSUJXIs34Bz9fsD0/qCsCz4vABwXpbS+AFn++mDI7L4v5o6yfjIDx8yM0sPviKx54fmi3sJxQGZ/Zt/vF9evf0TDJK6RXX8uKjN1Dn4vQKA/HaMY1Hy7hhppWx0Yefnx8PM2vyU+OHqGtkqdvi7oPK/etDxclxESLxESK4oInEO7rVcS1VuVUtcbLoWvXjd2eVs4nM6+ceKtkXT7WeXSrlAPZFcO9AuwfGBuA0h+4w9PEbw+ICUQrbR4dHdFUr7jusSax3hyv7xzf4KAPXsKtntvPj93YuAKV/HkPQWwP8w2DHz14CMYeSul48kmPq/jUVzyLnIjlrS5n0AER3adafBdecqcQQi7OJtpsUFxstTnFTZL8srv1cZHyy8xuS41vJLjWyarJOBMdr8MiomCSvTUZODix09Yu9rkiw+6/Yxo5ShjRkS0Pgnp/I7ObalHftq+M9b32bdQ5RdBfdMXDmHfiapqRuj6SB6X2q0xd93Tt6bcXpxSe7F+gO8kwag4Majnti25BTLZ2K7ChpayuoXve7K/g8SLmK/rfvGeoK4spSWhoEzKRMbc1mtsDqxkfwzNES/M04ggEG/olJbHlf43Noyao4PN654PDSc2L/esPIq0H2PKLX8PLSef6uj598Mt88x80m8tnj4+kV8znbjaNfN5ckW/+kj+/+l9jRPu7IJ+y77rylnB8thx81Rk6NnCWrbnk6ey4ZXqTyMllKBfOK2medBiY7N7445vEf1KZiEa4c9IibKsDExifT/T5crxxfkEI6rmyX2nXVL+5Qrh19271zOzUrdnRUY87wqbvz52Lad+8uT3mZ/+hn8fxZyiJsPoso8Bnwbruuy6f3+XKpogF4qvTbiu0tUg3x7Z9iuHkH5IrCN8HrTqTy7cNZ/KXa3O3Kw77pXO11b6wyzCL+JvxqXXxnVFRUPmjwaMSj8A71sl+ZZBnzUqC/Ylcruw6pZZx/kJF9TeQAMeF3cWyiyzvdDrY3TbY3L/DvFg51VRsyfMqj6rg4wHE1wbYB6a1z9dG60c/Ra76/UheDfTczLIBCZp6QajXbPHP+KkvcntNy5Tv9hfJl4TDBjLHPVO7M1cbjb1VQPX9Zwz+AQj3z1LWR+6UJfV6rpNE+jfdfidl8Xbt6iNrI0t/RJ81+iEh6Xg2wI06anHf+2euOAWlcMV/vtdtOTqRJ9biH+QzPJRjqqSjIfOm0wbf8Xz4ApxZOPrJEf8TkLbrHgSHSO0Kot7WeRn5+cZ+2H3U69sa6kKStqRkrinlmYa2bToWqfuT63m21/jj6Nbf1T809uMX7yGr9q/lmSjbCn1CRbcZlZ42GKwyO99Hyec6afOfN1NcpE1lpA/VGqX+B5w5WGeOyPWOs0mJAfERPR9n/Bbb0XGrs2PwhZay4AjJAGUSO+6FNs//lItQ0bGq+6wPFd3jlu2Uqj5NUq+75wejYbvxZ4Pk1wIiIo5XEZojHyRrPzqcHP8RHDgBzsmW/dVLnhfx9mdWdnZ4Guq3+4kH+JO81dXzVMxRXuu/QnIztzQHQER/P4jjh8vzW+Dt9rP2pwX5ecTW4dqN98b2xe3bFFHjuLKZD2Of/8tgfFuLocyme69V8D15UN0N2DnsoaFu+Cy06xtdVVE51L111cpd1On8RwHIgV54ATkQGa5MZuxoDiEJefCCnf80AIiiiLCBszUM9UQ76Ba7akty1uXwZTPCKJfiglJWA/3zVUh3S6c86F2kpFFpCFm3jbd/EVOW+41GpCvMFYcfOwI9d8yPXh7599W5d/dWVvYYulaxYcMRY0Vpx7r9zjpggSgMfP7vqC0X/rJSWonOLJNCTPPNELOJCXFiQhZBEgrkBPMwBy6+YURWFjsfq/DN8RdPPh8yjQ2xleICIpKkVyKCPCW46KIEgyF9tKSZUCcybky2fmcjVh7xBy5T28Q7cefJ5Cdr5mhEzy00Rh59W2Cce78qKAhLKxu6UnTPChOt4P5wQJe2HuPDbK4/ko+uatAFlAWHSKV7iiVJguj4ktnn8vvV91wWw9BNuPrVLJBjbWwbpK0YW9JnzGfRRJJsspcgWqsssp/MmnffDdlLXS0x2au+nQ8pWKUJ/0w3seyhqFVxWX5BkO/oAeL/whD6movQ6pvg6Nqv/DtzzZ2v38JEHcvBnqdKUUv/EjleXhuF1TsSFarMibtW1majrPxGwx9WfcSySayW3dr7C+V9gXFjG5RFKwQ71YFsyZ/Rn/1xCSgZzCoEnYlsigfmA/v3f2O6ATNONO/lpwZuDyLGOzA+eMc3DEGerRPqefT8bLET6mnVAac5JZ0nP33q04+NWrdo58cvqXdX8Ud5W34lR/1FEYloO9wrqLj43Y3/fX33gyunGlIPqTDfEiF1/tDGdwdKB04n/nJvzI6taHj7X2NM1VdO5jVkffiq97vXfyZ+lOrKmYT+TScf2DP2kKrmXzLyM2UzKQlBqur+2T1D6lFGTnadfPnCtUsX8i9M/+PCyyfbLn+Y9eGun+RszsfIzidX/gROD/Ux9fBr2OrQEfiQMmo0U8efVMqHEk6EK3+tlivPYT5zIakihBBUZ25X1sbXVkXLbt366eoJTzC9mkVhgAuEJ5Iv7aK98Cu6Ze8rp/JyN6w3tgSak5/HaN+B7UvZuv1aWZ3/+5rIm3VKjPcvfSK62+yjF6M3aiwnw8Op4eG0RXXSF7HvRV0X4y1yIpMsYieVNRmpA8cpWbiMSlhMuFr78SSy1q/QH0pq1QGvtO/3W8Amkh9ledfuWSJ9uaAbC1B/gWZoC522xH+My9y0W3bsH7jZv8efvOX932iNQ7fkH4KIMxybpp/4oeolGtMvYAZzvQpj+qQOp5CmeVgSw8IwVvcbuKXyYBysyCwy7X/X+DFfMKniopgajumX+vfjZVFte1flX0pbcMvjM37gTJIKQhIn9RomrkTsxD5WZG3RYe8jCwJ4Es+nTqZW03fHBjPBOc+jwwzD1Pe8q5oPz+eQPK96V/+trAu8b94V/dw4gEUsJzhF50Vx2ES/XY2ZAjiRmvOKru6OmAlOQP4wUuLjtv0pMvGPofj1CcZOv4rmTVLlIfOrbicgW9mzVK8ddbhh2+eOz7dwWHAR8PTJIYDKCZOEI/YWQfvggn0BYvAla6DYVJkrbtx4NEPKOLo0LGadFi1XGk8r19loG7LqU6oQVOXJUDZnkNlMDVL+cYx/fT0/kq9m4tRykjoagcujBncfH4wfvDo4OyH+cqzNNjA6en+8y/z8KDP6O9aidblgSTc4IN0QIVAFtN3Lx/fgsRIfKbNGrWt+HaVnRzj5/BQ48mhUeracBqxRP3akg9w8DahRTdScwR+XPmNDNoVLFv0S/aVoWYMgd5d67+BZUCJbEdGB6oCd77rH7bAus8jqcG9HG7Cy61h4IP2uMCHum75ST83/Mkv7volLSP/dNrfxK+PdlXfDdebVa9+A9u1AWD5OSQaHdg5Nrw6mwr340oY+lyHDkpY8nsw8curixVOPMMmnkjVZLn6Zh8vueXVN8cUPxP7fbTDmrKx5LSAjPbr5j4nxl+Z/p6APy/4rtdAl/JlpYO336KVLVmxVu84mhuEwxZ9ru0d7suL9Q2walUZsZtP6NSxiqXFgQTzqCcdTIsKpneZOfrXLeYr3xKt5/zrEI0T0WdOCVKT12rrHIpAamUcqlDyywAZ1tAuCaFGH0Xi9tw/jvt5bEXW8td7q613f23c99wlTwVsbIfSKG4pAsmGEJ3zDhM2NqEaoWke1gmAtWnN5uEPwHGtrVJ/xPRWVG121XL/Yb26d++8RUjoldzFWr009DyN/EsU3nnMRQ6d2wnXUYdXCT5m5MG0aXDC+u0F1mJr3dnn9/OyqNoV4BOWduugdn7gFDhGPJFaJBQSJSuI/R6G3IkESkZDg4mkHMVCOEQdU+MOGUh91L21oyDq8tCIbjHW0ODecz+ar6wfXlXzgl+tXbXJB/GCf9bFeokkfV7VxY9mamp3LX9mtmGpGxulXVAXNhJ3s/vuXjzHFSsbH3GalM2aUfKRDX9i09Fl59z7KjEdTVZIqyV6cXXwkSXVJpRvQbViJTcWr4usdCgthJSc8/dp1/rLgPf89NjibX3mF3WX936yig1W6Wyh/46M+GWWKt7opVMJ/VZmi6/AmWDVcNidmY6gGNhmpDaEaSaY0Htr2jc9KXBUZFSxJNDTHK1b4sfWw9MM5iQdYmeKqgKQXDvXpV9sEUWRD4MrQgsIGVbr8BnhViXBUMDpxyQ0umOJETnCRM3rnjcYqn16vzR8dH2tV54livjyLsu64bU9FR51NiU8K4SGOVqHjLTFX9kVFL4vF4ZYUHyrBGT3/TnoCOGIAK3x/XxUWweitGOabl5AEuMNr0HcmTxdPJaspTkZZZKkpryRp1ImKo1RJs6cuXafWpfunq2WDlDol8w01q25ESxML01arZHe4WQfkTlc9mZymTmreI3UZfesLL369tFx1brofO4Zk6uSp5PH8t0BAWjXwyddTH8yf/Oc/JzPxxtmKivhB445yfwgVjNzGe8zKoCPWNYaN9xVc5nz3BdoacaiHNVgZhOkPPxwcn+i+ifGPPM6ZK4QxQlY295A5OnDy//XGwl+loAXc187/65jYsFhPf84O2VhgoLi/PLbpRPymABQlMJB6dExTHmKjCFYE+MkMMF2KYCbQf0a8+cQWWv6PtRzPy0N9x5jja/1kf1AEBE5U9+SnbV4XgibBH+TXPsHAwGd+OfFl9w+zd7/x6gEA8Gn5xGKyfvlf6Qrw5z8DofxT94M0jk4hv2VKAUdMjf6c+SNhDtMsCRW6zVLb6/t2bTZWccFBORYX77waVmlgTsS7uljObY6dTpQi6uQlNJoh7iP4rTpEpQvKojPeLgAZjW1VeAh5qw2iNnIp2PmndGUYpcseWlZE0AYAJQhC3xkuZ/0lADWwmYZZvLy2j1NvYrjFDrBtki4v02Bc9nrVoTZlNGYhuQxg6SwaSsBNFn4IzXZBHirsNh8EwInDqFZHx1gaGOMlDnGU3Oai2iBKxBbbYUSDFbfjR4XxrN7SxseY2WJOUrpX/cmDXjsAwBuMofD0omQnovMisYoAb6t0BuYPDE0MYDAlBwvGq9qwVc8wEUWHxsZmybEIvCdo0WBRZTf6ffkz9GZYDw7QPH7qqKOipthSESXMEz55EZEdyaJsS3XiE4aLXA066vGObQN0mAzHOgHJsWi9RiEDW2mIYrXpsU07bE9grACLz5wmRfa+2B+0aI/cppgrtMyV9NthJsRnUFLFN7MhExtHjutkIV1VxPi1xA3sNNOTmR+MzhWmAA7FYChxHURXMDnT6FrPFyxsov6Ln57IKKIEhijr1p4bvOBnBiFkPVyZArblepyhMF6GHVmNX0kUaVqxxLiG9eAXjYbRUsfOnqkLwwY7tn7YO6hR1MMwaIV3wzrGAvgCR5+O+AG/dj6aDSKd+xTtZBYh7gSgl/hCB9TZkpC/PBBv0x7HfkJ+Y+MqNOssh+qH+hViZb0zkguAAcaT2AiJU33oJ7TsBpiFgbro4sh8FFRdrKprpE9QXNAaCPuVxb7QLEvnVANpAi1CJWYB/eBaFl5TKmjxoWoxDrIH/IAP8GfxCb9oRlGdcu4SkqkGfgbnG3X9UQxP+q35y+MVI340CDC0omTbMaxFyBsSZdJkBb9Juq4O5B77OLjtsqnwtwR+2+P/oMX4fTub83tDdAbwmyXTw58f85PU82P+oh38sQAtx+qB9z8WRPnFl0ow9LPNjp37dls2bxlS5+fmrVbX7NixeeuATl23vS9HXb51q7qlRvacooHw1sDuER31d500bBq65JZN4F9b17dju6btYPNwIdo0SPUiQ+9o90MGWEQ5pq0uHhuFb9/SZxeAuedreGC0xxAgUh8lp9i4Cmd0a8pmlAb4+Uv57vIB) format('woff2');
 }

 .material-icons {
 	font-family: 'Material Icons';
 	font-weight: normal;
 	font-style: normal;
 	font-size: 24px;
 	line-height: 1;
 	letter-spacing: normal;
 	text-transform: none;
 	display: inline-block;
 	white-space: nowrap;
 	word-wrap: normal;
 	direction: ltr;
 	-webkit-font-feature-settings: 'liga';
 	-webkit-font-smoothing: antialiased;
 }
</style>
<style data-savepage-href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">/* cyrillic-ext */
@font-face {
	font-family: 'Roboto';
	font-style: normal;
	font-weight: 300;
	src: local('Roboto Light'), local('Roboto-Light'), /*savepage-url=https://fonts.gstatic.com/s/roboto/v19/KFOlCnqEu92Fr1MmSU5fCRc4AMP6lbBP.woff2*/ url(data:font/woff2;base64,d09GMgABAAAAACmQAA4AAAAAURgAACk5AAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGoFOG5JCHDYGYACHDBEMCvRE3SMLg1oAATYCJAOHMAQgBYJ8ByAbpkIF7Ng0sHEA9uDsckZF2RRNBP9fEjgZQ6rxe01YhItQqVQqtbZKo6NSLXshCcvuamYvk1vBgLu/bcUlFh3Hac1oktcuX/oxUbCwXiIe/gKwvFKj7c732o+QZJYH+rX/nXt392MuEU+WxCwzRIYETbybJtFoounZHtDc7n8bFVKCRYuIgII1bJAoE5UQejBAova4gVSNaMkWR4pU2ggTbToNbJ7IvaFPp13pTM2wcekEtDrcSQAjAaTTrwA4mtvmfy82dfAWacEkNkImRIM5pgnT+vL//1oTNlsiC1wgrdO4YkG4LR55vP5798dW3L/TcgY+WmnmA8I5dyH3MX5AK92eUuBCVpgGtI7otuid61zaTQWLU3i9qdLbpkBPKDcF8H+lara3j8M8YXyCszrRMRTdu+g87ly7cukOWEAgDkeaBEhJEPiRdCAofaI+kvKb1KfcOeQ2lyGRcvqUQuUUO0//ReNx07puU2wjPPDX5DW/rfFQlka3qq+roArs8GiBCY2Ht5ve22orQP12taAtu6w0Ok5yLdAcAoNsv681oO5xkrI08fpvGUsbvvv6eJQiQqTxKUaJxW33l02A4gHgNNQuJxEhtOhBGDCAuOIahJEbcEyZQVizgbBjB+HICcKNJ4QPCsRdNIgAARDBwiFi0CESJEAkSYJIkwZxXyZEtmyIXLkQxUohKlRDMIxBTJqEmDELMW8JAoECtYGmUAgjt4AABKAmZWfgxzzWvw5ib309nEHso4edE4h9s/JyATF4APeNsfK9hwuwgQR4KECaI54e4crhgf5eDa6wcXSPxHF5dN8Cvl2bKsdm7/vcdghtspatqdeSfGiGBsznJ0AjWZHMe3I7lnduO49FBUAoU3OGDj0WcxzYInFov0c9eAoWKly0GHSlqjzEUKNWnXoNGj3SodOAJ154ZdBrQ4YxvfHRqDHTZsxhRQwyB13xCdllvrIlWpdsUxZ9CJoVLDDTfM2gmpZgSpJJmaZxIMuFucIXFdUzNF+E25TAW5JnMl2SJQYL4g1XbCHbkGhLsgVZmNiRveOLieotmj7h5iTELSkuWd7KtgmRaRwB8borNpAtSfRNlmUosmVFLXYGXrHwhX2KudoN9bWA5qNYbdKNyYxfYGDDDiPfEy/dFU7LQmDgh6wuD3kBDVXGL7bMWbhZsZwkSpTsgXRUmfHKkoAxMLCKFcErmDkdfkyCIUmi8pI8PuQV+OEUVIEQ8IAj6AAXOSvtD+mUtgaHwQI4gADKix7BgbiWgWFXPEHGjHPHppPwgr0APF+HNWiijwf/itsiGroyVI6pbJ6fTlKl8YyREF/KgpkjF+FPgFNEkG9JX0/hV+8tQLhba7j803F6NdUG9CNn3dXcVqSHN0vQjnDuRi6+2XgGm2RruT4EsL8hdt+dBmdPXDgjDr8HUhWmOk4xclcv+OjyHH6oky8EyWdayz49Sf/XjNuET33Ufd5fkaD5FHNBExNCOg3UPTY0EsXUEu3OsmT3Wh1ufj5s3txKD2+w536Djn/JQzYe8mFo/3twPX9K0eST2emhsa1K9/8MBR5YsQKOE/MqalxQZHphhmSLJ4TBV1c92yJ7g5N/TnLyebp1rKS+96Bqbv5Ncy211c7Ricm6Bj0oFWoGAWdA6YCjyw56sLOAlTXcbOBlCwcSHBzg4AE3T3iFg4iGXwzc6PAqYYdS2FXB4wF8quH0EHY1cKuFVz2oBlg0gnoEi2YIWmJs402z6QDcOuE1AOIJOC/AeQWrQYS9ht0QwoZhx0TYG9h9hMsoQsbgNwneNIJmEDIX4whZqxQKhPsMDEd4PFihKoHIC68sCSjWxKlRVRdJ5YXwmnn11MM3d1WsjdtqDlqTXAbIt4DvOZCQ8OGPIbW/LrJjRZnNsDQBVCGB5DPIDSi8V/azhmBt+kchJRBsIaXP09cRqg5SKmmVtE56dhZRj+naoYAAst1VAaGdvCd3kCa+gSvVRxn/5pIWnItlBRb8A1iyM24gDjkASEeWQZz4vE6tsYacOH4yzIMPY4COxOGh6YK4xjdhiUtuyvMwzXmVd5nItyJZ/QqcHKTzYhO33E1EEpOXitSmJYN5n8l8L5o1ODkovgfFYUDxCSh2guI0KE6AIr1AIAl0Bga7AMwAFgCbge3AQWAEomlFimmzSvBGXa6MMZPjdD3lNnMWR9xiyYpz7ipUsmWHxN6ceZ4pH33imvMkKppxE6zzzo+/ewIEChIsxGdfhAoTrk0rB9VdukWIFMc770Vx2N2CRbE+WJIjF1eA2t5Dn7naQDp/NCukC5qd932q2Sa7etS4cVfEgzdtPihKYDDlfPkqc1cYfRGiGYsRYwpdnNsSJZmRLI2ldPdVyJDNVo5cc/KUc1ChUlWI/AA+qvYw5FwLzTy0aDGpVRuqdh3GderG0OMZP8+98NlLr4Qa9E6d994L98EHbT76qNUnY9qNmxBh0jc9vvth1Laf3vnlryhIKvL0Zcnks60QBE9E9W3TQAPfNNRo3M8qX2WD5gcvwHdAeAw8BHVXg6YLIN0E4mt2Dvs7jCMNUyWgulSungZnMMHGJH2boBPKAIfCVE3Al8WmLC9NUJNYLkzDChWeTHWZqACoBIpoKzxNVKMT399a0SleVZztHLlth3rdvp1S5Tc50o07SS1JikIpaICxatobipTvMIXr1m4m8yJeYd0lhbu8eI5ZwGj9vF26PoRporZ2rrNDt3ZKZ3J6ZwVL0ABNzw90HGJFthK2AXSoR3keIu15rldV9PSc9bsOhHudTpIoEsLzbs5WfM5Cmaa6hgq9WzEGobIkV3Vo6Go5tUNXVl37+aC1w1S1wKhZ9QcnjtaKH/2VzBb15IS/wMTbj6oIGdK+1yLhNW38jRQPyvnXCDNT8e4Z0q/ZXFqQKt4hFk8FhhSbU+Ja7E5aOlCBS9zDlTXQCE53RZuDeKUklX9T/rMxM/8dPX8JzaEmh7A9gYwtek2i8dpjwrM5LWaULHgpvFLkbDh4RYWgnDF7M2PGlAuIMvCOQpUGMNghRioEpHR25HLRydOIpF4m6bijRnndzFwFgcNrigwTdqgZ3kX2OXE6pxss8wqgCQY5xGHRq57xsUYFMq3IqW0zYN7tRr522nPyh27vAGIm3GQ6pDMmIxiyRy6vFj1PLiuONyzZavcjpPP8p6GiUDz0pdl+BvkA+IHftDZtwDrnabgJxK6BLgkebixyOzxra67V3JSWMoThM3apwVJ5iRfdpdCvCt+Kqu4OsZOkpAYW6wJ3QOERtblFG5rweWclxVuTBBcrxA6r9GMsso9/UT0OleEeC6+C19vwOow7WXTM+IhJIF9tkOYYQkQ4iexvEVKrDB6nXBJsPz23mbbNVrG6QoocmtFywXN2WHHCs8skFaeJZ8+SMf846fLER9d7KMo4IuQKS89qlJUTrsuaxDvxsywM4dN//Nbp0iphissrxV+qGz0cDFVs58F4hEJ79B1xZ6xj+iAtrJTrwZ6aZAyiN4PVNF2NqXrz7msVJtsiXpF4NWr4QhbZ9Nzbjdlmi2kQGYJWJAL8R0NZAaSVPnXau1eDa1OyqAGiha5uLWx9MP3j7tkOzqa2ZANse+UIvs2ia83BICofGalSnJREaImYJeIyrZ4Kw1qOrCTW6B6z1HXRSMo2uF6alF3mFtUSF4Xa9HFpjuHuMY5oC4FLODXN7gzHfdzDP6v098H7SHERLWy90h6TcGU2csa4oB8istF5NGPG9Btx67FX3JMXbxdecNuRSdxbsfZ9v6OtNir/QFM3yfwGz+2K2a7AvLh2hgApWsQRXedg54ZHGdESTjxGsmUSjNpZQKiJLl4mSLjjSQAwmp+pIq3yJ+TEEyD4qKKRRnD9STByay57mzZTKjQbrYxCiSO3X2Kk0d09JzFDi9rIooksvEqI5iMU8Z6lztL8Fe/hP/Xotwqwblw36055r3DtlXSERbxRj2pnw1gqxGzyop9b0Yan1e7aeVXz1VeoQV/6OPnxbqDvOJtQfq0o0xEQFtJAqkJkqAx2M2PkKYCsXmQ1nq9irG4TEgDE5UVQRscCdDIwxGmDig3rmyEezNQBUl36WYTsyUdI/E/ES2B9Wz0jW7BsRGS4bchS4XHdLUuVZfnwUvHM6nkFN+iHGghXUWoTW9jno2TcfgXjA7gSvmw2iOx25wJlrxvyxb4ZtKgziPtNh1v+uBsgdJFhk1GAMvWmR04n3oQjhK9t3ogjKpkBkpz8A4yHGVF239fKtLM205yzrnQ1zejzhgf7snd9izqbGzrcOn9jHwar6wbYlxuHYW91Uy8annygHe+v7983bdGKTUAuQQZ3VZ8oErcUUj80UeEN72LHtiiKZSvcPFx3dsh4aSvKIgpTDduqj0W2eIqPvhaxeQQtEd0RiGftOGrNRxXi6j7Gejyi6LEDoblieymRtgkBYQRHpD5PxdDT6E/5JV+6TOzE9sZmfB8lcJTB14nTqIlv+6BrUX1Ggitorkmu/SByomjv7UB+sM+HG3ulatFwoeBII58JBchJU2auxG1hZdBeNIa9wEh7DlzM2wmRVvTEQfu6z4WUou4pDaKXU94FXQ+i2eAW2ddEOI6Mv8c1tds8vtB9m61dCHdafSVrF+5RhorjxTQdxsKqYs2OlPtqffVn4y7Pw8Hhrsuam5HsWAGQoTgkb4b5zAUHOExJ9WBM+8/0Pl4vSwqz+fK15Rhrr+NCuhX/sDrxaYXEmofrWnxqTJoNDIq4C681nz/K/eTKNfG5nXgY7Gb9dZ6ylJtUls5hBwUOr3X+Svu8uaNbZkuhf7vlHbuEHePOh1ySGRSa4epPdQ9TXKvucW4ZisPsz5KKA28qungk05e/00VyZDzManTLnaK/A6U1MBQuR1/8rNOIV+O+71EEgyuOR1+q8MD1jQ6Z8rQd3cwKKxJGfxYyqJov+c/OOjp2d1WzBS1DP4XTyx7a2gX1G8W7IXUrkTEHiG4ZDo67viqRMduFmAtlHoQy+3dqe1t2oO/Ikel2NezucbvTOf+K9pJrCky0LZ+ExOPsCDLu74G4UkwLEJ8WvV32PbzW4LXiOSD258DAoKrAF71rSuPT1Fit/GrQ8SXN36xnJtFJCsshSw/9hHt0Xhl9TDYPoViWzTYmXfSBW7VxpMTEt2LLnTmPyEwZ5V7ULOcgI+NZ7pL9GlI4CwgDQyy5qyBDR8VTHs1J6NebFqtX0TzhGQuNfwSxadAy4fHHB4gfelvO13t7fAKcveDx/JX/M8v/Zi4efGQf2LAtvPB4e21tB35BvP1ew6n89JSUtEBJb4a3PcEClK0R7R3qoqC30b9zRnhrvmA7P1wO2tu6ww/+KyhZ5PkjNEo75GHXv7ifUPtn+mBPV0+TcBOc7m4uv8rrEAUERiiZQ6Yc1mjN55qp4/bU2HOxNDC81RUINTAkUggSDYQi0fVd9kiJGbyFRO9/9tS/5YECw+7Ip1FKSGH5/5POx1IHYo4gRZchP3o/2n2Xl3XlwODzdXmudNya4quhBXlO0LmotQUELar9pbxJtoT9Ktr37ZMmXq5PJJdSy6mJGxOTL5PAOmUXUyxWfCpGmiotzoxJBK0deJszV7pv3cnqfBCaw/Jo6QPVKzYk0q/wngkFAtdaHza8rkfzIVvcgOuHEdbqKW+5+rYWeervahpGgYOX/cpiWFw98dbuBa2mziPb+39J57l+ex0aUzvLZK3yZPUvybxXULvAlBqsnQMn2T/j8l2fPKosKMACbvg4j8KxlmkFwFgGi1fTvph1/zEb/pRWugxnBKJGw6mbaQezsJ/vYv37ZrkW1PEiTcNf8ywTL6rp654IbucX+BPe1CdQEsmNzyztAX6VYKLlI/Kd6vIe6OmnDEA8Rg91rYiS856hAAtQwFghC/v5OSrmPafiL9Fs7NencPo7Lh2ewtb9ZLuwAMv/QoRm+rLYzSnv6a/nsBYk1RjrssNflq7ytHl0P1lvH9Nhs1KgJzZw9/Qm5VZv80FusB655XxDhqyaIBpyOBjYUrwk2/VfSUq81G9r2Jf/FQOli2edrJzAfR0O84VcnwlJPC7EsCQ1CosbkHREb7cbHU7gRlVOwyU+weh+ZzVLNaspp+jyx2Xx2Z9rX6A8t0L8k/yDigPJPlzx83WNfgzH/WQ1m1Y3r97xdeS947c9Tzsmqx5HeSveiA8+F29r2mw6qACRlb5sDjirINcTUukhdT07Sqn7MvJqQRwjYrAvxDDkffjdr8knUrHvQ9F+/TPbH6z+7mnq/Zrlc/+85pGroVF8Un8DmgZ25AbsSSxtAoW+Yc8sekhQTowX6Zr1TYa16SXYn/I/XljR96hi5KziHZAsmvY9xH4f2O/8ZKfDBe/F+DuQ4QJ/Vpt53i9DKxsVt7lT9jtPKC1smUdIAp3no7LRrLtqXL05umig6kWk7fy6qx1rF8XvognU6QndErkFjnJPxk9aSzr6GSZM5Pc//v4wnGJa7xMSU+Bk2a1SY9uZBjlWzlbVXklxzYE2WOqfur6BrSpQ/Eqc6+f5DGPoZgQ+WCvy+GKQ8M4jCGc5pWKLj3AAz/8P+4586f3fwT8LmokNRQ3UhvKV+iyxte8dKU8TGInUxKqnCcnf23etZ8JpVdXIvuxHOS9LA6lefo2TpLBlzqEuV/oDyoFdegJtuyc2EtdcfFtp1JgP9aVtIyX0lLbEPqTZsb4B2mJAy4ix2oNRlTejtMHlaF/A8CgDt+lBtJcwFrhwVTo9KAhRdXAj4e0pJ7h4d0oVQjaH6FRxgpRqj5QGufXVaQ4OG82zNF3/mDOBkl5KdNBJAfo/AC31xYm27aUR9EsuLR0YVXltHc9O1MAO5glyygdrjyb/LuTz9DgQGNQJe4fytpKEqEcFxQiw9u1s76tt65tS+j9aki2l/y4cGCAd8qAKTfD/nPutTFKp7o5Q2M4rWBT6JjwFKlNMKSZGzITT7l/jv2Jf4+K20jvk6TwJ1h8TPmGfEq0/JsXxdMrfh8M2u60TyAFeRT6mlPDZh+XNg7lJhhZ+ppKDJ54mTpS3NKw0JGHWVUH+kRkeNnfoFonFDo+3CXBMlYgRMV/pYF8KkXIXutZK1n2JvuNFMWNpLVzLCjlwupdIxJgtO5I5x88vB7o6KzqeJyHGPdy/hThhM19sKj2ZiGFiOMxILDgmhkLMhk/YutY6RsRmtGYwIF57E/UGe0Ml+UEONpB0NPxdVytuQJqyF8kIiciNx0Uq8WCwj3ECq79hdp+U/bb1USWRsjZ7LDAx0N05NoSKgPi1eVpxrLuXtXNhm4XTwG9h9gnQz/LcHgyLrJ1+W+kVWJTpn9o4xtK+YLfA1lj/CUjfGMLwlDnV7Flrf4isRK6z96l/PYr276WJQGp89d3g1BpIFr4nDMmJNQH+yQxIEQZzVWIP0tQzXO6QbyZvoWBRaOZY1DmINPdS2iFN7/LZCzbeUQUxLVgLPTTfwzU0C4nsAdnrxIuU41jdzds55MJ3HQ1VxD67PmfM+aLG+e2jQQlh3m70iLsoCDIV8CrKdr/n+H8KTVAPeZD6F/ezCdgJ6G5PKfXtV29MFZ4S+rb4U56seAE/8GJ63j23fnLMfed6Gsc/eVrtIHv7e9IH/JP691lxv/D1NxGW1e0tfjmUJ/M/rn30/ThZ8SFAwo5BkZBhBLzU+Pe3B36bVJpiprUJJrTdGkTy7utE2qlO/zR72cD9gWCuUSUo4NSpqfyDm+yDj6GfMPmSwU6n1nOdV9kwB/FEl+PHGe83wYjYIFZUy47GdbzvGLepLnK5b2fzqbfdaGc4LQhS6eGVUBqbRE03LqlKCMQl0iXVOfkt1IYgZxjXOdj+5eJc1ivHHG5He47TP4ikRoVkg27uZ8okWbc4KhRJe9VwRXA7gAUEe7isEOVebiMMSaaHFCNVJbRQpMvB/FPJMNLn0WKPD3pV8mpPm2CSiQsMwsW+9X2JdFiblyxjRGx+8+Jc1rs5z98fal6HXaMq7h2U0Bd4JPxpSSMP9TthZqO/++BNO+moDCPHOO/EU4zh/MicF3HdIfb+90Tv1SUVI7oktXF4jr2u7HLiWsLfXCRwOsEh5p7rmZZ0kLPf92QqM9TQyd9WalatNeZHJfNIDo523MTm0h6Fmw6yYelGjvHe+1JULpmddWCYVYbYB2rzBwu6JARgaTS8kJaUlKCFdAXoLfxIlZaVwVnlw74Si/T62ZKOxwf6iuBWFV+9XjrACbd24eu841L6tTPL9/f6x3zgUPqp+vrACFtj79Sprjmr6oz+abaaEY66/kkQ/7fX2Rw8AanHB2PCaY4gDk5HHl/arRi9M6TIe28KIwxqeGQi5CJ63aUcHS0NTgvxmVMlr8m5CUh5Wor0Eml4908LVXa6PIXbD/ObFRAhyaa0+u2fr0jjwjDfWRFeO2lfttUnsnUi3phPndALCXShYvOVXL3AmHifdE/2f4VbCl/mDrQJuVPcu4Rm5WbExhYOtAt4UrzahOZkZ3a3RgsXbdaLlRd9ixb2pfhGCBV/qxFjlKyFC3lTwNCoun5oBYbOjQyv1PvUkZQigkfybnAX/k6NObTW6HA3vhFyzxfEN1F5YVDSR9rHt+um1oTQYpvzTud9o2iuOtf3hv/JUk8pNpMA+0AvR+Pfua7IDwX106imE5mM6tmLgmavZknHZ+gV7O8YLXUqMlOwPGhZZOao68ruTmAZJAinBFa48MgCtii/kYjobE+ngCI0ds/x9fmdFLLR3gh/Q82DVyT2pxaZ7nYO8nZUMpOIveUl++LEhVOoprMj6agheTu6fZmfHl+/uoVc0Xd3dee5XzLEwzLI5S30skGwWzCsOVNNJvl53SUzOyKV/pbmL/D+Zh2jcTK6uxo7l4/9yf49fbpLH8LuUMza+CZlF6QyPX4MR0TUTr2v8KTl5wVmPV5/JdFdu3Efdw3OPRSvPGouWf7YMHycLl6helu64lOkRMUxC8mKZ/Yh68XSFSdv7ysHxRE1qtuzf84uLSDqI+QY5O2826woZf8l8cjr/rIkI3kyupWmTQC5vs+hUTGnMTpIEhTppzGIjg4FJdWt6x727Z72190UZZUVpZW/CF73tL9t4UUycj+srqjwUK/13ciLhIYqAjNZRVWW1VQnFA/JPIxdQKp4B4+KkRWupy+RY272RQ7ND96oPfC9annREldU0OZ7ZfCGFrtXn+tTrY01tRlqb/M52VW972y6urr0gDLTg6+eHg0NmQH5KSTp4jmNPvljuD1IksZ5zR55kDXSvRj+whkeCLp9pL664HEXBa9du/hKl68kO++J9SnWuedDwa7+sbrqUZmuKnkiPTeuJ6Q111YitFgvL0NJnzA9dTgnuYPbL+VOmP4FpxCz0z6j3plyFCB8dCWke1WNdn+WIVBwg+Vr6S7Z5AO2rPJmuVZ3LDUP3wk3uEC6d8VnONVcba5Xu+UP8Ge6bL8MjwGrnLq1Z1MrK+ODQ8fPQ6NSyJS2jvzNu/mBZNNV6u24Y59p5N6EKjQCKRtu6oxmxYhZoOPNYHSsrGU1PjTbQ9oTz6AlWKsaL0YSs6KlwLk3RGP2aKUc/TW9VdJqRIpyICM2SBv64mNwvLvySSUVCLrzJJus7iOZmUbPA6fuw8Ra6TrGjLdnGy+OHktv5Vw5lAMCApLnYg1JzK+y1OZzzb7uCULXtWZk7B8w05YnBYMt9aJRScUfGrVOHKp/TyrCaWv3HBD4+s7eqr3Y547APtOofYkPQ5Hy94+681kxpjYTI54A43jRobKL9PMP6oUqM0+iPaOlXjtepwWhTkktfF8Gyqh8Y8kJtjdEkywzZBh/M2y+14d5Msbkf0BNkJiPCSYZW3W1MpMtRpuiHcxSwt6ZsnjvbspiH2gDV170MZez9KgvSRgKOVcH2znTyLcDXhfrDPbjCnVmnfUpc/GO3cWbEaEV/4ctA66/09cz3uXK/TrYtZ+klhaAmKeSBq4YWdVnwC4hnVhSLEJDKkgVEyeAoE0FgmYc2xN3Cw0QTHpziNRLEnfVHsFGXNiKXc4rwo5QRh5aXpZdhxiRrsay5SrUlAc1tehQJOs26dsaMv8W/ZgLA9iAnbQdV7l98XNCU9dLQUwU0u7GxONk7MVduM3EeTAoAQKDmvbiRPuPFPq6OdQ0eFBR2NF5dBlAXBcSPH9hb0Iqb3ZNqoO9TYADj0RVjNh2Vsq/PUaKdi2pCz9YJ6cKfWRMM4IUey6dOzg/xtHVPL1rHlp/sn5FXYQ97tPsSoNkxZ79/1X905UrDecQxj2UiE2WjONFDsjxZbn9fBlOB9uMxsWX4wur48dAyWhYYHK629a6pcjzjoC4WdjulAo0CCkcYjxrpqYXVjGGQMCIUXl8XxkWlsq8uviTQdx2D0/cMr/ns3k8i0AZ4LclHo2rJ1Xk1cSdtOa2vnT6lgVl65DwuwLk+3qlL5HP0vrST/M8LBQ5f+UQ5YA6B5v8+lI1Rda0MlRgxrX/ryLHdtXR+d8ccKxB6yPZAbFoZptEF6bfdP4fGwxi2vu0B9HobXVJOQ0l9JrKyhrws4NjKsxhezvErJHvE+vK1As/j1ambXgAk0TpCPSNbm1MzWssib1uxUazh5PX9G2nZKWnR3HTPV3/J0bBcYrmUNJZJtlcXYiA4v2CbmT8Mzi3/7tUlwIYhGXi4nBlZxk+olflHiI3cE2vPtau2NPIxZ1lCfFN1XkouAiwjyl3jLhV+mAdAQ4R5olI2SzuHVp+Yc4LhG8pNqJBOFiWHpjCnX/p8nSz4TOefXpK37jnbUrp9/UXhWNpn0y78OrwN9+RUQsVSck7MCuy+wLys+vxvYhHQQfeX+C9CtZ2FKMIpGILN4BWaszeFb6p1Izew/3YPNSejMsoIhTqxSanh8HAG90Lp3UCkYh6NAeXo5NLFbt8IAcxxD1jrlYH4qjuBH+D5n8vEZtP3mio7S3zZO8ghIdgLdCGMAvjy9q0GMpIAzxJINUz0McpYozsFjHSvhn48azfO7Qek/ff518if0DFK+amVpn6WS0VYwou7KS6eo1v5IOMTOgfRuy+shF6XrATtBE7ld7XOalNnefblZ+72MeY6ZSdV9NUueWL99NCwc/2FQ4uXtQiipsInLkskRIWiCM6WNri7d1PcvDyyDTI3wx0CDWFxJJ53Buk8sIMBqJW8k8REg5dlezLzq9HHoJqEvO+YDufRORqKnrM8bwtYvKYe0t4T/BEsG3YDQhAkntwTbhijbdeIHxNqQal4CbeIo/SMpvqQdeOcdvPMcgkESmewzHRYvUlDMT6Kek/zMrjfWEjc9WMVbVRBUclljRIh4h1I1OWgEYKvN7LzdEc9S/zT8BlPCI8hgbB/P+5MWIPWR6u6mB++YMBevstQ9ULujb2Lv6bwrunqxB5VIPLR++rvrq7L1kgFjRwr1hWweDSNor/cFfE3WtXChjh0nvKHgyzKP3cm+b282VU6GOjMxLm5sdj0+u+P997U7mEy5IGkjJaCDmAjFuFAMcQsySkaAk3iDxUn8BAzF6+HA2g3e7tT8UV1LO2wm6bkWC7aLs4GgQ6hZndQ1IacFVI2oWnXnDovnA0F27k/VZDcpdThTMsdrF48NFSHZ3qH9T6zzkLf3iiLTy9Jk0m8EB6tUvH3jdI/IZd+CNvavB0SUXfaAY9cjB2ADod25nwKQbGH9iSbOF/xc41N4NanR6CNTaKgXzYPFKUU00/ac1F8ld79GcEZAXtVZX+n/Ax7RtIDVPUKVWVv38qTJb2Gx/bbdpD7YklxYa7kLf2moLNN53v2PdCUuFopr0URQcEEH4c9VHSKCuVlZPEqYRzs+t5onix9+2k9m6sW5GkaO2f1JPQsDKhFw/EC03P3jz6c1Ptx3pB6nyckHTf3lhJ6NAORwK3R8x//P+uitmPp6Re7U90qUaxYmWcQQBZ2A9w1N6DqCDqwAp4vNF/ZkiVw5DwfwVk3DmQUd1wAcsuAKooE4Rofh2e8H+VHq7DKQJQNhl/narKuHMit9BJhWR0hMrLuD/iMQEHcohb0I4eETLuPGWT8depqow7SzlkQgJVkfGZrrjkt5KXpXFWIcQnD8np5PzQqUOwn1eIK9FURluonIxXpEcYTrMoC0hIIK0Sgm6wD38afivVq8d1Q3w8b0oQDAuHCihnso72gmy4YOSehpiSUI6esyIL/2Ii8rGrSLPgwcAlJWA3gxrsGe+HhoqKA83PqiEl1Hk3zlDeu6XDmVws4tVQw4zH+eGiEheDEll5p7fYDqFEQ8XwlqkhkBN2w+7HVBH5A+nFImNwnEelYnSWqGyd+nEjqmQtPf8v7A/vHFJDN6ossgLPVxMDissDU2VZhMip/D9aMcGMh4+Ja0tcB2lMG0oRHqdAh1Q5v3ggKiiATI2eMJB+QuwAGv4cGwiTqtRj9gy/JcOmOE/kFZy3dxzT3Do1ICZvLfyhx/o8X4LQwXtbN+zpPsPQ6CDc47Tn7Mrh9g5nqVnwQ8a4th7HK+lReQSZG5FI0YgWgjwVl92nh2QBIlV5e0oorWRsnGWzEJjTeaRcvVHzR8oqSM4jXL38bT6vPzh39dR2Cynq55NL7SByZa9EGXb2NWZXiNqu6HD6pFBS1WqCExNAEWiS45L9RFniE6mSghIXSfaw3NXTm5YCnFwGNIENFFFDaeWRcqkVYdFLTx2LHw4kwHEYAmWYJXJSjImC5LUwFWR1wr7XxXCQ3fNJb68F7CAN2RUm/HD3YAz5yeAn1l5puaotxrW+rlEQA8GADsscDi/41p0gPepHsaKHMKUwsVAEVP3fwmZ3VyFc0i9pL0jfbD62Z5mBjqhFWFAfflCtXRmMzvLcVWgKRm1hJpob6Rs0HS7i6z0zSLzWZS1v+BVZ7ON2m6Xt3sX6rFDQXq0g/b0WFYHkWZPVGnShAfzSy5vLPhBvHx9h02DGOHtfDHtspK5M9PonezdXB2ZuccfPN1+uReKXHoyWi56bRewTzo3b3P+qQLqLq+0Ckv1laAbZN5ByKZtiO8OPswAGX9kmw5LrUxTTpof3JXLYY4H9UAhnjn/pSz3x7Tnfr47XYigLJNcXwMqzzHwMIEDs9LBDVeVdcep/NtE7AK79pfoC8GBc/fNXWx55IHb3BGpAAQKoturiAqgxjApVLuKp8upq32cMW78Jwl8QtjiV//b2iODQNbVc000Ccy1rqiO/DFDNjON5Zmd02GYf83hwjiST2MoKlcmcpjxfEp/uIqejyK2Df/UyGk+UZR+zaSVqEmCgpJQ6etxY1LlIIcacivHdHETSzaTVWptDrgTHe01VZtVWG31aKFxSdoabde4piozCBFLoB+V5jfmSXC5wifbrVNPiusVUrFTZ0TpTK2u4W0rykkxOEN0spheLpytUdkvl3IsNbUdl2ctrjjGvNZ+EMc6K3XODwhvxGm/zqVg26D6sonZQlLhmQgQoFVpyTyp7kVKpIF4sl9VqTZIiPO+g8KhPiMzkeDx/y9lXiayjdtZYW+4qzLy6XFWwfFypFpWWaUqkojuvhj6Tc5dTs17L6SZqOvnkh+Nzi/LaJFWi3b2iKvpcatgrJGmlJYuDdJrOdx/kxC5rMbkkU2ih5njJaY2mJCpaa5q6qrz30ZLJPX+30InOcPzFOT7/UU4KvXWW/hrjuq5QqSzFaLAOym23urDcqOzn2laW1GyIyE7eLSPRXdSrLb6l5JJKYVry625tI1nsGDp0SiAVGua9mL36BCYPjvOjBYYd6YnUQxoidZCiSBukBaqQwkhPUzYpiJRGquLYkIrYNO1NH0g7pJEpI+2R1iYVzBypiPRCeiLdO5WRskjDolWteaHKtwluH6SnaYL0RzpHQoNbyJ89f5saAweLIet//gXN/992uLVwm1SDAN/FEeYKBO5g1grdgOW0pOpIcJC0BjAHqC5G7GR+MYqHz9Q4/dR4cTGBrBDGArFiQ3lc103dA13dfD3I9g5e4keUD5uwuJarCsXZ7vI6LjaK7HwAzvGr1txTyez6wM7DpzHbRHDV1XoHr4Hi+laYjWjv7WwFuBH28CRzFxygOEzluBqqaY9D9nUIm5oGbkSlXaAEilZuwmyo69spunrYB85km4C74GWeqKWvo37R8FrHD2HIKuOubsnCFgA=) format('woff2');
	unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}
/* cyrillic */
@font-face {
	font-family: 'Roboto';
	font-style: normal;
	font-weight: 300;
	src: local('Roboto Light'), local('Roboto-Light'), /*savepage-url=https://fonts.gstatic.com/s/roboto/v19/KFOlCnqEu92Fr1MmSU5fABc4AMP6lbBP.woff2*/ url(data:font/woff2;base64,d09GMgABAAAAABpAAA4AAAAANNgAABnrAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGmobllYcNgZgAIcMEQwKvTSvVguCEAABNgIkA4QcBCAFgnwHIBv1K7MDMWwcABqjb0LyXyVwc+DqoA4IY0jVJsIYsAwEJSLAQpQyRBer3X/3dp0Zh6f780KjEZLMQpC2spZGC3RAAGaI/ARwdtnZRx8hR8SZQcPzc+uNMWgRMBGLLrmZ5CiRMnIbyMao0fuwkdKOKCVr5OgSZoHMiU2NLMU6AoB/6idfO8nrTr5eMX6dWbsOYSgKg7AOoZHMvZyWki0X7a4k6+5TWmEB6AErpTit2F5i55kdQgNIYwWwBF2bsEwul2333m3AWYRsfQtFyB+EIcLyUmNM0mm5++J/P+2V6sz7O9basr9lpzWldkIMSApNCJGeduW9elK0bbzSylWuz10ubZ1fZXeUUjogMYo2rRecCWFhMQyA4bAVlOj+U63AMR1bIZj3rXDsaezvV/bw7Lnbi1KGyTARMMagyPb5uX9ICCRuAMAQDpxEiSJIlEqQIYMgRw5BngKCIk1gtLAAo40VGF1swBiGAWOKCxirIggEEnACTgQgADsZyqiaXy09iP15cGsFsb+3ZpcR+2dyZw0xCqAmrbf819YaPGBpoDANid00I87B55VdXPKJr/u5omfsKr2+7rlbYsYehLN2wNhtdRda0nOpumfOiaNwQtbkxJ/8071Xzu4cUkNzr9Tn0lCd3LVJ25cwr1OcJ9Fyx5+HPet7blXWbrWstpbZNRl+FsEoEylaDFxaRlZOXkFRSVlVTV1DU0tb54Luf8iLl64ZGaPMzK9b3rC1u33nwSM01t7hMY7g7OJG9PD08vElNUD8BQhxWoTIxkk3Tr5dyq1Ra5p2713sncttuAq6RujDAIbSKAYUyBQww3XcgF2+Q2uIjfNol3dHvlib1DyoDQFAMEIoPOdFxMRNZN1iuXZuDEZNIhrtH1AISVDAfP48+bYpt06tRVnOu9yWR2MceiJ25KEQZDKiBcLDaIqELCV5iK2GHB8pNkGrMy7xVkkNZtg6bIaWJ+ROIuL+IprET2RbJNsjnTlHpFQj5XYQbXFwEK2fag30TUeUeZWbZaEc75HKTeW2HAcZzSVOOv5SLvFSSYjaFpi0yJYQ0/iZIURmhkOczcQxogFRjDKRniXZHGmXcs2Qr6TEEyq3RK9ltuQAIirksYojYntiBmzMYBpsxqZLMMdYKaxgxjRwRiukCeLaEe04qriNSXrd5TZdrXSN0IcBDMloTSiDKZkB13EDduQN+BI0RwCCEU7wYXoXqT0hlJyU8GmjNSZImzeDm7oR2QBe0qcVfpn8gSdrC1JShcpqRdBNUHcVmx6g44OZjf34OYgTOC3Mgbj0VCuTCWMrAluAHEsU4YScZUxAGP+TTXTCVjb2EeAgTi+wUNIGI1KvHbaMSe2SpBDPg1FbO0lhE9G9jY2EauTyUrT5nnfVyvrAoklWjbvVlDvNgJPAW25DM/0lAdhIE4V1TRXT4vf/JCqQ0zw61FaNjrj78ga6RJLAVwlJmpjc6NyYUq4EEyVwB1vJL+aDENQjECHwR6CeziCvAS1JYoIWNvWSWsWygXWdTgiGJkaAyl6u8RgFymemnPABb0UvHXo2NoDnZ8WztpqDMMSKps/FuqVlQE8rZa+hd41Fbwuu8GWHPXnFKMHbRfQW9IsCvencfE27pnlHtJnxsgO32PM5L3pbYSebk/sPE/Lc9MSw/cvpLJhvuiWeLxn9EeAXNWFPs7DSgsJaWuXzsBcE9S956HzonNil4MXUlVArDx5RoRt6VRCgKYA+HDRYEDKYVrbg1w79LtHtmSWw2ap2foCzo+/k1x6hk+lpQDP6pGndzCp70OEZHA2Xay6GvOeov20QJ70JoMutvaqhN2NAB7IpDetKM+VdSSkv3Fh0vBnOJSfkCTSWFi2V6U+vpCTgxatxAPTLOiBpLCg4dZj7thNPG4uKlao7WziDf18FmJEcI8CPW7qP5Bc+NNLJ4Oc5GW3a0a8C412wuz9837EAvOA+AcDMYD4DbZYB4ed9CCj8Iht2SH8S756pFwsC8OCGExcmLICBGU9W3LHgOI2w+sFCjjhipFk8SO4EfCgRg+ewiQMYwqnHBm6DYQDC9zABAELAMrz5XkW1pcnhS22BFQdmbKgFAgtOS2BPZYoFgvu2k/XpsuRgUkxqhNRAOg+q1i8lJjA9sJoQ4fp3iPEh3A1AxuZw9fyp/91ez7UBkV9mcAuAuTfzBhHEowBVtPQDEfHKDB1CfKF/NHYQThR/A4AXDwZSxY7qGM2NQU/S1P380gdQqLhRG+OxmsSh22s3eKnAOq4P/xlOVgPj8bAhgFN3etKw80ZIGLXhy6dzmtKWnyZjhuzWJ7kondUXLcBSKgTfdYyVpZWDc15/clsuK1Nu2zV69BnwYVgoZBRkHPQkyDo6bZBRBAoqvZd2U7BwJ/QRAJAnAEAOFJ8qJzUlVIY1JVUiNWWojGhKHYEoKNMJrcAooGwndwDRUE6VKU25jsYpKDeVaU25q8RpykNlRlOeRyAeyuvk3mAWUD4qCZryPQLzgPKjgIpEBCIQCwhCECYRjGCkIARRWEQ0YpCKWMRiA3FIRzoykIFNZCIHhchFLraQhwKUikJlAtsoQikqUIYy7KAc5ahBBSqxiypUoRbVqMZ+1KAGdahFLQ5wG7UD9ehAB057na3KQU11udi9bqVySFM9LhDqBQ1UHMYghtGNEYzgOEYxijMYwxhOYBzj6MNtuB39eA2vK2ebkV/qR88gGBIrUmvuAJwHCDfgIizPwPEmxBogegDABABIDMQA6RiOmmjNAt9SOjc3IMJQJZwtjCGzz/DYMxRCf56ShD4fBGAGQhl4D6+kNHRaXyBSGbnMok5JXcLTqdIGMpeZm+QBlDJxev7KWT2lT5LVLjl5F3q9o3maepRdvifSivTlU9bqC8bpNHbxyVtgvKE0PY8Iw8kP7Obl07qvzxOkyhgwty867y27gg+O0nzFHcncsl0AxhIqVyGbLkIdeKiFdKS8I2OTaMuM9deHePBPbWoqm55dNwT+HrVlEnV1XTa2youMOU+5PkQeAmMbRWjcjCiuw/0JYh3h9XwGcEDM+LZ0yxhS0XoB1eriCXFd/qndr2TTl82oMnPcaCJ8jPoql9qmGzF6W92LVGIqgUn2OceDQzg6gPwIGZbQf9HBxEmzOKTwmNoAc012XRRVi9jqskmQjgd1fG9TQsrragXzRKTh7OBjFLKT/kDHoHGp+ep+1j/aX5zOJ7yc/763US/bK3AxT4j1eyQ7EPXE8R5n70T4Yfch1+XaVeLubKDGNJdSm9UQWPFbZXaB6dUBnDyxynjzgTkbWF/vUxy5tyO3GYvHHp7OTT/Ot/kKOUZX9MG2kTAuQPwB829ueyDb0vB94Czps1Wxi9F5c2set6w4Hf5pMQsnrOhtZYZpPq1/IrYrkxIY5JNx7m/rpVqhGewjrDawyZQZA65oWCXIVQIIR1Nbp8iTaEEkmbozzST/9mY+u1rYe3VxwpfqC6/twqR4UBDBAXw4wkOMUGGOB++Al3QSNFQT/jfVs3Mykt81BIlneC7umC6AMeUSZrXUB8ZiZvAWnthnq9ujRd8Dh6PmlX8GARLauf1qebY9HQxi7A7YNJ1rt1BHmU7mZSeecAnRmiQaYrC2y1vEpWP8bFVs2jdcteLKLUIvvRtwYQwqKv9sw19090O9mo7SSiebRp+eYby4bjeJugRqZ913gsp226b4lvcRHUA+Kqh5pqzqmGREbfD/GifvD5DH3caER8Tf5m3rWeyEq38GpytM717g/e+el41Bu/n58duyizGAMAtgGV1CCxMPRwKyP9zFcC2wGAaI67u7O3vGkRZkbWNTSFIa430Irh0EaxHSwuBTkMxTbP3F1bUqfprpIbR9zYXVTeKb4Zni+7pJv37cXSchBru5IbO7uyOAcEBt07TC6/i3uXmZZHeX/KGYjrFLyNe1kXmwkwPKeAZybE5YnJJW2izfYHVNE4iqhH0T2g2C/MJuGoNrfwcaL3rMVDnmzcPL7RtJM0prM65fdaX5B4fy2TVsxRRC2oH9o6qys26LCXMgpmBcxn58MPLbWPLArECDRInCvOiy4eDvrIayeRGg1mAjaBi96WwRXRLcvC6L0nKKcuhCrN4QtnJkFWh85w4ojmtSEI0md5l7kR4niWYQx1/sQtxstr6edO+bvImQPQPrtrUnBVRpxTK9kUNpLIMf8O07qNdQLEP2yv9Z8/JmpE1wT6ledZFYYclYYJnlpZ/qs/Pj6+a9kBsECPog3PEBQHALhxn5bfCTZgvgsIfRsIdwcKkgPS3teaiEL92Xvx4havYwExGDk6AVIOg1y1u9nZ4Bpc6SQacy+4L79j9+d+mw88xtzU+IomdnB+CsMjjKYyOoNeF1msB868WJuWM7i4X7BVFyoLurP0rx/8LS5UN/j36nKHs5vliWRdT/ZSsO9A20HmsFl/vbaTZCTrEAQY8g8EnTwAal/Vo7GSDo5Phr8RRg+f1FAs0mOuVgdH2P8X722zA5tyT3+dOaFlgOOBh/zeC97gkKqxQJZ2Gj5EXlnoGjTfTzaYjzTYhiZ/cA3pjzc5zo84MHOiQI/gEAy/4YRqwqrIh2UOiq82yYqgErtgIFT2U5+gOEuNfkR19tKgikwzdU3r5bUuAHGlbWns93c5Zr4dWbv983PfG+DR209Oft9ZWwQGBqhNoBCBSZ/xttEzBAVtMkA58y/WZzOrWMTCMnb03PvEkBpx4vuk0fTwQoEU6HK9b9d9yye6sjcrlaVj6RfeLDYwKLgu+RQOhGZ23TWCNHAcg5d13g502o01sBs/6hHsYIIt6PJIErorHfo8jbzxWzoV8T8UFD8wJLBpzHW8d38zHJRhctzHSfdAuL/o1qHRItjRHkzCobABIQEqLqSnRbvDV8Y9EFhN8jO54l5FB1JGD2nSr1b9ouxsh2G3bJylQb1oC7StnQr6+x1I/8Kr9P5kC/v0TFTQio/Cbx9I/sFxWO7L8+Trn/psTDJZ/x+xW4JiHyb1Ku87tvlhwJID4TEek+Vd/7v0ojSPBR2ka6ew5B/jG3woM8rNvz8k2goSd6N97l7KVcSwtP6xBWaVnT5/pECJ0b6O8X72Rn7XcHaZpta+5rF/O1qL5ruZkKcTkV+kQGZriBjTDJ5nh3kSCZOE2GJiMuUMZdJA5sV4oOxIeKs7dJdwbbFQWBPeuO6y1pglbSyXD1J4AnzWfWNYnzl1bT2e/vCBBtBknzxwngHISEgNLQuHd2XHhYLtUHZ2t/m25//wbQ911NSXcHK2T4uj/t5CrANAKlc8zqTL2U8RpiZSbI68WEayqwL+9syqjsa24pLtZ/QQEIFJ1gDyyECwCd4/8VlSnkb4MbXDwP31NBO6HV98GX0NyXjvFMABNOnj1BA3D2zBRAoNTO3xXVt5FKDwuDaTl54DjxJF0BoROSRRocIyFO5T21ydS2mnwOIGorrJtH9nVO+eTg3RbYDZv6BuZpurRd8YZd1PiGd6J1l8VHt5cWwYCpL0r1dIdG6++ljxxfyimFoLqiqKelU7OXbG4LAQv40+gUgDDRJcd4ou+xFOwk0NtDbQsR0j7Ze7UG9cG+sqcZ+hXamsI70bpKk2JbCktgwHLMKQuMkLFmWDID6+4d3SnIc1JQ+qKjycVJLdF8AXhQEzBObipuIjfR1hqzxTZ+9KQxkujJ5OQqRlLqj+5Tm1ngspZWzFBOS+6bslCyT2DzDC5ylf9dHzGumiR/yly0S3x6K3nD3b+TQqZ+aizrYpXGpXUlD8HanRubQBcVoG7SZy2/a77/Trk5AuUbsNSmc3Lbg1/MnwCQw3dytiRJUmtA0pDQ+fYyH5+D8VWKWRD1SqiEj2oc0HQUlygHQinPeQqL0rgPpZXDpFkQS7zqcHI6b3FROp9wSqX4GKTTevozHPHxzRbsJe8Y+AKHf3y3dfCKl94K+LUZ7WacwDPihTYApmn0KJ/fs9e6XfsrLI5veZR0QK/K7+p5qVsHRKXpAGFiTJB9QykidC9IGa7oMI/lGXBDYwHopqCE7gCtlzJ1OrNF+4BLELXVfboonTzdxVo2o4t52YEIE/rDB9xDJ7qH6ruGZlUPYjAEjNS/JflhnLIX+ei08K+FP2o4zZr+aKX9/MLlo3vHZoGrV8WNzwrUhLntOQKfqepH738s/dZFl60ibnlR1VKGEt5yLq61AwRdU/R3YmNtHjZOv763CkjDt98e0HexFJ/lz2HxH3jE1zFWB0krMqnHIWsU3s7I7al+wrHikyPs0z1XbXvxf1ZOtgyxCwoH5jvFaC9Y+gMfAQ0Z9/ZLcrw1NuihOEE0JO5Rwnh2362t+PtRSXj7e0mEqHsOqifhnQ6Z99KPAc1ZpiQTQmaBsMdahoEVIiYiWcEsjyLSFeUr3qDw4y3HNIETwmHgsudu4i60m5Cwk96jEHcoyf5z0hfoS7L955SEQ70KGUDdQdw+iRDiU+x3nxQ1X0trH81LsUQH3pcY1WUkT9M6mtaaUiD7qrCgmEwvB7c4dHKJU9s+AuhoISEk5C/1xJ+EJAWAvo3STX+k/1QxdfJ5h8CqUi64PIhEQswOkVT+Kb3VUKKrirMeDnZ3QPDPUX6wXSA2m56KhCAxOHRT7AmVSkLmgC/QJmoTQkJzqDnoYCwTxfysXQBI2/ex76H3ZFwgcBfleaveQjBRmSbNmWq4V48e5fkJrv0cFW9yva7STmqzUMZXvuHgZaMwep+zf8XhyEbzWGrW1NXNt6/TZuIeLRqnx7z0fcBn6O8rC8WCWO48Je56KiAXGk7Rjpro64QPS5HOwDLDo/MS4TGqhyAgPIhkTbMg1jcWhLTBACQQfoxkzbwpX+usMISdxvYwQAIOLeRYjC7UeOtBBi7nQ2dL5fu7o9HzOqHJoZ6u8eFkGDhLB1wUSdro6ltmvwKZQV9YcM52kVIS7+lj71rUhXYZ/nOMdxpYZHvvj0bG1LM/VPqEFmcFPWue5OpeclziaW78As5aaHOmBK5OBEdOCKj+PpHt/+dLFHWU9wTvtPYCCUmqUO4f2S8u+Ec6v5fxu7Bg+IO8eASJhBqtuBvChfrdxQNKPFPGaxsPGaIhfxfY3XaNwHgSkApnalshMS0EtxraABg4W5FXl3ilX9C+Vv8OJthPLA1KiCRWxMr5zpGYzbjmYdKwNtC1IpfH8nh6c9p7Fnbed/3wU+b/8/nEvbEIav08k7vKmzuoNCs4u+4jV/cabo2rt24CXCqM0e0Azg/tb1OTmfUNoJ5Zd6T7sFTsSirHBWfrLHrNso716oQmU/AOSSQKAKeeomqgOqMq+NvKlFRmbS2sSRYlCyGh7qPi4YNTAgi4VN+bSBtO6j0gcLYXpGGQCMVFJN0xWYWr6iB16K27grpn5AMshHVAAv2jphwcv8Vfun17CGgymbJMCCl7FwL+5+tuYm/63M2gt0vVteXhn1e3p6bVtmW7wL08Q/D23sF+BA/PsNce4d4ig/hQRa3v3yGAoIcq2k1O+oGDipHK6vNNy+7XyMq6+Sp0NgNcheH8adSBILxZCqGqTGTsQxKbGDhMpqS9aqwqG0qFiHVPo5wZXo55blUio9OZwC18KIScOVfXU/EmDShaBZZTuYjLfcasfdnfUu9rYf0SU3K0fgH8py+2K0YW28P1nn6ilnwqRewUUyz+dIpaD9DI5/yRBWX9wE0E3xyf/oLi3WDue9v/trdD85Gjtb7BEccClMHevfQaFq6HXKUJFzZAPbZGj+ywx2aGnL7DI0q/+BuWKsN8LQwloaDC1IpwTVj8vGTgqEuTOZmVuUREw9yYmbkzD+YlvNF6yvd0e+vMj/mzIBHY+N4U9VPoKd7TT3pFBLwyHz+UqK7ZrvBtChX6nZYRpJhbfxjMjfekvWTLqMurx7vl/US5usoHesXbEYM7+L99L2gPqurl4jJU/KuHCRBtS28BkK78+lZPy9Jdg/o/Yy//p5ef/FbmzXtMGAo/0j7YWGUbqnCV/lYP2YM5G38tf7TVWd1B7MJvdIfT6RV6oIW7kI9XlZnUGR5L9zJYyxbD+Hp7bcU5J7O+MYoD+ee+zQ36IGXAeCPbPnvukKIDynaFY/Fk33caRNEO1gnpv22lT6tCu+PQWcp3l7Gay6Iy+tXz9DCbD0jBwnsPA2T0j7uHU/5Gu1/6j+fkBwC897dPA/hU2Pz7+/JNFq/YxxvAhgRAgGXlmwC2fEG9ZXQJBG/HOH+oZrxO4l9AacJmQetqOD1x9pZm7z7vY4WHWgH2ntXXA5wFztq/jhmmNXMu3KbwY+rxSBi6Xkm7mKUP2b0hyhpBZg+b2nnj3PEUc19DuUaUH6zYcda2NNd2l86aiPh40vwUuvIQ6Wyxy4vg9X0J9fLhP/3ubY2rmythEeTvFott8ubaqnqdNL+x3nnU+/HUuDLz6tdE3GvinWJ7MHjY8Uc7aEXzTM4jKWAEmZpgxSPtyQmVsp/QbBZzzhty+xpx4nH/+tyfu4ofF7nLR/TDoZIJJQrxCc3pOx7O4+Y97fJ8IPWwZ7Fvm3hr2qs7IR75yHmy4q5xymkn02WAV5P1Frjlbs4vp+ZIwKFccdo27x6Ne84Z559L5EjgNHZYh/v4rDHeLeduTp/LYUsUj9Qpk+Uc5oSaiPPxvXTiLFMigPMu8TMIgQDADSRyCxcCsDIHjlA2YAwwDxEEGDtE4sa+zQ3X5kodYiLOGTKj8MLybmK5v/r/F+B//wS8k885DTV19SJRRKLpro5HNHV3UCG9Fq6BjafwtpBjlo5efjs9lqUN0Z7wMZXAwoO3abyvKxa4ZXh5E4R7ClBZonnhYkJbXsDrVeaflksPpOorI0kVrAfJQTihowrRC9/pSnBwdE+VvNsHszA1MLK0NVIOP1MN/nZmHT0G) format('woff2');
	unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* greek-ext */
@font-face {
	font-family: 'Roboto';
	font-style: normal;
	font-weight: 300;
	src: local('Roboto Light'), local('Roboto-Light'), /*savepage-url=https://fonts.gstatic.com/s/roboto/v19/KFOlCnqEu92Fr1MmSU5fCBc4AMP6lbBP.woff2*/ url(data:font/woff2;base64,d09GMgABAAAAAAMYAA4AAAAABdwAAALGAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGiYbcBw2BmAAWBEMCoIYgXYLEAABNgIkAxwEIAWCfAcgG7cECI7Ctr1xApKkWPjsEdDv1/Z90buLqIWqmhhCU0mkSPRQqVSGRtJUmH7nP9hl7w+WiKrJCbMBiO7UwVWqK0lPnIvXuBtYtI10YBPNJKj81L17FQg8CDzQa7fMsncnJ5mT/RABQwOgC5EghDCgARoIBFBx4IfbCItJgd1xZ1MN7M6bSqthd1PYUgc7HND5DwLhurSpDjIQQMBhQCDDMOFAn0UYBAiIJCFh/iqduEBLoCXYkmcAIE7qaQ14SO95ZyAMnyvxF8oiACHqFWCyvRsfjoDhWH0owT84yzcxENALQDLHL0AgYAH5ABYRUFALOAKQdAmGd/7/15EP3kCABmAA4NWkTIYwZJr7mRaQA1qgBIvgBLTGkDSLIOPY7V+i00jkek23tqO7okpWnaOtompN98CNB4qpZywN3n48+/BmasrwLcXUDQZDN5XTt9nubCUyqiwlrqYmXLtGK47gX4+wpTngc/1QaNt7ZfvjzSzlTlAui7j3yZNTFtq6+79njdu64zNOtC0Bm6eXM4e6tNK3K89/flil2sC+F899vVylhPn3btR3wLzPJfxvZGMhdWeHU9TipOEEYAq3BeT49hIgAu3h/29SJ/4CCETjBl9/LiVf6/NVZisD8PFBLwL8UdLjX8Lfq3I7WTOgxAAC5Df/VEA5/Uv4Fyq3AwEAyOMZlAAQ/iG9Qm+0F/npwr9hEMjNw0omHgFaEGFuIUGgUIZFiRywBx1HbORxDI22JXtfcuB4y4x4EiB7btVIcQjB6jXo1KRSuQotBE4cOBZSEK5eA1GjVBqpTjFxP1CNGoIkO2huPy3VrFSTNqVKiJLUK1KvRT1BjKUbk3KtahQCkEY1aVYZ1GUAYunMnXfUofskvpZep1o08GIhqP2rEhVqUKhYhVKiek3KWdSoVKxUnWalmlnEiBQsVJxkodZyInLAAnSqlFgA) format('woff2');
	unicode-range: U+1F00-1FFF;
}
/* greek */
@font-face {
	font-family: 'Roboto';
	font-style: normal;
	font-weight: 300;
	src: local('Roboto Light'), local('Roboto-Light'), /*savepage-url=https://fonts.gstatic.com/s/roboto/v19/KFOlCnqEu92Fr1MmSU5fBxc4AMP6lbBP.woff2*/ url(data:font/woff2;base64,d09GMgABAAAAABP0AA4AAAAAJFgAABOfAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGmQbjEocNgZgAIUUEQwKqhihUQuBSAABNgIkA4MMBCAFgnwHIBtWHqOirHQag+KvEmw4VNehHkqQycVhdGhspAQUSkAG7/xfhAEcuuOFKd+5oayg6vf7t3dnz7nwkUKkwBGS0TE+MsakfCoyLp4ssHv19ufX/XfOHTzYg+cPaj7otkzbxLhpkxpUVcqWtAswt72ICPVkWzcKuBi3kog+/GASlccAjGPCqc5T2qd4XfzmX18PpXAagUZBgkFYi8JgCUn3/nXqP+mb7mQpQLgVRtpo6tOXQlzXLoHkW6Rks5pl6+uwMq6CNFPh6JwvxIpwIB8LGohP4PuK1v7/r/2vzv73y53/BrPGoJ4ahEZItPfP1zPyxecN9gQZfHAfzCOLBJWQPg9TS+KNECmNULqvji1Raxa3b6lEZnhCiI7IulJ/ejAgoAUA4DoCKnBxQQiLQEhIQEhJQcjIQBCJEAoKEErKECo1QGgiQZiyAAGBADAKwCggAAZAD4JatO7JK7fB+e7j5gDOTzdLe3B+mXo4gRMNAMlgAJj9CzcnkANgABjQQABwUKJ0QOhFA7DBfSe4hxP4xB/+GRdCRnymZBoWa132e8k4jwu5rpHHdo6J25XOc4+WCmiM5QnjDQchyMXNw8snICgkIiYuKYPDE+XklVWr16BZm049evXpN2CIZMocJQQzFS+KYtxkzhJCB4lnnnz6imed0vkHx84wNHjaQY0G6gu+CsbI3oJgNRr0WAhT+wpwN8frcc2oLvSGejWjXx+oxxAzVrTr7cS2nXbN9DAQOFpAyLfOLtKYWNPibUi0INUOKq3KtIxvC40WZf9xDieXb1ahccU6lOpTbtSwqo+2w/1ppQZMNW2WeE7g+cKvg0hbyFu9dGs40RNakVVYm+1apzqFNhVbNAy+L/CUg2eJ54mVftRjA3pe8+EQ0+2gox706ehgINKiKpwtOMeUYWq1GRWolCWVylsnUBmx5F+Sp4bhoXupFVNUMOcnEGuXx8PloCHYvFDdOAb5mhAI1qrU3I7rlV1/gQJfO2DbQi3CoyDeHjkTxH/08uMDmhV/NWslSM1SyZTZydyjGZHb0Td6C5XWq/KUDYuYr7dBM7pLtJUXTG+QpMHytAOElojRvFhGK0bcRnSqehpUxkrT1Blq9VsQLtAARvXz6Kng2M9pkHMMkuUqyP5WWiZhz9zl2+4oF/V50inPdahtLn/BVrRXLmcnlrzjNd1y86mqcgjVlc1KWXzG4XJbY6TRvIzmisPkORVVjKetshaw/ToN4zvcXNSo6eB0kmzeLL3oljOdvSKbydOtPG26uMudzo0MAHBQgosXsFUMDalTPQZEj4VqDVZ/syEaGEU9D6me11Q9nznHDyO+YisdEEAgcNQaqBPQWyq4DgwaGCg6dtACAyq71R0NKC1QB7vopVWPZd45t1YsEATs3/x0WkSZb9YtHeRisM8NcFtQcKCtKIRouAgmY9ixh6z8z4kIi3BGeK7jkYV2NhUYgJJatAE7WOyMulm5ALY88Grtvp16uYaH+vVSAxX+A7x8SnXwwywAAleoHOJHAbDRq1CAA5T6N2j4dfHz9ChgUaEKV6xyRFWJmtWhHg0kA3qp7jlScaKoSDS1qFv1Db4MPgPEHixJ/MDmryUgWB17XaMScBUcmkUO1Zxq1Krj8vK9j23aeXTo5NWlm08Pv14BffoFDQgJGzRk2IhRY8ZN4EyawpMIRFk5eQVF00rK1ABgbwEANgC9BBQAsAgUSJL9moIky6qClVDJAU3hksOqIjlWwBHEIR4rSEASTiIZyViNFKTiDNKQhjVIRwbOWZmQrNWUJbmgKVuyTlOO5KImimS9qjzlleSSptfZjeIN3uAK3iIfG1EAKq6iCCXYbNEguaOpVLJdVZlSLrmnqUKyQ1Ol5K6qKm4AdqIRjXiAJjRjF1rQgYfoRg/2WAOIsw9+6FoSRibEM7w/m1sg9kHTBdhzwC4CM9jegQI8gcwlLI2LSzFaQClUPg8S83KoiLlp8xLGsQljqKO3lQUldqtUMgxpWNhkUqlolLDJKiC2lcpSGpiXumwxC1LKYrk9X+ie6DI62mnInBxsMYsls2e+wzJGWsyW9l7snRjGwda2wWaTLY8r1WJtxapCzWRzAZl8rFt14nKURZDsBNVYlaszAIQFt8rZrsBweLJZNDEbLq1WzC1Zfk8256i8K63ZUvM8033f8GN9uXUP66taQeuFUax08SXi4OgPNj4dQvl/eP5W9sExNlAT+0SR6DVHpBydCayt8eY7Bhs+sgGtXKPKNaxWsFalR0yexFyd3GbRAtwEzWI9zDKKXhEQnsZpfKCCZLUdAAhL6iSx7SvAeX8cvoveiOBovN0uy1he4XMLVEQnYISRyOt7Gg91FTUskUZXqfIAmfpVbePGYNIUxLCaTB+5mf06e80sboK12XDkOnamdVM0lbHXfcPta9QSZr0I7+pyk++0PqqsReHRQ7h4FNc4OZP99McWCOmVQgj/XSAtn8FB8YJEFeJ0LwM3RzJ4bA319HMEzvpgupWJwwjvfauECEgJA8opA4SSUZR/CxGgXRYG55M6GtVzbJRtGM7OyIGsR7R7f3vKrN7HAL8+kjXKFQ05zLLd0OCKfLLl/rA4CHQ4V2uow/GVa4xRpO3JDbKqm/0s0RMZ7nak6YpmHT5XcKLLoZs2SQnLBcbPFLQkME7iJTUpgctiR3aVSpIZTn3nX3r+BuNtV01gdZuYKmJpKVayqOb9oynCS9DaIWjRsyDkxyhumCJ4MSIu52s4k/o8J4cCR0r5iafT6klXPqvqITsDKZ5wnnGulIb990kYZyY6hlEIj7GVZpTRNTg5I48FhFamuCYCfK0lSpl4m0fha9j86K7Y3w/1LzuzpvPMRSFjQ5trG4f9ZHmvdRrL2LhzPau+K5CAZpbPT/XxSpcsekFqshvpp4fiqjjZb6HsTS9LxL/Yegbwkg92gvP/yiG9uuzZeGvFlnTSxv58NL9vSJzvXbLfRAwo4NEQeRMFdPe/R7uQfvASz6xl9m6QKP24/cgF++nliIrEXDVFpHN+PZMzLyp3OQPGZONy7jCVReCV1DT9mU8WevEGL9aoci29pjPtJw9jqYK85KSV7dDed4U6wFDrUJaPAcPM+1zPa5PdezYHhTAMRhii4GROckJC0jMpT6one52fapghLvJdEIViwFA/rNCrylx9X9tJ+4unVj+p/jk0YV962zWzODsqiJqe7mt1PQX5NjQg/MPLIm04VUPLv8ltEwoYaoAtm2w+bJJpZ2kkwFBJkWcjyUBY6QgYoa2Z9RR51Si8aaZBivwKiSD4lYpNwGC/YvL05L6sIdh5tDG2jlEK/B3TwC5J5/bEBJkeGVnHYnLSFwg1Ic2h6ghKPuO4w5HExnAtRN51yAmTR9b4cjOvK3a2bilxJKM21Tq6l5XYQev6jZtd7NYs02Jr+9+JLVdrz8+l+clJtE+FCD8w0MPvQMGT2Au2bWsC8toXU6zjptu3puPfkPJJsfTpmfY4MEsQ7xWLPDQbLkOSOdQbHgsb1i9sRAyPoFf0bSfuwwOVdOKvsdDwIXa136IZxN+jQRGDHDSuN0tN009KLu5jmzD5UfsrF/vs5t+tsJYnEJdAEtXFmYpa4EjAkuAhVXGlQwrbfqX860H8wIE3Hkf+4lcc8jLFTcIdScQ5MqWYvu8vvktm9QbMCOS2SVden9SfuF4BlM2QLR+cz1QgbTJ2kotXKRl4hXS8HNRvgWTi00D8H1MJbqkKOfkKKWy5vFz5r0qV5z07cKVUJ9BIoMpaUm9pgFwAVbz+4tgBj+9w8iZFyVLpgH/PZ3iNpESoQB3ld/mbt1OKT6irRdmo27WqeCsUdf5K5P0NwOpRvbF3qAHgbp1ik8LEz9th24G5xeJq6wqrs30swf9CalC1sOB/22bzMSIV8ciwxFZw/ARoftnNR7IGxe1qRe+yhRQiq8hVhUiWoJjdlOSYPbaQfGQZWdZ0GLW98nJvWZP+oWZnaRBl0ofaX4a/GzSuoTUoYyGhtkXk9rkCyIFrXIJY5BIPiYVsVl3k7MlWTYY1r2SdwsOI5eeb05Jl5ROH6C5r785kHfbutDn0wiQR3+ROCzy0m56RuZcZcLDYoznZ4Ikh1qYVTsng1OCLhWAjz+PA44THDzltHevmv46hWee4WCc1Kmft2ziLX+DaKDNJo4+3spX9MoPtE978gM2ydcVTfQWWVnlGKkFy8fUR9bsz7ZZfL5T+IQLmm4tTRbVjjK9PVtpzv8KiUB0V/JfbDjlFBfFRH6lJEGv1QBl0PHBkkV3xCmKFKFqczP/5suB94fvA+YqV1JxIyX1yjtUq7GyEY+DD1EDrCJnOE1NiDRLJXpsjL8L62aToIl9aOeLEmO0V3UMxxWV1K7o1q/VCFo++ZBEfc79qX60EO4WmqeNmUnZ+hJjpnIaS/Y9B3o++eL0Mz7U3qdEusqhKgkxTB9MPHnFRtGfmxMS/n+sbd97B8X4qIeqvX7r5HNHYxTlMukjLSFL/pi7eIu+s3IVjXib26gRtCXyYgesxvPevHS3zr7VWwNOHwxS6Vi4Vf2iE7lvEq1n+qeG7+L+rRByczbXRWjQY7FvWQyrQNZbx/QNLFxBKGk+vRd/5/Y7lWAHrv6hv0Xp7hQIZyYVo5B5Mz+FU7p+9e0VdFG3uZEByjgxTAu76jfQLyaJnRXU1xQ1SlCjMZr4qBMtryi4sNYIUnwAxAYnHG1PKj+7Moo3Y4JjlYX8FEuUj2X7DnWz9ZGD2+/vAtwXPTO9pKJzXPv0w/KK23vHXl3WvKV7W+Hc6X5dofSHBIbGwO03ZUyY40Sxr99NQorsK6Pf2ltQ8PHPvapp/vj7OIYiA9tFeYu83axzIognt81ocPLsjB6om3JNkiEyIiXUW5hfnH9LGkl5PzQxTfidFFIxDGUj3Eb5OdX1ye56ZGpDxnd6OvRxtGittZHj7lFlKwrljzQSD2zpndSwEN7R3WHqU09x+9ASDfm0vzuECcSoOG+9NHwkKHWRR+y2yzCY9qH48Vq2+fi83q2G3S7ikfo2SXbdRCSfj+uJqTHSNrkid5RG9H/noiXt0armLFRl4dXCFkEbK8UgsYbQiMa7Cz49HqDMiEXSNrs+0wZ48IPH94+ZQRhQG8/rePH3XUNduHRHY2bVXzSNs5O1qSGoPc99P3JHU+MPdSvRWr+Ke/l+JKPn+M1zxmCqo7eIWG7jGYBK5HYx+gQ85uvJcSEQLwZ7vXbDDg1FE8/4n7iPG6xjveBdAVBcXLY7k6Ain+5MCaxFlGj7qiLKAOj9y5GYXkjnm0IsrKSf+R0Kz3GfvNyT5p1f//vzSKM+IP402iAD8TYq+hCFmHNHTN9tSu7DRi/dGXIPcLGpMQnQO8tXCkgAXWz9HwtvvL5MLOivv38TghWKt85pbv/JaogUbbpp6gg2cky09hZMw8lc5gr8gdUXE4Sbprpid9GX984dV/CUMxU+f2ANnQzXrkXVBA+Erw8uaDk4PYOG19/W9tp1hxbbCH9f+EP7sF5aX/Z9+ryPJ2qgLvm1DjT+ezTedR4x+eEH3JL+sN9qSj8iDFXisvxDAmnci/laB/M2UR9oEP6+uoUUp/DGvCyf1+KzSlK/bW+tnfQy/HvRE1Jy95u0h3aMuhhcwaLPXgGQCK3A5aACETPsvWKWJHm/AJU1W7yXINHnT+GQIqQWadRyE9B+h42CHDaZeldIM1OSILX8uJqABeAgYwQqDkAm5UAilUAm10Ait0Cu6/HyeOaPHOSlV38xbOywiuv3hkX1piXpK2ZuWMvUGZJRa8lc5mwWh1PGcW9iEUViWNjVX19z5fF+p+W2wjeHitF3YlQrkziqju8swywHrHtYEo/4/AfDGSXZVh/CCmK46eRrE+Xo4L4PYrEfyETBC1QZybYOpe21Q+KoOam7bDaa07y7LPqRZoVJsexU4j4e224DN1r2AEaxFVFRtYLdjv9ZmrdQhlnWAeIjgA44qH2UuJvx8w6SedP3NudXxrSyw2dpaqszwfGiK6qdp3+W+Y8uONNTcWu15DcShwCEhqyggoNnQBNU7PApUhJJsdTWUK9QsZJsrKotYd0fIC3JzY+gQALj0dMvVXSdj7pP/WURbAODdryk1APhoN7/+XvevvxNr3QE6IACAATQOQw6A7qhF/tej3bzHzp6tcCxcpG2DYFgzi3yICIpLPsWO5W/hgA91beAPzLPjetZNt7uR/MxTmhbs04w0JKQ1THS5cjc8eG0/UOIer50ZfqqfR/S62vGxxKGyzTjZ6jSPD0c7n/LwoW4Xc39HQtqnOJtnOE85RVohPR5MzgM21yYd48HzSCDUrXgX1ohrndOpJacy3sluDkeYHuH0nxQy+5vI+Z1MSCQREJey+75EucnvEZGnd35evrD5aPsDRS219w3USKukdIGaW6ROEFE3Yhc2FwVqgFJTMkpLiagb1CXqTnanqucQDJnZSioVlPYpgADgmXsImcAAAgDjAA7EDRYQgBZWUIBoVgAeAqE1gt1MawJaOFmaGpam0ZqBqBvMIu3NxWph2Bea7Npz7sC6VWuO+GUJxJPu12Y3jHpbltvvsGMRxw0ytooGveahxMuFt+zAyews4WRn1wJTjobx6/aQ3b/q2JZ5YFQ/cGjd3EkBbmxOSVWmYCmwTkY28x/ZU4mfxanHmbcHXrR0v4yz68Cqs2VdcZftpD2dQ7xuHZq06DWkRSZBXEDBsW9pZwk=) format('woff2');
	unicode-range: U+0370-03FF;
}
/* vietnamese */
@font-face {
	font-family: 'Roboto';
	font-style: normal;
	font-weight: 300;
	src: local('Roboto Light'), local('Roboto-Light'), /*savepage-url=https://fonts.gstatic.com/s/roboto/v19/KFOlCnqEu92Fr1MmSU5fCxc4AMP6lbBP.woff2*/ url(data:font/woff2;base64,d09GMgABAAAAAA04AA4AAAAAHggAAAzjAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGi4bhU4cNgZgAIFMEQwKpmyhfQuCDgABNgIkA4QYBCAFgnwHIBuUGVGUk1YeZF8k2GDM7MGCBa2EMmPxbcVthwuxCHwE/3jcGA9fa59/7rzbPSF0jC7siCSzSvmsysqUD3syKhWP5bciFOCHaXj6v2935q+t+5u/LdZJOAw4SyNLILRORFEHtM17eEKedCYrrA7AxCqsxAB1cxKLTBYdaG/tSldpUMjuDwbiA4kwg9gCJ85wmLiypvm//frV987MnHd3ff7HbPVidrpY/dpIYZ7s+iC6gpl9saYpcEiSRC1pgyrWCJ1QKJncCLnh98PtfdywAqNOR62PMP4bCmoKsAiOWtOA6MwBIRSmwTQCAVOceUP37ItMBPaVxiohsG9U5ZUA+25WTRmwoaBNgUDcuVZVBmQICD6FEFOfUERFrqEYAsAcAO01ANA+AtBUAO2nP/wJ0O752c0AfuxrcgzCBoCrjezrc6D3o75frMn+PRprHx7Z875SV9rvZo876kbU4UQ6XOCEHNRasnw2yriXSu6tekMfbTrxzXEqXS4PDESXw6fK7gslsnXBQ0VgUTlJjEz7yaLtRS8NQUjMy2ET5UEQZe14mkRpPuZjZOzL3PyTi184EkNiqBnunZ3zZnlYIqA8Kg4gFl+2G72X3wEUoSypkC+vbP/xmBDwDRznJRH+wfV+dQcZ5DkgdxQlDg48sGoBPLmoQ99NqOvdzxBTYeAR1qKvs77P+svZMzUFRIcwF22c9W3WD/orpkyCifvC7YXs2UAITLObtYYGyNUcfVNdDT0CrAUxC6xxxmwwz7bohfIVbLQt+qZ8B+tti34of8GYc8nL9TwAoTxfYRNmwDhATAPsBt0EyDOgnATdfwHK15byPa29plFKpDLBCPXv3ArbA1ifHhlPRZl0dTUUnY0xMR0yqsZksFioDoay6Tp0NluNpsWkojSUhAydpHWLU13j2vPTsegiRoOosJhcksYoRkrImelJFBHWSGliNInoKAuYIpVFGbSK2xeJ+55TpCN4RDL4SvGin5vYMUCR9uGgvR+TDeJFxCc7Qe/2JY+EEcUqRS9CqhdhDSiF0IQ1MhGWiJKCsQ7xzLfZg0heYRgi6aFInwBP0ovJVFxUzIVsWS8foRRnALEqQzKIybgErJHCc84M4Lmvgk7m473XKpX29CBIA71c1g/ABK2+zHCfNC0ziQykPAeNooalSM7LzorPSM9l+Mx/AyDuNT+CcD0TyNYskyImZ+YRMLQdU/BJGJmSI8IabCuywbygRYbfhNFVSRQRhTyGFDKmY+GpCA3BAGuQRo9WanIvB3ilEc9x9fYimPhJoipJFp+9LPvnZRUSjJWiKU2IKpohUnjjXht6+RsQEayXJVIQPmY2UhzHnBWZ9STm8kGeJ9+928f8JqS3t595fJkKFj/PEUrOY7KzID5DkZ6rpyOM+utnY1yKCMvqOFONJd4NmcnBMqXExEZcXu9tRNLStESoSPLrZ071HDvmL0Bwv0I2PI+0muR1JF7Gy+nURB4janAUOBp8Y63F4ogfFcseIxiBotajyV70A+Q/k0FFnya+xW8njfA2T8z0ChCUV/B5aUAgsdqD6j5j9a824ZH0eUg6Hjw3rVy2bIVEv1ZZq/7JxbbZSKh6gC54XTq1K55RKAWCck4RxWgXRIuAoBRB9KXum1IbZMuuKTeh8/LrcntkayxskpngLjUxSGNm929/MaeuxH+2vvfwvTkG4U7Kl7C4rf9dzYoRjX/o8Mem/hMPbxU+uH1sALKXTX/Mns95JTcUGXIeyxeDCyevMq8Spue+K32hvRBC1NEc77hLSaVrL+ybs554/GO/qGZ+Z3fLlraUepB8PnPg6IMjuE2wjhNF/c1tOFNtnvmp5xBys7U8dW49lBiPPze/OFi1N6MeiFCBLqja8uz0a0toIN7f9mnFcNql8bRHgyt2jIL3NOnTLtG3FRZrG/72zm+9OkJ9H4Bqn3j0Y2Pm4iCPyHDXjnOsaeNdJ65O295NQ9fsuAz6De4Nclf9c5H3Au9GnoVky0anHZLK+zDrf7rrGv4Ndil6qfe168mXbvzZsln8ce5oN6fe3VZRsvHmv9vg5NtdsnZs3+EbyJUL9Qed/qGBPYwG9xr4tmfa5fmSGa+/1SddOWVBg2xVkpBnVOS4SLfTrgOmHYNFI9pFEKrxfv/7OCQ9vE8V6pPvc7D+4H63y16BbcppcG+AGPuv9cp24LfKLeHylmsyO9z2nV09+eGCYrSofUX5ltZzMo34tThVI0YaM3vw+bwPPprJpNju8lHngmRiZF5HwZLCVUWs6E6c0mpt4//BLnk/xeZvXaT2rZuWXbj5b/MmaXz3L+txPTl/7pria5tObSce+gc+S9TSNVvbF8jQ7BpkXmI++DnNhFmxf/LB8siPYfmOY9uPldLGJiPB8uqj6rWKzvb18pr8hGy+Mjs1BmyP3s2sqImPXFjqZjPmmoxLE9bzbAP0Tl40s/b1cwUTjUctPGKMsa2V0C6NGNm9Nj3GPyU+kUiDDSP654xNzunrnTUxPgvP9+Xm54KVkzJjY2VHp7r8jILA3JBD2RG+NddKpXejQWvb1RrqV/mpvfKUileK7qMj+WBz5ZaCxuW6dWv8w/OOjHRt7/N1242T10+24b/O7auG8nB0yDhqKxpW0xUb0LCU7mj8De65vqdN+13G4weeCXwvzsswlxlaeiMm4368dDe7er6p/ifjUSilJ9p1NwYsCcq0lZXo4TGm5fhnBu8XhPoOAhQnohEcM6N80xThnGURfs68gHiiC1gabRMsr2sTlz0L8gNSw7kJ72VA9i+dDxEk+3OexzmxNU2yzf3ogWdSA20rw3Q1TOosoOcdzT+6asXh6ZvX21yae3md1awtpzhKxzDGx/m2qPhfltUzmLXFEXCroNA76eVNGJWvBE/UfCM/TkydvwCCFiY5zKhH+eoHYHkjktCy20zMDXHF3DonsXCLDxZkoI1L95oULS3Aym2zQxW5b/hq7IoeXC1NVQs4Y+0OP6wpIFbM9MNn2HggABuLIKutFqG2jJlenff98zLzvzckgSU/jCLpZE4rnB4tD4YALZvNQOdowtERQ8bcVd6YW+hEqHA4QWuq4j5eNi4db1C01I6VIzFqW4295quxK/rFYGkqtsdCiY4RX2qSOZaYel8ySoZKBrgVCiV1zClgzDldbRxo+VcuWkrMwDxoSCDmljmBhf8isNABvnFpqknRUhdWxlmEKp2+5quxK4ZwtTQVuzgTLd+qIyFeRHvNKjqAvugoBEVHvmiFGSloKTcDXWhEMDoLIWOS44EgfH77zcj8A2AsEmIBLb9IEuAdZkodxvx1+EldzJG0j5Oio/AoOvJBy7EQoKUUCZ2vwtGZ7w9M8m2bEOCNgKnxfQhUfUlRbzlAy90Rj5YhZoac/fF+BZU7rAYNGqw2OzZ1HkiG3uYGGGs72ziPYxHIiZ4pt432elHRAfmmd8IZIvDKCiwabtmgKGG4NS2jfenBabWO2lbpuvyakV9hhmBp5H3bwHpA3m+aXta1s7dzlV/sRDVaqGuSCx139X/k2a4Y8/QqJ+7J/ItSynCnqowGANAfhzbqqrV0pqMMFjzsN2yw5CoQaMuvw/Alqfm7/WfMcqn7m1LV1Lp0KGfv0qH86ootvt7ZKiDe1SIn714ERUlxT+JermgZbm30InyXXhsnoym0UejPQht11Vqyra2gCvJbaNhYKvnZ1rPul4NN00u7dvZeHcqvOie9nTSRjNZsrAB2mNVSXSiDcLegzoThYofcb/Na21KUVl7ivcXdBTgK/UFwlNc6vjfa+xmX9ZeP9LirZtKZUK7DXYdKhrt1A7mGznB/+4sSZ7DCK9P9X2V7ZB0o650qQ4TDFNpegYBu+ZwHe999y2B4/iTrTgLw9rOsAfgs/ss/zoTqZ0+qBiZTINDfHajA5K0WmAxqBmLXz3jlcn2k8S3IRpIKJz49j6dYS/G3ebfojGogXZiCDJObVYSpLyVuWzaRlMyzsndxOl/NuZQwbtj5lupck+RSZfOUVrbCxmaRVqQY/H8A/l98ppbRhveTC6SNatuiqcFj1Lj1tZO2Zf0Nlr1UenzMIXUOxhBRejAXNmOs5BqxG7EdrFADq8WwbdPpueKGVVh+Mrcpcmjk6GBEtTRs0fwdkjC+j5H/mFCrRlMZzeX+8XoWEkJgnAVoSUYgwAx09B4igUnu8CTVAOfBIyT2BCE19Tza0x3tIxRodCZCsN13OnVzqQDlKjSqUqRAoRoc9mzZ7X0cIcoFuFBer2HK5LBO+L2pwgnxDVLVJJF3eZQ8VerkyWUtXrls5WqU44j0NLzWLVBLKAsqz1elWlGqbGjVeo0DFx5MKrp0M4yVqdatUcGdjST1y6xlqUAixzJoHmvlqhTsKFQkR54yjkKc20akMAGCREsQxGrY5bbw2GErLpcG) format('woff2');
	unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
}
/* latin-ext */
@font-face {
	font-family: 'Roboto';
	font-style: normal;
	font-weight: 300;
	src: local('Roboto Light'), local('Roboto-Light'), /*savepage-url=https://fonts.gstatic.com/s/roboto/v19/KFOlCnqEu92Fr1MmSU5fChc4AMP6lbBP.woff2*/ url(data:font/woff2;base64,d09GMgABAAAAAB+YAA4AAAAAQ5QAAB9AAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGkAbjgwcgTAGYACHSBEMCtx0zUgLg3oAATYCJAOHcAQgBYJ8ByAb/ThFRu5Gq9UVIxH2YlHyCf6/JHBjKFZDrT9wZlLK0Ez3tHRUuO6bGBm1wwnA8nfsvwclvwoZmyp7QWqWmxTWjHTYUXKrZOQ/b/IRkszy8KgH/+cmaR+gPuGTmKzV0MzJbOxp7Tfv7Rmilk5w3d3zBL4fSySzFEid0kmVxA9gt3+3jErh2KWzV+Oss9IwV+K4geOM444zVknWLPvIaDlEmjJWNKymYjc2Wup/PG+/+t+v3fckeY6hPMrisAg7SwotsQI//y/pBACibBY6fmXd4ttP11bSCsnhUHbOHMZgp9ZQYddL8ofqzm9wcd93TdF/W5KmdEBjkmbPkzB039z/OxO4MLMb+EX5ToWqcGVPCx8QkozZvOeyvUIWhSR2rfouUMK/XzPt/gNSBUThb3xRmNZVqOT9+1luZpODwDHt7WGuuJsCkAcidU2ZFJFjEJY9G12jqjyQrZCqluH/477p/63nzdx2ogOY6kLbYpGlMQUWSBZS5y9/2V1mJnk/7Et4ELJ8+pBshi7xCPeNRTh40HeH1hbyKP1aF27PnxSlysOdOuxBzBjzELS2tyQknsyP/UrzYI/k1lC6meKIRBGHY8/PfsEEVg9gWUi2eYcgLCFA2GID4VoZhG9/IRAGQ8FQBAIMFjCF3vCNrStIHfAWRQOpQ+5JoSB1ZIURDlLQQN0GBIgjPkeFAx8IECwMkmoxhHZo0BpnfNhcBMc+qhA04b7A+N0NH0Df0Qb6sbMIO/VLkGkBGvJ1JUiEMIDIWl1dtqKNoPrRDFs1eyz9PpD0A9TNAR85H/kqGSci41Jvcxq/Wzy/tTUC62fD7Ks5hbWAsWRNn86/D9eGk8DT42zDVz8QlGsWvbcat3Grz6mWXsuiD65f4Ljhk7oIEa7DliLFjsrlUKutKBbX9drl6wkYSbDmGKQWH1Gfg4UoJxNqtob543MEoK4mYDiWkxLx0+xGjbCOufYaxWeC7YLtwX7eFgrFPVJYBGOUnDXfl+Fc/w0vnCEIQkYVLEQomjDh6CJEihKNIUYsJpZ4bAkSJUmWIlWadDly5SlQpESZ8ypVq1WvwUVNmrVodcllV1x1TRuudp263NBv0LBRD0x45IkpM5565rkXXnplzmtvLFv11nsffPLZF1998926DZt+QNjQDQEdMYpwzMTpt9ygqUMRoOFqYTgK7TOIz2B9G63KUupaiCgLIzXR+1D4ZxAaU3wIr2y9OgPEMSBkEpSdOVaDzkm2XIeMRS/vjNlyVcuM/AF1upOqTUYBY1Z1BWZ1h7EPV4uy1dD6QWxjY9RRgwGAvqSk0mhX5nDB15iHoi6J7gruCt4qBlVk3uqtLY7M1pvuOGLbZ3YZuWSKZBaGdOTXQBrsPogikS1JFHf0IkAYohPniB93T8Jlfs2WP39s+QvBahvQGFyjCj0aMNBcVfogZpiOkD9ETgB9d9QN4tQ4Pr9gk20cbIAQMDSGB/7BwQMuXDAIcT2grF5cOLikAUsYCplXXjXENdCRAmyMAMOJPQihKEY5OjmYE3HKqVCSlOTkpyj14eZbti/p6+Dl9Soyr2VOy2zI/MHhcI24S7jLuHbcLdxdWez2NkCgkRGOUlSil0OxjWv8Q+ULmNWlbbF1SZjM2jIpMusym4sGXNP8Ju7O2+2QQKUGKhlpzP35fbb3/2/ftuVs6f4FoH/2348l9921Axd/Lf5b0IfPvnVivhVHPVtAgA37D9ZX3QP7Vvbl8NfU9W1ZS/qnajVq1anH0aCREePm/UsTpu7rM2jIsBFkFFTB5i1YtOSeXv0G/OJ6+zJKSw5L5evzCIDDQYzsb4K6xyhglYrRzVyclJiHVRPzcVDFAi8LnAdi+WYVXJ5YFatXrOZ1gWtBrOf1gTVAbODNgFtBbO4tgLVAbOntgHtA7Oh9gIdA7Ov9gBdA7O+DgFdAHNwcsmX6UBCnAW/sTwfWSZyPdRMXYb2KG/Y3pu4S97N/IJn3+An4tuIf/wscCuI/V+AzkCZ6EvAcSJObKXgpaRVsnLQbNqnpHAA/gOlrex0A34HpZ/hcAF+B6b/4VwBfg+m/NX/jp3Qddkh3M/5Mdwt2rO625i7slO5u/JHuQexe3ZN+CsAeoHumeRZ7ZeYYJ5qxDwTm2eqCTKrzH0B/UTfe96DjwPA1wICXQLkCXHBIy9fq9YtPezBt0tCgE/Cip8HZPZPqe/duvD27yfFiVoBIeHDvtQMYkAr4eJdVvFeAQJAwYTh3yUxUVFaKqIMg0QfeU16wADM7jbalkZZgJw6LMoawrVORRAQI0bTMZ6VepLUl4LxWE1/Tovet7z92gZidWk01YCWqHseAMXOeAyq7kL0fD7NsPOsa7oQAtRz9OE5bc7OTPS0Wyf/D5/940fseXryG1wopDpNwS13ZyfgXDfKLw94vHCtLvySDSSBYjupZdKx+BgjmL+o2Og4p9r2TIquhkhqqrMxEOsCAqLYdDHpnn4VQFE8caXnBKCyfoLz6TcZZnbOOicKrgHYxQYZxTdcMiVk95fdbOldCZVw7XVl+qqHuRCf6Ceo2vyBpyxKMSn2qjv/uqp+tOXAEtJ/Fp49URi/1QIC5AjrdNMVeSEkv/7gZ+4YjT0CpnAZA4L+VC7MwBBQpqiUvxydkNWKsI92aV7aaneL6kvXC7bfqDXXOSPwoKzKjvGNfwyGUCmtoySp0lOuPbjzYcyDj8xa4XjmyzcIVzkb1SONUuFtRx6NaHGIT9gFL6QtY3anec7zm3JJTptQGa6L0M+7GILNjqgWoIKIWO3YzFlOiBQxh97yJuNn5HYdsXgh7Qca2Z5Qf8Yq40LTlo30avw3fDN/JQJaV/1Rt4fbYKYZMN2W7ZpK1U/hLfP4KpTbFLgvvvgl3VreRHn7sW7Fjmren5kF1XIiE0AcYq4g9guQzusRWuWky0YxUZUdE5TYpJGGg7Lea7Bd/V+SEnEo08uec7cc7WvPax7mV9XtuD51jn+9v0cy9FtX7jn0JFUqBNciycLA3nrPoJ5Sl8GHivYnwTnLJzkt1tNiSayhOlH0oObx5U26G/LyB8QopzXSk2b9K0SRLf2umjRzeDiKgltVrsoxmKNYcdolRX0lVL8U8G1iSj3ogYy/iFF08Pt7oeNQsgFu9vLlPUxTabDAeRbOwVKFGBm62W4ow0CaPue1yOnJSp9zUWEnFeomSwCRxu/nyA5JzcDorocoN+pzFg1mRTWyFhNc4IKNBFHjX7U/Meb89qFVx3vbWq/kh3yLY3Hi7T/EJPOhpuLhF77qkPV2vi95XEKzQhgsma8eeWNw+E6BxQKWCt4B4zhZygJlaCFlCmqQhTbwT4Yd9zRqFScIc+uLkNktY6L1QAg2RuEaxgljyR77ulnaK8yneGQFyhtCzeIbDdBRZqiakzt6nvBFZMlgu2IS6luCDxTtON5j4OO7thsID6Q/PzPz4sQgWwefpKB8my2b5xe9V8R++6XBh5T1hDLN3ffy1qhWtFeqUE31P2cnkS2H+kRbnCLnffalGWg2K6sJAUC6ji6p6TshYLucr+mE02qkbGtVZPtqmJHkMFYWn9m5iK0TdRmas/yY/xocZyPjyx+mkIhOxHuUjnaSGvboh7Okqy2FP1t56252OnTMS2SaUzuLUirZtzpmO6vecI+haRutp8ji6DOidIIxlVWNyqZ94KN97lH+QPyhAL181KRkaK7sC9NDxor7wL52lkq8FKgCUA4u+/pQTDwYORxb9MZTp/wQI+9vsVrQeDQDhweCR76mE0oUi57VKCvRZm1M3BJRjHGGkvDrYgjHH0JPLrxs2MbJu8asdwrNahRsyq83Rnsmz1ZbYF0MmdjRAtVXMfZKLf/gple31yThEJVUcJqpg0EKrZSuUKnwCVMJtWEr0VTeryblZ4f5YpSWR4ro59U3ZyYdnT1X3bs+m+ApFnV8oblOcOmMOaV5360hn0NmQVYAZKX2YjO9rPaJa6e/CxptSGQQMdxBF8gUMr1CadezXnczlehTi5NnzqlFgXF9RWlqeKhfDjbEPfIoOEXFU2FISTnwfllgV23jH+f1Bpgrcu9ufqf4/p+nD7n+ibxI0o0jDH5QxHf/+Ux/oG+gR64EewHDLO6U5Ndr9ZwaqNWUabgkZ2syLrInqoFP+1przACb9t1qd91DPAYZ7OniHYiuYcFqaW9iA4Z4J3nmgGex0rpp5C5JaL/M3FKpDX4+2rh9yUWhFcpQtVfuosXnisyzflaaI/6eys2ojdY/hjFiZVCR8TbhFuMUGDJedR8hLAPs/7SXod3ETth9/+TE2s/x6hF1zsaY8+9oNRDVsFxNj/D/dYyGtBqHKgExVrEoZ6FMeU08AhpsAol1c2VKMbBemcb/6eP4s2VX0KdnBbQ5Jzg6wyTZwffMmweW4GQTNz8eDfX/W2DltREPr9mHagbKRHD1EoyPUZysj++P38H5WfTz+TW1XBeqr1qPJ92o74dnJZr9BfiS+eI9ZHM9pmM1SCQ0RSNeJGomrvN2Wc2Ggtd0HzBDkuNacgSTK8eLgK83CT54WrtATR9gJpePXrzQPlbDo7dmZIWNRpNqwK8KPFy9AWMZQCvvCanvvpYelcMzaZgNsGzZgbNaD/eG4IXcJipJfvGOUr4lsoV9/jH/RM/mA+mS8+yUMntVYPPAMO6hLx2boGr7QXhZkKCeJu3FjRRqjFfLl80UVomPFrblJQCyVnpbKwy3nKLAVcNM5RfCFkk6VOHkA/eFI8BtPOIgjRZIiQTroXdiieAHYCKMDTZ36PcOq7l89XcNz4+MLNiMvIyuxIdmbCalf77R1PbmOrIdqnN2uX26sO9Fq/l+ediDGkug+Z5ig7pjYksNDj0YTIzl3fGizv5W3FOroP56czulYm+a9Es2b1FSZzOl4Py3/uOMthCr9W1DrexV15RQTeCACnR/VMH97RQNYPI8vfil/7dv/z3fqVXnzJzDFnnuTyV4vV69i/XmWlzS0tuu9JVq8Z2qzzr/I2sj2+KH0e0LYf5k9Q9imLAF0ZfMAhCr9v6DS9zqqDVMZoTD5fmM0ciII72Jhk1TZVtbMRVadjaqdu7Osvt1gTAefoV+qlGoa/XhJCOmn4/1vBIddax2AgWHmCBSwck/TL51TiVllAg8wQY6FZ+Uckrtn+8jqoe1dEJrB3y7Lr845IIcg3tHqeHjTKEv5ntVdZaWrVtegCnc2vZoFPMB6FhFXXVb+YGQ9Mh/kTrxoQbAEj2ozCugMCAerycNBfbQYXx72FGsMCuKzs1MQ7Rx+xgRrCs6lnD0TWZ+ilLTGwoPgCh7VYxzYRQ0Pc1Px4KVRxfozdy7n+U6tLclq1tarzNxnu64yM/n6R383cPx304R4gs/DixGhdWNb46CKmnFXj7iQgIt9y8THIC779U7pTYCeOdYrL3S/cY29baRTymxTc9fLjgLWqZrEuNg8qqtTrCf+WJXLiRjXrLmGjrsfunNYPFQO40zi+TAQJTmILhAXJr0JHrYyFvzogAUnNj0ve2qEOCIKhoSs0KrPVztHEYP3me2GW2irp3tYeAbU/K59xQMELY3JglhD6UgxqsCyaQL2Op4sFOj9YFwI60XGNuLZsH4ZO5CXundlnek5eEtdAIiznjR3xWCDQskM3XT4ENRLcvFURbETKFgv4CtlrJMkJ2shXfv1WwK0rc1CA0IB2w2Fa+LB8LO3UaTMtzr0LI+dVwThIN5d3ZN3bxJEfoNQMMN1NaPooCjXn9wthrMjH5M8ec9Nt1AAqW9SO9Z4K8M/qJZ69nnXDdfiBrLlLQvF2SEtlJJPpztBwoxQg4mrUE7/EJvW20zWeLCAZ9ohV2njdYjL+apyRTgIimQP04z8jQKWQ7Nbb7YUVM91TCB3e2YkFSelXUwNjt1V8K6zO5EbohxsFHgngjG48A3xPOTHvrHepSs3z8VouRekEwqCfG75PNaArMtxfFRUQBr9kHxFRueAcDN7/4W6DvAUxdsiKlDbrtYvcPykDeV9sWxV3N9XmTk0HxX7Oj/ig8AjYZ8rmOFH7loZS7hIHVfzqR/bGn+6g2Le9UVCX8xS1r1+9PcEHBV5f+19roq7C/hmHTUjm7Uz268dHjApXQc7BrHQvxDJbvL5kFn+WfZse4IeEqOOPMICAXAsPAsavmZ9a/FxC2duzRfN7xZSqwAsSlfP3/vcDP0R9Xhbja53s80JIOLi22HtE4lgTXVGHXs1zQL7Ui7w8Fyu7yzAE3eRHh/w9EuOddefWVq6QFyM+jOLiyCYgee/ETb++Xr7M8Tzn5fq2vPN+gXIj8w9T8XHfryS2JzLqKMr03lYeGi+e5HTUUywEd5zau9BYmhi5H2YwYqbO2kyVS138Kl9+3iNqeRz+bSogkOwWcJQhcnuNg51+D+tHb+vGL77uwMU059v6vqp5xBRgOhgvH/JsbrXidfTr3h6OTllE2yEdxMnCERKfOSZTcj0BHTEmlGNORow0DCcrYtsasl8Sj7uF4IOTiunNyTdyxZxrkLOxu3k/az65CKU8OSj6s6kbstURr4Xjy0pnVJMPR8sZJ+B5GqubfKLHdp/Ygver7E++v7YFmdV4Xxye00z+chnKkOG62818XScW26JXUllY1p+NprIQJwt2S08nYCRcfxNBuFGfHFrbUfeYeIuYtuhFFzYxbA/wz7jeWb8ZsmhMtbPyezE4dXfLwL+29czuFkVe978iJ7z6XOC8v+l9IwI16bsK2ruAY3rm0Rv7m7qDhP4/L8tSJG0LveF7vqIdv+I2RkKGkNT0VW5GWk1OQyyC9GDS/RxAH5rstw9JeV7crJ3lZXuAq9btGNEmKNDpLeEhTbpT4etvZ65uZ6uJUHf0NpcW9fCEnyoDseouq5uB3C7XQjq9o5eno7H2SWUw6DT9dA/guFsWxB2WPvzIS+kL43prmMpe7NPVYtgfgiUS7cLaq04Q62sNa0wqFo18XIimHk4Gxt5OCR52x812v/W6tl+3Aur96AsMpXozuOgpKNJ0/Xlsc2qOulg4e3syiMAtWuTPvcssJPcSdZkd3B3LCuWC5tzviga4qq513vqKpWoIXkSewzhhAq753Ub3dLt3ckzOzr36Wb31FQ7O7O0ODWxmFuJmaV4B2Fa/MAFVTRNnh6WVUKCx/ZnYMMkeXLYJ8qwQ/e9tEfAGHu8JWNI3nBIcLiUS5p/c8IgSEB7Ak5uPK/Pu96wo+fd5CPU92NMwHC9xtbvY69fFGWJLhz/nr3U3HmlliNH/eIK4KcHdQI/JoV8rCJ3aM8tkX5HS7Pp0rIR0vEx0pukN9pz18gVIR+TSB/1KR9Z+1DaV7Tm+JvmLmvM8ejP/dw3oXJ0QCo1TSpZbr7hnXhsqiRLxY5t4I4IQSel50qvQGUGz704gBxzZXogSyLzMiBwhomXzMAvMhP8sqLykcXtzEZ41DTMhSBHJ7bVCGnkipxXP7+1LWfoRtwnNcRpabrXk+4S10MZrVqVL6/MMZfftfmcnUSldiKvZ6c/Q8FhaxuNOKJQWx+OWitw7cvg+MdjAnkYMng0ZENnnhD7CI6qIBjfXlSJfiZLdQIjeUzkaJYOqrFV3Y6/ddYnVikOFVNPPNv6OyWG4+hfFqiJjz6oAG80aDbQP4gKkuUfM6d/U4tJeV2iaCaqU3RCeWOK9UhemLIqOs6E584RLvEuE8ETpRPUiV/f1pDvRif+LryHuNII+r/Ya70tlWW32hpQp+GeSMedT6+UY1STI9OidKwQ1zIHGOSUi8hz2kOjtKagwCuJbXE/rSM8zuyvwoWZkvXPiENv1xF9qwAk3T/arBcWrgaRg2Bv7LnMsVvecQ/NPL0tzN29CGYe7gRzLw9LEzmsorygorLwEX3Y06R6QscwAOGexUwzOpABT9u7yF3nHwOawnENju35uPRkdAxAyeq9Pvk66UDe3yLfame268NhrMU3C7bFH/Kf2vUr/sEbtd9uESbm30veS9yQa4ijZyFOhz0hP+G/WpgT9YtcuRguNoNzWpxMcTIsKbrfse9uaHvQmS8MHNHr6FPXrf0QZJ+suU+KQubVLLnf6YfFv+UieCXaGU7eB/Xg9ekiD7k4aU1GnvQcbKYnT/qgB/5ctUKdvgmvylMX2HhS+RbbUDxPwT/Izw59D6AZpymlloZJu9E6+bdgHmFggKniCFIKaL6Mw7ZR+trWDlEkMBIrCZzFL8qzmLJSAQUYDmM57roXwRMgPbC+1FI55D3Vcd8jGAL9TNpGeDk7eQxSeXQHN1byfWCTe8oJeDnAzXGQCcHJHjLlfqy8NWRDqkzH6Zh7BEfw0SgNtkn5yj9D+Iv7904FQqYCjfDyXuEM8RdjzvMIJ8CHmJi/l/kqfOCLHdgKsWABxwf0ggA7y7T6zl8PW4Ou/yhpUD08qhoQVSnHKlPV2K/bVvIteitkbXsvgOtbmnqY++DaJAtA9nY4xsGhuwJ3PVHHI/WK9wwE2ETmnlB55YTWEnZzyCAkSCEoRBZFTDDu76WpX7CAvqsF6Au5chIdvf84rEP8X1sh6KmesDFSJRFDQiGI5nhOSjnBCoIsQzumqNSnApJN9XzOIMSB33Ek5ZCCjyIq+BDHRSmAlCwCY8OZj0CFw82rgkeVPc+UB5DnIKau7o9S6oHHUsvKbwfFMudcr6pqDe6Akh7oXWqpHPSeRdGx/zyC225pMZFwL4mJ/ffCgDSR43PhNB2lInQXcr8t1LTRvaglijogkU9gnHQ7eGwioMpBtExtCl7VVcjErxoDXxsDaaJsI0xFKYKSOGJCkDj4wJYsvGPEmc8AxNoO4r1qID8GFK/jxjDWjNrKVgfyVSJJeT2XalmDCaCkOpI8qlLIpLsxXWpZ2W1WoM9h+hVzNYSrAGTgeBDJeOA6ju42GruT7rPuKASUNEwvvari8bEfjcl+2u23V6EUtBOW1cZL3cOd32BTRZ0/AqA5D1+oQhpG76InKctVmGdlI7yBFCnKpThLUWzsy1LXYvqklW7Bf0F62HwBUX8Z4dWldYOuEj4wQKVWbXRgkkQ44MQkUF7lrOxAmWexB1hnQsB/u22m9tiffNO1n/5PxSUDyheTJSjv7kg7JL2SVKsdwf1Z6fGF6R13frRPNKCtrZ8kQDfo2JfW9YDy7Big1eWnRqB//f7Au7uz1WJHDGds/8Ybdgz21xXy/ZkeW7TeqTP1E0idZkggumFuzaQGtzqQ934/zkFVIL8zB3Y4kA6ZfVsZec16sgu8RX/YLu6qer+XK4wY/e8HYsYo95TjX9z9dwcefA3qBXFg/za9n97zeojS+0RhySYfNOkjuzlfuBjMQz136bzt3bfqzu1SAx4AJfNKqAPx02QtzgiNYkpvhJCLn/CY+hJIOzdCt8XRb2M0n60Gnngzqpped469Rq3zWt9JeomEr3QcfOMdj3DVYrezl2boyKvbukB52p+DdHP8aHyb/XozZPEsbYYo3aQHN3jev6GNDRKXrWqwo4mpn6Y/ZmrQ9Da1Y08aoDbnRvjPrTPoddElxSUeT5HHykHIrtXusg0wo/Ze/XRL2waHQdMw4iou+qDmYHsFW+0I0ixG2j4A+H//WdtMQPvn6DIDzYv1kwQo9406XH+i3KfT3uJN/1FjxOMi39WApNeTumFH66EbhMCAGWPOrX764htwar06W5PMqzJmv380+pQGKIRHQ2i41SC/FjTmwSloV4DLQHvO+9P7olksrqB40pFjf9/0j2vFjTR3Dp6PLmusHYU+qDm0fhudBk8CZe1jmT6QFwK55w4bi1vvntRZ+f8IkGa/vHq3fXBqj/H/fJJPALzyS0gLgE/j/H0L998sv9RTNDAIAwIGHHnvAgxapWX+tyYE3X2nsf8eOYJsRvwCSiXT6EkvEz0n11Mpm1EshdWq55I1fShsml01dVDqTx32mhy7EImNiK5zl2cZYdBY4IwGk/KARBnjnRkQ6RhKOSao7OdXotCKHO8SEdccpCrjSjO9Ff1oq/fSiCs7JNlh807tyAXgJhURr/a8ovlgPT+Eyys8cqSsR4isiyAh1hEHHCMU0ndBO8itMHiVErQmnUSMajkeOAdPNAmRbC7oOgzVmdRc5yWkmh16JTJOPK8mCudwVWRmc2Y/3xyh7mEl/xHgXMaVXGwxCbBmZgvJGHhN4arB/18C/78bCstSxL9VJt9mu/nkp6ux5FsplZVIPr+jX6RpNOZCGjny+SafeCsu+0tMjbgZVl4+ZEsaiGd0KrI7LRA/p0xIGcTWKQXpjC4+uEspxL6HNKQc0uOUAjlA6gpcsjt8RrdS0ALqQt+p4foox/j+c+zts6YfNUqlA3EF+UCqI89B3K6kLqIeN0rFK8AlqhmkBTJQVTpMO5lmkIeqBhmCZIjy9OPTpTlBAnADEvc+xUHdAkmD/HqKAvmDVKEEQK6GUgDZqpoAkCzMBZSBgzbVOCGWUHpQJrQcCEXdzF2AVlJI0g1pPgUQ4D/5xHjBIMBwSPgoPAgMdAuFCj/A3sBWm3Tyq83qPfcuTvduobZtXAbr4HlJWW+I6d/+XyAUV1QwhcrA6enouuo4G7pOitNI9R8LD9RizQtEMzo789HWgyVFk6JiSUFaznQinUHH2TqBg5wSQwsAuVePig7mw8NAWhn6B42C6uMgZwua0JczIvDamzszSSsgAjaw1TxJix5FSaIFBxZ8eLhsNK5sj1la27tYa4YQo4PqtM8rCAAA) format('woff2');
	unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
	font-family: 'Roboto';
	font-style: normal;
	font-weight: 300;
	src: local('Roboto Light'), local('Roboto-Light'), /*savepage-url=https://fonts.gstatic.com/s/roboto/v19/KFOlCnqEu92Fr1MmSU5fBBc4AMP6lQ.woff2*/ url(data:font/woff2;base64,d09GMgABAAAAACusAA4AAAAAVTgAACtVAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGmQbmWQcg3wGYACHbBEMCvFE2nYLg3IAATYCJAOHYAQgBYJ8ByAbB0ejoqqX7VGUSdIwUZTtyXLwlwfcYfKFC0fBIUyilMmgKbu7WpMoFWG5zsch9AsNCpjLo2tDG96NooDOwHE+ai4PUl3Qv6RSPQugFzpB/t8nALqTnvzA2+39W2ZlRZpHRSRZLStlhTPGuLOOs+84cx6RzF3oEClzpEWibVd2aWyojgV2ZfWb4AD9m2sW3G9GkbKoi8Lo3TQKQ0nxCf5Pt//uhMHujcAX0+1Uap3MIO8fnjgrHplmAl2Aol0tapPOPEFX3Ko1C/TBzPZSANjHCSwfB/jWvU+vqq54+sYZlODy0zrck85hc1t9R/k8ZRgzTOD/gAjgA+qOSYNutFMgupF0q2IKXFqyOVtvO5GqkiZnkpzep15uz+3Zchm2bJnHWod1zRTiv13awp8JQwwoCP0pZHak6bkFuVnkpGzvexIkSP3NeptnZtprgkgQkSCpFPfzP5z2OYZqgJj/JjcCoi/DqdG5hqD+/TeXgLIXgOtDYXeSMoQ6O4hgwRBRoiBixEDEi4dIkgSRIg2iWh2UHl8hEChgF2APCBAQwRBgJ1KUYS67gmACB28f7EeGg3f2I3rCwXs4BnjDQRhA7wog7n5HP29gBQIIGCggsdcVEPkwwFUhWefrfMiFxhgck2N1zAUh9+Y+YzUP5omUT0J6Z+TYzszMLfFsLvHBVeK35YbyIJTUINEWWufHTb+msXrNxn29OfRftbZPOUCq6vy3OMQ1GDgcOPE6SdQpYsSdJUWWnHPOu0ieAiXKLtOijUCfISPGzFiwYs2WvRy58hXo0atPvwGDhgwbMWrMuKeemTTluZfmLVi05JNVa9Zt2LTjq2+++wFxsWdhyOOjgIcSfi/xQ/A2nhSu5sfTe3ayzUhuf7qIj3q/o9lrrhW0oANjmEab9QrLXmXVH9mWnZaCvOZXCIq13jbTh34MYBBD2qt+5zXe4C3e4b0212vmsYBFLGEVa1jHBja1rV61jc/4ou30R1/zCOvuu/e2EoqFvaK6BNbFnibfSxR6nVK/9RIbE8G0NwKePrRPscL0LLNuZd0jvLbthfYpUa0rvhmSugzGHnyu4F7nFzjiBfaBE2tib4jll+FI9qZLHdN4gRnMHl4ADhcOw7OQh2Z/dA1oQQembIWd5kEOLrlCPgrsuWTPgwUsYkn7hst3/Dg8B81edw1a0IEp0jCHeSxgEUudo2nbx4IMa9xwjLFXwRrWsYHNw2NxdSwe5KEQxfobJHEJ03iBGcwWciDA0wcG0cYdybwzWOOwOtlrYB0b2Ow52vEeYNFbrPs9tD294kp/5RgCFO3ezMEqRNQqvxd7LyTo7+0nKdram8CsZ1k4S+9L5NWNQvuRqC4V332SuldRT0HkNhNC2x4IaxxQ84YwYH5uw+ewA7qavQztxZZMqj1N7jWattz0fe/VPln5vwEg+Ab6qzEc/nFbRVTu7aJC4mV3gK5b1fBZ8LZxYdFa09XuXK29wVra4lfWCrCP68Y/Tza/yxb65pBKeXM/08Z3Jhv/oky5lncUu9+Q3S62fHXThVea71PLnbfNnTWI3hb0tNis9fZl5d1rvVLceHq4hbbtAfQn3H0m2RObQuiilsrSXZfVVHrtwB64OrXBJx202NciJ38gLnA32wRyYK+1csNu2AN6eW/xwSbWMH7zdLeeLOu0+YtrQutsAtm+N8wTn8WDPXam42ph79yRQX2zj2+lttKyfV+TL57+4g5A9DD6HO/V0Rv/jV1uKNv3KO2rtmFzD20Ra2BGYo+9qpjlCsqT54HnbjTd+8TLtFjHbc5g8HNabmzqfeTieUbBZEu12uZ887pBj4F+ravVZoLEB/dRsDNFcFEUns7RThKDIUEOm4uUU6E6TYK0oIcR4o8wZOIoMyrMKv44G3ZOSI4XlaPQKcX3VFKPJWd9ssnUjh+soZyPQASIADmEQzoURYQKEqXcTlGEGMWSpOzOUg5SlJ8sRchRPhfpHpcoO3l6kAI9QJFyUaKclOk+6hRHg7LQdPbvWIMVmMAIhjEUzYSyMKO8rCgbG8rDlrKxo3slx7PIkUtAnnPom2ByooUKWRo4pJ94RK8hKMM94zZiHNZTzyGmzWL3suI5vfIezlz8fvOWsPpUirFqE/p7ImXzrUEgT8JNuDN8uOXwp1qDT+Aog3JaFEWSCpClKOcoygVnb4Y12Akb2U94yT7CQbhYeoEWhI4EkhtBE0wGzRSKMKecLB1shglYEVax5xdKBiVVFCWtFJHtJv5UCTfhI9yEL8OnG8EPghOoPAmKsBEOsPhLkCQJWmfoli+whNXj/q0zaYX+9xevGk1wRLBcfDN1PFhdamRig68wurKf4vX0CfsTrn/FjN5YX3fwTY10hJUDKR8DJ5I5DQ17l2s9AHX1LHe4F8ULJP4vz0ogcw93V0dQZORq93r3oDiTwexegV5+YAOEPwYwhpcPihuSieStAraoG1xOuMZPPKZJhR9Y7wIZZmTCzIQKAeXvVfvdNNBVDWUpbYLDSB01UCMMXDfUXkQYe/PC2WgN5YEgBWBCNZQF4B1aqXPZVRRF7Bo1qHcHU5NmLe4idcPBR7tNCH8B7ml33wMdOqEg+BwBRt07JGDBfNmUBXE4nzO+jPTY6roX7DHgOOBaYAVceQN9TVAB/W+B/+BOm/wEPOTVgDpnMuHhocEoDtnR4F/f2EG8BSaeAVxFBRYrsKtwpeo16DHsiRVbdhHpMM7OHM5X+WZF4XfE74zfB38QfxQvjBfBy+Ll8Wr4AHy9kLAwz//d3f/3LtgVXqkyDRr1GvHUB9shHUaOQ/fGH8AfzvYy+Ev36O+95Ej34+4+0Lvdr5tV8q/wb/Rf63elt0n3cXQbIRwdzcAciS3J9z/eR9dZ9LZGiHM4R7/Myns5f1l/o/R48qv6fbQY016YMeulWHFeee2Nt955L17Cr+3fiZJs2fbZFzuSfcUJyHjMJ4rsOhpWj1L1Gu/3Wu9w+3qnD3iXD7q3D+XjPJ+Ax/pkvijnS/BBX843NL7pWx7n277v637gh57gR37uO/kFPMkv/dGP6k+e7M/+4df+6V+e4t/5vzVbeFp1+5u1qXGy4umxuLF0E8Uz1Hwf823NT0DxLDW/o3i2mt9TPEddU/Fcdd1R8Tx13Vnxgti6d/w+AF4I1gPiDwLwYrAeIh2meJm6Hql4pboeq3hVbD0l/lQAfwfr2dI5iler62WK16rrAxSvU9d3KF6vHnGk4g3qkXsp3qhuW/E2tf+1ePs77DxWqN79Vot/ZcWfMlF2O1rZ9/2xSvEU9J8ihxOOwEJDwigOlINepSs1YXDRYjnIqPFFgDJZSljf57R7bAwJ5KAiqjBlbF53HEDqGEGHka3MhjZg2kOIbBhSYxbMZWVyW4cUjNk3Bk2F/vsVUdyCxeIx7KyzXX7SsLS34lnhVe665SM7gxNNmx1UHrIZ5+XD3DMveNXqcaqNZ4kaUw9rzlQPOF+9lFekSsEKr5Q5P2kCB8G08kaXNYiDDj9dCz7YQH+hEbMqo7bRbkPOTHq+CdzuFQ8H35fU9Q4IkGx+cIkaxSu7zhENZR4ScSvXd8xPeFSrR1RrFoc4sXmV7AOS18vHLZf5BZvz/WgtsUEb4AczZ180OF612SHlPgYX3Z8X6u3dSnfJ22VddwtjCpP74BMwH6ef6ga2yf993fSSjQEyp23LampQAkzados61omT4zFLvGIEOQLqhZgCmPwvLjCZAGQ+i9pMGngdzgwQzEZMmfitjL1XHz3SPskvS+1rRjYFZxohNo5CMsJHmf2PC2Ddqjm1eeAe5ovERqofJhCVdQMXlDpJXig/WzqegpnNfDGezwMTpkJO7IVZkuZS5bB7elWttKix5i83RashAwdsCws2cwrUlDI2mUBtU8l78DRTBz5q0r2AaWoV+rn5sSmwZwrLxOMOsvJQL7M12huK14lQcwATL4seHrzu6bIPcNNNwXTGnuwhagK0sOAyTaFXFp2OB24jab6TlZQTzkmRBhyODWDEZKxwZqy/rC156gF6LR2irrH4h2zok2t7hRGGiRtC75fELriQG51NDOPOLOj9a6Oif5bVgVfs+eLOCxAzDVsHSCu8yCII2dbkwBSzAVUuyaCR3MnzbESRITnypEVNHp7qL2UCRKjF/3baTqezvy4zj/nfBhfq5+3F4h/cv5uFebPuYHyaqXYbqCnC2ItJO5rN7nJHU62rDkd9sHX2J6ZuLlmSJzTofJyv02CBmSEzKhW6GYCZ/olxhaOXAktmqV1eL9S0bOJec3HGAdFP7S8O8ccOv7l4UZP7OZm3V5MaesDriXF2vbsl0xuu3l91BbdKY9SXI1zzoNasI3UKip0Q41uR3F78VRS0qsx4a0pnXjn/M+pmvugnXKcXfLlCl6MCTHF3lTRJO5kTK2JTe1xpvt4R5PtTAnuaJ3YAY62xFI/9LZPZ9jdM+dQmbUcoZ7O9fyVr9qKXZSoZHHlWfu7w0T8KMaYtX2mKJrvaLMWtaN2l7R3c3cbuLgV2mmOR21Qd9rAwWrGgMSKwtWOQUbUT2Yx04xI+1sGJQiyPLar/lwIuqEthtgfIJO9zI8FHmzGbFWsTMFXCGtyPPZm0TAnnOcdzCbKJsZg07BkDSHeS3ItkkSKUT4pClmrWhZnzTFqY0xyAGjg17WexJy08hnkqI6Py5Z/F57BzHLBL86yiShQL9bLDWMiMi+BqjnyOxTn3lTYHq9Qq1ZxwoPRFvQNpWm1bPJnaks+6wvKqErVF6zsYdHl/MqHRiWupIlsHXlQfNDBT96o7yeQJcsm6uZlPhl7Bx/AFz0impMyT11WuX9F7F4kDOHtPM3N8uyu+NgpcibYIkFNr9t+/XVJERgWtx5stEu+tRrpMFVwLuDHWqs+cqu5uVW+uY82+7Y1dOshHB16pmJQHNfbnsnrhBbsW9ws24Gzizg4tMCIHmMkN1NSctIejH+xWqTwH6BGkFphM6B/DPK7wQqMsOx0PZg5jjjO8zaiLbCty3Vxvm+s2c8HTuzRf6IlsIaOWfvymA7l4lhJSDUtvM/3JSIIjWqPeRJx5eJa7zfEQsqBL2w7Y0M5sJt/FLjXLVCpjkRqAJXu9qhCmUxx1tuHgGiZdNn82uyR8dlOk7htCax2Eb2AYHc4IhBEXUJpjU8adofZaf89sOxWFzAczLaPZ4LVNQo/NK2AlQbqZS4nRzXC7+gXCGsLFlIl/jejPXrGw3AKSUGTwMMHXFq5UcxGJ2mWe41jnYS0H6ypoWNd+Vmas94PwVqXmlC/+uHA01e8UMUiKMGI1pOCXeYi2yixtmrxi3ChbJ0adhG6hU0Cu5HVF1cWiYGukccoNTUloDLIlewn8NlA/pFOzatN1GMJUXWvVxbY6uLe8YyumG1Vbm2pjy1/fEJvrFYV5lYS8qvDELRV5M+xJ3cjadKLCvEo0aouNnq7AtlA3eVui76SDVT0Y3mGaYvpd+oOQYjOus0YT2XUZu7ZXjlmCsuBCmneRxQP/cMFOQRwyqDMqG1tOzptgxhAnvyfG8aCznVNreCaVjG9eRepvOq7nF3hmM1TlpvUyLXDrcdkXBXgucU+GtFJlbhycFWNf17I+ghSEHAhWAJfXPwNd1SMbBw1sU9H+wFOv3eI6Ywgg9ZKAUiBTnsB6Y6GIQYwg1of3MWTLd98JLvwSoxue7lttf5fV6Mzt6N/0r646G7jJtkhvwDpubaqeO2SLWzS3GDI6LukvNJDCWDy/VNINbFlYpGabqrM5pE5F5jgg1fqM14oZEGDJM1zDuBEKfLZqcnYRVsv2YJK562iX9qrQKya19Znoci0dhXGUXO8ePgfT23/pq4hseR+BlggwrpvhJbvrxKL7UqY021SVBLkQaEC38sGKLxEgILNlXp5VuO3Qxo2KAZ4T5XQpNOzueS5doBopXOvejxNsY9dmnmxCHeMaxtnrwqJAsLJGrIz3hf/jKds7Jr27ZQX9zia9rKaGsEPvdHtOzJRUntRTg4BlDqKJNoBl4Y5WD9rioC6WoRHbRMQWDYduUMDlUChrlYdwN+GxsNAEoRP2nQvgvWiAGnRcSyHFEL0wbOfJE/Kc10q7tjWjdgEHMb0S5yIpSC7pZYcfd7pt1V0zXV5/V8A3qAJcNHSlpKe2Ks9rtPYXVIyM394zThq/s6/3wwi1qLw9jVF8I5R2Pd4uqJisd8OtcmK0eU83sWuU8/XCEK2wlVnBaM4B3PPc6NJbP0tvJWpGFt8KpVfUJDvgbElUc1MXmoOWrSvNxNiN6iI6cCa6tO57eW2y5vmsRHuMnVsy69hJy4EUYGbiGuj844P2WYkAv59XNAiqHjRz06s0zGttyku5XRqaUZVcQk9/ZpX5gPIz4tbwh4yw63q6j839n+48b/YI7SwMrOu2vkVQ1bg5825yoORmTEsxmk8iJL4q2vmfoZe2nqeblZN/GOw96c8lZYURvUmCFJ1XlPwZE7rWYYMrklEtxB/KcoPP7KKqRJyNSoRwnRkj8k1AFN/MmdOGkbq1A0f16pP28ctP7CNrJe3VqDGMYP8bTSkA/qyPOp7HXHRC0L5uLAGq4XgXgt+7kybK01IPJU0UIXCxw6djIXih2ad5Mhii92nIWgWjogsCw57CPv33xPeQoXm81+CpxhuDCUgO9RLBnSHDZT97uXnlN7PLF1HiWNlFhdezi1LoU1Ts7/LdqvKKX8jO0V+1LGWllQj8hDYaYPEpmT5b9/hAMMPmyzk6w/6+RXJZKSPrbsdNKt17bMIvZnTuWSsp/HPJ3ezxfyAoKxjqmnfNINw4tajoRmpzZQLN1/fBGJE+vDjT6R7L9D+dcDKlPbJjZ3EietY16EEoLaWvMbegvjgn80FeGw3OR00vEhd9aD4NauLH/8aIHX99UEXOGl5+0hiapfjKrAXoFw192s1XFv896V6b4ur257pRk5teWDx0r7RpLN80TFxdCeRaO9z3Yk/L7ONSMLpv5d+7wPqC4b44FB7xDCu5TUtwrtdAvyL6DcbjXUnd/VuFqfVv6+rrn4BKlafIsmeYSJW2IqkLlOUu+cY0e4qFief0hHf8e/Vs6u/LztDrfeHinmIRzb6tH6cjV2ewxz0T+xole27z368sKeioQDmrh9VXxyfWlsZE1hYkJdwqhAtXyy/bdpEooV0RrvPxGd5rw9HRr3dkDpKT9VMs+cFVHIPed8ysn9bNEPVVf9f+tYjRutRY0/gOnjDunQ6XbaI7yjaFhIOSv9JVj6tYWQnkZLKGJelyz0Icy8NPDwVqFhKokD7Uf9HgqINyWCyDSstMRAKoSMXLwdr64dF6smNnYpiPXWu8rnCjqEhyfwuIiNaDVzmCyXT8XR/v5xhM5LX27OtOTfrMPbG3POh/N8NL11+coCJzic1fL8KcnrvN+ojnhmWu/ZTR5ECUwI/CcMVHr22aTjcf1Y97KOP223WuuZrjIMf+N/URq992HbO6PDXDXVyzJGvfN/8vgb4qZU0pZ+qcB8Wy3OzsnCjhQGZgfb1ESjkh13hUBaEdsMzG1c+9nb4h1R7Hww/l90X0/Zx959lh4lvcXpqewCwsDCEZ5KFqk+NSG2ObZEHn8/CBZf6dj+U/yxJOQXdXf8Lp/+XVq3v/7n8XesaPOLwqim3+u3R6oG+gnb8d5zWb03KovPhsf/xA0ZmjFQ+4z2m+51vZL4WJ/MN95j2AUv+DOqN9bsmAZca5s5+sA6Xy2ppaOmCZ8e4c52tAV6rhshUXse42W8X109DXflbaDjXPvSQ4Sj9Y8riyZmJDiLW+2uf/ZEpSia+0Fl6BluCGgq3QByoPaJXSGSqMUNC7tJsg28oM3f1h89vY88W3I/TiquKclMZ7SBHs/ibG2B7tM7dkBXeBY4IY76mbIOv6xE0HsMxQ2N/KFMrGCrViK6cbD2FLElrG2AsN7lZN+NsrwFe6nMm7d6HvPF1yLu/fh4Bef9JY8lmkom73Evn8zZFUGaTSAMpSRFH9IftYNsSePNwW58xFb0k+fvZJnANkDAzhHvHtRdyHh4/+vtv2dQ1s6ajLzXnQfBsJA73opKeZcgl6rCsmOqfAkN+0GlWRJgGDFfDxNbeQilTp0RCoeToubQ5+uiaWm7GtsRPtpf/yE3b+YWrFdNIpT489MVJ+I8EFHXdS8wfqmqzhMkIKrksdCHfVznKvr+F5+uL6EiVshB6a/fBufc3QDRqlKSXBY8yPWOJVz/NkPh+8Yoci6fnLTT23HmWDlrrmDmA16fIh/eLuANrnmAuQGfHyY0DOCt9vzNu1kJftz8bdnj5sewXeLeGMw5V/E4SWmUHlVR0Pg4kS8+dneAelKbyx0udenl3kChANFzBlBvFV+p9IP56+/4R/kIA6MxxEZa/luWbNP9qev1FDr6Nnfp5feJQFTtmHpg4y8IupJ+gn8FOpmbDpGuN2wPY8ZlXD/Z0FHH1xcBJw65MkbWBp1wY0lhkogjsTCICL1DEyOqSpkN46PmjKs3gxQPyFldiLALiAJ/rS73045PLRa14gAzR5MM7Khv0WXoW9DXHFuHtrL+kBjNiksIoIKypEbXXeaX16F1UGRXhdzh+mtE5/cYfNF83IWDjFOp4KKjI8/96f6lrwr8Fk+QAOqM6+zLnedyewVNSLqo85/iU+p8gfLR8u3aj+BKcNwmpTcRR/jJNveac1efqn6O8TpZRvT+NSm1emWOr9WcKrCyLKmz9NHX/S/AE8Rf7Oife99qu3pwIOfDDpfhXvO5YkgIZ7UrWZ89am/6/N5OucmnVQ5k1+l0D/knO6kPZrhhE+tML5SRUj0D75tdQhU12BoH0xppub929C+xBvddIeTEHNAHyZbXFqiamubcpRK2V1PqTg7BXiG49yQs2Cp8j/uVN9b/3uYAt8Tjz7tDPqO+Eib3xVM7zgzs0aJqow0a/kTefi6d2Qc7OC9dAPMdciMmXeF27ZGHmHe+5ejXUDMDBMHYEMWloc5VbyqcBlSVaggjBNnpZ6sfN55tQeEbqA+7l8x830otTzwohTp2Tzo/sKSaLdal2iIg1qjVCIT4wpogEOaDM+wUU3c8ZHvvimg/Cdr1rguiGPvqPg2OLoDWrPLrn0kQNtcPRJ2hhkhKSkRCJN5WwBE7RJSI5MjPctixQJX6HJA9eSPLpd0bnVzVvMuysPlhKFtF9vklNnOSR/CxbRfr9OSJvhvFqYyto/+rOiXHY1IRBq/ajKx7N07PdDUBG2/qPV+S6w4BQVsK8o2NyA+nf9b05iqegndVu53kXuYi4s4jYljl45tdumLRx47VuNzfRJldBEAvNmuk3578Rk7ebNGfsHPBIOxW7uOXWbIIZ+bnbaJz8UH/SBKh+I3LbrmZSZAJkrvJYMz2OKxXoEX8PI6eqa1lfNGTT74rDgIIabiWGQhbxWobFOoEnSm4rmrtW2VBrOrTwgPizPC/YT9ffPOc09s1IxJxy9yoZxnDOkUxgpkyNOI/vhnEqSZ+FGQ8soMthLbTr3G6P2Yh9NPgDWK/mysumCavKOgi7ydCj+yW0j4Milqkji4j13yJffbc+icijvXXkSt7PV+ENuXksSb6U8Hbaij7cxvHnCRdJkx2TH0sJEvHnS4Mtt3gFG1OGlL1SLwQen94DTtAXZ7KS73HXBWOkYWHXpIRpbiKHpoa68lhCaVSNdN9/xluWD2wfsMrTVzNfKCOrt6F6a/n7v2j3C5yX08S8GwL+HCLhuIrCqfjFAH5/716vaq/pnWnFHR1Dvlkz10oP32HeeYZa3HXXzNdKg5/Q3u7on6lro+dZS81Mylk6BPp6QXdYZrx2rZG14Vdle5oKFa5gXNDRRIEAwlDWbKNxFeBSkX0xqBvTaZFZff5bkiYGsfUu05Xb+qSOfTrybFL7P/4b2On/fyDNsQeL+Yc2KZeVDGq5YfnJbdnOSELTl1UIMWfd3urWzCo2+Wb/fNjnZRE/IzooKy2IWsApZuIEGM05Xc8TAd6EyJ7/IxLS4lF7zejH2Vs0tE7PqusqU+sq0unpT81uNUXVzM8G15RWmZirR9OTIrfnkyKj3KRGbySnRm28T4H1C9CbsfUV+nszYvGRJmiBZkNToak4WThNOlpfSNlOgeA+ahh6lfaeB+uc7LiHJZLzVWYcU78o/PS+9ei+oXozznaAihY8bm0o7kn111WxLdB3tG0gWDKJLQKo73lTSOoVchvRPz/2cf0TL7Gkrq7qf6K+vZJujUywxINNJMk/L3fTkDPS3NNawL9UZv2B9iMIZ42fmFBdYfwFYswM+dRY8InS1Hrv0gwQaPPa5FWUSbVOtVo33notXleY6z816RJSlWceHlZM9yO5BsnMhzHLZky21NUJN1TItzVVSwsya2ydbS86aNIw/sNQejtQafmA5PtRirv10eEj7cas5JPauGpl5JgTGBLh4VweXpyiZ6tsaWpE0rnlgSVXR8aHXGfEKNoJmugbn/BTWaqNWMsNBPiqadviuhwQQZ8GrtJWR1ubHyeT/JybDLdWywky948xKuWZm2Vnhltu1wswqqcfblk6aqKyikLC41MuaghfIuSyq8VykqUxLbS2rDqtWQzdjdVOza+5II5xVv+zp6AmiLuNuaWkuxHTG4+uZw4zMVJKqhXmsqbmquqnl1SuWliDCq5RASmijtBWTitso/Cd2VImqkJ+roHP5kruX1mUDgr65rYWWO7ko7W8anUQMDCbNP20DrrvGZgaInaaisqmWIcFeS97doZBOjbxOJV3UsVUXKBPLIhXRo6PKw109lN0MTMxtdK4yHmsBbxtcXxFwB+4b8qF3w9qTYvP8DvujaPJeuDzHhul2s1A2KmBfQR0r+eZb/8DnKdNs/5fmAMuE8onjPQbvNd4ZdMP3nkq+mzZFnok4XUsflQvyZqctWA6Hw5Ncj5wP6laqHnuBNYCcx+zOzGJ2F9vl3enKzGrqLPHE+PnEuTn7xdDs/f1jR/0SAkGkEQmRDuzo8DJwi4mKS4mkpqUgATSk6tng7ebxkQaK+xgj3kQDR0bUEvR86WH03QZprliT5djMC/uZDqQ2frwuSUvQtttU+voelKwS2DQTyWa2evqmtmc66erV6OkRPV08u7ebfBDr4FLiljjbes8kq9va9dq5rBSPWtcb63EtcOCySjE2OFc0ZjUoumecu8kLGbgp/TSRnbcexnl93V9anm9MKfX0KaJPJ/3+ztwk2kiNBlgm9YafWlHPrQNtt26iYR/NdbR79lZed22y/EF7jcgDhlIXjVy70ywS9TOnWipNWs9GCLkQ8mRVQ7IinJTMfdrirON1HcLem9iGBVn/QEuDPhdfyjBZwUHBcdEzpe5+bUbRm+YJ1F6L2PCs8OiqKPcgzoyPLW1hTA9RdwXnTp+AwbltZNbj25GxnoX6+8mBkmYZMSoZLtYPrJ9IQNLtYFY3tGM05eLx3NiWAZ4a+rH80maQaRXY+IXN8S6h/B9LDY+eCqEvJC5v7hlxqSfIXxX7XVMzlyxz7VFEovujqyKkz5rVKX00mWto5/0HRIwa7Cllzc/uu1o2F7sQ0xp/TsHVhdiVgBVlTW8Rvy/Me2vQBRa19mXh53xWt8TO+aGtpuunLKHC4D+vE1LJ1qf0Su2cxp01vDZyqd4aXWqKB4wPaotbl439fviC3fVK6+YBWX5VIbOy0Z8ToOLXhmNdb1elAUspzSag7s3olVodjk3MssjJp6+Ap8nTwCcDCJ18IYU7WQmcSJ18fRz3t+zCuBZezc+Rl49bndG+a1IH4vz3fAncE9fzngXmzi3Oc4KtRBHtz6uEZMbrZKVYhbDp6+FDyz9fO/w9cn/oS/6kX5CsYWzS18S36ZfoilGvktJmWMeGp2ho6Ft56eC3JwIDwz9Uhn481pofjr43xlMceTSzph0k7n5lp9S0Vbd57dn4TwAWU38DHy8DfV+Z9vKshhc1QU/myhUZaVUV2XPqV85KX1UFqdZHDj4BRoQMr0tnNy5aomzIVDMpVaH7fWKSKlcuwt4VHk0Zy0BUQiTVx0a7DlhNdDRkLWmo2Jgg4jOgrGjIGnmzBEEelfhUlbYqWISgkiqo0S9tI2kU82BUcgXtoYLrW/pEZ2MjogNCOGVItDc2IToawgdx3hh3kRBekRB34GxhikSQ5XFXxJsECTStf3kkrdegLI9sBffb2c/DgUQhNg//I36/Dp9EYQ/1WvZW9lr17j1y+pdz7FE2MkXYa/jF+lKFVUVlBVzefyGnRC6nukZL0LqkoaTBSvDa/6mS51rPS9QTQDm6UZWkCkuzd53uUho6um/Hp8nzkoLlwn8gXv+c/s2qWXU12UBG1hEnV0r36tCG2g9GMlTcrs8P/akI2Flqpxt6BLxsqFzoD7Tnf6f/mdzso9kk8NPwqv4dWn67IS9sBg2w7H20ExqiGK7EGynsPDl8GgaeqGx/ln95QUvEOl47svpmdkZJMhL+kCfr9KUIOXsbBSsVSWvCmNX4rXHtFgwGA+LFZ1tSUOQbnubUjpPUa7NqY2Oya5DSjPLMO/EJ8vN4pRtHgEWIBfP7DX/LMIs7v4D9fk3478V5ngiLiA73jnT0lVQBShIiFDkPEalSCY+tqUcWr5y0C72eGBiSwUjwzwvBObvh1Oyvqt9p0bW3tTI+1gxCO0cslQT2a1vFBCQzqAHZeaFxJRXJKpNvloZRcR6X3OQlpexIXsY69iR9Q1s7yJzxnqF4dSxbmkKoupKm9Yi6oImTmmsRNSauiOqqZuyscWBU00rJJSMwhH49yPjJZVe9cqKuihrpWqWLDl+dvLXONR1TbQVFSw0tbXMt2G+uYREVmJYZRCsuCUnIrEKua0xOTo/DfjDl7RtRpCpUbqWoL+vh6G6o6+BdaucCL957I7XmUf+uOBaeKae1b61HR0+SAzgWzx8ANnh6exQQdSzncLhoaMnuG0FJqPXGSEv3gqA0DMFD0NDfMvcEZcGPNVNm9iVnxSrXYUXXXvUDqYlqjTnkQB/uOJt5NFR9GwifVwmuVNVB4E53UglAcwK0HH8BzV1gdq7fgkv7Ssf5cccGLeMC15ggX4pGvERmyk1ys9wi361q3T9xuP1jnfRcoBb8oSAmBkZPIsPjkRkmQm1DD42WHrqhQkS14FjoH0RsgZpd5AoSA6M2keGe7EHOJKp1dWIgvURG28kerkzcmsB7sntSzdCUZcPblMiwOzLDvzDwPDTeJbescigQR/gDEhm+yx6ekoGWxTlXnfY3A2T1cCQlMqSPzJCGLeSh4ZnTvbCyJjGXteg4kLXC/pTIEC17eCnI0vuKeRaBR9144tvJRrKHa4T9JWH1jCOQJ7JxmEYJDAlJhePsnofGs3Qjz2TLcAgSGZI8D9GzNTQo40YWHtcD1IIDI7jmXN3A4WbLPrCcsgL6P310xqP7Z0jUBYrb7RDyzuhuDJVRxF/5UB3MOjzMB89GLngFgir/Zt5Xko2VNPiEV2y2PVTO5ZgyCw+10kmOBbypf5a/QgqZl22zkXmmZfJsn5FLG5DtXOAQ5MxWyhlZ0Nc5fDudvJ6Rn5V0GiRXtjoK6scbhUOWlgVkiPIVs1WrnjmonZ1dsrkC+UIhzh4Qt4BVyTpIsf5vnbBT+gOS69+rv20EhPX6rW80SVaSj1NO5ROr2CjI0pUADgD1rs05LpzLRhbAgPldfuMNIeo8u63amCg9VrWT6QUwYH6PGuoFECfPr0pK+mOsp3ZlO1e7E+T+875CMD43X9vO5fALYMD8Lr8ePCT3eDa+QPJwJHcafqszmBzJWaPWVQvBs0sh3qNdUDrB/eQHkEftpTEwKGy/T/HvrII7AOBjf+KWBODrYfTT3/h/02wHd/AHdoQCCLC5x5YTYMczV8p/dTZZvzvav0YqF5PjL+NT+QV5SZBeNUyLwn9+4lXryGGDmmlSs4DQIDpSsXQikWJqk45Pro8zjr7ZNNAMdzhBFbWIhi48oojRKCLSKZZA6r2X7DopdJQz2ejxrV04rD5onzJippL2SsOOnA+juT7rIlLIVLnSnJxZutC4rCs2G/MewmqDP7TBk09rdbGoEXLzqvLacmwjQamWXahxB2qMVaypGKKk6qGCcVO6IwCSV3MtLS5FUVDH2JUfcgmzymqXy2e6aReF8VB2THXu1o5Rb20E3Czi7KKzsgdByNk0F+5BzEwCm85zx0siC6vLY4nUeATv8eZZSS0GNQds82GuTw91almjVibbOcar8A5Ep5KaQRq+khYiz5GsaFShl63QxSNLHpEDJ72UmOQSQu+ZZqoIrQDl4xHj+KEivnBi98NHvoRRyn9O8F84uSznBY16Fhst3CXFNsenrtu9YspZBplWjexKda0CWFa4khhVuwlG3riwGz5Kky5w0HkUH4jXSQKR8wwli4SwIHOqzpTDkVi6cE53amk4Rd2UOYVUpyMy99Zx3KScSxSKwri2eVaGqiKQKmuXl5IjErDD6WxyEaEQDnNoQgFkOEAZ+ppOLjGHAshwgDL0HZ2qs+rmpjWhypUdWQV5/VLEYe9BGo/1//oP1Wsfta4qZ7Xj1SpHrGqsZuvRfjHaC4qSWE81dFasXdQiymX1YmCvV/Y6Zs+gGwjwL+n4WcKK95XdQELdDw4C7IAEDakdgVuB23DEAXbDUfYKCqPnYQyGY4mItXHIt3q7l4l+qih8BPPjzpWbAHgypEg3DU8TRYeSEQ+vxZszSftKALnByCz8RZvoAZGfoJtxIRkyFE4dAdPxCCZuKHQViMwRmGl+/LkL7xiQXCDrAoVQG17wHO0MHwsD+JB39hJqIMmRD7bzKp5IEoUf14DMnXMQ3nFtf6IItKhSp8fYxJkIntR3jOukPbjoAAA=) format('woff2');
	unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* cyrillic-ext */
@font-face {
	font-family: 'Roboto';
	font-style: normal;
	font-weight: 400;
	src: local('Roboto'), local('Roboto-Regular'), /*savepage-url=https://fonts.gstatic.com/s/roboto/v19/KFOmCnqEu92Fr1Mu72xKKTU1Kvnz.woff2*/ url(data:font/woff2;base64,d09GMgABAAAAAClQAA4AAAAAUFQAACj5AAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGoFOG5JCHDYGYACHDBEMCvMI3BYLg1oAATYCJAOHMAQgBYJ0ByAbEUIF3Bhn2DgAaMTbJopSQRoTReUkHfxfL3BDhr4a6BMdhQ5bURRFIw9CmVRvsQzFG8/dOraGemlpI1p6piOIEvuD7mtXcAwHPwVBE8DPHKGxT3IJIrv4r7nt2XsgMiKUIkAhoG/E72eAUjJU6sMe8HP7extjZAkKrU0IH3UWWIjSSsbIHNEiIOWIjZykhCCD3hDYBERCkA5BPyhmEFWLWDk7VT13jsBpNBYhUXgFViAk4t/eNMC/8MCHON1+mkHiVMSP3Pp3FpaTmV328SOipRWVt1mWkyd+wjc5mAYSh6i1KcqkKFOJwRETitpdE9ALUorZ79Wahtve1ygUBlAjLEwiW6OaCGFbXrjcheD/bKbtjHb0TpvVvXcTlisr75mo6EJF56LKS9PM/F3tama0km5lEJkkk84USSZYyai7YBfuAUh3gQsBV0yvT+uuTFsjFXUdxMFpsyIPQMquy7RGIq0R3DC3r/s2f5N8M3733V8iQYKIpBIeR9q9jM1HBEg4MJENx+MvmwSFB4AXoEg4Aweh5SaELl0IQ7cgjJlAs2QF4cARAg8P4c4DwlcARLAQiDAREDFiIOIlQqRKgyAiQmTJgiAjQ+QpgChSBPHAA4gKlRA1HkNQrUCsW4d4ZQvitR0IBAroAvSDgjBmBiQgAZ0ou4Bl/ned2yC+M9TfE8T3+uM9QPyQfaA3iGMBrL5VDv7y9wY2FcVi0FPDUXthQLPn+icZWVzp9Hq6MpfBevrE3Hg+cszwOR/8CBQ+x3fFbvOgmnELAhH8ERyBCp1S/fxRfwQesRnCGkjZWRdou8k24ooTZ67TQP4CxEtgGSSWEuXvwcpqq49aQ4011VxLrbXVHUO/54aNGPXSmHETJs1btmLTK9uwcI0ytMCNgUxRmd7L9lEhHUKEt1gRbAr1SrhNRBuyrCuwiYObBxEYyoRjgyIsSfQRUZD3DCqg73OpscL1gaFP3HyQ6ZNsbxSawM7NjFATwk2L0CfRNmLcsuJSaFqRjxAFVmHges/QB252ZPqi0C4UN7tMO3a6Rtha4hISPvq+hnojwrx0neVaURA/hK4PBOCKZiU3Q/NRHFuJtNAUx8WVCCz/Ew/M0yu2pPPwrsyy1ZUr3GfxKkT0CLq5d7AcbATBtnAriMZkkfl1xWrMFPPjc7gWC2Me7I61MRe6qO8HxNB3D6tgW8yBMVj5hqdwwNWYrnGGnnPzDNGJzSAeFsPAQ/dgD2o6JLjPd6IEXFlct41oDz9oCOPUXom01E6pvGqq/5FfA7uT0ZeMz3t0pOhKQW9sgaoNlOHv2uGD9onnvasje0tO2BahvsP6oaa+9EwefKT5tPm2o9D6Ie84s9dvtdabXuXeZ5W0smy3DQ83PaXa7EY4F3dvpuR0VIpsliU33CBDe3z4gCZDRTFuhGVqe2qptRwd/Hj6Ghuk743x3F2yba+2Xq8xjojJPuXGa9GH9Yt2Yqk6PZPJ3Dr7RZ33DSDqPg+nGlypk6q54QuO89eHBdzLxMtIrsKduTflS74CcQ7WU9lgHRSmM+M4s5IP27NP6uHTcdX1eDWfN/Q66qxrRbvrTxUUVWcVFXSBomhT3w16wk06ZEv7HKjmSA0nipwpcqXIn2oB1EikiBTKL5VyS6O8HlIBlZRdLeVRR/k8ppzqKbsGyq2R8mqmKC2UVStFaaOs6BSjQ7GNm2PTDcqNQXn1U8RzijZM0UYo1igV8ZKyG6Mixim7CSpikrKbp1yWqbAVym+dstikQl5RYduKY2bzKsMb5XUyshVrWPDAfr6nENq07FWAWZ1wNrQObvFoaYAM2rQ7voarWV1wdzVoB4ujJ8iWgq1Bxcng33Iee5rFDuuykLHeWsVNFpbE53w3FBag1tJIYW+tIaIFBJvDmkbHPEntm5Qmr8l3yPtDvNG4uu7DgPxYVUEaTucWn3J+5wk043ZSFv3Q1wKN2FVw4C/AnpP/gDTkaUDMKZU06VjdIV6BIo4fybQNS5oPjCKNBSp2xCehuZuMPEh16kPPSGayli9Fco8KXsS2OMY3YUlKZopTk8Z0ZDSzWc/XonLPi8YsaIwDGs9BgwEam6CxBhppQCILjAGmuQRsAHYAV4GHgGeA2XA2latwfanA2qivVsWcxffp+g3WbNh+xy3YsV85N1TjESd4zlxse71mCvMWVs2G68JFWLW2dB5apCjRYsSKE++ORUsS3JWo05MVVM/0VJJoyXnJDMzCvRV2Q2/gf0iHOdgBCjwArgkFj8sTUXWny37BGQAWh4bNYXGMT85ZvvyU8xfkumAhHiIgqBYqVJUwd+lIksJcqlQb0mSwlinLK9nI7OTKUyNfEScUD2wrVs1VjUdqP6cO5j1W/zSNQOevQ4d1T3QK16XbKoanqHoMijRk2KIXRiQYNaPJ+BKzNQSJYZa5EHSGWeYlWKOFfcDFzwDYol+l6MAb+kuqk/4N4h/gp0n/BfEf8PWk/4NoATdR+yqiwm8guw25K5xTyLd6rqEeIXeHc8+QP/TcC/XeyT0FgcIGVVW6A/wJSN8AX4Fuc0Hf2yDOg9ovyxn6U8iNzWkoi/j4FrRgszdbtdmqmysYjRGb24xVnI2iGhhtiVsVW9jAwW7z/uxXaFTqrcZuftDN2n13q6umfhm7g+Zz37FDMfSJWoNji5p62XmVIXutPRH4/1shpGgAAERJW5WdkjlWNIhsl8YbjeF+8laPPdO+6n8dJf9J1EEDM0xw0b6Qfz8vxjQnspDfhB60H7HHqkowEcJYYey00b1gciZxWZ6mzllblKddVeKLnLtgFvO84RAFNrUi85Ye9HFM4zJ17dyX5eUOAC31VcOrhmF4zTXMwbJZ8Xw5SDfNUqTh2HUsCArhcIRMGV7W29yM8PWFL8r8P9XzGNeGVQQoulR7wqE0sMERWM8wBqFEJmkC0sjEmaGqYGeF+VZ9fS7iP1hNfnMGq8a/gnshjZpFOasy1M+9uIpTmi9wOcd0SVdSWkkMcoo+dRIqE+WUSJSGlQCW3lGtsVdgab/2RODrpULPZHLUSBdANyWtKZb+dKfMVGOdMtS7dhJxoVRzdRk/1UlETdeDsNVKZuOm3TCwNWJt3BVvNGsYGOy2PaodlfIcoLBq6Uy4i1469WWXySg046pfwmNOPHFcc5ITl//1b/FT/R08qOVQGN6YNPyNRmDHBsItk+vt7HWSqnc4crAwnSWg8yk7u54TWIJCGTgPLdE4JIZZ4yoTwvgwG6h7j3hJuGEN8denBDnfnukQJzTCHI/pKHNbOcyQI+5cBCQLaayH/nrh1jVbjft0gWlVKeY00zOFCsAZNqfn1gq4viNFQYBNp9e5WLeorTw/2zU4xPfJq2OUGGCbf6gFBSQppXmG3NKVMRRiXwtGWJnbtS+8BkGdh1l8OMxhGjAPEW7+EhvbgB6v75KdK0yrcKjZSeKadzuAeVXQsNI5AXWYrbO2A0AxsiGTlLodPTcTqc1JKEyZWr2A3TJoExDG1+NY9XBoGD308bACIx+RTx7N9rXgQ0DhhsNe8DaYMXYNsL/JkHoz9HBohFczalQV8dQwdXpGvjvvreWr0FfLvPYJytC+ElHkVAsWGUqnewuqYdCTDKyx19qHdRMl9Np4UGiz6qg4pAFk3wOPpaCiFQSCOeUJy9TCgJWna7XPi1VDFHUyXc8hZKgb8jH0mGcNhFuX0EUicoi03RevJatBlWXHcya5prJq3iGnHS88rj3WE2dT/cidvM5PlSfC/MeY6XRm6pQLNckl8yo7ax0+LDG4EQLPX2nnRMurLtIsJJFbmBxHh62/C1DkEK/3yWunh4SUpsr4m1LIF486+57rrWKahUa0mx3Yl9EabCCAikrofm/2znUQm1KB2za2ZiTUBX/6jXauCkY4EdSLna/HNd2l+XvbZN6cirvjy8vCaD2/qkMzNto5dOuM76jNwlbf8CW1VK3tQpxwW/tHbfVjV5jTLE52JHtsP1y8j5vflPj5ma1kz2IiXlgmlIHaV54rkwMUTeoQca2uWxA7ZZYyB5epWE7iXNd3oAjQGd9OwrWCDNH1g9IJKgG5KfrkPWD1kuEUaCL+9atYAqQi9igcdptJTT5tMODFFkGfeADJnHb4B+KrrYuZyyQXUawARezY4iL7UYT0g3NDis3FdM6BU4vjKgPSAS7s2f0RTveTvetuGuXXzMElxsWQRwA+270jiyeovsEuFwSxzV3iCPLSjaK//A8cVdJ52jHmQ+tcpOWscqOulsPdfreeyt1rRjjNr53S6Nx1e7C/unpf7PF1B/Hu6vpdd7B97yQZZ/eM7YSVxBHMwwWBXvWkEtlERj2gdU2YveR9S2qmhKdeRZGo4vQtXWNlodbm7sADJXHXCT16Y+49H9HVPvSkTG6e8ggRplNcR8zSo3XIPYoISExKYruesjx+aZE1OJmnGUvmlTBJK3TxEveIftqm8hVMEIHNONGKqfYNqB6G98GwYLz1ruLjfm+W/WSTibJAmjL5LVBkHAiAo6mLALEGUq4tLm98i2ULVnHNzHAPQPFwjQ7rHEDRmz4uBsEDG3osK6FboF9QA8OU0Qj9ExWtBezpPvWed2/wPyfUDot6AY2nEL1whFRkRrJ1dlF0GTHJDhuhc2SG+9ZILuJvBXWSgQ2AdYzfSG6qIGM2ZgofBZ2T+RdKt9MqV+hiO4cmXeLUlVa7FnqLM7pKZqUyi8nCNX8Zi9A9kpNpYvA2/8XZ9hJaSvBkjvYA2YtEljoKXLr7ulCY1nAoovjQosNKnOS+eRkLu2s5oCysJtRcXgSqIjlpnuemiW65nqTzpCOKW2gE9wX5MGXnBcZaHjRKUz1puSQaBisvtLjViKWNHUHhU1hXIgvIgzYFfMZLEf9p1c7UM81L5pa3G2QVVs0HdFogCblun0Cz+3khT1DUcZbxpyhQJd/5J0Awgaq+Pf7umIFVTqYinHT8WTXaR6q1YM7DnYaRNnSLZiUvmjQCKyb9b40U86JepN8gLU8lO82fikCuLTpjD3EmiUpYuPgNECsUEPgkKYDu2Wj5HQhvQJEmGxvVx4rZGHp9EngLTbVv6Wm8lJL5qyuIE0OT5Y8woANoeubKRV0WjMBRE0N83IM25RbLVK4oiUER0Kja0xeUh34WpgxQsl2wCOg5YDJR5+X6kmZ0uEDGDzEu7EaLnjkxHn3XiFnle+uYxlM6pB4bh3sBQ+1F460Ag+WPuxq8xxmyUYJGrJMQazQkyLhqWceh71W2EPEOA3BuLMIhKfyZbB+1SnaK2te4b+9bFejGjohvSf58h1L3U08duq/KXl7/WhQlsskUq3tYQGp/yfsr7W2faGNxzX2JJriAKglhWSoqAgw1izIxGQIGMcAsWS5hAIbK8F/2Z0LIo2oQF6Ye6C1/yh+P+z/F73hgsyDrwGSugN0pwTcFGGRWT2dcwFZSa9mrMk8jL9zGiZocNbX1HFUZGlNdCHpUqKP1pRDqi8evcUkafYgH9Ru0gwQmASPQDRiBCYozHL4cZ3VifWhsemkTf8rl1MbM0vDYGjhNhs/PSzkSpRhS5pfqIhz+x24Yk9BUPNycUg30N83G9gH+fo4Jjlp0Q39Sr2lwxwQdXfYxR8GU73OqaYG5rNYy8Xa/aYA9nI9zzArBmtezWAVV9jp6L7EcBClKwJfZxOzW93PYunqsa/pdt7T8QYZcR94A2Cj82FHs7vKOVaMBK9SwBXgnPnuyq/q9nbWdPHqXalD0wSi3Nok8BjqCi4LSH2VLwn4vpsdOvBFG6y6hHzT23nNI1D+p6Wua+FIgYNCXXMqfKcmDTU3MB6FbbWGkPlNU3BBtGAJdfXwsEm0VdNE0YAUaWBx8EPFnOTljiU/lLzeF8HM1JXORT+U3ja354VxqWsVif2PVYmpy5Rx8fbx3b9ZexFN2+kf2j+yZ+XmwF5mGtzy8PceeSaitOXEvyl7hIgnauN6UcTrsJSavOAisxpYnPD0jLMI9PeqOnvDyjFz/0oKTr0SKM4ohyBcCeOzUhu3WT2GobblNgtN65OvCAkxjfsJzyUXQWxXCJDZpGK9Zp1V21JKL1uuWVow9vMJc3VK8DNsEMj40dwTUG6kYXdCrM3Eb34Y3AbOs9XVdOdmEm+8uJhYRLvqSrhdpC7/dDERSvG7puln9h1YLyK7huycudSchE4zpODpIfdZf5D72Qz4n/NtMaii8fP3nTeAL1sri/iR6mMf1iJQEvo0hB1KZUBK3eGRSDpxImAktz+SqyCKQr93U076mp6sFClP/RrkUqz0V++YUSSC1uVmtwr72l530hX0ELmMEukAj5u82ajSoOHAWiNRN13Az+R5Efd7NpPC4M923RPiOoHbFGPxQWj6dLVbOBHZB/viDnaLNyBx9VbTnZyccUNLNtA/2y/cypBX8bX82/I2aRR/WNJWzD9XO3qoYpH9vTKddS3fy8L1rcaP4c2/26hsMqPTiGmtEn242smyHij8N2hFF71diF+qnDfzdx/Kf4Bb9I8Ks2CgRa4AbcbmFuYzcigLy+zf/euo34kIqCAwCJS5kveexZBmyB2euHAmsSsqLqU11ZQRGtW043/vMPd+DD6/wVgLjf71yrz4Td53jO+MZAS25qUV0IgHOsTRTmoionEi4/IU6nEsOxycdmqKG6QD6eOr5+QaMAMc+DR4ZM7lHSRnIxSI/K1ZHd9U3HB8PMKHQQeiT7oq0WqdqgLkd4aSlvhmGbIF2O+kUr2AmxaI+AzoJph43pqXrgUci3j8+zKCWJ5gjQE5KKOvq/a8VRFRt3DdQ/zZ6+5A3y+gTdv9Dfi9nm0Ehx4TnpqUicOuFRFPN/eK659zI3nnK+bcjgnTSwwKxOqbIpijqNZXjYbraAHEgTRVVXvVO8qf4FqgFlHwqoePq4ELDdNw0bTpOcer/OCVr9lWR7mgGjREl1r1qwxGn+D+c+CSgE2CGd4jDX21P22lq6JooJWubhhrIPbn4JOdVHaPjY2thu36qr5t7rL2epadeAK1w4AkLqN3B0XA0YSlVYRqOJgKLw4kv6Dj6YKjHwOoj7KjKDmio4XD0El/hbH4R0l4Zy42yy+bsuh0cfwRH4OdnUfTVbRxNUHhDMEbklrw8DfcegDbyYYSOo099mKLBWXS5Rzmt3ImUBJ30pFCpmKHJUaT7Mk2b5cNmTKiUOB2Me/e1IVMdJaULPfRBHI2LLOjq525t4e3viIB0fKvj/UgTqnVQdY+T+wSL8lep3OCf00n3mvdWKqnOafecQ/LaOItJudncxBwquBhTZZ+tLvR41Zgom6maNZj4dU7Pw9ODTLlwv2QH/9ikCLl+ucjwRDf30KRIWbCYxM1RB/oePzPLsskaqex+CvWLtCXAWwTecvAOcZmjz7k4hViZOwUCfgmOOOE0DcToyMTAjYmXrahpXDe5+yr9qqZGVIeAm2+wk713hCMCovEa/SyMTAVMUe2W2D/RbYbYo4dF5OYRFsyaJFlSfCxapawqU6KBMWoVhPqre+m6YOTczbv8FztG0AHqDbjeIVPZi1rILewPcmvjCf36qCzWIZU9Dt5jfzC8S4yXaPJCnia21rQ+aaWQW/qkp6iPpK9S++zMs55dh93DQUdoRyKCD3dKlOMeSEziurYYStE4UTNRM0g2SxS8ZVpoWk6LnnJRm267S3n5WybHRUEMXhRBQypU58aEQ2OH6Tj6c8LibXNF8KjE550J1EwUkwAZ4bF5SMIlN2ZwXxIlNgydeocRhZrZmfh0NDGh0S6Bb9/2VSC1oocHxsYwksvdUssvOHrHkdE6egNC9I9J/a+0gdZhdiE1MBLJbiixZGu9GgR9BGjUc6VkA1x4x/xufBObWh7rtgsjaDqxIHPrqL1vyt16pPaqS2gvHUdfHpNcKnODm3cQ72gks375rjlRftS7MDrxZO6xZcxMo0tMmf/x2MNJtXG9bJuraqkoL7dYzXfaEaFqqZWOYeVxJaCWAJSsUDZNlrzcuxc5mypI1cUcf/j9f/BzFMHRAPbLUUY+yKm8Q4Nvq1O08cEWctOa9dkfHz8/qXPMGxHh6smV9lei7odcUnp7vVQUNR4kSZA768DF0y+tnko/j61uYBz3F1t5H4eMaHO1jgqgPwihv73g4nS63zrwD/tJCLszjh1j3yphv1fvZUxyH+DYZKeE/1lJur/Mc/KXCuOKPVd+WRuqmCZHiCxp5sm04SeVUuFSkHSsbAxf/MWTpIQQl5dQo/FL8pbYlcQaqRzqS5g/K+Io5lgoqfOhVuuQUFb/Aanu3wJZOmmik7hOqN9rcoj3umwsLXpTkscXZO3kuYO2JINphDV5lM+hLtbVXLZUWXuaU6ZkKf+K9XKZIFHSmeaULkPiWAeLg+MTR9plfGk+NKnZQ0t8s4uHWvZ70DybJGcPLl696yAZNPyIGDDqJOVAs7eX9hutyPQddJBxpoFuS33H7N40bnJ2r8O34baqqapp/W1f2sw2evjG5Mz7Toqjb2HyuVS/fBJ4o9cya8Ni7wU4+hSk4pK8KU4uzOhjBrEybfpnxfx8eWPkvaM8vA9r8XUZuol3+2jqH3Mrcj5pEVgPV+SvVHXPP5Vhds9VvbTJVrxRoO1QxhhF008yu+erco2sEiNkwy2TyGP1Hdo7cPYJtDcsZiNlIl43Ylj9LPq4fqRUt+FPMX8fazfkc0Sbv1XfU6IrVENX0ZUy2ZYo9ll3dvW1x3FwvxlZFUlnWfUxzvhvJpS7bkx1DKhoRnR7zkhgjf99bkiT+kcp/iaFRVaYSFHzo5LiYfTk58FelkJ0BJRE0nypByYU5uTygr9PJ6c07a1W1ePTU/B3CuZapKpy5mNRXmDaKf3ocphsdYtG1WqadM3lMPmaYU+ZmqsRsjUdNyq2yPI1WqFS1aD0n1xkBcBrCfAoh1kQeyjsE+XhLR/D4+t/lOmtmHQmCDkX5ekmzjC6xgdHKhbDm1JxAToaIP1FREcDFwApLeGgdOfTbX+XrgCX276KB5UV5ZWXhG4HuFjbBjob+6lcVjwur6z11TjQ2dpWkhUqVgcV1Vg3+3la8QSPQpUwxJg79p/47tfdu8wc5rYtdGIweHm2LtTI7qodury0M9QwcZW2+98N6YQ7mF+7Tj07GeQM87HUxdzLl+X7lSf8+cZv98oVhf6jG0jWVY0rfUfV0QeQrCua13qOwhG8mKZiivlwgBHrzu1m0pmaQoI227zrGRwevsH37Qp8GKI7+0Qmezrfyw1SrxITl9ewjCsvvI/YnTVzuLLfi+CFB811/nduJO8IL7xXmNmpgD7fCHYaYOar2UJ8UwZ61hVY29FtBRNJVbeyMUZZj4xa8f5ezubOKvb88c6ONe4/q8zH9llsE8kkZjwdgsGhiZGWB5zyfqpulHza77knip5xsO+vTggUUiJGS4fkhhIbKnN9NzjLDeoqUDManvz5YJ1m8Dn7s2s6it/hWFfIBt1LMXC1HGeIBJ/aERo++TT7KZ539Iufyp46ihqAM6h0arHHVGwhtzCO9Ct27xjD+N0uqDS8SHpBLynIMx4Mcx9Yq8W+VNkBkawLy/7ncyOHMqNkucTahnDD6cPUibzAEjZySqD/STO4SoHQboERDbni7z19uOiQUPxdBj2jIFj6cjVXXIkYLhWWE5nZ8bC4fINW8rGEjsODhfT+mcbraZq1jRINFM3e2oZAmbHSO1AenPVn7uHuzuHuaxEEu3ihmo18l99PUoNbX6n+2aF4s+NtXPaFxqnmhuwGattruWyl/WggvHC0Dx1rBG0GV2PK6SDNjNTV7Cj+StyLXu4CirZVbeZpZtVesiKdUcJvw6j9PyO14fuIw0Z0oZ6Q/fuqaUoA3h9FC4q7XkS4mgqhIN4iFkIOQZhILDk2fAQwgkzA8Oby/6u1PQqCgmUC5F7yD66PZfSyGP6ZCk1FEA6ijkBeUnyxeo4igStEurUBn0dLP5kornc5r7+/m0XPzcAcPemqgBFbIimhnos+hDRfpt1kGX+8T4wkcov9Cp84HZIBQ2VYtasNfc+FdxtnvHi+n/SrN8OF+yKSOgZHLHMFKnpLcy30I0wEm9Mkf5eQUBJWlzyjrBeXvg43VRgppv8eU5NFW3TJzDVuPqIwUfDkA/OL/wzZH1x3NyvLSUkwNv55v5fvl+ZxPfYmrpSTJy+UXwL4rmNDvglznfjJ9l1iCyHfz2AMIAPPxp6BEEQ5PpV8sdZs7dj7KNxHQIn47fTWvVpiOdLYD++3ZfLAKogqUDOHMxnAiq6qoRWeM+TO0z2vqeVsNTUca6kEQs243Pb6s2pqGnLPqfBksjYrX9JysTrU4/4nDhzG9Co6EvRv9xVoCLdUHrhgdjXoMxb1SWKp4t6ZAcV0qyJTAWFc9RILz7yK7Pz4BqhMCdyjeEBE9tos9+utZz1c26t3512DOuMY/gV54cmlREJRRRoljQzKPSWEPDwEpb2b4N1e7evh3Fq+M40P7Yqn+xVRItPKskNl1bHFZMCpBpx/ChiqzCbbh6H+v/+voD2nTtumFufExFEoyQBar9g+DTyD3XWU+3kmYIRNwRvxI6LC0CUXH9KFLyuSUF7op5vbPXyFp+3Si0h/yZQ7aHBkY1s719vmFqdL60ZsbEL0khDSKPoJmqYx0wgS+MPPEDxSRfg+G5/8Aa1L82hZYGyIf36mbJhQkpi1vFx3vyup0ihTF/4QKAUUJDMs/td/dhSPd/Dr5cDrXfv5KyxoA30jC80AJOEFuh1N15ihi5gq0VHR6N9fDvdfwvLoaBDuZgQCbE0iprY+SFA+Kgmdq1nRfDxBNJoNPby1134XleTDGqdVNPsCyWGEgpfJDeMIqyAMJkesB7XwMLeoQ7tFFd0Ajwnk6jLIj0jAqPOSZbZRn8YG1jcdl66isdXV2fZ2/ToX3bQM/Lm9PJpQNRlRxNQQYLYBaeE1UtL4RZR0aBHl4WxsdKek8/oTNRjQyc+2tx/QOe+qdSuAK8JFGrlj0zwPl4IEtj4ub9L2tp1ZgH4caRjdgXRfGKM14NsOtfNa4Sn/WFMrrRlUBUuuilQIChVslCOaZbdus+s85BoX4alfsTMn6HuGtaJr0A0ag03C+kpUVDiS5/Gqk0B43NgGVNDLod7GW3nfSECyhtE0dKvmPB3E7Y8OIM5IZdjPmbh7NejmNiz9UFdstHwvZ3ZlgprKi58vaDja9M9p2oqTZaCuBxLejK5BP9J42ShipERFEdBvXvF0PEQ3tmLp0C5Ujs3PFkVq+eqzDrAXVizeNcUlJ1wurZ1Gxv/G20/WB/qlIjm5qARUntoIXaFRwAIskMSw50ueYBBri5jrI2Zhgn6NEjlgjM7tqapb5FX5JkkO+j6VkkKz1TyW4XDmXnz+QpPUo4p5kHolsECJoWy221n66SQgWaNYL815WrUQygq4t201urUV2wHiPeXOpBSSx01H6wBdXyS4EJWEJl2sazwSIZ7CiR7dfkerRje2Y9vBlEgYIVILhsiBzPjUzj/ORZizm33/BI1DlBIq8L3y86xZn51SaEFMF0Z6ZiUzMTioNqIU6aX0UpHmSFBuTIkgR4BsWkP6T4WdJo70O3bvGPP22x2QdqYDHZzJX6Cyup58VoW7cPFy8tM3oMBi8Pbjv4APYTBKHh1gDLzdPfhDYFuI3/+oR9y1zNrH5Mde0ZSuUW4IWZZfoa9QyJTeurxPx+RB3UFx6ivObJ2x/pb8lh3n80GvfI75Qmduk7er6FUL5AXVKNyjhdDdOp0FOHeJUl5X9M/jlT8uKiYTs8PWbE5YnVi365FAlbnWpLH6XxvDldWQKUeY07v39u5tdH1jb3MlGOcQMN6BpLIuwWKJc7ezSpSHKLm7RnEBKfBstHTsAVE0up+URCJ3l6hEth0gbEaSiJqxY4Pc2gwRNp5X5KiRa0Td2BfwYfCtHU6czEZxibAZcY2oGcV5wmHkD4mqEZ970vM3WvxbY7cy7zMnVrWMP+mpNe9cjF1JhI2nhRw2cpWcDO3b5VpJhmB55tBuVKTijJ8kN4cWk3eCJhQVifuAcGZyTL92MFq6Cm+7q5Tp5UsLsqpXkiCfMEKBFY8HJUswuz3VUQ6k7+vyE6uzbO9bIcFEfEZOEd4F8iCSYaJ+IOpQFMVG7SNKXCuIVd1mrkDWkoOJuYklFNP2u/tbKrEShUj6sy4RxQXJ6e4GMkxU2zKtlES3ALRDUbHEkpYqZcFuYkc+W0pKXZcoGyoJZyRJrNOukETljqSuE6TNxb0up4fW+aB2LgnqJMcjiTutw6dRKU93J454M9QREX00GzliWEmIbPtaXZRMSgKLjagAiaPdRmIn6l27diF0rPuS8djjVSGaQPCp+eNJxF7vdXcfVvyaLtfhkxfZsP+M9YwcNZ6/RN1YX4i0YS94Qpsi4ZHNWr1BItZ5kOqdh5VgqEeRc3VVA+YpqyKUb9DdqzTI3y/As5+CNp19Cu0gaWkPiXbeiSuVsG0z54PT+CH3Sh4V6okdVv3JazUuRWf0fMdHMCsi6XXBLpPa5iyldkGr4sfhpU2HE3fgQtKszw9UpA82imvfTGD8LDObZLPbzfLQm9kvlIhr8XHcEbUAeWTU9nM4X0Yig8SP8/r+AtE5v8vSf4iXRvQES5u3cnsZfAzjbtc0+xjt2ugKETwbt0L33vWXeEH4jCq27fZKtA2kY+0lQH36EScMyojZ7RvbD/LQw8Pr7u7/O8t3zq/qvvsR1NfKpHp3dyKip7qy8IizFvQJaFUFZo4Dim+in6f2mW0Qm32WAR9U3M9+Zvftrm2fBhBVuacQRMqP8uE6lgMFV5W9bLGMCzUIF1v82V4+HL3wQUXaL2vo53OAU/de5Y3p3p0MzR6R9s1NbV3uPNhgn6a55lj/eQjEJ772LqB4sw0aQTkJqEjP5wa3vbv7/QBMbueFXKrDdsqmYhPOc313igZiZZwvtPDqv9g2bmd77vJnHMfoSQXFzwOw8+7u3ehCgNror67/Y/FPd+5/tn0vAHzwL5oAwJfL6L+fdWvqrfwKADpCASRQP/ThAui4xBVq34BG49U4f+5UTn4XpwZI9zmQT9uf/xOSVNVtVv2c0Da/VTdGcJ5RySzOxAje2MAryqwSzzbnWcdVau6z3N8cKQaIFGbwu0b6Cbsfh0lv/VSQUxy2u9jnNxvQPBQBGprMIiiKYcCx+WzPZRLRzaDNGlpNKWPpNgypGGV1krUt5Y804eBjForCGClMseN5T1GKFO5Zh8OcS8wURZfTYVM4x8TG5C6eY9r6XV7GHY5reOudDQynJzMmq2tIbxatZ0xB5BjsBTr145IUluUgEmiKG0ra6CAKR16UxXX2pKBoccofgaDpopGf3nJG5fr/EmsfL9k15K1CP2+iSqhKstQsaAgG8ZSSKO0J53yNaVdYdmWJkj+prjTehfzP7cwpPKOq2oz4UkGdz4h9xy8TvCrmTBx1pH6cWrYilZ8ghcSlPpa6i183eZRcjM67XJHFD1r/Uoo9x3OjhiQhjoDq7Co8+qrngqImsk4V11RxSxtc8cjZrOhMaDgSX5lONvPZ5DQm1PMxKqHNKFihh+nKBLlY/6V6StzPV8E5AN86clnatTohDYpSfHS8IMmovMZF5LJ+39Yms2g+29j7qRovc5ZvbcFYwNwBT0APMAR0AFHABrAADhAGPG1qIwhIA6owGqAIt3a/bQHYAVY2BewBaxsFuwKKgBfgCbg3pYAsYLjW7GgcdUXHBI4P4GlLAH/A2Z4NHKFSMdUXt9yD0P3c3z19fw7dd+zgaMExuQEI8KcMIm7DsMp3s0VQY7DuN9WBMzQkOgJbADcMwcVmGAoP9x3R7R1Z8jCMgwjDWO1Hua88bqO7zIevUP7cuHAVSNpJylRUVJoWH0bqCT+qNm+OFIVmBXumjLpVACvwth2ev2B4ThSl2ocDH4EAUc5FUFzYO7FJ1F8AN+VtjRWXqTrlrJPWUzq9nHgCcz5XfOEoKS1EKDqErxaOrknxFPnw55Lz5FYr4Hn7FAHHDenQdtlVem656oSPsjK0q4OrlpwAAA==) format('woff2');
	unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}
/* cyrillic */
@font-face {
	font-family: 'Roboto';
	font-style: normal;
	font-weight: 400;
	src: local('Roboto'), local('Roboto-Regular'), /*savepage-url=https://fonts.gstatic.com/s/roboto/v19/KFOmCnqEu92Fr1Mu5mxKKTU1Kvnz.woff2*/ url(data:font/woff2;base64,d09GMgABAAAAABpAAA4AAAAANGwAABnqAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGmobllYcNgZgAIcMEQwKvFCudguCEAABNgIkA4QcBCAFgnQHIBuNK7MDcVMl7SlI/usDU4aYOqHx/0iIDKdhs1tiOhRCdRDXpnvAjjLsifTEReM0nt970fkrMLQeXJDxT6HTn73D58c1uMQYIcksRPRu9TVJxyDIwsGKnanb33NhiLZ5gMyBGdvURdQvUxujekbNsZlBGAlGo/1voa6MaIzEXCRPvOvf3SQz2Vdh6murBBaE//yd5dTiHNJg2Qx+t18AfvNFLxCP3GVR1G0NobyRWnYB6P+3+TkD/d0Z+EWoqESoRKicCEPbWknplGcg6bDfyAgZ5YC/G7Xl+BzcrHQIB7yAirdGGr7qg91Y1Pb/f63S1p2/8Gvnm9qwmw5iHx+ZCIM2xlT9anr1uxqHqmuIanB7OrwEIFSAVUMIyYVdhIxcGxkhHLnk+AipoytUT3xsko6lkg/FKl1zf69p8Ew6b1GHygpPxwmQhdeNr77+a0Gh4QcAWtNwySwzRS75KAoqSFFUUYriSlKUVpuirroU9TWkaKwxRRsdKTrrQ9HPNAqFBriAiwIU4KGg8qTqu2ITJH10H/+CpK+Obz8g6ZfB9B8kEYBYFKD6fB7/AxtQgELQAA2P0ZViOgHKD1PbPNNHYLpG5hirjJZZHqYrln1D11SYmy3pheTPmSWu59Yn9vmav1JXXe/qv8YvrdN6yknZLpP1tUnbl/Qv2ntnoaP8SFhZr4yR32jaopY16kenZvenSFFeJllklU12OeSUS2555JVPAQUVUlgRRRVTXAkllVJaGWVVVE11NdRWRz31NdBEUy201FZ7HXTSWRdddddLb330QwwENzI2NbOwsnY0RUaKemjqoW+HuTW2prnrE6xbKLYIKHqBOBJIBqmaZUA5II8SKmhWfmkNqgfejklGM1x/0bxlbGvggCNdY6VbvWgU1cnG4mI1SzX5DFTXMtiOxBEwb7Kyvi3m1tla5M68UJsNr8tLTsgIV2hpwa0FgqY3RSkHKnyaOYQRM9kJWlkIEolR1kINfK/006F/EsKD3ZrEomiTql2aKCUi0+yZ24a36WUAWZ/VahgbxNo8x7WsFSNqcVyOW4LF1hxz8oxXAgnF8KRakG1RA88Llle8ZLpx3SIRW9Qkqk4himepmqMZAdYM/UwmntfckljLGvQCQgrhlX+L5YkZsDCDabAYiyDMNlamrmDGNJC9FRoCXf/WNolg7E7xulAbIjNFiTgSSFKqpowhR3mghAqaNAFmtOxjjQOuRGoW71i068iADBKn3posigvNoEm3qA6Mg2kr5pFWwOX6HLBclSoSwfAYmsxqkAcYGDBjaASWFSbiosxAUntKyZSlX8WeA8QqEYA8mJXGFmZ+V7fy0BjbnFbEZYQGxhYkiLWtAX/RTpJ0jJUIUiE0/ga9OsyBVagYocDs+8pXoHwDik2q1qPdlG4zSDAi7j5J1YINS4kbhb9apXsB/ls4IanMFeNVTkg1Tu14dbuNCCMBSUyuyZlum/ZsZSmRAR5YWqqeM3xRAm/4wh3eBpVOwgYtSWowLWwYsCq1PMC6waBUs6W+IrXdijuVAOdlinykwUG/Zms2FmDOCcHc0hyGUSuWMVc/LXNV09XILtDOYKyjt6X/uNtDWHMRDhu1Sb5hG+CKa9ah5onVS0y35MiVdn22pC2D7SUFN9NmyM012K35Mp/R+aZbknnTDSFcWtVa3yysMJJeZatynvIZp6VKSkVJsYrByykuoipB/EJRaa35AHkB8QdNLygZ1y+i+BUAddezJrMJrLBqbQ2SzbudS7t4QPD+mhuuN3PDYVkGjQi4+Hqv5mrWd5ONz5AYv4vR/XcrGLSxWuqCYgaPq+1yqHS3MzmioXXcQpZFx5fkrizNWyp+1MooCi5hPdBYS9XQ1F98Lx3mulm9l42xoJD0q0Wscwcm/djTm6c/+pX/e5q7u+xpkv9K0/KEIosuIiIAAHwAQLyDGzlPBaFNBjDCxIcMZkGX3fsQ9T4JhGglBw8e4mmEL5kcBqCAID8uPgwsQMckjk0Ai5CDsv1gkddPm0k3NpoEgewiiSFuscwBdOXaJ+j40Q1QbnAIAFAKtwQHN7SqVjYAV7NGbLyY1JARhYXrSHu4tFsQ3HVerC9XWFEsK8s6LetC6TyiTB+RDDBW9zKodPvu6o90n4B2azGp3h5qvK9kQMWQ6dnkf3S/Vk+QkdoAkNLGQRll7JluQIqr6k/GA8pF+BGEBOlokk0KSLU0SIcsSvGLaOw/BMkuBaV6GmZhGO7sv0+sNeCfv//0f0igZDylNZh8b5O0kUlbmbWb8ZGXc+qslXaRXVc55j7l9Mir+lhPneRdCW7f3s8n/3qIva4/vcHKKa+CISqqpLIqhqq6UqiaU3WnKU6NfPbGxhhrnPFrr1YzE0w1UXOukJgIRotCOHMmCVGcNkI0TiYhOqetEOFk1sU4rw20gzjhXiALxOV0FuLjTBbi53QREuBkFxLkdBUSx8mhSyg8DHSDZODkFBLh9NAlGp4A5IIkcnoKSeJ0EpLMyatLynkzA70gWcKzAvkg2TgDdcmuFQBCCnIG6VIovChQClKMM1iX4uElgXKQUpwhupQOLwdUhJTnDBVSgVPVEXjOSmAYpLLhVBNShTNCSFVOdSHVhhkJcc3ZANSANDScKUIacUYJaezgTjYxnNFCmhK1Ls3O2xKMgbTqaQPwybZnzgQh7ThThbTnTBTSgdNcl4XnXQRaQA6GHwLTkOn5N08rCg0bLXYEAJgBUH5gFqyx8I6D6g/VFIABAGjoqA5abBAzceeS4FfWwBV1jNJNhGYlvVtin8VTdwsqdnNJUrGtoBDdKNHxbbD1gO2kuq4E3UDI73VsV9I9UVscnyfoBr053KDjiPhMOzFjohN2oq7PCVjRXJ5QKNFx3LAb9DleR7xOxgS/z0nxBWzTDJjRHB5f0OfadrLX68bhdHE+0ojX72TJGO+EnaRDwo33LB2QfizF45vzeoYln196+XZ4KfOAg4wYFX3Wu2Ddk5iI0WhVoUtL9yK/Fobth9gyKR6m6Oo1ZRsP6J+YJqZOBYZeUg6sUATHCMDVQiY7sIhhhTb0Ynqr16B+PEs0VCgiA3FsgY+VUzEosvdKqnd6c3Kjt9Sd/5JARceNaP7eFxZ5kebaUNgfYqJHuWnJy1AVsZJLTyucX9DlOeWXYDrFPL4duKe9gCheZofCM0nW6Ck2mpHnEEklXSX0qnulBEfMEP5nPFvjwaPzIqJQTqzJx2YLfXfT1fWw96UI7tWfbVnpF0m0ao+g1iPAgbpXxtFGrtzKjQZnkkTFQZwEyA0ySuPh+2mSLa+88HgkRJvufgNbMmupG3P5VnQzNrPHPmOU1/inqqjnvaRpDWaXPrrNAo6J9wA3eJX3YyUHlT8fg7cTyzVjrIvg+DJFSa0mTnYeFz/NA1Eo/B8FS48TUD4KONi+Hhoiks8Gb/rTvcpSZJBDGyMjpRCH5bVtBTYk85e1iUKSKkhJZp9RB8+H+r6siswFGr+LdaR781FcCz6cyoYhJotsUvxnXcCGhRznFfFkO8eMtgumpeN8DH3dPBKCLL2755fE8LiihBJFkSsHNSeVLQj9XCTXQxiPG4HErVYQHjVL6evaC4ls2sYAF47q0ZQ4skkLT3eyh+nsSUwYbTAbXGlBnltqHtoeVtYfWrqXuEV9yl08LxeBZWKxUOFs7F8qNQMZtk4dRrT+VlKStZVO1piiRbp5rFfa858b3OeU32PaC7qqG4q1rTrk3oq8QX6NdJ3AqdYoGphZE7Y0Cwp/Y/ibyYxT481pM0Os9raHlUN+mKZrra3QgUFmYNCW1rbKrj3pVI6OVT31CUwYOLvwhQviNNhRonmXk2qikkduST6cMzLuxwisIq8yfenFrmnrDTZbO1rvEo9R+Kw2Z4eTHc3fozXVknO994G+02EsXHsdz6aROWSdS1ZeZcIXTs2j0FrIWvfg6H+CVDMzE2BMebvJgWF59r3aH8bxVpbUhX9WJOWqEyOVux2/WDeHEkpZlWKJ4tvQQvvtZCX1xLkuhbYnfqSgSiIfuGhotU2VemSqjM72o1yaTXqdOVkZ63y1mt+7+hZ/fw3ltmUjh3ySkBC+AcNPMRA3mXKDzdM08mpauM+I40O0gBIb7H1KqMWEANeylUU+4OgtHErIngKrMWLau0aPzyT2NT80MbtIx7kDGX5uZdUvioXFbvoY6BmJr72F2u6jWAilOT5+omJ6WGyyp91YrplYTs0HlzBqEo+K7EutEvVuJEeJqqjlZ3rFf5JPPjIO/s2TbUGx5sTU8P9cxUMaBrXzK9UxXQ3gdreXYaDn50OfGNmH+hmfSiTY37NBDsAyxtbWm5imTolGp97IBFb5fvw7uXgjm/uOf3ywB6fUvbhDPmRva7cQ64j9QPZdh+S81K919DWHa6GtkZdx9KJFMbQoq0G8IDOBXNXF9zts7ZNYSWpu5L5ScBed5soxnpQEsIyY5N4+V6DgCxrSJtKYXsp0nHBsAK5/vkxwoZzRwNn9a6V7eHP8K9M7MIToGk0C/kCSXly1Ou7Bu/w7F28Tj9HOzgNJEcaBj/Q75+dfq18XMDrASog50EcV1L8+/qMCCtE5AxF3ObMY+bjs6BuodoueqAf43PwifHbE/f46FKZTuLqiSxi9a/W7RwpGWnnRf4D5TejrL1aCzbvd3LeX9V01USOeFhlEpCWEo43BizeCfQArWM+/3N5kU3DiEVH77Exr98A4y+S62fXZwfG27mlwgD6Sgjo6CU78xzXy9StNbSsLyQHIUhnBwMnRwcjP6Bmi6Ej+qOZS3YtgaFuko2r8O6FqCRqHnk1EqXxRczIAr4XGhGS2DqV5/BkLf9e7JIKRHceklHwMNgyQv/TAXi2gS9CpxZ6SLhAtxcsZGhAPCDAEf5E6a2PtrelpbQWEy6DKG/vuXIz9fuXu/YfK1YJSZ2yvXoP/2LSA5rEUr78TQRHj/Bf/8SS7/ZoKiR7jv/gH3luWORwaljH2pSR7LDQoaxg8mBH4Pn224ZO9Fw4G2JGcva72Ic1NM0f3VGEqE3oDs5VjsUoxeX6JPeDCQelX9krit+jKck4KflOVRVWs2ljkta+NoYmD2rNHFo+E7+QRlNzUohYKYeIXLXu6PsyhHWztaOGjAr4VsoOHDEStDw38jL1IDEZzfosGwBovX9Ppz/uuTBvzjB16zE0W0jV/ddD4hI34kXMtYA9B66y1tVe0LQWn4n6DhD6DKa6jJEBAIASc9Rt0p0dzZ8S4UZ6+knvxVE72GXiEFawD1BqKGG+BAj84ygp2QOr9kuXBiyeC+C+J5z/sCGyUYGbWUKQESmQsKiydlE4OOpuJv5GUZEsg21D0ZwxpDr+Cz2T8EqZ4XmYSBW7nRS1BSRC68L9ycnOMqxbjaCy79TNqaxxzFmAFgcR93oPqh/MCI1D3khy09xhZXl7Cbx1ouO7Ye18vNC3pg29iQhAKiB2+2eHKNLScRoPp2nqwOIE5m9iRmMCH0HBj5r+3Q3hKmIczJSQIDV6aCPIzUo4X/docRo/1NnQASqAfPSfjZCMTYAWpyeaAq9cNsEIdtJdQq1mD2fzQD0UE68osZ9H/Zz9+Qi1NYM5J8/dTyz6sF5FM8fWkhoWigLaDSb2JiCkToF11LGyNefqkbc0AVqjuyec3a8J53L8sDcBLIjWRyqRmJFA2lv5fKZoluma4Md2Sia4zTYVSNBQb3Hx80jk7MM43P9Sc6exTOWsavMMz0mTimWF7HhD+fzw8txO1avq+9j3TqZwamoREuYHbHGXJpVFokjd4tMvg3Gt4ps8w+smGQN6EAbDAlgjic/n7BxINhbdlJ2Wu1F520tB3u6Qlr46laGIsLhm/P6ouzXFtEFz3k5KmAz4KGZ+RSeHhItNQhz8iH/dn8VPIXHJkLn5K1v4OpN+4ZmS+vWOYVS9phpjtrx9Ybvk6sNQE8LtyVuOLE/hf0OEkeO3H4HhvWuYH8kRtf24Ooie0uD6t7uPVCiAmwujQLlHvblWeoT0ISzBiNokYgd0MPf0LqASKz1BLSZONYX3+hFqeRp8CWPF4TbXDeuFqOSEFs6LudOLshsyNxr/qyMzmngPRy9hPBSi37yvNjUwt+MqDYt9JvrPWIYSQMxPECxpEWWLoRQY+M/xKc1Rz2GU0PXtd6pfkPLBJ32Jnz9yTEnuS83O399Vui8QudHvENdxe+x8t7brCmWvtilYStlJd/PPbnQDLuHj55wd6SjSQvCuQSI8Bx5z+tO3wTwrEvdkY+xA5gju0q0n4E/XtdIi4wbN7VG/7EM/9QVL0Y/Qq8bjonEJKYEpFOn9haktQcErrx7jUKpAEZQ7Nk6NfabipSRuIuvjpkEaSvp4feXrJ2ZEgl75+KobNcWGX4y+44pS2nYZABYBIvidnFcIrxl8E00L64bvbd2HQd37zhywexf8Q3C0eIA7AA8Rz/cvE8zq4KdH6t0yY6SNeP6WLJ55bBme3BV87qZsYEk2eVIWtlBbX9aZTXqi5KxyuuVdDmitgVm9VJFbJh9pbWL4zkNOylnOCE5trOMCVDxAMwSLSl0VgCBYFY20B7QiEtLhbNU/lcXZeXAH3r0AQkmYvEisgSmbTOF7SHmngZKvxf4U6wK8dMcyTBQgWEpkV8hVVPnIEhjYAgDs2OxAI6d/shwEKTttKQyATcAtDt6LDdGNyIDDay9d9tZLwpxPuf3JIt6r76hF2l0SlwWPpPDj7wX5CRZfE7mMmX1vIlOe9WT7mI6/A+psXSireVXtA0wKRdzzeDwPcRwihbdFg2juBwcwH50AtEugu7dva14mqfwS/4Nhk+bpLSyJAxBiihdJgWggNgVTuAwgIl32ao1G/7igBhyGKBf16I1GJ6q9OSx9tQlrSH/x0ogiZO1jqaNo6GqEA4eOIWSD4YSyE3O7pw2hu3MLw/+sy7yuMIr1VGTpvcpqMLXs5LnyTprr8GggMLmNPZjFMw4JNXeMquVLJ1FieKBIDEGw5hAf37mnp4zmAZ+GSPf9OBkZ24/j5hjGDMATDJ8oyhyXp45+Ls8ZDg7OGwW1nCEIyxP7Rv/IxzY7aJlvGjpfBYPyVvJW+OjhmhjdzNwbHrsBSaA/yJzU08UhuTjH1jiJv9BniMzNtGTdzB3vNAL2jshgYplPpRXCRFHhGNIpx5dRgcOi4ZjUZ2k6gjwOpFOdvg36xFRvDnAUMTvNwf3NiHAOXGEuNxaVQ88GdHShRqhzdUxYb01NdgR5MUxTNEBJOmKWjHtCUVXCvM7l7RHmrLzsImTibqyo66BkCQEjdjIOTDpViuhhkyhBS24LfxCMQkvN99Hhs3RVO7l2tcBuomuzsbni2ughKRB6cd0VnbetRIGVj2FgDAg+vimP21D1aP5CvCjC4cjjtexoC/bgNA48HiWbhZsG3E+DaGwhC00mqghOTYSQtncNQ11JVRdsC6JkYTBmCCp+2kfBjk7ajwra2MMAyWo8+dnCoBIfCisO3mG8IkfznHft0g8raCiAEfns61UHNF5waIy87+ba6N3zIIseFJ3vZaZA9LE3aPAxpdsVCfQtkjGPol7cNHxqpNGqa5wDJGdzJ9DMo4tB+k/XRyHac4xiQTnbaHQp4qyt6cRZh8S3Mw9VxzQAyxE3jmDPjafQtVnI7ODbq9ZjXr4/exHiK/vHO/0ZIY2/1Itv7awW2swtMyV4b6d3/lZYdC7BIdlU9TFi6kTIW9pg4oKY4+WPvhsc3aQ26Xg9vzCiYsHxBmtfqkaQEq4xapVvK8kvmQdaPQsxlNvOpjBXzM5MFWJCFVJhsSoXjbD5rLMriWZJKrNxwisZ8TClznHuxVZXwk2VwUxM1VPuvItXUikaZ2AqSNLPlA0q4I+L8k8zjqB/9poRbXGMqzcrv/3n6ZiWf///7AdgFqX5ZHIIUtCo7ATWiB08/tPZV5WJndbWV4TCM1IhdP82wClLvrKT1kiTFZGUmbKqoa2p1I3YltWE2rnXDLh1+VoszrlK7ygcXEyNMz5BWiMsKXSnc1/oVreXIaRexTWvRoIyr2ui5Epp7PyatcvSusw4GIhf027B9unvN6ixUo9/qQNWoPA+qZ6+eamhVR0P9XRitbOq2CsY3VE0dkohVRsymbNpwMPHBK0DLEiLja1M9Xtnf7EQbgKs/CpwDeKaU/v2d/PeZc8EeAhw0ABRYfW8XnFoC/1d7FygOZ5mxQnV1XfUPjJKkJJ52V68yInP/aZXZNc9yIpaKVdi1/9hVX8PrsiufFapQao5SKq31SznajZ/MG0unPpHaZJy5kaHD+kOudUt0tdRtTjZm/G3NsjQpTg613OH+l7tdjXksUuix1aFNVL5HHSwXmzcHha8qknNBwx51qDkK1KLezkuTvnKgHpZfAQ5eTAe3IMH9jcPFsqXGllzHOVFxWqWvTlls3dKyOn3y74xEEqRl1lsbJWuTKm9/sC5J1rrfhDUkZ+/pmnPWRkOPPdLanWxRvkvbxVkXdQayuu4ZVmysQHqexcxUg1ty7K89P+j8pX8oI5tXpEUPiM09wmFJZdoyHSmryVysX1oJS3llajENbGzEqEOyu2XSS8lcbo0iGsTll2Hj0vsXaCu7NSQs05d62Br5v061TtaQuFioOM3T6xAQjfwUMMMCUU0ZDsW+ZTqyStej2XSno8QB2mN4ksKl/UkafnqvqEMrijnJILuRJ5mkWF/qp6nTqvx/AUY9WC899DRURoUVVOi2IaMaZMgaxq+30+b8e2GhWIrfdN1TTRzemwm9sbl7ryyphzwbphLhfZh16UlCOzQ28aX+w8Iy4eQVZLGyl2IV5l4GtjzABVhv2YIsJeGkF4HeEg8Zt2Clr9x/88+RJlapKCchpawRlJlDrw3J4cqy9AoA) format('woff2');
	unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* greek-ext */
@font-face {
	font-family: 'Roboto';
	font-style: normal;
	font-weight: 400;
	src: local('Roboto'), local('Roboto-Regular'), /*savepage-url=https://fonts.gstatic.com/s/roboto/v19/KFOmCnqEu92Fr1Mu7mxKKTU1Kvnz.woff2*/ url(data:font/woff2;base64,d09GMgABAAAAAAMcAA4AAAAABdQAAALIAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGiYbcBw2BmAAWBEMCoIYgXsLEAABNgIkAxwEIAWCdAcgG7QEAB4HuUnzSsxYfoQjHr636//XPj09rr4PUEToO4IRQRHJlBgf/nKvtf1oamttuvHbwyKKIA440WqDfeCcvOaBRdtIBzbRsjUOrG390v1OEaV+MDndbbsEe4CJ6bp/iJAUgC4UykIICU00BQJ1IyWo586v9UDtOea0AbX/oWQN6pBdlx0oami1BOLgfacd9AiEkoRQlXSiRg8spQAhlPu7qad8NB/EQzzGHwNAVjozLpaTX5GPgeh7bmVv2S8BEOVMoLZgRbOIVN9lvLDsf/arXJH0o7gWSpPzO/VDUyFZLyoBoE5OpdYPKOWDvstaLS755UMCNEEvgDwDEmUhqap1924DFVCghungAFCgCXJAGn4p29N0KMrV9sVxbUcft1jpD95qOygOjkx18odf1s/9+uP1ccon3zTO++oj60/7eIUfrvX6iH/8JxuOm5psrO/X8N66tb8Y9qz8zmezfVuhqbQ/dd6R/8ZR31yfxbazY1tm5aU3vX/R9MoNd95au/HSifHagW9fMqt+86131G+8aOZ7j0f2RqdH7n+zU/rt4H/e7vPQw68U6V/dO11+2Kxv58+nbuq/6sYt6dgVvVLkru2PMIR6PlTk/z1Dwv+jW/MV+fsCaWCHc37YPHxH29Tfqt2r4Nkf2w8H3ovVv/7tv/dqI6uHgZoEgsoh/zepLfq75/+LayMpAACV7Hx7AYh/yE92ZNbPPn/+/9EUqLhIFz0aAQqSNIhWoGo/KlGqYTtMNjRtZ5PCQd3srlsSNjfEgy3r5b1rYa2POfYOnk6W5hYuaB4ubk/Qivaq4htEXLYjcYymbWzQOpXmOTI1cUac3BAyh4490V6Ouedn5q42BCcDxMnZ0t4OzcPBzSso+ovx4BuT7KZ2Vj15EOGE7qGD4GA1kgXCob4TAWewJBHYfVPizKmmLCuvoSvPzsPBRQW0DiIDAAAAAA==) format('woff2');
	unicode-range: U+1F00-1FFF;
}
/* greek */
@font-face {
	font-family: 'Roboto';
	font-style: normal;
	font-weight: 400;
	src: local('Roboto'), local('Roboto-Regular'), /*savepage-url=https://fonts.gstatic.com/s/roboto/v19/KFOmCnqEu92Fr1Mu4WxKKTU1Kvnz.woff2*/ url(data:font/woff2;base64,d09GMgABAAAAABOMAA4AAAAAI/QAABM3AAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGmQbjEocNgZgAIUUEQwKqTygfguBSAABNgIkA4MMBCAFgnQHIBv7HaOirHRCheCvDnjD9A89gJXUUSzMTSNVez0Cd0NTFveThZeyhl9ZMjzDb6vHOkKSWf/JNv/PnZldRGMOBOIs5nElRoiYvepXYtKqVyaVF7224kuAjLV78E4VD1m9sg7r0C2ZSXqiWHJpULoOwTY7yujGCuxcRIH7Gbk1rwh2oEyMxjWYzcoMRHvYWyfAAD//fN0DO3/f7iacJtpMmkA1SZ4GJnkQ/3ufzs9ZgWFGX7IOi/auB0qXCspodz+Czg6B7WmkhK2kTY2vB6YFQ4Db+P8/rdl/z9/yw4YSWg+1H49Fg3AIhcu8bHvJbOnJbKdnqKH30B1oFFgUspQ6B4XqTThQWBDOgkO4bpvEOFeOiP0yFbtsQGZj6hx4mLMpRnxGx/zpAWHMAABIYwzKlCFOLqRePdKoEXFzIz4+JCSERESRuDakQwbJWkEIAxgBRgQggJ5XM1H8ZTCN0m/XJ3so/XmyvovSX8tnByglAFD/IQDV/Dg5gBZAACIwAKMPDYIMEwCHh5fPClk+PvCtHx8+cnIh0iJ9Sn0+PT/HyQSYGHPi8vTyry504mNXcppPszwtCx84iEOZchUqVbFzqOZSq04DN4WHT0BQVFKrNp169BsybMSoMRMyspboObqmriT18XO/q7zJ873LgrkIv1si7x9E3wlCRXsUJq2eh8BSETA5ai9hBdOS/nXKY6gMlBYlvdnWV50YNcf6YoIlGkPmEF+7FrhzBERhnncz2JZirqbVtqCubfUxNbanqQ3u1nhi8bbCz3JArizYklAzwtFFGhZtSro2p9ozs3SmUdkWLAovZbzswxauWGrpoKlNSh54W+evOGCPHKxTqG/CrUiDZzLOdnhReFlYfwBd2IaVN5tQ4J0GdtCFnfl3SFyt2GG/54AVafK2AwODWNUYgi1eBharvEr5Rmk/fKZ1WQZLYaa2fVl7J4KjZdV9psgRrBfjaEAkxshd1ZHf4KraY49FORJwdR2IP8t3gp4A1KJPPNW6I2K0bgmiCq0Knt5lWg0Mz2Jpassuz5NYwbKxzTTrk5rW75nZrYEJa0uP8rbKF62NNQxgsrqHwDK8DM663xQb8vZTAkcEUHA5sDJQJpgLKkOOY6gboI603hqJA3df2ze9l7JxV3eUq1up1rdtM5lFB8Q294SXUZkttNYyVp06btNraSmNueUnetmmXKYsKIhRGhYlVd8HVs9if0DZ/ltM/3/l3ZVN8ju6gGJrHdQwlm0swFeofVngVFNGcqWe2RREAwAYvTKVgFpjCI2GxBfIktrocKcJJlMwKmSMSlmjytJntSFlulkwhFF0jebCWCHryEhqiY5m84ShFtAyfBM9BWNUJ74lt8icP7hy3WQsjRI7CgbVUlon7N6Mlo7qwQHWKmmgAi4GITUsZa7EOVDPL/hHkU7pzHSuoZyfh938JJNAmjlGAMXrB8MfbSyAoEduWr1nPV9HetA6UTN6wf8AGx9yF7ChRQBEpkRhY8NhyeWzgHTozV8JX6deyIqTQ2+f+Ekd0lCGOn1Dny8z2j/gbfXxEydtiqd+aNM/n5PQv29+GcAh9ijpl68jhZRRK8UB4/DDQLESSaVSmrUoU65bj14V+vSrNGBQlSE2w+xGjHIYU81p3IS0SVOmzZilmJPlkeHl4xcQFBI2LyLKCABtAQDAAbwNeDugGHhHqpDUeadCqca7RI9CSud9Cq0aH+CRDG0YxRjKMI5JdGMKU6jANGbQh1nMohJzmMdAfCFVqNL5osKQzpcUbDpfVhjW+aqCXeNrYl1hROcbtq/YxCbGsIVtVGMHQYxjF3twxfdThVmdHygoGj8URwpzOj9W8Oj8RCGj8VM+A7w4xznmcYFLRHCFOyzgEU+Ixd/SSUig964n+49FruKzG5kG8QGaeaAmoHKogP0ZjhZga1BbUl3N/6vBBc/Tooy/DS6yZrZn2b+yG8pKwsLHquUWrc5oFEaLPtfMrdMadXqrkV2ny9PzCqHTyYKLXDO7EEJrKtSwl7jEuUULOeWNajZhPk6tqUwRFeBRzuURzJPilHIqcErnicgoN085G3LZ7wzWs74YpJwuI9MI71vaJF5qLFSmsLmH/ShOevFgsttfWe0w4TTKoMc5pEHGGKfRD4Ioi2nJJ+4vArL9/XTPyBntHIq3hjjeYPP53ekcdZtd4mr6B07+mitRL/6m5s8ZqAfnOJkWUM1if6EHrYWz5k+o/sDDhfKSEdrHI47zgMpr2C9x0Kf7BzSMMB7TXg7HBPdEDlu199T8C1AZg5P/hKwsnb9hYvb4u3zUk3D9c/Nj9cPkp70VnyNLh3SbOqjaSRUgb3UOZMseYYBdCujIfjXT8rJwe4+i26U+bzpCzplG4718zKnIdpwdUxZlPIz7sjqHHmeQjnUXxfzj95fhUcWgL3JIr7H903o1vQAU7uHVXpurFXRPZWXbmFHtVMt79l7EFLFbBBiyCtl+mBrd7hda1O4JA1DNJCSC3FyIeuGpsXvJaloQJNyyt+Rp+Q5OohDjih9cjITU94SwVIdFhJ8LO06Tau8PgltbtD6MtK9QBHz3TgQY1klPjSy4Caj+PNS+MwQxW59ybxs3SKj4EeXQIQ1O1JpyxC6TLWBdL00EaBj2TTaXldJaUL9QH8vEv250Tjh5lAqWplNkB7sntAKPH5iOsvmqYzNnK7dkQvaMt6ZQ2Se95lb1IzUX5fbDNp9oGL34LsXilfM5pzaHyOql7/Ccep1hsGSDtPjTnkWlbv7ZcQrdHtLy/x4TJWPkV/b+Xy5OWthGcZybmZTVFCcqwnbt/fDx3A3Y4wQ8sOMlnnaut9FBubmY5TmxQSBtem+dDR/wBk6iSkpvktUyKk5EztEDF83vVskGD/yz6dz6DAULLU2hThxmuquhf7YD3PKN2DUNuOVN3e3nqWMYUlDC00XCNr3GEeQXnW6Zefig5i8KoGY6EerAiJ5b22IAwef6ruWGk1izKu+G4qksVJQRxv1n4oT6+h5OZrSwOxXHqTUKLp3qGYgMvQQ3cmWLypVrIunieTe/L6sl8qrDQvkzneIXPgkP1iFu5w903fc/3f/3M6HYeExshHDdjyT4+Dr+UbdSF6VcaYTS9Ury21E5qAAIyvTWdkfr7eD8G1qhCvgPzzv/zK2al/NY43JjH7LL2eYoPc184h8R558kA2ygpHD4TEEBQFDSiF+Hw4Hfc9BGmiW1AgSlNWg2qA2Erw0EpMj1HzbP/An3cdUKq1aMZAIpUYpsJ/nV+XHB0c0Eb8gy8tJkh7OFrppdY+8aIIevX2+7W8HD3L9zpwX4pVaMJtmwl1GqOctTzSFD976k2HNVVtdxlSfZjTRDYB9FGt59EoEeYH59kaPSBvigv8GWBSpNVfIb317fMAfj6HT+udziZAK2OC8RehO4hwoNA4RQi707X4m3gYYj9iJyceDz6Azjltkds6WxmcHPC+Dm8KPJSfkbKfKt8vPlnQbm+fYFKskGDKwg8SgBnFcrfPxnlpA0I2Dwl5cY8XM+LnVawOA3leNN6UR8Qsl0z6vy6XhC2QRg/ex12DZJOKESJKJQTYDt1AWkv//j84/8MTVayPv+T4i3DIgaoyLBy9t5IT3WWQo0VBlH0EVcZZqf2wgVICZB6rh+UlCdvl5tUCJoHsQM0VC0/gvxfXMdPCs6a4Bv1LY22C8AyInhG2+yx9iq2BnNdLojQqgZDIrtt/cMaNyiVKqYUE7ZAUS+l2S6BrCCpiGyERYvRhKt2Svqg30I9rJkFITezFV2/vlJvqGhnkyQfE2ZrqhKqVY0oXwE4fkVBRWIYNvdgl2EEV86MR34CYA2MN3SfXmVfMHOUKM+f8G/pHn3eZdkOnGapg2MVY6KoICQ9COfmPjj72PFsLr2up8wM11W+uOtYLfY0++g1HaTmQ9tPfUwztewlj5U8tu3rX2r/1k09wOD69x5YjWjlbxtAoVP9zdTiXzoNjRTTEATuinRKghekkfTJchtYJ5EkMqRhq7hVtoOjrf8WA5k5asnS7/cLJi5dyvc8OUDWrzyHyKp6HdegjT0NbYo3PDerekCBrBhoGQhcT7CDDH8jkeaKhlN/ty6JLXPz7Fg1TmALhKIN3KuS2mPqTlGbT7ZUI5LXwSqI51t65O1F65lnUE+1MG2Y4e+bw+9Db88QAOIev/IBjr6dtTtGBzGPyMBoK1fJ9lcxuUUvgiLI/tDM7NPRTwJA5zCgv+rNYm/gUzQ5sU7fjYB3uEDfD+xLi7aFFKYiXp8+qarjT8tm84D71DPCd2djEqZZVPb5S1pYE9Sgl9XQ8dZ51OK+gn4wr5ufPnkYwl88SCQ1fNOvRYWmHv/GDXv74fuwSNKGm3Q/ozytUj3dGZJP+37q0SqS+JNTADuvEfRfmf6/BoCWJlQIi5VEopuMaLPO1w9JnHo6ifucsYH4oSpcjC5doJheBLpZOR95rbO+ewX2BTbpxkyDk+ygXAUiismPK+3uaYTDFykHsNjHj2/+OK0orcgDQXslZ7bbxyfm9ryhBkNWW94z82uO8IMZ7VVzZZiWXhcHGtJha6qvpzyAx7/DbIIxDtRmrYP7PQxINNZyP2HmgIQOLV9PdxX4bLUm1OG3crHEVmWWt7eKtanZHyX/QSXuOkSZMSV9TveyAWQkAfMVDn+V76WUHj71JR3dHbmW0h3XnFAUj6f3SOVPR/9e3Z3Sl2t0CeSuG3UPZHLhuX2YXftsh+kFPdnIEM1sVlXCyEfGCkP1ICrMCk/0c7Ezh7nl4KYFBKVVJHjBlThPh/3JLn+gDbWnHhLR+AjDhqDGLAx+IzD0fx4alVMRnvHUPJcdGjSWAVQHvZpYg6/qfsvGvsgKnfytQLc9nq06jUZ3yt+ctdL3a2vnVzmdtF21joltGF0BL7Tyg77OYoHrq9JMj1pgoLxkevTMfGTnLr/TNmhO+MxccDOVEjzfeFobBxxfKC6aCiOQBwA1mnk4Kzoe9qcmi88jdBPQlJBStqFTDwQSUfFGPS9qPjxDs4MVegIYImzA5vJ9p45yGgWwAj8K8w3/FX+nO3p6ZtRRXGc+zVvGCWSg/cIjwOx8/EvjjL2FDXgIkPUD7cqRDtmyPBy8hjW4Tgw6ES9qhRvZ7yCr0RKtYduiMNk9DhFeqh9fyXhJsJM2i6krWSJJFEPZJxQBCm27YHARbR/QHUpssEQFAdUYNCBC4Ns2wSpDtyemDBdv5F1cksjpDgivJGuLyyPWweuqcHvjnOUQr4wtnoHDg+9KxXilxSX+zjkRUp26bgcj+RE0XtaZmF956srDFchzLFoUiNB9i68Ky29Eyg5ss3/1LqOPnPGDaWc2aPuouyjgUe1WKnj1Hwc1XuUMyxdz6JPaF4E116T8LZVXYINhlU7x7ePb7MqG/VEWmzrQ3IuCQAIJ6zCvdvZowdWPnBl8ey9gFIIib2BHUIKKEWfvrIAeFTca8+0pB//C2u48mlix+qZI1TKFa5b8FT1Qvgl62hSoIRdp4SRQBnW7R3G8R8EeOBzCiArBJYGAZAVbwqy0CTeIHW6xniOpWvb45AsY0yH9CDL+j3rvVTg5LiGYtJ9uJK02lxsEOCyIElZ5kiN1Eqd1EuDNEqTNEursOQBTs8v4HFIdVg7+OgxlS6z+AntV3q9oa3+TG90XThwC7nkE1PdEFVa8hFcFslcWSDy8sxa5//ZpAy/ycAWl44FIbJUSR3j0XS4daIlgM2z/Men8AXAAwvzRI67cpBiZVtVE0SpF2bnlKLktqyB6q5dzDZ5LCKIxxo+F8xrP+SlGyLTQaJ4rqArRymm4o20JkX1fgeoLh6e7i5X8ga8aPNwSp9o4pR9wCQsOEBM3KlGJrOqD+zbV3x/WWtjh18UVtuczWuB+96dWPnSH7cfMnBVcQ8Of3jPPA4iT6Awh/bbWPh0eLfbzrdD8f0SdbnX+uDUx7zNL5dRdPvrwo3n5ErKSiAAS87FUg4/qc6sfuMo0QIA+ORfrgAAPzzH//9d9t9ItL42GKDDAACBpvY1AroKSv3vQWMEymCrE2/NkvQNUjrKt/ee2/FbnTfHWUi8c+B47v6mQ+e5i8PD+d18daqxTWcJ+8rUTjY38+xz1vrGe3yJ4YEd7JfnxTubG8dLPL+y1w5Z1Qm0eWzg3ymuzluv49F2nQZDhyBzjjve0dZ2k2c3FMrGW9p+Vrcq9jfefTvRjZXl4/0f12xa353qc/bOyOROdTC2L7nnFJs+uRZvlhub5Ho7NHnd9Pv5NeLTPG8yW0jV+qTHPIaqyLnd2soqLw7k30bFgjKKh8WWqiMU+UDYhEa4hETxoDhRfOxYeUfqmSYt0zbphPsEEGDVPQqlSQgAmIFhKZARQGsDR2E6AAtA9SZGC70Zs50teb6lSHtLalz1lpV7OlWztPd1OHTk2oltm7acsfHz8hnTpsehJcae9f77HFilcFuBvaJxY/eU5HUfrDtxof4apfAPrVicEcvfdN7nZRJT5olT2+6BbShzAiISINGIzNbcNVm3ZnAkzmPkJSsEHy1epXg99Q+dNOHh7ap63QEmn8qRBvXp0GXYhHPdGJO8OGV8Ktoa) format('woff2');
	unicode-range: U+0370-03FF;
}
/* vietnamese */
@font-face {
	font-family: 'Roboto';
	font-style: normal;
	font-weight: 400;
	src: local('Roboto'), local('Roboto-Regular'), /*savepage-url=https://fonts.gstatic.com/s/roboto/v19/KFOmCnqEu92Fr1Mu7WxKKTU1Kvnz.woff2*/ url(data:font/woff2;base64,d09GMgABAAAAAA04AA4AAAAAHeQAAAzjAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGi4bhU4cNgZgAIFMEQwKplChTAuCDgABNgIkA4QYBCAFgnQHIBtbGTOjdouTiovsv0owp9iaHfQFFGHSWOudV7e9tUY2e+yBOZblJ+aIWjoTBxQoCTOPdCC8gxFmaNvYp9P/PeZ4+H+7/9rnFh+mPqOnNqKxQsBBZAQJmP6Vjxgk3Y5GyFquH2ib/447QEyswOgfYidgF8MKrBr7+1bgjMj1bQ2LQlxGEI3NY4D2uBKb74d7Xa7d9zqFQyIUQhXhkrC37Trzg0zqNHqptbsGEAClzP5/a6ndvZl59+94grxvEpWSMG2EYVcldv/hwpSJbIWrqiRAWeFQIkpX6xrhVXwbur8XulwT/4g9inRHGmeToZA0gAmCZJ4SxGoNECGhj75AoGszN9jlVwZGwuD+qmIuDB4uTs+FwRPJpfkwgEH7RyAef7A4H2oEQi4h1JK9MKJExCR1ATALoH0IANoXANpLgPZTDuUwoD2dN5kGhLF31ivoPgD+9vEiQxZdXuGDRbPYgA3b5389D+TB4XyvZTq78jKTFBBUVovCzVnzG8V5yCb5/AJF/EIlk1/kfy/z0NhmZ6MLHZVdZof658pU1C9kC29jXNZUlcyOatawRqJCydAuO2oIQdQ3ydwR571rOaqysPDIm1xzKxuynCx3L19QnVPmjdWJSPHuWoNYZEdm22R8wAOhLF4pv3pqV/0hLLi4CLoLJZwBD93hDxZDPgLk5xY4XbpQMHYEFVbtFj8id/2NN2pJGSgiP3k3OTg5vnih/UMkhDqZnLyfHOKfol0Fv79zz3Ea7lxH0Lftu5oSyAO9qJoah1YzYA2IabDaHzNg3jP5WNkPNngmB5SDYJ1nckg5Dp7zL17o7JXnodRPsW1NgVcB0QcuAGkPyG+gOBukUwDF05bxPKM7d4pzEqsKtDH9m9utJ+9zl0ClMYVGsYNhOl1qT6XGKNS+/DSm1stP0KiepTmgyKhNUTBZjEyNqxxZqcFk5WSVpmXlG95U5VNZORK5d8jlRG7DXgrLLGhwNb8phURRTcuMhlDMs0CiqxP2s54l1/9FmHbVTXXX2Xe0qr6G0F+S1NdDXfGPbRqp6BuhTenaGeoJTjbRKEWZ1z1EedjuZP9D7hcg98P+S3Vr2m2EbxOLjJS2K0vELRVd2nI3EEyBBt/cq0ems05L2WL0hX9MNKvYjanm0iVctPlq+ou6SD+X85zFqrsQzkVn/fmtWyb+V6KLX7xyjKRIr5aM4jxtEokIYalCho8E9Q4vRMdi0WOpif9j91k407tNo9lblqM8QfUqM+xtUdPyCmZPm3C5vcvoyXHiQqIVm9S7RhRLLVWlO3voxFeTlfVEmuLYRYrtmDuqPaEzi5KM1kxrqB7SukxYGFTE/uHnasSbuqnmStLr/vveta+mbbfdZV4KlxT6aX0+UYOlQMXwROOJd2FYcNCyce61zJJPdYRtXkU+lbHtR3LtdK7MOXrvYakFfeY89WlsNvFKrYYt9CJK/W273YmiF66Qa625r0feEZJ8OHA+TLtGVV+d6C2OvYADyfXsz/9x3mRFQMRFaHoiEMnytYzyN1IVD6ZQhN2MsFFwObcqpZkn1l/CF/Uv4Utb1d98WQQ3wlQFdmtsDIh4z/iFixVQcRCI+EEI7V57ucONvIBvoCx2OyCnss93eUqu27BZcrHD49J+BD2jtGfnWSXCp8d/nNcWCE/IEn66dg6/DacQb8jpvdFEkHt3918f37hpambDxvEbkHaRd/26TmqXzkGd0wQ3wbF6oHfXNAW02esTiNEdMP3TCatu2DZ5cnvbWhD+vz0quaS4KLUhlSUMK+4XxZTvuSBEZ973GcVQP/JjRuL0Wbe7IpdjSpJhhdG3F/QD+/NrbARAgnV1SX6TeO9L66+7SbsHzjTioWPvwgc3NA+cg0DFm4q67/WnVv68uabmwjNlNOgWOrFV1JrSFGLlWRjTdFah5HjhwLR8t7Ysmd80DFECJ8Gytlke9794HjcX4o0nVv263dJxi2r5W2a88vvdtu6bVMufAont89f47XM3l7cu3uS3LFwDq+rc3NXnNm4/CqLvBXs+H73IGFMUOOHwqpE78o9Y0+ZemsxNfR/pfsUVWQF6aaZ5aob046Aohrs9oz7wF53be+4OpGxcr4soMSAxKBjc6+gdE9HUUUInIYTUPqdm1lMWel3h9AO/wENy/Ya3w8McUmJt/UIQySVpPXzahxJOK+97YoMOHFdHYrrhlT/vtnacaWphkt1Tq/sNTlv5tqxVGef9utPceVvW6sfu+9tXe9v8dQF1x+K1ttYUn/LXcalYMqm5Z+I8fvaQVC2ID6LnlX3RR/5GQj92bu/unkAZOJozJS9QnTfira/F4oJF2mvJob61vWuHpD9NFYJZw5Wq2W7puZ7KAWZAsB8zOIgF1o9uclc2JP61Lcl1g9Zf45Y8Um5lG9ughOOop/OPg7cZmIquweC1GIMWHG8cQMruG8qIcg0KCZQEJiwfeobHstjatmpgFdCJOGehqKn5cK2cQftgFz9WsJtt3LXhgqV0UDd6OqhHWlocXzsuttLDPo4S2sEm7czS2rHdfkR6xMHCSP4ysnw94OE6kQCXvlaOuHcfD2ovRtfqebdjCdUNMX7Ofp47pbzJMfzla//9UHf4oRuH7WmXxzSlsWmBCu6cZI+/eQxjHRetcTxLzrWhmaUM+HcOXzFZVFc/WczutvVz9WMEuNgdpQLPD/VUpTlPksI6e4K8rEKdvEkyYGa0iTNZ1ti0Ks/pt/Nz92Wdox2f4s/JYjqV+/+r/944XCmAk8OwqP6nGBwblt89OHDw/5H9BgvjtnD0ztOr+2Q8dwQxHfr56/72Z3n4yv76VOEScvfeT0jGgAGoQBM+IvaMSfMhCKEUS8E3xI4pcD4gWKo2kUClN8D83ZWAhr1GmIKG9QBV04MjQR9fgl7Wt0zow3K4ZupzycIAjoZmpkHeY2EIy0FKN+Se27NVoAHHwk7iblaCRr5QYQTPw0wYQUf7dWVd07puf2N4vUK2QY+VaWLcDNWEyTmr1F6UKGeV6guuJFFTfhAkpSxbke8WNEyAhAsaVgdUTQs6gzm+BK14LBN6sCacMvVivwl96A6/TP1EsjCAnjDJNGA/Ng3DqQJq70pk7YC4c295iY9gxbti8CP5sjSpzPr7a9zFkyxXR8qBB+lScnahBigV8rEExLighq8Y5+2o2hKPwmJQacl90TMyfxbulOp6iJH586BRquthSjbCqQTC9RAj7/wiHCRGQB8hY0SleC5ulaVUZSumu6v3Irh6z24xLqQRTzHOe1G1kQdhO740ehzLtElBASiCj+SLlDD8NWDyJExKdM2fFVPKgzegSjW5/GJ5SXX0T227q/eCUb1npxgXKBIuxnl3fmoj14M5tjR69G0WEe/5c4yp8ybAKGQPVcgutNpURiG7mALxj5IcfRNEIWu0BANb5hkW68TUxzpq10dJA89l6znlcY9AMa5k4cns53uVHPlX147KmesaP2B05tOlFbi8N93PJziUCrueeu46kj8Hz5reB3Lcx1rI8/S4coe83tSZ69pf2QUOsF1rUGv8tUfgSkplI+90RWQxlpnXvg3DLHBsE/4wwQCgEkMGTj2l+/Yl/+ee0aHgnfPWl3IiQgOI1Oeqz6Yfok3pYwORofyVl4GRn4E4hpEjIH7BjDNvfa0ucA0h5SqrECnvuBSrJAwYmzyYQwJ5L6SvrE91rZZGC88aLF/qoYhkSyh52Tei3EHyZlNnrms/2SG7BA+mjl3LNRMS28HqKQ32JbxpOPbwhrIGx+8x69YqzQP65jgB8hKuEPKSn3rFh1Z5aZuVI76k4gCpkNKBeBXecKeSYHQMwDrJmcPOnsWpnClQfJzeVSaU9UT1n6t9ifNOYRsOCKRls5bl78ScosvhWr0G+HlfeSMAuyL80HfN07soFnUJOhIIqifOSIPONR44cy2uFt6Oe7X50gBxslEqzqaV8639zOzaauVrgT+nyWWjsFTlnDzek1OHM67Y9U77dEqYbK/3MSqpFm+Wceq7kV3twemWsJ3480Fzi6ztHC+0GI/Td3H67KxfR8+q6Tfy+0PuGll4tD/K7gk1Pa6mZ9UtWyUHh0QJOEScL4HdsIVlD4wdY1tCgrzdolboQ90wfsOMmql3GjREjRZHCmvLIvJF9FtKwueD+icoovdSIq7lorGvKRyBV3WwXJGK26UhScuRBGoPyUROAe4i00LfXVrSeGzFzLViDq2w0pxKrRyNX29zg9C7oLCqODszq1TXysJyV6zLKtCJc9Mr+uWn0k3PksptH97ALmFmuh2ZXlyezZxGDy9IydRS9EycWcZN9qrRxcUl2Xb+cCp9vLW98+Ar2zddhVmOAxr5yEIn8x2pMOlOs3BlpnqaT6cXFGfOzs1OTc+XZpaUThjo580IjmCYDf2WLFCQnLQt) format('woff2');
	unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
}
/* latin-ext */
@font-face {
	font-family: 'Roboto';
	font-style: normal;
	font-weight: 400;
	src: local('Roboto'), local('Roboto-Regular'), /*savepage-url=https://fonts.gstatic.com/s/roboto/v19/KFOmCnqEu92Fr1Mu7GxKKTU1Kvnz.woff2*/ url(data:font/woff2;base64,d09GMgABAAAAAB9YAA4AAAAAQtQAAB8CAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGkAbjgwcgTAGYACHSBEMCts8zA4Lg3oAATYCJAOHcAQgBYJ0ByAbOzhVRoWNA4Ab9FhRlAxGMPj/0wInMoRVH9rpfJ0o0MQ6Q/sePe/4HhTAomQRdA6OKFBFs6I66mkaH/Th+wd8tz1/C5c/XLsXr5JVU2oMDFEZVvFJtx2SVLIGz6+teX+DOHqBk17SJHJhLTpFeiVaDgGjET1BAZFIXSpFVnsxoFW0T8xGMTAKvB2ezv0LhK5bXvuJQ9QF80vi+b41Pu/rqer5etcvx8gFFQmkANBFsTJIEo+PMBEyEmk6ANTJBBalqeefBgHwCNmA3+Diw58Ct/1mVQUk9GxI0XtzmKstiOjeZ/9IZzkjWYYZwfnCWFThErD71e4BguUJyAlb90UTgh6oZBTYvico8/c203b/wvhrvDeTtYo9m79nNmugDfU0PRdVOhfV3lut6OsfrA4kOZpdm3Vn0smkM68MOl1Qdog7QFAYOoQKsaQqqVICTp+iSlOl6oHKFHUT6J2fmXlKK7sYcwgjjFmM0ZWU76gtNTIz2WIbru8u4oEA/spRxzcQCCjIAAAAExQY4LBGwGWLwJ4LglgVCKpNg4CAAiAFQAoEAAEgCRNL0B2v9wyE6gNZGalQfSQ5LgWqTwZlpUEVHQBuNiulPJSRBnEghNJjCQVJeCrK6AuYMLBod1sJmHCRKFmqNB8wcHt99tagKfQFffu4CBzXBxT1BB0Qf3w9dquwqyTPvShNm3dl9qXxp6YJT+6brxnURr/io6066am+TkU2TVWkT3u4avW+SPpYnpxX1W7cp1Phm/XlJMSsf9jmeetY9JWnJryc26bd8ab6PFdDdsN3NTphUn2zZdSzScVmPT98L5TKDTc4pMjJajQUGPYqjwEVW+u40KRXnDPed+XlAtDzkithk4c7eFabpL5Cudm79EYH/1vyHmFV4ilR78Fxj6vcujb/Fz4RgMSitLHOh5hyqa2Pufa572/L2V5QuGdvSVlF9T5+XUNTS2tbe0dn14Hugz2CQ0eOnRgcHj0/dunKtRs3b92+c/fe/QcPHz1/+Xri7eT7Dx8/TX3+8jW94BsIbB1HgDtGLsJDU2DudykL+cklABgQKwWilzQsASyJSgpSAbpOBwFNBeRKUi7FTpFALOUviKsykA7DWNhue6gMBWY1YnAjzx5FzWKjbw7NhJjtkgLZCfD3r5t2F+Q10aTTQIG+FoctAylUSWC3JcBokps6kg0DVKKWGobS5uAD9Dr0chk/iYKeoNdvTlg1aV43erJp3mC0jE35Fgevmv2TyUi5PEClPhOTCplHpjjZIEAUJtQRYAzGnLN72pmTDycFGdDxE7/wG9MgoCPNP7JtQ6PvzF4MgFHT5kC0yhm06+K/ncObbdaDOkASX1a43OjjAttA+zQT/wPxe80AG6kDgM5tATY22uKQOiP2+pMEyH50kg1wZosBJpEl8tHP7JiEE4/4JSoJ2ZKtKUpJGiLIp4gonsEZIWeieVXz333R/MVms5t16nJIrzNniUQAAZGLQgwyJ2axjmcCw0tinFKfnuDvMctI84pm9j5rfg02aR0gdPpyRckgSgRR/Iq7BvBW5K07+v8f72ksnwYA/pf988Oz3UM7+JCnPx7OPDDH409n87ijzjqyBQHguJ8C2EuIlQD17aeV9lp/y1Dcp4mvVp16DRo1abbQopv3M7nYEv0GDBsx6px4CRIleeyJp545q8+gIT8QSAoAqAUAADB8XVEWQDIQuf04iAUij8EJEAVOH2ODTAwwrGUyGyMuQOYYTQB7gJhxHIxKmVhjcAPExmgL4AOxM9oDFgJxMLoBWoC4Gz0Ai4F4Gv0AB4AEGqMAh4HwjCsA/UCijXGAYSDxXEIcpTERyBbA+cRWMJbKpAjDViYlGHYBUm9sANgD6UkIltlKPoHxLkB+G6cBiUBmjCIAj4GaZVQBnAVKlVPDGJQpDoazTIVguAQ0GQPgA2heMV4FMAmaLxlfD3gOmr8YfwUwAZq/cX9jfJLZ5zACZPabGD9k9lsYgQH2O9wHGEEy+0OM7zL7c4zQAPsL45cBYcD+mvsGI0LWPj0JohBQEM+IRMgJAWYA9JNr+MgSHJC+BGLHgV4FPzj2Lb/Uri/qtEhFyggl/DaUtvOP9AnyfV+7jTIOd0JioEAfENFEqly0AfaJCPj4JRPxqwIBoWUADOaI6Vy95V5qo4vQtQON/YCHJAkVGLQPnVxqJwY99dDHoesfeeSFSgJBaErycSQXlk4KeL6bNl4pQ+kXXlP7mZAe1YlKwHUCuncLGDNyBDir3vdrK9rM5QqWSm3KZdh6vMO+ML+2u0xj5NOdBA1ZWKoujwRLuOyLP0DCEe15Y5KeooMLNrYOz3JyPqQ8p3YHQLc6aWFYvOR0N7ztnVOqWeXkELHV0+KXAuiTOvhW4BN0Bxab22dUaXz+4rQpzT3XigPeEdtFnUOOSR6XAoe9/AqFq3FK7Rw5zkJm9pWl1G2fdyKaDpNhiVLNc8Yb1Bz9wTShZjX5eTWWuBPSusw73QX9fM8Jqtuh2XdL9Gj6SVO1CLTnJUVRrt39PMfWpJdlQHUTuRTi7PQTj52GIzIHPHEj4hvAS6ScnlMPti/oCpdfe2BlPjTC0TNpUFOrFO8v12qWyGnydB36UfqwDhG6C81x/TVdKIRuZXy4VdQqCk6y0IpDmEoKg3QAL7c+1XCaTsFALMXQJUq9bLx1uHeGDkdkN9PDMCrH2M/R1UkRu7oW+FlFryJMm+1UBZjnFpipeFKbUuLoqszcLCcFHx0skeP3fqgyCXtU46HWf8k/8d/tf6X6nNt+HD+0Eat1E3xL54dWWDbzPLNIffXCghO2rQot+MHMPVWmmsovWLI2nvz08wDyD2WXKJrVKl8YI04lt0kvfqaDqPC74VKU81k3sRio5Mx5BP5diA4h29IAz+FSjAJT4SpJD+EuGlVi8/sVdugujQ8lhTFNxt+SKD7WQdrQOP1DQ7swEJ4h78YI4jrq5f7URvqG/AEqrDgYgD8aNL0xmBZoRSa3H2EUPVQ22IhDyeHaGbV0I7ufER3oFCn9Ck7DjTVaEQLmfvPe4gjSdIgz6TNqwX9bIajVTMt353AcLv8KkWktvfBLbbNCVQBRxMV52y56yMANLExnoCpP47Xq9WQrsLkRoI+I9DxZWdTKe/DCIsk0TIhAsaPQOCwG/rX9WzwaSupx83lsRSQsDB9yKisrAEBCPaa1EprVFyYDsipETh253FM9qu8o+Oh5a/ZEdXRMX+atVLVloRFibUIn1RTIAoDx9JCfm8hCJQ7Ubpr9X+briz/7Ng7/ZypSqpSu91YTwJEYvhU0rFqgWOstUTLBdlTrM4gB7D2li0jhGD+juwekFeEwi2EYoCFkidODCdSBgjkL4V2tIskmFKwoV/aeTCHm5IkQOdQop37Nv0gSf6Mn+UvWeUBr5xAPUPgvmy1FJv4j+Sqtb1cyTV1YpuZ/XKVWvD8wt0wVFz2bLU/cMFaB6kf+sjCoCfnz2lz0Qg9cEGEgqLdoziU2IWO9w4veTTU8TauM7hTrCElYKoXT01TRvHayEHp7cYOfLoeEh3mAgRQHB2goFEl6iAc1uQYbWpXYRK/bwBV0Wbj+3G2JL9dKJNYhW/SLylSLHC55aEjNc55aLlElTgcD7/cR6KZrGSh152/2IVA+eMlWkJMrlIWszjgtIkWai8L4Jti7TF0g5ThgP4Y8us2WAQWfU6k4tFGy8xYQ5j1dIiftKCA8vGiQt2sUDxaoCU4pF7w5vNouSAkT46wBOQAdFbM8ViB0hAp1YaLgQnJ0aEWqyzP+4eS724yEqf8GVdzxusY6nh4ny/GmoXKYQtZkhjRbGBS03K4828VLwF1otAa95owNZ3UctkQET2pk8aQ3vDF5axH002/Sa6K1y/C13ujvNaopVugjXwjZ0l1KymnBx6j3rdyuVHfpx/J/M9U7vwfno+/t2RFwKvXnHtr7n/fPvmvj6Cs4asMD/zw19Yo4hkZ1RgTtwuOCkcMqH3+0w2vbieorzd8fKKtVVgUX95pLNPe8+JtSfjEwq7u1qXHuTHa68P3I34frB6t5jqADjI/2rYi01arhfMSVWkuDliEibamweuzRd5Yo/PFGYZxgCdWwlv6QzweMj0pbL99ci0WD22O2r4Hx0c7Wnc8x8Orvc0uiTBRMsLRWqJCxrW7KScovlN5ojvdbsnUGRmWA/W+P2X2pDQquLk7NjPDNBgMNjxosFfZnPMoYwNqfHiBMjgnWxa7/fnf97tTD8/2b88oKMkoawE6oNh+ae/O/Na/tvPpmUY5Bk+ErJO2faEnpFQ1AVUmgMdz8jf+6umKOfIzGi32lfDerFHgcbctvp+HN6Oh1oyfX8GtCwirwLRm4VrRErE2AULWXaJLLSTeK7SQ7D4xStxcZjp8ltKuKp46fKVLfUqZvaAp7D2WoGax4LrNz9AJ/9l388a28kv0c06Ak2ivFE7OL2uvg3PYqLDBAUN435ydXqg4wP3lQ8TYz+3D9wI62RnxlK/yPVyOUHHa0Ibor4RDLzdcVtIyC3a0DOwarmnAd2bfKs+C2mvkmGP19/ML8rzYe7i1j93AZjENZ7Y9qhDx9cvZebfdIRxcy3Hobz8bU+uYcUKD9bJQv/Tm7R3NIRvfFgLKSoId5WF85TTmNlaXfo8wQ9CP25sZ797RiigX6tcJ/Fndgvd+OL7rX9EWOfO2PQthkV9Zj64FG8706ov8Yc0S1m7PRk+1HR/MHQO/E0aAVKclJIegQlwWnjMat4DWLl3tpTZ/L9YPlvhYE7/PScXlUHHguODMILM6JKc0QC+khDV/dtxWTdpduAK3azG93cstmPtwV6+6hSNxTHFVIOOjXPVW9j0j9X++M+s7Gb1MVggmd4plpmJ3Tk+Y/TzJPVp7uFDjyP/pXIZew5/BkPWCxP+s0bJh5sGfb5QklmtdDWt3hrd3RuQ5mdukemDOFzLH0yk6mEk0ZsYJcAojS/zMxb2A0LZ9ybYfgjZ9TF6sfx3CXRcWn1yKYRmypX1ru2tm3psLPJ8rPcnucat8F1gxisOdQDDgBVWwHUzkXhZeQlZiQ4I0J0PeiCcEEIYKE2sJzmoYrU1He2bExQPEo94S12mLTsh8WS2ztXRaZNRekWVqpFPoA4dfMdbDxVTcEr3EMedg8NL76LoLcfRuwmrn0tWvKDp77dat5HQhYE0q2blw4lpWUjIrAQJIJV3pHJl8ft69hF3LBOspl5G7ct5fYvz0uQg3qNv15tKvorpzp/9K1634/yS+5I2c6IxQ/2nqzoLDnzrnD7XcKdvXdxNzMskal47Y1QMg9Sm0Kre5TPwMTbS2PdGfOhc0B3imO6AcneuaenClbsM2Oiw5L8rAwiTJhXdwV5J/pUfP8oLBgNyy9O1BIGr47NiMqxxXMWm1LPlY0VmdnaWsnuLHCkuW0CdVQ2rZdNMKEWXZKSs5599E9DPcLT307f9OJzyLkCuBY/9/bFgpZR7kVcpkWcjSLClYz96R0wDzZoXnleC8jOzR/VM3iaaz0Ax1HqQpWZKKNUOyclbP0jMZgU7G+0idPLEDFs1p1O5hhmFxqKjyIjmHpkfwUmL1Uri9qB6xRPPlLvxwaR6q4S1bUJRaRJ/u4qxgv+LVCXOtfrE4HmTI8i0u8ZxyG4ARukuWWd6WlksJAkPy685oPpsFVDhU48QiVVc/O9EUGTIQ7fK10U3x3Reu/vF4i/cW2MV9WFIg45IWl9U6HqzT1lT7OydEJWOVtsF4kXuT2TxQZuUcMgx5GFPYtItiWR90PHwelxEGRUWVxfrPMRVNHT6X0OJv6L/Hudk+6eI2JzDvMnu6VhvpMj3dLc/mZS9MrLPlWSu9fZJH8lQF2Ub5qNIvMskHG3apaO/6tBcBXbcPMk91F8RFSrqVw3NFgtW1t9p/HeXsjg4TdyuCYg0tlJ9UHeWnu0/ddd/fdjDs6457mMhppX9Z7Swj34eunrxOOyfDwnSITrxSvEuKWtByCGej56u3ZSflgoOFhwwAqnvCAzsO7ps2zjIeDDxHUq92Ls5dSLufXPRZWsP94SOr1ggkol3owAzZ/JzE9zE8pRkttnrRxAMpDcXzdjaO87W2qUt/4AJ71wkdJAKbd3jnfrOcnXe21OO+ay4KbyckibxZuW2IiYGVxpaozuw+PnDglY1LNrbNNE2/s/Qvsd238yE/bihp13CHby0X9QdDes9ypE6CoXLjB/MTDepxkaSxZ7pT2RYz6ovawZbfWBaM94XyPN0Yx6xxct/fZKh1rbRm4S5e5Z6pz78ZzGNC6LlJtbTMEPXnpkgcOduYRvqRdRUOZpPXgYodOgKJSSciiLtrghovwycHKl4GqrUwPV2JvFNtKdh34UFMTwAzPKeqzYy7idYFvZ2jUFaUza8fsvgMV0r8bPc3dZS62NCa7UeSKmeuuAfYRdt6TMZs+SeF4Yb9ycsfab+YLLAGfjasZkry8urYLwbWBv3Iw+oV2Q8mVth68K75f9x6trW0jcRKQJFPxjmw3IQ466i6pQDmQW9o5MI7TMc2Zs3iT1AGtrz4PpOf/0ivf+ON2wXpce/NnIusys61+N68XSrGEy9CMzy9GV3Sx5kmrbs5rACaxHySrywfqB/C032TToa4UVD7QTv1HftVveUk+DP+9vb65lralNLPS3MPeytzezgKIZ8GU/SvUWZ3tsTHVxxdKQ6D1c9B6azcFewclEwe7YGNTJTM7J7Blf2Kou6W9oo3UOieZ0JDwCHcvNB7iwPzlg9QNaN+5R3iLD6jPrTXdyExZV+anmxmlpa0139rBEPrjolPXqCPVaOSuUQU2TznHBFpbBAVYmAf4WtkG+bmbiix1FunqTenqszBn+C6q3OlOGt6h4h7MpHJ8eJCel48tNZxx7mvKLzx7h476joWOanx1em96GTD1u+VDOdTohT7EfnG+1zqlHBIlTjl2olpJchtRfLLj576+P3/24X+H+mK2ZCdE5eNRjOPhmz1IcgMQlIncr3pcxpf7ZnDJ69o3/W/uVt0t2x48vd+XVkTvs/lispBXeotVeqlswZHKjDXT8jSVukREPe+at57IQpQ7wfnxrmI5/ur0KLlgKQTjo/Vjk32sY0Xkvcwxt6dp/fm7y7G44opCoovgZt58G3x/IPZ+blW/5MD1qqm4pyeDnr4MzubEP52sutRKNVUVDN2q/tD7U0H3j6htt0rXGTiZPpCqO3Dk+wI7qD2QOB6jwmEoX4+9zrBUVh1bOmTG/PvA3wLMb+b2tK3RzkYIbbeGZsQDh4Z57Vll3IdCpyWr4lPB5p75CaKB5qEDBM023H6T0crRSPfS+Kpy6O12y38mK4e01QYrXyZ/y3KYUSmNLNevWBbihZC+cA9wa/XEohaFyaW+clkXDPlVTIe+Hbk/uVDquXwiNv3ahH3JimEOcnyW8ZlvYipGfD6wayKJJdeLrMVbehxTmEK/pB0GrX5cKGSN/M44DFp9+Zuvc4acl5VYDuMvq16x6RkGJ4R75BGfXqJ1STi2h10rSW6PCkcodWbqI+0LQkwr+LD6WFdrr+66yr96V+Wt/PuL48xXU/Db5eAtFrqvo6Y8t625EvBwUXHw+sS7FWppgqRIYT0c2r/977oKpE5s4HJ6Z3Q0Ibog8V70ctfwLxtn+xt7GCRQ1zBcn+hiEUu0JXzcNsx+ApgczocrYagpZ5y5fSJj2zXnmEBL02A/K4tAH3PLED9XUxFtPd1Pega4/ZQqHF+Y7K8dTaXlzRfYBSDgXE1/Zf/WLfuIOdUzf/any+NnxO2Ff/J0BmIvbkyrbW5SN85X0t0UYn9Du4nzmtPHuVV5y/G/2vjaK46v1/oT6J2onPBe77vs71Xe613zz7gxQ6AzLjCg7bzq/pidyQeSjKR5xWHkHGBmK/aS6bix7EPb8gEx7/ea9kmkspvGrFJCZNPxxHwm3/eM+cpkNXJRxAZ53aRJsj9q7yr3v7h+QwlC/Dbvo3TXwxxuzSqeveMJsnd2lOPKN/yeWFU7sCfKxG6OrhMwoAAQw9wKAkpYVaCT6IRGR1SbTSR679FMnMuSBUyYPEn5qLn7SjggpRoRGMQ0owmW1F1zkOrqWpRJ2WVTPdINuKpgC9y8zPJmxe0WwphWXxsHVAPC2242SxAjJkrintA8BH+nJBbtA8wW2hWW5pFVsYequxA+VDcdUSbxF4xJHE84L2p13A6H5u4LWQ62v2ymeqQFcM3gQ+fl3oElMZlvOZPsSqdGZWxTNGIn/EW816I4SzauDsqjmawlcQdcPUw095USByDrB+SY8Ppewie0ax3fonUJD+qx90GkKEMQtKfoZCqISkFcsymWovPAZb7uCNKEl7Ljl6SryB+vBBliyiCmkp47KxUenZfUozPDW9NEGD5eg4ZnWbN0j5z+f4R4xPr/40wutcq3WDEDuWGgKbxc6yQZxM1lS3OLYjhRJvt7kfgHLl4rCfLExERshdmuycOUjpHuUY5NSLOVIh+6T1JiTXy8AaYwKs0eXJzT4bgAIUac03/9EBX6qxPn8N8xVjAt7MrmVl0/8KiuKqNMyu5418xSB4AbsX6azsvqox4m2FTJSDcHYhOCf1ZUTCa+WZznGKnapptlL+hmPsIEbhrCQXgp792CUs5bf7hA8zlmx530d93EYJaoJndTau4+hgO4+YjpnvGIne7ptb90nzjGivh4AoJy0DUjbAFUD/Hv/kqQIccsu+4xaGyQzVoQzTXJyNUGUz2SBpSLM9JNazhWH8XtGhHMiem4EHtmKWcDMQspE8lmVirnHGrnqmPBIVWTb6rpOAUqV0U1zjegPHAaqqpfAC0wwZm4Ldd1l8lG0+oGpm9l3T9y48A3WwHYv3OddP86BQv8W33FeDS/OhFnUNadHJAlYY9o2Ha+foZqzbYiDcOFGFTUrVglFW9bsGJtP7YOtnOvbAp6GWZiHoVLUtQ8ZJjYbIcAMyLR0hW4um7NJ+1r8kSuBdSBobGXWVkfsr2dUfJ8Lnd33S6TumNtmVd39mDh2D2TE6JK9R70TdoB9NHdXtT0nj8pRjjPjyKGbF+6u25Xle5IdGlQtPcmsMGupR7swXjHsu0zonFd35FUuEH8/3e5h51ZGhtgiNcuANrj3W6dAGrDbrT4Qnj9dh2R1PlxTRD/7lqTxGFXXTduz9y+bdaxPOtasKYPhyT2MLeS7NjpOLEm+1cLFB4em3sPs7RRKme3XEedYXM8sz3ZDF++U0kxgQa5PPXdVClQFnsFFAm6Ogk+JEgQ3zGwD9nPuucHQJ3WA8cRrUD4kMAMAT4kqpa3kqLT5Y3TsWZNtgTzpbA1WRbdrToxOemOxFlUhHagX4jD1A0TfEh4LRBgs6nK8vahGHQ55mtGZOzWoja5nTJ8eOEexocTLm/5f5H8/3e/tpipmYNtAYnSo9zb1uTDy8QZ1WDAu9UyoskxOdcrYJTq3HXkzdge9CR18znr9kky7A5icd0MQ5am3XYphgiLgfl+tzIfGP9NQBagn8KWyofHJpg6py1DbEOfLAfaEUvy/Fm1B31feNFnEAve7XmvyblpEqxZTa2xZwtbG8pion9pmYSo2iPjWfnjl04Fpj9IPjDXLat+pLRGC6wsve1tebfj/98ItIBjtBU3TuplO+qiTlFPijou0w+AKADdOKt++MQKPqTjCCnnWISgL48/AUDpnpqt4tkbECz6XVzlBAAw/BOTEQCMx/+33zx/xiVMTjIBEqAAAAQQSz2kAAk3F/7fHWztzXgztAKxbgOZBoY19ba0tpfqqALmUm9DZ9rQR3UMQycqmWXT+5z3oDmj419TZ1tT77VXH98aZdomCnYVi+sKWfqYZdt+dGo9mwq1skyE13ZRtVhEbcarRAWZUpiJtVoyKMyU/TsN+ZWsoPYKG8cUFFvU4JOgJ8mbNMzfLlDefli+LRe3ddozcmzMtKx9w/rc2dbMAd6VJ6zOieoaGvltX2WqKR86Yy6dztdYeYozo75zRQzhetFbxFWJsN6roL7IGzTR0eKI3oZ1bIUKt2Tbxv3ZanRbP+Yx20ZtHDMiR6gt8CcS/ljTkzhXq7AZRGvu1XayRnu/ozXnK+h7tErHvMrCblcR7UNruyJx0BC+VSgax9Tmj7qOIEbfuZ7VKNPBX1/bmnqFWlnCymC26yRY5/RWEbXSI7VYhGDlsJ51EqwdVlc0DNeJ/F58QZfQYTTJ3RjNrEyt6Z4ds9KJ7Z7C7MBcIT+w6qLy1pAppTnq4IMHj1z7uctWLy7ZF3PvrNNrhnz4GuQQMmNLCqAx2TxybFFYcz8gCbgRyDTkb4MI8g9ZhUsA8hq+FCC3viUAmYWjgMIR4dI3EmYJlx5cJuEchD3+5ZINoiqVgj9szwACYAZFlAVigAAAZAAKlB4mCABxxKOB0CUAREJNI5BCpEZBBsnltNPldDQGDKzTmFBXWyiDQL0c/lGD1tdKkiBRFjYzJkyJsLn4xzQlVdzobtLEMIrZdVBqk38zzaym42RycYY14sQyMul/RANlgSs/wWqpVijRMpIhUxJN642MFpjjWDhIb05MJcMn6J6fJR2XMQlrc0ZWSOf8GIniGEkswyPjdJIYj9Jg1Ms35smNAyfeAjJqOMZ0EzRs9kyULBY=) format('woff2');
	unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
	font-family: 'Roboto';
	font-style: normal;
	font-weight: 400;
	src: local('Roboto'), local('Roboto-Regular'), /*savepage-url=https://fonts.gstatic.com/s/roboto/v19/KFOmCnqEu92Fr1Mu4mxKKTU1Kg.woff2*/ url(data:font/woff2;base64,d09GMgABAAAAACsIAA4AAAAAVHAAACqxAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGmQbmWQcg3wGYACHbBEMCvAE2SYLg3IAATYCJAOHYAQgBYJ0ByAbL0azoqzvUpziv07gxlB4DbUXLomlMJ2KzM681XQRJRADNl/GTXHdO/QLHl1szuTaToSWEZLMEjzfn7xzlXyzfbWxlrGnkXNAc7tfMaIGYhSgYBQtLSlRLdGjQnLQo0toGfSGSpSiEmXRYtBmHOm0ZmTJMKO1Hohqbu+qB1rtKnYuQE6iAxZMs0o6KWkfoWjv+++olO3AP9Pz7LzqbyIwEBpQr8EBzu2bVJtA58gmjph8C7KhpijuvXb/c1pwSYSuq6zR3aY/jEh4VHt+Ts0ogjsehf3/15W2uvNXtuRV4JtY5oS36MKdzxn7pExRpaikJ2kV6c/fwYDG5nFAHtPMrmPAnQlMACsgrgi5ShdXoZLLlARFly51lxahKJvoqGaE0YqmlGWpfk+OtbSOoukdc1hrEEZCYJRRPGi7j8/+6JKgsACwMRQGfGQhlFlDhAiBiBYNESsWgoQEkSQJIkUaRK0GKP2+QCBQwAhgDARIECEQYBgBsjBrrq1pCMf2CCF6wLF9iE7ucOxAO38vOAYDiP8QSAfsRfQCPJCABAMFZAz1JkQRDLBuulrFqlgSSTfLSJkt22UtaWYWFGfZzsfy2SyZNbNXjpoPOysFezG378XCVi+WixTZolkFyi0MDa9iJc8eU7bLSEaocg26bosPGDUBAgmxHyAQqjBwGDHhwOec8y646BoBwkSIEiNOkhQZsm5Qo06TDj36DBgzZc6CFRv5ChQp1m/AoCHDRowaM+6hRyZMmzFrzrwX1m14bdOWbTt27dn32RdfffMdQjyew5DESQqBDC4vcEFwBDcBbMGFEFz4wtjVwQ8Qx0k5VqjEDlWCGjRgAKMrNo63zGKbebxnJUFaEBcGlxKgzBzI2CCGMIwRjJovW/EKS1jGClbNtXasYwOvsYlt7GAXe9g3D9r2AR/xyfwc730pIyxi8PBAa6CYYhEd5bDEY4FkbJKKXTKx7AUCTARKsOdj+omdhoeJ54yji0WM8xz0Co/zokMLKdIlRSUMZpzksWfLG+xwAFjBhB3BYsOib+BcjX0ShAUs4hmer/UwHDaMLmIlgUq8pwqoQQNGZCBIM3E+NgWKIhQHa0GwDmzgNTbNr9h8w/e1MlKJXapQgwaMkIY1rGMDr7F5w2jqWJkSYoEdjgGCbWAHu9jD/lqx2CIOAYUoQRk5il0LWMAinuH5Whlp4qYD3Ss2iCgmkcECo20n2AF2sYf9O452JoaZxgGLWIXGHG/Jxxd2foyiHvthvA0RnSmzYIgN+iawxBSFgXoiNo7nTA1RV+kZd4XH02c+chVIMSgpBpTGHERBUCDUMUNYYIRqujCgPdfndMIRVfM4w+N1m7assbaNnXzSmxYOvbbb2qZorAbiryC+MMFaJzvodlVyhAwST78PRIOh9Me6/AAi2PgeU40bBQqxd9miA0C83b8mYO79x+Z9f1+p4au6T/G+/Zjt/efmvf8k52PWxxOjS+TWhP2+7ea8b1tvqzfk5Pv2ubwYyOP+Xvec79qLqupptwrsCDHWRh8wy8Vb2ONKEDxJVEKUtll2aV9ZW+R6N8u5bJHayDjHHViDPygfiQ0cvMuxZEDs9PbC3cMM4o3T4p2S2IHNtn5ERsUszr5/baRh93IsIZb6quN/joAxhxTxrPDyXUVgX7GC65LRWz64jxHi+x1vV0P053wexzmFNXH3gh20wT1a+Bqhl4tRF/SGpyQ2iZSg1OEnWerZW95rodVsk0dxEb9vRuw+vDc9apPva2KeoHi2zbr2++Yr0oV6GsQr1ThhLM444RoHwzmCjaMQHgganwswLhNBT5wsAmUqjlITfoJmy5P0GDrFmPMyb3mGJWtnJbc8J1+J88ru6lX9Nl2zZZ+Rz76zgCJmATvMDrPjOG6Bo/Dzo85xduc54gLHusoZXOOMBDgXYY4Q4ZzEOTMJzkCSHyPFj5DmbGQ4E1nOSpnj3OR0VMJDDjbDAxPrwzAARzMM6ZxtxgF6C5zAitOz5iySBV36Fx1WCHE8DYwOKwE/rowf198SMWAUytgdYzduAta0eYgFzzF4IS2ZvLQKZ63lIes24W21x9i2D50kcnpf04fJZOyMPeLEriEXpc04BS6LUC4pR7nKDxPmKKIc5XrIQmAzBkbPDjEOxsoYGRtRF6tBaKhASGNohonQjMARJpyJWYglMAkew0fMYsFRUjlKWntErjxclDJ2xsnYGWfE6QEEFzCcQJHJUIyeMYJuwiaERoa2MPzAJ1iGl+y/dLZciX9dLyY6WThcAd1q2ytzg/ka+oa85tr68n2lN9LR7Ke5sbwx31xHK9pSXyPjqDM/wAxqYsEkNOxB9TsBUOdMt+fB3p5w+b/lWQKEDnR1tgPpS6Zw09oHejt4gPHBAZ5EsAQSOgowlAMlas1t8MDuCSAzN4HNWaqISChm2yck+INATlOyYFqEXAbUBBb1iWmspRaKV5Lc6AnWwHXVCV3MvaqbYem9OOACtA4nQNyKQYO6oLIM7tClIsHIXZPG4K4WzVpRtGnXoTNvLXHs6e4Wyo+/Hr2oaPrcg4LgdBIolfQyYEG7rMIgzVqbUV52y2ut1dBjwDy0Deid52ydTXRUQA7E3xf+gb37RQFuyNWAunyj4MYNDV7EDAfd2rc3BpBugUmpwLrkYOGBkSJVaNai35gpbx34jwgmg3wvY/llXupQ3EO5h3NP4D7GfYqbl5ufW5hbkluJ25+7mYeXl/Dv////+w8wErcKlVrcNWDctHc+JMGkjzd8PPcR7hPjF+KWuGGMb206GUPYYxDE/4KfN2Se5JX6+/D/rv87PyKvJfUPO2bttkhhgg2v3WLl6ur31eNWspEmdyHgKVgeWBMHb2E6/64+ZbR+U7+pP/wYsRYseua5F+LEe+mVJctWrCJJ+K19pxMlOfDBR598luwLJkC2Dja6CJIceYGCDkehqtfgflyLO7CHeicewF14EIfgIfl4TT4BOBZPyhcb5UuAB/GyfEMCb+ItHIe38T5exwf4ECfgI3yOd+QXgJPwJX7ER+pPOBk/4w98jT/xF07B3/J/RTkAnKaG8puRFtOcrqfNlS00Zxj5PkIt5CdAc5aR39GcbeT3NOcYXdaca3RDNecZ3XDNBXo3XpmguVDvpijTNBfr3QxlFs1lRje75kqjm1tzld4triyh+V3vllOW11xtdGtqrjW6IzXXGd0dmuuN2WbX3GDMPk5zozEIzW1G/M27fXmdjYeK/ylt4q9C+okQbeRH2sQ19PKTOAW6NpFPWx+BhYY0ompAhWhDlvrNBBLdVAdU1GZFgPIv3XotlM2GAFXDHoGyUMXUVpJROQCk9ZKhGxGXvBzhgqfcgDS2WuCryWZVDdI6V4RLLd+hTNfH/FAAd90W33ALHNi68TiNIYig5UOxnzktRZZOAK345+0vG+9MTI6GsmHKOJTnnvxoE5oYS0Mj4CCp+9J5m691DHZx7MdlpaXxmI/IrWSF1RJ8Pm9zAfwX8zZzK8OsOcPUsHhgopLksZL6syVz8OejbR8QmTcuxdJO1lg0YeMVE0S4esMciydeWAFYUMhDsZ0aTVUmVjEKXJAol+3dfroY7WI6nFDJefXVfLj3+lMSGvPqefZa2GJCNQ57N056MFCWlqLrl4rMjLBiiwxTBHIrqQZw+O+scKCBcNVdY4fcsFTMDQI4oJowbskgPmbXUlHxYXvjgXDwdlkxEo8zoe2LtJDuSG/O+42EmEwLVDerTd561XCH0kU75+7Og7z0y5Wf2riVRO/tTKOepZMLJ0fazH2MlZ0YO9VjWbOrI07W45UFPUqMKAg96oSoOrxgHi5ykN0NrY9CKK4/JmHZE6pthPvOjaadnvPas/9abUSDFbzEd96GqU0loKo9xucbkXAk9oXvluOzwkHjL1K8o4aEgTzqDFE4V8FLy8sRDJOnFJ1fqCbD0w7XQXV4S4Ba0DILDYj74/pivdAGejOSt9JteLv06Q3Eyt4N88FwFEQ5iJaxHySKndtI/615sNetEPKc1xq2ONrV6QUoUeAY+kYWuooF5DiZb9OWgOY5zw26nSSBVm6F6mSmaOd+WzFebCm8gtMhrP+PdKv/esV349/H+apOxqf+Pzf1cd7SXLx3/Ovbax8mJFzQTbXEovw5jXZ9NWfsV2uKpisJpf2ng6GDlbYr7WOaEdUC4BSTdDzA+jgGJ//oss+NI1ZmZOhHcJvcDkMvciSVufbfqqEG90fVMaaTqpYEbbEPOyFGQoFCFftRwIotBe4TnmKGFDPtyVRkfL2g9a/YubBwpCJYzqk89/x0obLv2uwBixODju0V9TGYfWtMR27TZ+aPmhrcPzmrmAvKiV7Va62yeeg2CxNqV6B3gG6+hNl420/Vnsp0L947hcYOYUF0M3indNtOn6kH0+Mu06b2Lgm3RUqexFa4uKamnNHfXL6kvL1Du9uU7nK2v7Lcp6Dk9dvhDmTP4iBUMISi/DGmqCJEluqEUt6hglkdaItv/ywF7uSW+Siqohx7LkJgZulg5sRlofZ1aX6n3vy0Konw0oCHLe8oHBwEFwBXhsuVcxMDLqi/lC1Fudwpgwv/Kn8qiOHku0kbt9c16AhnxD1CnkoTNvUtnTMYqHuQlgMFYyMb4TdYsYTMy8Mdn4J7uQ7dJWFFAx5NlOr+XlVv4Y8agkGU4kJMAql91054Y4eYNjWelw15OllF11adHro0kOlsKj9YdqjUAzifeHHXFa4X4ZPu1reE7z0YhezKNCWCgtwy6HTNLKWm/4vaNRjlxhU3glWJx0C+rtwaWttjQMgdIW1qwL5Dh9zjPq1zS8gVvShFLh2zqtso0SZypUfFywbPNmxtyXPrTqAM8Xa4MiPEqsBmivNxu4oZuDCdwHHcfMhpe1jldVe6n+wUY3Al1JOZISR32SG0I9XTs083jMshSqNAg+QxylnlyoZY+SrYiVUfYBq7EOk7kH9ii4aQR6QUFxS4OzVpY1GqsrEjaXGoF0sL5dLhtFlDqAjbWbSzghA2qADpmnd5ocfdHgUsLkzXDJnGrMD1mQx4s7uhFgSZ6cVUqv1KvNZfmwRf2/q1S8fe4HCmwMSVXnT672F7ipX9R1F7/sr/P6yPIXJNzL5azNkEk0gZXfbWrsqLdAfhzRZ3MLcva8hZF/9Xwi+vetRTWwgN1dWsgL66PTZLhrfX2SRDjJILR7TF4XwQh9s5RZNp2RAna+TnAiIOt6W5Wgk3bf6m7GH6QGK+DRJ/itwMzRAu5pFEAdUNyVNUsbxPyKvHUEKjfhWvxqYUaFznu62/14l01p4L+qGWArcE0joajrWGqL8st42GyVp05xYiGJ6LSrU0nmzZzUm8sam3NtYMCiqFwvmR3hWeuZO7KBtelS6qlE9Xj4x4B5czHUOPZMFX5WRFAwooNLxjz9fzL05AuR4+5aw4SO0gN1Ah4ljZBHp+w3pD6sMhz6EsmsS2Yx3PwKUX+TibC9YMprOYP/Ezqsc12iLSV72b7tZpGfPcFLOj8/jxnovpr9vyhSpt/6LFj2mb6FO8zclttsw8MZ3KC2eTXw6GfpQ3eZDUOea8x+uCIl6o6sRQ/0GXAjKacMzzyOpWFSs3u4K+6/A4u1XU794rPExtFvGiYvlCdp9ToIUk1L/uEtdIqO9TuklbOGHZhA2abFkjsYT25obROmVT5nTUXNRC6JiysCQ08ijgBRR92tpzFgmV5/2VjTr5EEaes++6QpAk5odzUtBGuPdKy0WkXti9kwas4GnzTHhdlpMiL4vqXiqR7qv5+y/lY9HQAsQEvHgMStoX0fROqcmXurYNpzBMQ7M1ZsHp/jCZ/ffsGgMBYWhS3uDOc+OxglB3cnbjviDR+M8N1rZvxTZpes6qS2bblIYYZa638OBTvinakght6Zqj+r3qo/mqPieZZ721P6fjM9iwaZ5D3jQbgh1stExeS1jbvpxujPMjgKWMoJ0sAUvHHqMceMAY9LoSjVglIlZoMC+qg/lDQ0y5Blc8PMLNQz3cgN0o+KSTN+ylMieU88KfQrxOIfnq8g6S6JXsUCkn9crJouCVjB2epLxb2GE96feaqRmlvPj2KsWDaHSQZVREgdC83NB0Zc2DmVqOV7lLdVy0nae9afF3iEERoV634oIF7BsDdUvdGiaedhP6c/qpbLNvx6gpmbmk2NwIwOUUxNY2xMbXNCaqxNXWRsc01KYk4bRNHNVVjO2M1LSNnHKMnPTPdFyLra3H1dWlqJgQI6ljdX4AqRrZG6nrGDuoqZjaG/745CJ/2Z/oIg+OJFf/1hp/2wB5skrtcF993vDdeFPTHC89N23FGMXo8rr/Zd3UjxVbopr8mJLzArK+qJ4d4m1fT1cbGhNYkIY688Y2gBygr3hN0FhW0cFUU9/GC1gXMjm9o2ElF+K0N4LLDwyilPlCMr1pj/EnwDAm9bhhg78Nn0+cWnDLjgGIKa36F3wwi1Xl9ykMoH1ArnLPmriUnLTT4feJVd03CiwGzupnV8yu5F3xxpsU0lue97ySL2Q+yhJmRT3dcSTcHC68Zs5rzXtCe7L/D7qBBvE8LipWMejkuq5MJ/tHwHTqff57INsKkImtAldbiGlACS/nx10pA7kqT+Vlqd2tVRG00Hf55et7O8uXURK9TDuxi+T06AVk+fxu4lpKUtwc6jW8oM34J9azj50EYVr892vekYZlRsSkTL/kgpJU6iPU9xePd+bbnrGfj699BUcchFJikNx4IvzP0xN9qS1/Nocnt14M2YS2uV4K4otpvT3yvy+C5W6aX25SJCktJfZ2cUIWkgLidlsT+RP6NP18z/PDH0fPfR4fnTCBpy/UFrabtC6luskmjm/h1tY/oV4Ofl5gu9fKFk6ODIzOai3OKqPEhSiJ0IHI17ZS3mfo/98na8TY8QcbjPPrX5gnZ9Byf1WyjOv1MTNkm3LfU0ZKnZWvQiLSe3LzMnpBwUiVr0t1iC/3CGfBc5AdEfEkNthd8xWI6wu6/291evbP2r3AqPu+ArbXvBs8eneeDQ7sPqP3swktKvyVl/8jKy02Jjud3fGYJ4nk7x2XuBKXuJKUAsIbURJKFQa2kbQwp7XELK+tx2GJz38oHBZr0CHpY86WnNtEf33L/tOqxPeMgXp36euQ0GxaYW76ANDF+v4ZEeYJyxLmCR0BmeqrCpUKcgoVCiDYnpyDeiBqF5olahc2AI6FSKBgkqZBDMnA2tPVM9D1Vpi7BRWp3Bjrvj870dZa2eDhrqpbMCA9i3/yeFR+Y/z47HBXn0idqGGyvnp65jvCE5aaYNSDbFe92xd1ZIUvEN5m+6asYjsIcTfztR9dH+sIYvjK+J3swT8wY955qf0scfGJoOsv57X2WsZjjITl7rD9g58Oqy135NZ9mNN0802lvZzOWOOvfAuXCyjYA+mZcPvEsDHeUUo97wJltP3owfd6aAAsZWlvf3jgll+Jw8UA7sTeqJG/q+/F65lkSEXJoXSnbhTyRuZFeASneJBPgFb05LG3p37to8R8xVIfZwjT17S+P4zi2hw80lJbnNc7zfo7bW/0cHtFY8bxjh4up/jBmboyUXhoNamrEv+dYYRZlBAwsfKN47/Rj/eEBQRkUZVBmOXSUsBSssvm5oNAeijWIXYAsBRS2dgnB9Aa6FdzxbtQ2jlq84SRidtqQq5cm4fen1oYEOXmHRxjAQj6usrd71F5TVVFr4QNdKNgsHKlUk/7AeIKcRCCvniIINBFGaSb+bUz8+Lj8sOBiMSUmKCsXCDBsZq23t0bb3CaNPVeOoa/+spbcC1pL/ULFhmEY4coJ0dqrsKf2ZVfZ3c4uVmcfXK+gGB7vavT68wr8/b2gyov6pUvzs79oJvVsEiWpaujNNPXZ4kjT11nM+UZGptbGerJcgsPEPQUZ1/3NCfqq/vv2VNU2mMW1B9wkZDE/Ng4esDy+dE8fvO7cYlZUXVGUkx1cTrKEXQJDXPxAsFG6McxwUKfVdh19e9AYWgoSyz0A/KE60EXlH5Nn/YX0x9oGmvZ90Nm4xt+dTEx61GMoJEr02Tm1jlyfXZ4QgU5H/jBKPHbzbXJgmbC2lLetl/U45DBuLq8wvSycG+zvFA3pyeh9tXebRzz7/LQxNTx24NxQwXVBZVhi7n+oBZAmAcsoR+whLmyW6B+h7ICTwH7ipXn4DiCrK89eFnWcreyurml7BUk/zuGaZvlGaU08nRRZmBj4nj/+WYC+kcVe/aPc62nhlnObA5yHaK0Ekb4uLy4vDj8+Vq5sJQBOK8YY3ll4/HM4vKm03Xn66+fLT+ZWQfH+bCXL087ZJ4eOM1P6BKIl8iXns0Q3IxhL/ueBtxd7/sBl9hfEAJ0vSGAiqdQ+HFXKAB5UaF+FFowmPkTaxCO1fTdX2++uHj7wmITSETl5+TnwMmal+UI3yqc/0+vFxXfUfGkM6UBaFudRnZ+RF+HeAcVmh4xb8QksG+Ohq7+lMtnwvYl1aTYjFdlJdNw3MTPDhRQ7D9eXx546p2BD28EHFBlvJOT1y6gezH3M58m9eg0zBs2dMZnPwWZGIfsIDqzVoxlQN2Ig9cyhh9Ol/l9fZ6Q0/3hBV1LK51LOsk1rWhi4Exf4SOw5vu5c7X/gVe0CBVw0Ij380oYu7cr/KMX15s/RaLoln7UL2hOzJ8BTY4lDu5PvJWhf5bSo+e2DqG1ltHl7SPJ9gk6QvI+JgnTBL8Jn/wq9qxTLHSpCUXwrqmJ3OR9l3q/UW6NPve9qLmdiz3xHfldE9jw/d26ODjmlYIPauCe/fFxNmDVQdLYxiswqzkhLz/Zyuv2wINtQeqnntzphFa9sn3DosaE/BngNOgJzRs1QcU8pj4Bfxdvb/MEGz4tNBVwQAUjqiR1/NQVT48I8zAPd+DslOwRPy4jmPNdRFZOwaCPcOqyl6hY2d4CqF/8gsNVr7rSPlDXYb1meCHgBRjl3wjAUSOJCQpMmXit1yKq1WwT6IgNXqBO+LsSI5wTQhmcJ2mju++6FYq4qJLA0SmJTQgrfmjXf7CSYM5fHv53JYm8zCb4j7ks+NdaStYSm+AfKr6z9kVq2p2l8fb6pdSkuhcgv8H+ff3K4KhPOD0VsC8b8EE+KY+GN/hwveie4rnEeoMcrH52U3zJLFjz/d86PzrgFUXn0QjYl1SMdxBt5L3An146Wslc/B3jTIJJehWpdB4u+OXkCXbLFQFVshMVbp5RJ3YfBHhOa/joH5GqMdD2041f62nt3byfQ9OM8rR38jVRUXRV5JRpMtIPNsl8d5caM27hU2NLxVgmexBdIw3hSIAcboI8US4vKifPHZanyqEcHpGbXD5KHsWBUJS7e+RMS+dDGBmg9n17OK9cykGVpMCHX2zDN+4fUZJMOxIneR+UKv4rtBSVo1Myj81PhA0tksdRI9nLbHCRdfhiLny7e5D83I7Lg3fxZ87PnGcevHZci7DHwjp8aey4yLoj8xKvElMeh7WLBo/jec8jZ69OgEReSL5FIp/BUc1CkVgI8qQItm/eG/88nzn/aRrpvrvWLsRHnFY4NvG61rTW7M0kivBUHhSZcwH3IBcIvybkcPRL7zoNOvW2Hh+bVuAjUgTb1qB39mA+c/7zxL3O9buCoP2Zvoz2PEn1tkh9ttqZi9oGbtYVSEXP0xTNlButFT8HNa39bKChqR7mOYeYcjXD76OhFuOYHUxn2oqNGH1IPrpF3W7nGT++dGb+0QkK9wZ1NZerfw6bOxGVolfbgCfdqyIZ82UowZdqBQvGIDZ4uqU7pRV49PiXvYcPlufa+t0jw7xdowsj8OgbpaD0wacsdmaqLDo+IZqUnOpcPDHpXZWVF52QlVsaNDxSFpyTH5uYV+hZ9XjEqSydHJsgiQoLI84Nh2wMh/rMhtE8I8E+vsNBC/cDeDqOCucteE1hI5TuKIUrKQwqyIbL3pGNoJNV/DlkMR+mHX5I+0YDlczW+1RDJQGVBMd69Nimz8PryuLJPk+pSExHXkniHX9bszSXaZmua/1elkXetC4TeYGbCY61DOMr79HrT6kRdUUp6RVEe+XrOiS5Uzr1Kj62ElGnGs0Oqx/TIag7Kt8U1YmTP65H0eIwcFQ2TfYAnJHFjob8un3LRQ//T0H5k1Fhc0VZ6b3Zbi/Kg9niiqbq3CwHa0MLa/ekMN9g9yRrS0sDp3+xzeVnCXfk2O+Un21uLOUh1FZWcNSUnSE2TNDM1Mei1MZoZhOjHSbq02Oj6pNdJuA60GakYRQVFB3m7F0b1OwhkhCurC0qXMHo3BSdFF+YTpK2O6prEXrJ5Bwt3UcfTGWFrUFqPYiGj6+8AWbPsfPEMVuuqaKSo76Ep7mphJuj+BHU/0U/TY+zsD4mIuy2XIggFMoYY9ky56jcrDZEdOvtEhXERF5MTUvaMRmE3nBVkCsAf3bAJafYzSWryNUtK9fFNTf33yExtZsSkmrqYtfVtT3U1eB8trFHvkdZXVl0fnRpHSfnZ8UCRWgsMvG4dNTDSVnwj7TGTQ2JUmJtNr4w0svFzc1AuzWDDGw5twzVVaQFrmzdlFAXulBqXxUanlQc7HJd2Vz+SM55sltDeGLcnQj3sivaUoo3VaWEfYsRU+AYgzVGvlw4JCIZAlW3iv3HZfK5aZJ1eKIlqTFbV3Uhmt0VIOEN3+L/Li7Q/3vLDFgKZD3/ZbKqXrtLTIeTHQXHsuzKfcg4Nx1j4WuXf9rhT8fDflGp2TPxKHt/AtDVOJT1DRUV9w3UqpfTBkXuD96pwjra+hoaWBOdtB1sfQ30bDwRW9CoA9fzkTYBPQMM9pyefkRPx3AfCypSsfa4Z2ThYUdL+V0PH3mtrAHpQMpduVw8gQE+4MdiK/LEduM6ltJT0MGxqJ2veogwaMQe/PDUEpiqqumoaAeGqmqrsmUtz9KMuu5lpfTrapti9GyL0slv7/e7GOyV7vJidvitmrSyf4k0hE9GzuGLXeoZxUQnr9p07GoFdZm7r8fwGvia3wjZl/3c5MJS+A5nv3IJHIwconv7miBBAyyFeo/3UHp5XnBYeV4CgBLVZHpym9Y50l4ecXtCWVjduo9slKBZPtHWrdd5yeJiESvpkqi7j5W+iJrNvSzjJD2z5ElDa2c305d8oL3GiU3okDNat0qr62vOL91oWV41cvcMdXFN8dTrIZA/dvb5teoL6stqtxi7zr6DLb/nuNaWB7k5wRr7NxJKg2/45KmWqh3a2/RHUjwNtFwtRdEifjmNbMnHTsfFZ4Gw8qEfdIVWZf5/npFJCa+igldJ2d7vJ6Ojq4N9rw2lpr7IVrXvj4k17xYQL9sXvZt8L0rVARfLfJgpWdUys/JBXWrtcFREwXovKMyavu19ayMRHKUWYhqqHlz9mdAa+qUO/bOWTPayuqCaHe70ZKhQrizs72pihosRr1pOuMPEk5xesSFbL+OuVdXkuhdOnd3GXgatLgoptS+poEBcV88/XQVVzCd2SDxII5okDXwyQLeNk1j1NzeFBRpFm5PZ/uZVhTdw3PzZ8EuuNAizrCJ33NeJOd2DxaNlcBOZlJgb2gZb/orgvy8T05Me+ks7yDkNE32mtv7u+E/iGqpHSPeC3NUiEmO/JjzxlnWQsR/zIS3RXfvXi+8pf5aSVL442V3xPDmhckFhetEht+pQEsuxiMRcEHDcZyjMbchpKGT6yuoDvAFRJmH+FpahAc2hIb2hIepG1xQUBQUU5e0VlASF5JVB+M2SR2i85YUOW5nmExfKBMNw7sEpVmf8bCR4Tl8SV7wCbG85XFUsI1FZib3+t2zHgT6Hy0XFMhpFzuiJAQfbMUg95qJqHonKDKQ6Otk/gtTLLirmiagCKrXGw3526UbT40ircbedBnw1xsTL29LC18OKz8TT09rKy8sUTvIhqWn8sRz8cWnA4td1Y28WT0fSkHSo8nT+p4vi/AoWJZyMQg91zNzxd+LhKA86GfzjxFksY3t4e1J7RDs99sSpz26kUxyVHrxeNZQJbN/tiKikKJB5w5OSJJKSmGR+1IecTk73OWr+ZTz7ocXDHL0YkG1PUcxXhIkE885adk0+uu/TKH2qOFzQN8jFzd16mbzcpDa4iwtjHcI4NB/OuiedIbXLWyMQmvt72pwuSgP2OTVB3UuGXoVY4ephvUJeeckZymjKokp2raG/k3HqXg3yJH2pz1Nnj+/wlNyHj5IvrqvxW5DUo2rzcjPKk5HIx4TsSxK3RWwspczlrlpoPjKfaJxQ78D0IlwsO+6XpliePlg6P58uUarKb0lLKW6C8vTqopaUtPwmpKQEN5Acj+7oGEqJQ3cCfiLufw/pXHSPcW8obQjCOgVjK8O2VnkYwTtGMHMlzupQg9wJ9aj0xJDw8gxSSG0E1rGY3j5aXkVdSVZLU12tEXhjT8WyMyJcL1U0iYGkjCD/ssK4lNo7+cLTy6+fYCIyhTUvsfIa6hvekNHSDr+pATU0S1qtZdZojDskKwQGiiVLxrqVB7OVB0eQm4Ka7a/AX6BzXkBEJF1P4qiIloTsDXUJEVENicj6dZtycH1T4Oo7n1kevq6iJOcUmJoUGVJVHp1UXIPK/ktSOWxv58XahZZaX7gpbaCtIy2lYaAor6YF499A/sDtfycMPX/pn7+PSuzwY6fiDq19/AdgboQ/APoI8f34OWewOh2HbYQEL//nX1yelOKfxytGQlyR3EA+bolXu6DElekIfMgpedF2NbuOFBYt7dlcTjWnfJNShMCNtRLMxW668zZoPi9XfeYJcGtvOeM8fwiY+UkR8IIRUCpIN1idq2DthERl/SLFFOQ+UDi4fOAuLlEpapvarnaonayL44B9wtz4kOcqJYEk1pQckPqT0z0anWF0SR7SzU8Jk5x0kzw7Z4ak2fjOLsxV8jspQHJASk5Ol9PDzQi7kqwUdckBRJPTQ66HW/hIIWvZbynsJNdP4qpw43qWnG5nZIZ3S9pGPh2XkBJYUy4lsTw5INGS0430cPNFysizxJRyNt0CyDxIO8IxetIPLntk7I9xURvLFE2yrxilCtnshqrGR4GqJg2bHBNi53cPl6UjZW6P0pn/wQiY7tKc56/JA2EMKX0rE7tJA9NsSjVg1k0Gct8GUmR7enawAmmGw8ZVM2bcZCZ2zT1VliGtpNlxZqW5bWUxZwQMpxQMQIGrrVhBb/EO0P9U3RA4Kb4uz6rkwmAlGZm13pJfHEvHDRWU4NLFCLRz20JX8DS8L//71qc6p2za8MoUvY+eKJcqmPuOg3TlyqQDuCC+zqQZYrk4faXQzWkpba+LAt7CIqgDjOOmdpMMcQc6QXk6VsuZydW3C18+nZlR3U6ANrMv9tTEu23vN7x9MgUNlPC8jWgAde2+AlTS/V8D6Vh3FwrIm0GPLayNheLOfbr8LdOG0PEKjN2Hnk4cms72xf2VuhWm8RNb5H5jBfXeoniegLPdA1it7crR/IosWA6U/narNivB5/pgPXU9MQbSPu0/fATyMtD/v4gFce/lr9lGpILyYdtlgRMwIj2AVqm+bVSifpbqgw2Ta6DxR3HAdk3nyuNvfEol5DMwF61cSeX5T0CeY9zbivsFNqzSv+OPDgHAYz/7VQBeX/q//Tr+d2GowBA/YCgUQAL9AwdMAEPXy2j/1Km5c7Wev0oqR4X078sLmkdDRSZLrHuCaiSvtsS1/uSUnDjne+LzQ/yF6ERNop2ynMyi+PJxGUUT4anAD2iEFawhiiLEQQs+T7Rj+oCU2lyo7FSro4J6FamM0+hf49bEnai/NTRRQe2GhpoqgYc71E9o67yCfEVPdSPaMKGi3bf3R4Ot5SiBjan2xHrWPfW1n24dI9gEpLbDTvSPkmkYkKiTWOsE43xaVlF1Xp4Y+mJKrpTA5BAS1ZxnjZBaBVjWWDZVhlUeJx3ckG11kZo+Sktb0T8sWPSjkj4aUqrbUxMyWCjHqWxyl4L7jpair64SyxqeHhoule8ulEe4JDBNxnLps1bEHM1XrHNFKdWWa61l0TrxwIWxph3Z9T3zYsqpp8aiZMsug25VL7d76uENNUrPf8XUm+zyAQW6Y7I5RlhNVFS2KGYu2ek3f0ShpRoPZsCttsSoJ0q3J8G7PKLUD41Po8SMxBew0tTWISm9QdqVIrJmUqmJLGqJ3PRLcb+SS1+JXRnRkFuElDFhlROk66CAK2yrqurdkJEK2coNCaR8Z/M8l/yKdB50rqhdcjOXk2/xwMM1K9Wk8gGxGsmmIeBcs9RXL6drCune/NMygCFLsXQ0CAV0IQsxWMAYktCB2r7SlSCmE3OnNyt0jKt7QQx02OdnBJlYg4aix1Z6gXk1YRwBqcQ6YlbdjJhGLOuU9U2/GtPk6U2l+xhWE8ZxlUjsIqDZSrOYRmBeCPAXbzgXQ9hHyiiQoeaEgwBD3IKGVHoAO4NyJIKJ9UgUFm6N6NZGDIzE4hc8EueEsgfKwhCNIm8+QhC5cubCHzchAgQvJjcV3pZyD05rVePFwdVA3odHWl9V+DUFTiZ2QhTYI0dX/cibPcGfZCqdBfBgl4OxSeTHVXgZxtW5wq6TsmOv11H27sqBTFf68yHp2oUFBVfjwIcqHbhwcrUExI5rkSuHDi+y9Kav0aRGkTJtBpRdsaJNILE4vXDjqAIA) format('woff2');
	unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* cyrillic-ext */
@font-face {
	font-family: 'Roboto';
	font-style: normal;
	font-weight: 500;
	src: local('Roboto Medium'), local('Roboto-Medium'), /*savepage-url=https://fonts.gstatic.com/s/roboto/v19/KFOlCnqEu92Fr1MmEU9fCRc4AMP6lbBP.woff2*/ url(data:font/woff2;base64,d09GMgABAAAAACk4AA4AAAAAUBQAACjiAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGoFOG5JCHDYGYACHDBEMCvI82x4Lg1oAATYCJAOHMAQgBYMAByAbpUET7jBjHAB7QEZMFGV79GcjEcLGgYhgzwz+q4Q4xgBzx8qkjMaUlpKSLuy2LWsIBq57xjTHufb1dfw7Uoz45gxwhxVN8echsvv/Tk9Xzdz3V2REIQacbwgok/D2T//Az633F4yRJVJioSCtDoMWDJA0iCEjczBi9MjQAwGVHjVBkBbpMrFo64SJOZWe+BNykuz+3DEXKlDWgnDe/7lV/Sqg5f8qGM715RpeeJudQdHdzG6brd5aw09gNoPp9CzyIBJLNTqH4Uwe4h1DDTwAs5bqwvQ2QSbAY15a0k9uLAlhMSAkRdJecWl37z/ZTNt5h7vmCbGCcgh6rtK5aINFOfu1GXl2Bas1anUgMEkyy6gzrVFnoC5YNMw6nR0ErFL0Kd2lTNGmbpGLtkkbi9ZWOXZeB3tDmldSpkVxS5pIBR1DbVLszmXIlGXaHKTFfp+GMUMcoTdL3Of+x3DedzhZshabD1E54MGBDkuzP2wKFB4A7oOyx0kEiHMuQFxyCcKMBYQVa2g2bCEckSCcOUN48ITwEwhBFgIRJgIiWjREnESINOkQ2bIhcuVC3HQT4j9FEMWKIUqVQlSogqBrgGB4DfHOO4g16xCbPkAgUMCswPxQEFauAgUowAzKzsBssdUOe5A4/NzTTiBx9GlHHk/ihEPPOIkEDGD2TXf8kaedhN3ExjgUOxzdgw6p5YbXNFW4kIjSd+WUy6Xvji5cO1LCTvTInQAZJ+tjxawboq1ZkkKRlyCC3Q6nSUduzU6gEztRWLWUaTnD2AXEiPNOXLgdtQIEipMgUWqUvwar1GnEcF+TZg+0aPXQI72GjZgwZdqMWXPmLXjulddWrdmAQ2jPmPWCe1IofZLTF3ntuJ01RESfsAltFaU14a3KbkVu7xS1ioN7MUJzRilBeC4iWpbYjuzuy21MUTVuN4INoW0zi8m9bTkx5cV0u2147j1BaV54SyIalNiGbNxycbndkuJ2IIp6A4vQvjBrm3sf5LTrdh+huPdRqIZ3qSnElrlKytffX0pbInouo263eq0IP8SltgkgtCgmXZjVhIqYfCT3mnXcyUcIr3DFg/OS2LqMPOWUI696twpXhNftshHJNvkZjtc1JbQN4b2W3axcirreHXvWlfHr3wbqIAweeMAYXOys/m2zXv37HyogggNYKN/4Ig6E1uRSc8wa4Y5M0alVERNqAhi/F/9D1YswoD4zP4gSkJlSoq2oBX5gLia4eltTprQPKV/bYnsf+VswryrXZtrXZelUS8oD2O1doDU32l6Hmyq2rU+79kW2evvETt06obYV3V5W7LYceds5ZRqzLg06b1PlE8lej81eo6U/NaovG/6olYoRCh49bh8jHIG7fTukcEOyqlU/s3JJXa0B31EKrbnxRhWiEZ7YqP/Q/DYwF348y5rtBCsLqEY/Qi/BTNod4uN8Hdy2EGamzbUWv4K7SdTl62jBvcgNL63eHpViI+FkmlXdvppLahG7AQeHokc58QoWFxnF5FZrCF5FjWMwt7k21jo5Dae1ODmTV+z81q7cnly9htl84XTo1K1nRkffbfMoarQcF3SGoxhztPNcwAWOR+Q4jpwbifNy4hxcOAc3ziGAcwvkvBI5IpXzS+Pc0jmvSi6giuPVcR71nE8D59TI8e5zbk2c1wOO0sLZtHKUh5xNB8fqtNn7VewnQc6tl/Ma5ogRjjbB0aY4zjQXMcPxZrmIOY43z0UscLznnMsrLuw15/eOY6xyIWtc2IbNMbJxVVCABpygYweDBw5KiiN8vlllTe0zcFbqJ/G6IQvSUZ3Pgc+Xtc+Ku8ln3Vh5gchTHE4+kzCF8E1wc9uOh4NMJbbSTHYhE7ASwDdGwRA3p0YcV5pdBCoEe1gMOZ1YlfoPqXr5evkmeV+INxm3yldYkLemGihL8BH1VS53vhl80gbK1n933pr2OKRgD38BjnoLO5CB3AWIjZNBhkys0UVRiTOO78lyCEzRB5YmAwMVjuEblKBFZpRGTTRGR0zNk3k7u4tg+VKw/SMOCVL4RVgkRU7cCXo0RWdMz9N5N98WheVv/5h+CqbnANMjYLoXTK+C6bdgOh0o5IBlgXXpAvsBhwEXAjcCdwMbIVhVroLRVKG5UVqj2jXXf06XrrBjj/gTN+Xgxsy5MbpaTpy5cLVhc86knnsxa2Z/J1yEN95OnYcXiSpKtBix4sR7aVkCmkTdumZQ6WN9kiTTuCeeSplhN7blvQzPfFCiFNeAGle1VosqB+LQblogDis1X+hhiKR8M2jBD/4oRwCCYQQyQlCJUISiBhRQUI0w0HARSUjFNaQhDStIRybskINcrCEPN+GAW/gPdBSiGE4oQSk2cAc1cAMdtagjUvXAczSgkbhqAjoQgE504h260I1w9OAR3qAXfWCgH2OIxDgm8BKTmEICpvEEzZGnSw0Se1EQ72oe7xXvWbzZ+zvGxwD5sk8t0REa+9OyVH+B/a35SI9/2L+at/X4j83UXKrHyJrm28Gp1FzZK4vmXT07Nqfmql45l+Z7Pedm82zg7oRAYYdqzRwAPwDKO8BbYPb5wHwHgjgPdJ9MZ2qPI4WU6lEhNq/cCa1YYLFIJ2m6oAQ0ECktwCvDjr29CUhNtCWp9El0lvI85DPwG4WrPqeFKvzeX99svriCifhwztekgHxSilhvQOEsRysr8E4qh0hc/kkgNShCCGENPm31ieCCwoVVj9BOKwgK1nmOeB0qV7TIC9rnDav2YMZz7poTCjrhnzbozGD7bfsCHGPaiL23oWLqvlld2LIqS2uNqdtL78q5tg4j9Lyuv19zDcOgsMVFfefO69imcZu75iWamAKbUMXs6ywEb4QEeztebTrbQ28jtm7fNIgEBZWw21NMibqmz3B1x+L6mxF5+p/KHOKJYSEBZEolInalBwfsQ6zO6Lg2MtmeQzreG05YJ2gNd5ielsic+X8Mg/eXX8Mgj/AIT6hg1cGuEjfzVq/WarNS242+vtU7G51U+2+aigoT+Y1I5DcyBjBH07rSUGYM50s8IR6dOdMLJSKqRHKK1VRdnBd8+DBPlwI7lODTujHqxV1ZVDxXFCH2hSDBreuZsJL09zFanBijsN/Y6ACUuBYyDfe5HD6YOU4hwkxL3KB7Pe6jx6CnLYeT/ELFXDz1XH+Uas5RaiaFTR/ogl3h8cFA9VSUbt8rRX3smaRYVs+8GcwIm4obLgOAWs+vBbPPCqzugcXQxk93QWOXuMwAmVSmiEJ4d6LMNEhKROokZnLHJgi+OtMMB9TDFBf6UnI72e0g+9xAWCSreyDROinmNHOtYZsuD2k3KE0pzhGd3GiuVMAtvTY8AuQ0WJeysW9w3GogTrZUt47uI4nWf2VgkTz6aDPa6OBK8UavySGbtnqVKB5y2QUyCWqFJ7QNO0L3+/pQ0H5HCXZ3yNThTl4d/uKrsfNM8+ZGw+7RoLKgIhO9X5UTYADHOhUdX7tjZOUc1mUNpYHLZEu9T5llrd0xd+SGY1PnucuvIejwjRTDqoVdj1GihRNd0ieLJM28YszG9jAk3hCXttfH1EFnhV2ylyixW079Ugy9tup1JWGe2+biy6zhzsa+FPXNaKmdpy1qyw5tJXw/RBTjbmc9S9GWdltKoLqMDBHnMTVpIVtJ3e2nhHa5g920slSXOgDRadLtbFdqGztkzytGsZLUtItiUMRBLbDEbk1qPEeLBjXMUfIsFjqNgduysW3QyvVWvBlr2wyVOfDSMw0eXP4OLMMFDUyw1YDoAOzFSqqIR3JJQ2B0UMiOy7TWtyzXGpI7Y4i3UJFQGt+vt4He3HwlViQaRAt/ptwPUDBjgEg4sEXRs/KV8nYDfWlZqxIVQzXRhOZt+bzqiO4tSmhdt/7dkv0kOICX54iogcg7ZF/Hd0Qww4Wg6zmhpsb5RWPZXM7f7455pZDAu1s7oq+tVlc2xJrLTeXM5TAawoY/FWB/zA5TvWzV4t+rcuBepeLnnkOB+Oge9O+ay0DcqCNBUGdpzspk9V8JQKZhSQMxzBNIBWH7gskoLmXLPObW4TLEbULObpwcBIUA3ch6Q1/IE7sxaHPZx1Q0oMip30U9a3qCIJNT5R0MGq16IDymOCJiUYzjSkmFHypAYSRSffIErZMorDg60T4D3uiYBjIV0iWFiAhoYWdqgUqNy+Ng0sN4Eo1fE9T7XSMFE3kmhLTEnqXiHHiZ67mlgoIwSE1kCNIAQxWyv9QPzB2P/NncN7adVyqdLhb2l9Vpdj7k6WdydGsP4/S2mHqXbh/DZHfLRIz59mk42t0xMtMzjw2ifvJoPxiwsn0FeFdBoHeFrY3s2UKZCwNTUfyB9CBK6nJhInaLpdnZYYi2TSR2ZJ1d77OJwmbnStpneXRHc8/+9ol2BEsokJLTE0tMV2xJ591hlagNAgrvY9l4PNpyxCap6ZFDIhsz2gmJ7QUmdtWAqPZ36a+n8Bwf3E7KYwDHWR6olpteKqOyuiF1tZv5JndWO6BYfrSXlio1loVk3yxJiJFoR26qa5mGTTuWmHntojgYviS40KIiHCcgE84is8bmA2ElfFAdr0sdOkTrJkbhAdq6Ta3fN3DAsuZv2SLZrByKlRHnsiwrykSHvxQpG3vtm8lsxGQ31f7i/ToUzk6OiyViy3o5T/+H+yzhADIe1eKJnGW1XM1DymtpbqmWtTmKor2dntDQe3WAsXT2bXPU9fkTRpBcTDhuWpxqWomtlsyjj6qyOSkay3bqVNLQ3CjeUxS3DF7bx546ZR7nQ3XuGXCFH86YxsJDsg4lrrSQ36K1Y9jG1vOK6+8x2qEOvdYbNWao1qCSYAIcPLzBQ1OdSoWwujBV4T+81DWXB80cxRYzFMEgq4SuDWVUvYG3CjfUXmUG1zCu5LEHtsMJjYXexRJHj6yVh6bcZmORbTrmi9cg/Km6bGyo/DRUvZLr3cEZnw8iTrfqg5Z46DtG2TKTuVrMZhqDNs+ugU2hazi/fxgIu/yHiDPjiduj1Vi723NWY1sLEpeD/dpAVqkufGU5baPI7cNFGFwOVuJmUK1sRE2TqbQB9gN1x5ZJ7UBpxeOryL9mv1I77kzWJfiklL7iuj5/hqtqjg5NZeaI2c4I6Wek6xO3oUOpF16JalCOlJxQVtJlxShESmsXNETmbAEEFKEkMyyd1aalnq0V4wLLyBYvti6siHG+ZFHLjG/che7HekZ7WBwaACxjAO1sC1gcf6wB+X/OkJW7aMQuCbFDQ8m52EsnryvQwKD7BTSAu9sCnFKxUA82x8k5M1bkghnsJKnKORswuf9cePMI5mcDe3nmMdRI0lT2Sb6a5u39WNHPLP7bOSkZFYwvyGbMx+rvZfn5CYJFcCYgxbq3c6wNsIz7Ew9yrMEsGliJqYl/AMv4Y59qz4JSFmAZLNg7yJDQt5VQtz137PqNWIzEqmOLxMINseg3iv+XAbN3TUu5ugJ1zfeF6grOwoz5YoGBQJ2TAD1fGxZYmJ1WpLq8DZD3ll86sJXVD/59cwGzOrT6e7VvIg4O5d5T4ZZ/OPt4+eAJ4ZPCB/qW22blweePyX0xqbhbYn/FpHJviQP6L6dBYGh12WR7cj20/Z9/ztLOycHC3VybeS24cOxKZM/bHlTdZs4ha35UgxHV4NdEXGKinbWhrTmcqLcJcUdfWkM7BTeMOgev4Pejxb/jDq0+3nmDbV1D25JDbMlRdKg/yKDWgYn8rw2l/jr3EAUmsMFLTl+vuNae9yo72xXUtuB8/fgp3Zh8clQHXBXckJLbkboX8fdteuryF3GUYz9Hwu2ikCo3WmyEtsNTDw69UqewVL6IaQEcxT8CRPoHyMmt1zEp/cwOPwdb+5xzMVgmsAETTA6URv5dScp7wSfOscl+J+z3anLBmoDy3x32quSub0mP2svTHlFCUrsAFidJPOeFrOS5nvgv+fNYyRsLccE3mxfLaInVV458nzS2u+ZsbXUlTORsRQc02AC7aK2qq0fW03PS0yPr6o0o//x3FdQruBpLGiHYD2J5LL49tJBT12C8GaTx3ZQflBQcvWmm4kqRSAQTB6E/qXe0Ly5a0BjDjbdLPtVuPTpnY+9y1Sbgqu62QA6rbfC1T3wsbRhZ+IT6kPyQPeVmMS3Z4+RjowqD4zNnNaPbCImEsdkBVyTE3lDH1kS1aYNETealDkj5e1EglklgggzGdFXwyDfpwrBfz9ISYPEzfEqicyRm3AmxTjlwSd2k2OD0rC5e8I51SLpAYIu4rx8FjrWsUxuLDjQWUVt1zxvFGxvpwoGEf59RcTBTcWtZsR2k81ZnFPBssvj+r/hfoBe0emeVBVjGbVDv04sVyeyu2v7zC7UrorwrRmYF/y8iegz99R/rT7id8ThK1IsF3IQPf3+LCvGNM2dEhTh/A1HF1o16Q9p6n0OoUcH38rlHfx9k75yimFyz97949hTxvPtw/ewsKvdshLWLY8xV3Z0idPv4Iu4+aMgT4nIkKifLMW99BT+HvhPESGlwiU8xZ9B7OVQB/4m5aS+pFQumO0FlQawgulOg1l4LXEu1qg2x2IHlUHCVqHK/cq8gngCaA/sccwOjvLMDbFghce1bLum/hJ4NkEIqPI9RDkfTowaOrP8q/OEd0R3Osi+KDqXdi3QOz/BJqR25xwU6WIZd57gPfuX+EuCBy4uMF4+12iOuyoNzlOtMKNEeC7DG2bsrL+Fn9oxAaeu+H+LSD9oUr5Puv3HFIsOBp72O2KiFScvLC4HJWilP0Om7hxhYzjXYmGLNQEpk8M2GssNVIDYfp3Dnk3f4aMKE0PfPiGKz36Q/Ph+OzkgJ9EmNCkfA9algcUFC7m36CnYJNYHCPWGs3EnLS+UvZol+ksftNPNW5RyHifDpjBOcVYwfRzDCG6A+T14i7xBGQM+sOaaZ2Rz9o0nBRdWC7fDX6oAaZk3gbvVhc7SbhhJooQWMrENN9rVrd+RvM8Yjxs39b7nv6AZfvX7NzVzP1MbAoaHxwT0kXtfO2Pyyt8mpncz/W9tHX5eBKoawQ9iZq5r9+LkJrNrwOiaByfBUaDxKw1apnQIdfQKBSVYVDBXgpX9kXIBRIVMOozr8quAk/FUQQvMqE5gTh8crFKZnmARV4Ge2VLQwCcz+in4mnEZTiBQmxeZxPbxhutsJu73aWhi+yLxIdrvh5UgS4WJCzNvPCYM2LhU+VR9G+2GFwNxj9+may9VyJ3srABnBVKtgN9M1p8CGUdeAlxyKLPGi0N8vErPDh12824PX7ILJ1i6RtziTLKstuOMiM8BLmIEs/fg0t+ZV7le+Nb3zeRH5gwl2ibF3948OQSOYIK9Y0g1vKgUN9guEJuh99qJ7mJjunD7etvCyv5HZGGDhaWjh7BnQ+LUxwMbT9Pz1b19GUFAnyB3/3o9anWyoX517hP5MaKhtUGOqycmWUz/ZODlds3J0s0ZAXM+0haM7X5K9sDb41L5Lf8kxvtX/HnlTtX3vdFMs4c2jmB91uDs5aqjRB2mh5sMX6lcsuIz+O/f461t0yyobMTjI5oW4eERtOD45It3nWogr1nQV5RzSOEoKmkf9EXxRMsQkMOGECp1Kf0sPzKhhk3Fm7MpgGWzSrhZV32FFxE60X9TGVeS7RB5hQ+IB4XtZ/w8SVowkRoIqFzeBZuNcTnrNf/g7ekDuJn1/bYtqHthnZFH9AzL7/34PLiHuXuacXiIdokwC82Pw9kNwruzQH/SXp5gngSwvJDrYJ9PtuhnLlpoZ5I2K9GN5od78fM4blrjqH7xZPQuxTcjkwOw4l9Dzr8IvBvm655ChZuZ9oNr5+yPHUHd3yhUpRG9UeEe8W56sAgw63CU43XDBdt4SlEPj18IT69SMLJ60qOwAP1S4r0zcyc2aOchnQK46ybiaSWA+qxB8+lX4xQBf9zwy2LxT34ADEKXvocNOsvbx7mF7hoU/cUy1+EXc8z7Caf13kH1jU4OCInne1Da7FXfwbitCpTFSypCz/pAU4cxUSR5O5sPrleYUTnMIi9wUEsVPQeCfUwGG7pLtoi+/l6ebOpCt9z3VqSvYbZjRjEQ7emZqX86KOZbfgvhGVtPugIYX2fRsHi67Jv8dB1ogu3CGU0S4QEiEYxLMAhjFuhn8Nbe0UBN2i/l6gg2rJ8rm852W/3FPu3DPmz9xYDRLm1bxy134pw+3wOTA3QjWSlLecwExjq1g+gZeT85fEVREvnl5xmfzJiG8iXE5Hi894vN4Y4A3Jj4PHFIk/CV8Vr6p1xYVhPUiS2IWyIFwkXivr9L7GdzwzlvQQdCB0n1NdlrPWeRRi4xcxShfmdS8yCrhN9bbcIAraFOOOkTdkOMLQElayfNT+vfNfyhgS46LcpCFnYr5GiLnMHQjVC7+6/TF4ThWmLwx4n2L2p0GkwPPX+7vkvJp8+6Renlwmf/FqwM9kp5tXh3bG2P/sulVs73XHtyMvXbfVMqhzcFE5sr9/DirehMZtzYwNWBMbf+ElyLLX37NrPgX+xevTP2/i16QWd5hTU8QndOjBaOd04nmfgmxu4kQ4BflJn2NvqPJWiTL9Zz/KeTmxkeTe/lhvwHPiImP+FKottkRtxpXFWJIA+iVn+t4sQrjPybm11sGHbIcs/qa5l6jB9CTL9Y7+42vB0XsRpgH6V228wX3sY9j4N7xzHQtMsXMTRp3rdPkLZLlei+0Cbu58SXWdTwDl3wlpsMWk5f6vDrltxFmBZTrUS1RDqtMU5kVfgIdZlL4PqrpBP12mZP+Z/iTvzFNFArLQIp9i3VgX1r1qy7B1o9v7mQohQUy06NJek7upWRR6O/nSTkRgy4+HeQ1m0DKVf+Yruo9dyM7fJFbcL1XpvZclFzNgzONb9Jl6IZR8vRRV1m6UYwcvc2gbiVPnn6BKl0DSvkyVAbAJznwZMBLEBMR/vxSLonPzUNzLzkFDtMOhoIoGQ5ZvYzvhnyCr5keyOSLmOkRfCG/Ox6UvjItA1x7Al0t/ZQOKCvJKy8LWQa62hGDXKz8VfSUjsorn/tmFeRiRwSKL3nbA0rqbGf521FS5BqMPRUJDj+iIVH0lb102Q1tZF1M8bqgUaueYu5g4IAuv9dNMet0S5T8PH4x3PGaoRFDa3UdJx83SJ+9pacnP6w8H8DXvhh9/f3DCitIroG2/qCCJnovkquvY9ivAAoaWDlU7EgCFns4tyZmk5zAPr+dlyW5l5+8qRW/Sv2ZfRxEptC8SIn5oZr0H7oPzpMz0yKR4yxl0/yAKG8S6KEEvnoXuFN8HF1Drmr4NXkEIUzAPn+D9/aIaRt6c4BtG3svujs0XzvsjU5kgSGfe5g36aqL7JVU8G7erZbTUhXiwh9mQntcYlMfEmbJ3kXhhw+QRF5cM0akscsOdo/paQGSlvXLxgsxhQUZJkNMwghzupG4col5zrNHvXS2yraueTOqd63Ltb9lf49LOIOxPkELsTqu8aT2WFNR05W6HcIO48pw0fDBqUbQcvSr92N9fDaw6JRaR9Jktkl9UwZ1pC68bodcMJDE8DrcqJjIVqWqBeKC5qn2rv2en5Zis4MfuaL/8oAdObU4HTDT04nsTTE7hLUaw3SvjwLa9/IicUcwr27Y9tDTb79m6oIOumW4xpYXFEaNMcmVZCYhDez8hF83GeYaPuw8Ul+tgeoZbYzY++ZO2oJL5JvutifP2ZpUaG/H9Me/fFRv69vRZhs60DFqI+jhLxbvtOB47m4GfrGBSL9qaddwFy5mCXTkaFBOZK0d0cHoTj/npI8xz+eGG1f+Nx6pk5H3LqyzZSuvoOvvZMh8dO5FvqvbGfQQT0fHmnDPUzQVVUKIB4gtfHQY6Bm4IRezzlplCCpsEI8Bf3PIbn9//4uWylBmaK4gallHCUQXGQvITZMTht6YI2XynniSWNVQPdGniKK++A9la7vYn1vwahJ2me763JiUstQWge5pYFzeuQy1MVKcZfwV9VxMiM/b5ZnSQ01s+HVkdfrPdAqLgq/eSjjdK6pkZXHkVKFo5Ty9pTXtcEPuHlxpMbDLks7Fu2mMjS113681UB7QPfhEUbrZy0u67b9HeRmNL+GJ4IL4L8EZ2RjJhN7cmnzXY1IDbDudS8Mtq11ocSOeeeTkLwo7jFFSIc+P3b8jL4qt/30b1eXgTal/h35y8x79Co/xvgjFDMsOEw4Nhzrf1TvYtdckBuyxrHttiIyS0+JT2Bs07QFaaTcqgngwIzIq8g9gNx92D9YYMLiHtM6Ed9yKcidFFkSFCGz0hsKhqb230iCLp8dLK6J3Q6e+MeCg8IC2V+B5z8elpyUZ1SCvSzROnsG8fvX/RFWBmqryYF2FsUA+pqD7GX/XrHR7ew8c+7rqM5EN9yiMYdFnW32DEhvvU9+6RPbEsYi0KDdKJtUp71Z8dlodHNMjOw+kQWFI15jY0uajPuGtD8krrmE9MUyHpHivsNwo94L/YnMza+EEqUh4KL7ZDxR/CoyaT5oQ+v0epTbDdjgiOyk4KCUrFAUBH/ey5qeEfr5HqQuz1i/YIZ7JmAjEWOap5IVdKbv944g+en7n9zjKZJRNITIzOSg4PZsCYMfNsaLRX+YUosu8j5hcctMLRGJa0dXo7rMLH0WuKrWiYpGqiB9v49MAbdnq0TTe/hrDsVx/2dA1LpDaO1lCqSGn6/tj9WDtxmRVDsR5Bc9y20w6/QTs3MT6X9LuGQxyiV3LSM0J8W9B36v8P6Nd8RsKo4gLUk35/iw+pRJ9vxJXc8YnPNIVlPk+IFdMriHEDBQVna9d/fFogmgUO3p8c7urGFXqh4s1irq3jnT1taHjCsb/YfuExpCVCs1G47Zj2FoosIG+qSyIDvD9ICyC6lP6jEK9GH294vzOEIWnTdaRSE+unY+9eOI4GWdUxkAx4v0j44OgqRCa3s5lnPcwJ0mje2VWQkjuV3NK6kwYWkiffGeds+szS8PYi7bHI7EmlfUQa1nZD9pBK6Wi0tx4h6NtyUXI+bEACrvrK549e15l6JtfIJ31oTajK9Ed2k++7XE+NIQ4I5XUX6/j0voHe7tBJTAIL+DMJ1LFrENZIMujF+HtIphdJhlcdSlF30I/ODv8Udj0GAMVjhT6rg2FR/YNdUMbXK5g6FtfJJ7xRSKb0BU1yVmqHf7tLS2jA933iL3vH1WnnzJWJDUSVO52MDqYBGY3o5s5j5iZORvaIaQKVBa64ezkB3FbhR7EoaGPMDUYJxAHHUJ1QkWFokgdb+N/kiLF9a8Tr555mKB3r3hgvQta+zdLIpDYeFQgKk99fPcAQ+AKXEFSoie2vOFaw2VEXxcxDBFw+SCZipihS2cbWzb2Kv+Quhn+821KTidRR7HL8WRKQEpXuUxhbg9Iqa/0TUZP9lEs+2XQg0RiV0IwlEk6c/2EgFhuiEF/ar8Fn6UZycAOcYxDBaJzzhZ/OBIuFsmGHv+y3UfuxJuCetO5NfI/Wn20PSs2rRvldodbe3PgD7d1sHJMheOg0nP8zZ9uKe0hf67dC4rIqgkgPVNrHXlUfM8XVOcSi+nFoOtb5zeAnlNqbfSB9Bds4szcw4T0CfyI743rJvmGftyDWqcjmrBwYOHcm4E/+M94rpfe++jPozevTyziFw/8gaeVfCtlldbSa11vT9wp+gbUGfbZndkyetmjkYGl3+xgjfKznJ2gTwyzhhfpi0ysD3n3qe+znc4N+kbRTtEAfUDanadY0uFzRtI6L+H0PfmJeXn6/wqv4Ya8GHpg5CnLrjWb607+glPOpXHq2/n73/qH7PGSf6ugx/I9xDX6b5LEw/9QGsDyDgIr2Fskyxk4mIhuQ8MclHNuxsSaqPZX09y90wqgxHXGXY4yVPgEgp2TBnU+NI1b5iHM+ykUOPbQ5GOlDtWxhTpUJdYksDNSoc6aAA7mAtQYq3OZ3U7Onf+HvbJLw1Tc83cFdyourHrh/DfCp8qe/tru3MMiDnF8oGo+oY+uB1mm5KsFOf+UDuPCQ1zI1rXeEaGcSGQAcC6S1i4HcveunHZndIyck+2uxuYsVa7xqfMHNuBBGMgxhW+PtWxvt7ec7qrZJbExlcBmfEM0wfsvWpMS9fneEz3HZSDuJRLAUacSCPX9M06jRJtdX+GVRHIoabY5tkBWinbCjmiJakdRElV2XajJ1k7kYE7MWtEZKCs8sYP/bho1baFMmeBcFGxPIyHNPMZZjMR4ZKR3BAthYEyO6Om0o7uVchHDE4rwvqPDYHIXItbcgcOW2TalRDeTkirHamnGHTBUaGbhhWbi6N/BPDLLdwX4aFem+64z5sdVgz1xbhA0ZuEfcb3v+pfECA1e61DgPQ1NXkOajOCgSB8Rnrk/VYMIvQ7XbnU3GwhGusY+WXcLbrrmOqVkU+g5/i2u45M5ges/ai6/8mjxOmJ1nxEZCydYLINUDOofBEFRIqNylqlHH/LwAjQs4jDkII2DyEA9iKXloUVxpLYFZEdqrJGv2gRR5J2sDyosU+91AurjhUgWesq43vPZ+EKa8QthpRQJFpPVky2gD/KAgOknl1TGdx3x+2G1DWLBnGHKoIlTmHC5XVO2OcD4I0RU9rv0adWBuTTmZ+KDqPMzdVfhOOlbyyAzEvP+CcpAuevWRMBw27CPFOgPyGdLJrDO3GyQdL2P9feuUyOq30Tfgoa9k8EoqI+Py/KCYodhH4tEcGr7NetDSvUvXGv5MaZ8yE76NuKrPWLcp4gaRF2qeQM7Yddtzsb2AQ3E3HlX7oXDG9q+XfqYUgMKTPU4rYjYRDQHeoXJsIFNxzyDYvsVY5aom7ipk+1DLJ8PuMY/dWkbFr1ffONE/amZv+0/V4L4CddNBPWTfaQI9PsMHJAJPV8mv2tGswGsNhYGIxn2xcF15nXTKz7r9Y0q2nEcCeoSXvsio1525UDjo2GkhgkKqP8cgEP6GFyYSxCgW2buP3W++UWc49Sf7OITADz3C//qgDeL+Q+/5GcuTtlOAoEpKIAChhNGLoCp40vaqEMVf7aXH2oaJ/ezjKA8QzItCORJLkOTt43ybl1lL5Ip2uj2vaCYfA7HG7iwdH2UmU68n/kCi/FHmQxabF9A70tcNYHd80LRm9C+9Y8Z3RIy55RrT4TgR5ox+J8LE6xo6fSHB2TCgcOOMn3vEYklxlstb2soJgeczGQZi4OsvczzuS7hkEmFiEtzC218ZE4h2R4da99Ee3btCxfEcfO9/WavsaRjym9c25vhkFsHAdpygeT2xHIMuuqOpuLzSfSui/1VVfb+lDkyDtUv1DIMcZKYrgQ+VCnP8RpIOvqCPIxadZc5z3b+ORH87+JZoD4znFXo/8mXX+oV5O0eNJz7kYLyhiZidhSNDOqPJJF+nlRO5Lfvl8w9KmtqZZx86lnudH9oBeNOvDNR0bX30zlFZxf1KHCdfw7dkw6HIelBhWL6OZC/ZMxLxA0SPYGClib+yBOJ/sZTVtaEyjiHKOZestmoaJJQj4a8lUB7qfJOqMxNYjaVbPykdjNMvTbije7qcASRThyYn21uZ5n9sXAqDzkW4YzmjKC3h+4ppv3+kXdKhCwG7OYoq6WTzb3010rMMaaYZ8jWdll7SNHVzPbma4wdCVIwY+/i36+TQKa/nlwsqDt5kh4ZUhcSJRuyoIuEyVNy+RokaVKlb6RIKdulB9mRleRkT9Zy9JKTInmRJ7nPl5MsGZadn4qOr8xNaB/ylBryJ2c5Gloou7h/r6SoaO1LjfYuT7WV0G8N7zuPDaQ3uREI8INMIkxgRdtPsiLUmthw+eYmXKAhgQdwEJB2QUiw3wWFR2glul6JoV2wDqApNjTW3V8elobo8eWHIoA7V26CyFClTMUFZJzjK8C9OPdvzAeJktIZwCtu3mwHiso55p0FIAflJOTN+XK8Iygl41K16C6Yd4tyTZJebmiwDgQI5G77gEBpgprjtLA6Hzc9hCIl45T3Q3DM1Yfkldzgd1Kk0n6dKfEVwFV5qUUSZz7IKvBe7SJjegyYsAi2IkROGVp2VM2DEw==) format('woff2');
	unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}
/* cyrillic */
@font-face {
	font-family: 'Roboto';
	font-style: normal;
	font-weight: 500;
	src: local('Roboto Medium'), local('Roboto-Medium'), /*savepage-url=https://fonts.gstatic.com/s/roboto/v19/KFOlCnqEu92Fr1MmEU9fABc4AMP6lbBP.woff2*/ url(data:font/woff2;base64,d09GMgABAAAAABrQAA4AAAAANqgAABp6AAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGmobmnocNgZgAIcMEQwKvFyuQwuCEAABNgIkA4QcBCAFgwAHIBuKLQXcGOrdDgQJ1dnoKAQ2DqA8diCKisF09n+ZoB1h2u8CBzaohVCEaEVD6ZQMOzVN7qJbxURtyZnoHE8x9FL336xJ/tR8GsOUK2uSSqFeSjwenh2hsU9y4Xn+O6tzX72qSiVpEOlJQxhC6fUIaw2/lkZ+6k97AXCqA+I7E69YIVIxSfrPez/tEL/NP8qoGTjFhYvItxZ1ujBqgTZGLQzEnJEzKtDGnFlYtSjjidE860/083y659tpe2fmuU1WfI8zCBAYQQhmhZZY7C9GbpaAME9Qzm5379yb21e7GyqNQm868KaAKKqTBe0HyaX9uLQf3OD/pv1+WrJz20xCKc1zEBJhXu+Rr2avyR4cOQiDR0gQGv6323ie/l7TnBTxBINx+4VuQsFS8U2oVLd8XLEV0531/3+t0tbbP70DWAsBXiQhV28s6hjT//+ayt769Qf4dw3WhHprmWq7c053EG1YRUc1hKB7o2ZyPKEEVhEybuNGGBdQmwirYhMJ+Obr5MmhYmydqyxdapyHu37x3f591bu+Cm+uIEFCGsJBxK92H9ZZxRcPzEKTZhj034KgCAUAJlF4yZRJKOAj+PkJFSoIVWoIdfoIAwYIw4wgjDKKMNkMwiyLCUvtJQgK8AJeBBDAjV8T3fG90YfE3/bjB0j8a3V5D4lfetMnSKQBgkECyOI/4ydwAQIIGgUo3PYFhAANNC1LetbD+35/upNO/DScaf1pw221jOETcD7+tfDpxIxIIMfz/VgU5rhaM9Pqtnqy9jp6pnVqt/S4vq1cSVsS8mm3qo/ffV/u7VF2w/TNhhkjy1nl8yXzH8OkBIqOnTrj4OTi5uHl4xcQEhYRFROXkJSSlpGVk1dGIFFYHKSppafvk8++++mXP/46d+HKrTv3Hg0MjYxNzSysrGF7escNcTTE07pAS8JNSDSi0AoiBtSg85YMGjJs3SKmZStwTHzDDhyX6LQNDhRPq/zTz4HmhZuWqEmuISVTVlVTL0S9KGDCAtLS7z5v3qBEw1ZNYmaw9uZ9w5enukriwKMnlkCLts/NcvVbT8lTTqpq6oWoHYUwwEJa+t3nLRqUZBjLJGaGxmfWnjfx3sWXze64eg6bTcSQa0JJWVVNvRD9RUkYYCEt/QzaMpHMsBBeiIsVgifvjS/MEdOyWUxj1oxhjLF3jDHG2M9ojLF/jHHA5r5nGIqNO2nWKT0HFriZ+SVYHhfkmVRAy4IevQvcs3gyDHAs318qx601BK0l1g0iEXHuNODvEiftVRaLWU97M5Obia6jraPtp2ohw5SUV88zqtAhmkE3C/mGPTrnVC6kuwlaPHZVOi6Zw5y2IKZqbbgR1GidkUyZAqIitVC/GaFp2XXKadMcnnY3yTPBbxsGmlNpXg/nwKCGYSnggwt4JsEmTYAHcPKomiyMFkxWQrxa4DCDTz6ERa4V9nJZoVWlKWVRVVMPUQ4lmLCAtPQzgVmWbdbevEfcsFqHbLJoA240essnZiVPWVTV1AvREgpoTFgUrgEIaUHfxMzam/dpGbJVXeMyrcMcWcH+UyNfp0pfCY0V0psWI6ZGk5VPFlRWsP0tSmZQNBsICrQRCtqsjac17UDa1BTSLvcFXJUQuhG9MsaCObAC7SEhbVe0kPEWStwgLMBssWc7uXwr8INKiGPTC2HUOlRgh6SCcTZNQdeczLBlhEiJTEMrfieS6SuqTqXnKLgrlGCU7aCiFSg2ptFfbRYfl9HnJolb3yRqa6Eue4CWDNISmeraLPn6/w2R6q4mGglAjmY8/XacOsI+JuBz9wm5WeRZ3nr627kyRsLyqUhSEekItlLqsyFXKiFnKiFXsic3OdsxRDA2YI5SX4E59YAQqAxmkmigZYyV7CUCZbKnzuRpRm4Q7hRicF6+2ZkAgAtoKhgYYBiCm7bqbSHZeY0+6zxoIvfbbqB1QH35ZgbAMMbor3SP1kTv3iqZu7R/YkxmwxVtzT3a4F6OWI8zBAyiLIPWJvoq8GRrw2Oic/Dee98yOn0YamgKPEdmNbWIcVk7TV8ZpZhNC353V/LtdYX09JWBSuvOBeQEOqNGM7QQr5uoNJfbHjR4e6aM345Fy5lRrYrUbIPuWPWC3kdLD1zOSqi5+3jM3sATajpGXe9ReQNm6ffCkisgRi6IRr06m6pR19NJj4ysxSCzwtoH4gNQN3MV5nKZFQGXsSFqW1c1oJbBkv5A+thOng+i23IKyI+d7qyrbXLsnLFgrnbOX/KXLVinesOCnXroXkKW2WLuCXTIMRFH6JhtAoqksI0hi8GiY/diCSC2aAloYos5uHETRxF1Zvq9AAHCheIlBBMbMLCI4CIMm0islItPNjy6fIa5XChJEmJ2HFFERsMEGcp7DAxC5wUGIDr8ShCEknwWKx3ZTfkmodU+cuHBkiQdCTbeu063hREbVLXzpesaZSqwbJY9Ozvm4w318fUrE8yTVQ5JOz5f4yVXG1DvyUX03tH9q0VBS8h4WfwP8+bkJp1cDdB1dUq6dAZskQwtx4/HDeJF8yMQKZyB0jkZks4Mz/QcyOlvr+D/0Ojc+NOVEdmfU8/d/zqpnMA/T+yM/iUSfMsmgV1ld5oswxSZpmr09i9umFXfc7Zcc+S1PPLV+Rq3bYGZijtAL1kCn5I+cL/KvbNVGjVptlqLVm3ardHRIehUdCl2K0bqkaNstsVW23qu3Fjb7bHDOF6AtIOIKqDZrgo7oaUNJjdpVUdGkzbqmNKk9QpkQpsbuoCp0M5lHiCrSXvrmNWkQ1ZhF3ToBjAbOgw+7ckND58zESEhl7DIJR7vFhW93i/uIRBuJCT+HDjgP3js9viJwMmTw1OXQ1eujK7eyNy8Ob51p3D37gR6UH74cProUf3xk5mUFEEkLqSlSRmZpawipaS0V1Zeqah0VFXXampd9debN6Sepuablta7tjbW0fkYEvqJXuz4iIeBjFec+TCRoHChgkFhAAwCJBS4EPYWeAYgSyBjAJgAgMJAZlLuWcbYUza8fEXmGjllRKc8z5iz5maWnNBI8HZlkeAtZAvZbv2N8vJFjD5HeMYcKsl1IogcntO9GFCwoorecwUQApfx4YtHcAfucYJc3vuEt6pHAHyLqwQcgOHFgyTwmPNgUNbKfUCphGN8jJS34pctcnx5lxK8enYfbsNDxhg4oXx4mnOdNN/oyVlsNvKZbZKmFVZq+Uq2SKWPAjs58QJ/46mTPkCi9+hQISPqeIhp3dZ21GkoGymcckySxmBfLWI0uWwqrY0XVyxSQKWQCxJP6lDZfXNzGSiEpwWXqWL5WC5a1lalRb99g8PNja9u0q78HmP5IlZqYdSmkqrD4Cyg5j6CU7ur6bJm5YjA9rY2cnLKZyfcnInikdQ/9XjCMGmIvK8qdUKZhECZV65T5qkPS7DhXXTxfQzwd+FqSiTvKTOLY/e6nZIUO+VZBDoahcwL7FK2sXOYbtjfKwZh7S9svSoIMS8792xoDOqYFGfCIduV2dNoqMXBPf1Pj3Q4HHI3YtZTSM62F0oJxYvkC9ghA03q9DytB9WwtrLleFtu5qMf9rkPZnkouhWrjr230MoB+rtZZyqnm3dtNsWwVoRH0dvKbkZZfPtGXfYYigr4ZSPv9WLYb3tlYozCMgFvzEuHaL/MbtvMdh0CFxOaC5ocBflzKhhunczlqM9dRCFEP9qNU8ywAlwwrIlrH35E64tWZ5Zu0VcOt2jWMdt1pFsZeGVuMWCBJ6xnckoFBWrkpGXl2VWztJ5cu1r+1Sz9sNQi3KBS1Bafpux1IzcLY9TLv6jHuDT1sRK4/JZoEBCSq0mydCDXNqIBlcM6obRF2sCt05HFGPNae3Kqmig0yhNuY+1QP2VNHgdzCPwc4MtO/UPATdpt3vzZEZb+sUzDyWbhUjibAO9VJq4Gxzq+FEZoD81kPnWSvcZiwSfcTFtJY6QdPp1X2LjKvhLQDTEdt+Bbx8wCUOecfPTNFAVy+oCmJSV8aeLxQyd9weAe5UM0KpcIua0ys55LCuiTw22RghfmFt1cocX3WCVPjlQP/RaxysBf5yylxaqYpYjFCQKF4wR4lR0nC6kBr8jas/R1xOBcpUnXWdhHchs9r8fF020RsFD/Oz6UtPX2tw5l657c/zErE3jcof/7adpaT+ely/ktcAmztfqzNVTKGyB47Kpqk5mHN6qO8x5Sh7iMxSF8qlJyA9f0Iag6iAdsc3AkvOts/gGAIMPZbdapeu8eiW2Mph9o2MqUz2jWZCelyck9LVBeJD2F69Nc1GNW2J7l92Qzo2KzzHi03AtF0lplebRkychVHwyNj2ELRpPcelLOuKGyjvyU9CekdwnZgzDGxJPb4cH9X+O09xpwPGUxlKUiBEVTzoLsyVIQL5NsvhqFwgsofr4yS7tOzJKHayNpga6tLwvGrEuvLsLUnHItEsVFMstAOelkoyFLwy5q5IZaI/P5ebQfhZYO77+VM60AR2/FGGoCHKeQuwxlxWM3dS0GpeWL0sKA+1dVb9iqcB4zpE8ds6VzGRzOGiKBCICjI1uotlayebLxeVtJV7pTr/VzQzSkg2DnZWs69QX2YtU2xdNM38JBx30TKBT/EJ09j/2vkCsj+DK607cv9K5gTunaSZzYMlsoIexzUCZ9FTXrtpi9lR4Z6SUSDx5afdZovKerEuDoxT1lYRpA2RWwffx9dgGOvqvtr80GyWyAo7PBJS86e3cQuzEGb033sm2sbCyNqN524KZNRmF7zdPN88/P2b120PN4y/kL4FAbXUJaU+KG5tOWpcfF3SRg/XKJET1x1/G+KgPKk2fGwqWE80uL8flRj8CAyuhRmTBdsnBu5GMwwsayKlDZGZUANa+2Wo3Lyi77f1PIwUNj/O/o4vrh3cEx8aXdC11XfAJdqJTgAGfUbfAcC7fDO3Bzjwc4E552lf/El8Gm36fviN4VPdX8u3LwBDgSMdjFf0kUYPZ4Zayp2em9VX4FoHIl8qmaFllX1VTlMfLGNq7rlXP9RD06fzbsjIYQulDOReZfj4ePj5aGrKYKeC0yc/gY63Ca095EoP/vVQJav8XtlRBvRzPxdnd6rPttEQ8xmezgL+jUH8lp/9EJfEIgBCV+R1be86n8ndtP5Txl5YBYBhSXlVGYTBx48PUVe7RHOt75x6ZwKtl5b8o34qcggWeWK8VhB/aLmha+ssfiovnVbvo2VGUENNjb+dcCIlp4Z+pcY4mZDQoBuB/j3JZmbpXt46c41nBprnXUyMcO463OUTbu1eDHySdmxK3rKqfLX9ECFmoqmpcbYhFZY00NJbL8wztaDw49TNNQsH8VtlbW6tWvY03TW9TqdpoGugZyAPzvNRB4hlc/wff149hKAT/j5PF8YJP08z2MBPxHX3Dp5lrtkKamlITCMU2x1pscADeqcU2KSCESyUQiRYrY6V9WBkRuAVH8pRbgjkAIuFzOdCmKP/V4LZcKqWdynvJyUoBoA18DG+OIBKD8GLBhty8flj2Qx8JLAeEnrXbEVuhCC3ug3s+f6gkoQd7B3naSAbuwZ887oDDxlujMeex2EU9GyE3Q59hv+eXKkBPcYgtfi/jwwKDUIphzcKeLzfErF04Mj/QWSQBKw9dejQHzNi7Dg314Noy9BdcTrglw6h+r8Up63LVKg1Pb9MoY98CBzrwffGddQsJsrYOC7FGAYC3jas6+dD5hydKla2/vwU3kiIOrQ4SM327enqW+NKFeH96HuzsB+RPwiqG7RDAfx8I0g93l72QfF+B1oJQNU3pmAu6T9F2mG3MX4OjlJrsmDKRaP03WO5eyZOnI2NuD31pGXSRhc1L/d3LWNeiz9Tv/T44ooLlmtGrUYLTOedlWQ9X4sUDXSa8oZtXJDxI5YrEScUCRZZNuw7bJJVvfO6TKWZ59jaSTqMvWjXqtc7U465AINwRutR7XD7f+ZBlqRWLbeVTNGQX+w39vNbDLNL9sf9Y191NrEfNf3LalU50jWzveleqd5mzoGPTuc15nGh94gqOn+CT/6XX++QIBoDR66naxKfu8guSK49sEybLKi28NiseNcagO6wcW50nXHSRPnMADaPrU/mzhqBihvNwYAYFIGpCoR+rFc0Siovhz8qKFhGJyxdsRnpDs2sGqspq+fFEBREA8u3qw9It15mFXBqaLnJvVuQwUpr2Y9oPUM3Qc7zSY6WMPgM/OttGF6WdpgGBIZ9Yy2cwqpkDGiA5y7I8jlnNFJP9o61/5bwvKi+5zUYIf59QZ6N2/uYS6sOTvfWPzqAQPD4m6dl+YFP8RL9Db9TYhPhx3fjLG36ISo7zCE3KncGPoHjTnV/pUSkCEv1AiW2zpBCerdB8t7Dbocey/7A4vjb59His6Az5Y5c0jaZzRiQf4ayu0Jv2FBpfiOmNvCGQUlGTQf5r6RyTEJfdJm+3aCQ0Kz26gVgCOfrllztHP7/MCWAjw8QKn0/d6NgTn+eZ5jk7/FToNZLW3ojeu+R1Qenir1fCJs49C5rFoyaR0iZCQiLBmildSZoJHgYO9ZyE9wDMNBENf/szEhT7XpLyVNBV19CJF/0kc5Ozh56nx9qnnKOjsd3BtwQyDG8OUsXUsqBO4NpE/SHdZifyJK9QoLEuqQmoTYCjJNCGOPv6zgKhc6laKlLpul5wzuqbKcXY92yoHybHeyD6rgjG5eQncwwjLaVAVjlc9ro5co3c7dY/6GGPKkrJ9/faNiQpRkSSjW1hUlobylNKSV1GyVLjPCl6pqGL8TQfXsBALYg3RBm9nF4Cd55iPQAjd/FzRBW8c7fp98EQaghDKNRGq8L7cRfpzwMAr8sjlc8MivWDvHB6z7wqE9JztzjzXP4BA14AQUp5ZjkBIS2YLAoQRShYFgQLAA4y9jj1iT2q6E6jycw/cqnnx83nW0P+6eujGEWSY8EVV+lAuK4uhWDSA5pokfrhfYd2+6OKLduf6rxt6z9PMs80ucjMI50zeN8JFhhBqFhWhhougfz+5BMYRUy1Rkz9fjXS8QF5QTPQs9A328yFALAKiqhAvp0plQa8GgIAY/kcPtard74OaEoCA/MT3Xm0ko8x3tAVGC5ii3Gk9f0BraaLR6wyytjoAW9c4hEjbGNfbb1Uj5pg5RyL+6rYmitNzrAsZxla/3BfZhHjqzk+fUMeOeZZVttNTbSkaRs4xvL5q2ar8Hs5BwMO7R6KPe6v7dVdyiA+DH+5qRSCk8vRLz/K5gBZQS/1v/mAdgliBU8wA9l6ZwVmzeMPo2cY2VI2ONFlNcaVm/I3pG3DapoB3juJX8Rb7GV1S2ZIlGyJQb3HPsWHpg66mdthTNxxSkJSXlI1k84IHBW/sjHGKsDK0K2IY2MD2KTRhi/MM3Lg+gSmHOXRsbUjvnMK5A0jZb8VFwKMMyH67GQ3XJKfMNjajZigSeKowP22pECigGHhlHtlC7kmRXhb0aOm18auXcgZK6gAskzLjkQ67/s78jUBINPEGbw3fPJc1UCWBM2XlkmvejrUg1RCPKPfP9U9FUiWZtbJeC3puhBgpQIAoveTiToEL8FeuRBcBN0sp8+sQaOEuC9h/TNCL1PO7lFzXeKe+IUshoarpYW1TWiFOl2SurPjW3EBZh2ShpEiy0BeJ0OKTvMHDsw5wdF5JMj8/C0i9z/9QRr7KZP88g1tIpeTmFXBn0E7X21jH1Phx+xbGqcNT0zFL7336XNhWQa5eTr6mOkrhZqrk3h06WR8q9o+tJHHZhPX67Tql+8R6epqBOwUkO1PMy2mDbFvIMLSdck5iiL25iTWOqygMaVLsHpV/ygUFp+ku+eD+Newodg/bletACEcTFIo7iPdT9ga3AOpXCpLSl5uGVw3dYe0t7O3gWyRiaBGVywaYpZOcXY2BpdnJa1do4ZCtcXqv/jO5A4tJpPYwzbJVFZgvSPUWh2llJpW2VzbfL2I0rhTmZS4WotITC2UWC2PhLFJFidQ0n+/YRVgsi2OJKiFRVGiR9HVV9ey5yR5m8So66RR/tkL/qZhn3eLM7rgsSrSsnB+45kb40i+6a1x9NVF3d629tgiP5lh91ZLu/1/bgMPQA+NwOXT1NLoecnWdug1Qe8fBdxcPXHSXw/TXKYdhxk+V2ruQ33NcJMw2yqCvdynRfq8HuAhhuDRbixCP3efdxTpcjw7NZrT/KlWfBQQSnUIB6Ctr0i5nJ8OMroHXDw1AR9ZbWokepLe8d+JHce//P8/rgaTB+NIld8J8Fp8uG1q/6O8+XahsK2R/vXbzoFbWY7f8za29OMo/6Ln1s0RQTB6ap9FvlOq30P48/EIAlfX5aK2V6R8N/7kSHgAe/VHoEsBrovLv34ndUW7NZzVwUAAI2EveDM7iSt3ek0chrNYYPJeYowr5H+ZokeXlcNWal4C/+t0O7zbXZyVfeTg8stwwYF6lOJx3zh7CFezDSjPGpPQbvT1s3RI8n8sMz05bkqE/FmnnM2pjr6n7cit2on1ni7VZ6/I67fL6X2ZNtOC5gO/Z5/KerLRuc7Bc7G+L7LlObF7H1C0O16185aa2pxtefzlYF7psLnd8/nK8E8X3Xy4vm/01wuIZ45r8IT1bTc4++84ke/O7vF4nNbGG5rhAltmXkzLaQOqYQCxyfpNVr+jnlVjdPNP7MoH+1mvFSevl9mWBvVqDkb3HgayQl5X3J3Yd0y856E/54MK7LGstKnmute0psb/yXY5S+A3zKhZn0npuA1JBC6zCk4tecGTUt+DWdyMXqPyU2rjoXexSz0We8huOWwyCFr5FHWRtgsSbVx+eRN7VuK78k/uQTIeSToBB+8QaxkQACAWF8rMQwMU8BqIdYBquS4RE05YoQtmQa1C5Oi4xyRWAhcQde4YyxuPU/iVAG19tofkWWCNdGb/S+2Xpui3nYi8xd8Vey8xWgtYFLekcVaBW35m5VptZZZ1iDoVRjZiVaE2QbigRLbTWUh7mD2aJmSyNW7DKagvVMnUq2VGuWr2V/GqhjXwicU2vUGfIna1Pl5hphWJ2ZcO5Siy3ynws4TF7oZbZxGoUG9CrXadhRmflU570M1hdW3CROQAA) format('woff2');
	unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* greek-ext */
@font-face {
	font-family: 'Roboto';
	font-style: normal;
	font-weight: 500;
	src: local('Roboto Medium'), local('Roboto-Medium'), /*savepage-url=https://fonts.gstatic.com/s/roboto/v19/KFOlCnqEu92Fr1MmEU9fCBc4AMP6lbBP.woff2*/ url(data:font/woff2;base64,d09GMgABAAAAAAM4AA4AAAAABeAAAALmAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGiYbcBw2BmAAWBEMCoIYgXkLEAABNgIkAxwEIAWDAAcgG74ECC4GcjI+VaLH1qPessdqQTg4OPxeLR7+f3//P9c+51y/9z6g5CpVQKG5/UiMzionhUSE8p0u/H/2V3fk/tMogiviDeiQN2SVtkk6fas6eeAZl//gEhhvUFpkm9sa3fbWF5pYxdLBhuju6irPf3Y1g27yfUt4eqABBpboskm0NtCnFuDEJRhDIBCl3bKW61OPJDlzsg8Rkm7AACwlIYSEBhoCgZqpenHrRqeXwXRPh71g+qfIA2akM+oHgwsdAjFMYT+ECIRcQqhI+uFiCDW2ACGUHKL01zidjdEyWkF7BIAsfCn+KiiWixkQ6XOQXdgdBGSY5wKdJc1zY+hPN8bNKNrsjp0ko7A2IK8uDo2ixAHxMBxB1MSZuEYhrwYjG//FzscHAjTAMICsS42SkFQs9V4PKINuVDEL7AHdZcMCkNANZ1mW9e6OVjg9o0hmnG6hZ1vhZnmuc4pvv9aOfH+/Okoffhx/erdqZen9+P1vVyo+1I++iUNbEqxN2iV4tWrlS2DSM8Xhs2zXNnix3ofr936l9r1thWJTMzZxsOz5+PZsTZ/dkxv9dufnuFv+ar6+z25Pn+25uZ6a+e/zuH75vnhZ+fNhcfX67c5/g6sWx2df9T9bof1oS0pPjcyNXax7ygTcBqCs+D0joT2sc7GT9yqQ8n0+l77s2KLo/lcZXAHPfvaeDLyK5e9f41qvqhuFEUBVAkF55L9Bdfh7XHtedRssAEA5dMxOAOKPYtbukI4jctUehoWykwaYxROgmySV4QtU7MYR4So2w1TDELtq0m0/nb3pHGphgrpaMsTpM7qtdGmBgKCUMBcHpyglCxMzM6UxAQEOXkRpgt8RA6UBXl5Ki7IXWbNJBAmLI8cMFgUcCsghgJnyMZcYn5J3iPE60LAqhkW4PL8JDKlVmy6vcVupXm+3fFRQJ+PGJF6VwYGgA0eciEFAmIORl8sR4hdBIoymTRgyYtaSEXoWBiYcoJNjHQA=) format('woff2');
	unicode-range: U+1F00-1FFF;
}
/* greek */
@font-face {
	font-family: 'Roboto';
	font-style: normal;
	font-weight: 500;
	src: local('Roboto Medium'), local('Roboto-Medium'), /*savepage-url=https://fonts.gstatic.com/s/roboto/v19/KFOlCnqEu92Fr1MmEU9fBxc4AMP6lbBP.woff2*/ url(data:font/woff2;base64,d09GMgABAAAAABPYAA4AAAAAI7AAABOAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGmQbi3YcNgZgAIUUEQwKqUCgdAuBSAABNgIkA4MMBCAFgwAHIBupHSMRNpvVqgiCv0qeyNgLPAg+Qil1xSbBZ8UhIQs5zXS4/qr6MHotaoT2P98jJJn1/3+t8r7/fjXREFXzUNVUNQ8iqD2tVoZQrgq4rJAhyS5CxsXuSfMAPw57gzobFOe0KLSVNocsMPngBKFevDz84Wlu/253G+xuQbRkmtSfGAMzFmSMqsF2wN8GEm1EYM5IlDZIQReEQZSRExOU+Gj5raane/+dSmBxgiwxImQhPNKEbPcFQP7Pl/v/v5bauecH/vT8EqspsjAVuhWObd3sD76dDfDMhnFS3KQEuy0rUpGprXC7RUAVVzq+wjkiYatOhs+6qUd3KSWWUAf39yN311zEM1LQj1r7nwZE8AAAYIHgVKUKiYgiDRqQJk2IRkNMJpKWRrJySJtepN8asmEPIQLgArgQgAAOhi6y4sXEAipfC+4uUPk+7fAclV/CHq5QSQJQ+IcA1Pz27go2AAGIJACCI/IvMkYCVO8VLbKkaN762T+LEJFaTct05ax71b26l6IQGVVPK70cieVd6yil+Vbd1fSSIqoq1WrUqhMUEhYV10jTwpSUktOhR68Bw8ZMmjJtxqx5azbssAslTUPotKCMMJlRkwpPJnTZ0ORCIdVERefWE2kSpCQ0voKaUvgwrlvfdtUZVVuqZ1hH1Hrz3ABmtNk8M0+KjE6tk5mvOmjALh7ReZSJ5oV4hjRkXFMaNecdLW+05DMjExIaJXFwqYxI56VMumQjk8uAhZyWqrQCz7UobGTINsMO7Ua7VzSfxWX/zXlP52XkrURGSe8/lS7pfJLJhAXQCqONCm032m2kLDEmovUiJqTYcwJJUTkVa1I8anqqGbqKSfukFJcFDL2WtzY47diFeL7Z8KcjhTImnB46v1SeEEJpl83onh5mgW9gdVEJ5jNdxRHK9jHz+CmquYM84DgIAa4hD7lWDqtcKdOcD7loUMIExlp/5Dr4brG81ZBHGrk15w0tCkYmJ90r4g3a0Z/+GNdJJmSNbpJCe5uIDhLoqLz5OJ1MMgCFUX41Cu+gIF+QnDds06c738IUFLoSZzL6hS+SCx+Imk/LHhfGTTHLUCnLaJ6hxMIYVlah12jWZ4Q6Qv2+xOW9AHfQOpvRi+bOqYtzmQ9qMcZJ4w/cIX1jIijr9h1CCSOdCxt4N9yZF+AxrROLnQf+4KOTlwXCWAGA4FClFhDfWktNrRXmziw69LIaMM9t6dO6xlrrWhut6+y8UxWpMsRLIMJMWLWURgULxwqmFBA7njw7nAWAjcByc0hw+drVvTljUzFSbYiFGHxJ7AuBXDJlvrDrCjbe1wEOkG2TBBTDapDMSgozFMnGDgmpvz+LsEhoZK6TPXP0/PqmgLI4C4DS6KbwO0cF0HTvPHf9MPxqehg9u2ArDvA/cHQ5UEBFawDI0pFSqRjOK6sxmsKh/Uv67eRL+TBL1OAu3zv28S3dd3fr7trPtiL/gLvzLVfunfvkvrrv7bbdM9jevb28DqAHC/DJJe9dFEipkxFqtQHm7B4oV6FDpU5dulU9wTdx2Igao8bUGjehziTVlKBpM0JmhUXMmbdg0ZJlK1bp1m1oscZgSkhKScvYlJXjAoD2AQAQAu4G7gGUA/dOODoE93FUqtyvDHJ0Ch7m6FF5lCYZejGFaVRhBnMYwjzmUYMFLGIUS1hCLZaxgnHX6shRJ3iNY1LwOocqeINjSvAWR1DlbWWHY1rwLkdI8B7HrMr7dACEcYgjzOEYtxF13Rk5VgXf5dBVvqfc51gX/ICjRfBDjjWVHymPOQzBTzg2VX5Kz4AsnuM1tvAO79Hq+jzmox19d91FwMv28fPRLIN8A9ZFoE6gGlgAjQVGZxBzcd7EpnPAg8GRxFfIl8Eqc0/WrggnHVFQo/eJxUc1SejqZAoFodJRTRpGVqeQUQ1MjUzWxqgIcuaQEBjJ49WQ9mirU3XV1Axs9TFdHbI5xd5Mi0ZWpxq56NIpVhoUTe1ZZEMLC/IsbR0TTRpOz8Z5cerxgfQ4KJ6G03wpOCag8KnBGE4tGwIiGUsuB6JBNI5ALVNCYk44NvGrQFGcRqADDVwTxBIcJdKQxIYhdCJGhTuLRWAPghtDzRBQcAFOFaBwSDYqDHrWKwlC9X6gAjKGi3oH0dLhfg4kHhipGOpjs8T9vKyXBUR9ZvHxSI+m/4sswgifHbCCQ8Ep6iiZAmnj8VReLCWOR4mPo2ax6Ox2xkQhNUITaEE4DdIGIWKlEisbY1uIZDK0dBxii+TwZqks5jKzUeHQxfgyuZRVqoi4koMjcnnVxlXD1FHOKOMQUDxEkgBE1FTEoLyMjBGwmPx5OOaLnseGasQlJ4umzY8A18QxgTCCLsQpEDWs1iQ8u+11uZpAA6cDmpAmouRkN18b59GBFhQfB2kCWiwuH5JLZIMsgEKKYQUkVo5wISxDJGcTZTKBWMFBlEpcqYRgjCAbZRFQWDHOYdFybQo1LFk5wG1BQADyfWxAgTBFP1RWPFY2TADyRw0MUHCSHGX67CdoaRMcWnz+QM9jc3mi3jhhlSy8TP5WRLRJ78nKKAOZRPa5JgeFsLL0kD2dgMJL3dxyNphPCURseR8bPJfZYbGSq6dQBInu1zUKeNK7AUUmA1RdEbkxgFJ5eyImamgeDiJlgXQ2W15WTY4SOsnhKFw9KD+GS4NwtrVbHSWnimeRoolLekdSv39dhX1+CdSWuMMo4N0o4eIkUc/PwwPI0HJYDX+jpkC+3MaOfQN7UHCCAyk4R8FBrTmP8cKI2nk5YbdD8v/kQCZjcZ4rIXHXMKeCsDqUkQZsHEWSHzWRQkLVUEgbL+sjAFEvWspBUDXxRHYL5N4L9+4EObLkbNHM0YmDIiVOkfcwGnqlAZeeLVrriomIGmn8WvZdt9yfSIE0cUSyqK3wx+QEyWhFAYmJw+OQeIDLlslYL09Yuz3kQDQaSkBD7niFQi4COEl5twxHw5fpD2OsIkwN72XZhr1akRYpBb3vTO1tXPwJC4iKLIBgLmFVCCn6OU81h6D0SDtc7JcpIUCZmWzWnmZj8RCUGHe6dTjtz64KR+VAor0lWMxFIZyn4GjIELpyIQzIZ/XN9AvAeH5ytpsH68YKBWe+vy+K+ytQFkdp3wKI0hY4KhAQ1TQly/mfsKyR4zAUVAwFwcBr3lbXzC1qllHSEctMqXqE6ZlnAWAXIEpVP6CHLZHxR2OcMs1E0pxO0sCUbvVjw6xtmbEjE4jL3Z/8grjwBGGI5DtYnFbKaahtuwOI0qsdN3ZwwGYRmC4qK5oBROlMcFnwNDg6DYjSaeCWceryo+ChXii7WDMz37jAqtnJGZg8lBotCzRyDVwpDjxoIDYaDb9pJAszEA3G1webU8CM/S4yeXkC9jeo1D82D19T7PTWunj9qs7FPUtA9xb5nuX0VCO1zu9eCmTTyNcb0LlTdwD0lvX5HvHMuRv/voNqYxOdz5fLN9OZHoXBhxnntrlFFXnZ/KryXMgDrEVGH43+GW3qyAf2O0/Mo1rf7ml8aeep+5+ubdPLOz3WIGlmw1UD0/x9Bn8NTHfuMwTiVrTN69QcnaXvO8yOgQ22R3P/jhTvGtAwRCfVjwn/jJbsGdOa+/er+tmS+9+L6++eKq8XZJXdB78W0LaS/qRkMJ7rn2JMA6KcM9/bh+/jE+kT9O8aT+ZOH0tgsIyRA5X7iuPOMbK1VIzXJHZA3E7BOvemr4DYDypaGavXF65Zz1i4fvVS5trV4OOF5ItfGV+l7I1XnI9Q7ji7AQ135usybjUwHb8yJnbYwZ0938CINFsK27TMJ2P02Gjbz+QQsJ2SqqzGpL4pgNU6qJdfCxjzBQ1NDcnFrd+6VLNm30JCR5B8t4/25WEif5BPt5Aa1Fw+4FCwTG/XMrrZmHTKbEqqAeJbm9qaSD7rvz/6Tkx32N+xH2xeCqZHxfb/7wjAvKbuUuL9NtLeJ8wni/cNd+wuAiDb8vcEDFeW/l5c9h0qP4nUzNSchaGy0u9Hj1XMQOWn4Gsz/w0eOnrlEPHyAeT4TY+lR48cu7GPeOkg6bAUuDb5SPSqHpz9MvObMJU8d8qAP535SU9/DvwtTE+BQ5WN/+snTINXXelmQjoYT2w71FddeaC3M/Jdng7f5Omv9t646CzTI+G3KoyRI6dOEw9XmdyM2su3iouWtX8GS2QMEkG4QnOn/nWXdSdcT0Qdb2weNv5JRUfcWm5G79GSjCw9IrjIL19e9Hh5RWl6Xguw3d3y9IvyWkj4Ae6cbCfxfUnnl3c9pwVbnnwFxJp0vKE3ee0O/5TMoFBRLm8Wd/Ph1OLipERBdTTA766SASrj3d8/+joa7apufR3sDyrydvvzU7dXp+huMGre4xq7wc/LNaiW+XrDS12o3figYGqwcPewpiV8+vbfYgsdf7uY4kzJieJaQVZh7a3s6rYEPUHxAxAyL5CXF2bGsQrNXrXnx6ln9X9vbP/qJdjgF5y6bolXyJrYx5d6egg7l+RwosPFvt5fD8J32+VqV4HXaen2o1PshOV341lefmv0XzBDTTeEbV4puLt5UU5kYPQK40TzGKPmeat5nhvLzK03DUx7NQPdFgYm4e9pvHWxmdASqdqaFMTj7Vku0FQxgHdTmteP1SOvfy1FnN+4Ta0dH55aiDg/tJTN7RK0pSYL2rqef5Q5dgu7B/mtUCswbGGgi3QXO9bcX63HMXDTBJo2fxKyWKYbjW4wrdikz+D6/a6JGVzHpFbtqSdkAx26f5/FmlUEy+OEm1eAp2t/iRVXWMRee8e45FF5uEm4qMiAmWRtt8kjEVopvLeRIYlY5bLRweo9wWetWZ5r5q1d2080Vzkn2uEVYWeglpH9OU5gzc6co2sWk5bYvkH5/d+6LkeVc6EuB9hEbhj87apJ//rHqPF6LB9W/fhKMIdgJ++M3fyci9fv4C1p0fyGY8CqYEPL+xe1o2EJ6aGpktrzRvk7D7iwQzznbgHMVXl1az3yIm0d1jvb311K++D+fgZ02x3O+jVQDFaO8LG72/9UCt69lGwfpjvBlI+Y5SuIuWCHfU3BPWGmqPbBIcm1TH7hFbC4WcDbtPD8ArsoG+lGpwQdxJO1pfES0LdmlMLHU6snjxHGE42/pI2ZUuDFE+3dQ0C5G0RS4VnG7v9O5SJpW/ZuX7l8bS0Uc3x/x04TUdbh7Tk735UX/t6hsrBHDZ+qvu7cZXCqYS+0p7wuw2MxcLNm5O8wOtN5ChlK1v6YPayNmLpRDLtU3bAJOh+QP6gmg40XSoApi7FNj/T+QWibb0DIoV0r/myGKoL3+vvyHT0gfSzQ+VI+4SlzaZ8MeOlKxtvlLp54UcFqCXenD5rZ/2ta/hxCZCbvIMtccWVSeMK2/Jxum39aVT+rpqX99Nnap7Xz12AH2JYJOcfjzJbD+xNdvVcBK5Vazw/HCEFI6AqmoU1jiz3TdJntLq1pDes95suWOu2yari3JiYnwikeRPnki9wPXzFtdbr/MWAIr+DHB49djK+6HUy+Ha4BGDB81OsiY6EUPli3pXVtYNQesGvy90eoMnxP0NotrYCisya6PGZeeio02DkVqnzY6CpcqyZKs3t4lLWf5H/9bK7pj38/9Zspp3OZe1OY7C2h24+1RKxSELkkwKL8KweLYnWfFthgK3MMtnVyIQhXYushEL43bJaIPDTexzPNX0XMvSsSCYEpzML8zMpszM4czMlczM08zOftBSwpTvhaZIKvsx+WO4tq5WsTBDcww+AmZgVNsSTcvGMsbVU8+q+xMhZgJXFFcTL7asnZ8+IvBbYCdswyON2aGg7KDVXO+QJQT9YbhIu/ALxXy1fN3iIAStnBPCdANkT6zJHqXwW0dvjkbN7PfE5DuVLKcy1B/rcpi/Itsv84oNN5pD6J2RrRgBnX8FRVUp1vdOfme34CliakDHWU92M6LVT5kTTgCFZ8RoPyj6e+QPRcyNOdXgiW8q4FB9NO3jclaq/OMEny/CbfIsMOZ3nNv3rspvdANjfDtO65qDf46AyVZrykFPi7JaSj1m7H3E6OeThNOZg6zhLMB+RwOeYjgMi6V4Qvfw3xev1XN7wCAPDjL38eAPy9tnz7HbmSXwdepQPsBAAgsLacFAB7s8SvmRtATgba3+UOnCRdQ2kUGIpPutAnc8Un5e3NYD65QFy5bJtCR2OiXe4B2mho6EHW6HdmutL0nird22x2rY/K23u9z3sV5Of2cjwebKavp17FCK2PJ7WrXcxVX8dXZ3g+GZvzBibAeRdY6kc+axq1iy10qJ02stvFXvVx93XeNx2ntLUdtErrYvn2MNfV5tpFd4fKT7eBaTV97HLU1z01nqEjVbLOT9KF58RcOT+X96maAzk7y86hSc61Op+iPsj6FqkXciL7kVfeqedkDRJbIktciST7Ifsk+yN7ldMkdUqzdMiwpBL3LiDAvveVmqIgAOABQRSwIIDNEUbRCYAt4HqSSls9BY8Xb556y7GnIuY1WRD8/cl6LPjZ5PLkFEFabAwvwzxhmE5Th5N1tE6I6ns0KWI29SZIiJuLNtJZior+otL4UZGzzUoO98hIN58MlsdmJsZQDLESwmrgaGnpsUYSCPTqZLYdyzerOrKLnNgzUhgtjj1bs3cTz7AIXtTs1ndazJwLioCRhEzp8Dex2mf5hq3BcIHIN/hkTOIiAQAAAA==) format('woff2');
	unicode-range: U+0370-03FF;
}
/* vietnamese */
@font-face {
	font-family: 'Roboto';
	font-style: normal;
	font-weight: 500;
	src: local('Roboto Medium'), local('Roboto-Medium'), /*savepage-url=https://fonts.gstatic.com/s/roboto/v19/KFOlCnqEu92Fr1MmEU9fCxc4AMP6lbBP.woff2*/ url(data:font/woff2;base64,d09GMgABAAAAAA00AA4AAAAAHeAAAAzfAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGi4bhU4cNgZgAIFMEQwKpkChJAuCDgABNgIkA4QYBCAFgwAHIBs/GTOjdYOREiD4LxJ4ImJ/qDBQR+ShaY0QeagfGrHEEcqmBoyh6miAeOL9MtgZXmJr/ZC/LuARr+7R84+dVFVRxQfp4AghAzjP4Pk5vffzJUJDoAYpBAu6FbXRYFal1AS8hqiGboMag4qHacUs1I0adNShAxqN4SMObnSmkwVB3f9a+3PnMyvY6xAyj9BI5C+41rMpu3WXa5UkJv8c/P/mSpvszHT/7h7NUQ4U5eUKKNSBqmNXV2OSf5jm5ohIMaoCGlMPjApt5VWYCl0na2Vfha8tyTULthYj5niu8Yj1n6tQxAB+UCijjEJYKFcICnzABwEF3mx5oxsQNhXSyU5JhnRTYpIg/QVpSyFFAxkG1MWkLAUfFCg0ChQ+yg00VIyQEAB8AfIEAPIaIGqxMh/zvxB6Ie+SONjCPhx6w7oOwC3PRtQPbaUO1MgjqxfnlZR/MD/lndaTmMPpyzpfFI6FaeBvtb21GWLZWMq9rODeUjv/olAZPT55W3NWjQwE4OpxK/edBiuwOjoCHhQAArLgIyYoAnADCSwxGwNWxHr8aH8AQrCvFHh2kiAX8kEjBgwXAj/q3e3hSOREbpcPTuq69G/1EQzAtI8DQAzqCLzLYh9gSXCKIUDf+WSBv5GB8CnKcCuLISD2tD8wBFkLAO3OqBjCEDwAzXrggbAsDIlHI+zpxeCDAkt4REv2QTYg+2K4dXgYglBEIlsn65d9lH/FsDk87KHbV+39WyAANHBRbIFRIIsjAhxAKAfhNVkCRHIGWLgRLcAoh2yd9AEw3iHrlwYAPzlkH6UvgEI3w60dUI6A8vwJh4fhAwAqACAaALwA6jzQPsBYA9R3AADz4gV5tXGFnUqQCZmOIFrqzzx84aDX2+PzNGgtsZaApg1EmqIxfFooEWlJ6LEiWlesI9KVCjTGaApoMc0hT5OT3Se8Qc93+yRet5j9Eq+u5zYm/XXY8NgbdZjKTsGdZi/hCwl3i9WP4yITB7VPjL4bGsLmT/VhXcmwgVFNiqI6xRY4E/xi9A9ZygRvVE1mixtuKYXpilvugu5cXHa72/U1jN4h/CGC8B2jVyEstpbiN5UIFJbGdtDduqK3mbC5bFAMldG4Q8MEbxQbXuNW9A+wbagnhFjdLpv2+lpZ3pN04f3dxTwthzZWi6LXqHdGX/anBgIlOkxSq6z4+gwsntF7SFMqoNhwlMOVGbdUw2iEDNVaYyouu4vImPNhJed1NoJAQEKUtcBphz4eoztctx2CV/mm5ISJjwlnZajWnU0Ws4uSFGKI6VKJmtw6TAa19yuI69YYFsIoibBCXSu3e4F6NSR9n3f9vnNV8Siiwpz17TleoKKthRDNQ/KSIlkXMK1K9sA3xvaDc24/TwqZ5iYwrgtGkdfdFmgWx2ldq/TFTZ/Zw/1Ny/Smb1N3R1/mwAhemuQwjUXaDihJDJOgERPD2g/rT4DSWHZw46+6QP4FM5j4C+ZoCP/9DMriGBjVMV7MbDCcVol/xjtR5uNOHplTRebw4Gk3xTF9Mmcco3psnK7iL5JtuDIL3ik1UYf2ntkFRtVz7u+WKKz9Dkb1HZMe/dnbqtDe9lfPqG0rf8GlyeqV/trbFmtvafPBte90/79kU/cukFdT3+9hNmz6e3hQqFp1/oWF+NNXU+Gbc98vtLT+3iR3PIGlPyJ6dGSlv+kM6chaf9OFW9HBtoNtMFRePjPCejR4QyL/1KxNXed3V2/HrndtgVPnLJ4/JWGyz8CM9PYz0/MPPDxAbXvRYhGlRe0IKvD/eq60snJOVMDsyYiQf31ufXRbQqbVAFjcES1LLv33wCu7/g/rC3alt/mVXVAUt2UU7kH0yOcy437ZH3lDD+tr777XpRYeFZav6sjcGF9Rkucz/0ai0Hft4pxaSd5FbS57RR4KB9wHiI5rQHBZYLCrS2BQWUAQIszW5g89rlLelugKX/DX5Xx7Ur3yqbbtUD9/Y/W+waqDu7vrDmZn1u6D7aHExPzjqr/PH90/sOfC1h6vAsGA+1MMzrp9lyd9cm+h5I3Th31XZs9WSCOMZ4895sRipDNGj7I+ilD5qZ2nwggu4lr1f9vyrWGgYacLb+H/61oy4D6ASdTL2ulv1QP3DHdujNAILSnZ2511Kdc4agPv3netwf9+vLjQvl2CTZs/ToRZZ973x1XKh9VKhU1CysaK6X/Gyr3zyGr9dTnfnlW3PR45nnwI6r2xbh8+7593J1kwp7jznyd71Mf4s579mk8ua/2zobtTaPeUSUjlsasCWZ8JdCgJhaPja9HRtg5lx+ER4pGNsPnnWcHODrPLxyr4VxESVBYcpIBT+sulBc3zzYviPbZKzYKsldztb6htntu6GY5m0Taw+viMbQ6ifcaZBK1kczZviYv2DA8PFPjj8XungICywIDEQP+yAH/YX6lcvWU1bJmeBTtyV7apshf1OIZ6BAfun9ox6dx/Kw4WQVf9sNP4y7U19Wvq1Q52m+9dF66G07Fb6mPStQ7tO7Nmre4V3j0+5/PDf3b2dB4e/WXvrlQ270mmvncxG19dGx3qEuSTIez4e67vqT9s2de6+5539Fwvu5wJ8nG142aM9Yme52lV4GMm3acHW0blKHDx+X839bGJUS3ckdXcUr62hdsdI139/MPcHO7pcOmB5JnUuYWdt2ZNpJ9NuJsvJ4BNuipmZ47xQt9+r06hbgFBwR4OjeLQuYmBrkUBVgbNpnHS8PlJ/jaVAT/LeuT4s27vlr05Ow5bbe3yOBd8sdvNdvNx79p39vRI19lr398HvDn/5VjPjqegzEAD4DAOg8RZZBq8CIigUIQXGlm7lYUok1mTMSPAa5XRPjho+gxDPDV9uhGrGuFgGbWyMAruQ6sxjvlJoRhHDGomOGqkYhJ+z5Qf7IyfPOISN4d0Ome/ZzCDNNy1aop3bFtZmGZ/46iGkQ01M7zlrJhlQTPHB+JZap7LOjPyHh7YXFLkKBiRnyVk4tJG0Gj69Jny1fTpUqyqh5rybmWh50tWmhHKykIx8urTjFFd9oqxYlAzQVVJFZMJ90y51a7hJ1dc4uaQTuMVBPOfMiEOZV28LDFaLF2cYY0kc2CFJvhsQsoFMvoAlVTTqzME/ACJHpOQ3xhHeqnENplx5DaV2KYzjtyiElsU48htKrFNZxy7legXMSXKNRkTMpJcLQcxViyNBs2uIuK4qwi7NX1mDHHT9OkOrKqHXo6tLPRmkZUqY1gAHfH2fBJ85v0A0yIKwZ/GGsiWaZFmoeOdXy+F0OXS6enT7CoirooIuzAIl5Igpsvb0890+C2+VNcx8w3TsXSZ6ZIGJuAnSIS2oTgguYxknQnCtMmMVMGPI7tQbS9dAnnEI5rDgJQ/ChyE+D1LAEBhLuA7tH5QRnH0MDN96xvzDAfQvccZGkOhvN4NaA/9OopRmXk71jc6wkb3ZrLlsnfI6enU13Yeic5nR9pFNln3UDx9b2a5F5HcOY3bcXFnCdsxi2xye3m7c3yhQ+LRsEQPY11Oedd02KhMucoW0REMAOcIpM+dLacOTmYwmNHkdjWe2nlRc+E/abCRbJ+W89QM5bhh6F0DsdO6HW8D9V1AaaF9I5AfbIGubXttHJUsfYRqGrXYy46MOSbytrQXHSE2fTJweyF9LrbcZ8Hpaby785iTz450iWwbuVc8f28m0qtTcuc0bpxLIEsQ7bPlOBzN3g1iu1g5tYQZhKuEbQ23NqA3yW/T+0clsy5bn2h3QPrg7IX0OdnyoO3FKQ5kLJg+etKdXUFdgXkEtxZYAlf5AHQrbqi7e0dFY0iT19vuny3bK8pBf/6mZhCRd0f/uQECgDLRvtk6r2JG0/N/vi4fAHDsWtYAoDb5/6v8h1ow+5UKEIACABCAS74agCBJBUMTDoPgYrOKVWexvwIZApgptOkKykehayrEjgvy+wGOtwqPUrKjp5rZZSI71JRHhPxuENkpMtdTGlSH5f1G5CNF49SI7jUip1t+9yNzPaNcedLXDTnwwxv4ocfbrWl1Sm8Rclo0HNYipG+V1alyovPJmzE9+sHpEuSRakmyOO0Yf2AOGHOUBFGvKg06zBCzMLhB+YiQlSfmNRJJ5qtLly42yzESvip8FYkUm/0YkZDr8Pg8f4hEAFRoNAYRGBAAgBigQNmCBQHARyweCC0AMBcklUCKuSoFMbIm89qTaagMzFBhsmDbnlQcxlQn+FpmuWwpEsSJl8aQPVt2RYihQMt08GQx6wdbahFrc0JDTk6dbLZSRTOmfpYYKTLEWMzaZMsstEyaZQyFNxsTpFtiMeOE/GQLbHJUSIpUCdZSJ7J1qwMXHr6mugwaZTySUYQs586GZOZZW9Awju0XiRfDmtdPEcdGsrmIrKWezVRcK0wwX/4iTOFvvItKWzwXB0uixQA=) format('woff2');
	unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
}
/* latin-ext */
@font-face {
	font-family: 'Roboto';
	font-style: normal;
	font-weight: 500;
	src: local('Roboto Medium'), local('Roboto-Medium'), /*savepage-url=https://fonts.gstatic.com/s/roboto/v19/KFOlCnqEu92Fr1MmEU9fChc4AMP6lbBP.woff2*/ url(data:font/woff2;base64,d09GMgABAAAAAB70AA4AAAAAQoQAAB6dAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGkAbjgwcgTAGYACHSBEMCtpgyyoLg3oAATYCJAOHcAQgBYMAByAb4zcV45ilwMYBQHj/zqIoV6OFKKoGieD/6wRuDMHeULs4TRSwjuJRFI+iGCSKIyo0tu3vUnyz2BtKKHNB18+a42mLmuJ3MAA+mqr5bDgO5VorjviF9X1bH6OhkcTkoVp78vXswDHLCxEoYDCCVBSgA/ZIKhV55QPs7m//wK/z/St6nL6ZHVMLdvROn8nDqRmb6vVtalM5VmrWp2KciFwka5Erhls7LOBpkfcBBwnacoiwW6rJv9JVdkvcPSMdpM+RcwydOsOR5gBB2k4km3duQ2MSEcTO7FCrNUHlHtxmqoTC6E8i0ucCFw0AP35uk7bRKp49f0FSpCVKF00ku39vpvD3OstWP/9rTl4odN4ssHZGUYh7mv6o6BIuKulZYEn+S6TsaIFsH9lhX1BHOqQAs9ch8l6YO4aiS1FdVaZO23PRlumAed5t1b32B2vFXq8JFsFSzpzfN2cCPX23hLCGEpaylpJ76rfPtYSQtK0p4gX27DXU2v6erg2Cmlx+xASggB8AHAoFWNCiDQEyQwhQmEHAVy4EbtmAAAIoACsArAAEQABYBiT6MIfPtnIEws0R9JNAuJ3udwII93hdOA0EGAByGwIgTtxKPw2cAAJAAAZQAApwgxUERGEAdJCnW7JFAAdmAgU76bQVWHiTLF+B1hAL/UpF4HCuCIWaDrrm6X4TIAyv0s0LknPMCy35TXdVMn7yffEk85yf+T9rs0/5QSrB9gos6F0kWbvoyFVVcy4rjh8l5OKibOhc1LXwhWJtQsyiC1u5/Oo46CIzglyY24rzrbe9mzkq6p7fEUwv32FIED6HmJ2UudaOQpYy35ok9z2l4ky1wBoC2cBA4VdvKy7a6nRhafai6lwW33iLrmplcm4l9kc5e2aFx6bsAb9O6BKS+IzaZcwuBJlJZplxMKZk/Az+KTwu9wkTUmljnQ8x5dJ2t/vz9f58f/+4+ISMK1evZ2bn3rpz70F+YXFJaVl5RWXVw+qaxBdTrXqNmvUZMGTEmMeeeu6l1ya88dY7733w0SeT5ixY8sVX3y1b8cOqNetYfvoFAUNNEIAnGhfhugVw7fg7QHD/IgAacMwMqM5UuKXALYP5rWMRVcIAAugisGam8K/A7lmK1vPLgbOIH/h00PXQ3NpQDgpwpi4HR4CiAWoWc71atwMHx0cBsjvA2z9rnCRr1U2U0IACzLFw3KuAF0XcABNfCtg6gamjHToA8ylLNWBPc/AA7LWuM/ojEcti2TGKOu9ysr7EikjqYiMnItHLadaW9sikiJ4RlJP3+5wE/kKFJxskeBxIIoAxKG7ObofOTXjMZ8bAX//8twEBzG0Dbgz3jsbkxU9YgHa7qYHYradML/iXH0Ctdvoy2AEsgw3ynEtoZmAYIL0eB1vgtnG7ABHyAFDJjYmIuDQI6UIDoj993ABxFwaJBc5mCAs4ZAciGHKhEKTQCmrYhUcERHTExLXIjPxgxmpso9ZFq7jsCqlTpJKkWFL/iERiEbGCWEmsJbYRO6WFtrdBACIQ+JAPxVAN7bAKx/CMwDieETXB/0V8u6ROloqVWpf6GSwkloxtJXaMIu8SZKAg/fETpCfu7w/uerZqr+YKLuc2APPqzT9mm6f39qzPzJ8Z9rQawJ53pqlT5VMFU/lTVoAAOLN1OJ34FhbYuzw9c2/obxny+ybdc98DDPkKFCqiQ/fm/U7So69HrwGDhgzzFyBQkCnTZszq0q1Pvz8c8QJQDgBwEAgAsQPAXSAE2jpumRC8jhF4cp468ohjJxSuW0ERydNUaXmeNm3HO8TQC4xQnMCYhTdYojqJFTtvcuThfZ68fMybn8/5C/j0DoRoX58YiVMmrsWpE5lxWgdjzQdOB1FzmMt8sYrrV/Hfhjuwbf97wGSixLFKNNCFItTOQN+gtAKmg3IJmLUUFBJYaSn9Mgh8b6nYnQRz1rF88dO/Wn1EIgIOQywM/Bli0QVHKFHhiEqVfqtV65hWbY7r0FluT1oIzrPBg0eA4YO3U4AAgA2A/AUGl3DDl8nBxOQ9cLRb/syaBXmb2NAGPP0G7G28Iz2CPN699rB7OHMQEsOFVoN/u9orOvA2BHyXbUM73rcHgdq/ibHfKSvvFdKG4PKgB5YRA+xlGzLDlRSwscGuUvw+LUsmAojsF0QvwRniXCYA5jpZ2xz3S1/IcmWMnkiZJWcAqkH62AD0/f7/AKXbff3HlPV+MacF03dfW0pf5nbe/2RzwcOhXNFJXsneQXK4lB/gYSbZnBKu1Y1eka6hYfVriW38H7u2oJ7tYTLQmLJtWBbtyl8gFvxNg+QjPbZMknk5TEmBlq2WgEpzVwQ2vPGfdvcV9GvnmQlTtK8wFwplEjjIQ9Zavhuixukwss1evDWeULLd1Ve12nSTXqQWXZO/WtFg+ekRGiS7vXXKEpYe/KZH5S+FaSMU2KFOTgXhvMi8XTL7oG60Q3uppIz8b9nbbc3OjIHahBUy/+lazZUeUjaiEkoVzsrQNDT8YczROmHkxiIMSn5xuBa+PdRVQOFAb07DDEqGs1BP4bTwjp7+RHXe0GEz4/29U4sJO1N4jYQTh6cjBOs02+GuUq5eH9u/4DEa0b7axjkmQ65Jb31kF1BSuISPfQzCWyz0IBv/3VN0NPD3a+z5xrGcZLqptNfzQ10pMvFH64BCP+9cRCSsKeWdFs5AQ8dh+YShjiV+ddaWLvxu7zYLRc7WvqpnvJj4TsaoaZ1MZplC7ceRSuGsKV31vq8tHjep92HRmCqBZ+bXu0xgsdH67vlk9S1D8LpmCDEej85joKB5s+tDR3duSMi+CFZjyEFzpzrmhGSVK1pZogEvD2K7q7r6OcbkZx2sHWjMJeNthtMg8cJ17eY3GSTDXPQWbT5yHykBlWxlakHnFPwb5/OPBwz0CjSTlCHbuw+hSqSY8bpqpPQY7TtuVVWRcG2OdCgZA6aEYg5IF0JdrZdukhxeCOoyYEuNUduCiQ3T+UvNcXTbyMPvIXbcqr3vitNMCXRsHxHXjISMDFyhoIHBCsS00sKlspf7XOWYgaFjlzwV9vR66L19JRZMkMNUaNCG/COlW7tGCrItNhKUCiKOIWAgoInTVOvN15hq4fHdz8fp/iblxfL5CY9ab9SYZbKypJspMtTwvolWHIehZTkCeADKlwrkhsxYGqsGH2glq/1RKALY24VPwcR8A8Irq+OIqASU4ppmrSRdlLFUXAv6D1IOI8S8g8Olub522VLprDdQaQh3WSzDFu0gS0N3jByIT7pO1iF0l7QwDXgpk6UB0O+78RHQNGr5b/uz/7qQl3KQ6YvCW7TAc1T8YYO+dtscmTlnPNHwMCgnwNazIq6BI4Q/MRVRrFD77GlR9xuRmxnkb4BPCLn86LYYEbC70KBAEAaCYo92lOXXJwSiwLZWRubaB+HzlrmYj1AzXUeGAhLtTOEzPW53bkuHF17kOQYyMrhEK6oXHQs8b4nusaVjNM3K3OvJZMkgvQvyWyYVUVqHHGhSWgUHDwOKfUubu7JgoPv3GGgnu90HGeQnuldqW4F2OxpLNgxLovOgZPRlsLr3lJ2WpqMzQN1EuTmfwYLJLpgAUcbtPBD2XAYUdgSjxN/+FlkUa17Y4QLaWmEU6J0Mx0PGzkOn/WUAkzU0a60V7bfY3mgoinKikYFC3HNqNmQqy7ztu8TwS4pOmBBaodVjrOmOVfLiRqgMjkvI42CNDRyhqFwJvsJZUI5ar7nHaJxR4+OSmtBoLWwhwlVahNNZ5yXv2yy+0CbC03httpWYn/jHcfW/ct3Rg/t2iZrOfH5S8t8ReX7TBr0o/YsmeeoHCD99+NtLEMbTr09u/vdv/9u7uPyuvew7e0BXxUE9xJ5Dxo85JxPC5PSRKnl5HGxq34t83o/5V81ZdO0gaiT16Y1DAuX1a3JYsRW24N3My1eLmavI57jvZX8Kc3KShO5ANmCZ1b37Kgt1Hls8KzpEKh/k9p0nFA+//ot8tlt+8m8A9OmXnXrax1oAy6x93JDpBHp1QZZBm4Bl3nvMqLUEGnTa+IsF1TVLFOepo57QLXXoO1m7fkjO/N2L46hg/kR9P/t9CFPbZ3d014EeRbCLBXZKeoqZF2/TPd2dDQ/YgGWyQTmJyd58gfk1Mf9n4Qn7Av3CKf/w5DDQuFBUPSQ1isr0qvJNVcnEPf37D8D10ZyxzD9Hgl2DTMJhV4K6q5m+kafF4wjz3o2E117isdMWZgS7s7CpmM3F9fqGKjbw8W2A3WzdRBYFX1VfK1yVawDP7cdzTfBVvviKHEN4zcasNyFlRS2AfHVcbcOWlDVs/64gEVTu/9cN2IIzE5kZA1Hqpu58O/Q2MLHXYs+GJMYkIDyg/SLMMznAIyjAcChylCk1t5C3fCblaTSbfjU2KTI1yMM2K9jB90msR8nZJtGJH/c5L2Q+SduMLEy5lZgYDFTM/ND8xnzf4wSgjjOffFbk//Nfjnv5MftpZtat67LqQ2CbH8iPMd8QwTLXBR0VBBsUWCJY5ga/rKmgr+lrOL1pUysulZAnviUulZUnAXGj3GO6RQeFDb8/3s2AQ7HdOd05QMx+McanLALoLR6T8+FlhU9a0x5Cy48cM0c3X0+HIHtDlkvI7bGjUV0zXaiqz5mKToKoavNok/+PE1JS3JxMXe1B++HxsCC09QLaN6R61C9kjksOLfGHQ3G+d30a27SAdg0Ncw2NqYCHCszoKrCR/b+k3F8VFLaPBTj4yHPmZEJT11eV9bXi6JaQnMOJTylxOaExbeAstCQlsy5VELk1cyX906oEyrufO+nunbDSwOT4SEPPt8Hcxg98L6YLRD7Dc0SciwQ7Wfb3AwOtQbHcJz9IjHP+mgmb9SbT6DfOJBZFpMfHWAReqOlaVVr7VZjYFpplGvXx8MWs8MROEO0fCE1rOoa53M9qO+vp6p5pFodlAQ5YEMMisxBxbVOLRDMLbS0z80RTcxArIt8uKap+YPy8x5kUgUooFP5n/MOzE6e9zGQOpbrvs5W90dcRKgExf/z2B6LCZNzZc4lD72jFIcUT9dWDSM8wqyfAxSPQIdBZ2AwWWCOYrq5M3VABFhlE5sm4yLDr/fbY+EeszgBnb78ss0QUiww28g+ituZSsz8ISHB/5mRc3JhPy13Ak7bWOUvTOn6ndrcWZXRHhKV3wH411z9qpYdi5lnkz9jwoxl31XfBezmjYOM/avYKjUdLM761N/WtdN9imQa4Otn6Wuhru+nt0i9wsok4mrnWMJD0zON8qdd3zJEwVx9PH3MgjBtxdDd0u+rjDPaVd9iLWotr+NU11HEAqSc4OGqQ2fCkv5PV9rSyVi+ai0VeAGMVWuXXZfwF8sjS4tziCD6RvFw9+2m2GX4f//AJTZif9BZY1ljreOnqSiHYyLiKDWjgIKbXsO/0A9mJnQO024Add1KlGIcaG/saG4dSjEdIP75zQL2Yt+Z+DQhpgoiwcj/ooYpzCJlBlSF3cKkUU+V9SqYyNMzuyxByFuL5HX43O8ioazCnB5MFbsoOSgqN3rRTCYggpIC9TPe/mA1FCZbHcsqvL/f1BRiP5xgLVdcNXvV1ulqauX63B3R1eqjPTGN2+5CdHFPYYMQujpL956SyZK01gso54f1t4ym8mc4wtBp3SGYO19y9v1z5pdvsuLu/83G6M2UNn8luGZw6nRifPIy8XkZ9S2vmvHzzXnJa8KFe82ITrecGmrEt5BTy2IuBACTM3dTI1Ua1bsknOm1H9IDUuZMRtb+14KPSpldv+D3rzzNs/7Cbyy6zzEnap09y1jXuGnjt81w49D4KzpLsQ64Kzgy1S6ndQJUdqRnpN8rRhV/V+kbFxlXW1RottDf9ku3mq2MKYniH8mHZvJEFkSg2Kl0+J9p09/S2dIhnkVmwUnmmap28zjxCqzlwn7flgAYQhAK+mGbRtrhzxjJT4uSztp27CRKFEVnqmZtaYl1PUs58EpMssM1hAvZzc+dguSmTb+gS+VKLisJPERHOlzvfR8LCIDJA5s2hlw8wG8bhxUJDW2+JyXW+wUs6kcx34a5ngm5Y+hNZZEi/29TbxzRwFhZvua1a2JYHzUKyFA+LtOeYqckfj0tz1VRJg/w1Zu/JwRcsTvTn6xKqy17ldr4T7Hixu7W1CxSNk0cfMRu6i8xt+bu8jbnw4dVjxQ3F3k29fbV6x4TF21107kFVwCjYoUAyD3MxPOza7fxSG15qfHx7UfjzSzJOJehJtuDv5s0l0f+9hasMGq+IfH4key41eyYtm3IwiF6afLTOX1Y/Erm3i3FxYzEtZ05ICVmj7tjSjA7g+6/v40kut7j8xvm28QHO49z/ldsnZtVdLcrnVlnABtHvg5lqhjM0wlARKkioky9BbXNvqelZ/q6T5Miek0DE0OaF9v/effvi/3cZSTC+AsupFdwpVxlhTpflrdVt7pnovaBwCTGcwq7gzzdKnDkbAerq33j6c+5k3+nl4xe6Brvk/G/cfs4tKporLMb1FA42LkbX3JGv7zy6iWJpnmhhTgFOERcNY9PoUK2hbmwSfYsg4r/PLeZiRWSYp5e884roiwLPDhoZKh80NCAp61O84ecKZDhpWbipHDY7MCF8xXy/q8f5U8cbWSdAI+TL6egbngqxgTqVBHlz5WyO09E5HocYLuryzgdBPml7hZQGryl9+aTUCnHLFoHeNhZBnlTLIDcLm0APXzPpTRnpxzKyz6Rltr1+LuJumGMMd+4xz8VdLK8IcNa1tjbjMoG5VQ1T00Qz02Azk0RTEzCao4fTWfTzI+dDWaHhoCb9mrQrxcCxBUYlxuwD950+Llp8g4EOv3v9JoYx9u9L7tK7ll8egYEezv5xgViGwXUyJlwDjp4ffMt4exPSrmxqLjussFdeMV7dDC/XN7b616PfNsRZMtk3WXm5K+6519LjqAddJ/bvMwYfteRuVz0xA/kvA/a1DN9ZxWobRd3f+R+wzIzX7D6xuhCOdY5uS0GHzJ/wZwHLAF3Zfa/0kti0pFRGGTF/lPE1xOyfm1mkhxoONZtj9M8eXAEjziZpQz/pnW7STN8wXSK/xDr/FBTnf883f32F+HBSAwa2bZJtOkAaILc/7EbGqCzKKRfvIPtQFlkimzzBErINdvaEgOqf5dC+8vojjEvu2tzdwmgx5w8Kl9JDSNGJnWd0iiAdjBmNyH+JncK9ChRzY8rSqgn5HKLNGvippXIJ9LL/vVe15PcdU2m8KVL0oXpE4AWRbzfbFvI2z8a7ilAJqo1zpNtv+TQeVYMJfvvKeJGji6+oO5Eu5PbBaZiYNGZkUzrsN9MuI1ba5XV0xpYjtRt/rUE1IXhKEXUKLdTNaoGB2JVze3NZOWf3xq0P17LaUELoM5P7XrBAjbREWWIvPXn05AT++jGitLb7//PXgqsslO4JipGGUuq1qKizGVcuoXThvfDM968LnnjPdi8PsS+ortrx26mV9XfkH0441fpbXw2IOPnK2MrS6ZH/XoMaohOqFgbyYqzM6aiLvqM0cLVvBdWXKfcq7sHOoZzFn705cZ9sTvnYWJ3wtbYM9KbZBXt5W8qR5GRX5RTWZeVUAe+/P95e/yRyCZJeJbk4NkJdRntF+8UN+Aj66Qt9hY9es1dNr69PbfUCKAgPL6E3L89kuL+i/xrr2hLoZOGf4TfwrypeKfY9Tng8pPhshZ+z+G3FW741Hhd9WfGyeJVfnu42lY+YMiFM/Z6cciXbi25+sOjE7XnuUHERbPWyFYd3jAvYX/NxZIRw7CK1A8UCzsgMJSe7nzpmXB6Dl1vMg86qCn+N/jS7p7wYfJs4fs+4NPnIZhF49ks+zUuci9L92sZZyyrsGo5onAmyy7/9cLtd0OwRTueGJ8brL8+dAuyGBwbsrXRQTIwRvMM9JMnOIZ9zySOSdAV5WgfSuZgiWvodmkA+FmCsoBI9QWUl/XtZZK433OHbX0RqlIYjwBXAJWtbeNudY3EaxUlxQOixd0HwrAiGjTkEkoQwwlIu6UQMJEvtoVdk+QiI1DgdpmC6Crg4roUHORZXUK7D4S6y23eqjGtprcjyYRCpURSOIMq74ZE8Y27OEHxCQTTl3tOeHYYSTV4s3DCPYJOHRp895MrxXyRSqo6RREopNkXLDB49gOPEP8DovPUfuIi01fYL9B6CSjJMmQIvigKwtCNTeFZPeJsiMDMRU5akIpOU78D8vfkT3QNeaR7B/I6Kvf1UFi4hbngkzzg133U8CUCDVLgxlariSmVL1BhbvBao4buooQKJtb/tS+QHwh4QwH+c84g8F0YGXItA95+jg8Vd4C9Jz2xiQYFkPMhYwSVdhnHUWAYfoqmaQ9KAZ1xa+ARppjA+qBLnsGKRiocwKj4dUSSH26BrIa2/t4PFl4UKBeOrZO9aQSYF5q6FWGR+Akk17gFJNQ7nH9Ez0uZMMF3SnMq1LZQE51hcToWFD5tzyL4GI1G6SKqRDi2q+ghAmLw6/SXxpEVhEcFGfWih/bEONpchDm1BuIrsR+fnEck1z+5344FHsgrTCJaszFNfmxZkCdK8paKXBS4jBxqDm3nEcqUiJab7NpfgQfVUfEF2gskquzuBSNQIDgOJGijgci3c6e/tYHFEwWqCSmkPCtUQ/hyEqJfEdMSymi4ptw41LqFwTiNEMPAUqZRSoJ8LRQfjFcCvMY1xCT7nEB7HpoJFGjBJduSW61QOPsbjbUtgGxehAUMe4tinxr4OEVAx1Ezzkkeyq2OhK9pVFzhIEr5MlFq2xgMocR9ekVQoJSrhVp5y2px907ySfkGDjWsd8by1Rl1E6zDEDaaLAah2LgD2Zp5C4CcezF+y8/vXbUx40Cij1jG7fDd6ZF6fz2vV+UOyfZG/Tsi3QUXDxr86sFYsQvYuv036Np8N6suh39fjHYVqq/6NQ3l4sNX5Yw+7yO5kJ4l+Dw6BYDn66rBiPzuFF0QlH7vI7mUmuQUrrw004spjA038riPA9He4Z72gXB9q+YA1ke1HPinp37JVCGylOSX4jT356PHbtSlis/M8aEwwXz7X27h7IArHToX5/clvl0bN1A0ryaZSObSyscEn+uPZ8+GK++IHMZodjpdOfmi6DIr3uAEejZHf5MiMRmJKWq0MujQ/ANjOtw7HKRAbmgBusc01JjejW6qNsFoOsl2sLfSahEr1ef3Ra9WSPyTZRVbD3kbK5NAUCUoOYFw0ydAGUA3Vxsdr0Tb/C7KMXh6aqNI11sEmWiDcbMS1AW7W5qpN8+If8hY/vPoOq7RgV2Zj4vR3F3ozYt58uHTrNBdzQCHiHFboCaJ0QjrHAWEs+NN+5/d5Wu1jJekiSHZ+d0casmSv3LaBBPrC0CIN2uOOAAdAnYDEu3XBTnSgwBbVGUOT/cDU0wFEOL9NEq2XoSPbv8E4OijM8/tL7AgEZX5lRixahkWvbe3Pai4MgKDkkzYO1RyvHbZrdaPP568f9OA1BnqEsG4+VUr1sfncbsj93NbrgcZkVrdUchzbTpNscuBw7TfaXJ06CAWgqnTh5yOx2EXInl1jFXWY7cPD1x4BUPaseypbL+mxQ/d/ToklAPj8L0FlAH4O+9/+y26Oc7kuOQ8sBRQAAmCcnHkBlp5wcVtGxwGBait7pwy+sogt0Bzg2yH+jSy7JreMXxaNLrjVyC4UhfUcpcazzTg2DMRjVjOc3BeoW9HXLOsH3+PXEjW4gVSncWBesJ3uQqOyqVRHxyoJrSYyOyqHmsa+nhNReCCqINuNUdJ3zQMZ/bfIel5EvYpWnlt/BiWNWspxYujC2jrpaQEvBgf2UxK36YM75Y6zRcCpkZS/W6Kg3su8pqLVZbTGtLsQXKt5rlZfMoVGumW5XlNFF15Ib6nsyvOc72XO9T5aKxJV14UV+WyMLpdpUfbERJ8ihRQeiCt1wou1vdNbF6fdLMpJ8WBxMsBiB8aNYW3k0c+S7i3ylmTJ7oqke4bI9ot0VadYC0g3d7QWRXGaZRVrTKZW10ud3IKF7Oacf+q1fJ0PlEp5fq4DfHV0DLAymO2ssZosuxFCKz1RVA6AlcN6ZomQh9DqEilZb0I+IHqCWtEU82gzxAoPQOq9Xq+w8kJUdmCukB9YNWRI8dLLGh8J5stgliBbkANPym1jFmcO8uEpkEPIjEbdtrWzeeKkURma+7I4wbyATEP+5ggh/5BVuAQgr/qlALn11ADILGQXqEfo2jOyFyWftZceXCZ6DnSPtz5kAVr1g/Lrcg8QAHtdI8oG1ny6rAIogCLBAQJgCfhDA4LhAnAdKCICBO4iCvgh3I/e9mMgYkFesokDW5XCD46GjjX+twt63JXkolSqJJVGIJp5mbyhm7VwGJhGG8jU2Xfb2TSpjtLUlSgB9iEmekfFqaw347K1PSY3OgzsQ1chZWm7QZDyNDUtnbHm1QrdZQ1MLKF3sMFhUeAVWhO7tkvBmcTNkxLH1I28PrmxWowm29OkHqKPhK72KA8IAA==) format('woff2');
	unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
	font-family: 'Roboto';
	font-style: normal;
	font-weight: 500;
	src: local('Roboto Medium'), local('Roboto-Medium'), /*savepage-url=https://fonts.gstatic.com/s/roboto/v19/KFOlCnqEu92Fr1MmEU9fBBc4AMP6lQ.woff2*/ url(data:font/woff2;base64,d09GMgABAAAAACswAA4AAAAAVHwAACrYAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGmQbmh4cg3wGYACHbBEMCu9I2GwLg3IAATYCJAOHYAQgBYMAByAbO0YzA8HGAQBQZyajkRFsHAAUbVAUZYOyJfg/JGgLhuC1B62JCWNiDIqaKUtrEQhjIJnCeHmtlh3y9eOs6eV8V611d7UeMfbVnwlxCKJ/aszxY8wj28fCDwDEjtHQSGISRHbBf3VPz+0TnYmOzJCIGIRCBCiDP3jzAz+33l9BS6kI2qBNxQhHjsiclLhROUa1REuFA0ERRaoM9IBDLEoseorniVIA/0TV1FpmLrOQP6wj/IhsLIYeC6Bn2ABQ3Vyt6V3efeJsp+PrOpULAVWjgHWnSrNxwAsHITjSac0InGRm174HrACpKHF3ZQqg6YEF02iTTkqK9rGouf4KSlk+ggSBkkpws/7DtXLzO9lkIJPNA7F7ZYgLqICnZXnCVZkqc0BALZ3NUi8zR+n6ZSTC4xRCpbRLOEdpwmRQyDhKtZJBV1mqURKhuG8/zWY62RfAadn1H/EX7kiXhTnjkg1uN1vgf8R7wKpAOQBwCEIyqcgDtiRsX4Wrsvd/+32r8xax9Anpk6wkztY0787TmXnPv81fUw1liV5p4pI0beKQ9lA9QogeCiGRIyTRUAixEGnA49Z/7z3xjdP+r4QQggQRsa281TCLXRWGF1YHo5axVcF1IBlz6Me/UkDhBOAAKKyEKUNocIAIFQoRKxYiXjwEDQ0iWTJEqnSIOo1Q+nyHQKCARYAlIEBAhEKAhYhRhtltDz0zEDg1NNAHBM4MdL8BAuc5k/1AAAYwIoA49/RAP2AGAggYKCCx1rUQxTDAXiFSARUQUqHZdgswKxRn7JecuUzxp0AKJT710i9j6mTmZGFZZUfjakfKoRzKrak3iyI1cpFaSgW0mgqogOJvu7WemxdjcoXFk2VVz+qANBLwh0NowcBhw46XsAsuuuQyUWIkSZEmQw6eAiXKrtOmQ48hYyZMWbBiw5Y9RwUKFSvRp9+AQUOGjRg1ZtyESdNeesX4/GPGa0uWrVj1xVffrNuwieG7H/b8hJDLMAw8PgrOU3LEa0cgeMNMDFfbxNMOC4dAJGLk4sMa3yP+RgvadJkyv9viGutf2fzM/krpDIraplKUF/0JBgwaMmykWPjeG2+9896HYvE3S5atWPXVN+s2bBZbv9q2Y7dgjM++HyFsD6jXPoVihVNs+xeWXD7AZ5VC1inlndckjOt52nBM/+40ZpgxzGIf2y/59hHRT2zU0NIiOVUwOPBRwR0+WuaMF4ew+9bnnGL4IxyRbJJvzZrzr/nKo3C4sDUzj/Mg/kwL2nSZ10ekdD0owKVQFCtJi15awrIVq8UPXPb8vBtRmEJsiNdpXbTpMpdu0ZJlK1bvx9DBx4oEW9xDB0xMLxt7/L7W9K1Yt2GzEosrVDyKlCqvL269illz/jVfyYu4kBZtugPTAczLsGV+ttikW2lRLFm2YrX4OsA36zZsvhztXO6yusX2B2gcY43K75wd+zWKzk1ff4WIDa3mxDqW9z2Hc7zYudCJaIvzrDTf9ca+20R81nX/SrQMSE6/svyzT9mvMAmySAcHn3vAqB4MwL458TmO3y0HeAw/WLmqqsAt5+CbZQfNZ428r78kac9e8c1O//4HYz+/NSlJsf8USS283vO9UbU7htqmpmH8tt9OBwd0O7WhGm/pm7UGgC919zvO3m/17fojgVPVlTu7Z5Q97gajWekT5aEK1v2y718X7bh2CRx8/EVwPj2eAzvdd+W8er/XSQeur8SNJ6OXb+MI6i+4c8wTXhENB/VXWyHW+bSkVtsAcOFK5+GKcP97Ds3nuNr2i8CNqW2t/OjkmszewLHOtq4tP/XJb8zcEnEOt7RJe78yZp/W95n+TOXbevplPBW4TgcHpmchGMxq8IdDqWd5BsDf83gPtLbxcPv+kP2KdvHeLuHnkvMvOP9+6zyo9h5INByMrO5am5/0uf+gpvzBG00HS7LFjbM/bDxNdR3cnInhcx18POGqz1rO7Ei/urpL7c2PLHGpPVZXt3J/f6BR+OAeBMBGCC5C4Xk6mrBLMK6SwkKOsrexBqJjtKHj9MwnGDNzkgWdZWM+x44DISnmCwqUuqj8fUX0WSXqi03mGH6yhSIzAx7FoygIgiEI5Twdc4G4XSTEJcISIVaixEaMjpAkhBTxkSMO8sQKTwIUiJ8icVEidsp0iAbhaFKGMCFWH17Rgxkw2gSGKYRmRkwsiJcNsbAjHvbEwoE4pUimV0frqCKk4CukdVQpJKicBPWZEf1GoIy+Hbcxk7CmzUDMmsfq9WVmt+ADnEXzYUtWMfuyOcZXm9AvmMTiR+3RfSI3clt8uIWO4CE+whKHhXIFQhGhoyQJRZpQZKs5d+jBiix4GHnxELIhF9+e1obQFYnsDdGIsdDMIYQlsbMmLJudmXeIzBaHeAgljVDSN0fku+3IVpEb+ZAb+Sw+T5BHQJxE7RNRyIJswDRnFbJJRIcYvmUXFpkV91tS1Ur9dVzOavVw3IVp52M0TrPZ1cRMBfcwUSlU3N9Qr0jvABULOsRQP3iYia60ScAKIXEmdoHW0Z+s+gigbs902gX+vnD1P+3pABLneXs6g6JC1fsbnOfv6gMWF1B8A8Gul+dDA0w0azYoHbNihGsW8EHviYsQLYFo6F5BgMAckUizPAeGJwkCqIFyaoCmQ18dFP3o1MexkYzUS6PUXXUGYUYeL5xAy3AeCA8NOlQDVRo8q4t4yruiKGLVplWLu+jadbink0QFgc99X5hbyB7o9lCPRx5DQfA5AbS6L77Dgv5AlwSxum7X3uORAFpku9a5PtgX1vgQ9jzQkAgEMMYZ/AVnTMUJpyGPAmqnTne6LkcDyFVWjOr/N1YQT8LEncBeCLCYgUVFqdSiVZ9RL6zZctCJh2k+ztHEaiHfroEyNbOsfb3VqW71W4/qDZ5nTqd5OzgYHwAA4HBapSqt2vQbM+2T7RDvThy+qG29VPxT96fj2LvqRAZxZwAcHBT49TSb+LnC42PXnTIXk+sQxy1GixIuxJDYE/nw88P/+1ykWRvERxDwnOpK0k/fd/LLY/V58T4+9HHizZrzr3mvUSVY8MZb77z3AU3iezqSDyRJtmXbjl0MKb5jBwCo3ZQVURAQmaOFdloUzHilXlVauI63eqJNH3/9eUTnUcQayy/sPI0+L/NcxL/mxXntnVnvfUC16KOFvAaaT3krlreRaMeeL376T5Jf+U9v/ouU8YPS94hsGSC1jBxSOjRAWkQ9V8/dRL1BgIyI+jxAZkR9GSArnp4EyI6npwFy4ulZgLzy6cXqJYB8cHqb9pYCL94UOzkpc3NT7sevH+4eKvR6VQaDOxtPAlTHM6cANfHsPEBt/DsCNMWDPw9CMwQKM9Q40ANbPQTk950EjpfD4Y8y+sf+rTHYOQDoo5r1I7DQkJsoDdVDj6pKx6mT0eY5jKPWikCdULeQvkul1Dlroh5qxghzjuRxawBpq1H0TWSpVnHQ5Euo/Q5i69vQO7laiocNtLf+N0gG7wOSJdDmDJD3s7CnKSnMofgykoWAQTVMbzPMj3GYJBqXABCm+EhinSPOCwktCLSpyVSUcVwzbsYSkiBFiAFggVuZyzwex6a5AKVHFOZLy1iEbox1lLU0CVP3T5MsLc6mCYBDRZxRnaFGdRyBSGByDUaYc0g6TW0tk4gp7m1znFcgGY+xZZiUBYlIPI6IhBoCrKYIkzCxBZswBQCTQWmNYWaUwyjGUWm1qPUymc7XF+PeHBfdCRU6814rlFKZ9Cnmh6f7jh6FI8JUl3OflwFdJ23xiMRb69roPUGulgrkjSqBUm+QeUwDhH1D/qVcu3gvnsKd/kqqDO3KRSyU4eSvjDslIPJgO07syHvSGr4Mwi/AHK4i/wGrXHO/3K356AnVTYoSk6f/2Ykt98Gdr5cS26S+Oc7WtOViWqbzxq1qiH+Le4qX+YKVvWR3yP19uzMww74TDSqgWlyq+cKt3Otw3EjaxfxYB3DgmN4tCctxoqoM8aQinQdOemCCr9MkPcJ7/ILvaN3rldtj0KfR9R70hJ9D4Ix5yFoe4MXzJGOggGo9Fc50vHm3fVvQ/pufoUw5EcdLVWTTDwWoEfMWWr+4bYh/d3pciYFSaz8xyUi6WExl3wCzLCbA4mzdcaWJloqcts9NAMfrHUIe+PCIJUj3u/gMChSQc7GA2u7T55hDhpPc+POvuqoSF8xm9aMJ+ZbXJKpeCK1IVvWxsOMmjPu5tFXV/Lxp8g1nwQf2f3G4rB1dqre2gsWFrLE9koz+fSbp9pVwrr7XWyy6XvhTzAMdngTtHTs2a6qWFx16foUbS23s5gMR0vECQJmuU7H+BJjbSzfI1Tm5LZjF1o1oGTZzgd7JjuMzuNgjKHfgULlsFLPU4yh7TqYQ7vjctvhGUhN0iM4m+ttc7avZP0suOqfwLqNiMypfqFiPzc832NbSjV/1bHHBGl+kJGUPF5/Re8BttkdS5lTulsaavX92ZPjaFk8VbSVmBur7tfB2s67l0n47+0bR6IfOap1Q3Ih1yi0f17gJ7/+smd6JzqjQIqzLx2LO9y0LPZ2p+VQt5jrdZimLyH/V+7eFYbQujlRGOYMSLZyT6FwhG0mfFjyj/FwsRkf6tn+ULWDeBFw/dWxQInzOwpfDYF/K3KImzFAPLGhPDELML1Sfm7xbsQ3x965vKSS3fT0qMOYvJNMvib0+h5/GZ3kImfMV0F0+z7eNFPmdNfaN18hxHSXR/P40CcBAHa7phNfTH7iETITiO8mqezaIyBu6rQ4Za0J+NhbTddBJHeJvZYmN4WM2ySKpkz6YqSKj1gQSEuBK3MrapJi7iUn9lYV4V9Np++CtR2DM1WBECaEUZWo1y2bMKXTG/D0VU4ZD43vf4AKmA+YREsnm9S4s4IqiX79c70O3PW06Oa7tzSjGDL8a8RpHep5NmJnburc1PtCmn45Kaumwv3iyTc/7quGrHU+SQtfUsfFTF0v15uYQfRudGIMEatPcEJYBlnjoIjR4AbcR0wOdp6CrCu3ARVe1yIAoEFu4zbl6R3VPzSfS4dyCD6sYqB7dvqFXtbAl9Mztkf87OkI+9X4WFGE1WFxGxiUtM9ggccSUGpjEtG5R6rL2VQlqaVNBpQwKsOnMThy/0oM1bgJ5thLA3E6QQ7jW+PO2lccviPi8pTvC2lcHUMktIPb+DSYoPEAUOUjAqYcuN6manz/ON85Wl4oxiR9DlJW2aIYD4U6QfBp7QE9L3a1fUKiu3HEYL9wrPw8oJLTosNRS70M/FKfl7IP+VZytViQ2B0HAIYx+fZt++Up18XmK9ZdMFX+SRosc8Clkpw7Pef/oCc31AUm01EykpxGrDMh6MWJGV6y/bFv18GD/gRvcado5SSk+xn2WqPef6lgJdDVLGzGmCWzI3eC5nKT5D4/iQFFGAzk3VH2FN/6gd36iQm+NJkfJ4cQeHJqjA1yALEJEmyPsEFcOshJlnC/8zFOmJksC7g/lNEQ2sJrOczRhF4bo2NqeWW8d+VcZkYUCCwHkGK0n2DHaDFdYZ4BE+gHFNfj78fZV4BGKrOJCB8rEBygwszb38W4udzG+/4Gv6hOYQTjPYmxlhsJdAC3VVtY5kgCUj0pc/E0rPvM0/WtU0cevTCOA1rSdE0ycFzLs5HBe8rnmjcmb2PY79/pSGD9AMoClFNpMdUe3AeQM4XMuTtu8LgMeNXlrf29v//EeybVALHxUDFzciuBoPrixqMmBBt/pP22ucIsBHSk97CEc0GiYOLGiUKmgPd0Ou5Gs+oPdkdh3AxoUaOnTMCDPFyOO6BkFVJNLnlf6Fx+gKWFUMIzmwMl2hTB6G8jMxzlh3mRazE5TkGaEkJkIP6JEH9LD8qzI/avCvDjP8K//QL/toRGwBDwuMAt1Kr0K57324980Hsc3XOe5D4Ao0560/1V+w0BAKKcTPmCcvBppUOa/fPQcZxA9mxwJVGLE6Wob9Su5168C2MbJlBYK8ydpgDlEg+cwrf+9VR/l78jfQflZf0+whdo+1JOw464+QF8j0B3H+NlpapsCkYGy49w3fUGGAUsfRrvbAZaJO04jaIsteKUKjdgn/Y+JhsiWlwgZDVwUJXltnRgSlZclalMPBtzSwWfjUtnbriYd6sLzokXdoga6r0/lWHk5RgW3fkU9YUvr5HTFpi/5rvx3VF49RXZR/PG5CJLiEeY3/H5baQh/jvhRNzHbwrtQ+baVv/f78G5UQJaz0w1PZ0uS18FmtqGrh3SCTePzl908QxWDI4dntkYZUVHJ5MAgX8BJFVIbG1LSGhqSteIb6lMS22rSGrEaembXlXXNtLU19MyjdCyIZ86J0VprE5NaatOJcXUN/E11yY0YdQMLZYKWia6upr6pkrKeqc4fRgTxKjkwggjmSe6+DVW3zEiq+ZrNL0bpFZM9Kba2hV66LkR+XcXg3H84Iptn9wWMnUrIdNTq0tlwkrFS0MdgG4p7bCSS22TmEeWn+3nOQt7cSJVo6ghcXfHHaBWYhxBt+Cm8fEszQUU0PY3Udp9JAMxFmxaKwkVpCSrbWmGlYHT9fWjpTxMaQZSWf6vtrcTRIwXj6cnfRBOjlbYUomlML4Bv6KmYpUzDNTKn5T1O+4uB0s3SlmMsWd73TrUfyXaHyytnIt5HtK+3R72Pyl+HcmKcvrzNZRpo9L6BNvD2mgFTZo5hDuA9UNfWw8tr66ir6GqTtfWgP6Lv3Ny1PlA9647/qry9vimDEtvHf5Xf2foqjhLb5X3m/yQs2L8T1afz9OZYGMW/GzUMB7vZbqHPZKdPAeGf+lUuL7JVqaEXOdo1KDYzkpEQMrNIyZ35PvPYN2wztcA1ogb4XYl5/hB308fLJzDAJTLAlhEQ9GDGg7rw38KQS0SLy5XgixF3w4bZP715xfahh+EUE+x7MzY48FZiOC0T8BJMXUNd6gx1Ci73gtr87wvoxkinU9D60uAd5yv1C7Zu8tHTayxL7w+aD+O4ZdaH3zi8wwPcAiIqUhKSi0NVbc+agMzhBxMnp1CM10fGyGH3B94f+vc9hmdmgVMFaxpNbnf4dt+Ian1SR+dO4pP5yCrm6FpQc78k5H7pQMhs7AztAEFI7IZvnYPoTamwB+R+9OL8a8xSHyX8ob+4vahP3Y2erddPn2295pA09omhzkTF+96IiH4bFn5y39E3rOcG2d3Nh9LjQwHxZefQmOr+FfY3Wbe+zMamv0WIvOoprmVOqBMT5+ZQH8ffcGpHm57VdqyKH3oV0UJNjqwH6SD9DwwpY8qAlDGZAcoMdodJh0CHCQf4Eu69vCWDkAdkEPIW2DcnqJQNPi6IIDrYudhYeDmZMhD6+rOhicWpB9UFZWEhKjpIJkNGQVvrOqGakeY3ks8rHNuvd6efdCkWSejSdHVybm9xjx+6E4UdzPAzi7lkoSIpwTOS4BG3gG3lClBOJAyLDLUHMq+zbafYnex6YXb/wo5w0thzMe+/N+ZspuxfsQqzcb/rjNnZ++Wx0F58/SWZPVE7zVjG0eGEydV1HlH/1GlQFDeVopgwnXWnr5yl0JldT9a9soU8wNIZP5GRYbcbFZ5XKKdi6BHPcG9+HM4aOxacQPFe+YS59vC/ICrJxSfMMW4PDNoXDn++gvnVxlyTKYoaT3qeLc/VeG9HGHt0c5+7NCclo5a+jXyOXW/4WZ2fn8BbouaBbBu43Fyt8FT7RY28WOMIq9uqQO3YzH/IZ+ONZ7+GQTkwxaL/0WQ3YOntTztzLECpw1vH+w9g6WVPK9t1wBB6DTyOenc8OFZbKIV6FqijECjIOLF1fOm/SzimJvp31HrA+iimfdDY8sTV/otgHAP7iamJYtr9cUh12IeKfRtWXZEEcdF/XmJ+zK3+/PhsnxxI9vUIoQWDNLmmbZTpAppTWxr/SBynnhu6chWyJvInc54eCE6M0AXU7QSk7Ij8DrzIHyuw6tIlMOPMH7MooTMYB8Cfi3ksLIdruNjJwfEbjJc75nJVeVrutfO13L4O0yaztzWcN+rG05RPgJl9zO59pKGmG5CvZts92LqGzoM9yDp+QsxLc+37yT8v5/g3/lydFEtMjwoJykyLRGTBOKfjRZwIxerXYHWu9PenmnT+YXsnXf0PZu/Bn4bqNb+Yr3ez1DcWNIkJiNOPGuxzL83lpA1HSmk6cBxS+o2JyYwJoFCj4xE2kHsZ7ETzdPT2JIxGTNBPrnws3PBPfB61H5gRkxCR5O1olEsydXsW41gXcP/I3FY5MznnWfKfiOrEIiqVBLqY1dHV36uDT+NBd5b+7PNFzp//C7NuPN1/npNblCUkNQp5ikeFbh05PUL/cbqWfhiMqrw4MVq/D2Ppu9xmF7g7LzBs/3L7zSmkye2mOQMXc++Icwg9eDnw7oLcYfnD5wffdb8UAr8/Bu38J+ML+f/yn8wtPAaxE6yTijWifIT1p6cq4czIdC3gLGt7C4HpVSEgGvTt87hr2wCjGcXA9Kq4tx5wlvUgaFbX/vT55bnmS3PPQD6mL78vH07nvZzkEDkM6L9sGrdCGqqfPUxuhe6tfKKZvZuTqbcJgWFNKZ60jHyy9ATV8jnnogU3qk0rSuP/p/GJifYWmnYmoHaWZ3/x0sgT7yQ28lvAAUPEK7xjdEmYeRdHD78bVH49+QkhrSIwvBFpBrlW22BvtP5HtBulbcKdssIijD72k+ni6sDuIvb+R7RdULBdUHQTtF6gR7WAgdD/n0SGWryDLzMAB2/Z/H3i7z/5Kr67UxvVTclXpz5Xjc0Piu4BK95PJ8/unrwT8XcpPfXd9jGUyxBrQmlJcL0XLS6C4DRPYlWrcAtL5Yp4wcMUejMCXtFqO2tdO8eGOglEbmxvsHTbPae3nW9pYCy0v351+KF3DKvPm2OzzD+Wgpdd8IaB2f7UmtDUuGhtL/LdJ9vXdn5UU3uCcjUj36qH5YZQe+HI0HBQ8n0bTMoQoyfAyc4hhxiLZQAOGBDNwDMQfjlNbSpRW06WqEXV1IKjNfjiupq2CrXpfiuxUFR8Nd+LlW9e3PBzFnPwIF2Hy0ZC2YOPg45B9JP/OsERPjwu4CZ19LVhLaV27l7bCNI/xuj3tHb0MvWy4iPCR8Y45smTHMUgLgYeDq/icRHBWUMm2LgpRq+nlYt7LpGKYuDB4HxF5N+VpLw3XMdYPzNXhv1eTb79kUfs7y5zffLjvaS+hzVpfaHBqY9BDcXze+VyfweJjDAAu7DI4kuK7R5dPI/bwd6J6Q3JJ4QtqkTeJsf1gIHw30+XRru8ozhI84BdYOx7+zcNfhHZ/VEX95icpRG2rkHJIFMfwhVJu5+S9fLRqwz8Z2yIZVqp1AlYEFYhqf2UNLnQZVmf9u3R/cHNviKGpqedhZGbtrKcvdIJ5TsWBqGWOTudwwkvHG/VO69jzIPtXJ1ctUBgVoWpr7PPThl3/XLjY5Mj+vzS7h2dHUwg1k8iRY7QO58N9TJ6nje3K0WxMPAf4Zckpynu91Ey/p+jNfh9UBM3bP66wUPGj39aW1kb56HiN9qW3y0/AJh95sqpw2cuxP7vzbmbnOZC2nzssGf75h1aYPW9C9eG9M7jV3Z2qgIGZ+2ODkvjIHqAMOhXITQnOGxYDE6eTi44x9mlgY6B5RnUvc53d8VFU1tksb/ml1psWmxXZrG/WmTBjKMecE/qQWDhrhz67/SnLvMu868v0H/vyommdkm0rz4cXxrsGFwaedSx2ikB+kM89N73GToRskVJqqfPq+l5mg00dy3k6VDxWfGqZy6omfqbQWP+S4SKQrgosnJEHRVVoo6cLFFHVYWoAwat1sGeWMNVdOLw02zBr+Nf75+ZOvn2+OzUqftnlsYXywS7/0WXrOIcKWTbtUOyQKRHtcC+Oe5u4nWzbpg4NmniddnPFtddNVbCtDP568vtT6+7fzh6eTlYe8R6Me/IjoHa9I3yqKnJssiY+LRMWpJryci4T01mTlpmelZxYP9gMTkrNz0r9zapfHLAuSQlLT1LLi48xPdZL/ltL8Xn2abP1GPy+16yzxSc5TXEOnGYTXwtFE0RLRC9tn9NqkAqRarwP4sJbkjkEPwoOP5x7yNoDYx1N1nRXB+2cox83Ts1/2IH/EqCk0LTHAz1E2x6ZB5J9PraFfl2N1gTxNUS3Zp4pta22RafMkhZ0SHhKQ4mZsEWcZ4DAdaFEr5GfLqCNy5EGLuY54l0SH0IFKEYJzoBdtZCQlUtSE3NTU0tSFVtXOzuPQMay2aFbVWExzOdN9uezJRXt9TklzrZmljZeSQGUYI8E2ytrY1caql1+ce5m6oruKpLTpkVn+KqI/A0Fpwy5qqf7LHWGY3RHtWNvOO9mTg+OKA19sgaHPu51PSMI4Liwrz96kNaPSSjySpaEmItHF5tMUnJRVlURedjRo6xF03PNSSTdMFcQdwKFM8EjWPCm9XAYp77uo25prT5LbBgY1xgcqyomRZJCVYmHJf0q8ap3eYqGajUU1HVL0vI1LBTktHUxNtBOkjVst8tvwv8lGFSUdnbgmIf3+Lit6VlpERpdU1pKXVdElHXS4sIl4biLccsqfNU9zH30Hleod/Gw8bQXuwWeRwT4KF22UwKr6Ii00NuKEzMLIm+dcPb3dkiNeAWcA37GlOdz5kpixHOn+txrQmPy7wT4h1Mk8i7mEGiR2YmNkXd7DmvLqV4ffSyewLYAa8MHOYTGQJ+dXwEkmYVS2rAh/Aw8B9w5rak3FA9mcFdwC4AldlojfnP3CzL37UzgKVD+gReW5+mo49X0NcmqOhqgxKqNl8gx7uZUoJLUtUUuXxN86wh5lQKIMUP8eOiOX5BvMBEdq58MijTN1CnVfFosKy8d6i6DeNi52tkYHMDnIyc2Q0YGtj5eAAlDwmXytIy8PC2diI5ONtZW3m7ToQvdfPF4POl8e7a4ju2dvpElkC+PRk5TR1qfWJ6B3WAEgBxnKZ7D0zPSknTF0doXAVCI8d5JwqMxT1DBRJB74aCioJKbj7+ar5CEwKeACZnZ1l3srMmXscmvzFrY3DQU202X423rWMkw80ioz5nt7QfFBX6dV9oRp9yxVuYJc66DvaEqe7KzZO5QvrS3nOkz1V+Cp1/5zv3xvgY9yZj9yoDsPTdhbW9yKzY96nxoQiE7RrPrW48fzTX06gs4KwecJhw4SrBtD3XKEm3eb571KjpvJXY4FSUkIyztbmauIZNa6JRopFV5Li+sa2TUTd6Dwyc+P6kVhL0Zk1p9LG7peUbzV/6iLYOHla2gVaqOzw5+90jH/yocbQxZGYD9S35AXNKQRktmSQ/oFWrITt9XSamG5+In3w57IkEO2iq2BlIdHxyjUo+FDV88qZPKEgJH/3DnG1SF4lbzU5LWYqMXErKD1h7FkHNCbARa4gMnaLp+zTfjDSuuqQzuCFOp90L1vfJWEMOClmDqJT6/Ki0ej//pLFWUAvU/D7zfa9dXVs3UFtHXU3FR5kupvo+HqZaKB8vr60tHG38PBTJ8oxmRrb7i6FCQg8ts7c+Qczx7Pf5s/xd6dLrzpedPso/l8Rclydvd8GqUjI8qQ/UAnl+LosMdtRnZ6pkXPIOoo8sC+N2MXVxD4PItcQx8AwInAbXOt7QeuacXHakEd+Sxc2a3xAeoHa/Eub+uEX/Iy2x466e7HI3OWyr2p7DI5mB9mmwOf8fp8jirwQ6wfS+vQ/MbaDWk5pYU7LKKNYZlzTFDco0lZ8SQn6mtia07q1wXEXtMDclPAwNTujur6P1hlASHypz9ZgHJ/GGdB3zDwgFKalvbEP5JXklAxycvJlw2OOyfXRYU0Swk7N67KaoMK4XoioEEVHCdTERZVUXnAtIU774RWU7XYjxUmgWOK8lksdkv8Z8R/lKa6nzVqLAXScQrmdDRWUWzqaAo8c0sH6HLWodh8ouP1dPr2louRSmbxaFys1dSAA79xdAV4rCMZJQpRMLPXBD5qaY608CveuW6ej8qYX7ru5vgTke7RgS4uYZHOIi7BAU5OkaEojYgQL/Xkb9qQTeUwn1cOhe4x381OxlQqPPfy3XIHPnkBu29U5zoZ/DacGp4OMxIHyKCWlJb4lRZ6+V/e8VgT8k6knBp+5n3AuqmpmrXdPdYtxAOYElnKwcfovsfzw5JoIakXDcf/NxyiPKo3THNFDe9QU92jvBvCEdiPLuGrBNMso5Y3BMaDxZNNDf1clZfbZzluYZ446S+dfdOXSGu22nnpQx8OE7w/25weEwNVHAfmTMUseTua8UHk8UvVXBoJFpzyUMzmGA41isDjC8IajQTBQYL05v7tALwvYO/rWs9nlbmk5M3e38rIoUJGqKJ/eKfLSUo52CDUHEVm/CZrJpUuceRgq4XH41rY5R3kGB6rvARqWKssasnPLG8sKystbsrIqW8h62Rkogqry7MdgfXQRsL/vBXwoO8o4FjtWPqj2YQ3z+6yUv9OzOgnOUZP76bTPe3AVifGpCWMidwrTYlnjE5SHGOJASKaeqpqpGhHPnzhZlJzmwO2uq36DQ0sOC6wtpuU11JeKvtj69z2+4pnJuTSg7WlpNQ4YMrc0mzcMmUZ23Q+A2gRwgE68YQ6oMzUirDrkRE68YIEP2LqbExhVTBE9J79oQREkHw93bTRbDFhdEnQ7BmrSKnE0ULTkypLGamlpVi9xW+PfN/Afgzc0MMXZpNjinoaVCkJIk6MvLKqhBR9qjpkdhfxgcQDn142D11Mxhsr+tSdr1Vu1tG7Aenv8CTHjG72bsAlnD2mIcrqUi7oqfFleECdNduSBVRtTG62sdr7NQdFUIYvaJniNh22k7ErOms0RyW5qAJxoMelMG1l1itNdU+y29twiMVTcFjzMIA7FgcUFYM92Fi+YUhq2Tpxnr44lnMC2kgdQGHeSD0Ab3MTprZx3sHuvELsLhDolb2Hr1OBcyUpYSg2UzDI3OMhKyJ5NsRozMsAoSXjBCY2L8xkvr1X0OJcpSIrFshqQcknt4raz6KChLIV02Y8jmkIaLkpgXN5XkmohnkviaSeICk8S3dklPYJV8GnIzeaQz+Z3biyDBqVVamoMaI9W1Al5q9UjO2Gn/HwVZB3YHkS7tNDItk1LEbaMS06YqwLQVwYyM2GnnH27SeZhaoH7DLy9lzeiWW84tHGlAymTGrMOV6Wi8z0zR3G6qjrB471RH5ZDEKuskvMus0cBuvdOB4iZdruzDeATtnaRdQsrNBnENhW7+5oHlEJhttQb0f8b1NK4fP6KsjInMu7skbbaU0hJaI3soMluDfkNvZmS82dt43vl//ZHy9K3CJi7ucvXpD+VTp3NrJoBQgHppBnD7+NEvBsC2WZJPOp1upmdT77ahkFoZijZAXz/sPWR8LvG41szMrC6DRUyWPCyf4QF9l0nmYrLsKkMNXh6PuWJ968PyQO03KV1MeWOWMQ7isbKyGuRVuMZU8vKYc/Q586ZBkwf68tNVHmO9n5xkYLJxgiiRzurtDyYbwmAC9bHN5mzt9wywI76+p54yeA4M54rtRxkwHevFNoM9Qn1PGJ0g7uy/ZII8G9d4YJzR/x/Nd7+4jwgF7V99jyGnU5oBdiQeK+xPBTvbz/6wI2EM9vm+bS5C2DZM2n6w6dPb9yRgbF2AnP79A8i1l9rsky52x8UV/858bAEAvPY792aAj8Lk5/+F/j/LYrfALWBBKIAA03lzdoAFzw055yqngbhN9K+Wxk1TzNu757oys5B7LXOq2Afti1z7CRZhwyQnxeQrF1sywaJQj0HHsxIxFSW2y1B9wlugAYzBEKQgFiigAV4f2j5PiYtPRIrb9QqTXZfFtFWK00W23RRPZ5fX5Xl0vLyiXcLdLg5E6nfvkqqq66Zrnc23mdVPSES0FUHrkye05VR/i56hU2/imcqK7NfwTKzEOpdLtYip3mOU4qjNCp9kwt7OJd1KofMu4mXGpmJFlBjDorZvr8s051yrZCZ1U2iyiUxkdNU1NJu02V6PcUtoZfB+W2p9TksmZ+hmI+a6RHD74UjfdaZwTrWP2C+zSd4llyhtv0awG6PkSbSK5FZdHHoZ+yJgi/9Jqv/YtAT2M3WWLVt6W2ZZvqLtp71R0lhFlw7H3KPYvp1slgioR3LaBo1UlZFcfH0d0+oFZ/yz9n2a9ewKo5klz7ZKdUoYe6VOv1N516S8+iRpNkCr6nlWNrVKYlgF4lJA5tTJbcKzbWvyskNImxdWfCCe8E/2m/UNlc3ExEY/13hMMp6TaHx8miBCYyaRU5J6nZQM5JPezFqpa7lHutI4dBa+Fa+8GvDXY9emc6ULYIFt2a0qYgBdkAVpMAR9JeMrTZ0+wk+dn+7ibjV+nlzalp9g5PwSwmkY4yWsxVSp7Tu9fiknKzJydmc+jA0psTPrQ2EKjE6nTNd4UNt8Yv1SLlAwPy8O/7zyzzH/G0T+OB3/JdMRBrDm/VgMJNQWcM+XLcADGlILAieAHyMRAhxGonAK2Rnt74xhJNZ5NIGDtWTKycwoNf4ChArkzZMXstMkiBFvik8j8g9DPtzH1ubHlYhQccCn1qRb3jKFu9buAgVV4dahTfhzaYE85zR9d7U3Ct8e4WlYPpz7wMIIdIu39JsERNZLkqUw166yqdNe47IxWQA80QaDtQhnAa1wDZndifAXyFP4FOHqSL+ZxS3upkebGg0GTJW8NkWN2GvGtcYfNzcAAA==) format('woff2');
	unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* cyrillic-ext */
@font-face {
	font-family: 'Roboto';
	font-style: normal;
	font-weight: 700;
	src: local('Roboto Bold'), local('Roboto-Bold'), /*savepage-url=https://fonts.gstatic.com/s/roboto/v19/KFOlCnqEu92Fr1MmWUlfCRc4AMP6lbBP.woff2*/ url(data:font/woff2;base64,d09GMgABAAAAAClEAA4AAAAAULgAACjsAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGoFOG5JCHDYGYACHDBEMCvNg2lwLg1oAATYCJAOHMAQgBYJ+ByAbYUEzo8HGAUABZ0UUwcaBJBhsoijdk4Xsv0zQjrC8XdgD30udEuoRGhFDq/g6OlgjCZ5LmfWhOBwQP0/oKAM9aQvtywB+hMY+yT2en9u9v40e0WLAJu1AVECYSoVE5agctKNKaEElSpCYgRiUCUaCjUEqiIGYoCwA7nPaO7OAwu4MHM65C7F05/9fD4skdKdL4edLEtOAviPcFZ3bHIrGlUs/EE7h9SaqJrROMnuXdxRGI+RrLEZQFAbhNBpjsDdnczbBwwKhs5tT76b1EHXrQ8aC1hhn/r9reuCSqzGtEWjbjV1gm5owE6pOTUl6nj4EjuD/pqV05jTKSdFd3snuzErrB1iMU2lwkAEbfe3OjTRbbufq6s5tN01r93Ur21zWTmWl0laZWy0opXQUZpgEoMDQYJwXVCqANDwPXDWdt3vj3+3gPLOM9rq568JosG60hCfQHAKDbCy1h0SzNl1mEerj391Ic3QNr0sOu9IVgoRrkHD4v5fl8oROKq+Z1QSoEUqAjBsREMYbfu3PY1s1qcjYDFlYIBs75OCIc3NHvn4oIACFhKKoWJSQiJJTUWYmyspDxSWovBxVVqLaWnSsAZ04gRobUXsn6lZAfRAwvYNg1iwECxYh+OgTBAgwAOABAFEYIHDgDADZI8MFKrTgDb6b24PEz6SYMJD4HRMQChJ/fOIiQAIeAKDejRb6KyYCOJACTzoAAy7sfKfguRWreDA8KPW6dlVc1te1O7ORvie62OIn3ILODnzFMcg2C/UtgG5CIk5Fx+vcsBO3+i14iwcQaGEqDtBiyoyXrIG/QMF+YTFiZcmRp0i+34KdevRjOuOsc84bcMGgUWPGTbjngYceeeyJSVNeeeudeQs+YAdqX7Lpa7TukNTPKvqtqt+P1xGC1H7GBvTOS+qClM4r75zKzmroPC6g9Qh6AyQ1R0oDpfaNvH4v7xmVvaOhXY53AhtQu2rTNVpXK7pW1bXjXeUEWp9L6qSUPpPam/L6QTkiVOKB433mRL8jaOh7BKD2m01Xaf2koj+PdwUGtK608+MEiz7g1TeCjG7ktL1JXZLaV0o7rK7vNBCAwKKrgqjNoGQgG43a8ZEgDa3nUnBTkCql9Wm7Fyavi0obqqIVqtqrrika8MHxlvMKLPoFO4IH6P3ghn6nvI9VErUE3TatHmcqBLrngx4ieIUwxVNod89qMdY9X+3hhQuBSvVTXEDtWRZ9wqYTaDyHci3mze4t7sFd7ZZ8heHM4QFw1/KHGEEQQinBmrNnBAAI7Ck1deGOAuKTC073aZfhz4CYY9N91B9L60HzGwIUvRLsud7uO2c03SrdQ8f6Fu7K+rx/cIuEn729q366n89dOev7AwbWGuSgylft9M+XYlEqseBm/AKttAajneuLJRPSq12RLY3IWnJxwaeSnZ2fxlZpfp3bh99nA+tc7za9jabmwR+UT5UzlgABvDN7zB7PFQGiK0ynYWAUPa0UJJWsKQH7s550YLq9P+niA9opyp+NzMb2itspNiFIf5ZDh6exA7uvXvXB7rOBFdKK8rUBDwCAmlxq3+K3t1/60DjNutMrEQrSG4FmHb59h8eiY3x/vfrq8Ytz2RXDRupydHYcYLDPgeF4hGgFA9Pg4HAEmYUTvMIOviGCX/jAP1wQGC4IDhfEhAix4YO8ICiKgOIQoSR80BFBneGEnvBCb/j1hRv6wwlnQoSz4YPzwWAgbHAhGAyGDS6HAFdWHF085YBRhAhj4YPxIJgIDu4FBw/CDg8j6lE44XFEPQknTEbUVDjhVXjgbUS8i4DZ4GE+whYi4sOKq0yPnTCYnQAEeI4BHniBHbC0Q/FLd4gDuOYC7os6xw43GuKG94ZQ+ID/U7jmAeKTn+iauCBA08UOFp+QMzvkPGY7GV9zAjuzMwKwbSdug2hH8Ee2fQzwwziuFDv27Zj5EQ4QcHw4+R6AC/ZS/fZUl65Lt6U/CvcjvvMeQgAg1M8+gCTjA+6xwLfdgK/SHwUw/mFlDFLxHqEFmwCBu/4GKagRAOCpwyRFSsrNNn7DWHD9k8gbHhkAIJOCBwzvm8gkJTdlaUxX+nM5D/I8M/kZROAfobIA3jt+iUpy8lORpnTnbK7kYV5kNr+CEQTKAhsvwMYTALAxATbGwMY82JgBGyUAEJABSANQpQfAFYAvgFwANQC6AWiimtemnUklGdaKcpdTXLh+hy7P8eDJ63vtuTefOlPV7TR/AQIF+eBjbTl/5XV9eT0rRar3ZirN79Kky5DpCIYs2aa9kSNXnmFDdad81TX5CuzTcy8U1q2qJctKvfTJSY14itKkLJXkGUyFzzHQBHjfDsx38X6LyI8DokRrEyOeiQSJOtDRdUmS5JRkuczlK+KiWLE5Jcp4qFBpQZVa3uoc063eCf5OavRBky7Bup3WM7SLXnilT/8wLs7CZTGuuGLWkGEpRox6b8w1TNfdkeaue6bd90COh547t37hfZ4GeVX4lxoMV+FfeZAPvT4T/w4A5EIzHsgFfNBPD3ztXwD/G8Db2n8A/i+A57XXAf4/gEJ3E4g1AiwBAmw1KK0iIA1e6gQM8WtQVkUQ0OCTThBEQt9zOAkBBhyAsVgCAABYAQDoBYBngLgORD4B8HEAe1+RARNHDUsK5uxzT8twYNFmihaMqJjDOIxYKkLPOchlcTMFR1mW3c1LsGSnOTYPlY+uA/LIS2ry8/18ZwU5UkpEC9m0Trrn3C/iBmEA1H+Q+5DugM9yhUJid8l+GNw7QV2sd+duPKvioThOGB/0ipN0SaV3S8J29l2C2yI2ebfZmdvYtT3Zo33jgbHvL33frHeSp1n0Zvik2EMHTZVB6nnsM3XVvrfut2CkWVgWeR6Gvn93dcoqUVSVqV02kLfuBXSd4j048X0uy7r0wzw29eOo025FSvT6pZUkcWCCqezu9w7GwT4e+EObtBZNVIP4AxKe0QmvSD3S2exgEf8z1RTTgnQJEeRoKk7EVwEsSQgSCHm+mxC4gccMKbPOG+BMpjpbViSSu7912f9qB+nlefcLrBcHj3rUJ7zV7PDuHm1v0sE+ywFPXfIyDj5bgy4JC04MtlkHoIJbUyGoRsOVip/Gpl7dxfhqkmswAr98YRUxtE6KWAyjPZ6RvZRhY9KEdVr+ZkRYH1tkvAK/yRszriErIWJM5HKWOX92yRt1GNUh4UkB65Ad5+PZ7KyxEE/yNBX89NFAO5zUP1Oj1eCLfrjnk4wllyBRcCkKsWlMeQzxMZClEd2HFfx4hww0DDJemZWmlrd1vRh4BN3qygOHgiNKNa8gpSFH4Zbe3BCmPks1AeRQ2zIgBgu+g4u25dkPHtWjxzeeEZrr/hLe1CMz0Jne5ut6xOL3SEJpGR1WP6taKXIW8+iL5xBdqezdOBmnhV0QCtSGQOLBRvoG9Bv0Kjnf7lul81++aIMmm4+rKeLfQficvy3RLnl6l2Sf94zHrjng3RnJ3vIOGYgaBHYLLZCXOaXRtKp1CLpTEu7IjPo0/tnVkcYLLaeT0wOeRrWw9l257rbKvSbrrLcOq71dhSfmvIN8jHxhVw3YaHdWtJ2QAb4Aaheuul8A5vAfygp1qN8M0Iocvb1LHRqHlUl5c49OeaasoJdsdZF0fj3ydqnP7qFWK3REu1xO/K4eiK7Zl2uSVKBJUq+4RfpWsSny2eHxe3QJwxgGrpOFCj/wAqeDnqMqjssqJiaVMW6YhMNfOvmoS3NQAuxyZ34Gb0qeEbeGqAKm8D12GXsdWkiZBudaivoaF6CZx+vXV0yLEvyM0wzU3PfjetROjrR54Ad6fNBLiEMMRGvo7ZpwWdq1ow3QI/d5dPh9+G1jbXTAn9FRnp+Syn8Uj3bM5Oz04x03TUmDMYAbyCqcKOE8Vjts7MtRShfdAAhhVyFeKRC2nGzfei3VgVNYMmk8tM1kd4BAUgrw2tkcHtyPbA5zhcI/SDWcfUJWtjCQQT8yOlqQPdqZlrEogLFCzhIDm3obzWkhNHIzoYCU21SYBC7xqm5hw8qP9LbEb9CABwnlk+1PznbnCLE7voGo/2GcTLR5vPjRpZpSxjsxTset9urV8ue+MaHWwXN1BBoXs4M3lwQNNJNaL8iCxjqDHGM00DJOiljS6gHMDmsNeP0oho3c3INRmk6p3IROsRgoAZSONDv8Tu2+TuHAD/WdKBPNAO3lNfG1doMMOVyUCmNhykk1wUNUwI0pMrDTsNr+CIiSE5cO/iDD8Ev2kg8Bqx5+Rs4ioOMgm88nJASsjJjCkCUKxyuLYC7N4Vq0PtDperL2lDCDeCEZ2CSwMWSVmClR3kR4hqox2gxNFEeZlrIGMgtCK634839gS566H5VDm8t388uudchvX7YZ97z8g7F6+0Cn2R2pGVy/cw3Wp7et45rcuRGvTu9atRsrT4yS4ez2YTRCg3ORgadZBy5JUMF4+DiytUg/L5iQFFdbk8icydLor4jtr0MKEkp5frxcUNxefGXOJ5UDmtM8g78V2nVNkmCZeVOlbMsJtnoKmcffndG+Bo1aY2mHjJyIRYacMN4GaMCk/x4gGyJ0OWiPngOqxgI0HinwQsvv0RnaDnICoI4Rnonw4vXtpbjVnYS4QYit17UXXujIQTcxOipaEd14YZEjgQHn0k818uQgEqtr7/JpppBOZQA/AMAFXthWm3HevJ2zHQMQwtCiTtDnnhmTc4vRGIB2ucudn3LJqGHGMaq1vk/EO8gTkXVuFfub01cDq/L677f1+EbY/Z7NeWfqYCekO/MAvQVd+NC6jf/F55OCJcj9pIkCLOhyOdvq2zJ4Uo9qPORuEx2r31caTOKFU/SERoqZhg9IvpVVuhGaqL7cq3Oc/XDFZyB3f1sBRShsmOhU+E/Ax/C7SSFKL045PPNzYAO8ZVMOiQj+bNlijMM903JiF4YbjGoXrZc3Tm6STN77QKVoGfik4m/S2YCHG8bnetraCWI/uAb8z3N6/UfDOx8txIsbj9cF6CGpa7xrFdqW8Go7oS2F1JM/4GrsyjnLDMi8F9F6qX9qjtKQVtrXkapFTow+oof3FBp4Ul44g8oqRCDKxFdpCd05xuJKaaVNwTmw4CB+SGYKt0O1d9mOOTjaXunoLr/BUO3V9v/R4mNznYbU1UPwzEJ4q0WdqdmgoSMSGih/HWM7ZJVDJs96n9vxaxHnRMmVAQGyLk6VNowaz7hFHu8oZewjGjzLsCE8w4xLLoYBXe8AUIeW4B26TgRXCaaY1W4HDZNCeU5OxCWhjaJDJ/uMduNs7fCuEb0Vjrg7u1+2EQ9THpin/uB98e1sjcHrhj/Dk//l6X/vOS6/lS47viBwKazGcqeOXhKiv/8B4xARPgMHjlB4/XZpkxaYomQRpjZpR9gFKbAqX+Bf1iCCuA7d1OVJpbZg94V/+4hiaxSu4tz0wpqqZ2gmYqX0ZX1RfjpnNmjNJurktq0+AgLzwVqLmw7YqgAlyjSKAgQmxdLUkgJXP+cHUu1MMX8pMQOpwnYFSqXb+8SrDj0Qzz64vbe0dJ3Bdnbwea2+RP/gwNbeBj14bPK83kiiL2yk675+thsWWj/X1bYuw6zxu9af9bXNKzAL5m3DI8OU4YtrLSB/7jL/T5fOx2ffeIj9L7mH0M9yXEAuS0q5zGT7XBP/Hv76PwVTmFUvCI471fhosLhnc/Ab5GiZmbvYmdoaHiJ5xjZOumZPLE9ceArHFCwFBCd0QjUaL4e9JFaDupkhqExZ+zkhEz4Ij79wzz/1I7csXmidU+Hj3R+LaJQP2fkF2wYEVrTv6AwoB13p/7O7bp4MjJcnAxsQiDGhud2jixSpX3W05oBc9ajTalE5j8Fb6IvizrVtrWmwWFj1dlMMJZwh0oozwjo96CUnddzeBHJl0R1DYnlDK8SQl18wiFfeiCno88bXDZB6PaycPTI0fBAJ2IAEurLNKRsfC+pfC3PzLSDTvyKUKdFdmBR7fWxLGC2+6UxtfHtoSGwbcP0a0FPbJ6Ajpz5mN2q3X0dur4A6YHXjTepbVWfp3pPvtHUEXzNtrffQSM5i5w/tBzE1BzULk0NaZqbBZodTzQ8/3qdubUI9YGmqpvrzP1pQa2c/23kW4o7CEaL5SIe53R4Z5p8VR17fXysrfAUhFshlv5gmWLwX/l1YoW1wzyLt8tMzzSd+n/4OpzTNrR0sbD2NVKUEq/FDjyZjw9Mix9HUKtvHE8e5YguT4i0Yko4U8yGT/c+0VaOm9sRSfIfbPCHC7pCGtd6evW2uYTE8YenbPB0DoJNEJYEU3mpuu+zP7Y30zbdFpfBiDVs+WcoXyUgLKvbfPWx52UTzuS7nviQLn2i+kEgJbzdf2LPwK2ekVX2kOe++9mEDPV1TA22Qa2fhOCmv2imsecoUSE7Nf93JucOYc+kHpzjoTw2XDFOAwLwEGoLJ8SIx/c0PlV9gXPUni0pWtuRSctaFpA8AxxPKwxOZzFhRVQrw2k/MK4uep/STLopW7VQGzz0X7+o57vShm1Rine9vsy6Vk/b5H7K18dFVV3U55Hrl6uh1yNwfdtjFPcxQjVTPeenRO9GzoIGnOvNpaGBTwQI8mS+JXGQN4rbHpCd4SUF5DPeX/EXpD9UR7Gdtem0oNmcOWids8yP2t8TrH642o5hVHDgc19m8dY03EdTwknZHPEO90z3NKQnZQz9o5f+2P7/lE9ceuidRIaM748bexc3G/2GMoTRly7zgEHphqF1Mim/iuU+XnmGgNcBUAgJTaVw6dlyXM7fBVoaZnbThbDHEiwtQvF7YBZarwlG83os8cuIN0Mri6DeQnDojbxl1YykYh32LVdc231Uv7Sn5g1cfrG+2XAf5/G3VIjtWcfMPlCYgNSG8srr8J6sGJOZalxHlz+SUIvxGO4HAVGaNfY3NTwvxY8REIwiZ4ykuSs6pLLnGNXJv7S73tZqh4vy8TK5SisgXFSG4INhdqTXlpadDF8fU+fkXQQ1nPWZNos6ACSrPKieVH3l21MNezRoX+L4ypIpUFTZTGWj5z+mgH3Ssax7WMjR2pqqTKtcGRx5/aa01caRb7ryoc7l6kzl18//gcdJ+H30rCwdjqrHNQdsTQ525ONh9nkqiko7TGshUcgOIlsaVkaik2qBPNeE+GEP9COjeoFJJ1qzwkZ+rf60X88WW91oQjE9zPBC6AyokB4n5ECqpDqu99r+4hEQNgi3kpqAmEpV0NugsGbSFbN1sybYOH58Ai6SseY1+/n6Ixj1IHuDhYh1rYsm/QYLWfw+LJz2i+uJO/3v06hOVLKN839HNytDIxdYZJCv9Ld0cTfkj4gbuBCQv8CliQk2Jf2bz6ybWFsb47dxDrJxoydx08hnytmWIVmO+meFk+7CQ2JzYvLjARZibVpqC6+4hcZnvlJ9t3nILfv6ogT+eykDPln/cBuw2rSy87P748ho2lUsuxL0u68Le5ZJL8C8aKnGvikDRgArSk5Po91Pfny8eYevUE4Mn5MnywLpmfN/J1cnCxNHJDoFEpXs/8cpxcbbKUw5mQ4QrB3iGepf72B+WjZYX5qdy5lLs0XgsPuRCVOyNAd/SoCYpT+Ql7u/UnZ/m9yVyRmzJcNBI8PNFa39nZMyHTm2+H33Kvi70Ovc6mUoGDVKrd+t6q0N+ywEpEaaG5E/mgR8xchVj8JrHgjhKNHTnkdtSRVWYxpCLmHhg8nh7hs+GkWgLQTutmrKgpa1mX5CuaMG3CrAsudZDshUj0Dj+byVafkvbTzHPfpYgH8AzQCRTycJR7B1gOin5m/3bNP6ZH8UJooK9k5DbLsquUEaYL0Q5URxgAWaJKLbMugmyfa6E0kGb4T0mMvB0dIA0DBFObtH5pHzRIHsPCB8ijaBrI/evC/I/lxW8kUbYgGu22Yo+FA1+ZmUxX3SRdRPEHx3Jl7Sz7H3Vxwcy5Vy3HyVRSVNJvJOy/C+z4ciD0fZecUwJ/t9iYFwqYR1kDcTNI1wTCasYcN288HnOPUuhqDvtFu/iN40w5O57VNOGeYzcNYrKi8/VnUKHgyDJ33ZarGi1MRBFerBXapSX/uZWEh9UkuD8DaY/YiY3J/lW0M/6kq7+w9O63dW/eh8hrRTk7pepYd2Ut6txDCWmdRa2qPkk62vmfDt2/sQo+z5cRekvbiXxC8pbuH6DHZkqLIsmUto0yXk7get2yyTH5PJXvwdct+LH5bY8AG3ZltSNpfxjy6IUPHf9Ml7Ma3gvIsL1aTPeLTqNJ2qAGBeV5FLqFp7AF32eJyIyCeiFgq6Cjg9eaVw805HvjTa2OpLjhdus9u8IYGrBzHEhmiDN3Xd3U3vPEV0bsXkh2St3l74Li9ygKnP3lr+esffzxUaRpBspn8liKQoHZESSP0uHXqetSu1I3qPMPhY3HyYVeN130ixD83L4fJC0xw2fUFLM4ijoyr5blBuXCOkMvS6xKP2Bb/ajzLh4WEf4TYk56UUr1TKN/7xxRvV623w6/Ur2c3/Pp0Z0gI0Mc46Hc2lhjotz4Q29hl4zvVCu6MMCD+fcmguNnrWURaO7HOR+nts9M1vf3TnkL61/2p0jvuYK2xaQJswvigbS+HLI0x81Q6XpyklkU5CL07HZFdwVtMcrsQ/0z5s9+f0Tnt14/vH3zSt+lQGVl24urGGP7j3//XtyycItIvEa3Txcu/pbvXBTsnWkgbZ5OP1aonuE+d0/ueLOtfI11J2KwcH8OTJRKcHhdhIJ8UCK6HvKsefz6NDRDzlxKDskYyDDDL0p1b1XczUIOTYZ5zNAdYZGcX9M+nVoTba/UBDVtBMkcL8pBdHpjOSi4R7ZtsIxOtwEFZtwks/g9qekGZE/xF0Lt1cXx/jtPGgW/mENUC9JfT4IzmNSp40zyF3ntfrfl0h1G2VId98OInWbHCF3XzTsmav6ftOYpUt2gdJNqXQmwGcyhDJhGsRuiXyZJufzB4fsEgyTr9NMQQq5btFbHliY8oFs+3TZMDRRQ230QSpewkafGgpNw2WgrLpmHxM0EhtkH6Usq6IsrfJG2D42yMMrLtAheo++MiW0Gv9yqCmgkiJwl1VWZStZXK+sRHdjbu1MqXWXmkSV19+MSVe4sv6R0rKg+EBvkq23oTeurWU4yea5SK9tX+6ap/i6GJkwD5yWoXm1T1K7Tl9felxlMoY/O7UGBjLjinOo0lDH4KaiOm77apWBrtF1RVA0+AtzLgH/fbZGciPw4xiR3wvIlL3CNdr+JOZMlzwoSInPCnDNqkze3/3S5YR+aFpWBBK6zaP3LrkwwBX0nwihhBr/xGAnnzjHfVFt/tGbJCC84uGkBad1TbyRw0sRakNaA/P3Bl1RDck/0BCZG+TsHCpu4tFtelLbzoN7xdON3ptYlZ1gwXR65pl+FK7T7uzp7aDqpPXY4G5eRVhEFffxnfsTx7x3BWeAb3plaoZqr1dxkO1+MlOOSeiLZnJZqs93zttYKQOBqWxhg3ViMpvmYI6nSiO1/THtpbu767qtqMEBtBqpG1FqqIFDDLseO6Vbr5bZjQv8jCLGmF83g0CtuSy2jGRV/Km3NnCFQuAjk0vM+ZpaNixH3gqsJobivKCz8ct21v7C44Otvl6ZHAOOOioNG9PWeTSBbkWzg4jG5lLT/S7WpfGbRpGW4UevOZKPfAfI1iHWJGoveL8nvr2o32B29bZW7znNsanuLNGvx2v+e4Rfg9pX1x4v95y9N7F75HjE/2uFKblPquOEek6JtN+y4fMMhlinMXuDrJzFax1m5fbFeCJYSUmMV6km7y5dP7T/l2texPS1hOTVWt0Uf//coq5onFfmhfrzI8u1J27hH5ZM5BYaE+3/JBW4egdZV5e7749QUNzv5wSi87fMl68umzw1HlUaHReyEvc6CPyV1tPL15dvBYXYkO1ahOSIusqwpZ15PcY9MoSL3EEK5LDnq1mjAyxvyBvP+bFj8Q/vnzX0fAL4yfZO9tUdx4aEr43jev1JAbgMTwmsg2fy5T8y5FLSAQpLPYJ6Ucf7SuNv8PLi7VVw4JsY1ctml2zdlo5358c76ErQdUxE+EQb8CgEWdR7iQwMXT4z3nFI5Zc+aVaRe+pJXuNldGyfZG3eSfgp9GEnL8/Tku2VDxsHTsQp7bjD+XX83T9xSGASaUUBH5ruUEl3IOUbvy9WY4b0Q/HX+UKhHwWnYCEls6cP0TT/WarXBpj6BZjEgcoTl9Y9p2ZPobK7sJvj+ygGpYL3mkogyHbUaaMmHLYSmKHGoUpAmJi4P3XBSp+f+Pm1Or032NhudVIgPkWlmiv0hNFbj85b6fDzzp/WoPcPAer3CcjevGEeH6pXNNqsKnW6FxRMAiwaLrBduX9r7N4FLZVfltKfFHjaJnO6zgk05UnWdZ6A3fhhn7VLMO7PGJSaWL56nbT0q3w5KHOEQbFNCHANOUJzLMjOLs8ZAuU564DlQRgJKLkkeXvp2nXJj2tHl4Iyhhlk+2SaR1hOuHNRbnZ5wUVQ3zn2TQkIzO1tG/T818WFua/+J7WsPpj+FpvHCKJl5UYhaGhbp+e9Knmhwrb++ZvSiL498ssnpCJz4stdsTIS1gqb6DBuYZ33NT7j2/RqTBsU+bZxEWab2Xuz2itOm9Rooe+g5o9oHbhy3GWd51u3BsjfQgGoM+33XM5RTpzHpeCBseszUusNHAftIv1DTg/uuk8/6MRmCndsfl3NgiSfoFM/7X/5ICQweefzVm9+KttPG45aGXfk3YIvHuzdKbvFR/E2CkSnjvz+kF3lyC7oru1Fi/WFPT4Y8jQyROa5WAKuTrtdgpIjlsGBu/txdeg8NhDFyTAJjv2PXo9Uo/CMNhZhactdLMY2xu5YfavpEAXfDUd04OPmB/y1oPTwaBXCkuF3HHH29tuPgd+NhAI/3OT5zjmY1ljuslENJHhNMnFDIUtL0JoBt/85O+trHnS3sAYl9g/KL1iXm0+cdOjUxg1NdTghCmaaEbiQ+U7ItMEqejHQxl8OEt8gIv9dI8XtmPOqcCSv1xxxy5aes/uMtRzV3GgncRWSqnLfTEf67/nco/efTNwCFZzFa4HDAdCh575qauqiZQF25Sgdd157XELUdjcTo6P66LkHaVlPno7DKNgsMPcd1rLb540CWnArGZXoHWeMV57HLlr7Qt8uPF2XUP1Q+NYV002mkttj2klXzUyd2m7I7ih2/T7t+1vexuO4qWvc9+CK8HnJ403i0Mfdf3Ir6eTAuzznQ+/z9JvLBx+dQq0XnlREoYQYLAArV70pI9sp5ISccE+l/4DvU31UIxEl6CmxNQsO4zrfXRj5Rtn9d0fdkd+fi45d9tJVmvPVLKCldZeQ8/K6YdvU5b5fmb86tE207FRngbCbLmi/lgFZt470P/PvzBu4h+Ocd0G01Vp+uWhZ76WpqdMhC2QfjvniirSrJBRjtiThcHfXV29P4J5c574L1Q1OHcE50ceCrCkZpVc5g04KGH24mZHXnakQvTuz3e+W6kvisb9BZSN0JfNCr9DMKm/b6xw3Po0XHfUFVUbBlcEr4G7biwBxivM55YGQ7E/aS4JTSBEN33pwzkKHn7h4ej+9RxIU8LqzJwPzD6l7evD0ReWLszNG/8+nRoMdb6z+VkOpaRxs9Lu0VhL1DTJvrN4k3ewa7BqZWR6bWoUorj7M4XuD9/op/dcHr8/wtssw6jZGuvhm8E05qXx4cFjI5meVpe2XpMMWoHWtLCqr9AYgxcgYtEG3EJoL/8+Bu2ACX1BffZRMfPjPF8X+iRwM3TirfkZvQE/t39kEAFmQAwA52I7YkSawAx6fyKpPiIoFeawNA5eGZWhktbBjAETZIJwjzQZenewxcGmEw8BnEVUDl6ISlTtExMBuEkUDH0zUDZwPlgeuSkZkrzVw6YTDwGcRVQOXQrgMwkuyz8B/lFLOC7OpTqBjKwrWlGyy5aCJVcDyAfruZK0zvm4rbsZsxaYZq3Na8TUA8BSqTmNsQNpjaVorhwNJvDuSiYhZD9w664cibInicwEINyYgTJG1WlAeK0fdbSqyclU28zqrROXHOm1swJtXkD3mZKW81HbsENLEhmxHZ1kbdwmhO6widcyH2KbzB68rBi+kmBmt2EKuSBB2Ay5GBLEVHmpe8hziMirbItsIWowQr+uKYdKmGDrGOn+OGJQiLTtB1wBFzhKHu2i/UAG7ipv9PU8MviBNUyxiQ+UWi50gwhX+ioliGjV5QvwhVUxNMTeIU5yrDaKhq0D0iVdgopwgovoHS1VQxohoyzcAu+649tpRdydBRBFhXMJEvebnBmi7ZpAcTvy5u6gLkfsBnbeGJpKLqghEeXqZZ3VSRc24/hBF4+4g6sbVqEopRKJk+B5NwZg3WqamZs+19mYDIRnvtlNYlPinyj508hPHxzLL+PcFgH6C5nd3i5Rl5H9WCZyGN22Y5OR8WzlU2Yk5EyOGnj+YgTdxJP6FRbEhFsE6uADPYWc8bcmfoGFxUTRkE9sxaOEokQ19uwDkBmOAzDFL9bOm4dPMRaFPoSviRA6Vaba0wbpasFQBoc4yOGTCmWVzAxA42IW6DPDSM0vvMdSbF0uOsl4feXOD/pHCV811hoeEex0LMuXxG9RiyuN+BRgA/quzBI5X2FC1RFdP1wLY17ubHGC7PKt+dewABPdNYdCVsBF+WZXLm/I6uynXw3JWSawxAN5272aiuTustc36aZTXW5btCG8VmwhPc6nEXX9Iv59TtQUZduV5PVPogCC+FkQC/LbpPK3eROq4I6VGeXPlTESfXNfY2js36HTSjWXV5rY1pHW+zG4h2onnC1Yl2U9ZvQtwmz+I0HtaXaI6pWiMvXVY8DYg8ND8nQGORXgoB7DFATF0Nn7gMG+BBSB/GnJnmCSl6VnVCuFpfFha03MIRWqP9ZugC0eRwJwbHTp78WdjKRjg+dgAvLqS3bGAAADb+XVmDzVNwoP/Oba8AABwYwWUAQCeju3fdZX/T1/eGwsBJ2AAAIAA2MMODwBnoaJtqsFFfrjEyinm7zcB2gToh53ji51wkGMyMaYT43ZQvTFgTC7HrJEfEaL5DsMfZhAKvTDoxY5JkgUnl14EiQQPONwQz2BwaufA2K7Su/fIzUGWT4mQx4Qm2PiWeNnziGpECCSW1/xgsDcI5Cv9qUefDvJDBMk5qmAQZI4qrcdV5HpKLtsYplBkGBgpJrqEMBa1hUgmgnMSX04KtHelxHWUfxK6sKWlLU6IH1N8+CTs7Ckql5/09Vf4vBX5YaH65YHcbMOOZ4dtc4RrsumMNG5+YTu53q6QuBKF0S7vD5cYc8yGUPAvdkJ0/X8N6tTXVc4XtTTQx4vkMMxwACOMz+/VDdk5IOwtIbANyc132bMPY8xk5jHGaiAzl1CXDsbsEblnMKZD2Osue30WtsJ5jiKHcSAaJyJbAXkyKC1pBSPGeaJFJEXw1MpdvKyFTHRo9spMOcbSIT8aROeU7GQLWwYYoyZy12OsVowpY5Ym1mlhmwdkntNEp55ojhMZBKZbT9ya4ros6I40pfTTTZLwfZHOGqe9fziy6nmMAKMcZDRtJGcLvdESso6QnySizxMls0v29ZLrvbWN84jq/GAg9QK6/BbS+yjkqAUoNWgS9Abag1KALkEroAVIoFvQJItMd6Bj0Cd0B/QHW4ZhtKA10A6LoHXQMis/86A/0BRoEjReMIJOQXtZ66+c1VhiT6/PgCYZDJoHDbNyQL9JDWI93dJzrQsL6K3R7r249VRfg/4EfWMlEACsKCPKEuFkdwF4ATDA9mIDBAAcEAgHCM8JAJ5gdA4CHvB0MV0NOQcHomKmx9tzCCAr32WDGrRve3ntb+hHRiXF0IKC46T2quwZLCnjSHpmhgVc0DTCT9nV3eGwuC1CxJ64ARIExCT4pL/yLhnpW+M4rOeGneSN4lJWOGKSsTSIiPxYeci+/QfYlO/HVSpBbhqCKOruQUkMlH2iguun1A5QjowJoi6b5hcQwdKN3a3M3FTf0NLOUClrWgUXboi17w8AAA==) format('woff2');
	unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}
/* cyrillic */
@font-face {
	font-family: 'Roboto';
	font-style: normal;
	font-weight: 700;
	src: local('Roboto Bold'), local('Roboto-Bold'), /*savepage-url=https://fonts.gstatic.com/s/roboto/v19/KFOlCnqEu92Fr1MmWUlfABc4AMP6lbBP.woff2*/ url(data:font/woff2;base64,d09GMgABAAAAABqoAA4AAAAANygAABpTAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGmobmnocNgZgAIcMEQwKvVyuIwuCEAABNgIkA4QcBCAFgn4HIBtoLQXcGOo9DkihmxXJ/2WCdoRku+IbhelWaet2Kdfj0sxzpbLpcRAXVqxP8lR8iokVhUITtf9G3RikkWZ+CFfaEwu+fHx8saMRkszC07dW/uqumVkgPHBBBEXqYEMkAWVeZDygikMZ4U8FaH/gt9n7YDUm5scoxEZFRayYIiizkylgRa3U0zWuXBnLNi7Suk3XLtIN1mkK4EVPOCh1Cv8npCbPx1/ynn17Z2b5p3jIWwNsHSyoMQ5ZMP/ds5+LpqUQrfI6VLyJelwmqfht3CB9dwq3HWgoDlPL+x919kqW7Ajes+IS4Uiz4ZD9CZJocVpO7pahtAIOE9BW0yGMHbsVnv9fmtK5npUT2WmT1pW+6QIwhj48BQcFsK+vr/L271g15Xu2WeuUsdzk0pQmVymlFsByjFJLASSHG8CEJYCwBo0CIIstUHcPaelw44uEUFxjf0w/19e7/ZfPo0jTFCKGGMAbe6MFwRABALbAEKKBAKGfCYRRzCCMYwVhAlsIU/hAWMYPwgp0CGswIGwVA2GPdAjH1RAEA4SAEAIIEGAUKoF5j/2CAP+xgJMB+K+cxHTA/4rlsQFPAKDbJYAc/cxhgwgggKAYwBDwLhMuUmDGISSLjivLodfW3lN5HM6oXuu15kx7rp3N58q7FgkthJ7a9OfVZBQ4G4o3UXW4btVOy09UazStWX24VpKFSsLVMKf7Dff+qj8Pxps2PpM/QVIZDg9kjBv1EGZAadOhS48+A4aMGCMyYcqMOQuWrFgjs2HLDoU9BzTuPHjy4WsXfwGYguwWIkKUaLHixEuQJFWadJn2yJKNgydHngKFyp0ToydvIBiI5kqmuYhyWwCZAfYC9eMlDQ70ck3jzTLs4p3zc/E2wZ8n2IRopkSeZLKLOLfdeA7QASbABfiAEIB9yAbygUIABSqBZuAlGW5CL9MwMA4sAtvAKXBuzbubFPTFo9mSqfSbt577ikFoFkbh4BFi2KMsJA8KICo07/slFW5SL9swkzF0vLJg2Qqn+zq3zMnbpaCn3/biOQoNAwePEMMI4lmRPCiAqNAUPJehyBh26gVKQXaRk44PPsGjI8KDWCAIgiDfIAiCIIie4EEQCwTBgLoKTPdF8iXyMwoiFWGVMAyrLEWwYCUnqQajV/mZyL8LxhnPETS8WcrASrjOcZvJC4O1ptQgMNk8Pkn/1LwIiyi9BL0kjZYc7w6PTTBjGcAEuUSI0xsMXmi8ZqOcQe+MVjVvU4Ju9AFv75WnhuGv3DLVdhskJXd8uQDdtJXhjxPkOBItGiXh6ckkbLKGvEBwR3pVdI4FQsNCQ8H6rBBQY5SEAq0EqVMEHbz8OYfNc8akVUdmoqMwAgePILCWrJCXAkBUaMoQjGU2Z8GWk0iHzoU8Nc2z0748fQYrmoUROHiEGKbJghx5KSAu9g1BKqDJkDELtpz6K2Qy1RnEo55MIHNwXF6H6BlbsMm1gjSi7QO4hlacmtKYs+NoFUS16gS2JeegqMquwFCnC/QRI0NbcarYMST1LG7QG0AmypyjyAbBoJzGG5h6zLiIC6lOF36kRGSIELE5kUuDVde67MpORNYQchK3y3Wrzm+WicQoBfr/k3gsKCdX9y9RibTpltlTIT1mG6hQKUI10M6b3WjSEnC3PUx5Db7pKYllHruYexnJXUlotVYqFQ2xlLPlq4WoudDO779bGrEIG4JtUY0o4GzbjEeUic0WBlhvOlkrg+zSxy6DdBGhLxgmEfNQpLYNMmdkETHkOHE0SUDX55DTiMRWznjeIDCP+EyTAvcrycniMTnaYmIB76VoileP09VSZsFavyLrOVIu433nLlKf+aq28PY19xLRAemx0C+aae3WZrePZ/Wo8dbUsduDXBeQ3eKsnlXDV+Fz6gU1T6tNR/jf0AKer1l/DuFcH1/F9EfN6m5S+dz/kS7JadqDpdg3lpSv/DYZGdsH2uutfqBeoESvCCid8mnBM3LuP0SvnQFsPSr1NeHsoM4u5zGcUR9ADS0ZLn+6x4LXKamrT+R2xfUi70U6UOJqx71LU23w0o1Nl9OFv/TyUiuoiIKiGqDqhooBivygek5qkx5ooS3AMAdd8NqZL0XFKEz6wfTa9Rj+QDrHniOPHz/zYxjelmImSbRS8tTLniTXwvxJhi2+QGgOReE+AWF4nshCmH5JQILoEcH18VMdJqWAzYD3BSXj/AQQZ2FI5r7AWwUBYqQIEcbBAywucSJE8XBMZ30wPAla/7pEIhiBzWMYjIz4KokD1ob6yBIZUxaQMLYEACtuq/Fo6N0sTpA2NLOmjyAuNtoUPEKGUoDKsAfeQ/8vX8cYw4qv4+tM1OmI50n1ybrEAedeWQKpztfXfvS+AExoea4+ORa+CfAErUD9fsN/woEZXFCDPAbolOKr0YKFF1uKw8y/nABICGWEQIIMLEbbMY17AhKdjkw9UNw/oWhPzOIRetrDN1f+SvVo25/fv5X/uALC4/5bAJ2HGrYVKgJBpOLNL3AS11t4Lz0J9LUvBmZ+nZuTIhaxEMKMKTJBKof8r3QvjMMRlRMuGmcuXPG4FYq4m3iYVJkEWjtDkWIlSkvvgYKVqTZkAwkmGQJvXF15OQjDUNkKtm6m5iDUgdo02AZUSQMaQJ10H7AdqN8LHoQm0FAa7AEaxvBT10jbn71Ao2QiPcSIsY84cXpJkGQ/KVL0kSbNQTJMqIwhKqiQAh48saiiChE1CKSijQ4m6KJLFnqYYooZZmRjjhV2WGMNBzK2OGKHHVwoOEKDChUeTjjhBg1ncnDBBXdccSUPN9zwwB138vEgAE/o0KkikEAKYMAgECZMCgkiCAbBhFBEKOEwiSCCMiKJpJoooignmmhYtNPBbvhMeTV1FPb99k/PZggGH6abKACXABIBrsA7C4JjgBwBsg4ADgBgsMiIyS/ybcG4Ou69xtptXaxtDU6PHVcbsmrsGYp0744s0l1DkMav6PeMyXdRMCPE7QA8QBP4IAXwO/UNgmtUPjTVAxAozTl64tY7b5bXyRsApfFueEQFwU1SwjXQRKm51PLOm3wUt6BpCK9zfw8qbCIIcatWblv5Ejz4Ll0HvrjnjhvltfJmUIKjr/Ch240JfGtadmFJpCNmiaa+9QfAYhuu4Ji2uLJHsiIsS5LthksO9mvajac0vXeVUrY4hOVjRrsbB7p/FEFVqV4Rkl2p+3VifceQpg2I3bcF1+dgkRsXkZWNeVlRXLDNPsUGvRo6a1ZI5vFSG2w7hBY23lKd0bUZNlvd3V5dj30dx8reGpnqLpRysKYfZA2mhnehisGCAAn0WjuPC4swNwPpElo1h+brewrVq3oKNU1vdRqBbl3v1Evs6ZQR+X+sJ6nqhTdli6d7x5R2N9bnW3OIX5b8ApKoKDq9IkRaVQgS57rQ+bKiV1lxpU7LxPRX3adyqMoNMv7yencWjrLD5WM2ORjPzmarEx23jbxkt1UJnX3xYhiK9+ntLbjHJCvL6EV18I2KYolUvToc26ShgebFcWw9FULb8/3lZ5MO2vWaSHcUpsMZ9cn1ipnLFdV0VrS+ABtYkHPv2iGn1pI3C6ArlJblXBqxpzQ0EwmUk9VJUL0X40sYWV0tQkXfquvh8YA7WpQ51rjofKyHviLC9opQ3f1mkQPMbysKI7ENxQywvrMpCx+xwaUbq/p3xJoOjOl6IxkPhL7GNGBg1SoOrloAu4SL2kWuU1xog0342Y612Vcy82461bX+1qfRAouebcOWHupqiq2wUHH8lWZb8Yp7a+S9QyBAj6kemommOCOc1SEYdLOhrYQekWuu2t1CLe/HDJ3DNcO1AjdkfKTdsUtzRSY7wfDtby/h8jRr1iErP7eWrcD/4HyihoBi6hpQnyD1KukkthSXmFV5WtW1zCvPtYGA1xXTinUPEtPubPA0pnq2v3UrMnR4P8eMbmwkdU31AVLHSh39CubqBWi30hKVaHgfK9QQqdT+hDg+SWLwQJoXeMHJcOBpDsx3wEV0VbhqlQNMKf0RBzVDRw/jzCJclTPUt85myCqS+cAC3AWQyC8qBySCt7CAFdBQA4mtxi/6rdnU1akxfhy/Fhg8VJDFgQY5CT5jJtwvDcbf39D9dxg6fxetzmLv3YXYH6q3Hitsmef4V7o/3HnPpj/o/lzmY+hhw7WQjWYAjGRBdMXWrsh4JDpyWbgik4UmX9TsFGjA+VwppRd0big/19bFabwv2SNfOXJ2+V3Kq6GYIjw4n1RZ249Z6lk8paxgvzZcVUw3qNlyWlZaRRCtDPKal0x+sm7Ret6yimqKRJ/tD6pY3FN/dSieYLB8ZyrGoBXtHFQaTgx4NIL/dN7NpKPr8klh/J8T3NfspxVnhUJVkfMsMgyPliCf0afmSrdeKYlS+iKz1VmkBR3vPBpu4NUSsGmvGtX9ePtaZ2c/jcOodkzYL+KyWluuFOQCU48qVC8zU1zQiv5UqO7g6Di29EO/EPfbX9jsCJE1gvfcr/yrrx58xujoJ3SUkVe7RH/60TfvPvr2hnuu/5b/He449fNdpEPq2/9l8Ncv6enPJ+8/e2t2yP6j6/9Etj+lS1dm9gOE3nqAkXv69wp2Kp7DKn4CAR3rMpu2kqBMy/cOfdLlJHf47HdLSaVtolhzbWljb/cS8h/7Retvg031paLVQF3Jo9WOvX0EgvwH2wfDacAwA2KWdxYRBPlEf29/Ilx++0aBuMgnvmkqXPiH2/I/srlMSg9NjGUl5mSCt+fQxH6359pddZrzd7ZALepqmAha43ylBC0lN63G9ccxaVU/rtzt+EC52kH9WOsVrwIYK9PLfa74E9NnVI8NucBjr+VBD/zxDPzxx69jyRTWR1/2941uwornP6MfBvsOvIAVOAYI+N23HfucV/Nny5LOR6zQO4m84j0p6SVVbAQHfmMzszPEmXPbB8Hg1AWZD6ETj0/+Fan0474YySr+k4WCdtCdlzhre1ghN6TwJvc9mm4+ujP9Bmqovn6hTG+GuyMaxd33NKz61uats4swYOgvK3eLlm6770LGb5I9QPZ1hxjcKyPtbbXREtho7P57RwnJnZJMbS7LmIjMb9lLC/8rSawqn5XGlUrvVEKi41NgP4FCsFByZjF9fXYznV1CmJ0hTMDLU3JOzdw45esqs26rX9WUp+h0Yi9r+Mty1jtQ9O1Zw+Af8uLS64j3FwXigqIxRkt4kHswIzVn/1Rfznh6GncMXJ/gvq4QL48lZO+gIPi7hGhqSsmhW3/pC2gJ9qWNJtVbJF+0Sqt/COeMrFMcuEoV9E/tr+YvP/h4eQilBtCDvQOcLc3CKMtG3vg15sPuH2dv1z+JyZuMk8P6pQaGMoOoIPbxjIu1pSxNnzzPnGPa6BdpIUsGTP/N/WRVq5X8mKf/ONHk/uAz6OapaIjSaUcbULIOtt7l5Uj19U7x9Sn283lsSaZ7Uez9va2tTO8XyP0C9vLWWzCBUlAwX/9YMztKnj1Qd9/Jx83F2dvNCVwXZlpmUt12Hhg0MJnx/PQl7wevhZnfIqs12FjlF8txHid8qZ2FQquwnFRVVJTDVuSZzKRtZ0HA1JrMBlkGo+CcjaGmLHZR5Q6f/WEhpf8ffGVjc+9F8Stz7+ZEZ5uv9dQ1lYvWAkP78tuD3/Lr/2haIokhCjPVSogUNtHoauMh8H8rnyUVsyGhrzz06L+/X3Pra9MTK6vYGFDF+ngmET+/m/2SW/9r04KB4DZGL9QzNA++XzYWm0/fcBoqU5Pqy3ng2T3z1LhMNlRB4y129QHxakVBZmdomex+4kzodggIB/OJc55zJiDIP5pCSrkBrtUzjfhNhPj56YIRfBIdJv5pGnbmX3PrS9LiKznZCERUJCkksRPVpIPZId4xGjJPDSsuzB1GjO9lK5eql0PQSuCxQGLglAM9Vy1e8sTBHFefHl+ib6e9D2/igOq2VB5YC2gyK6LSY0qj/Ii51Zfep3Z8V1++EcsbTzfPMyw7XHbNYmNn34+MykslJP+6lLT8xnQmpygu79TW+SUMUM/wA26a3NTh3ox41m1g6PKNq8PdLxZGf4otN82FKQP/rGvPU7CYN1yyk5/xoE6U5nspV3DsIckdke8dUJgS8vdVv4BewB1T6O6XPHaiDyc7OImozaKWZ3sOXzyy/9DZQelZdFa+a/Lc/onxmaG3DzB3ExegHG0P9OuhNAzq1Xr4vpe1Bya3oDg3s6vn2dZewC/y57r71fa5wF3afqCOC4vemse5S0QlvL0L0sO/JCnD3bRvtF6UH6+qajnqkBmNk/wU+DtyNT+5n6unil/tXPMVpK1KNDcV1nS1XBGbvbd9V/xK76Xm+rpysVaiwiszHJyVO9xFXYh2oeUrY8gyMhvAtaiZ7RMi7WPScmcmXO/8K60+KHJt2ER+jH+2ej6+tuR7IQk3csa/RYJLSs+/IlhC/y2O/lqYm5n38P7DV2BQtfPoE+6l1CZO5fmmvJmQGZLxbnjbukzVm2y+OTI5smxUptLVo1LXUFP9PJnXMNSY3Z6Rmt0CDco1vEaopFz7a2uo0zcuN0qTq1BdHtW3MvLr//O/Px9Iz+xAXl8+1YfE2DPMg0fBGkufp6OU/6DkWWyy+13sBeFYtcBhEupe4a5POhke9x9SIWIIXkhHVQfaUbHUHhlkTccm/duV1o12Z/zXleT/fbdDPPzy1c6H6u4ZQiGjXdvTs49fjfZ5sfL9tc/RLvTs8Beu/5geRm1iXQN2BXtSPAMdGCOXJmqxYHqaglLQ4dQhAoUwBIqtvDaUgvYlb/VmxmIqyRXgfI1CQek/AyQjdmL7GCYOs2mxS9DziMgD3B0wQ4Pxa2kUtB/Td+VHcwtKSQYVwv7k/SgFPZl8kgBqBHoaHaUcAyccI5xBYAQ/ewKu8oILlhe9Lz1EH/3le2nBQvXDosb5XX9MohMXfE88FRRddXnkdDD2aKiaUNUZ7cOSiNm8kfG0XkKVED40ehKE3lPQwLRAAvMgTl/SmQQ/vU26lMeHT1/IzUg0EiJD6Vwvf5lvKKgIUAINAwmuTouGxwAFlBe+KAYvMGH5t8qaBwpg5Cksx4fNTyOzjvOOfH/0+ybdOphCus8KD3D3CGWEwOj3FbO2KASPa4xdV48rum4d9j+GZleCfzjLW4bNO3MnsXBd2giD25/3eaW+/9b2+rwMMyItYHdqoXg+YYqgtgkTg8Oat8We3f0aN//mU4lt/KOB6waFcEr/G93u6dwD/P8nTkEUilbu9MMc3a1fYogpA3Ej727dxQp8uSF3aVc0nRfuCwa88Z3/OA3HYwT6ly/ffHzanyYjuXbEtoB/MyogJLLMNhZBx4enh8fQsR0gLdDjdyNeMsDOPXMvoXhDUldA/quo4fM7HzaQORkkOD4p8JLUV/MpU3FeXCY4E9Zu/Pf59sTktzt/bNE/yO6SETm6fUQgBVHIhtCjKtxnhfs5U8O9WNDWl3yQwLruHvnJfJSC1juum20YNCXxMuBTzqDHIoNsm2qdbu1H+yVE9dIej+KRjCZ7UMAt6KaEWMvM2ftpjeegYMOkP6OjlGf2KOSbHwjuCq7Q3j9302r+xgR176WbwyOzt0aPC8ZHZNEDwrKTneIis/13+YpJct1BVHz8qVO1SD1p5545Q4AIxjHmKfLj8WyI92DP81/vJIFNYGlwXphNQpjZp5rV3Opzn5OHhrezau9XERNK2Bx2QYSfZ2t0o+HPkrR6eEZ96d0krrB9scXkrVZ6HgvMdjclDZk5Z1PPxPUEcMGzu+9fvTTCjE9hJCZ1jmtMJHaA4wrmPsYcMzOdVPvRm9hGavPi55bb8mBD/Osf9Oj16Ubzaov0dXhrjaTL9TM9hMHjon2HXeJrBqUjvyGEH6c+0doOMBOIXLZYQ3GASzjIiW8SVfzm6qt1OludWQ82aOw4+RD2Q77PD9ta50d814/6MT9hk2h9Tjup29/8jJ/1CzbfgWu4FB8urT1om5ILfs6mulA9zsCxtzZtDmkRBsz9Dbzndj3ibZu4pUFjw+RDF5zndv0muVXceJfHT6u4f/7vM6ADXTOSnIdOHzs9kDfF38fAPB1J+vYL+6X4zsOZwkmdJ28Pmebe0rdjShicbbaKPkcWK2diTXwWOLluBnV3oE06zYU37BcOqa5h+60h9Jstqd8konyQTAdknsRCuoIXRINlD+JSvTVHcU3tuZ1Qx+Wx/18oolMoQKxmIZNq4QB2VX0zKWmfPHomkGf1/x4Cc7uu7wPTcTla/1OR890sVt35HsvmYzdoh1lvoJDBpy+Aad78mnL+rXH4J1QuAHg/EpAAfBXG369P/ShF6y4uBD8GAAHv2C0A/quNTXee18bOx0NcOpvt8xzkP5w9hJ2iynLASjk/9oTOCtaTfAYlr3IweXqtDWWoMtvfAwWKa9y4+HALk9fSlLnk9wGtNScVTkadbeQoRABrn9uxJuxf5mrPFkeSo7oO4eK5l8vT7iuXwZWvuonQGnSqsAzWy+fnm3he4+WxztYv+kqSatdb3HK9H1K7K3PE0GWsvI2o9BcXCmGorFCn2c3c2Gvaal3qdM4we0qSei8KR417Gp1MpU6aZRqn0qQ7Fftr1q+E/HvfbCG918mxlFFOc9nXlTx/LENOXao2BQxTuO9lD0zrv9l6DU71/h3r8bKVnVOOXerKy1g1pzruRrZAXpVX9zFutV3zq2MzEhhX3eq4s7yHE9xrzkJDiXIS5PnXfgvcC1EjAWyfqXX8Wo6sRJH5HpJJ99ot56uA89W9YzRNFgJcckuGP0EEgAgYzBhCCODjACyifmAHWO4rhNghRhNH9rVkcLq13dehR724SBxfOMI61+D/G4CCx0qVLAWPFgtmzO9TWjztUVWQIbG/N7Z4JHFOySgxgJZrcIm47jhyzSUgpW6POIGHi2QUHWNJq7UsqONKhWXTgDTPkg17Ur028E1MNAVlcRYK03s/LyaJlWUS30hPRLIHR7JSSc2jRGyywr16Ij/eXLnzx+TOxPu4GSyPx9Y9IgE=) format('woff2');
	unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* greek-ext */
@font-face {
	font-family: 'Roboto';
	font-style: normal;
	font-weight: 700;
	src: local('Roboto Bold'), local('Roboto-Bold'), /*savepage-url=https://fonts.gstatic.com/s/roboto/v19/KFOlCnqEu92Fr1MmWUlfCBc4AMP6lbBP.woff2*/ url(data:font/woff2;base64,d09GMgABAAAAAAMgAA4AAAAABeAAAALOAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGiYbcBw2BmAAWBEMCoIYgXwLEAABNgIkAxwEIAWCfgcgG78EAJ6HsWOiKfOTTnpviRAP/9jVd19VQ6fnA1JkLOhImcMJF2BIdhu4pldi/yoFSwiKUqCURiDn1IyaMCzcNDvlCDILIfQWh1f56iK2zVtHN/mbwtMLLNDAEpzWxrNA1gbLPWyLFHhCgVGUzUvO8oLRul0fIiSqgFZ4ckIICSqoCARKhggRzphVaIDbfmK/Arf7IF2CO7D1uAFHCPW6QOzfud9AikDISBAKEi0I0Y4OX4AQcvp+oqkcxoEbcANtmANAmnkqeUWW7L3Z4RCd34P0LbsSAKeeAnRmLy6nQTov9I05NXbl7010wXsIMuOyz+mCEGBxBwIBVGImEOqCMtWg88J63Sd++aIAFdABIM+CSE5IFHTe2Q0gD6ooYhLYC6qogCwgaWEm3VHTaq7Yb3eq4fip2UK6f4NmX+yvnjr/4Zela7/+eFVc+OSbG776yLYL75eufXeF+/fK132w+lRfYU1pV9l7K1d8vB99GmWfa0x3boAo3/QiR7/Kx755JI2NV8fG1LKvn//gruntnv3Q6HnOB/fOaveMQzt/7P3BPh/7/u678r6e/cVjv997l+drbW5P499p+d2kDQOXP73J1Xz3spVZ3YarN0IvwlmQl/3/FRLUWteXPZz9QCDp3mzu6v8HNzdM+L3QtgAaf2o6CHgvlr3/z5D/3yteVTiEpygBQf5ArULx2n+G1EYWrxIAgHx6vR0ARI3slValnfWzr9Za4yHvJq2UiASokkiGEQsU7CIQmSI2wV8IFZuERME+IdXKXp4Rsnp5EHJaebxG1QpPpmO2LuzNTc0cETYWVlZEGsNMrVBE1saICRG1skLUmPZhvZE6oPZOUWMmNcwQc8TEMCvjuE+m3eLCwdzsTR3IlNq5+asau/m0Ua+h62jLx+zGz8BkEIWBUYaUMmH2pieszI1QGwfUgVlBVlxSSV2SkfoTFgKgvs8YAAA=) format('woff2');
	unicode-range: U+1F00-1FFF;
}
/* greek */
@font-face {
	font-family: 'Roboto';
	font-style: normal;
	font-weight: 700;
	src: local('Roboto Bold'), local('Roboto-Bold'), /*savepage-url=https://fonts.gstatic.com/s/roboto/v19/KFOlCnqEu92Fr1MmWUlfBxc4AMP6lbBP.woff2*/ url(data:font/woff2;base64,d09GMgABAAAAABOQAA4AAAAAI+QAABM4AAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGmQbi3YcNgZgAIUUEQwKqXSgYwuBSAABNgIkA4MMBCAFgn4HIBuWHbOipKnCH0WwcRAo87rgv0zgpgguL+hnkg5rUdBVre7Wdi2CU69aalGyDPdy370axg+hqtcpgAEew6J/yuPyeIxhhDG8HMbU2AhJZv3/11ne95++iYZIHmBpZPMgUufdINcp06XaMuWc1Iinx35Bg+7C7KQrHuiH2wM+hETHfd1onhmryaohm9YYCeNYuxmy11mDsM3Rv2A2GE8MrGoUq8D4j1GJL+to8/Y+2yTfRj0KfST/wF/xO/O2X7oSJBCWxtsiHoDFUWBZze3/df3yR3PlwmF29vLPqkCq72sR1MID8hQu13L+jGmFIiFRlZN9BFny/b+11M5k9gI8CcuUeIWr8K1wFapu+kv7bw/3rq8LJbgpBwC2ZRVQkTH2qvZadkCkSAEaUeErhK4wfYX//79v+vb/vzp3rQMSDqPnMIx/EiqQiSnAVhZQQImkboiLOkVwcM36mhXNxtSujcelGMmLz2h3XRDBw6k+RXDKu9WkVBlSxUkNJxonIU5ignhCkjdo550W+JINQDuA61xM4O1gaKHUvhqaQu77wxdHqeIzefuw4gfx6qQBBYA/ktn68cJbbwOIrmCrBEf0Lz5AuRuqd4eQiA4hPPElfiMoZY9bl9KVd02PuWvuYg+4DpvIUscBw7IZbiaSZ20uIPCOIqo8+QoUKhJUrESZCtU0dUIiopKatGnXpdeAYSNGjRk3acGSNXbFkVUlTEuAkbVQKNESxBOWSEgyDEVBfDq3NsZhoHRw/QTecqmPrIusYpffqkKpt6yJq9pb1EXHqsZbMIlSpk+rngr1VQC78u0jOo8sZb1Q0ZKq1tV0U23vaG2p6zOjDWENI7i4aCtivRRvRiKmZM9N+TgTxTk0X4htqSWrFl3Dda7uVdZnFbKs7T3dlkZvhT2MzMmjzYj1SbwNU8A5C5ciuJrEEf6J1DeiRZBuiJlOVSdRKqdibargKr0mhlDppsgyymUqqRd6a4nTWnWhom1LuBpFcWtKmqfzi/aAUNxjiVYvp78r2jmsKL5gn+kQtV6XUPfZk0+pqoAWlVGPi4eD1hnx0TxRvmR5rPJepto+pMI3Et2ga/wj18XPKe+tqu6p5lbbFi02o80Hp4N0iz6mz5z7uEwMAUq0SZTtZyJnFKDMsOHj5Cw1AOYqvwPMd2CyDsTbEj99c223A+ZMSZaLb34RNz94WX069LoyvCMcgg+G6MyWDHONa63JXcjrrcSl1LySei/ADYBJb7Witl+/qgk+KEVfX038wF0cXt8IQtnuJZblXqXpwTv2016Qs4cSpn9Xx8XHVZAuIisAEBzyFAIqKlfUcCkkLZq0y9W7THKbAS+wwAst8SJrv1RF8vTwEoiwzrUqUCwSFuIEk+uJXUZPo871gE1huTkk6nLb4UM54adiJF8PC9G5Se2GAnZM9kg7UrDJbgc5hWNj9ygFGRZWEqK0HIkAo8Oif39WykozSrs4Z+bw2bhCgpzbukApf/oMf9gZAFWPlA5+1vtmtBfqLJvsA14AOw+0AVS0DIBSsxlUKoZ+md9aWuEY/A3FV99n8mGW2JuzbrbM3smYl+bDOTMXJrv/Ou7w3zfM1tk/T8/p+WjOd1fPnvw7Cyiv5hcf/0vIxSKZnOVCDYAJ+IhsOZrkataiVd4T/MdefQr0G1Bo0JAiw1QjgkaNKTauRKkJk6ZMmzFrzjzdoiV1FhhCwiKiYuKWJSS5AKAdAAAUA3cAdwKygbuWQpO5W8gN7kl9QrN5QGgLHirHLNoZZ4I8JpmmhxlmKGCWOfqZZ55CFlhksFlaCEXmZWHYvCKo5lVhxLwuBIM30qYwat4Sis3bwnjwTrkLStgjwwT7HFDWHC6EefORoAcfpxNh0Xwq1JnPhIXg83QrGOY7YTn4vnwACR55ZYUPPqlvfhdnNKLvbiSK6HxzdX4BmAVF1mFRh6hxfwYeTBQY9YjoKg2lncNFO8qEV3tuoT8EQ7ouh1cUnXJ9MStMTxsrNCFCAKmZVYKSprF1Acy4ZxKATQiMwI9gkL2JTBjXJ7MY6Cq+zYwmRLY/anE8j935Rsu10RrdygRRIao/i739mYFGv58O5DBXCyjnYFo2C5TFLZJiqohFUm9UP5Oho4GJFt2MPS9QnfnERxUFLBGSuURcJvyklLBnFBMTtkdOzt3+OeNqvGXWm2/6LyLGfGqqu5WWmOdues+3lw0NVnaKS0tko5jtPf+pSBFLhM4OJAD9cBYxaF8/vdgAQgayQMup8bK3D7o7YaBf4oCcTUByXnb4JACh5mg5iCzmOBHuaZ1fXVjnbcY6TRKZaPkUhfmsMUv1OOCELB0EIhGxRTEwMaKaRcgMmL1dBgrYMnR5TKsJj0FWMjGDLShrvX64ddjQEbtELgNYiq9gbr7Dn0egsi3jGM7WvqUtBSJUYeI54huYrJldTiXldpLkfloKhGIxWprIc1MRDCRejmngdiyVk+lydfIncy5gm4TjoONQLtmi308/Bjxer+eSbdZKyxEXapMTUpeniRBExFwREd5JNCniKkFc1MRlVk8/2Ic2CM08Xvq8bNKBvQUyLMxEx1yXvxkrKyVK3HXhQ0q1t3hUuJ3FgDdoj6P9yVAjtQQgq+Y+pWWHrWKHPyFiEg1BIJv95t+pZ8BlTVK2IA4f1IbMN3MLEhIzEIuu0dRUxgW8DAgAeKxYm6OoHbpYtwuyj/lcIVcTC9xmL9fzguReXSuQ5jTEyZeiaS21MX9ThGcAj4V6FIb+hw+x7EW1/7l+ptjUNwKa1zPeReOoMRWsSZ3mn2KRH0EFJDAOsFvjbA7jNHoJcy8EpCDt0XJqLY+RFLdHDNweERzMvZd+Utuw673YuhVFegoSIAqast6h1zrCxx8If/aljiGkNG74L1qYJNBSKjyvqh2HyAha2A2eZmh7nQcrVv9kqgIR8bqCcaKVwYALQI57CBW5AxsL92N8+/XRCsXGRb+kfi52sU9b9P98URrwB5b61u/JBjuel07fsCauyNLMDufJz6eyc0PdCyH9Lj/3S4DRH1d4RdtOiIB1JLCIYK8ZL2EyFJEkiF8SukH21q2P+JSh34Ded+zYmFrA+lhAGbfvAUjGO97VSBCo0hL7nF/omZ1eGDU9hSxz0liM98SctsStMaSKJRn8tiqlXMCiBCAY/3fbR3cGhl4Q1dPli4apC2j3HULnnkEOpjvrPs2wssVX2/xVB9nc+Q5alnPPPh74MkcyPv/rW7QiPtoAn+E8WgMQjJoR59s14OZhNjhbMMce+smLbSs9R36VCa/A2e6PBdhBhjQJK22Hrdk5hUaD4oRMm/mMTJmZ4njDqNbABwvwaqXXrgEuTa31xo2DwGf5IqfTVm7y4nn58R4bMO/EoTnITaT8ivUK2Fpgrf9DV2f/W7Ds+Lp/h9bZ+x4sg3EAwdx6o4EfueUv6tlq3xj5FZObl56QnF+aBhEHbgNXr13FX53i94F9Z6dFdubp82dehkv/nbRlFlaKYLC3FKvZ6KS4clKEIEKP3xlc//zqiz9DAy44y7cbA9Z7eqm/N6tpzyXQwmsQ55+SeKbUQSgWRaP0pSRmnzzdmT2YnEQZALCoB1xNKaaJRL06TSCt76/v7mRu4eqc4OqS5+Yyr2vg5WRi6uGsryd/NEDujkl6gVdOTLthwm+ciSDCNiioKs4RP4UDiGegdso+1L8hzL8m3M/BKSQAyDTkNOJMcJ2Bsh2p1F112hQgZmCN7AsaA8oXOLBu17ZEfT03lzxXlwRXZwtzd6cEPX0PZ1MTLyeD+zWALfNq/dV6/U5eAurtDENVFiO0CXR8rpfwJQEjn8jCzcKBxvY33e8nLgEXt1dq71enNMiwBxGfeV94KF0//hYfcda+g98BfKyAxlXH6f93nwS6C5zwNfKX3Z0XHC8Yd37hTwexgakkagQJaaz913ei9ju8jo4cwA8MwxF1td9PDNT/ByND8GG81XJNa0sRMrgE1XxMX7u+tuFECSqoGNXYCozEjmRLkid7ZzW5UAHa8dr697IV+PJf4mqmYNcCfvZYEYMipYcHr76Pu6CrQPPFwilKZtHUxcAl4q5ItR/Y5qUQqVKgPfh0syyqhz6I7GoRg46SevKlkqO42z9glWXyDpptJ0SR5sq4nrc9RRw8f2kJuXsVg1oh3BmMapcoPmmMf5prEHnfICE7JnkU7FW/w/0zO02K6Q3RPrK/5GLp0w+rc8ePuM7gAOJkfv6j15nGJ3yiY7z9UzNivnsbV8TlkkNSUg4lERc9RhBA+T1Y1ZQ6h5/ETUm1qWoCDHyp+8XwtdhQGVvxV22aga7+Ohpe5ukL9BjWcb4MfmNLTuWH8/iZ35Uq6pejBhS6D48mxueOjqfnX4ySSDgyAiIIU49tAlSJh5xaofQ3D/9fGsXpksx9vInWBnrB5iGXb16/DYoMU1yCw1Ls9XG03ZfmXkudAWbljMG4sawy/ytkH8cAnrlNnKpfkq9r+S2iXUt6JAlmYxEmHyw1pXnqbEd3nljL+KNPyE0g3WYiUJbTdcMPVZagEgql9xbahaFwJsCOHKXxt+f91n/zUQ2m+a/z/QYwgu3Bfv9El/TkMkar1b5d+wxpLO4KBv9V/EXAhtmKNT73ubsskQDXEwGi6wL37PrkSKpfg47aEp9ppcI+vS56f8YI80WHkfR20HGAwNFqQbDtJYHxACNE2hejQ0lws2x73/il1v7X5BejVlVXmVOWUJpH1a1w6/b0iGgHmyzlyWTssEVWhHlDRgPtapMm2/3yWeiTZ2GFEt4KwCMmfBRtesdmNv9so7QROvoS4bjw0y/PAPVVjzf/CVrwx7L8jf7YQwgcd1sQB9FVMgwtJyV2jPQljUUHkwaOA1U9j8ef1m6KBITEeZFSegBNOS+zQswt1cwoFOLgWXU32KIjM5wIsXdKlH5vzuFDH+3vPfRruRI4rnj9m6jnrQrKvNmU2g8X/CSm+tGUcGbfUXJvcnRS7+mGZFpMAqUTWC76kARM2wlaWMXnfspxHcND4DTabXMeyOqZdMwVRBU8aYe9I0pLZK5gxWBGX77ivILM9AIiGo1VNoHRcpEJvne7LezszkMi9QRMzYonhkp6+CXN/7p21A6gFeZw2KvFMvX0BkhN9mCsrh4wgpsECRsZQZkJopginqCAipGgwjxuAa4stg8K+6nyUeNHQwBQvmNSKIjcGfc553ISF0gIxJ1085qmI78XoSXaVrBqL9XZaQPJWvHa8f3JqZyXe1SxbSvAtcX6fxv3eyMMf8kVxPA+iIGvqYdL4n1D89KDHx3YdsEuqEyxOEG3X0CuI2PFHbYDg8sDlOUgVM+oShD+7zxbu7g1M9nZSUZ9vcxOyQF7YlNzU/GEooNDmcq6i4x9Zn2SBgUk+qYUEFr71Nlqc5SxVEKSTtJoKmWevVelj9CaNZMgApJhsGadfnvCSVjbmOt5h0ncDe0buEmHt7cIoEkuqe3xhKyoZ0/p6vEvr5zCkxyF+EBm9R6hMbPwtJc7xa09lk1LT9asS/joT41c2CYRP79OEAlkgqiiAKyySxcVS+X/x8hn5bP989kq5gg5rmTl3Qg5vg85/3nBUg0ZJCJLk1ROmBpIKEBMMgvzMyuzMTtzMCdzMTfzMB/xriaU6XlexlnxVfr0MmBlVdnXXvBVMbhSk+M50TJhif6pspiV8+JpYlkswDLCaXGQ/DiPLpnpb8VQAjmeKTHb/6ZGSaoGzDxANb1F9TH8BuDtoXwtTj9WkM3H+h4EJX1s7mGQ5cfyHgG6OLYl9n7baORsRDZxrApDyT+S2xmfzy1MfzM8bo8XpOlvluSQ5ZNPgC6onDwGSztl9zO935r7zpI/KnksBfhPmZLU+EXsPgiG6SkfBNDfu5V3rK32fmlx1dFNDNLN32xnpB+8c80ROQt4ObZMdOgH2i4Pb91RtMxOuIGzHG0sVjK0r8f3rD5+rFcVx8ykqK/EMUUAUf6kgq69W/Wa/d0l+wgAwMP/fh0AviefXzeesx5XPqIgdgIAEFjbbgyAvU/KfKE/BuLMtTnrbPGgAsgW1niex6uoZ256kuHvky3h8QPUxlOQFWb2fu1u81VuSZnrKxF+1r8qdCw03LEvrKwP3hqJ3l5THo8zflFi7CwvNtvLmyd9F5vYmnyr2uoe9jJ79Z19+vec6I6ys0osslK/uyT5V5muRbG6oBi6/nxHa8rZOOPNFcMamu3uPaKrzPlS0rradKwDaleE8/1dfG/rOs0m123Xa7qNc1z6jmYNqXtw0+/h4X+PbFqJO+A+u7MqHGcitwHK61crYAntEKrRuAQlADcwAy8wQTugTdAeaNWmEJABozENhMEH3ueAAJvek2mERADAA4KoZkEAmx2Mgh3ACkhTicvKVMHmYCrLdLGwMlUq9zpasu/zy/WY8oBOp87cu7Bv154rqjBDaKpS9ToV4Ee2Z+53YpOO7Q49Ck9E65cibrPcduGmyi26iSO4MegV7dCOXJdtWF3oM/HWpX3oJyxU3x+R0EjmTiAyNOAQTZ5pUDexW6lbd9ZwU1Jv0526sCty7X2btp0Q46VzuiH9OnUbMambRrDVwJwlmUm2AAAAAA==) format('woff2');
	unicode-range: U+0370-03FF;
}
/* vietnamese */
@font-face {
	font-family: 'Roboto';
	font-style: normal;
	font-weight: 700;
	src: local('Roboto Bold'), local('Roboto-Bold'), /*savepage-url=https://fonts.gstatic.com/s/roboto/v19/KFOlCnqEu92Fr1MmWUlfCxc4AMP6lbBP.woff2*/ url(data:font/woff2;base64,d09GMgABAAAAAA0YAA4AAAAAHgQAAAzAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGi4bhU4cNgZgAIFMEQwKpmShKguCDgABNgIkA4QYBCAFgn4HIBtDGaOipFPOQPYXCbYhxvrRIKU0MmMQpEWKtlMkXqasjowP4Ur356c3suNC/3SMoNKF9SxIOjCS7Gfiyokhyb4+Bkw5RTgdz2/zHinxCW3mNoy1NmJUAa7BoG1sFmLFItVlOxbZxiKtFS6T562DfLObbI7lqhUqoNYwPwkCCj4cKiMIcWzeKLHt7+dKm+z7f/J2OUc5UJwrwfgDYdhVieRNNrfZ7C8zCgXCEKKwnUpA62plp76+P5y9n3Qwc86oUzmadRkn/1whmQvo4SSLmUI2VQsJCVNMBQJj2wpAOPAnTgCcC8oLs4FzSaFUDZwr04tygYMAw3+BuOKvMBeMEAhFQugkXyEQTXyIagyAhQCGAQAwvAUwdHt98iVf7V5v5n1Uju2Gjy4+fDGaEASGoTmGzFps8dkRzzlg5eHNZ6dT6b9c7IOociaGrGUMp7UpIXCKadTJFDLOclV/BWoAzZiB5qoRpLE7wwypRlo33y5VLxr7uHUhY1ZEfdFRabSRdsqwmemIGo/cfKPqEKCqSVbC9rt0Ckoxq3Drl1x+ezywB3vFfmZ1zljePMcRqc+0O4i1brL8MdkBLkBat0jJPbdDfhMjgRu4de82MAMXT8eBddCLgLJ71ePiwgNntRXh5HH2pQiOvPW5OkkTORb0P/e/9n+ve3f4T6SY37/Uf+p/83/EsAn4+v4X3i+anz2EMHXstilTwIWdafual6YzNgMxH20aiAVosbd/SfoZbeXtf5F+RVt4+9+kv9GNgXXvtnZ7OlD3HIeYB+4AxBR7gfQRKIeAujlI/4GabBHptE5Ksn2IxQVKTH/mIfOW++g6EfF0ApvBphAIo2lMqrkRgcags9kECyrBimFB51hRMEsmjcAgkPGwmWRTjMBgYqYsBpuhZZZpFSoj9XSmCqkZWsZkuhYrp5dgM7UMHBNYWjbnYCWPUFt5/zm1qe+hCOke9be+eADn63qoTV0CqOymNfemaUmVA/SmIREZI+VuexpZq8bKCHRiBVbOCraWKsOYnbZd1KYepOtNo6F+LWH6GevdnKgbUrcQcpt7nRG1SWBpO4p1j1YthcRereii4NYPMwdrlYbHwJymIYRqzN3gBzhgdJedXtjVVdHUJcBQTw9Gp2Ay3WBDbupq6hEPPOWgUFg50DKET5+iFTOeJ2GgiIgRKvMHSSNiRnSNGCtzsWcUIY/mXve0CkyLZVG1VKPYLn4qMyIqoiEMaGUZxZpVEYMn6hb29iJalcAtKlHGktFWeKxmIKjs6lpsPCTKnFBnLfT2221ll4CgolncDIkumrADLSfVAlUkoCI5rbF3Sw+i7bAXmdzxas9BdL4NJz+Ljhe6D1x73UMVovZddNS0Z7ORALJV6qNlIGaPmv6jZtCEVC0tfWjzHpt8od04mTQJdSrNDhCKLl0bqF0Yt8JNG8BYJXZ+q3lYdXeyWopoPJsoG4lEvZtVBRAopg2lnI6do1ZE6uXQWPOLB6Dy/nOBmIprR/qZ7w9y/Hmo+vN46XQgktlV4SXvaaX96/BoRgOagQdfnQMjc9I+NoN6c1szvXlBo7IPwpILAX2lQXUbP94Gov7m5/XTguCMAxD1DpDSd6h3eShn96EDI3atCIE7Ub0dEZzObE5n9lJ7nGFww3Db8g1voC/y2YavHcvXGaAP4on6U+8nmuH/T8Jwb686nJnfUFX+F0+5AuOquU7zo0b2r2W5srKRosEb+uaf/lPBTnB12MTDF/r4jYqirWtuH2rZ+e/QB7g9IDZuSmp0Sri/zUzNmntTay6/uXywG9onJLCNLwepvdYczX6ALQV+bDgE2//tm3Rhtax4vC2QYGGF6rrtp4YcuN/alOukdfz8HR75tXdAYvJuot1n6w2zYahpydN/FqhkD6ZsmZu9ZUZZ6+qgaU9k1OoykUrDUC+yQOJMBay19bF1swgWpcbGpKUGh0xOXTQ5FYLHrtP+ftXY8ciUxhxE0T/NHLrMJ+G4bYdmfbayeO2e5cWb1CrNRviWo9Kd6Dx4CZ2+YHNsW8divvSfrQ8LcG2X1vJH8PrKJPeeBQYZP9KnJLkqbSZb7Pf3BOPN4Gvq8RZivpxdffYBBP2xPvbNn0N/6m3rV/POKWe0v1tsfGwgpSpBv+fBxueS5KlRbI/5Recn98clbER3ncmv62IDx66f9ft1Q/twc8fj0WUiPUuf6FOxyqzkEEP1K56bmVHfcu0erhGVZp1+mWZzNr3izz0QQ0zxs8+tPvPh6IPLRv4v/FOlFrUfnNfZxrSfT1bjBM2hDP5Zs8Gz6BV9qH3B6QPuY40z2Qqug99qT27gn1xXfyMwJiwkODosEPjSLwXVbeKxa+Wej0fYIac3KXlVK8SjEkq9H1nZcZ3AYeAzozYKH2TNDZ9Hqjt+Uj7NNzkmlBIEP177TYmPjLIvws93cuKitETgVTUeO3QMXL/uVZ6Yu2rliTmqU/wk75jYRF9e7bFr5UeXA4d4d4P14P1ls3GiXvf0ZS/7mFuAbzrUu2zO6SD3rYc2FIu3vGAM5e/QcGt17ZraWxaDnUdk9IK7Co5fOTF/4UJRvEtIyGZS7XOdvWb2rdHPV+/rCk6c6u2kC7SzvDIyyzo4fprXhFr/MZZPR4LrV73iuM7vWKVyEhm5J651TuTmoZcfnbtu/eWByU+7JngnxCR5uf0cyVBF4L5ZerSQlNu3J0Y6xvhEkj24XtCrT1byMmM1kJfoGRtNHx6vkRU3UxHuXR8ywVoyptQmZoY6YkJ78FhriT2srdp/aH/p6dNOO7cFXAu9ttnXeddJCNU9Wrhw75E3D8OeHhrYs6TmEeDGIgAAwYaPiO8hOgLGsyqBkszwwdjHmUDIPkQ2UoKOAQR3rJ2uRLPg6Upo+J98hzPIZASC856SHIqlFrx+PeEUwrR0u+9UuGKUWrx7+zbAgROIL3EPDRwXofkPhpHnCIQnQVzcipre8BI5aT31pzOsuPcYyT8xpPAcUHZEFgiikmoXUAXD67EAoFpXwmPkqStB9z+aQOtOCOJmdIcGZK6541M5lBFaOPbrCXWIpeWk+06FK7TUcrxHim+mBnmLJywyYQ6M5Mc3M7MBIcrleV+d3WcdnGB6qgyVkTPqKMBYfUp7fsH6zYEdZ8E6ToZbjOU5ipPhFmN5juJkuMFYniM4GW4xlucoTubXaiQSFjlMKJhnL4o74S0svB7JoLrsJFaXnXhOX1Ntw1NfU9bnv7bavT6N1SeYqGaA4xZqW74LI1a/wtngLIzGdv/rydmwSljKVR9FiLDiHkkxVJedxMZlJ57V11SHCKIUtXgDbbUnOzVWH2+69NVWLreCRZyi3mSbMKasWpgl9r9F8EAApUKJghBZOXCxFGaPDtnxOe7YoykFuDFH73+edks9cxyExVCk+odE+bHlw8+OgBra2cNPb72wXYFUjg5lR9s0t/vJ583SIV2NBKkpeUnKxTkEN7olvDYSt/omnhk7MPxgg29aimso8PEKtllYNIlvxgIHmsuJbtmlUlovapuNKgmMAOcExhBsahnfV/9l1U3btiE3GklucJflKQwFcfIrc9uJZKsvu2vAihUY70Ya8DhIW564DqSVTbGGbe1a1csJKdSefkl0wJ3qeSFbK7QSfIwB26MYg//VJufN0iHDjYRpU/KSLBLiULjRTf7aSNzqm+gYO9DyMx44NvvYeAtaukPET/Baxsf+h9uMbxRuVXqtUTSK21VP4Toyy+7AGLA9ijH4mtszn2BCmqU9q4FdDDb5LBDra9yq1g+xgaso7XCz972vevjZ2oLK7r9VflTcDub+QdWZ/XepzgcCaYOFPyTd3iJm+v3eregAPviJvTXg80j5/+Xy/+ej+o8GMpJA0F65dEb3NOBsJ9rNeqznjtsiy18hZtTzkctNqo7Q0q4j6jBF9UnKj0DpdbwSI6yqhFSqqDNaTtUJqQqKIsjpTdKm2f+4quuVkmqzoN6nqF5Q3O+uva1U94Fi8P8F/1P+ROtdRVVktxZ5+BbZbSsq2ujTyTHIMWOSBIph+3NyEql9yrFAQHOnuc0lYW6sVQqbSDeav8nxBn1IQ9JoTiyZrlUWNdn6zRNRekh/m2y95wm2lrPH23lJCIQ7zGcuAVGAuSRpeySBzsXwooxwNvjMD1Nn5yedy/OzpQqLlvxqYw1OU8U7b3auE50XKk++coWU5BSKcLlx4XrK5oqUZ0DNJq01Wq5MTk5wQc5ukdLD1XiO1A2SKlRiDp7FSYo8GfIUQQ5xsitmMS24cEV9Omko4ebGl51GuPPkK2ZRz36KI8UetUg+H84nKB3kJF2+dTIbGpVykqeQvFM2ZXGIVC6N0NM7ixMtVLgEqcI5Ru3jAu+oDypZFAAAAA==) format('woff2');
	unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
}
/* latin-ext */
@font-face {
	font-family: 'Roboto';
	font-style: normal;
	font-weight: 700;
	src: local('Roboto Bold'), local('Roboto-Bold'), /*savepage-url=https://fonts.gstatic.com/s/roboto/v19/KFOlCnqEu92Fr1MmWUlfChc4AMP6lbBP.woff2*/ url(data:font/woff2;base64,d09GMgABAAAAAB70AA4AAAAAQvwAAB6eAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGkAbjgwcgTAGYACHSBEMCttYyx4Lg3oAATYCJAOHcAQgBYJ+ByAb1Tezom60WoUhipLB6Gb/dQI3ZEh9cEvERBjCDCqqUlTdsVXHXdQqEwGGJ9+p/c8J712zwgZbDlhiDkkqWR6iuv77E1mZVWPe1wPSX95JWI3RFf7TWnvzT9zT7C54Ep0ZzLRaCURIVwKlkyqJO6C53W8jS5TcEAMZOOiNaNmI6JFt1BAEg8xRPTLNGhgNkiZMwQaGTlHpsuu9SoMAeITMb3Bxa+IEch3vbWoytXt1fs0YAtJMvIdYXQfArxd8AFxMosbe6l78tymOWmCWrUUo7+9V1RXwB86fMnhnpEFKh+5wENORNpe+0SnDlC3DBDwK3wQ/v0qnIB9Iyo2uLG4V0iW0lFbKVKpbp5VaypasW5Yp06opZVjH5PaY81/t6bqG0+UuE0QwwjyMMUKZ7Xev6UQsonj8kWNY26U6a898FRo5Ge+fmEC8ANaGYtFFG6KXIcQAc4h9xRCf/kAwBOYCc2EgwBw0+jDb7zg+D4njXm7PIXHS7cEZJE7fub+EBAyQOwLEaSfcXoITBAhmBAqncYgoDOhV6V6WirAaFSpcpGhfsPlNlnXMb/As/seMiKH8Aqq8M99S92QTJErYo11ck1zsA2jOj3lYe3NXRWt6QQ/7nj8eQu/3tczC6Vzo7XkbYeO6I79JTfn5zdBXgFw+5QvB4csNpzBZClgJu28X1v2hyuXeu0utFb0+k2ROtahadNXPC2+mT9ERIpzXW5iOM/89A9ncB+0QKlaVmVtdgPkIZn9XL+V5c+Vj922i4BRdfmv/DF9Px550SnLRB/qKl88CwqQxpyFvErmB9FZT21jRegjv8/vcXG/hkPPA/4UPGwwFCRYqTLgIkaJEixErTrwEiZIkS5HqmONOOOmU0844iybNJZdly5WvULEyFarUqFWvUZNmLVq1adehU5duDD36DBgy66rrbrrtrvseeoxp3lPPPPfCS6+89saiJSvWrPvksy+++mbDJpbvfkD0GYTAGYsd78yF9WVIn8CRRUBj6wqnoCKeHZ5DTwuIQtXNGJMC83WJ4rmwX2bnF5TnZFe8PARsQdgVdicusRaEcPAe1g81/tGeztyzcUjC9gP8a7onh8n8ajvRTYh5Ppz1PNxQnHDos7PU+ifN+wgAPRUnCWg3ug+g/ZmXxZ8GkTySH3iQyAq7PfYaz5PZs38xb9e+UxOnfZpkLM8M5fSz9s7Bf8lEp7kxIVulecDSzDpb78eEsmriZfbnl9/+QJgPFXAwWD26NpdxbNAebWogZuXh3W8FH5FJ2vZpsJTgHJ6XnfaaGgU2oLBgD//gxKG8QB7yFKBu2hiwsE5FqC0FjX32nBCPw8QZYEeG2FhDABGKfXEgCKEVlmEfvhESp+J05ER+1AYjvkWWVNEq7jMhs9XMDWPJvI+dTD6mTbseoyYukjsENl8Ix/44GKqhHVbhFH4R2kKmR/fGH5TFjGS2nLl6mzJvbfycpo1GjG9AngpkKJDBUYH0y+d+/Xb6f892b90W7R/AOO/0lYXVY+t67/32+7/v1MC/v5h62+rbl3FAwPYvLtsKOl4E5d6ju94c+SvGU39NVarVoKtVp14DHboH7NdSj75pM6665robgoUIFeatd95bMGnKrCu2OeEFyuoArHE767wAp4KY72UHjgcxP7ZRIBbgVsAulWJFbGMpVsJOCMTKxrWBO0Gsy22EPSTFxtimgdjEuDXwBIhtjNsCm4HYzrgL8HEQuxp3A7YAsbtxH+BzIPY3HgV8BcTRxmOA74A41ngC8AMQJ3InxWTjySCuBH4SvwrYVlLchW0txb3YNoF42vgMsC2IL8a/5GBTvAbsxUB8YPwQOBnER8bEv0FZzLg48C1QluCWxL4nlY2wHaVyCLZTYIYxAF4BM1satwJeAjNXG28I/B/MvGF8E8DzYOYt7n3sNWkywfaTJs9h70qTj2H7Byaf4D6DHSBNPou9I02+jB0amHzV+LXAYWDyDe6b2BHSMgvuwlEQxN6eKRoqHwH+Pe7JHz37AmDuu8HwGVAfBicZ+zDf1ao30kQieSj8awioaeXfxsfsez9/LTUP5DTak7kr3mSjOBPIsUnAw+9Z4g8lMNSDmfA/HUqbicnJkH1XSo6Sn5SdBTjoVkpPq2aoi2Nfh8W9y55FFIgqjcxvpY5okBbwsHbxnvZTDDZPPhCzi0k1AiKl2nMF0oGPQEoRS1EU82EaaWIGTLiKqdqG9Nf1RsdjDq5zWdctrbRWx7IVXO24zhA57tF+nnD+PO1cjLE4dk6WFzTVtRh4BXkly2Cl4/LMEYD88GnVb6wlsaiFMTfKYJe7DLz53ZzQZZ0/bXLPHjVNDn7ozAwWCTNH6Y8zczjJUmZvylQj976+EVlSGg+o6Fi+XeS9PZpl386up2JOPNkV/fFE2lG/ofQVmuzyw6JMdqEjMFNcZR0QCbkMmg5yr5aB+hPcmNg0iz5raixCkGVVARFYzNGXWM7sYdTIOZfBUYvahFe4Rn6eBhdNkxtMs0abznF3tPYs7jnpVi9tCaVsg2czuBxdB6+A3i68zIkFzt683YuBlA4rUUKcJGZBVGnYmFdyJ4qBujavNstg2TsBIuBcytpPPNzTZHFMljrlHpm+8eisI514jq5Xclh3LOAxRyLoVPuS2vt+wzjFWNe3ruWWc93a2JsYkrfi9O2M3gV5aDihVLeYcmoBU5sXCg3NJlMkXCsvbzP4g2jD9ghk39DFGPENd26ZoujocqpJCCc2/t+ZUNsd5DXZoZNemOmC1vn5nYuxMhAaeI7/eDLGl9fdv4d2mLOoy9AwBSoNFlkYbDaHvkUSRJJ/I8sHwcvOSCpQxpvrZMpJLrNtyi1ykyQpJsYET41Pyj0XepdZY+8vw702eDPtwlXCy8DxBTuLtROSHczc5IrZW+Uu0pRY1Ma6XSM0BPxZhxloOOvzREqONUrYGk8QgZmkqqJurleh2blemKX7i+wNjAdHw0xx9rigyyMQTxpu88rwJApvN3Wx5YuWENg25+eFjV7M9eqHZ56fLszYGz5pEi3A0BgIc6NAiQyyG/uxToJoITLePhzNNNeNdQCQ2COiaWGRXOSlZXIWFN7zUoU25Y3mhO5oDxwB+wHDvCSxnucgSa50W7YLnevUh6QdWuslp+Gy+jnf/HZOHroQ1RhnhMQ6ZjaJLvGYN8qATd1qA9mJQWIfJJmJJMqDyi84VQJsH8U/u3YBcoydyMyCrQECnQmepEpwbr3PokEmiRwo9FzLm9XZduUFngVmcBQxs+QhvmRC6JXBEuSVA2GUwEXprIQ5sYCyH75ToxRvNrIKaszdwcbCbP5P+R8W/w7+5pCbUNOjOk2oBWby1/pD8d0A5jX1Q/WBrIO3iSNySEzz6oPOvyi/wuLLwed9EwIWwnI+o4K1lF4pNahJyDFH7Oi6XJWwGWTAmwVK8z0/7pKagbDqJAgoj2iB1FigxPKY59npns5U00Vso3DZmPCn1d8YOomj67xIdovxdHKUhyQIpTceo4lqwYkRHoYzS9KKdeIsdSZ73K6sdERxURtqULuXcsRlsCkn/jStJtTUJJbJSIG46VpjfvdZjF5PwDP8p6F5Ai6dqLRUwreFuhXDsyxZrg3tuMxB7kJiO6hDYwYLCkqNQrDoqgvDQFjXJqHUewD6/VpQgh0xWzCXRhpJARHwAleJxdL64KxBL7C0cf2PwhiB54omAmLxrRiANsGiVNVl5qymhIIC9x33bZ7TXimBA6hwRWlnTkyKiA9AK57ZCSjrBN3Wp/UG72QIrwAqYlEoiyBSxIiLil1PpF5sZGaIdtWx2OxsfiRFI40uMjb4cqzpDA8npeTBnusGx1YebtQ1cq9+vxA/uzMUuT1dxsUPw/1S647vIX37+3h+DNLmZQef3/+/PHDulRjHX8bxGB4tLCI8f6CiAW6JsUtOjGGAk4kcxIJt3pLgujYf7DZMpbg/LjAQbhv8q8YnzsJzX8o4daG4cB55H/0p+3n5xcxTXOeABmyM6Wnljlb9OyZ3mnQOd04CV+LE3TzGyBIyZ/1m6EMP6C+kGGY0bDwENsZ9Vp2n4SPlaOiIF+X9rMzUl9EOO2zndyC8f0KqoZyIPAl8ka8v2jKK3rsl8VFe5ERmQmpZzRIseC91rrW0mpv/EXqtBw4EwMdSYumTeBuKjaczj22An5sfwbeLx5hvEy6vIOtPlSPcqf4u1KQooJhXNNNP96EwrT/+TRwmA2eiQIXObyc3WqkeDraRIR6EFTfFXgA2hlK2dNfuQr37u8/pSndmP8wMDljAy6dNTtI/OPmC6WlFcFgYelpiItE9NCDZWWEMj8hPy49IdEXuputH5w/DUv3n0pL6dVgwf1v/o7yk9hMsQKXoVzE8sDGkUeJLj4vOXKeRSA78xlg052bc8XhqSGLS0Q3osj/lnOKhFeRB2E5fTD43/DOkopIVm3EvDR90Mjo++qiXlXm27wWFHau1xbYJ6fnNFpFjuXOXlTawESkuYNUwMTmBnxhm1YE1G2Pq6yEx9M5qjvp4Gz+Tk0k7+g/NdQtskQZeU15FUQ2GMv+T+9hRPkpT5HXiFsBw/4daGlY5hyy9SBdUEbTrRYGpb282nBlKGL4HLNMOIMJCzvaPH/Y63f4soqEDC5pZoYktNQ+HLnX8H/oGH/qWVu6OFAczPZxPQs0Tj3M3128OzkGZgo2Q8E3DCO2a0cjnfEWgaWkGBKZdoCtCFoCopMG7QSdWefZjRH5zKaze+b6MTAkgjoGhDtTg/EaZZmoeGMn/W1C8Vh2cdFAO2IGNLz4io21qGY/dKg2rpWZoxrZrxKY/Aj+RL4f2sqTqT8LyhcI3/8WR5B6+sEunI5u9Uy9XG3q+DuZOS3UJT+CPyBdHfANDwVT+16fDt3oDjvJQ2Xdln3m0FlWV8t6HaHG6OyK1POH0sThSYGz9lW+Ev1jOgqSmkHPaATeIAWdDk1thd8HV+KwuP0zpAK7T29bN+7S2P4IDdsABXY4kpypu5OJoaeHqaGTs5pjv5ggSoqSkvonrfZYmgkvaB9Iupuw62VrscjN2uyckxIL4b6ajlrmoqWfGbEuyFdDlgB3kQPwDiTM1/tLVR9YNcQ1PGO137/fJ9XqZO3nSVI7DMq7Hh+LoflyT+g9HAvFCEueJhNwrHc23kJtjuH4vWwfvY7ohf3AkMNpfe/zPalb5S1EegSWE8ksMz9yliMJylCfURYYl0XtKkhojwhMaAM9SbFZqUI2+iiMJcZxxKSwnVMLwIY1Q3QTxs3bbuV+mZ+5vzVTg9G3tnCm2RmoED9LTQxSJD44PCv8N3sp87JfSEiCMtgqzd3d00gepIlPuEeaIlO4TI8KRl057PAhsqubVzGpu2IoKPzvRNXgDmb6GG2stL9Ck/pcjCYLRTNZlhriQKWnzbsvdlo3wThK/1H2t+xGgSm/QNSXVF1L9nrw1MBR+yXCwUwnDuYn362nB6KrQGo3xt+bLemA7iGs4a1iT9fQtKaGWFiesLB6padqRSTo2FA31zfcKaDRy9Db3gnAT6IhqfAQ39tYiifzowdNN7E2x9oeMZSwwMtmQmAtn+awmm6wcVfYxfn5y4Q/Y+vRJICvcGnHXEieCowPqF9lAM3PKHc/euDobSkR+Xwe7L9hTjo+lL/RecySd0ekgxE5+J+tG1ICFpmJK9oUOrZgpe5fUFYIPuvU70e0L+Qamd61Pjs711FZtt29Ck2hl52zt4HNEHStchBl/+CQh6mTMDYS5wb5aVcmdcOFokjVN1gVvNU7WmjdQj2WqJOADJhp8INpRT9vOWEW1wSMynjfylJSPCzXcbxTVScosuzb48Y3CI7/E8Ou+dnvMC2qDn9zPwxJpB63RInqK4QkNkyXx8vY7euRgWBIdLL7PVs9ijPEi76uu/x36e17ufLX6lbCcy3dUv3ms65hELX4oShJxXXGrSquUVbyWgw1lmALNOBIOdmcn5/jll7juKY46znmJkABSrNPzek37LlZPbrTS0vc17TITD1Jq2ZeVm6qs4jLTFJse1mMcOLAxgO321TvzQ/ZGgnyfX2oc7RKVT2AylQq0GRqePIH7f428JbHtVzu6XsLT8dlbD/tsDAX5lu9oHu1kRFoHuJwxceHFkSCpeOj+jSlDX1HxNyMahWX5MC6iQKZaVwyyj927Pn13UJ+w9Y+PkGQcG2FyYapWVbajC5Pe2idEPy9b2lwFhwqCH4BI0fj0rQ4bQ0H+D2OnVGwsS2qZtUFI78Mr4wa+ouJvy4h5xR4PwIHWEH6bg1nv/Ow9yEIaOYlX3RatbBqQx4c5ukOpBvvrTvxZyyz7fLH8lWyqC0OQYUs6ViX2sZDl8xXvxMS4P2L35qshPKGWUZzQFAlV/pf+YMHmoCsfnTbzbfT5TU69mv9NOSaWDWZ3lQrIC3Jk3pvyogm/pvdJkEwk+cDU9Yf91oaCfB/GNI8ynAGLsV2U3v9Duib1/5uL2fCMhVqvzhaIoZ0MuRR0eMJmlEx8asSldtTaP04gPEbCzzMAtBJXeZfLcre/1wuCB0RaQPpHfF72No/S7iElCa5t0Je20ifrNSdrz98zsDA1NqKYGoBg44qZjVWqjaWZqa1lqp0VcCQpuZxN9vaiJbo5n07w9T+ZFO+Arw6qxit3BXRBIUXB1k7DyBi3KFVJVvQKOhUd0PEmCDSp3+PSSn3300O0XknuRZSbuWJoFb4yNinEl3v2YpXhQONOauNfNOJ3PuCZQNG2D6Fmh/jb20X45kcEYjqwrYZHsXLHDNsA/4HFn05GG0phzbLZM8YnQzx17C1MuAzh55quu7U52d1aV8fNNt/VFowSKP4UHMVrwysZl+wP6nlMQf7jeuTLG3s3vMyFamXraqrQUWU1hejs21trD5gb76ZlnV3c7U2c/f0w2XI1kugocXBHv7xHv1enR/9SUvspwjO/btNv1+GnaU5Jiqk4h9+jJh5Sd+tUbW9Ara25BRc7BAcGD0vpXDVW2f/SaY97Re3XqPtbuQbvQhSGAGyM0ucCszK9ftw47mGLPJ3QyITEiJDEqLBw0DfNuKKgoEhUyKZ37U67QV/OnCFEzUhEzKhkzbymT+5Oa6RnEBUUFRSuHVJA37KOF04rVU2LFUoreiKK+2ZqtIpdNQGNBtL1nkHUIzM5TQ+yAxUS7XAkCSbpAW45MtzSGnHpkKuHQYO37PAbmJJQK71VH38ocMQRrhJS5hN6N/B20x9rnsFkX9l8JfhP//+/1y2vXEG4tL1MmtcF9K6uPVIy5/O5pX6taFfhVHefXD8bv+WiJnR/HlEu/Kx2rY+Q2ogIkjY7wdz4jzmzzESqeyi3SCRuVHeO0dTHLGnFsSuGc5hL5ZgPcrMUoa/AoiWo9/m/UcHEIPwMXDe0hLzwPByIC5IPJfR1ZyuuZSBx76upAuwzHGiemdWfxc/eYd2xYv9ujWs2bCdlvrj0TP4XG5uVvp4++u/rsNNnYoEfVkR/ca3CcyE3fv8xfzfBJvS9q2udpaOTNQfK32oN+2mdCoqLeSJamHlDllAiY90Ns4XZNuR41KkQprm3wzVQp2WNDY2B1PwMG8/9/rMLNomRzk7xEbbWMWGubnHhMZb7Vaw29itsWKmAkMuuQmvDJIQG6e8vQaBND9Bp/UP9KSP3QkzOvszL6x1Zf2F6c1sKz72Eg0HTb/9OKes/PTY0loXLegu6xp+HP+M/Px16umeYVceatKRdT6xgDjFvt+yZDrMTtq8PXa9o2zdHbYAhJ8ZwQt/482luftilRDuqPm8ZcVm8wXDSZsNeCMzOnBYjMS+SRUQvpeeNOp3gTFxa3AbD2G49j3h15Jzb6q9vMieMkXkrOyCeUFtsMoSC+hVnGi8uppDiiUeUGdwwWQytKPx9oqr28k3K1WWccd1zX9B8s1ywImFg0Ki5oGgrEEbdDY/gEyRHuZfPdE0zEOoTe9rKealLG7GhLI8HYlP1mA59JiXefiDKmJFoj6JZ1FQ0hi3A44EAzyg8MpSJjRg7ywlBRKWhblfF+gVxqJoToYVkPG2ZhC4NB1GRR0BTERsWUHhihCRcqCwT2zCugdC0IFu0HsOl74o8CZqKjrAFerKEOHE7vo45uroXBHWI2x3zsgOGpSwWJxCPO5l6wzIiPMW+6oqWeECxBDSZoYcIcF3aAPQrT7yDiuO+W/8H9VMosmGhUN3FGSCcWyyyfgu8jeEbkvgRqUiplhnfkHLD+IDthWw5jiB+V6dsYfLwZCNT17cjbqxwtx2ADb1o1yrbxFj9oqt4MX2rqrg4bWaCIaXLS7H99GwBte34NBYAYk60xRO2LVfoiaNRU5E+fkZCquZKqKMDnrZNS9WlHaRTkVdAmQo361ZhHjDh7miwjg4qujYAOhE/SEwKxO4VpQgHB3yDiTRDpYLCGQJHlDF5pkEZW4JuBB3oRhzMbW07asc8KDyF8bycUWhtVibWZaKUL42lwqF6t9oJdSMu4tAMNwGQK6plqYh/7Wwk5k5iTB4p4sk9iRquqQlqW0Q84V8SKPISVI4HJYqqi7QVvg+2X0hdVESP2a7dAOUK+EZbiItzkHO8EtcXzX+5MVfDdntC0wtR2ZMA1FWEh7mu7MD8nlBsSffEapdITdXLYBgzDcpREEG9Il7GGVHTU8y/gyjJWAB4ZKCH66UkXI9/Co92QLHQBMKT5suSx6Ejrkj1gwYTaI0uCfsGlTKEUacjCjuS1KflEKycrffJVXNUi89CXZupr0Id4IXlDpRJl8f3JpfIE86KVuIfwYWEQ0JBpmQpZ+3EnQxeu27CJyJ94wnNjEOXybgFmTMGBtKeAj7Kna0K/LrpX0PLT39AY9BstMOH+0zLLWVnL5P6BYmtMzjgE1tc/LZ/hi0ZX5K31YOwYbC9x7birgNeQf37QGqe09h2334JeUtlWmdwOCxuiSriT4GU7BHY5BIHxaHPbHRRrgcI6sG2JWpyP/H0dzPbVcaGW61HW24gCgV1Xo4T2QuU5/Awkg6Ue9qXZALlxn4pZ4FyfqvKx6wcDuc0K1ohPnqdmcaAHg89PffBRaBuUMmp/PTZQGrUaPnuRA9KFecHzSy5h1vQltFGUtuXK4wZ5atHDyNFV4PBKVCuHzJl9qqoeY7sQbco4MLdvOwI3FV6GKFy2UFTGABP/+PGwCN7M8aGopaL7NybI16po9wvGK5J62RwwItbSMN1bDsY+9qwoZgeKPTIf+PsGsuTJWCTvBEhZ3niDLc4dbWqdn56j/1LPKridhWPCrjaSNO/0nQzAlHr7hCoj+h6uzFw16TKHc2UCMTNx3VLBwYW4blH2/dpEnOIDwc1oR0CbP8l2C8IncEBL24JOUF2d6RRum3bsRzo/z1okgLtuz2gQH31rqPH3UQS3CCI8ySkWoDuF9UiKAfbT0FrIT0CBMMug/GM67/LOHQja0djOHOV8Zq0v0t2BwL3STZmT3WtrLyQW9lW0EB/pXcAC6vDeNF4bWs6npXdtODx85Eoj59VEQyphTjK0ktBbRq2/4zHIrsQ0mKBuuEwjvHoVihcpY1BOmTIo4cy5MVHQFlugfdm7677Cui+zbn4bAD84DWhNQF/DIc//SZM57jOz5YA4UCAgOH0kRdmv3UDHjWA86cdfdTdLkgJMYJ2LI6my6WpodUzmgsooSXwac1oxVlRTXWwiUKcc92AQqA1ORm1vGTmI7PKyj10U4n9juXqJlR9jtLlQ0oj9WrBtOIZVxrJOCLXPLKiY0KLrKgimrUeEXNu+Kn+s5h6XFjNYFw+iqnStEs/7rp+uL0nVTFgqsIsunE+FQXN0oxa5AUM6VAtjXc/w7ieY9x4YKDNwhYVGXUYrSaTLvFkGt2Zek5kEZHUcliWxXw9lexqE8PWJ7G+F1qMslNdNt22491Vzt1l4UVWcmmTUkLhuueSOjJiSZMNprPAdKg2iHVGcJ9mX//e5dm60PokPHpTTPuXq1RtB+v009qSmLYGah3LoZin3j12ubTJLQh8NPv8ZGnekeVg6c8FL2Bm3RLwCebZPdlWDa3tjvh4rhyRAT6DJ90SiEDEr1zn7L0qSSc8k5/lH3PTIcSLLPSMRf09RVVNVFzSxShLIgP+QmzAYzsjTzZQRv8A4gTe+0q6Hs/8mEEkfAWiBzGxRbSjQnE8d9K0EZn1iXGDewUxhlGPEoHIQzwh9hBREn+Iqm8GQEwhriDERrJvm3QxyqzdDXYN4j3R/fOUdMY1IFk1hK8BBHwkxy422DDAPFDQutgxMJtDaKxyARwJvjXGbVMjdqeaSVT8Xs2aRVWmY4XErkKveVeZ/KuFP7qiMCFCJcJSRaCyqVjmYkyRI1FDKaIFUnaMXnxkrYOBJhQO1Weo4iWLew6izEGMADESMW985khB67XYK+piSSQIwzR6eV55ohotOt0sqGUbqARL5yaKRXJ4QykZZf5itROYMVIpixEvJBEp7O1SRXfpJIyrskJhwowNR2aUFu0kQEtHlOEEAQ==) format('woff2');
	unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
	font-family: 'Roboto';
	font-style: normal;
	font-weight: 700;
	src: local('Roboto Bold'), local('Roboto-Bold'), /*savepage-url=https://fonts.gstatic.com/s/roboto/v19/KFOlCnqEu92Fr1MmWUlfBBc4AMP6lQ.woff2*/ url(data:font/woff2;base64,d09GMgABAAAAACsMAA4AAAAAVPwAACq1AAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGmQbmh4cg3wGYACHbBEMCvBI2FULg3IAATYCJAOHYAQgBYJ+ByAbIkZFBoONA4BveJtGURo4n4qibFDWBP/XCfSAuWp36rF1yp3jvUhq0vwzcyu53Yq6aD3uEDqKokO8Sfm2/ogLbHEDIMCTfHbUvBoNYg6/NE9Nu3zA5bKcgeN81Nwf+Ln1/jaiRwjSA7QRqRGjYhtRI8YYFRsDRoVIjpCSFpQKJdKmzCRsJJUQxbw7T7lPnd+MhAizuwjuchdiKq+6TrsifYtobPNDFjDNyr+TcNFcCm2IRRWTAIfq268/er5uKyEEfmRB0AFhAs0k/p8Oa05yFQi0nZg9l+Bm/QXwz0Pffn3erq5+s8z5+QEARQBqz6o4QhaK0DG6OITp6/drX5qFegdUMkPEm9iTk0S6xBC1JUImJJfdp98Env9fq7R9+22dqe6uhRfiCnELF9I5FVBxsRGqpmpman//+QtcIf4BoB6g9Xh8FIFcoM4GGHwssDArTIyIcLGSdIQW+fm51P4U0FWdG6CdsFWbrHM/74d/jvKvbe7KuXK2EqVIdiwMoATKjZDU9JyYUBPCEKhOWDk3yM30UtbXPiISQs1ZacZOj9uuEEaYwQhXBJHXsr88a7ZchNkGsSJ24s9bxlbXZeA5jDFygH78pwQEijAAAAAHKPz2Moaw4gORLBkiUyZEtmwIDg5EgQKIE4ohOnRDmfAdAoECIAgACwGAQCRDAPBTZwyz9rJzBZkrOYYFMndMcDjIbP+4SJCBAQA7OxAA8pwZEwm8ABAACDwMFBD480KIOhhgn6KW7GQX7WJdvyZglqRwOSCEY0HuDpnYE4Swi8jISIUoj9qkxlDFVsn0SZ8MqpbVPZk1Lpm1MNm1LdnJzt3163POdbPQtWQrhaRmrAIwq+y/cAgSDG4CBInba78DDjrkKHVatOHp0ENgwIgxE2Q27Dhy5oLCHRWNF2++atSqU18FmDBpyjXX3XDTLbfdcdc9Tzz1zHPTXlqwaMmyNes2bPpkyzff/fDTLwi9bsAgkGBgHyOSXpKEEI+XOuxsEes33psptdTp5Te2+g7xBhKQsYWC26rdr/K8jvYj7zLF+FOz5TQ0xJOZmuIa17nBzfhV77zmDW95x3z8vg0LLLLEMutssMkntuLPrfvCNl/jb330/WKE1ynxah9AoRKWOXO46DWP0DKDNhn11ksMxvLIfDI8SfVu8sJ0g/sJXp+KyJI2UmYWOPUoqBmGEAlmROOLF/kTL4ggyIYVdnHjFdzU2qJ/mGGWOV7sWwo3LIFe5pXFEH9EAjK2uGUrTPH0NbBqRR318fs+WmCRJZbjH7B+8mvViNpqCSDeRCrI2OJGMe9ZYJEllteMYUMClSYvoks9D8rFpdxt/ZiNwyaf2NovF2w5xDjFaRqyzdlMYYZZ5nixX3HEtUiQsfWUbG7nxh538yKgeDPvxQKLLLEcr8e2wSaf2Hr2aCr1o/7M6zw0oVaZ/c7feBSbW96vQ2RGFwvjbzHuU4R7HLmxSbz7F6gzi/r5RuyT9qHMmnCaUtCkMz2P004KyadMbQhlvn8W6nCCAVxvnJCzW9MQs8fO0mXHF0TN39mQp50787L1+rXpnO0O9qt98vsf2O+jn+dnOsEvk995Of7wqpnbivuCGV3rB98sC3YG9P5PYu7P8p5aBSBBHXt7MPHPcyN+EIqQJ7pdxv42GPursaC2J1ZFTnjHEyz++qbl6oVhV27oisLOiwmlAz9x6Qs9v5eZp25uIkqsWxd/IfTp14gm7UxY4mGn83I36o+d8lP3AcDCNvLGFVj9MJEZ2PnyABjmYvBNnjq7Wk7+RGifwe5tP8zZDRy1hRURLog08aU1i7UZZ/uc2jfPy14k9gx2WbBDyYX5xokd/iCy1cNv6n/3lB3SoJNbDNxkHivij7GT+2OtWSvh3y/caY12NOfs3Ny9tn2b/I+m7LQld17Lg8/F1X46cx5jC1eFnWi50cayvJV3h+6jtNMR+WeXj/4T+cF6WA+CnUDNMf7+qgB8kMD9KZiuCKyiiHVG2+sgDFXa+Ogxdle2QiSNjMqxa5bnzJUCd1VGa1ZB52OPwub9apx2QMPzVTNh2VFrtrj55hcvKDoeREqkRBbZFEXZp9L2q6gDijioXNSU31EVoK6StBShrRL0VIi+8iOoDAPdzVCxjFSQsYqwUm7WyoPY2rVjAy8Y7wKDgqK5Kg93FUdTPnQV4618fFRYoeU5OpZSTmFlj5CllNOorAaVNdGMmHQTyq1nJ+q2e7g8MQ0x4wV+L6tZ0CvzuL1v3mXBMl5rV8dYtwV9wFQ+P2ZKxSmiIlpKEFVLMtQGCctdSpTDUhQ1laKlKHhF0W0JR9jAL3yyS8RFRAQEm4XnyRC2skhsghZMieaGIjxUkGeLK8IEXsJbCslGUYoUpfjqiCrVJEMVUZEQUZEoJYxjJSHcFhWnoIRPBOA5ZRkSpKBT9vjsKy7hdaKnpON32t+uQ9U6Gzer4Nn1OCsc0HZ3cR3T9nIx287wQEe77e0OMnPXwxztyyNcbKOcIHkKBrcq7TQ0Lj3IOwHUffOcsSYqAlT/ac/NQHM1I9QfDIWYb9phdVQgC9zXxEfEAB0ofCqYrIKyKXOtQHqrkC3aBKw9SGJwDHhmOKXgfbFId0Y5zMiaFlArF9YFzYa9DqhxR+th5aRbnXRaJ+pom0VSsrU4boPWHmIQIR40qEazB88apY/yfFEM8evTq0e/AYOGDBtB3zMyH/R5KWLFueCiSy674ioUhAR5oOYTcRUXaHc3LVDWkufW94X8bX3N13Yh2ADYH5gA7H2wIxFwoP8Z4C84a1QXgIM8AMid2wAcHDTo7Yode+QfN35QnoQp1wL7MMWFF5gpXZMevSbc8tiqz3YQjUKJq3ErXsWbicBNxU3HLcDJ4BRwyrh9OC0cAWeJi8P1HCeVxf7u7Pzfd8BMOE2a9eoz6bYnPvhSNIoLcY/5uN04uWPXxOl3LkH7eZl814g2BXZ2Dv92oiWMwXf+H31EaVFwN3NWcVy6VEmuize1+V/zF2cV7jl9ENdpQBBaOjoj3flMfvdMEx6fxzd/lmwzZs154aUcuV557Y233pnHkXdOfx35Cnz2xbavvin0nSAAQC01AUeFCCfUgwZHOpigiTGa6SEw6GWcPiaIYtLdlO4WyOS2e6y7J2CCp27WwhwvyOIlb5nhHfPk8J4VXrlVwOGD+2xzX0Ae2/xkjV/8Qz7/uj+z7i8oDHYq3yPkDOFEjXQ5rHJ46NooXsi48+IthK6P4ovQDVF8FboxmojQTdHE1NDN0cT00K21iflmwcZtLGEZd7CCVdzNJJPcx/qsz/1szTb8zk7szAPswR48xNmczcP9zyL0SLT2ZOjRaHJe6LFo3ENPR/3Pk3oGAoUXqnezQainKj9FEs58KVy4olZ/sFfaaAob3UlHcEFDRqIkqBZ6VapikMGh1TYIUEcVAVWIQsnL4Ci55c6OFNU7mLaE+aje9JpmooWgRyJb9VZ6D3rnZyJ6EUYxaRvGRsV2x2YOtD9TRy3R31C50K7b4OfmxDZrNxoVCgjzuHoqDnOLc+rMIhCX8mcalpS2FMqRaBaKNGeeesmP1KIxS0KKwF4rt0VmabrRBjUx7EbOKkX9Np24w+U3WWIxQz4dNnUO/Ci69rKRUHyWocLoNl4xVvFovsx1yEXOnl4vTVuCnKZUBQvumDLlQn2LPEacLWGG+R0PLAEsoutVsZlIHEuaSvXmNgT7U97aKU86/g5OqmOaq2mGYIFSkUhukCd6vxyXqEFtQnXNEA2RDhSgjdUiTkRV8PWIIQpYLkEmglIAiwC3JADCrK2yKDLJiRwM4WV5eIY0Rgix4bizOmki4+E4bVi3Exl2BqIxLQmaa1fsEi3NVQ2qQcY375HdaG5p+JeEJUgeV5TYo1DnyoFdrYOVOTSxVfuBWds2XjMmXOoDzf2w1xKNge42g1bDsHJkhlyklOYmoOsU5HCpin7YCzLMuppY8K6yTtNyr9k93exLZ4GD9gRhXVa6eE7cWMPxWuJEUoQxg6HP42TVcSXQphQzb3U7c0wCYgHkSLdqWE2V90gU1aHauI7T0CNzZdTVUqtb2X+AgXIIxA7cuZjYk7CTwhNgxbvxVHIVb2TJygK9lUyngxMa+TaKMAKcI2RwLG4HGnabboBvnExMztNYO+bkjDNHdriG9ULeLj7rzmDqjq/lvwiU5JeKUAITvWOYyI91UsG5okoz4wqHa7sbpW92cjpjHHUyLfaD/VY13fTvQIvqsH1kTyi/PJjWNdrfad2t2pCsZvKq2hD1qD2lWsGOo0afx/pCVU2I80YuyMEh2mq50l8C8Peom5UHAFZpY16sgG/bc9XxocZ1qHEvuyB3Wm4wdmgAF90rO3CU6LP0GtVTBd9TqsaKFnUjGzkSB069+uFYXZ7JwaP4XW2Gpp4bTWl+asliOS/iW6Ok2Cq5GNkH4HYJ0GhSkOxnyJnsb8LCyGs3NNBB//vvjZi2Am1l2yDnFgXenX9sVnrvc/FpR4WbGuMsZFlWJN7IRdvItDMlVZSwj2npEqupnCt+heNhZix4rLa25eZQTnYW0MQsSA/VAx9uQLtsBEwzAXTRmUkV5LMhbtBEbNOshnWmTeV/tmAtYLfjieCqdJYUzkrH40gpuc1iiBosoBm8gjrjN4hjouD6NOSoPEjU/i6iFjkwo8SaKPXULMIwM9Cr6m4pJ1chpBY0AtVleVudwzyFQOFdbFEiFJ5k0uA7NHEIoCJkfqjKPstMuNMQPhd7WyfwOtwbF6TXVzB7lZIVtXlPyU+0Mvf0YPhDmLmVVvDQsR1xxbzhtrQ0TfeDNJdAvafecZ9VWIDT0lWNfiGEQmNtm0Xn9jYCuQZbEjknMy2OrUzMpKsmGY3+IzhpGli6BGaOaemTGLw6ivIxfunmCKCCvRXZMsgOi4lHKNxzgtONpwpW4WVpWV6KXIqnYtroEM08FYm4ndZqgBh9gij72Uw+HbrWUWs4kViHC8c+JgFm3HYMnbv/3sjSgaUeEQvt5xZfuWggPNoHLAWkjv0uDxVWDAwSoqDqSMwk0w+aEG2JpDjN2IeQA3uzgRVrS2x8m2LtGkJhVGJpBdxzMzAhBRJeEQNi0oIUh4nNrWzQCmzx4YzUlgAJCjLUuaBCiWJHNUuqWJKeYkuCs3xR4UCb2jGvgxTgzF6GUK4rejacqeYKgpiMbLS2O4yChUATjiHeOCEOEnNMabG/q6/v1UdzbhE5qiB+a+ClnBBJJBfT0j7Qw+8jB7BX5FaxHrUxluHtsw6VVLbv8AtavB3oPefUzQgbG+Iq+twBQUF75dUpWGSa4shwReSw4yqO/0tB036AkrqKReYmX9PkF6CRYNDEEv23Y8RqSFxoC64YMrlnBH3vTw7kLlGzKJF9YYM7YAyTDbNdGJMX4ECYsXuzGjGr8MGKLeSU0NCZBOW14OFfMhy7mc21lmwknLNrKpBNf6422uPNcDTWw1GwOcwKYEeISEaDCxK2N7V0YrbomYRtdVzKjlgmxHrkSB45Dsdyo2SFllywwZn9UmhFJAOQIXvILGX9ca/RTyQvXtNMAyNSucWl/rquGTI7ViWCD8ZW8HwSP+jK8T0sXNJRnenGFpFvwBsvzrJGF4pItBJx4iSir5pZOvmOqnsJ63zKKDJzZVaAmJrHtTwX6q7KSBqH5/JpxkuzN5A3ADow/ng5S8ootw5kAsKx4EFHQvvrRtZf+sPFkbfVd04YZyKPy29djLHmj+pEsllv8iw0PywU5EmaRiuvdiZr0qTNbuuMYEjtlixjzTIhWcGiM5moHqFY98pc5SpYjBRIUNyglkOeaLZFgTbJIZdQzdWE1BW/zlmJrebTJhf6peiqj2cy142vQHZYlmSRZUU1Ia2Nw+eWs+l8ltwYTJaLA/HxP9Bve5IDaALRuUKd55PxQ1r3vY5eUpTXsJdq1oXLUvNxyF/iJQEEhNKMxVDgbe22Ahn8ZtqIEUsw7q/RTQwpBMdeHduNdBWsnWzlbIsmEv1YGsRiYIRAI2HTufCT2XerAEOficlm7kSYybXJVjjUsGFOHOVNS8MROUcc8p3b+sD3vC8e3FEHvFJ9d+vq/2F8cu2jZ4Z3PuKNhsrxLyiVbW2+tlIy1c3CPE8hk46UQ3UHbHvC/rqWvfcsC/aZNneh7tQU8dQF67bUg2jIfuzjITWeF+gaumC+7vtvFH7f83fPpFlN8VtyDTT8j7vQ/F46n3FihxkXB3TPzQ2Jzja9HNh9+/t1paSABIqLJ93FzsNry/eUK4Opm+919vnzS2K3G2892PX+50OlRHY6kzrsy/xey+ntKC7tbSsm5fW2Fpf0tRVNcBlakthmZGMbw+I2EXQtSGaK2Ufz+tuKSvpaSkmcc22lZb3tBRNoXaK9TkqenaEFic1tTJDvRbaqcTH0yQHrvGD2ueowU3dyLeXs7PT51idTJ/v6c6Kc/lMhxBb9lMvqf/k/ycatifH0BXpxDRNG11ewdaDahnpEshDxeFuPaDvNdlViEJlMMLZxBFF8hFzFhf3TkOq4knzmk3GhkW79yfDW62qKR5tvVkTp1uYZrmunVqBfg5/eYmbzN5NCA93aYmbrGEoJf7jl9snozdeU8AWfU496A7so1/SphBGdNHH6DfFAtRTCeRPqU74G9qjKsHRDOBwOPMrgYtSK1zK5mHnicIlV7aBiap6MJL3/DrchMmIBmv7TWtOCY8VWdDdrYj6flaWXK4k45QZ30+aUOEfmwCKGpvavyfbWb33UHkGN/w3e/NZD7cFJjwa0MUL8TqM6Aob8esKZ3u3IWVBSolGZG7Q5RSCv3pr94cegnyH6+0d6BbA5kcq56a8/pFUs/DN3kxU1n1dpQ4+B3RXUukhGhBc1zJ9OiaKRlWJSRt4E537genMtKLkzQDXlcFJ/0jWZtbVXEitjSi4pobTQOBbt1ZdTYNCyp32t3UDJwE1wdO+3iT37tIrMUTgovk9ZUt+2UNEL0Yl7soF8LE549zalJmeBmTWFTlXgpbGCqAGMosy05Gx26I9LoIuf2JK7jtq6KdUfGz84Pi/yriRl5f3xklUZE4GwT0NMh1tvYO2FdTTpHPVkdEuod0x5fGLsSbDcJayCF9ZQsbw1vY4Bs5wjTEYrXZ2hmTqUfI3n/S2+hank1OEwDbp6WCvz8q83M9P/vMJqWflGRHvTI9h+3uHRfj7MCPnXbp4hLs6egVQPT0YblQHqMdbsyJqpD7wLpWlb8zlFZfOlqULQG85iNNoKWcP2lyRs87h1ELE8sSKq2ccjqiEpkVULVIb+nd34pxGbYsHdYBqwnr6ZfsYFrrIs70nqREdsiKcQKQW08SKL9pEmgyC97tM2VBriqIyMf517Nr324IqbbbWPl5OdLYnf31BO04LmXkJz922lHH9AEOGym3+nU73unkZWtAw80Vze+EP8rmhbFtwuiCsNMlvXNONz0KEY0lNnuXqxdEICfurI9d4w/o8i/gZZBpTRPWYKe0/dfqTO4I2dPt8goCggvjSav/XrN2N2uNQkM5Y/2yLfTsPDTc7p4AvWwdj062CQqSoceNhEaWVAUnnXgImSAus8DrKAa+Avn+i9G+ERLf57o2QzulOfCcz9QviSCxBmQpDnnUcosefvFtGDgTHuac/A8eQKdl1PCHabJpE9n1WYiJ09/7+WkNQ3Vf4iTnphdeUsshC5WfKy7kR+Ol+OlDs9OanW02183+J+h8HR3nHgixt7cHLg0goyY//uwvIgGC8mmnLatp8A18Cjby1epoMSiilFlT0j3/KTnqIT7qKT337myJhcW50+8jzwVbmxRNdltMpPmY094mn5sUmnGldg0Xuld62rm0j8LfrWCFzUQZVNZkMVqzqQHWo6dTRBdSZew5cTKdPzscUfkPU5tXDPYH/34PgIIBPrO5tEz6EyeyTmXTSGo2RxMBh6vxzb7gaIgWsfkArCSVniCt1RjpTI9+2uNHq0O8dQvrfk5gQXHKjuyR4fh7q8PRWTk4fBZfHCXI2FTP+FUdneenN4Spqrs5bpY51xeRWzR2Gl9VNtTes6LBLnW3/U1TRvwiL0AgLz4Uav7Z/E3DfFs3t+orm/CiamRYWFH8uORMTB5dqFB9mqcbTtsfEu/D8kfQnWaYnPu7afMHmU1MqzqoxbWQSCi7A5Ds37NTo1Jjg0Lj55G3Sd090SabpBNPVfuUsJORf/Ca0//Y3NeZitGnQsMiYymW5HLPEtPLizvbZ0dkx+9muXeErZTPGRbVx4ojvYtY2Nj6mOXfzWAvZcAxOfD+1C7ywaauOe6lRpflbyHzTfXagPk2gjOiiODugrVg3YgyPSJmgpeFgCP6Am/PzRWVVrneZhQVd+EQz/XzgwfBn7w7Pz6dBbb6k/VvGuce8InrA/G6dWSpJfasJqYP29JMPEl3fbbgNHMEKPv1n2gHLuRAlwa5XMDwDP9gCAN7B/H/eRAwCrhorgfOkWztvKYVp676wemn2GOiugD4ul+DsEKq73PonjDWBRpyAsrqvxyYWinr8XvkCusa2dJ4XsYmWk5BPb+JyWc2f9zvkZOHXQQVTsjmm4XuNl1kuhKtCxtQKLRXHMW+XbA2EcwRgMcIPS/sC41lvvlXlxmLbQ6tBCLfY57egTRYcLJMxAfdop0AMhiUBE/PkHQWkfBfZhxP/jO/jx/vdVZEIEoQSGuQSHlLcrdAafBLM9fxYP32wIiT+gDNzAJRQTzjk7saqK+1nLaA7m6LDP4dm5T8FPfOuQyje51mOwWlj57q8UkjAoxCg6zur0TipuMPV6G8KfneTOjBUOL5dCfAPDYDykebqZfv7JnauG7uKoeT3t1lMec9NzIWC559/No3eHApIFgrklSzKerkWcSVzw0bc53h+eVBd7PCWaEMhuvf5F/X8cb0V8R2iOXsBt/YDMsIRu2F1xI6agzw9TO6rU6+1I9T6u548oATcoQZMyQVlTysydYmvjQTEzp1LKqRSQkSDED4/dGra1wK7o7c8+kSgpmFvtvrF/DYaG2rS0d4ouUcLSi3OtK8EOmsiYKlAGqWUCb1JM0Y2n9m3Rbc8Hzj14NKw8RCe6emVppMKq0qAPmeKZqhP8R4kAUpUE3rTYsus9nXeRO1eURuiOLt4phqG/lQhgtq859ffHgrrXEgIiKwj5312q05KHUTieutgWFiO+abAmvj2cGdsGFs/E/1tUnWoLiv6rBFyvBPkYYce677zdj8Fx1TBbQ/I1Q69oM/Mfg9m+nWr/rb6QHDH2X+B6pfwnkN0ytXIY9289qzkwV4cxgQ/OCYpoBtVvhzuPtGlG3lAiiPJkuFfWqZ+Gi4fwYYaxUplOv8q2Jqce/ZyqVzJ2dHIjO5ppqdMIc4fIMsuUx5V/zt/Nf+aX2BUghrZjOHtSXI1BrsqS/9L0JTnD52bq1q9dpWnqXJrEhukGfvgZwcwc6zt/G5m8qXSlu65CJ/ivMgELaL87L9QkpwkS+WHUwGyqoHhAStSS8PVBF6vbjdOUKuq/2f8U+H+OmuO1RE3360xSJii6+EpriuoAqvZ2k46s9mKS3/N5E1Ox1wMuThoMJarUiJEuXP4oupY18H/j1nrgOSCHhgaxbj8fetyHvjV9aXhpWFO3yVbWvhBJebY4RBvyWprGrFVrgZdQP3CP94PSzRo8emN68bzHeY8PT9Efa/R0m4Y0hhfhwhz6Vt/joacvebx9A8COIn350ocKm3S99gZrAjHQYRa5PLJQ7XDcoKXOwsCGYQfd6dso8S9a4qN7LbzcirzcOBlXdw68dw+LlE0+q5Nbv7N2Wf654qLkq6dKVxXf31lokbv4FmnBIm6BIc4hwZXQqNQVUgZaJlyFqUak4m2VbTpRtBlT37JZx7147+fa4+nt95OKbu6ezubu/n68i/hNsMCGN6Tfu3/mWEZ2USknP7D+5gNWU2l5z6W4rC5m6lpdXGkFu1RUMxvuTfnXFxaRi15MakL4vfG4qMgc120SEsPvjMVGsQnTlw2UA9PmtfTN7r7qx8Xi+nCSqpKafZqxmv3fze8awHEhWYzsHcxPDJCnFs43UznBl/vFbn1FFGbu48DzuM9xdqyNiVmGMyvoYgStijnSTM8LuXRu99PPv0QW7ip5pDDDIxPtLcmR5Gj/axHetU9jbA4GWrnYpO5/a+6s5m1Mc4jfD1J4N7w9ycjYlhxma5NmZ/NUS8eJRDBwIOO1i18XsHrmWtvXbHp1eiS6b3y6obWnreq0n5cLlR7EAXZqfDCH7unpFNCe01ElJ3q2tRHbWq/oWqeI7TAV665RdMZ23rvsaXMr44zLjwr3HozRSDcnpki3r3iC12RdmJ1zWkJWCiOyM6k3ROt4nBlJU71HKKwvIz+vvjTX0F/ayTfzAGVPVz7TFtwMNKhgKJpwB5N6zgLcXoCv3hl7vz//W76iOde4SNf05qez2UaP1CPbuC2qsfVTTXZm5vZncvNtfEy0ra0JdCgGfDvPUOcQ8CwPxDQ3R0Y0NK02nI5gtZ4W4cVbWuO1LW2ZRNswEhEOUfpI2yTWH5bjtqPHH7GbGL8NPxipYxUqAFudIuXqq9l79FHc2TN5lU0ZidbE2HD3ihhfEJWIdkmkqhRoHOqTfxjSlZJ98mxCeCJHo+pAKeN8emXJuZyYR8oW6tqGlir0ZKCDWAcYSOA3QOYGISrdKSGwSjfstxJBiMuCSs0PIapeVAKuV1DI67TK+2d2hu/v6iHgGoATF6283EroboXertYkmjtQuburZMojzx/v4O5gOx8yV7DBKJSASN2DPVcPNMQniQNPkH/T+DWdiakOUuOVa2caxq639mEC6BFODrRw8HPyp0c4OtBZIXAqK9eotaFCjyhHVj1TZ0/zRJyUkInt2WczG/euNtbW+tHl9vkjVBUNv7wdcWWQKWQ33mFH0dg78M+mu3DAz81NkQKmPeKpK6UPpnGaelmd7s6jOuo6lVDUXB1/FA8Ul7nVPtuyrFzLrpZs37gWpj8NOtfz/YXO5NQruYtDNymEDIMedfb4d5Jh+Izo37okTM3esAsfrNS8a1o2YRFkggcEVpthK/L4A849iW+fMV8VlYBrAPdtZD4+M/5GemIUAl1K9m9+fVqcWL11jsXstd9r6OBhX51bObhyY875jGIiftlfK9mRQjJRs3R3NqtKrPaLGHG0prpYNmKmwf69xK/CchPLB/bHLs8MNp/5de4rdOnbObnZu/hYa+PEqjBXnzyPjTgWdRuZ3ub+eOY0f2xhcrx9lqK7qt1Vku6siTZ7WiNWNWCszQciKUZ6TuYamm00VowgK13Oxz0Y8Luk/3JBnk1Pjuin6oqylfSM9/kffvA/yg+3Va+JY48n20fX+MUSK/bR5j8dHcw+y7KLPvfKwqUiOqYyk51YEhCaPNIAlpoGmFVMq6IljUIi0l2sLDxdbGw9KOvFcq58LsGuclYlzSHPblda/7tLdd3XSZpY0Rzy/NFJnH7WAXu0uNFhZmzbeE3MHucd1QcY8U1g8TV7tg1k/1EGniZllUbDllvze3lw3Kf5c/DBE/gwzi9TpxJBCeJNIaFDPLlPvLyWH9Vl1FuNlagaSO0Qj/vW7GWyYcJpJfvJ+4QKbkYJUatbL0IPfqDjCrjsa07+70Nh3YcTlbnVs8jsD2SzoUSEnZUW6paLc1Z1uEzSf2mauHaisqDymcQhNI6nnt3CYkY3DZ2KbWUyolsJA232/tEizCgZf7o/6MZ9FFw9VXaqbBG7X7wLeOKPuGcmeNOz4qhux2N9/Y/Fx7ioNgQ1qKr1BfSBTvD36Oxa331NobpvZFUQtU6+qKx6XwWHRP3X0io4NRBdVc5zcMtDnehcqfYJmwZBle2+KwdV2r1aTwt8CnfVcVj7Y6iK0S1oB2rAQ3hiWaI8uaiGF3VvgZ/3qx2SnO5WgTo5dzvuIb5BL4HzJyz7WBgrIxUJOhyalcZk5KSGg73A55LLUrniZ/syYIfrC5QrI2WXfS6Fs6z/WF6B7iLGEVNeXlHPcFVQ2U6Vy+HRlOH53V3bnZbf4Bz2t6EkNyNr4TP46q3oJSuDs691THMEk5yvkb6kSD//DIWGuKhjUVUKGZuDmf0l/fmsGjBRdvXd9IXRmeR7o4nc//lmIW+ScHDLXy4ca3xvWHVmeiYkT5mgDNpWODc+mxud5PllS/mrb98VufLh3nSNiyJw/VCeivYHtdzgXOtipbe5KCjE6Qm03775LZxhfP3e8zLP+6VcODc+2x5kAFcFM46LwbfHj5/B9cemX7YJL3XJ+7w4Nhkd1VVljYVI+n2xisP6x7V96QY0UzUvu7u0e2fv2QxjMBg41HBcnFEun4nlz9/pBvF1TR0VVY3tSHlvfWv3yfLWc6fu81YzA9Gcm646pprBqALAzsT9/x+FWlE7qY0PG2kCnKCtR5GPWqZnwD0NX/OrxUmCs26VU5CdktpdeyJzOAcJeoS2Cg6L1uox0jx0F1Ra93aVZ7gJe1qahifklCTH9ZzOP3WuBVVp9OTb+sf6S47q+1LjjpgZaQTCYLNN8wvbyK6O49BoEhOpm2GQwWpJOlnWkcTKzCKw9dlhZxL5G+IvXXFmhjofKFI/WiQWTvZwI9t7uMEbNXMtp9QTWdlJgx3ZZW0dUGvw4s2LNdFWprc83pRy2oTLPp6hSrDSDX+tQNPFIxdGEv8XWI3FRfamk0OX1r+w3NywMuc1WJ9YJw9Gfqdq2T/ZOpsbdpGl3vj92Y0lgFpvatpUSq68gd7+Lmrlm4siXq8D0de9h37ktvJhNGirnoptlMl4oNqBx+unFvWstf/A7m1TyvYXQ8hZZ6+vZorx7JriS63XztqytjBOFhuB31PKe/7n1gEZD4C4hz7uDAaCwWAoGA5GqqPbBzfRU9a3l3m5ZREp26vJ+HIjWWJsnzK3ivaSTC8scWnxLVWyP04uN3qXg5dbmi36SMaXE8kSOVsyNcqRpGXnJOMRkiVde0tmd3SafuWj09pT5AMteRZoyf1AS6YDLVl1+ru4f2sPRvYhWaJzS6ZrzmhCTjgrmmaBwGqmbpk+MGLLz23LdupbMfeHLOztPOM7GVHMIFeGuecAej7PUAeGqGSjnFrdw52IWeNu2xNl2/gNt9PfKaMH1kjtd/FYPdRR9pL6gCJOkyjsG6FwWIRSPcN5cqQm8BS7SZ4j9tTqzFOct1l293vXyqbpqAsQ7krtjqhYaxIBw9YTgP6nN504r/8oN5JOf1yBw/Odt3esNtXT3KTVwVovGLQ17SxX0N5sA3zAMKM9rzZcs62aiqhae5Y9DP3suYZJpLodiwPAZf1Hiy8H4aM+Jt+vTIziGGoXd2q1l5UZPFj7622Mlmeoz+4toij69FuJLL+jOZbxgHbCSOcSaAX8v51S7mON/N1uyAJ52Ej/cmWh/hwor6pSFcYDs3s/PEPvyU+NhOp3ok2OtLj3zaY20kFhm8YvrpZP2qObGAXyRyxXgHuAyW3sPsKqjmU+1qg/0wGpVkBhAKRvG+lPgvJU28YBcR21foa7qF/1He0mBsjZbcycFbAYAJkth04LOx4ig25i9Hh/IDm699ikvizifpf6OhyJd67bJfmTL0CsM+99n4jTySKGv/NKTwHAGz+Jbg7Ax8Xlk//U/5+p5E2JheCDAihgtHosCDD16igc45cB0Rzkr1okyLwy7lcRm0mLk0zzRnWuQW8f+TZTxGLLJB4nr+aZA7WJbPrQL9+pVBmRQZEef/q3N+MrOrHGFF0YBDkj27vtHuaOhTT9FKOTlfJTWVL97cjoINsWgKctl9UMuTctWZmBCdh3i+XTOlcRbqoyyYEm4Stus0eoxHQcw3mzB2XLlXOl3GCV3/JXaymuGeAZunXaLpL5FSo/Rw59SdUP6NUI1Quo1nJIPEY7bVhmpqhUZZ5Z/f2kIMU78nmSVaYIC0URIdGb01pUKEofPonKVIzkMJ+v5M6ODV1Vnl6M4J/tmAKE2vfkUsyuxEDTbBNt1INb72ej3NQxw1xG1gnMKYGtkHPa4I+U0XjQ61U+gzX3Wqyg7jgwGdL1jonSVWWlpx3P+E23Pzv16BScn5yoG2yDoSj2CPD/1nm1S0LdcWQbFzxIcR0S+dTfrEa5ULkLs5bNO9/JbYLyhinmWcc1y5hkDvMsFB0aOKMJfiNdlPpLapyVWF+KTwlQHgYFgDZL7SAroswLK7MI5X/m1QS7WiFVfebxXl7bkBvpWMHkVE9SiN/wWcOzCYnLGBX5jkR+ID+KwCoBU7ZDj//oFnkQUQePFdZOC94ifiMCHiusndaoaxZZh+xazblvZm7AHjJI8icXgNEvV36i/pzem73++yyW4iB3kK2Bi66asz7whEbvSsFJ7Wd89RnrH1lThGVgoOF+hfsYboEBudsdSklywKWGmfXbUFvj/jrRFCHQkJwKnAS0OQhBPioqTWTOQZMUMxqDOVz2yeeGm6Dv1QpzdYOFKGzJYjCEChMHR5M6jRbjEEWlXlmC90sWKZCaMTOwZrpM21jVBE8fLEbCLYOomTJKQCcOc8Oyd3xeCAfrPl/GYmAjvaB2uRZdBs5G6zK6jmQ+rhiHjeBoa0j0avyxuwmMmoOpiRIj1LMwBBob6djEsio7ZBasOKAMe8TBDPVDxvI70yPbAA==) format('woff2');
	unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
</style>
<script defer="" data-savepage-src="/lib/mdl/material.min.js" src=""></script>
        <style data-savepage-href="/lib/mdl/material.grey-blue.min.css">/**
 * material-design-lite - Material Design Components in CSS, JS and HTML
 * @version v1.3.0
 * @license Apache-2.0
 * @copyright 2015 Google, Inc.
 * @link https://github.com/google/material-design-lite
 */
 @charset "UTF-8";html{color:rgba(0,0,0,.87)}::-moz-selection{background:#b3d4fc;text-shadow:none}::selection{background:#b3d4fc;text-shadow:none}hr{display:block;height:1px;border:0;border-top:1px solid #ccc;margin:1em 0;padding:0}audio,canvas,iframe,img,svg,video{vertical-align:middle}fieldset{border:0;margin:0;padding:0}textarea{resize:vertical}.browserupgrade{margin:.2em 0;background:#ccc;color:#000;padding:.2em 0}.hidden{display:none!important}.visuallyhidden{border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px}.visuallyhidden.focusable:active,.visuallyhidden.focusable:focus{clip:auto;height:auto;margin:0;overflow:visible;position:static;width:auto}.invisible{visibility:hidden}.clearfix:before,.clearfix:after{content:" ";display:table}.clearfix:after{clear:both}@media print{*,*:before,*:after,*:first-letter{background:transparent!important;color:#000!important;box-shadow:none!important}a,a:visited{text-decoration:underline}a[href]:after{content:" (" attr(href)")"}abbr[title]:after{content:" (" attr(title)")"}a[href^="#"]:after,a[href^="javascript:"]:after{content:""}pre,blockquote{border:1px solid #999;page-break-inside:avoid}thead{display:table-header-group}tr,img{page-break-inside:avoid}img{max-width:100%!important}p,h2,h3{orphans:3;widows:3}h2,h3{page-break-after:avoid}}a,.mdl-accordion,.mdl-button,.mdl-card,.mdl-checkbox,.mdl-dropdown-menu,.mdl-icon-toggle,.mdl-item,.mdl-radio,.mdl-slider,.mdl-switch,.mdl-tabs__tab{-webkit-tap-highlight-color:transparent;-webkit-tap-highlight-color:rgba(255,255,255,0)}html{width:100%;height:100%;-ms-touch-action:manipulation;touch-action:manipulation}body{width:100%;min-height:100%}main{display:block}*[hidden]{display:none!important}html,body{font-family:"Helvetica","Arial",sans-serif;font-size:14px;font-weight:400;line-height:20px}h1,h2,h3,h4,h5,h6,p{padding:0}h1 small,h2 small,h3 small,h4 small,h5 small,h6 small{font-family:"Roboto","Helvetica","Arial",sans-serif;font-weight:400;line-height:1.35;letter-spacing:-.02em;opacity:.54;font-size:.6em}h1{font-size:56px;line-height:1.35;letter-spacing:-.02em;margin:24px 0}h1,h2{font-family:"Roboto","Helvetica","Arial",sans-serif;font-weight:400}h2{font-size:45px;line-height:48px}h2,h3{margin:24px 0}h3{font-size:34px;line-height:40px}h3,h4{font-family:"Roboto","Helvetica","Arial",sans-serif;font-weight:400}h4{font-size:24px;line-height:32px;-moz-osx-font-smoothing:grayscale;margin:24px 0 16px}h5{font-size:20px;font-weight:500;line-height:1;letter-spacing:.02em}h5,h6{font-family:"Roboto","Helvetica","Arial",sans-serif;margin:24px 0 16px}h6{font-size:16px;letter-spacing:.04em}h6,p{font-weight:400;line-height:24px}p{font-size:14px;letter-spacing:0;margin:0 0 16px}a{color:rgb(68,138,255);font-weight:500}blockquote{font-family:"Roboto","Helvetica","Arial",sans-serif;position:relative;font-size:24px;font-weight:300;font-style:italic;line-height:1.35;letter-spacing:.08em}blockquote:before{position:absolute;left:-.5em;content:'“'}blockquote:after{content:'”';margin-left:-.05em}mark{background-color:#f4ff81}dt{font-weight:700}address{font-size:12px;line-height:1;font-style:normal}address,ul,ol{font-weight:400;letter-spacing:0}ul,ol{font-size:14px;line-height:24px}.mdl-typography--display-4,.mdl-typography--display-4-color-contrast{font-family:"Roboto","Helvetica","Arial",sans-serif;font-size:112px;font-weight:300;line-height:1;letter-spacing:-.04em}.mdl-typography--display-4-color-contrast{opacity:.54}.mdl-typography--display-3,.mdl-typography--display-3-color-contrast{font-family:"Roboto","Helvetica","Arial",sans-serif;font-size:56px;font-weight:400;line-height:1.35;letter-spacing:-.02em}.mdl-typography--display-3-color-contrast{opacity:.54}.mdl-typography--display-2,.mdl-typography--display-2-color-contrast{font-family:"Roboto","Helvetica","Arial",sans-serif;font-size:45px;font-weight:400;line-height:48px}.mdl-typography--display-2-color-contrast{opacity:.54}.mdl-typography--display-1,.mdl-typography--display-1-color-contrast{font-family:"Roboto","Helvetica","Arial",sans-serif;font-size:34px;font-weight:400;line-height:40px}.mdl-typography--display-1-color-contrast{opacity:.54}.mdl-typography--headline,.mdl-typography--headline-color-contrast{font-family:"Roboto","Helvetica","Arial",sans-serif;font-size:24px;font-weight:400;line-height:32px;-moz-osx-font-smoothing:grayscale}.mdl-typography--headline-color-contrast{opacity:.87}.mdl-typography--title,.mdl-typography--title-color-contrast{font-family:"Roboto","Helvetica","Arial",sans-serif;font-size:20px;font-weight:500;line-height:1;letter-spacing:.02em}.mdl-typography--title-color-contrast{opacity:.87}.mdl-typography--subhead,.mdl-typography--subhead-color-contrast{font-family:"Roboto","Helvetica","Arial",sans-serif;font-size:16px;font-weight:400;line-height:24px;letter-spacing:.04em}.mdl-typography--subhead-color-contrast{opacity:.87}.mdl-typography--body-2,.mdl-typography--body-2-color-contrast{font-size:14px;font-weight:700;line-height:24px;letter-spacing:0}.mdl-typography--body-2-color-contrast{opacity:.87}.mdl-typography--body-1,.mdl-typography--body-1-color-contrast{font-size:14px;font-weight:400;line-height:24px;letter-spacing:0}.mdl-typography--body-1-color-contrast{opacity:.87}.mdl-typography--body-2-force-preferred-font,.mdl-typography--body-2-force-preferred-font-color-contrast{font-family:"Roboto","Helvetica","Arial",sans-serif;font-size:14px;font-weight:500;line-height:24px;letter-spacing:0}.mdl-typography--body-2-force-preferred-font-color-contrast{opacity:.87}.mdl-typography--body-1-force-preferred-font,.mdl-typography--body-1-force-preferred-font-color-contrast{font-family:"Roboto","Helvetica","Arial",sans-serif;font-size:14px;font-weight:400;line-height:24px;letter-spacing:0}.mdl-typography--body-1-force-preferred-font-color-contrast{opacity:.87}.mdl-typography--caption,.mdl-typography--caption-force-preferred-font{font-size:12px;font-weight:400;line-height:1;letter-spacing:0}.mdl-typography--caption-force-preferred-font{font-family:"Roboto","Helvetica","Arial",sans-serif}.mdl-typography--caption-color-contrast,.mdl-typography--caption-force-preferred-font-color-contrast{font-size:12px;font-weight:400;line-height:1;letter-spacing:0;opacity:.54}.mdl-typography--caption-force-preferred-font-color-contrast,.mdl-typography--menu{font-family:"Roboto","Helvetica","Arial",sans-serif}.mdl-typography--menu{font-size:14px;font-weight:500;line-height:1;letter-spacing:0}.mdl-typography--menu-color-contrast{opacity:.87}.mdl-typography--menu-color-contrast,.mdl-typography--button,.mdl-typography--button-color-contrast{font-family:"Roboto","Helvetica","Arial",sans-serif;font-size:14px;font-weight:500;line-height:1;letter-spacing:0}.mdl-typography--button,.mdl-typography--button-color-contrast{text-transform:uppercase}.mdl-typography--button-color-contrast{opacity:.87}.mdl-typography--text-left{text-align:left}.mdl-typography--text-right{text-align:right}.mdl-typography--text-center{text-align:center}.mdl-typography--text-justify{text-align:justify}.mdl-typography--text-nowrap{white-space:nowrap}.mdl-typography--text-lowercase{text-transform:lowercase}.mdl-typography--text-uppercase{text-transform:uppercase}.mdl-typography--text-capitalize{text-transform:capitalize}.mdl-typography--font-thin{font-weight:200!important}.mdl-typography--font-light{font-weight:300!important}.mdl-typography--font-regular{font-weight:400!important}.mdl-typography--font-medium{font-weight:500!important}.mdl-typography--font-bold{font-weight:700!important}.mdl-typography--font-black{font-weight:900!important}.material-icons{font-family:'Material Icons';font-weight:400;font-style:normal;font-size:24px;line-height:1;letter-spacing:normal;text-transform:none;display:inline-block;word-wrap:normal;-moz-font-feature-settings:'liga';font-feature-settings:'liga';-webkit-font-feature-settings:'liga';-webkit-font-smoothing:antialiased}.mdl-color-text--red{color:#f44336 !important}.mdl-color--red{background-color:#f44336 !important}.mdl-color-text--red-50{color:#ffebee !important}.mdl-color--red-50{background-color:#ffebee !important}.mdl-color-text--red-100{color:#ffcdd2 !important}.mdl-color--red-100{background-color:#ffcdd2 !important}.mdl-color-text--red-200{color:#ef9a9a !important}.mdl-color--red-200{background-color:#ef9a9a !important}.mdl-color-text--red-300{color:#e57373 !important}.mdl-color--red-300{background-color:#e57373 !important}.mdl-color-text--red-400{color:#ef5350 !important}.mdl-color--red-400{background-color:#ef5350 !important}.mdl-color-text--red-500{color:#f44336 !important}.mdl-color--red-500{background-color:#f44336 !important}.mdl-color-text--red-600{color:#e53935 !important}.mdl-color--red-600{background-color:#e53935 !important}.mdl-color-text--red-700{color:#d32f2f !important}.mdl-color--red-700{background-color:#d32f2f !important}.mdl-color-text--red-800{color:#c62828 !important}.mdl-color--red-800{background-color:#c62828 !important}.mdl-color-text--red-900{color:#b71c1c !important}.mdl-color--red-900{background-color:#b71c1c !important}.mdl-color-text--red-A100{color:#ff8a80 !important}.mdl-color--red-A100{background-color:#ff8a80 !important}.mdl-color-text--red-A200{color:#ff5252 !important}.mdl-color--red-A200{background-color:#ff5252 !important}.mdl-color-text--red-A400{color:#ff1744 !important}.mdl-color--red-A400{background-color:#ff1744 !important}.mdl-color-text--red-A700{color:#d50000 !important}.mdl-color--red-A700{background-color:#d50000 !important}.mdl-color-text--pink{color:#e91e63 !important}.mdl-color--pink{background-color:#e91e63 !important}.mdl-color-text--pink-50{color:#fce4ec !important}.mdl-color--pink-50{background-color:#fce4ec !important}.mdl-color-text--pink-100{color:#f8bbd0 !important}.mdl-color--pink-100{background-color:#f8bbd0 !important}.mdl-color-text--pink-200{color:#f48fb1 !important}.mdl-color--pink-200{background-color:#f48fb1 !important}.mdl-color-text--pink-300{color:#f06292 !important}.mdl-color--pink-300{background-color:#f06292 !important}.mdl-color-text--pink-400{color:#ec407a !important}.mdl-color--pink-400{background-color:#ec407a !important}.mdl-color-text--pink-500{color:#e91e63 !important}.mdl-color--pink-500{background-color:#e91e63 !important}.mdl-color-text--pink-600{color:#d81b60 !important}.mdl-color--pink-600{background-color:#d81b60 !important}.mdl-color-text--pink-700{color:#c2185b !important}.mdl-color--pink-700{background-color:#c2185b !important}.mdl-color-text--pink-800{color:#ad1457 !important}.mdl-color--pink-800{background-color:#ad1457 !important}.mdl-color-text--pink-900{color:#880e4f !important}.mdl-color--pink-900{background-color:#880e4f !important}.mdl-color-text--pink-A100{color:#ff80ab !important}.mdl-color--pink-A100{background-color:#ff80ab !important}.mdl-color-text--pink-A200{color:#ff4081 !important}.mdl-color--pink-A200{background-color:#ff4081 !important}.mdl-color-text--pink-A400{color:#f50057 !important}.mdl-color--pink-A400{background-color:#f50057 !important}.mdl-color-text--pink-A700{color:#c51162 !important}.mdl-color--pink-A700{background-color:#c51162 !important}.mdl-color-text--purple{color:#9c27b0 !important}.mdl-color--purple{background-color:#9c27b0 !important}.mdl-color-text--purple-50{color:#f3e5f5 !important}.mdl-color--purple-50{background-color:#f3e5f5 !important}.mdl-color-text--purple-100{color:#e1bee7 !important}.mdl-color--purple-100{background-color:#e1bee7 !important}.mdl-color-text--purple-200{color:#ce93d8 !important}.mdl-color--purple-200{background-color:#ce93d8 !important}.mdl-color-text--purple-300{color:#ba68c8 !important}.mdl-color--purple-300{background-color:#ba68c8 !important}.mdl-color-text--purple-400{color:#ab47bc !important}.mdl-color--purple-400{background-color:#ab47bc !important}.mdl-color-text--purple-500{color:#9c27b0 !important}.mdl-color--purple-500{background-color:#9c27b0 !important}.mdl-color-text--purple-600{color:#8e24aa !important}.mdl-color--purple-600{background-color:#8e24aa !important}.mdl-color-text--purple-700{color:#7b1fa2 !important}.mdl-color--purple-700{background-color:#7b1fa2 !important}.mdl-color-text--purple-800{color:#6a1b9a !important}.mdl-color--purple-800{background-color:#6a1b9a !important}.mdl-color-text--purple-900{color:#4a148c !important}.mdl-color--purple-900{background-color:#4a148c !important}.mdl-color-text--purple-A100{color:#ea80fc !important}.mdl-color--purple-A100{background-color:#ea80fc !important}.mdl-color-text--purple-A200{color:#e040fb !important}.mdl-color--purple-A200{background-color:#e040fb !important}.mdl-color-text--purple-A400{color:#d500f9 !important}.mdl-color--purple-A400{background-color:#d500f9 !important}.mdl-color-text--purple-A700{color:#a0f !important}.mdl-color--purple-A700{background-color:#a0f !important}.mdl-color-text--deep-purple{color:#673ab7 !important}.mdl-color--deep-purple{background-color:#673ab7 !important}.mdl-color-text--deep-purple-50{color:#ede7f6 !important}.mdl-color--deep-purple-50{background-color:#ede7f6 !important}.mdl-color-text--deep-purple-100{color:#d1c4e9 !important}.mdl-color--deep-purple-100{background-color:#d1c4e9 !important}.mdl-color-text--deep-purple-200{color:#b39ddb !important}.mdl-color--deep-purple-200{background-color:#b39ddb !important}.mdl-color-text--deep-purple-300{color:#9575cd !important}.mdl-color--deep-purple-300{background-color:#9575cd !important}.mdl-color-text--deep-purple-400{color:#7e57c2 !important}.mdl-color--deep-purple-400{background-color:#7e57c2 !important}.mdl-color-text--deep-purple-500{color:#673ab7 !important}.mdl-color--deep-purple-500{background-color:#673ab7 !important}.mdl-color-text--deep-purple-600{color:#5e35b1 !important}.mdl-color--deep-purple-600{background-color:#5e35b1 !important}.mdl-color-text--deep-purple-700{color:#512da8 !important}.mdl-color--deep-purple-700{background-color:#512da8 !important}.mdl-color-text--deep-purple-800{color:#4527a0 !important}.mdl-color--deep-purple-800{background-color:#4527a0 !important}.mdl-color-text--deep-purple-900{color:#311b92 !important}.mdl-color--deep-purple-900{background-color:#311b92 !important}.mdl-color-text--deep-purple-A100{color:#b388ff !important}.mdl-color--deep-purple-A100{background-color:#b388ff !important}.mdl-color-text--deep-purple-A200{color:#7c4dff !important}.mdl-color--deep-purple-A200{background-color:#7c4dff !important}.mdl-color-text--deep-purple-A400{color:#651fff !important}.mdl-color--deep-purple-A400{background-color:#651fff !important}.mdl-color-text--deep-purple-A700{color:#6200ea !important}.mdl-color--deep-purple-A700{background-color:#6200ea !important}.mdl-color-text--indigo{color:#3f51b5 !important}.mdl-color--indigo{background-color:#3f51b5 !important}.mdl-color-text--indigo-50{color:#e8eaf6 !important}.mdl-color--indigo-50{background-color:#e8eaf6 !important}.mdl-color-text--indigo-100{color:#c5cae9 !important}.mdl-color--indigo-100{background-color:#c5cae9 !important}.mdl-color-text--indigo-200{color:#9fa8da !important}.mdl-color--indigo-200{background-color:#9fa8da !important}.mdl-color-text--indigo-300{color:#7986cb !important}.mdl-color--indigo-300{background-color:#7986cb !important}.mdl-color-text--indigo-400{color:#5c6bc0 !important}.mdl-color--indigo-400{background-color:#5c6bc0 !important}.mdl-color-text--indigo-500{color:#3f51b5 !important}.mdl-color--indigo-500{background-color:#3f51b5 !important}.mdl-color-text--indigo-600{color:#3949ab !important}.mdl-color--indigo-600{background-color:#3949ab !important}.mdl-color-text--indigo-700{color:#303f9f !important}.mdl-color--indigo-700{background-color:#303f9f !important}.mdl-color-text--indigo-800{color:#283593 !important}.mdl-color--indigo-800{background-color:#283593 !important}.mdl-color-text--indigo-900{color:#1a237e !important}.mdl-color--indigo-900{background-color:#1a237e !important}.mdl-color-text--indigo-A100{color:#8c9eff !important}.mdl-color--indigo-A100{background-color:#8c9eff !important}.mdl-color-text--indigo-A200{color:#536dfe !important}.mdl-color--indigo-A200{background-color:#536dfe !important}.mdl-color-text--indigo-A400{color:#3d5afe !important}.mdl-color--indigo-A400{background-color:#3d5afe !important}.mdl-color-text--indigo-A700{color:#304ffe !important}.mdl-color--indigo-A700{background-color:#304ffe !important}.mdl-color-text--blue{color:#2196f3 !important}.mdl-color--blue{background-color:#2196f3 !important}.mdl-color-text--blue-50{color:#e3f2fd !important}.mdl-color--blue-50{background-color:#e3f2fd !important}.mdl-color-text--blue-100{color:#bbdefb !important}.mdl-color--blue-100{background-color:#bbdefb !important}.mdl-color-text--blue-200{color:#90caf9 !important}.mdl-color--blue-200{background-color:#90caf9 !important}.mdl-color-text--blue-300{color:#64b5f6 !important}.mdl-color--blue-300{background-color:#64b5f6 !important}.mdl-color-text--blue-400{color:#42a5f5 !important}.mdl-color--blue-400{background-color:#42a5f5 !important}.mdl-color-text--blue-500{color:#2196f3 !important}.mdl-color--blue-500{background-color:#2196f3 !important}.mdl-color-text--blue-600{color:#1e88e5 !important}.mdl-color--blue-600{background-color:#1e88e5 !important}.mdl-color-text--blue-700{color:#1976d2 !important}.mdl-color--blue-700{background-color:#1976d2 !important}.mdl-color-text--blue-800{color:#1565c0 !important}.mdl-color--blue-800{background-color:#1565c0 !important}.mdl-color-text--blue-900{color:#0d47a1 !important}.mdl-color--blue-900{background-color:#0d47a1 !important}.mdl-color-text--blue-A100{color:#82b1ff !important}.mdl-color--blue-A100{background-color:#82b1ff !important}.mdl-color-text--blue-A200{color:#448aff !important}.mdl-color--blue-A200{background-color:#448aff !important}.mdl-color-text--blue-A400{color:#2979ff !important}.mdl-color--blue-A400{background-color:#2979ff !important}.mdl-color-text--blue-A700{color:#2962ff !important}.mdl-color--blue-A700{background-color:#2962ff !important}.mdl-color-text--light-blue{color:#03a9f4 !important}.mdl-color--light-blue{background-color:#03a9f4 !important}.mdl-color-text--light-blue-50{color:#e1f5fe !important}.mdl-color--light-blue-50{background-color:#e1f5fe !important}.mdl-color-text--light-blue-100{color:#b3e5fc !important}.mdl-color--light-blue-100{background-color:#b3e5fc !important}.mdl-color-text--light-blue-200{color:#81d4fa !important}.mdl-color--light-blue-200{background-color:#81d4fa !important}.mdl-color-text--light-blue-300{color:#4fc3f7 !important}.mdl-color--light-blue-300{background-color:#4fc3f7 !important}.mdl-color-text--light-blue-400{color:#29b6f6 !important}.mdl-color--light-blue-400{background-color:#29b6f6 !important}.mdl-color-text--light-blue-500{color:#03a9f4 !important}.mdl-color--light-blue-500{background-color:#03a9f4 !important}.mdl-color-text--light-blue-600{color:#039be5 !important}.mdl-color--light-blue-600{background-color:#039be5 !important}.mdl-color-text--light-blue-700{color:#0288d1 !important}.mdl-color--light-blue-700{background-color:#0288d1 !important}.mdl-color-text--light-blue-800{color:#0277bd !important}.mdl-color--light-blue-800{background-color:#0277bd !important}.mdl-color-text--light-blue-900{color:#01579b !important}.mdl-color--light-blue-900{background-color:#01579b !important}.mdl-color-text--light-blue-A100{color:#80d8ff !important}.mdl-color--light-blue-A100{background-color:#80d8ff !important}.mdl-color-text--light-blue-A200{color:#40c4ff !important}.mdl-color--light-blue-A200{background-color:#40c4ff !important}.mdl-color-text--light-blue-A400{color:#00b0ff !important}.mdl-color--light-blue-A400{background-color:#00b0ff !important}.mdl-color-text--light-blue-A700{color:#0091ea !important}.mdl-color--light-blue-A700{background-color:#0091ea !important}.mdl-color-text--cyan{color:#00bcd4 !important}.mdl-color--cyan{background-color:#00bcd4 !important}.mdl-color-text--cyan-50{color:#e0f7fa !important}.mdl-color--cyan-50{background-color:#e0f7fa !important}.mdl-color-text--cyan-100{color:#b2ebf2 !important}.mdl-color--cyan-100{background-color:#b2ebf2 !important}.mdl-color-text--cyan-200{color:#80deea !important}.mdl-color--cyan-200{background-color:#80deea !important}.mdl-color-text--cyan-300{color:#4dd0e1 !important}.mdl-color--cyan-300{background-color:#4dd0e1 !important}.mdl-color-text--cyan-400{color:#26c6da !important}.mdl-color--cyan-400{background-color:#26c6da !important}.mdl-color-text--cyan-500{color:#00bcd4 !important}.mdl-color--cyan-500{background-color:#00bcd4 !important}.mdl-color-text--cyan-600{color:#00acc1 !important}.mdl-color--cyan-600{background-color:#00acc1 !important}.mdl-color-text--cyan-700{color:#0097a7 !important}.mdl-color--cyan-700{background-color:#0097a7 !important}.mdl-color-text--cyan-800{color:#00838f !important}.mdl-color--cyan-800{background-color:#00838f !important}.mdl-color-text--cyan-900{color:#006064 !important}.mdl-color--cyan-900{background-color:#006064 !important}.mdl-color-text--cyan-A100{color:#84ffff !important}.mdl-color--cyan-A100{background-color:#84ffff !important}.mdl-color-text--cyan-A200{color:#18ffff !important}.mdl-color--cyan-A200{background-color:#18ffff !important}.mdl-color-text--cyan-A400{color:#00e5ff !important}.mdl-color--cyan-A400{background-color:#00e5ff !important}.mdl-color-text--cyan-A700{color:#00b8d4 !important}.mdl-color--cyan-A700{background-color:#00b8d4 !important}.mdl-color-text--teal{color:#009688 !important}.mdl-color--teal{background-color:#009688 !important}.mdl-color-text--teal-50{color:#e0f2f1 !important}.mdl-color--teal-50{background-color:#e0f2f1 !important}.mdl-color-text--teal-100{color:#b2dfdb !important}.mdl-color--teal-100{background-color:#b2dfdb !important}.mdl-color-text--teal-200{color:#80cbc4 !important}.mdl-color--teal-200{background-color:#80cbc4 !important}.mdl-color-text--teal-300{color:#4db6ac !important}.mdl-color--teal-300{background-color:#4db6ac !important}.mdl-color-text--teal-400{color:#26a69a !important}.mdl-color--teal-400{background-color:#26a69a !important}.mdl-color-text--teal-500{color:#009688 !important}.mdl-color--teal-500{background-color:#009688 !important}.mdl-color-text--teal-600{color:#00897b !important}.mdl-color--teal-600{background-color:#00897b !important}.mdl-color-text--teal-700{color:#00796b !important}.mdl-color--teal-700{background-color:#00796b !important}.mdl-color-text--teal-800{color:#00695c !important}.mdl-color--teal-800{background-color:#00695c !important}.mdl-color-text--teal-900{color:#004d40 !important}.mdl-color--teal-900{background-color:#004d40 !important}.mdl-color-text--teal-A100{color:#a7ffeb !important}.mdl-color--teal-A100{background-color:#a7ffeb !important}.mdl-color-text--teal-A200{color:#64ffda !important}.mdl-color--teal-A200{background-color:#64ffda !important}.mdl-color-text--teal-A400{color:#1de9b6 !important}.mdl-color--teal-A400{background-color:#1de9b6 !important}.mdl-color-text--teal-A700{color:#00bfa5 !important}.mdl-color--teal-A700{background-color:#00bfa5 !important}.mdl-color-text--green{color:#4caf50 !important}.mdl-color--green{background-color:#4caf50 !important}.mdl-color-text--green-50{color:#e8f5e9 !important}.mdl-color--green-50{background-color:#e8f5e9 !important}.mdl-color-text--green-100{color:#c8e6c9 !important}.mdl-color--green-100{background-color:#c8e6c9 !important}.mdl-color-text--green-200{color:#a5d6a7 !important}.mdl-color--green-200{background-color:#a5d6a7 !important}.mdl-color-text--green-300{color:#81c784 !important}.mdl-color--green-300{background-color:#81c784 !important}.mdl-color-text--green-400{color:#66bb6a !important}.mdl-color--green-400{background-color:#66bb6a !important}.mdl-color-text--green-500{color:#4caf50 !important}.mdl-color--green-500{background-color:#4caf50 !important}.mdl-color-text--green-600{color:#43a047 !important}.mdl-color--green-600{background-color:#43a047 !important}.mdl-color-text--green-700{color:#388e3c !important}.mdl-color--green-700{background-color:#388e3c !important}.mdl-color-text--green-800{color:#2e7d32 !important}.mdl-color--green-800{background-color:#2e7d32 !important}.mdl-color-text--green-900{color:#1b5e20 !important}.mdl-color--green-900{background-color:#1b5e20 !important}.mdl-color-text--green-A100{color:#b9f6ca !important}.mdl-color--green-A100{background-color:#b9f6ca !important}.mdl-color-text--green-A200{color:#69f0ae !important}.mdl-color--green-A200{background-color:#69f0ae !important}.mdl-color-text--green-A400{color:#00e676 !important}.mdl-color--green-A400{background-color:#00e676 !important}.mdl-color-text--green-A700{color:#00c853 !important}.mdl-color--green-A700{background-color:#00c853 !important}.mdl-color-text--light-green{color:#8bc34a !important}.mdl-color--light-green{background-color:#8bc34a !important}.mdl-color-text--light-green-50{color:#f1f8e9 !important}.mdl-color--light-green-50{background-color:#f1f8e9 !important}.mdl-color-text--light-green-100{color:#dcedc8 !important}.mdl-color--light-green-100{background-color:#dcedc8 !important}.mdl-color-text--light-green-200{color:#c5e1a5 !important}.mdl-color--light-green-200{background-color:#c5e1a5 !important}.mdl-color-text--light-green-300{color:#aed581 !important}.mdl-color--light-green-300{background-color:#aed581 !important}.mdl-color-text--light-green-400{color:#9ccc65 !important}.mdl-color--light-green-400{background-color:#9ccc65 !important}.mdl-color-text--light-green-500{color:#8bc34a !important}.mdl-color--light-green-500{background-color:#8bc34a !important}.mdl-color-text--light-green-600{color:#7cb342 !important}.mdl-color--light-green-600{background-color:#7cb342 !important}.mdl-color-text--light-green-700{color:#689f38 !important}.mdl-color--light-green-700{background-color:#689f38 !important}.mdl-color-text--light-green-800{color:#558b2f !important}.mdl-color--light-green-800{background-color:#558b2f !important}.mdl-color-text--light-green-900{color:#33691e !important}.mdl-color--light-green-900{background-color:#33691e !important}.mdl-color-text--light-green-A100{color:#ccff90 !important}.mdl-color--light-green-A100{background-color:#ccff90 !important}.mdl-color-text--light-green-A200{color:#b2ff59 !important}.mdl-color--light-green-A200{background-color:#b2ff59 !important}.mdl-color-text--light-green-A400{color:#76ff03 !important}.mdl-color--light-green-A400{background-color:#76ff03 !important}.mdl-color-text--light-green-A700{color:#64dd17 !important}.mdl-color--light-green-A700{background-color:#64dd17 !important}.mdl-color-text--lime{color:#cddc39 !important}.mdl-color--lime{background-color:#cddc39 !important}.mdl-color-text--lime-50{color:#f9fbe7 !important}.mdl-color--lime-50{background-color:#f9fbe7 !important}.mdl-color-text--lime-100{color:#f0f4c3 !important}.mdl-color--lime-100{background-color:#f0f4c3 !important}.mdl-color-text--lime-200{color:#e6ee9c !important}.mdl-color--lime-200{background-color:#e6ee9c !important}.mdl-color-text--lime-300{color:#dce775 !important}.mdl-color--lime-300{background-color:#dce775 !important}.mdl-color-text--lime-400{color:#d4e157 !important}.mdl-color--lime-400{background-color:#d4e157 !important}.mdl-color-text--lime-500{color:#cddc39 !important}.mdl-color--lime-500{background-color:#cddc39 !important}.mdl-color-text--lime-600{color:#c0ca33 !important}.mdl-color--lime-600{background-color:#c0ca33 !important}.mdl-color-text--lime-700{color:#afb42b !important}.mdl-color--lime-700{background-color:#afb42b !important}.mdl-color-text--lime-800{color:#9e9d24 !important}.mdl-color--lime-800{background-color:#9e9d24 !important}.mdl-color-text--lime-900{color:#827717 !important}.mdl-color--lime-900{background-color:#827717 !important}.mdl-color-text--lime-A100{color:#f4ff81 !important}.mdl-color--lime-A100{background-color:#f4ff81 !important}.mdl-color-text--lime-A200{color:#eeff41 !important}.mdl-color--lime-A200{background-color:#eeff41 !important}.mdl-color-text--lime-A400{color:#c6ff00 !important}.mdl-color--lime-A400{background-color:#c6ff00 !important}.mdl-color-text--lime-A700{color:#aeea00 !important}.mdl-color--lime-A700{background-color:#aeea00 !important}.mdl-color-text--yellow{color:#ffeb3b !important}.mdl-color--yellow{background-color:#ffeb3b !important}.mdl-color-text--yellow-50{color:#fffde7 !important}.mdl-color--yellow-50{background-color:#fffde7 !important}.mdl-color-text--yellow-100{color:#fff9c4 !important}.mdl-color--yellow-100{background-color:#fff9c4 !important}.mdl-color-text--yellow-200{color:#fff59d !important}.mdl-color--yellow-200{background-color:#fff59d !important}.mdl-color-text--yellow-300{color:#fff176 !important}.mdl-color--yellow-300{background-color:#fff176 !important}.mdl-color-text--yellow-400{color:#ffee58 !important}.mdl-color--yellow-400{background-color:#ffee58 !important}.mdl-color-text--yellow-500{color:#ffeb3b !important}.mdl-color--yellow-500{background-color:#ffeb3b !important}.mdl-color-text--yellow-600{color:#fdd835 !important}.mdl-color--yellow-600{background-color:#fdd835 !important}.mdl-color-text--yellow-700{color:#fbc02d !important}.mdl-color--yellow-700{background-color:#fbc02d !important}.mdl-color-text--yellow-800{color:#f9a825 !important}.mdl-color--yellow-800{background-color:#f9a825 !important}.mdl-color-text--yellow-900{color:#f57f17 !important}.mdl-color--yellow-900{background-color:#f57f17 !important}.mdl-color-text--yellow-A100{color:#ffff8d !important}.mdl-color--yellow-A100{background-color:#ffff8d !important}.mdl-color-text--yellow-A200{color:#ff0 !important}.mdl-color--yellow-A200{background-color:#ff0 !important}.mdl-color-text--yellow-A400{color:#ffea00 !important}.mdl-color--yellow-A400{background-color:#ffea00 !important}.mdl-color-text--yellow-A700{color:#ffd600 !important}.mdl-color--yellow-A700{background-color:#ffd600 !important}.mdl-color-text--amber{color:#ffc107 !important}.mdl-color--amber{background-color:#ffc107 !important}.mdl-color-text--amber-50{color:#fff8e1 !important}.mdl-color--amber-50{background-color:#fff8e1 !important}.mdl-color-text--amber-100{color:#ffecb3 !important}.mdl-color--amber-100{background-color:#ffecb3 !important}.mdl-color-text--amber-200{color:#ffe082 !important}.mdl-color--amber-200{background-color:#ffe082 !important}.mdl-color-text--amber-300{color:#ffd54f !important}.mdl-color--amber-300{background-color:#ffd54f !important}.mdl-color-text--amber-400{color:#ffca28 !important}.mdl-color--amber-400{background-color:#ffca28 !important}.mdl-color-text--amber-500{color:#ffc107 !important}.mdl-color--amber-500{background-color:#ffc107 !important}.mdl-color-text--amber-600{color:#ffb300 !important}.mdl-color--amber-600{background-color:#ffb300 !important}.mdl-color-text--amber-700{color:#ffa000 !important}.mdl-color--amber-700{background-color:#ffa000 !important}.mdl-color-text--amber-800{color:#ff8f00 !important}.mdl-color--amber-800{background-color:#ff8f00 !important}.mdl-color-text--amber-900{color:#ff6f00 !important}.mdl-color--amber-900{background-color:#ff6f00 !important}.mdl-color-text--amber-A100{color:#ffe57f !important}.mdl-color--amber-A100{background-color:#ffe57f !important}.mdl-color-text--amber-A200{color:#ffd740 !important}.mdl-color--amber-A200{background-color:#ffd740 !important}.mdl-color-text--amber-A400{color:#ffc400 !important}.mdl-color--amber-A400{background-color:#ffc400 !important}.mdl-color-text--amber-A700{color:#ffab00 !important}.mdl-color--amber-A700{background-color:#ffab00 !important}.mdl-color-text--orange{color:#ff9800 !important}.mdl-color--orange{background-color:#ff9800 !important}.mdl-color-text--orange-50{color:#fff3e0 !important}.mdl-color--orange-50{background-color:#fff3e0 !important}.mdl-color-text--orange-100{color:#ffe0b2 !important}.mdl-color--orange-100{background-color:#ffe0b2 !important}.mdl-color-text--orange-200{color:#ffcc80 !important}.mdl-color--orange-200{background-color:#ffcc80 !important}.mdl-color-text--orange-300{color:#ffb74d !important}.mdl-color--orange-300{background-color:#ffb74d !important}.mdl-color-text--orange-400{color:#ffa726 !important}.mdl-color--orange-400{background-color:#ffa726 !important}.mdl-color-text--orange-500{color:#ff9800 !important}.mdl-color--orange-500{background-color:#ff9800 !important}.mdl-color-text--orange-600{color:#fb8c00 !important}.mdl-color--orange-600{background-color:#fb8c00 !important}.mdl-color-text--orange-700{color:#f57c00 !important}.mdl-color--orange-700{background-color:#f57c00 !important}.mdl-color-text--orange-800{color:#ef6c00 !important}.mdl-color--orange-800{background-color:#ef6c00 !important}.mdl-color-text--orange-900{color:#e65100 !important}.mdl-color--orange-900{background-color:#e65100 !important}.mdl-color-text--orange-A100{color:#ffd180 !important}.mdl-color--orange-A100{background-color:#ffd180 !important}.mdl-color-text--orange-A200{color:#ffab40 !important}.mdl-color--orange-A200{background-color:#ffab40 !important}.mdl-color-text--orange-A400{color:#ff9100 !important}.mdl-color--orange-A400{background-color:#ff9100 !important}.mdl-color-text--orange-A700{color:#ff6d00 !important}.mdl-color--orange-A700{background-color:#ff6d00 !important}.mdl-color-text--deep-orange{color:#ff5722 !important}.mdl-color--deep-orange{background-color:#ff5722 !important}.mdl-color-text--deep-orange-50{color:#fbe9e7 !important}.mdl-color--deep-orange-50{background-color:#fbe9e7 !important}.mdl-color-text--deep-orange-100{color:#ffccbc !important}.mdl-color--deep-orange-100{background-color:#ffccbc !important}.mdl-color-text--deep-orange-200{color:#ffab91 !important}.mdl-color--deep-orange-200{background-color:#ffab91 !important}.mdl-color-text--deep-orange-300{color:#ff8a65 !important}.mdl-color--deep-orange-300{background-color:#ff8a65 !important}.mdl-color-text--deep-orange-400{color:#ff7043 !important}.mdl-color--deep-orange-400{background-color:#ff7043 !important}.mdl-color-text--deep-orange-500{color:#ff5722 !important}.mdl-color--deep-orange-500{background-color:#ff5722 !important}.mdl-color-text--deep-orange-600{color:#f4511e !important}.mdl-color--deep-orange-600{background-color:#f4511e !important}.mdl-color-text--deep-orange-700{color:#e64a19 !important}.mdl-color--deep-orange-700{background-color:#e64a19 !important}.mdl-color-text--deep-orange-800{color:#d84315 !important}.mdl-color--deep-orange-800{background-color:#d84315 !important}.mdl-color-text--deep-orange-900{color:#bf360c !important}.mdl-color--deep-orange-900{background-color:#bf360c !important}.mdl-color-text--deep-orange-A100{color:#ff9e80 !important}.mdl-color--deep-orange-A100{background-color:#ff9e80 !important}.mdl-color-text--deep-orange-A200{color:#ff6e40 !important}.mdl-color--deep-orange-A200{background-color:#ff6e40 !important}.mdl-color-text--deep-orange-A400{color:#ff3d00 !important}.mdl-color--deep-orange-A400{background-color:#ff3d00 !important}.mdl-color-text--deep-orange-A700{color:#dd2c00 !important}.mdl-color--deep-orange-A700{background-color:#dd2c00 !important}.mdl-color-text--brown{color:#795548 !important}.mdl-color--brown{background-color:#795548 !important}.mdl-color-text--brown-50{color:#efebe9 !important}.mdl-color--brown-50{background-color:#efebe9 !important}.mdl-color-text--brown-100{color:#d7ccc8 !important}.mdl-color--brown-100{background-color:#d7ccc8 !important}.mdl-color-text--brown-200{color:#bcaaa4 !important}.mdl-color--brown-200{background-color:#bcaaa4 !important}.mdl-color-text--brown-300{color:#a1887f !important}.mdl-color--brown-300{background-color:#a1887f !important}.mdl-color-text--brown-400{color:#8d6e63 !important}.mdl-color--brown-400{background-color:#8d6e63 !important}.mdl-color-text--brown-500{color:#795548 !important}.mdl-color--brown-500{background-color:#795548 !important}.mdl-color-text--brown-600{color:#6d4c41 !important}.mdl-color--brown-600{background-color:#6d4c41 !important}.mdl-color-text--brown-700{color:#5d4037 !important}.mdl-color--brown-700{background-color:#5d4037 !important}.mdl-color-text--brown-800{color:#4e342e !important}.mdl-color--brown-800{background-color:#4e342e !important}.mdl-color-text--brown-900{color:#3e2723 !important}.mdl-color--brown-900{background-color:#3e2723 !important}.mdl-color-text--grey{color:#9e9e9e !important}.mdl-color--grey{background-color:#9e9e9e !important}.mdl-color-text--grey-50{color:#fafafa !important}.mdl-color--grey-50{background-color:#fafafa !important}.mdl-color-text--grey-100{color:#f5f5f5 !important}.mdl-color--grey-100{background-color:#f5f5f5 !important}.mdl-color-text--grey-200{color:#eee !important}.mdl-color--grey-200{background-color:#eee !important}.mdl-color-text--grey-300{color:#e0e0e0 !important}.mdl-color--grey-300{background-color:#e0e0e0 !important}.mdl-color-text--grey-400{color:#bdbdbd !important}.mdl-color--grey-400{background-color:#bdbdbd !important}.mdl-color-text--grey-500{color:#9e9e9e !important}.mdl-color--grey-500{background-color:#9e9e9e !important}.mdl-color-text--grey-600{color:#757575 !important}.mdl-color--grey-600{background-color:#757575 !important}.mdl-color-text--grey-700{color:#616161 !important}.mdl-color--grey-700{background-color:#616161 !important}.mdl-color-text--grey-800{color:#424242 !important}.mdl-color--grey-800{background-color:#424242 !important}.mdl-color-text--grey-900{color:#212121 !important}.mdl-color--grey-900{background-color:#212121 !important}.mdl-color-text--blue-grey{color:#607d8b !important}.mdl-color--blue-grey{background-color:#607d8b !important}.mdl-color-text--blue-grey-50{color:#eceff1 !important}.mdl-color--blue-grey-50{background-color:#eceff1 !important}.mdl-color-text--blue-grey-100{color:#cfd8dc !important}.mdl-color--blue-grey-100{background-color:#cfd8dc !important}.mdl-color-text--blue-grey-200{color:#b0bec5 !important}.mdl-color--blue-grey-200{background-color:#b0bec5 !important}.mdl-color-text--blue-grey-300{color:#90a4ae !important}.mdl-color--blue-grey-300{background-color:#90a4ae !important}.mdl-color-text--blue-grey-400{color:#78909c !important}.mdl-color--blue-grey-400{background-color:#78909c !important}.mdl-color-text--blue-grey-500{color:#607d8b !important}.mdl-color--blue-grey-500{background-color:#607d8b !important}.mdl-color-text--blue-grey-600{color:#546e7a !important}.mdl-color--blue-grey-600{background-color:#546e7a !important}.mdl-color-text--blue-grey-700{color:#455a64 !important}.mdl-color--blue-grey-700{background-color:#455a64 !important}.mdl-color-text--blue-grey-800{color:#37474f !important}.mdl-color--blue-grey-800{background-color:#37474f !important}.mdl-color-text--blue-grey-900{color:#263238 !important}.mdl-color--blue-grey-900{background-color:#263238 !important}.mdl-color--black{background-color:#000 !important}.mdl-color-text--black{color:#000 !important}.mdl-color--white{background-color:#fff !important}.mdl-color-text--white{color:#fff !important}.mdl-color--primary{background-color:rgb(158,158,158)!important}.mdl-color--primary-contrast{background-color:rgb(66,66,66)!important}.mdl-color--primary-dark{background-color:rgb(97,97,97)!important}.mdl-color--accent{background-color:rgb(68,138,255)!important}.mdl-color--accent-contrast{background-color:rgb(255,255,255)!important}.mdl-color-text--primary{color:rgb(158,158,158)!important}.mdl-color-text--primary-contrast{color:rgb(66,66,66)!important}.mdl-color-text--primary-dark{color:rgb(97,97,97)!important}.mdl-color-text--accent{color:rgb(68,138,255)!important}.mdl-color-text--accent-contrast{color:rgb(255,255,255)!important}.mdl-ripple{background:#000;border-radius:50%;height:50px;left:0;opacity:0;pointer-events:none;position:absolute;top:0;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%);width:50px;overflow:hidden}.mdl-ripple.is-animating{transition:transform .3s cubic-bezier(0,0,.2,1),width .3s cubic-bezier(0,0,.2,1),height .3s cubic-bezier(0,0,.2,1),opacity .6s cubic-bezier(0,0,.2,1);transition:transform .3s cubic-bezier(0,0,.2,1),width .3s cubic-bezier(0,0,.2,1),height .3s cubic-bezier(0,0,.2,1),opacity .6s cubic-bezier(0,0,.2,1),-webkit-transform .3s cubic-bezier(0,0,.2,1)}.mdl-ripple.is-visible{opacity:.3}.mdl-animation--default,.mdl-animation--fast-out-slow-in{transition-timing-function:cubic-bezier(.4,0,.2,1)}.mdl-animation--linear-out-slow-in{transition-timing-function:cubic-bezier(0,0,.2,1)}.mdl-animation--fast-out-linear-in{transition-timing-function:cubic-bezier(.4,0,1,1)}.mdl-badge{position:relative;white-space:nowrap;margin-right:24px}.mdl-badge:not([data-badge]){margin-right:auto}.mdl-badge[data-badge]:after{content:attr(data-badge);display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row;-webkit-flex-wrap:wrap;-ms-flex-wrap:wrap;flex-wrap:wrap;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-align-content:center;-ms-flex-line-pack:center;align-content:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;position:absolute;top:-11px;right:-24px;font-family:"Roboto","Helvetica","Arial",sans-serif;font-weight:600;font-size:12px;width:22px;height:22px;border-radius:50%;background:rgb(68,138,255);color:rgb(255,255,255)}.mdl-button .mdl-badge[data-badge]:after{top:-10px;right:-5px}.mdl-badge.mdl-badge--no-background[data-badge]:after{color:rgb(68,138,255);background:rgba(255,255,255,.2);box-shadow:0 0 1px gray}.mdl-badge.mdl-badge--overlap{margin-right:10px}.mdl-badge.mdl-badge--overlap:after{right:-10px}.mdl-button{background:0 0;border:none;border-radius:2px;color:#000;position:relative;height:36px;margin:0;min-width:64px;padding:0 16px;display:inline-block;font-family:"Roboto","Helvetica","Arial",sans-serif;font-size:14px;font-weight:500;text-transform:uppercase;letter-spacing:0;overflow:hidden;will-change:box-shadow;transition:box-shadow .2s cubic-bezier(.4,0,1,1),background-color .2s cubic-bezier(.4,0,.2,1),color .2s cubic-bezier(.4,0,.2,1);outline:none;cursor:pointer;text-decoration:none;text-align:center;line-height:36px;vertical-align:middle}.mdl-button::-moz-focus-inner{border:0}.mdl-button:hover{background-color:rgba(158,158,158,.2)}.mdl-button:focus:not(:active){background-color:rgba(0,0,0,.12)}.mdl-button:active{background-color:rgba(158,158,158,.4)}.mdl-button.mdl-button--colored{color:rgb(158,158,158)}.mdl-button.mdl-button--colored:focus:not(:active){background-color:rgba(0,0,0,.12)}input.mdl-button[type="submit"]{-webkit-appearance:none}.mdl-button--raised{background:rgba(158,158,158,.2);box-shadow:0 2px 2px 0 rgba(0,0,0,.14),0 3px 1px -2px rgba(0,0,0,.2),0 1px 5px 0 rgba(0,0,0,.12)}.mdl-button--raised:active{box-shadow:0 4px 5px 0 rgba(0,0,0,.14),0 1px 10px 0 rgba(0,0,0,.12),0 2px 4px -1px rgba(0,0,0,.2);background-color:rgba(158,158,158,.4)}.mdl-button--raised:focus:not(:active){box-shadow:0 0 8px rgba(0,0,0,.18),0 8px 16px rgba(0,0,0,.36);background-color:rgba(158,158,158,.4)}.mdl-button--raised.mdl-button--colored{background:rgb(158,158,158);color:rgb(66,66,66)}.mdl-button--raised.mdl-button--colored:hover{background-color:rgb(158,158,158)}.mdl-button--raised.mdl-button--colored:active{background-color:rgb(158,158,158)}.mdl-button--raised.mdl-button--colored:focus:not(:active){background-color:rgb(158,158,158)}.mdl-button--raised.mdl-button--colored .mdl-ripple{background:rgb(66,66,66)}.mdl-button--fab{border-radius:50%;font-size:24px;height:56px;margin:auto;min-width:56px;width:56px;padding:0;overflow:hidden;background:rgba(158,158,158,.2);box-shadow:0 1px 1.5px 0 rgba(0,0,0,.12),0 1px 1px 0 rgba(0,0,0,.24);position:relative;line-height:normal}.mdl-button--fab .material-icons{position:absolute;top:50%;left:50%;-webkit-transform:translate(-12px,-12px);transform:translate(-12px,-12px);line-height:24px;width:24px}.mdl-button--fab.mdl-button--mini-fab{height:40px;min-width:40px;width:40px}.mdl-button--fab .mdl-button__ripple-container{border-radius:50%;-webkit-mask-image:-webkit-radial-gradient(circle,#fff,#000)}.mdl-button--fab:active{box-shadow:0 4px 5px 0 rgba(0,0,0,.14),0 1px 10px 0 rgba(0,0,0,.12),0 2px 4px -1px rgba(0,0,0,.2);background-color:rgba(158,158,158,.4)}.mdl-button--fab:focus:not(:active){box-shadow:0 0 8px rgba(0,0,0,.18),0 8px 16px rgba(0,0,0,.36);background-color:rgba(158,158,158,.4)}.mdl-button--fab.mdl-button--colored{background:rgb(68,138,255);color:rgb(255,255,255)}.mdl-button--fab.mdl-button--colored:hover{background-color:rgb(68,138,255)}.mdl-button--fab.mdl-button--colored:focus:not(:active){background-color:rgb(68,138,255)}.mdl-button--fab.mdl-button--colored:active{background-color:rgb(68,138,255)}.mdl-button--fab.mdl-button--colored .mdl-ripple{background:rgb(255,255,255)}.mdl-button--icon{border-radius:50%;font-size:24px;height:32px;margin-left:0;margin-right:0;min-width:32px;width:32px;padding:0;overflow:hidden;color:inherit;line-height:normal}.mdl-button--icon .material-icons{position:absolute;top:50%;left:50%;-webkit-transform:translate(-12px,-12px);transform:translate(-12px,-12px);line-height:24px;width:24px}.mdl-button--icon.mdl-button--mini-icon{height:24px;min-width:24px;width:24px}.mdl-button--icon.mdl-button--mini-icon .material-icons{top:0;left:0}.mdl-button--icon .mdl-button__ripple-container{border-radius:50%;-webkit-mask-image:-webkit-radial-gradient(circle,#fff,#000)}.mdl-button__ripple-container{display:block;height:100%;left:0;position:absolute;top:0;width:100%;z-index:0;overflow:hidden}.mdl-button[disabled] .mdl-button__ripple-container .mdl-ripple,.mdl-button.mdl-button--disabled .mdl-button__ripple-container .mdl-ripple{background-color:transparent}.mdl-button--primary.mdl-button--primary{color:rgb(158,158,158)}.mdl-button--primary.mdl-button--primary .mdl-ripple{background:rgb(66,66,66)}.mdl-button--primary.mdl-button--primary.mdl-button--raised,.mdl-button--primary.mdl-button--primary.mdl-button--fab{color:rgb(66,66,66);background-color:rgb(158,158,158)}.mdl-button--accent.mdl-button--accent{color:rgb(68,138,255)}.mdl-button--accent.mdl-button--accent .mdl-ripple{background:rgb(255,255,255)}.mdl-button--accent.mdl-button--accent.mdl-button--raised,.mdl-button--accent.mdl-button--accent.mdl-button--fab{color:rgb(255,255,255);background-color:rgb(68,138,255)}.mdl-button[disabled][disabled],.mdl-button.mdl-button--disabled.mdl-button--disabled{color:rgba(0,0,0,.26);cursor:default;background-color:transparent}.mdl-button--fab[disabled][disabled],.mdl-button--fab.mdl-button--disabled.mdl-button--disabled{background-color:rgba(0,0,0,.12);color:rgba(0,0,0,.26)}.mdl-button--raised[disabled][disabled],.mdl-button--raised.mdl-button--disabled.mdl-button--disabled{background-color:rgba(0,0,0,.12);color:rgba(0,0,0,.26);box-shadow:none}.mdl-button--colored[disabled][disabled],.mdl-button--colored.mdl-button--disabled.mdl-button--disabled{color:rgba(0,0,0,.26)}.mdl-button .material-icons{vertical-align:middle}.mdl-card{display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;font-size:16px;font-weight:400;min-height:200px;overflow:hidden;width:330px;z-index:1;position:relative;background:#fff;border-radius:2px;box-sizing:border-box}.mdl-card__media{background-color:rgb(68,138,255);background-repeat:repeat;background-position:50% 50%;background-size:cover;background-origin:padding-box;background-attachment:scroll;box-sizing:border-box}.mdl-card__title{-webkit-align-items:center;-ms-flex-align:center;align-items:center;color:#000;display:block;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-justify-content:stretch;-ms-flex-pack:stretch;justify-content:stretch;line-height:normal;padding:16px;-webkit-perspective-origin:165px 56px;perspective-origin:165px 56px;-webkit-transform-origin:165px 56px;transform-origin:165px 56px;box-sizing:border-box}.mdl-card__title.mdl-card--border{border-bottom:1px solid rgba(0,0,0,.1)}.mdl-card__title-text{-webkit-align-self:flex-end;-ms-flex-item-align:end;align-self:flex-end;color:inherit;display:block;display:-webkit-flex;display:-ms-flexbox;display:flex;font-size:24px;font-weight:300;line-height:normal;overflow:hidden;-webkit-transform-origin:149px 48px;transform-origin:149px 48px;margin:0}.mdl-card__subtitle-text{font-size:14px;color:rgba(0,0,0,.54);margin:0}.mdl-card__supporting-text{color:rgba(0,0,0,.54);font-size:1rem;line-height:18px;overflow:hidden;padding:16px;width:90%}.mdl-card__supporting-text.mdl-card--border{border-bottom:1px solid rgba(0,0,0,.1)}.mdl-card__actions{font-size:16px;line-height:normal;width:100%;background-color:transparent;padding:8px;box-sizing:border-box}.mdl-card__actions.mdl-card--border{border-top:1px solid rgba(0,0,0,.1)}.mdl-card--expand{-webkit-flex-grow:1;-ms-flex-positive:1;flex-grow:1}.mdl-card__menu{position:absolute;right:16px;top:16px}.mdl-checkbox{position:relative;z-index:1;vertical-align:middle;display:inline-block;box-sizing:border-box;width:100%;height:24px;margin:0;padding:0}.mdl-checkbox.is-upgraded{padding-left:24px}.mdl-checkbox__input{line-height:24px}.mdl-checkbox.is-upgraded .mdl-checkbox__input{position:absolute;width:0;height:0;margin:0;padding:0;opacity:0;-ms-appearance:none;-moz-appearance:none;-webkit-appearance:none;appearance:none;border:none}.mdl-checkbox__box-outline{position:absolute;top:3px;left:0;display:inline-block;box-sizing:border-box;width:16px;height:16px;margin:0;cursor:pointer;overflow:hidden;border:2px solid rgba(0,0,0,.54);border-radius:2px;z-index:2}.mdl-checkbox.is-checked .mdl-checkbox__box-outline{border:2px solid rgb(158,158,158)}fieldset[disabled] .mdl-checkbox .mdl-checkbox__box-outline,.mdl-checkbox.is-disabled .mdl-checkbox__box-outline{border:2px solid rgba(0,0,0,.26);cursor:auto}.mdl-checkbox__focus-helper{position:absolute;top:3px;left:0;display:inline-block;box-sizing:border-box;width:16px;height:16px;border-radius:50%;background-color:transparent}.mdl-checkbox.is-focused .mdl-checkbox__focus-helper{box-shadow:0 0 0 8px rgba(0,0,0,.1);background-color:rgba(0,0,0,.1)}.mdl-checkbox.is-focused.is-checked .mdl-checkbox__focus-helper{box-shadow:0 0 0 8px rgba(158,158,158,.26);background-color:rgba(158,158,158,.26)}.mdl-checkbox__tick-outline{position:absolute;top:0;left:0;height:100%;width:100%;-webkit-mask:url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgdmVyc2lvbj0iMS4xIgogICB2aWV3Qm94PSIwIDAgMSAxIgogICBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJ4TWluWU1pbiBtZWV0Ij4KICA8ZGVmcz4KICAgIDxjbGlwUGF0aCBpZD0iY2xpcCI+CiAgICAgIDxwYXRoCiAgICAgICAgIGQ9Ik0gMCwwIDAsMSAxLDEgMSwwIDAsMCB6IE0gMC44NTM0Mzc1LDAuMTY3MTg3NSAwLjk1OTY4NzUsMC4yNzMxMjUgMC40MjkzNzUsMC44MDM0Mzc1IDAuMzIzMTI1LDAuOTA5Njg3NSAwLjIxNzE4NzUsMC44MDM0Mzc1IDAuMDQwMzEyNSwwLjYyNjg3NSAwLjE0NjU2MjUsMC41MjA2MjUgMC4zMjMxMjUsMC42OTc1IDAuODUzNDM3NSwwLjE2NzE4NzUgeiIKICAgICAgICAgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgLz4KICAgIDwvY2xpcFBhdGg+CiAgICA8bWFzayBpZD0ibWFzayIgbWFza1VuaXRzPSJvYmplY3RCb3VuZGluZ0JveCIgbWFza0NvbnRlbnRVbml0cz0ib2JqZWN0Qm91bmRpbmdCb3giPgogICAgICA8cGF0aAogICAgICAgICBkPSJNIDAsMCAwLDEgMSwxIDEsMCAwLDAgeiBNIDAuODUzNDM3NSwwLjE2NzE4NzUgMC45NTk2ODc1LDAuMjczMTI1IDAuNDI5Mzc1LDAuODAzNDM3NSAwLjMyMzEyNSwwLjkwOTY4NzUgMC4yMTcxODc1LDAuODAzNDM3NSAwLjA0MDMxMjUsMC42MjY4NzUgMC4xNDY1NjI1LDAuNTIwNjI1IDAuMzIzMTI1LDAuNjk3NSAwLjg1MzQzNzUsMC4xNjcxODc1IHoiCiAgICAgICAgIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiIC8+CiAgICA8L21hc2s+CiAgPC9kZWZzPgogIDxyZWN0CiAgICAgd2lkdGg9IjEiCiAgICAgaGVpZ2h0PSIxIgogICAgIHg9IjAiCiAgICAgeT0iMCIKICAgICBjbGlwLXBhdGg9InVybCgjY2xpcCkiCiAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgLz4KPC9zdmc+Cg==");mask:url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgdmVyc2lvbj0iMS4xIgogICB2aWV3Qm94PSIwIDAgMSAxIgogICBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJ4TWluWU1pbiBtZWV0Ij4KICA8ZGVmcz4KICAgIDxjbGlwUGF0aCBpZD0iY2xpcCI+CiAgICAgIDxwYXRoCiAgICAgICAgIGQ9Ik0gMCwwIDAsMSAxLDEgMSwwIDAsMCB6IE0gMC44NTM0Mzc1LDAuMTY3MTg3NSAwLjk1OTY4NzUsMC4yNzMxMjUgMC40MjkzNzUsMC44MDM0Mzc1IDAuMzIzMTI1LDAuOTA5Njg3NSAwLjIxNzE4NzUsMC44MDM0Mzc1IDAuMDQwMzEyNSwwLjYyNjg3NSAwLjE0NjU2MjUsMC41MjA2MjUgMC4zMjMxMjUsMC42OTc1IDAuODUzNDM3NSwwLjE2NzE4NzUgeiIKICAgICAgICAgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgLz4KICAgIDwvY2xpcFBhdGg+CiAgICA8bWFzayBpZD0ibWFzayIgbWFza1VuaXRzPSJvYmplY3RCb3VuZGluZ0JveCIgbWFza0NvbnRlbnRVbml0cz0ib2JqZWN0Qm91bmRpbmdCb3giPgogICAgICA8cGF0aAogICAgICAgICBkPSJNIDAsMCAwLDEgMSwxIDEsMCAwLDAgeiBNIDAuODUzNDM3NSwwLjE2NzE4NzUgMC45NTk2ODc1LDAuMjczMTI1IDAuNDI5Mzc1LDAuODAzNDM3NSAwLjMyMzEyNSwwLjkwOTY4NzUgMC4yMTcxODc1LDAuODAzNDM3NSAwLjA0MDMxMjUsMC42MjY4NzUgMC4xNDY1NjI1LDAuNTIwNjI1IDAuMzIzMTI1LDAuNjk3NSAwLjg1MzQzNzUsMC4xNjcxODc1IHoiCiAgICAgICAgIHN0eWxlPSJmaWxsOiNmZmZmZmY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlOm5vbmUiIC8+CiAgICA8L21hc2s+CiAgPC9kZWZzPgogIDxyZWN0CiAgICAgd2lkdGg9IjEiCiAgICAgaGVpZ2h0PSIxIgogICAgIHg9IjAiCiAgICAgeT0iMCIKICAgICBjbGlwLXBhdGg9InVybCgjY2xpcCkiCiAgICAgc3R5bGU9ImZpbGw6IzAwMDAwMDtmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZSIgLz4KPC9zdmc+Cg==");background:0 0;transition-duration:.28s;transition-timing-function:cubic-bezier(.4,0,.2,1);transition-property:background}.mdl-checkbox.is-checked .mdl-checkbox__tick-outline{background:rgb(158,158,158)url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgdmVyc2lvbj0iMS4xIgogICB2aWV3Qm94PSIwIDAgMSAxIgogICBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJ4TWluWU1pbiBtZWV0Ij4KICA8cGF0aAogICAgIGQ9Ik0gMC4wNDAzODA1OSwwLjYyNjc3NjcgMC4xNDY0NDY2MSwwLjUyMDcxMDY4IDAuNDI5Mjg5MzIsMC44MDM1NTMzOSAwLjMyMzIyMzMsMC45MDk2MTk0MSB6IE0gMC4yMTcxNTcyOSwwLjgwMzU1MzM5IDAuODUzNTUzMzksMC4xNjcxNTcyOSAwLjk1OTYxOTQxLDAuMjczMjIzMyAwLjMyMzIyMzMsMC45MDk2MTk0MSB6IgogICAgIGlkPSJyZWN0Mzc4MCIKICAgICBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiAvPgo8L3N2Zz4K")}fieldset[disabled] .mdl-checkbox.is-checked .mdl-checkbox__tick-outline,.mdl-checkbox.is-checked.is-disabled .mdl-checkbox__tick-outline{background:rgba(0,0,0,.26)url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgdmVyc2lvbj0iMS4xIgogICB2aWV3Qm94PSIwIDAgMSAxIgogICBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJ4TWluWU1pbiBtZWV0Ij4KICA8cGF0aAogICAgIGQ9Ik0gMC4wNDAzODA1OSwwLjYyNjc3NjcgMC4xNDY0NDY2MSwwLjUyMDcxMDY4IDAuNDI5Mjg5MzIsMC44MDM1NTMzOSAwLjMyMzIyMzMsMC45MDk2MTk0MSB6IE0gMC4yMTcxNTcyOSwwLjgwMzU1MzM5IDAuODUzNTUzMzksMC4xNjcxNTcyOSAwLjk1OTYxOTQxLDAuMjczMjIzMyAwLjMyMzIyMzMsMC45MDk2MTk0MSB6IgogICAgIGlkPSJyZWN0Mzc4MCIKICAgICBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lIiAvPgo8L3N2Zz4K")}.mdl-checkbox__label{position:relative;cursor:pointer;font-size:16px;line-height:24px;margin:0}fieldset[disabled] .mdl-checkbox .mdl-checkbox__label,.mdl-checkbox.is-disabled .mdl-checkbox__label{color:rgba(0,0,0,.26);cursor:auto}.mdl-checkbox__ripple-container{position:absolute;z-index:2;top:-6px;left:-10px;box-sizing:border-box;width:36px;height:36px;border-radius:50%;cursor:pointer;overflow:hidden;-webkit-mask-image:-webkit-radial-gradient(circle,#fff,#000)}.mdl-checkbox__ripple-container .mdl-ripple{background:rgb(158,158,158)}fieldset[disabled] .mdl-checkbox .mdl-checkbox__ripple-container,.mdl-checkbox.is-disabled .mdl-checkbox__ripple-container{cursor:auto}fieldset[disabled] .mdl-checkbox .mdl-checkbox__ripple-container .mdl-ripple,.mdl-checkbox.is-disabled .mdl-checkbox__ripple-container .mdl-ripple{background:0 0}.mdl-chip{height:32px;font-family:"Roboto","Helvetica","Arial",sans-serif;line-height:32px;padding:0 12px;border:0;border-radius:16px;background-color:#dedede;display:inline-block;color:rgba(0,0,0,.87);margin:2px 0;font-size:0;white-space:nowrap}.mdl-chip__text{font-size:13px;vertical-align:middle;display:inline-block}.mdl-chip__action{height:24px;width:24px;background:0 0;opacity:.54;cursor:pointer;padding:0;margin:0 0 0 4px;font-size:13px;text-decoration:none;color:rgba(0,0,0,.87);border:none;outline:none}.mdl-chip__action,.mdl-chip__contact{display:inline-block;vertical-align:middle;overflow:hidden;text-align:center}.mdl-chip__contact{height:32px;width:32px;border-radius:16px;margin-right:8px;font-size:18px;line-height:32px}.mdl-chip:focus{outline:0;box-shadow:0 2px 2px 0 rgba(0,0,0,.14),0 3px 1px -2px rgba(0,0,0,.2),0 1px 5px 0 rgba(0,0,0,.12)}.mdl-chip:active{background-color:#d6d6d6}.mdl-chip--deletable{padding-right:4px}.mdl-chip--contact{padding-left:0}.mdl-data-table{position:relative;border:1px solid rgba(0,0,0,.12);border-collapse:collapse;white-space:nowrap;font-size:13px;background-color:#fff}.mdl-data-table thead{padding-bottom:3px}.mdl-data-table thead .mdl-data-table__select{margin-top:0}.mdl-data-table tbody tr{position:relative;height:48px;transition-duration:.28s;transition-timing-function:cubic-bezier(.4,0,.2,1);transition-property:background-color}.mdl-data-table tbody tr.is-selected{background-color:#e0e0e0}.mdl-data-table tbody tr:hover{background-color:#eee}.mdl-data-table td{text-align:right}.mdl-data-table th{padding:0 18px 12px 18px;text-align:right}.mdl-data-table td:first-of-type,.mdl-data-table th:first-of-type{padding-left:24px}.mdl-data-table td:last-of-type,.mdl-data-table th:last-of-type{padding-right:24px}.mdl-data-table td{position:relative;height:48px;border-top:1px solid rgba(0,0,0,.12);border-bottom:1px solid rgba(0,0,0,.12);padding:12px 18px;box-sizing:border-box}.mdl-data-table td,.mdl-data-table td .mdl-data-table__select{vertical-align:middle}.mdl-data-table th{position:relative;vertical-align:bottom;text-overflow:ellipsis;font-weight:700;line-height:24px;letter-spacing:0;height:48px;font-size:12px;color:rgba(0,0,0,.54);padding-bottom:8px;box-sizing:border-box}.mdl-data-table th.mdl-data-table__header--sorted-ascending,.mdl-data-table th.mdl-data-table__header--sorted-descending{color:rgba(0,0,0,.87)}.mdl-data-table th.mdl-data-table__header--sorted-ascending:before,.mdl-data-table th.mdl-data-table__header--sorted-descending:before{font-family:'Material Icons';font-weight:400;font-style:normal;line-height:1;letter-spacing:normal;text-transform:none;display:inline-block;word-wrap:normal;-moz-font-feature-settings:'liga';font-feature-settings:'liga';-webkit-font-feature-settings:'liga';-webkit-font-smoothing:antialiased;font-size:16px;content:"\e5d8";margin-right:5px;vertical-align:sub}.mdl-data-table th.mdl-data-table__header--sorted-ascending:hover,.mdl-data-table th.mdl-data-table__header--sorted-descending:hover{cursor:pointer}.mdl-data-table th.mdl-data-table__header--sorted-ascending:hover:before,.mdl-data-table th.mdl-data-table__header--sorted-descending:hover:before{color:rgba(0,0,0,.26)}.mdl-data-table th.mdl-data-table__header--sorted-descending:before{content:"\e5db"}.mdl-data-table__select{width:16px}.mdl-data-table__cell--non-numeric.mdl-data-table__cell--non-numeric{text-align:left}.mdl-dialog{border:none;box-shadow:0 9px 46px 8px rgba(0,0,0,.14),0 11px 15px -7px rgba(0,0,0,.12),0 24px 38px 3px rgba(0,0,0,.2);width:280px}.mdl-dialog__title{padding:24px 24px 0;margin:0;font-size:2.5rem}.mdl-dialog__actions{padding:8px 8px 8px 24px;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:row-reverse;-ms-flex-direction:row-reverse;flex-direction:row-reverse;-webkit-flex-wrap:wrap;-ms-flex-wrap:wrap;flex-wrap:wrap}.mdl-dialog__actions>*{margin-right:8px;height:36px}.mdl-dialog__actions>*:first-child{margin-right:0}.mdl-dialog__actions--full-width{padding:0 0 8px}.mdl-dialog__actions--full-width>*{height:48px;-webkit-flex:0 0 100%;-ms-flex:0 0 100%;flex:0 0 100%;padding-right:16px;margin-right:0;text-align:right}.mdl-dialog__content{padding:20px 24px 24px;color:rgba(0,0,0,.54)}.mdl-mega-footer{padding:16px 40px;color:#9e9e9e;background-color:#424242}.mdl-mega-footer--top-section:after,.mdl-mega-footer--middle-section:after,.mdl-mega-footer--bottom-section:after,.mdl-mega-footer__top-section:after,.mdl-mega-footer__middle-section:after,.mdl-mega-footer__bottom-section:after{content:'';display:block;clear:both}.mdl-mega-footer--left-section,.mdl-mega-footer__left-section,.mdl-mega-footer--right-section,.mdl-mega-footer__right-section{margin-bottom:16px}.mdl-mega-footer--right-section a,.mdl-mega-footer__right-section a{display:block;margin-bottom:16px;color:inherit;text-decoration:none}@media screen and (min-width:760px){.mdl-mega-footer--left-section,.mdl-mega-footer__left-section{float:left}.mdl-mega-footer--right-section,.mdl-mega-footer__right-section{float:right}.mdl-mega-footer--right-section a,.mdl-mega-footer__right-section a{display:inline-block;margin-left:16px;line-height:36px;vertical-align:middle}}.mdl-mega-footer--social-btn,.mdl-mega-footer__social-btn{width:36px;height:36px;padding:0;margin:0;background-color:#9e9e9e;border:none}.mdl-mega-footer--drop-down-section,.mdl-mega-footer__drop-down-section{display:block;position:relative}@media screen and (min-width:760px){.mdl-mega-footer--drop-down-section,.mdl-mega-footer__drop-down-section{width:33%}.mdl-mega-footer--drop-down-section:nth-child(1),.mdl-mega-footer--drop-down-section:nth-child(2),.mdl-mega-footer__drop-down-section:nth-child(1),.mdl-mega-footer__drop-down-section:nth-child(2){float:left}.mdl-mega-footer--drop-down-section:nth-child(3),.mdl-mega-footer__drop-down-section:nth-child(3){float:right}.mdl-mega-footer--drop-down-section:nth-child(3):after,.mdl-mega-footer__drop-down-section:nth-child(3):after{clear:right}.mdl-mega-footer--drop-down-section:nth-child(4),.mdl-mega-footer__drop-down-section:nth-child(4){clear:right;float:right}.mdl-mega-footer--middle-section:after,.mdl-mega-footer__middle-section:after{content:'';display:block;clear:both}.mdl-mega-footer--bottom-section,.mdl-mega-footer__bottom-section{padding-top:0}}@media screen and (min-width:1024px){.mdl-mega-footer--drop-down-section,.mdl-mega-footer--drop-down-section:nth-child(3),.mdl-mega-footer--drop-down-section:nth-child(4),.mdl-mega-footer__drop-down-section,.mdl-mega-footer__drop-down-section:nth-child(3),.mdl-mega-footer__drop-down-section:nth-child(4){width:24%;float:left}}.mdl-mega-footer--heading-checkbox,.mdl-mega-footer__heading-checkbox{position:absolute;width:100%;height:55.8px;padding:32px;margin:-16px 0 0;cursor:pointer;z-index:1;opacity:0}.mdl-mega-footer--heading-checkbox+.mdl-mega-footer--heading:after,.mdl-mega-footer--heading-checkbox+.mdl-mega-footer__heading:after,.mdl-mega-footer__heading-checkbox+.mdl-mega-footer--heading:after,.mdl-mega-footer__heading-checkbox+.mdl-mega-footer__heading:after{font-family:'Material Icons';content:'\E5CE'}.mdl-mega-footer--heading-checkbox:checked~.mdl-mega-footer--link-list,.mdl-mega-footer--heading-checkbox:checked~.mdl-mega-footer__link-list,.mdl-mega-footer--heading-checkbox:checked+.mdl-mega-footer--heading+.mdl-mega-footer--link-list,.mdl-mega-footer--heading-checkbox:checked+.mdl-mega-footer__heading+.mdl-mega-footer__link-list,.mdl-mega-footer__heading-checkbox:checked~.mdl-mega-footer--link-list,.mdl-mega-footer__heading-checkbox:checked~.mdl-mega-footer__link-list,.mdl-mega-footer__heading-checkbox:checked+.mdl-mega-footer--heading+.mdl-mega-footer--link-list,.mdl-mega-footer__heading-checkbox:checked+.mdl-mega-footer__heading+.mdl-mega-footer__link-list{display:none}.mdl-mega-footer--heading-checkbox:checked+.mdl-mega-footer--heading:after,.mdl-mega-footer--heading-checkbox:checked+.mdl-mega-footer__heading:after,.mdl-mega-footer__heading-checkbox:checked+.mdl-mega-footer--heading:after,.mdl-mega-footer__heading-checkbox:checked+.mdl-mega-footer__heading:after{font-family:'Material Icons';content:'\E5CF'}.mdl-mega-footer--heading,.mdl-mega-footer__heading{position:relative;width:100%;padding-right:39.8px;margin-bottom:16px;box-sizing:border-box;font-size:14px;line-height:23.8px;font-weight:500;white-space:nowrap;text-overflow:ellipsis;overflow:hidden;color:#e0e0e0}.mdl-mega-footer--heading:after,.mdl-mega-footer__heading:after{content:'';position:absolute;top:0;right:0;display:block;width:23.8px;height:23.8px;background-size:cover}.mdl-mega-footer--link-list,.mdl-mega-footer__link-list{list-style:none;padding:0;margin:0 0 32px}.mdl-mega-footer--link-list:after,.mdl-mega-footer__link-list:after{clear:both;display:block;content:''}.mdl-mega-footer--link-list li,.mdl-mega-footer__link-list li{font-size:14px;font-weight:400;letter-spacing:0;line-height:20px}.mdl-mega-footer--link-list a,.mdl-mega-footer__link-list a{color:inherit;text-decoration:none;white-space:nowrap}@media screen and (min-width:760px){.mdl-mega-footer--heading-checkbox,.mdl-mega-footer__heading-checkbox{display:none}.mdl-mega-footer--heading-checkbox+.mdl-mega-footer--heading:after,.mdl-mega-footer--heading-checkbox+.mdl-mega-footer__heading:after,.mdl-mega-footer__heading-checkbox+.mdl-mega-footer--heading:after,.mdl-mega-footer__heading-checkbox+.mdl-mega-footer__heading:after{content:''}.mdl-mega-footer--heading-checkbox:checked~.mdl-mega-footer--link-list,.mdl-mega-footer--heading-checkbox:checked~.mdl-mega-footer__link-list,.mdl-mega-footer--heading-checkbox:checked+.mdl-mega-footer__heading+.mdl-mega-footer__link-list,.mdl-mega-footer--heading-checkbox:checked+.mdl-mega-footer--heading+.mdl-mega-footer--link-list,.mdl-mega-footer__heading-checkbox:checked~.mdl-mega-footer--link-list,.mdl-mega-footer__heading-checkbox:checked~.mdl-mega-footer__link-list,.mdl-mega-footer__heading-checkbox:checked+.mdl-mega-footer__heading+.mdl-mega-footer__link-list,.mdl-mega-footer__heading-checkbox:checked+.mdl-mega-footer--heading+.mdl-mega-footer--link-list{display:block}.mdl-mega-footer--heading-checkbox:checked+.mdl-mega-footer--heading:after,.mdl-mega-footer--heading-checkbox:checked+.mdl-mega-footer__heading:after,.mdl-mega-footer__heading-checkbox:checked+.mdl-mega-footer--heading:after,.mdl-mega-footer__heading-checkbox:checked+.mdl-mega-footer__heading:after{content:''}}.mdl-mega-footer--bottom-section,.mdl-mega-footer__bottom-section{padding-top:16px;margin-bottom:16px}.mdl-logo{margin-bottom:16px;color:#fff}.mdl-mega-footer--bottom-section .mdl-mega-footer--link-list li,.mdl-mega-footer__bottom-section .mdl-mega-footer__link-list li{float:left;margin-bottom:0;margin-right:16px}@media screen and (min-width:760px){.mdl-logo{float:left;margin-bottom:0;margin-right:16px}}.mdl-mini-footer{display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-flow:row wrap;-ms-flex-flow:row wrap;flex-flow:row wrap;-webkit-justify-content:space-between;-ms-flex-pack:justify;justify-content:space-between;padding:32px 16px;color:#9e9e9e;background-color:#424242}.mdl-mini-footer:after{content:'';display:block}.mdl-mini-footer .mdl-logo{line-height:36px}.mdl-mini-footer--link-list,.mdl-mini-footer__link-list{display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-flow:row nowrap;-ms-flex-flow:row nowrap;flex-flow:row nowrap;list-style:none;margin:0;padding:0}.mdl-mini-footer--link-list li,.mdl-mini-footer__link-list li{margin-bottom:0;margin-right:16px}@media screen and (min-width:760px){.mdl-mini-footer--link-list li,.mdl-mini-footer__link-list li{line-height:36px}}.mdl-mini-footer--link-list a,.mdl-mini-footer__link-list a{color:inherit;text-decoration:none;white-space:nowrap}.mdl-mini-footer--left-section,.mdl-mini-footer__left-section{display:inline-block;-webkit-order:0;-ms-flex-order:0;order:0}.mdl-mini-footer--right-section,.mdl-mini-footer__right-section{display:inline-block;-webkit-order:1;-ms-flex-order:1;order:1}.mdl-mini-footer--social-btn,.mdl-mini-footer__social-btn{width:36px;height:36px;padding:0;margin:0;background-color:#9e9e9e;border:none}.mdl-icon-toggle{position:relative;z-index:1;vertical-align:middle;display:inline-block;height:32px;margin:0;padding:0}.mdl-icon-toggle__input{line-height:32px}.mdl-icon-toggle.is-upgraded .mdl-icon-toggle__input{position:absolute;width:0;height:0;margin:0;padding:0;opacity:0;-ms-appearance:none;-moz-appearance:none;-webkit-appearance:none;appearance:none;border:none}.mdl-icon-toggle__label{display:inline-block;position:relative;cursor:pointer;height:32px;width:32px;min-width:32px;color:#616161;border-radius:50%;padding:0;margin-left:0;margin-right:0;text-align:center;background-color:transparent;will-change:background-color;transition:background-color .2s cubic-bezier(.4,0,.2,1),color .2s cubic-bezier(.4,0,.2,1)}.mdl-icon-toggle__label.material-icons{line-height:32px;font-size:24px}.mdl-icon-toggle.is-checked .mdl-icon-toggle__label{color:rgb(158,158,158)}.mdl-icon-toggle.is-disabled .mdl-icon-toggle__label{color:rgba(0,0,0,.26);cursor:auto;transition:none}.mdl-icon-toggle.is-focused .mdl-icon-toggle__label{background-color:rgba(0,0,0,.12)}.mdl-icon-toggle.is-focused.is-checked .mdl-icon-toggle__label{background-color:rgba(158,158,158,.26)}.mdl-icon-toggle__ripple-container{position:absolute;z-index:2;top:-2px;left:-2px;box-sizing:border-box;width:36px;height:36px;border-radius:50%;cursor:pointer;overflow:hidden;-webkit-mask-image:-webkit-radial-gradient(circle,#fff,#000)}.mdl-icon-toggle__ripple-container .mdl-ripple{background:#616161}.mdl-icon-toggle.is-disabled .mdl-icon-toggle__ripple-container{cursor:auto}.mdl-icon-toggle.is-disabled .mdl-icon-toggle__ripple-container .mdl-ripple{background:0 0}.mdl-list{display:block;padding:8px 0;list-style:none}.mdl-list__item{font-family:"Roboto","Helvetica","Arial",sans-serif;font-size:16px;font-weight:400;letter-spacing:.04em;line-height:1;min-height:48px;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row;-webkit-flex-wrap:nowrap;-ms-flex-wrap:nowrap;flex-wrap:nowrap;padding:16px;cursor:default;color:rgba(0,0,0,.87);overflow:hidden}.mdl-list__item,.mdl-list__item .mdl-list__item-primary-content{box-sizing:border-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-align-items:center;-ms-flex-align:center;align-items:center}.mdl-list__item .mdl-list__item-primary-content{-webkit-order:0;-ms-flex-order:0;order:0;-webkit-flex-grow:2;-ms-flex-positive:2;flex-grow:2;text-decoration:none}.mdl-list__item .mdl-list__item-primary-content .mdl-list__item-icon{margin-right:32px}.mdl-list__item .mdl-list__item-primary-content .mdl-list__item-avatar{margin-right:16px}.mdl-list__item .mdl-list__item-secondary-content{display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-flow:column;-ms-flex-flow:column;flex-flow:column;-webkit-align-items:flex-end;-ms-flex-align:end;align-items:flex-end;margin-left:16px}.mdl-list__item .mdl-list__item-secondary-content .mdl-list__item-secondary-action label{display:inline}.mdl-list__item .mdl-list__item-secondary-content .mdl-list__item-secondary-info{font-size:12px;font-weight:400;line-height:1;letter-spacing:0;color:rgba(0,0,0,.54)}.mdl-list__item .mdl-list__item-secondary-content .mdl-list__item-sub-header{padding:0 0 0 16px}.mdl-list__item-icon,.mdl-list__item-icon.material-icons{height:24px;width:24px;font-size:24px;box-sizing:border-box;color:#757575}.mdl-list__item-avatar,.mdl-list__item-avatar.material-icons{height:40px;width:40px;box-sizing:border-box;border-radius:50%;background-color:#757575;font-size:40px;color:#fff}.mdl-list__item--two-line{height:72px}.mdl-list__item--two-line .mdl-list__item-primary-content{height:36px;line-height:20px;display:block}.mdl-list__item--two-line .mdl-list__item-primary-content .mdl-list__item-avatar{float:left}.mdl-list__item--two-line .mdl-list__item-primary-content .mdl-list__item-icon{float:left;margin-top:6px}.mdl-list__item--two-line .mdl-list__item-primary-content .mdl-list__item-secondary-content{height:36px}.mdl-list__item--two-line .mdl-list__item-primary-content .mdl-list__item-sub-title{font-size:14px;font-weight:400;letter-spacing:0;line-height:18px;color:rgba(0,0,0,.54);display:block;padding:0}.mdl-list__item--three-line{height:88px}.mdl-list__item--three-line .mdl-list__item-primary-content{height:52px;line-height:20px;display:block}.mdl-list__item--three-line .mdl-list__item-primary-content .mdl-list__item-avatar,.mdl-list__item--three-line .mdl-list__item-primary-content .mdl-list__item-icon{float:left}.mdl-list__item--three-line .mdl-list__item-secondary-content{height:52px}.mdl-list__item--three-line .mdl-list__item-text-body{font-size:14px;font-weight:400;letter-spacing:0;line-height:18px;height:52px;color:rgba(0,0,0,.54);display:block;padding:0}.mdl-menu__container{display:block;margin:0;padding:0;border:none;position:absolute;overflow:visible;height:0;width:0;visibility:hidden;z-index:-1}.mdl-menu__container.is-visible,.mdl-menu__container.is-animating{z-index:999;visibility:visible}.mdl-menu__outline{display:block;background:#fff;margin:0;padding:0;border:none;border-radius:2px;position:absolute;top:0;left:0;overflow:hidden;opacity:0;-webkit-transform:scale(0);transform:scale(0);-webkit-transform-origin:0 0;transform-origin:0 0;box-shadow:0 2px 2px 0 rgba(0,0,0,.14),0 3px 1px -2px rgba(0,0,0,.2),0 1px 5px 0 rgba(0,0,0,.12);will-change:transform;transition:transform .3s cubic-bezier(.4,0,.2,1),opacity .2s cubic-bezier(.4,0,.2,1);transition:transform .3s cubic-bezier(.4,0,.2,1),opacity .2s cubic-bezier(.4,0,.2,1),-webkit-transform .3s cubic-bezier(.4,0,.2,1);z-index:-1}.mdl-menu__container.is-visible .mdl-menu__outline{opacity:1;-webkit-transform:scale(1);transform:scale(1);z-index:999}.mdl-menu__outline.mdl-menu--bottom-right{-webkit-transform-origin:100% 0;transform-origin:100% 0}.mdl-menu__outline.mdl-menu--top-left{-webkit-transform-origin:0 100%;transform-origin:0 100%}.mdl-menu__outline.mdl-menu--top-right{-webkit-transform-origin:100% 100%;transform-origin:100% 100%}.mdl-menu{position:absolute;list-style:none;top:0;left:0;height:auto;width:auto;min-width:124px;padding:8px 0;margin:0;opacity:0;clip:rect(0 0 0 0);z-index:-1}.mdl-menu__container.is-visible .mdl-menu{opacity:1;z-index:999}.mdl-menu.is-animating{transition:opacity .2s cubic-bezier(.4,0,.2,1),clip .3s cubic-bezier(.4,0,.2,1)}.mdl-menu.mdl-menu--bottom-right{left:auto;right:0}.mdl-menu.mdl-menu--top-left{top:auto;bottom:0}.mdl-menu.mdl-menu--top-right{top:auto;left:auto;bottom:0;right:0}.mdl-menu.mdl-menu--unaligned{top:auto;left:auto}.mdl-menu__item{display:block;border:none;color:rgba(0,0,0,.87);background-color:transparent;text-align:left;margin:0;padding:0 16px;outline-color:#bdbdbd;position:relative;overflow:hidden;font-size:14px;font-weight:400;letter-spacing:0;text-decoration:none;cursor:pointer;height:48px;line-height:48px;white-space:nowrap;opacity:0;transition:opacity .2s cubic-bezier(.4,0,.2,1);-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.mdl-menu__container.is-visible .mdl-menu__item{opacity:1}.mdl-menu__item::-moz-focus-inner{border:0}.mdl-menu__item--full-bleed-divider{border-bottom:1px solid rgba(0,0,0,.12)}.mdl-menu__item[disabled],.mdl-menu__item[data-mdl-disabled]{color:#bdbdbd;background-color:transparent;cursor:auto}.mdl-menu__item[disabled]:hover,.mdl-menu__item[data-mdl-disabled]:hover{background-color:transparent}.mdl-menu__item[disabled]:focus,.mdl-menu__item[data-mdl-disabled]:focus{background-color:transparent}.mdl-menu__item[disabled] .mdl-ripple,.mdl-menu__item[data-mdl-disabled] .mdl-ripple{background:0 0}.mdl-menu__item:hover{background-color:#eee}.mdl-menu__item:focus{outline:none;background-color:#eee}.mdl-menu__item:active{background-color:#e0e0e0}.mdl-menu__item--ripple-container{display:block;height:100%;left:0;position:absolute;top:0;width:100%;z-index:0;overflow:hidden}.mdl-progress{display:block;position:relative;height:4px;width:500px;max-width:100%}.mdl-progress>.bar{display:block;position:absolute;top:0;bottom:0;width:0%;transition:width .2s cubic-bezier(.4,0,.2,1)}.mdl-progress>.progressbar{background-color:rgb(158,158,158);z-index:1;left:0}.mdl-progress>.bufferbar{background-image:linear-gradient(to right,rgba(66,66,66,.7),rgba(66,66,66,.7)),linear-gradient(to right,rgb(158,158,158),rgb(158,158,158));z-index:0;left:0}.mdl-progress>.auxbar{right:0}@supports (-webkit-appearance:none){.mdl-progress:not(.mdl-progress--indeterminate):not(.mdl-progress--indeterminate)>.auxbar,.mdl-progress:not(.mdl-progress__indeterminate):not(.mdl-progress__indeterminate)>.auxbar{background-image:linear-gradient(to right,rgba(66,66,66,.7),rgba(66,66,66,.7)),linear-gradient(to right,rgb(158,158,158),rgb(158,158,158));-webkit-mask:url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIj8+Cjxzdmcgd2lkdGg9IjEyIiBoZWlnaHQ9IjQiIHZpZXdQb3J0PSIwIDAgMTIgNCIgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgogIDxlbGxpcHNlIGN4PSIyIiBjeT0iMiIgcng9IjIiIHJ5PSIyIj4KICAgIDxhbmltYXRlIGF0dHJpYnV0ZU5hbWU9ImN4IiBmcm9tPSIyIiB0bz0iLTEwIiBkdXI9IjAuNnMiIHJlcGVhdENvdW50PSJpbmRlZmluaXRlIiAvPgogIDwvZWxsaXBzZT4KICA8ZWxsaXBzZSBjeD0iMTQiIGN5PSIyIiByeD0iMiIgcnk9IjIiIGNsYXNzPSJsb2FkZXIiPgogICAgPGFuaW1hdGUgYXR0cmlidXRlTmFtZT0iY3giIGZyb209IjE0IiB0bz0iMiIgZHVyPSIwLjZzIiByZXBlYXRDb3VudD0iaW5kZWZpbml0ZSIgLz4KICA8L2VsbGlwc2U+Cjwvc3ZnPgo=");mask:url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIj8+Cjxzdmcgd2lkdGg9IjEyIiBoZWlnaHQ9IjQiIHZpZXdQb3J0PSIwIDAgMTIgNCIgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgogIDxlbGxpcHNlIGN4PSIyIiBjeT0iMiIgcng9IjIiIHJ5PSIyIj4KICAgIDxhbmltYXRlIGF0dHJpYnV0ZU5hbWU9ImN4IiBmcm9tPSIyIiB0bz0iLTEwIiBkdXI9IjAuNnMiIHJlcGVhdENvdW50PSJpbmRlZmluaXRlIiAvPgogIDwvZWxsaXBzZT4KICA8ZWxsaXBzZSBjeD0iMTQiIGN5PSIyIiByeD0iMiIgcnk9IjIiIGNsYXNzPSJsb2FkZXIiPgogICAgPGFuaW1hdGUgYXR0cmlidXRlTmFtZT0iY3giIGZyb209IjE0IiB0bz0iMiIgZHVyPSIwLjZzIiByZXBlYXRDb3VudD0iaW5kZWZpbml0ZSIgLz4KICA8L2VsbGlwc2U+Cjwvc3ZnPgo=")}}.mdl-progress:not(.mdl-progress--indeterminate)>.auxbar,.mdl-progress:not(.mdl-progress__indeterminate)>.auxbar{background-image:linear-gradient(to right,rgba(66,66,66,.9),rgba(66,66,66,.9)),linear-gradient(to right,rgb(158,158,158),rgb(158,158,158))}.mdl-progress.mdl-progress--indeterminate>.bar1,.mdl-progress.mdl-progress__indeterminate>.bar1{-webkit-animation-name:indeterminate1;animation-name:indeterminate1}.mdl-progress.mdl-progress--indeterminate>.bar1,.mdl-progress.mdl-progress__indeterminate>.bar1,.mdl-progress.mdl-progress--indeterminate>.bar3,.mdl-progress.mdl-progress__indeterminate>.bar3{background-color:rgb(158,158,158);-webkit-animation-duration:2s;animation-duration:2s;-webkit-animation-iteration-count:infinite;animation-iteration-count:infinite;-webkit-animation-timing-function:linear;animation-timing-function:linear}.mdl-progress.mdl-progress--indeterminate>.bar3,.mdl-progress.mdl-progress__indeterminate>.bar3{background-image:none;-webkit-animation-name:indeterminate2;animation-name:indeterminate2}@-webkit-keyframes indeterminate1{0%{left:0%;width:0%}50%{left:25%;width:75%}75%{left:100%;width:0%}}@keyframes indeterminate1{0%{left:0%;width:0%}50%{left:25%;width:75%}75%{left:100%;width:0%}}@-webkit-keyframes indeterminate2{0%,50%{left:0%;width:0%}75%{left:0%;width:25%}100%{left:100%;width:0%}}@keyframes indeterminate2{0%,50%{left:0%;width:0%}75%{left:0%;width:25%}100%{left:100%;width:0%}}.mdl-navigation{display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-wrap:nowrap;-ms-flex-wrap:nowrap;flex-wrap:nowrap;box-sizing:border-box}.mdl-navigation__link{color:#424242;text-decoration:none;margin:0;font-size:14px;font-weight:400;line-height:24px;letter-spacing:0;opacity:.87}.mdl-navigation__link .material-icons{vertical-align:middle}.mdl-layout{width:100%;height:100%;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;overflow-y:auto;overflow-x:hidden;position:relative;-webkit-overflow-scrolling:touch}.mdl-layout.is-small-screen .mdl-layout--large-screen-only{display:none}.mdl-layout:not(.is-small-screen) .mdl-layout--small-screen-only{display:none}.mdl-layout__container{position:absolute;width:100%;height:100%}.mdl-layout__title,.mdl-layout-title{display:block;position:relative;font-family:"Roboto","Helvetica","Arial",sans-serif;font-size:20px;line-height:1;letter-spacing:.02em;font-weight:400;box-sizing:border-box}.mdl-layout-spacer{-webkit-flex-grow:1;-ms-flex-positive:1;flex-grow:1}.mdl-layout__drawer{display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;-webkit-flex-wrap:nowrap;-ms-flex-wrap:nowrap;flex-wrap:nowrap;width:240px;height:100%;max-height:100%;position:absolute;top:0;left:0;box-shadow:0 2px 2px 0 rgba(0,0,0,.14),0 3px 1px -2px rgba(0,0,0,.2),0 1px 5px 0 rgba(0,0,0,.12);box-sizing:border-box;border-right:1px solid #e0e0e0;background:#fafafa;-webkit-transform:translateX(-250px);transform:translateX(-250px);-webkit-transform-style:preserve-3d;transform-style:preserve-3d;will-change:transform;transition-duration:.2s;transition-timing-function:cubic-bezier(.4,0,.2,1);transition-property:transform;transition-property:transform,-webkit-transform;color:#424242;overflow:visible;overflow-y:auto;z-index:5}.mdl-layout__drawer.is-visible{-webkit-transform:translateX(0);transform:translateX(0)}.mdl-layout__drawer.is-visible~.mdl-layout__content.mdl-layout__content{overflow:hidden}.mdl-layout__drawer>*{-webkit-flex-shrink:0;-ms-flex-negative:0;flex-shrink:0}.mdl-layout__drawer>.mdl-layout__title,.mdl-layout__drawer>.mdl-layout-title{line-height:64px;padding-left:40px}@media screen and (max-width:1024px){.mdl-layout__drawer>.mdl-layout__title,.mdl-layout__drawer>.mdl-layout-title{line-height:56px;padding-left:16px}}.mdl-layout__drawer .mdl-navigation{-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;-webkit-align-items:stretch;-ms-flex-align:stretch;align-items:stretch;padding-top:16px}.mdl-layout__drawer .mdl-navigation .mdl-navigation__link{display:block;-webkit-flex-shrink:0;-ms-flex-negative:0;flex-shrink:0;padding:16px 40px;margin:0;color:#757575}@media screen and (max-width:1024px){.mdl-layout__drawer .mdl-navigation .mdl-navigation__link{padding:16px}}.mdl-layout__drawer .mdl-navigation .mdl-navigation__link:hover{background-color:#e0e0e0}.mdl-layout__drawer .mdl-navigation .mdl-navigation__link--current{background-color:#e0e0e0;color:#000}@media screen and (min-width:1025px){.mdl-layout--fixed-drawer>.mdl-layout__drawer{-webkit-transform:translateX(0);transform:translateX(0)}}.mdl-layout__drawer-button{display:block;position:absolute;height:48px;width:48px;border:0;-webkit-flex-shrink:0;-ms-flex-negative:0;flex-shrink:0;overflow:hidden;text-align:center;cursor:pointer;font-size:26px;line-height:56px;font-family:Helvetica,Arial,sans-serif;margin:8px 12px;top:0;left:0;color:rgb(66,66,66);z-index:4}.mdl-layout__header .mdl-layout__drawer-button{position:absolute;color:rgb(66,66,66);background-color:inherit}@media screen and (max-width:1024px){.mdl-layout__header .mdl-layout__drawer-button{margin:4px}}@media screen and (max-width:1024px){.mdl-layout__drawer-button{margin:4px;color:rgba(0,0,0,.5)}}@media screen and (min-width:1025px){.mdl-layout__drawer-button{line-height:54px}.mdl-layout--no-desktop-drawer-button .mdl-layout__drawer-button,.mdl-layout--fixed-drawer>.mdl-layout__drawer-button,.mdl-layout--no-drawer-button .mdl-layout__drawer-button{display:none}}.mdl-layout__header{display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;-webkit-flex-wrap:nowrap;-ms-flex-wrap:nowrap;flex-wrap:nowrap;-webkit-justify-content:flex-start;-ms-flex-pack:start;justify-content:flex-start;box-sizing:border-box;-webkit-flex-shrink:0;-ms-flex-negative:0;flex-shrink:0;width:100%;margin:0;padding:0;border:none;min-height:64px;max-height:1000px;z-index:3;background-color:rgb(158,158,158);color:rgb(66,66,66);box-shadow:0 2px 2px 0 rgba(0,0,0,.14),0 3px 1px -2px rgba(0,0,0,.2),0 1px 5px 0 rgba(0,0,0,.12);transition-duration:.2s;transition-timing-function:cubic-bezier(.4,0,.2,1);transition-property:max-height,box-shadow}@media screen and (max-width:1024px){.mdl-layout__header{min-height:56px}}.mdl-layout--fixed-drawer.is-upgraded:not(.is-small-screen)>.mdl-layout__header{margin-left:240px;width:calc(100% - 240px)}@media screen and (min-width:1025px){.mdl-layout--fixed-drawer>.mdl-layout__header .mdl-layout__header-row{padding-left:40px}}.mdl-layout__header>.mdl-layout-icon{position:absolute;left:40px;top:16px;height:32px;width:32px;overflow:hidden;z-index:3;display:block}@media screen and (max-width:1024px){.mdl-layout__header>.mdl-layout-icon{left:16px;top:12px}}.mdl-layout.has-drawer .mdl-layout__header>.mdl-layout-icon{display:none}.mdl-layout__header.is-compact{max-height:64px}@media screen and (max-width:1024px){.mdl-layout__header.is-compact{max-height:56px}}.mdl-layout__header.is-compact.has-tabs{height:112px}@media screen and (max-width:1024px){.mdl-layout__header.is-compact.has-tabs{min-height:104px}}@media screen and (max-width:1024px){.mdl-layout__header{display:none}.mdl-layout--fixed-header>.mdl-layout__header{display:-webkit-flex;display:-ms-flexbox;display:flex}}.mdl-layout__header--transparent.mdl-layout__header--transparent{background-color:transparent;box-shadow:none}.mdl-layout__header--seamed,.mdl-layout__header--scroll{box-shadow:none}.mdl-layout__header--waterfall{box-shadow:none;overflow:hidden}.mdl-layout__header--waterfall.is-casting-shadow{box-shadow:0 2px 2px 0 rgba(0,0,0,.14),0 3px 1px -2px rgba(0,0,0,.2),0 1px 5px 0 rgba(0,0,0,.12)}.mdl-layout__header--waterfall.mdl-layout__header--waterfall-hide-top{-webkit-justify-content:flex-end;-ms-flex-pack:end;justify-content:flex-end}.mdl-layout__header-row{display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row;-webkit-flex-wrap:nowrap;-ms-flex-wrap:nowrap;flex-wrap:nowrap;-webkit-flex-shrink:0;-ms-flex-negative:0;flex-shrink:0;box-sizing:border-box;-webkit-align-self:stretch;-ms-flex-item-align:stretch;align-self:stretch;-webkit-align-items:center;-ms-flex-align:center;align-items:center;height:64px;margin:0;padding:0 40px 0 80px}.mdl-layout--no-drawer-button .mdl-layout__header-row{padding-left:40px}@media screen and (min-width:1025px){.mdl-layout--no-desktop-drawer-button .mdl-layout__header-row{padding-left:40px}}@media screen and (max-width:1024px){.mdl-layout__header-row{height:56px;padding:0 16px 0 72px}.mdl-layout--no-drawer-button .mdl-layout__header-row{padding-left:16px}}.mdl-layout__header-row>*{-webkit-flex-shrink:0;-ms-flex-negative:0;flex-shrink:0}.mdl-layout__header--scroll .mdl-layout__header-row{width:100%}.mdl-layout__header-row .mdl-navigation{margin:0;padding:0;height:64px;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row;-webkit-align-items:center;-ms-flex-align:center;align-items:center}@media screen and (max-width:1024px){.mdl-layout__header-row .mdl-navigation{height:56px}}.mdl-layout__header-row .mdl-navigation__link{display:block;color:rgb(66,66,66);line-height:64px;padding:0 24px}@media screen and (max-width:1024px){.mdl-layout__header-row .mdl-navigation__link{line-height:56px;padding:0 16px}}.mdl-layout__obfuscator{background-color:transparent;position:absolute;top:0;left:0;height:100%;width:100%;z-index:4;visibility:hidden;transition-property:background-color;transition-duration:.2s;transition-timing-function:cubic-bezier(.4,0,.2,1)}.mdl-layout__obfuscator.is-visible{background-color:rgba(0,0,0,.5);visibility:visible}@supports (pointer-events:auto){.mdl-layout__obfuscator{background-color:rgba(0,0,0,.5);opacity:0;transition-property:opacity;visibility:visible;pointer-events:none}.mdl-layout__obfuscator.is-visible{pointer-events:auto;opacity:1}}.mdl-layout__content{-ms-flex:0 1 auto;position:relative;display:inline-block;overflow-y:auto;overflow-x:hidden;-webkit-flex-grow:1;-ms-flex-positive:1;flex-grow:1;z-index:1;-webkit-overflow-scrolling:touch}.mdl-layout--fixed-drawer>.mdl-layout__content{margin-left:240px}.mdl-layout__container.has-scrolling-header .mdl-layout__content{overflow:visible}@media screen and (max-width:1024px){.mdl-layout--fixed-drawer>.mdl-layout__content{margin-left:0}.mdl-layout__container.has-scrolling-header .mdl-layout__content{overflow-y:auto;overflow-x:hidden}}.mdl-layout__tab-bar{height:96px;margin:0;width:calc(100% - 112px);padding:0 0 0 56px;display:-webkit-flex;display:-ms-flexbox;display:flex;background-color:rgb(158,158,158);overflow-y:hidden;overflow-x:scroll}.mdl-layout__tab-bar::-webkit-scrollbar{display:none}.mdl-layout--no-drawer-button .mdl-layout__tab-bar{padding-left:16px;width:calc(100% - 32px)}@media screen and (min-width:1025px){.mdl-layout--no-desktop-drawer-button .mdl-layout__tab-bar{padding-left:16px;width:calc(100% - 32px)}}@media screen and (max-width:1024px){.mdl-layout__tab-bar{width:calc(100% - 60px);padding:0 0 0 60px}.mdl-layout--no-drawer-button .mdl-layout__tab-bar{width:calc(100% - 8px);padding-left:4px}}.mdl-layout--fixed-tabs .mdl-layout__tab-bar{padding:0;overflow:hidden;width:100%}.mdl-layout__tab-bar-container{position:relative;height:48px;width:100%;border:none;margin:0;z-index:2;-webkit-flex-grow:0;-ms-flex-positive:0;flex-grow:0;-webkit-flex-shrink:0;-ms-flex-negative:0;flex-shrink:0;overflow:hidden}.mdl-layout__container>.mdl-layout__tab-bar-container{position:absolute;top:0;left:0}.mdl-layout__tab-bar-button{display:inline-block;position:absolute;top:0;height:48px;width:56px;z-index:4;text-align:center;background-color:rgb(158,158,158);color:transparent;cursor:pointer;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.mdl-layout--no-desktop-drawer-button .mdl-layout__tab-bar-button,.mdl-layout--no-drawer-button .mdl-layout__tab-bar-button{width:16px}.mdl-layout--no-desktop-drawer-button .mdl-layout__tab-bar-button .material-icons,.mdl-layout--no-drawer-button .mdl-layout__tab-bar-button .material-icons{position:relative;left:-4px}@media screen and (max-width:1024px){.mdl-layout__tab-bar-button{width:60px}}.mdl-layout--fixed-tabs .mdl-layout__tab-bar-button{display:none}.mdl-layout__tab-bar-button .material-icons{line-height:48px}.mdl-layout__tab-bar-button.is-active{color:rgb(66,66,66)}.mdl-layout__tab-bar-left-button{left:0}.mdl-layout__tab-bar-right-button{right:0}.mdl-layout__tab{margin:0;border:none;padding:0 24px;float:left;position:relative;display:block;-webkit-flex-grow:0;-ms-flex-positive:0;flex-grow:0;-webkit-flex-shrink:0;-ms-flex-negative:0;flex-shrink:0;text-decoration:none;height:48px;line-height:48px;text-align:center;font-weight:500;font-size:14px;text-transform:uppercase;color:rgba(66,66,66,.6);overflow:hidden}@media screen and (max-width:1024px){.mdl-layout__tab{padding:0 12px}}.mdl-layout--fixed-tabs .mdl-layout__tab{float:none;-webkit-flex-grow:1;-ms-flex-positive:1;flex-grow:1;padding:0}.mdl-layout.is-upgraded .mdl-layout__tab.is-active{color:rgb(66,66,66)}.mdl-layout.is-upgraded .mdl-layout__tab.is-active::after{height:2px;width:100%;display:block;content:" ";bottom:0;left:0;position:absolute;background:rgb(68,138,255);-webkit-animation:border-expand .2s cubic-bezier(.4,0,.4,1).01s alternate forwards;animation:border-expand .2s cubic-bezier(.4,0,.4,1).01s alternate forwards;transition:all 1s cubic-bezier(.4,0,1,1)}.mdl-layout__tab .mdl-layout__tab-ripple-container{display:block;position:absolute;height:100%;width:100%;left:0;top:0;z-index:1;overflow:hidden}.mdl-layout__tab .mdl-layout__tab-ripple-container .mdl-ripple{background-color:rgb(66,66,66)}.mdl-layout__tab-panel{display:block}.mdl-layout.is-upgraded .mdl-layout__tab-panel{display:none}.mdl-layout.is-upgraded .mdl-layout__tab-panel.is-active{display:block}.mdl-radio{position:relative;font-size:16px;line-height:24px;display:inline-block;vertical-align:middle;box-sizing:border-box;height:24px;margin:0;padding-left:0}.mdl-radio.is-upgraded{padding-left:24px}.mdl-radio__button{line-height:24px}.mdl-radio.is-upgraded .mdl-radio__button{position:absolute;width:0;height:0;margin:0;padding:0;opacity:0;-ms-appearance:none;-moz-appearance:none;-webkit-appearance:none;appearance:none;border:none}.mdl-radio__outer-circle{position:absolute;top:4px;left:0;display:inline-block;box-sizing:border-box;width:16px;height:16px;margin:0;cursor:pointer;border:2px solid rgba(0,0,0,.54);border-radius:50%;z-index:2}.mdl-radio.is-checked .mdl-radio__outer-circle{border:2px solid rgb(158,158,158)}.mdl-radio__outer-circle fieldset[disabled] .mdl-radio,.mdl-radio.is-disabled .mdl-radio__outer-circle{border:2px solid rgba(0,0,0,.26);cursor:auto}.mdl-radio__inner-circle{position:absolute;z-index:1;margin:0;top:8px;left:4px;box-sizing:border-box;width:8px;height:8px;cursor:pointer;transition-duration:.28s;transition-timing-function:cubic-bezier(.4,0,.2,1);transition-property:transform;transition-property:transform,-webkit-transform;-webkit-transform:scale(0,0);transform:scale(0,0);border-radius:50%;background:rgb(158,158,158)}.mdl-radio.is-checked .mdl-radio__inner-circle{-webkit-transform:scale(1,1);transform:scale(1,1)}fieldset[disabled] .mdl-radio .mdl-radio__inner-circle,.mdl-radio.is-disabled .mdl-radio__inner-circle{background:rgba(0,0,0,.26);cursor:auto}.mdl-radio.is-focused .mdl-radio__inner-circle{box-shadow:0 0 0 10px rgba(0,0,0,.1)}.mdl-radio__label{cursor:pointer}fieldset[disabled] .mdl-radio .mdl-radio__label,.mdl-radio.is-disabled .mdl-radio__label{color:rgba(0,0,0,.26);cursor:auto}.mdl-radio__ripple-container{position:absolute;z-index:2;top:-9px;left:-13px;box-sizing:border-box;width:42px;height:42px;border-radius:50%;cursor:pointer;overflow:hidden;-webkit-mask-image:-webkit-radial-gradient(circle,#fff,#000)}.mdl-radio__ripple-container .mdl-ripple{background:rgb(158,158,158)}fieldset[disabled] .mdl-radio .mdl-radio__ripple-container,.mdl-radio.is-disabled .mdl-radio__ripple-container{cursor:auto}fieldset[disabled] .mdl-radio .mdl-radio__ripple-container .mdl-ripple,.mdl-radio.is-disabled .mdl-radio__ripple-container .mdl-ripple{background:0 0}_:-ms-input-placeholder,:root .mdl-slider.mdl-slider.is-upgraded{-ms-appearance:none;height:32px;margin:0}.mdl-slider{width:calc(100% - 40px);margin:0 20px}.mdl-slider.is-upgraded{-webkit-appearance:none;-moz-appearance:none;appearance:none;height:2px;background:0 0;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;outline:0;padding:0;color:rgb(158,158,158);-webkit-align-self:center;-ms-flex-item-align:center;-ms-grid-row-align:center;align-self:center;z-index:1;cursor:pointer}.mdl-slider.is-upgraded::-moz-focus-outer{border:0}.mdl-slider.is-upgraded::-ms-tooltip{display:none}.mdl-slider.is-upgraded::-webkit-slider-runnable-track{background:0 0}.mdl-slider.is-upgraded::-moz-range-track{background:0 0;border:none}.mdl-slider.is-upgraded::-ms-track{background:0 0;color:transparent;height:2px;width:100%;border:none}.mdl-slider.is-upgraded::-ms-fill-lower{padding:0;background:linear-gradient(to right,transparent,transparent 16px,rgb(158,158,158)16px,rgb(158,158,158)0)}.mdl-slider.is-upgraded::-ms-fill-upper{padding:0;background:linear-gradient(to left,transparent,transparent 16px,rgba(0,0,0,.26)16px,rgba(0,0,0,.26)0)}.mdl-slider.is-upgraded::-webkit-slider-thumb{-webkit-appearance:none;width:12px;height:12px;box-sizing:border-box;border-radius:50%;background:rgb(158,158,158);border:none;transition:transform .18s cubic-bezier(.4,0,.2,1),border .18s cubic-bezier(.4,0,.2,1),box-shadow .18s cubic-bezier(.4,0,.2,1),background .28s cubic-bezier(.4,0,.2,1);transition:transform .18s cubic-bezier(.4,0,.2,1),border .18s cubic-bezier(.4,0,.2,1),box-shadow .18s cubic-bezier(.4,0,.2,1),background .28s cubic-bezier(.4,0,.2,1),-webkit-transform .18s cubic-bezier(.4,0,.2,1)}.mdl-slider.is-upgraded::-moz-range-thumb{-moz-appearance:none;width:12px;height:12px;box-sizing:border-box;border-radius:50%;background-image:none;background:rgb(158,158,158);border:none}.mdl-slider.is-upgraded:focus:not(:active)::-webkit-slider-thumb{box-shadow:0 0 0 10px rgba(158,158,158,.26)}.mdl-slider.is-upgraded:focus:not(:active)::-moz-range-thumb{box-shadow:0 0 0 10px rgba(158,158,158,.26)}.mdl-slider.is-upgraded:active::-webkit-slider-thumb{background-image:none;background:rgb(158,158,158);-webkit-transform:scale(1.5);transform:scale(1.5)}.mdl-slider.is-upgraded:active::-moz-range-thumb{background-image:none;background:rgb(158,158,158);transform:scale(1.5)}.mdl-slider.is-upgraded::-ms-thumb{width:32px;height:32px;border:none;border-radius:50%;background:rgb(158,158,158);transform:scale(.375);transition:transform .18s cubic-bezier(.4,0,.2,1),background .28s cubic-bezier(.4,0,.2,1);transition:transform .18s cubic-bezier(.4,0,.2,1),background .28s cubic-bezier(.4,0,.2,1),-webkit-transform .18s cubic-bezier(.4,0,.2,1)}.mdl-slider.is-upgraded:focus:not(:active)::-ms-thumb{background:radial-gradient(circle closest-side,rgb(158,158,158)0%,rgb(158,158,158)37.5%,rgba(158,158,158,.26)37.5%,rgba(158,158,158,.26)100%);transform:scale(1)}.mdl-slider.is-upgraded:active::-ms-thumb{background:rgb(158,158,158);transform:scale(.5625)}.mdl-slider.is-upgraded.is-lowest-value::-webkit-slider-thumb{border:2px solid rgba(0,0,0,.26);background:0 0}.mdl-slider.is-upgraded.is-lowest-value::-moz-range-thumb{border:2px solid rgba(0,0,0,.26);background:0 0}.mdl-slider.is-upgraded.is-lowest-value+.mdl-slider__background-flex>.mdl-slider__background-upper{left:6px}.mdl-slider.is-upgraded.is-lowest-value:focus:not(:active)::-webkit-slider-thumb{box-shadow:0 0 0 10px rgba(0,0,0,.12);background:rgba(0,0,0,.12)}.mdl-slider.is-upgraded.is-lowest-value:focus:not(:active)::-moz-range-thumb{box-shadow:0 0 0 10px rgba(0,0,0,.12);background:rgba(0,0,0,.12)}.mdl-slider.is-upgraded.is-lowest-value:active::-webkit-slider-thumb{border:1.6px solid rgba(0,0,0,.26);-webkit-transform:scale(1.5);transform:scale(1.5)}.mdl-slider.is-upgraded.is-lowest-value:active+.mdl-slider__background-flex>.mdl-slider__background-upper{left:9px}.mdl-slider.is-upgraded.is-lowest-value:active::-moz-range-thumb{border:1.5px solid rgba(0,0,0,.26);transform:scale(1.5)}.mdl-slider.is-upgraded.is-lowest-value::-ms-thumb{background:radial-gradient(circle closest-side,transparent 0%,transparent 66.67%,rgba(0,0,0,.26)66.67%,rgba(0,0,0,.26)100%)}.mdl-slider.is-upgraded.is-lowest-value:focus:not(:active)::-ms-thumb{background:radial-gradient(circle closest-side,rgba(0,0,0,.12)0%,rgba(0,0,0,.12)25%,rgba(0,0,0,.26)25%,rgba(0,0,0,.26)37.5%,rgba(0,0,0,.12)37.5%,rgba(0,0,0,.12)100%);transform:scale(1)}.mdl-slider.is-upgraded.is-lowest-value:active::-ms-thumb{transform:scale(.5625);background:radial-gradient(circle closest-side,transparent 0%,transparent 77.78%,rgba(0,0,0,.26)77.78%,rgba(0,0,0,.26)100%)}.mdl-slider.is-upgraded.is-lowest-value::-ms-fill-lower{background:0 0}.mdl-slider.is-upgraded.is-lowest-value::-ms-fill-upper{margin-left:6px}.mdl-slider.is-upgraded.is-lowest-value:active::-ms-fill-upper{margin-left:9px}.mdl-slider.is-upgraded:disabled:focus::-webkit-slider-thumb,.mdl-slider.is-upgraded:disabled:active::-webkit-slider-thumb,.mdl-slider.is-upgraded:disabled::-webkit-slider-thumb{-webkit-transform:scale(.667);transform:scale(.667);background:rgba(0,0,0,.26)}.mdl-slider.is-upgraded:disabled:focus::-moz-range-thumb,.mdl-slider.is-upgraded:disabled:active::-moz-range-thumb,.mdl-slider.is-upgraded:disabled::-moz-range-thumb{transform:scale(.667);background:rgba(0,0,0,.26)}.mdl-slider.is-upgraded:disabled+.mdl-slider__background-flex>.mdl-slider__background-lower{background-color:rgba(0,0,0,.26);left:-6px}.mdl-slider.is-upgraded:disabled+.mdl-slider__background-flex>.mdl-slider__background-upper{left:6px}.mdl-slider.is-upgraded.is-lowest-value:disabled:focus::-webkit-slider-thumb,.mdl-slider.is-upgraded.is-lowest-value:disabled:active::-webkit-slider-thumb,.mdl-slider.is-upgraded.is-lowest-value:disabled::-webkit-slider-thumb{border:3px solid rgba(0,0,0,.26);background:0 0;-webkit-transform:scale(.667);transform:scale(.667)}.mdl-slider.is-upgraded.is-lowest-value:disabled:focus::-moz-range-thumb,.mdl-slider.is-upgraded.is-lowest-value:disabled:active::-moz-range-thumb,.mdl-slider.is-upgraded.is-lowest-value:disabled::-moz-range-thumb{border:3px solid rgba(0,0,0,.26);background:0 0;transform:scale(.667)}.mdl-slider.is-upgraded.is-lowest-value:disabled:active+.mdl-slider__background-flex>.mdl-slider__background-upper{left:6px}.mdl-slider.is-upgraded:disabled:focus::-ms-thumb,.mdl-slider.is-upgraded:disabled:active::-ms-thumb,.mdl-slider.is-upgraded:disabled::-ms-thumb{transform:scale(.25);background:rgba(0,0,0,.26)}.mdl-slider.is-upgraded.is-lowest-value:disabled:focus::-ms-thumb,.mdl-slider.is-upgraded.is-lowest-value:disabled:active::-ms-thumb,.mdl-slider.is-upgraded.is-lowest-value:disabled::-ms-thumb{transform:scale(.25);background:radial-gradient(circle closest-side,transparent 0%,transparent 50%,rgba(0,0,0,.26)50%,rgba(0,0,0,.26)100%)}.mdl-slider.is-upgraded:disabled::-ms-fill-lower{margin-right:6px;background:linear-gradient(to right,transparent,transparent 25px,rgba(0,0,0,.26)25px,rgba(0,0,0,.26)0)}.mdl-slider.is-upgraded:disabled::-ms-fill-upper{margin-left:6px}.mdl-slider.is-upgraded.is-lowest-value:disabled:active::-ms-fill-upper{margin-left:6px}.mdl-slider__ie-container{height:18px;overflow:visible;border:none;margin:none;padding:none}.mdl-slider__container{height:18px;position:relative;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row}.mdl-slider__container,.mdl-slider__background-flex{background:0 0;display:-webkit-flex;display:-ms-flexbox;display:flex}.mdl-slider__background-flex{position:absolute;height:2px;width:calc(100% - 52px);top:50%;left:0;margin:0 26px;overflow:hidden;border:0;padding:0;-webkit-transform:translate(0,-1px);transform:translate(0,-1px)}.mdl-slider__background-lower{background:rgb(158,158,158)}.mdl-slider__background-lower,.mdl-slider__background-upper{-webkit-flex:0;-ms-flex:0;flex:0;position:relative;border:0;padding:0}.mdl-slider__background-upper{background:rgba(0,0,0,.26);transition:left .18s cubic-bezier(.4,0,.2,1)}.mdl-snackbar{position:fixed;bottom:0;left:50%;cursor:default;background-color:#323232;z-index:3;display:block;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-justify-content:space-between;-ms-flex-pack:justify;justify-content:space-between;font-family:"Roboto","Helvetica","Arial",sans-serif;will-change:transform;-webkit-transform:translate(0,80px);transform:translate(0,80px);transition:transform .25s cubic-bezier(.4,0,1,1);transition:transform .25s cubic-bezier(.4,0,1,1),-webkit-transform .25s cubic-bezier(.4,0,1,1);pointer-events:none}@media (max-width:479px){.mdl-snackbar{width:100%;left:0;min-height:48px;max-height:80px}}@media (min-width:480px){.mdl-snackbar{min-width:288px;max-width:568px;border-radius:2px;-webkit-transform:translate(-50%,80px);transform:translate(-50%,80px)}}.mdl-snackbar--active{-webkit-transform:translate(0,0);transform:translate(0,0);pointer-events:auto;transition:transform .25s cubic-bezier(0,0,.2,1);transition:transform .25s cubic-bezier(0,0,.2,1),-webkit-transform .25s cubic-bezier(0,0,.2,1)}@media (min-width:480px){.mdl-snackbar--active{-webkit-transform:translate(-50%,0);transform:translate(-50%,0)}}.mdl-snackbar__text{padding:14px 12px 14px 24px;vertical-align:middle;color:#fff;float:left}.mdl-snackbar__action{background:0 0;border:none;color:rgb(68,138,255);float:right;padding:14px 24px 14px 12px;font-family:"Roboto","Helvetica","Arial",sans-serif;font-size:14px;font-weight:500;text-transform:uppercase;line-height:1;letter-spacing:0;overflow:hidden;outline:none;opacity:0;pointer-events:none;cursor:pointer;text-decoration:none;text-align:center;-webkit-align-self:center;-ms-flex-item-align:center;-ms-grid-row-align:center;align-self:center}.mdl-snackbar__action::-moz-focus-inner{border:0}.mdl-snackbar__action:not([aria-hidden]){opacity:1;pointer-events:auto}.mdl-spinner{display:inline-block;position:relative;width:28px;height:28px}.mdl-spinner:not(.is-upgraded).is-active:after{content:"Loading..."}.mdl-spinner.is-upgraded.is-active{-webkit-animation:mdl-spinner__container-rotate 1568.23529412ms linear infinite;animation:mdl-spinner__container-rotate 1568.23529412ms linear infinite}@-webkit-keyframes mdl-spinner__container-rotate{to{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}@keyframes mdl-spinner__container-rotate{to{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}.mdl-spinner__layer{position:absolute;width:100%;height:100%;opacity:0}.mdl-spinner__layer-1{border-color:#42a5f5}.mdl-spinner--single-color .mdl-spinner__layer-1{border-color:rgb(158,158,158)}.mdl-spinner.is-active .mdl-spinner__layer-1{-webkit-animation:mdl-spinner__fill-unfill-rotate 5332ms cubic-bezier(.4,0,.2,1)infinite both,mdl-spinner__layer-1-fade-in-out 5332ms cubic-bezier(.4,0,.2,1)infinite both;animation:mdl-spinner__fill-unfill-rotate 5332ms cubic-bezier(.4,0,.2,1)infinite both,mdl-spinner__layer-1-fade-in-out 5332ms cubic-bezier(.4,0,.2,1)infinite both}.mdl-spinner__layer-2{border-color:#f44336}.mdl-spinner--single-color .mdl-spinner__layer-2{border-color:rgb(158,158,158)}.mdl-spinner.is-active .mdl-spinner__layer-2{-webkit-animation:mdl-spinner__fill-unfill-rotate 5332ms cubic-bezier(.4,0,.2,1)infinite both,mdl-spinner__layer-2-fade-in-out 5332ms cubic-bezier(.4,0,.2,1)infinite both;animation:mdl-spinner__fill-unfill-rotate 5332ms cubic-bezier(.4,0,.2,1)infinite both,mdl-spinner__layer-2-fade-in-out 5332ms cubic-bezier(.4,0,.2,1)infinite both}.mdl-spinner__layer-3{border-color:#fdd835}.mdl-spinner--single-color .mdl-spinner__layer-3{border-color:rgb(158,158,158)}.mdl-spinner.is-active .mdl-spinner__layer-3{-webkit-animation:mdl-spinner__fill-unfill-rotate 5332ms cubic-bezier(.4,0,.2,1)infinite both,mdl-spinner__layer-3-fade-in-out 5332ms cubic-bezier(.4,0,.2,1)infinite both;animation:mdl-spinner__fill-unfill-rotate 5332ms cubic-bezier(.4,0,.2,1)infinite both,mdl-spinner__layer-3-fade-in-out 5332ms cubic-bezier(.4,0,.2,1)infinite both}.mdl-spinner__layer-4{border-color:#4caf50}.mdl-spinner--single-color .mdl-spinner__layer-4{border-color:rgb(158,158,158)}.mdl-spinner.is-active .mdl-spinner__layer-4{-webkit-animation:mdl-spinner__fill-unfill-rotate 5332ms cubic-bezier(.4,0,.2,1)infinite both,mdl-spinner__layer-4-fade-in-out 5332ms cubic-bezier(.4,0,.2,1)infinite both;animation:mdl-spinner__fill-unfill-rotate 5332ms cubic-bezier(.4,0,.2,1)infinite both,mdl-spinner__layer-4-fade-in-out 5332ms cubic-bezier(.4,0,.2,1)infinite both}@-webkit-keyframes mdl-spinner__fill-unfill-rotate{12.5%{-webkit-transform:rotate(135deg);transform:rotate(135deg)}25%{-webkit-transform:rotate(270deg);transform:rotate(270deg)}37.5%{-webkit-transform:rotate(405deg);transform:rotate(405deg)}50%{-webkit-transform:rotate(540deg);transform:rotate(540deg)}62.5%{-webkit-transform:rotate(675deg);transform:rotate(675deg)}75%{-webkit-transform:rotate(810deg);transform:rotate(810deg)}87.5%{-webkit-transform:rotate(945deg);transform:rotate(945deg)}to{-webkit-transform:rotate(1080deg);transform:rotate(1080deg)}}@keyframes mdl-spinner__fill-unfill-rotate{12.5%{-webkit-transform:rotate(135deg);transform:rotate(135deg)}25%{-webkit-transform:rotate(270deg);transform:rotate(270deg)}37.5%{-webkit-transform:rotate(405deg);transform:rotate(405deg)}50%{-webkit-transform:rotate(540deg);transform:rotate(540deg)}62.5%{-webkit-transform:rotate(675deg);transform:rotate(675deg)}75%{-webkit-transform:rotate(810deg);transform:rotate(810deg)}87.5%{-webkit-transform:rotate(945deg);transform:rotate(945deg)}to{-webkit-transform:rotate(1080deg);transform:rotate(1080deg)}}@-webkit-keyframes mdl-spinner__layer-1-fade-in-out{from,25%{opacity:.99}26%,89%{opacity:0}90%,100%{opacity:.99}}@keyframes mdl-spinner__layer-1-fade-in-out{from,25%{opacity:.99}26%,89%{opacity:0}90%,100%{opacity:.99}}@-webkit-keyframes mdl-spinner__layer-2-fade-in-out{from,15%{opacity:0}25%,50%{opacity:.99}51%{opacity:0}}@keyframes mdl-spinner__layer-2-fade-in-out{from,15%{opacity:0}25%,50%{opacity:.99}51%{opacity:0}}@-webkit-keyframes mdl-spinner__layer-3-fade-in-out{from,40%{opacity:0}50%,75%{opacity:.99}76%{opacity:0}}@keyframes mdl-spinner__layer-3-fade-in-out{from,40%{opacity:0}50%,75%{opacity:.99}76%{opacity:0}}@-webkit-keyframes mdl-spinner__layer-4-fade-in-out{from,65%{opacity:0}75%,90%{opacity:.99}100%{opacity:0}}@keyframes mdl-spinner__layer-4-fade-in-out{from,65%{opacity:0}75%,90%{opacity:.99}100%{opacity:0}}.mdl-spinner__gap-patch{position:absolute;box-sizing:border-box;top:0;left:45%;width:10%;height:100%;overflow:hidden;border-color:inherit}.mdl-spinner__gap-patch .mdl-spinner__circle{width:1000%;left:-450%}.mdl-spinner__circle-clipper{display:inline-block;position:relative;width:50%;height:100%;overflow:hidden;border-color:inherit}.mdl-spinner__circle-clipper.mdl-spinner__left{float:left}.mdl-spinner__circle-clipper.mdl-spinner__right{float:right}.mdl-spinner__circle-clipper .mdl-spinner__circle{width:200%}.mdl-spinner__circle{box-sizing:border-box;height:100%;border-width:3px;border-style:solid;border-color:inherit;border-bottom-color:transparent!important;border-radius:50%;-webkit-animation:none;animation:none;position:absolute;top:0;right:0;bottom:0;left:0}.mdl-spinner__left .mdl-spinner__circle{border-right-color:transparent!important;-webkit-transform:rotate(129deg);transform:rotate(129deg)}.mdl-spinner.is-active .mdl-spinner__left .mdl-spinner__circle{-webkit-animation:mdl-spinner__left-spin 1333ms cubic-bezier(.4,0,.2,1)infinite both;animation:mdl-spinner__left-spin 1333ms cubic-bezier(.4,0,.2,1)infinite both}.mdl-spinner__right .mdl-spinner__circle{left:-100%;border-left-color:transparent!important;-webkit-transform:rotate(-129deg);transform:rotate(-129deg)}.mdl-spinner.is-active .mdl-spinner__right .mdl-spinner__circle{-webkit-animation:mdl-spinner__right-spin 1333ms cubic-bezier(.4,0,.2,1)infinite both;animation:mdl-spinner__right-spin 1333ms cubic-bezier(.4,0,.2,1)infinite both}@-webkit-keyframes mdl-spinner__left-spin{from{-webkit-transform:rotate(130deg);transform:rotate(130deg)}50%{-webkit-transform:rotate(-5deg);transform:rotate(-5deg)}to{-webkit-transform:rotate(130deg);transform:rotate(130deg)}}@keyframes mdl-spinner__left-spin{from{-webkit-transform:rotate(130deg);transform:rotate(130deg)}50%{-webkit-transform:rotate(-5deg);transform:rotate(-5deg)}to{-webkit-transform:rotate(130deg);transform:rotate(130deg)}}@-webkit-keyframes mdl-spinner__right-spin{from{-webkit-transform:rotate(-130deg);transform:rotate(-130deg)}50%{-webkit-transform:rotate(5deg);transform:rotate(5deg)}to{-webkit-transform:rotate(-130deg);transform:rotate(-130deg)}}@keyframes mdl-spinner__right-spin{from{-webkit-transform:rotate(-130deg);transform:rotate(-130deg)}50%{-webkit-transform:rotate(5deg);transform:rotate(5deg)}to{-webkit-transform:rotate(-130deg);transform:rotate(-130deg)}}.mdl-switch{position:relative;z-index:1;vertical-align:middle;display:inline-block;box-sizing:border-box;width:100%;height:24px;margin:0;padding:0;overflow:visible;-webkit-touch-callout:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.mdl-switch.is-upgraded{padding-left:28px}.mdl-switch__input{line-height:24px}.mdl-switch.is-upgraded .mdl-switch__input{position:absolute;width:0;height:0;margin:0;padding:0;opacity:0;-ms-appearance:none;-moz-appearance:none;-webkit-appearance:none;appearance:none;border:none}.mdl-switch__track{background:rgba(0,0,0,.26);position:absolute;left:0;top:5px;height:14px;width:36px;border-radius:14px;cursor:pointer}.mdl-switch.is-checked .mdl-switch__track{background:rgba(158,158,158,.5)}.mdl-switch__track fieldset[disabled] .mdl-switch,.mdl-switch.is-disabled .mdl-switch__track{background:rgba(0,0,0,.12);cursor:auto}.mdl-switch__thumb{background:#fafafa;position:absolute;left:0;top:2px;height:20px;width:20px;border-radius:50%;cursor:pointer;box-shadow:0 2px 2px 0 rgba(0,0,0,.14),0 3px 1px -2px rgba(0,0,0,.2),0 1px 5px 0 rgba(0,0,0,.12);transition-duration:.28s;transition-timing-function:cubic-bezier(.4,0,.2,1);transition-property:left}.mdl-switch.is-checked .mdl-switch__thumb{background:rgb(158,158,158);left:16px;box-shadow:0 3px 4px 0 rgba(0,0,0,.14),0 3px 3px -2px rgba(0,0,0,.2),0 1px 8px 0 rgba(0,0,0,.12)}.mdl-switch__thumb fieldset[disabled] .mdl-switch,.mdl-switch.is-disabled .mdl-switch__thumb{background:#bdbdbd;cursor:auto}.mdl-switch__focus-helper{position:absolute;top:50%;left:50%;-webkit-transform:translate(-4px,-4px);transform:translate(-4px,-4px);display:inline-block;box-sizing:border-box;width:8px;height:8px;border-radius:50%;background-color:transparent}.mdl-switch.is-focused .mdl-switch__focus-helper{box-shadow:0 0 0 20px rgba(0,0,0,.1);background-color:rgba(0,0,0,.1)}.mdl-switch.is-focused.is-checked .mdl-switch__focus-helper{box-shadow:0 0 0 20px rgba(158,158,158,.26);background-color:rgba(158,158,158,.26)}.mdl-switch__label{position:relative;cursor:pointer;font-size:16px;line-height:24px;margin:0;left:24px}.mdl-switch__label fieldset[disabled] .mdl-switch,.mdl-switch.is-disabled .mdl-switch__label{color:#bdbdbd;cursor:auto}.mdl-switch__ripple-container{position:absolute;z-index:2;top:-12px;left:-14px;box-sizing:border-box;width:48px;height:48px;border-radius:50%;cursor:pointer;overflow:hidden;-webkit-mask-image:-webkit-radial-gradient(circle,#fff,#000);transition-duration:.4s;transition-timing-function:step-end;transition-property:left}.mdl-switch__ripple-container .mdl-ripple{background:rgb(158,158,158)}.mdl-switch__ripple-container fieldset[disabled] .mdl-switch,.mdl-switch.is-disabled .mdl-switch__ripple-container{cursor:auto}fieldset[disabled] .mdl-switch .mdl-switch__ripple-container .mdl-ripple,.mdl-switch.is-disabled .mdl-switch__ripple-container .mdl-ripple{background:0 0}.mdl-switch.is-checked .mdl-switch__ripple-container{left:2px}.mdl-tabs{display:block;width:100%}.mdl-tabs__tab-bar{display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-align-content:space-between;-ms-flex-line-pack:justify;align-content:space-between;-webkit-align-items:flex-start;-ms-flex-align:start;align-items:flex-start;height:48px;padding:0;margin:0;border-bottom:1px solid #e0e0e0}.mdl-tabs__tab{margin:0;border:none;padding:0 24px;float:left;position:relative;display:block;text-decoration:none;height:48px;line-height:48px;text-align:center;font-weight:500;font-size:14px;text-transform:uppercase;color:rgba(0,0,0,.54);overflow:hidden}.mdl-tabs.is-upgraded .mdl-tabs__tab.is-active{color:rgba(0,0,0,.87)}.mdl-tabs.is-upgraded .mdl-tabs__tab.is-active:after{height:2px;width:100%;display:block;content:" ";bottom:0;left:0;position:absolute;background:rgb(158,158,158);-webkit-animation:border-expand .2s cubic-bezier(.4,0,.4,1).01s alternate forwards;animation:border-expand .2s cubic-bezier(.4,0,.4,1).01s alternate forwards;transition:all 1s cubic-bezier(.4,0,1,1)}.mdl-tabs__tab .mdl-tabs__ripple-container{display:block;position:absolute;height:100%;width:100%;left:0;top:0;z-index:1;overflow:hidden}.mdl-tabs__tab .mdl-tabs__ripple-container .mdl-ripple{background:rgb(158,158,158)}.mdl-tabs__panel{display:block}.mdl-tabs.is-upgraded .mdl-tabs__panel{display:none}.mdl-tabs.is-upgraded .mdl-tabs__panel.is-active{display:block}@-webkit-keyframes border-expand{0%{opacity:0;width:0}100%{opacity:1;width:100%}}@keyframes border-expand{0%{opacity:0;width:0}100%{opacity:1;width:100%}}.mdl-textfield{position:relative;font-size:16px;display:inline-block;box-sizing:border-box;width:300px;max-width:100%;margin:0;padding:20px 0}.mdl-textfield .mdl-button{position:absolute;bottom:20px}.mdl-textfield--align-right{text-align:right}.mdl-textfield--full-width{width:100%}.mdl-textfield--expandable{min-width:32px;width:auto;min-height:32px}.mdl-textfield--expandable .mdl-button--icon{top:16px}.mdl-textfield__input{border:none;border-bottom:1px solid rgba(0,0,0,.12);display:block;font-size:16px;font-family:"Helvetica","Arial",sans-serif;margin:0;padding:4px 0;width:100%;background:0 0;text-align:left;color:inherit}.mdl-textfield__input[type="number"]{-moz-appearance:textfield}.mdl-textfield__input[type="number"]::-webkit-inner-spin-button,.mdl-textfield__input[type="number"]::-webkit-outer-spin-button{-webkit-appearance:none;margin:0}.mdl-textfield.is-focused .mdl-textfield__input{outline:none}.mdl-textfield.is-invalid .mdl-textfield__input{border-color:#d50000;box-shadow:none}fieldset[disabled] .mdl-textfield .mdl-textfield__input,.mdl-textfield.is-disabled .mdl-textfield__input{background-color:transparent;border-bottom:1px dotted rgba(0,0,0,.12);color:rgba(0,0,0,.26)}.mdl-textfield textarea.mdl-textfield__input{display:block}.mdl-textfield__label{bottom:0;color:rgba(0,0,0,.26);font-size:16px;left:0;right:0;pointer-events:none;position:absolute;display:block;top:24px;width:100%;overflow:hidden;white-space:nowrap;text-align:left}.mdl-textfield.is-dirty .mdl-textfield__label,.mdl-textfield.has-placeholder .mdl-textfield__label{visibility:hidden}.mdl-textfield--floating-label .mdl-textfield__label{transition-duration:.2s;transition-timing-function:cubic-bezier(.4,0,.2,1)}.mdl-textfield--floating-label.has-placeholder .mdl-textfield__label{transition:none}fieldset[disabled] .mdl-textfield .mdl-textfield__label,.mdl-textfield.is-disabled.is-disabled .mdl-textfield__label{color:rgba(0,0,0,.26)}.mdl-textfield--floating-label.is-focused .mdl-textfield__label,.mdl-textfield--floating-label.is-dirty .mdl-textfield__label,.mdl-textfield--floating-label.has-placeholder .mdl-textfield__label{color:rgb(158,158,158);font-size:12px;top:4px;visibility:visible}.mdl-textfield--floating-label.is-focused .mdl-textfield__expandable-holder .mdl-textfield__label,.mdl-textfield--floating-label.is-dirty .mdl-textfield__expandable-holder .mdl-textfield__label,.mdl-textfield--floating-label.has-placeholder .mdl-textfield__expandable-holder .mdl-textfield__label{top:-16px}.mdl-textfield--floating-label.is-invalid .mdl-textfield__label{color:#d50000;font-size:12px}.mdl-textfield__label:after{background-color:rgb(158,158,158);bottom:20px;content:'';height:2px;left:45%;position:absolute;transition-duration:.2s;transition-timing-function:cubic-bezier(.4,0,.2,1);visibility:hidden;width:10px}.mdl-textfield.is-focused .mdl-textfield__label:after{left:0;visibility:visible;width:100%}.mdl-textfield.is-invalid .mdl-textfield__label:after{background-color:#d50000}.mdl-textfield__error{color:#d50000;position:absolute;font-size:12px;margin-top:3px;visibility:hidden;display:block}.mdl-textfield.is-invalid .mdl-textfield__error{visibility:visible}.mdl-textfield__expandable-holder{display:inline-block;position:relative;margin-left:32px;transition-duration:.2s;transition-timing-function:cubic-bezier(.4,0,.2,1);display:inline-block;max-width:.1px}.mdl-textfield.is-focused .mdl-textfield__expandable-holder,.mdl-textfield.is-dirty .mdl-textfield__expandable-holder{max-width:600px}.mdl-textfield__expandable-holder .mdl-textfield__label:after{bottom:0}.mdl-tooltip{-webkit-transform:scale(0);transform:scale(0);-webkit-transform-origin:top center;transform-origin:top center;z-index:999;background:rgba(97,97,97,.9);border-radius:2px;color:#fff;display:inline-block;font-size:10px;font-weight:500;line-height:14px;max-width:170px;position:fixed;top:-500px;left:-500px;padding:8px;text-align:center}.mdl-tooltip.is-active{-webkit-animation:pulse 200ms cubic-bezier(0,0,.2,1)forwards;animation:pulse 200ms cubic-bezier(0,0,.2,1)forwards}.mdl-tooltip--large{line-height:14px;font-size:14px;padding:16px}@-webkit-keyframes pulse{0%{-webkit-transform:scale(0);transform:scale(0);opacity:0}50%{-webkit-transform:scale(.99);transform:scale(.99)}100%{-webkit-transform:scale(1);transform:scale(1);opacity:1;visibility:visible}}@keyframes pulse{0%{-webkit-transform:scale(0);transform:scale(0);opacity:0}50%{-webkit-transform:scale(.99);transform:scale(.99)}100%{-webkit-transform:scale(1);transform:scale(1);opacity:1;visibility:visible}}.mdl-shadow--2dp{box-shadow:0 2px 2px 0 rgba(0,0,0,.14),0 3px 1px -2px rgba(0,0,0,.2),0 1px 5px 0 rgba(0,0,0,.12)}.mdl-shadow--3dp{box-shadow:0 3px 4px 0 rgba(0,0,0,.14),0 3px 3px -2px rgba(0,0,0,.2),0 1px 8px 0 rgba(0,0,0,.12)}.mdl-shadow--4dp{box-shadow:0 4px 5px 0 rgba(0,0,0,.14),0 1px 10px 0 rgba(0,0,0,.12),0 2px 4px -1px rgba(0,0,0,.2)}.mdl-shadow--6dp{box-shadow:0 6px 10px 0 rgba(0,0,0,.14),0 1px 18px 0 rgba(0,0,0,.12),0 3px 5px -1px rgba(0,0,0,.2)}.mdl-shadow--8dp{box-shadow:0 8px 10px 1px rgba(0,0,0,.14),0 3px 14px 2px rgba(0,0,0,.12),0 5px 5px -3px rgba(0,0,0,.2)}.mdl-shadow--16dp{box-shadow:0 16px 24px 2px rgba(0,0,0,.14),0 6px 30px 5px rgba(0,0,0,.12),0 8px 10px -5px rgba(0,0,0,.2)}.mdl-shadow--24dp{box-shadow:0 9px 46px 8px rgba(0,0,0,.14),0 11px 15px -7px rgba(0,0,0,.12),0 24px 38px 3px rgba(0,0,0,.2)}.mdl-grid{display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-flow:row wrap;-ms-flex-flow:row wrap;flex-flow:row wrap;margin:0 auto;-webkit-align-items:stretch;-ms-flex-align:stretch;align-items:stretch}.mdl-grid.mdl-grid--no-spacing{padding:0}.mdl-cell{box-sizing:border-box}.mdl-cell--top{-webkit-align-self:flex-start;-ms-flex-item-align:start;align-self:flex-start}.mdl-cell--middle{-webkit-align-self:center;-ms-flex-item-align:center;-ms-grid-row-align:center;align-self:center}.mdl-cell--bottom{-webkit-align-self:flex-end;-ms-flex-item-align:end;align-self:flex-end}.mdl-cell--stretch{-webkit-align-self:stretch;-ms-flex-item-align:stretch;-ms-grid-row-align:stretch;align-self:stretch}.mdl-grid.mdl-grid--no-spacing>.mdl-cell{margin:0}.mdl-cell--order-1{-webkit-order:1;-ms-flex-order:1;order:1}.mdl-cell--order-2{-webkit-order:2;-ms-flex-order:2;order:2}.mdl-cell--order-3{-webkit-order:3;-ms-flex-order:3;order:3}.mdl-cell--order-4{-webkit-order:4;-ms-flex-order:4;order:4}.mdl-cell--order-5{-webkit-order:5;-ms-flex-order:5;order:5}.mdl-cell--order-6{-webkit-order:6;-ms-flex-order:6;order:6}.mdl-cell--order-7{-webkit-order:7;-ms-flex-order:7;order:7}.mdl-cell--order-8{-webkit-order:8;-ms-flex-order:8;order:8}.mdl-cell--order-9{-webkit-order:9;-ms-flex-order:9;order:9}.mdl-cell--order-10{-webkit-order:10;-ms-flex-order:10;order:10}.mdl-cell--order-11{-webkit-order:11;-ms-flex-order:11;order:11}.mdl-cell--order-12{-webkit-order:12;-ms-flex-order:12;order:12}@media (max-width:479px){.mdl-grid{padding:8px}.mdl-cell{margin:8px;width:calc(100% - 16px)}.mdl-grid--no-spacing>.mdl-cell{width:100%}.mdl-cell--hide-phone{display:none!important}.mdl-cell--order-1-phone.mdl-cell--order-1-phone{-webkit-order:1;-ms-flex-order:1;order:1}.mdl-cell--order-2-phone.mdl-cell--order-2-phone{-webkit-order:2;-ms-flex-order:2;order:2}.mdl-cell--order-3-phone.mdl-cell--order-3-phone{-webkit-order:3;-ms-flex-order:3;order:3}.mdl-cell--order-4-phone.mdl-cell--order-4-phone{-webkit-order:4;-ms-flex-order:4;order:4}.mdl-cell--order-5-phone.mdl-cell--order-5-phone{-webkit-order:5;-ms-flex-order:5;order:5}.mdl-cell--order-6-phone.mdl-cell--order-6-phone{-webkit-order:6;-ms-flex-order:6;order:6}.mdl-cell--order-7-phone.mdl-cell--order-7-phone{-webkit-order:7;-ms-flex-order:7;order:7}.mdl-cell--order-8-phone.mdl-cell--order-8-phone{-webkit-order:8;-ms-flex-order:8;order:8}.mdl-cell--order-9-phone.mdl-cell--order-9-phone{-webkit-order:9;-ms-flex-order:9;order:9}.mdl-cell--order-10-phone.mdl-cell--order-10-phone{-webkit-order:10;-ms-flex-order:10;order:10}.mdl-cell--order-11-phone.mdl-cell--order-11-phone{-webkit-order:11;-ms-flex-order:11;order:11}.mdl-cell--order-12-phone.mdl-cell--order-12-phone{-webkit-order:12;-ms-flex-order:12;order:12}.mdl-cell--1-col,.mdl-cell--1-col-phone.mdl-cell--1-col-phone{width:calc(25% - 16px)}.mdl-grid--no-spacing>.mdl-cell--1-col,.mdl-grid--no-spacing>.mdl-cell--1-col-phone.mdl-cell--1-col-phone{width:25%}.mdl-cell--2-col,.mdl-cell--2-col-phone.mdl-cell--2-col-phone{width:calc(50% - 16px)}.mdl-grid--no-spacing>.mdl-cell--2-col,.mdl-grid--no-spacing>.mdl-cell--2-col-phone.mdl-cell--2-col-phone{width:50%}.mdl-cell--3-col,.mdl-cell--3-col-phone.mdl-cell--3-col-phone{width:calc(75% - 16px)}.mdl-grid--no-spacing>.mdl-cell--3-col,.mdl-grid--no-spacing>.mdl-cell--3-col-phone.mdl-cell--3-col-phone{width:75%}.mdl-cell--4-col,.mdl-cell--4-col-phone.mdl-cell--4-col-phone{width:calc(100% - 16px)}.mdl-grid--no-spacing>.mdl-cell--4-col,.mdl-grid--no-spacing>.mdl-cell--4-col-phone.mdl-cell--4-col-phone{width:100%}.mdl-cell--5-col,.mdl-cell--5-col-phone.mdl-cell--5-col-phone{width:calc(100% - 16px)}.mdl-grid--no-spacing>.mdl-cell--5-col,.mdl-grid--no-spacing>.mdl-cell--5-col-phone.mdl-cell--5-col-phone{width:100%}.mdl-cell--6-col,.mdl-cell--6-col-phone.mdl-cell--6-col-phone{width:calc(100% - 16px)}.mdl-grid--no-spacing>.mdl-cell--6-col,.mdl-grid--no-spacing>.mdl-cell--6-col-phone.mdl-cell--6-col-phone{width:100%}.mdl-cell--7-col,.mdl-cell--7-col-phone.mdl-cell--7-col-phone{width:calc(100% - 16px)}.mdl-grid--no-spacing>.mdl-cell--7-col,.mdl-grid--no-spacing>.mdl-cell--7-col-phone.mdl-cell--7-col-phone{width:100%}.mdl-cell--8-col,.mdl-cell--8-col-phone.mdl-cell--8-col-phone{width:calc(100% - 16px)}.mdl-grid--no-spacing>.mdl-cell--8-col,.mdl-grid--no-spacing>.mdl-cell--8-col-phone.mdl-cell--8-col-phone{width:100%}.mdl-cell--9-col,.mdl-cell--9-col-phone.mdl-cell--9-col-phone{width:calc(100% - 16px)}.mdl-grid--no-spacing>.mdl-cell--9-col,.mdl-grid--no-spacing>.mdl-cell--9-col-phone.mdl-cell--9-col-phone{width:100%}.mdl-cell--10-col,.mdl-cell--10-col-phone.mdl-cell--10-col-phone{width:calc(100% - 16px)}.mdl-grid--no-spacing>.mdl-cell--10-col,.mdl-grid--no-spacing>.mdl-cell--10-col-phone.mdl-cell--10-col-phone{width:100%}.mdl-cell--11-col,.mdl-cell--11-col-phone.mdl-cell--11-col-phone{width:calc(100% - 16px)}.mdl-grid--no-spacing>.mdl-cell--11-col,.mdl-grid--no-spacing>.mdl-cell--11-col-phone.mdl-cell--11-col-phone{width:100%}.mdl-cell--12-col,.mdl-cell--12-col-phone.mdl-cell--12-col-phone{width:calc(100% - 16px)}.mdl-grid--no-spacing>.mdl-cell--12-col,.mdl-grid--no-spacing>.mdl-cell--12-col-phone.mdl-cell--12-col-phone{width:100%}.mdl-cell--1-offset,.mdl-cell--1-offset-phone.mdl-cell--1-offset-phone{margin-left:calc(25% + 8px)}.mdl-grid.mdl-grid--no-spacing>.mdl-cell--1-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--1-offset-phone.mdl-cell--1-offset-phone{margin-left:25%}.mdl-cell--2-offset,.mdl-cell--2-offset-phone.mdl-cell--2-offset-phone{margin-left:calc(50% + 8px)}.mdl-grid.mdl-grid--no-spacing>.mdl-cell--2-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--2-offset-phone.mdl-cell--2-offset-phone{margin-left:50%}.mdl-cell--3-offset,.mdl-cell--3-offset-phone.mdl-cell--3-offset-phone{margin-left:calc(75% + 8px)}.mdl-grid.mdl-grid--no-spacing>.mdl-cell--3-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--3-offset-phone.mdl-cell--3-offset-phone{margin-left:75%}}@media (min-width:480px) and (max-width:839px){.mdl-grid{padding:8px}.mdl-cell{margin:8px;width:calc(50% - 16px)}.mdl-grid--no-spacing>.mdl-cell{width:50%}.mdl-cell--hide-tablet{display:none!important}.mdl-cell--order-1-tablet.mdl-cell--order-1-tablet{-webkit-order:1;-ms-flex-order:1;order:1}.mdl-cell--order-2-tablet.mdl-cell--order-2-tablet{-webkit-order:2;-ms-flex-order:2;order:2}.mdl-cell--order-3-tablet.mdl-cell--order-3-tablet{-webkit-order:3;-ms-flex-order:3;order:3}.mdl-cell--order-4-tablet.mdl-cell--order-4-tablet{-webkit-order:4;-ms-flex-order:4;order:4}.mdl-cell--order-5-tablet.mdl-cell--order-5-tablet{-webkit-order:5;-ms-flex-order:5;order:5}.mdl-cell--order-6-tablet.mdl-cell--order-6-tablet{-webkit-order:6;-ms-flex-order:6;order:6}.mdl-cell--order-7-tablet.mdl-cell--order-7-tablet{-webkit-order:7;-ms-flex-order:7;order:7}.mdl-cell--order-8-tablet.mdl-cell--order-8-tablet{-webkit-order:8;-ms-flex-order:8;order:8}.mdl-cell--order-9-tablet.mdl-cell--order-9-tablet{-webkit-order:9;-ms-flex-order:9;order:9}.mdl-cell--order-10-tablet.mdl-cell--order-10-tablet{-webkit-order:10;-ms-flex-order:10;order:10}.mdl-cell--order-11-tablet.mdl-cell--order-11-tablet{-webkit-order:11;-ms-flex-order:11;order:11}.mdl-cell--order-12-tablet.mdl-cell--order-12-tablet{-webkit-order:12;-ms-flex-order:12;order:12}.mdl-cell--1-col,.mdl-cell--1-col-tablet.mdl-cell--1-col-tablet{width:calc(12.5% - 16px)}.mdl-grid--no-spacing>.mdl-cell--1-col,.mdl-grid--no-spacing>.mdl-cell--1-col-tablet.mdl-cell--1-col-tablet{width:12.5%}.mdl-cell--2-col,.mdl-cell--2-col-tablet.mdl-cell--2-col-tablet{width:calc(25% - 16px)}.mdl-grid--no-spacing>.mdl-cell--2-col,.mdl-grid--no-spacing>.mdl-cell--2-col-tablet.mdl-cell--2-col-tablet{width:25%}.mdl-cell--3-col,.mdl-cell--3-col-tablet.mdl-cell--3-col-tablet{width:calc(37.5% - 16px)}.mdl-grid--no-spacing>.mdl-cell--3-col,.mdl-grid--no-spacing>.mdl-cell--3-col-tablet.mdl-cell--3-col-tablet{width:37.5%}.mdl-cell--4-col,.mdl-cell--4-col-tablet.mdl-cell--4-col-tablet{width:calc(50% - 16px)}.mdl-grid--no-spacing>.mdl-cell--4-col,.mdl-grid--no-spacing>.mdl-cell--4-col-tablet.mdl-cell--4-col-tablet{width:50%}.mdl-cell--5-col,.mdl-cell--5-col-tablet.mdl-cell--5-col-tablet{width:calc(62.5% - 16px)}.mdl-grid--no-spacing>.mdl-cell--5-col,.mdl-grid--no-spacing>.mdl-cell--5-col-tablet.mdl-cell--5-col-tablet{width:62.5%}.mdl-cell--6-col,.mdl-cell--6-col-tablet.mdl-cell--6-col-tablet{width:calc(75% - 16px)}.mdl-grid--no-spacing>.mdl-cell--6-col,.mdl-grid--no-spacing>.mdl-cell--6-col-tablet.mdl-cell--6-col-tablet{width:75%}.mdl-cell--7-col,.mdl-cell--7-col-tablet.mdl-cell--7-col-tablet{width:calc(87.5% - 16px)}.mdl-grid--no-spacing>.mdl-cell--7-col,.mdl-grid--no-spacing>.mdl-cell--7-col-tablet.mdl-cell--7-col-tablet{width:87.5%}.mdl-cell--8-col,.mdl-cell--8-col-tablet.mdl-cell--8-col-tablet{width:calc(100% - 16px)}.mdl-grid--no-spacing>.mdl-cell--8-col,.mdl-grid--no-spacing>.mdl-cell--8-col-tablet.mdl-cell--8-col-tablet{width:100%}.mdl-cell--9-col,.mdl-cell--9-col-tablet.mdl-cell--9-col-tablet{width:calc(100% - 16px)}.mdl-grid--no-spacing>.mdl-cell--9-col,.mdl-grid--no-spacing>.mdl-cell--9-col-tablet.mdl-cell--9-col-tablet{width:100%}.mdl-cell--10-col,.mdl-cell--10-col-tablet.mdl-cell--10-col-tablet{width:calc(100% - 16px)}.mdl-grid--no-spacing>.mdl-cell--10-col,.mdl-grid--no-spacing>.mdl-cell--10-col-tablet.mdl-cell--10-col-tablet{width:100%}.mdl-cell--11-col,.mdl-cell--11-col-tablet.mdl-cell--11-col-tablet{width:calc(100% - 16px)}.mdl-grid--no-spacing>.mdl-cell--11-col,.mdl-grid--no-spacing>.mdl-cell--11-col-tablet.mdl-cell--11-col-tablet{width:100%}.mdl-cell--12-col,.mdl-cell--12-col-tablet.mdl-cell--12-col-tablet{width:calc(100% - 16px)}.mdl-grid--no-spacing>.mdl-cell--12-col,.mdl-grid--no-spacing>.mdl-cell--12-col-tablet.mdl-cell--12-col-tablet{width:100%}.mdl-cell--1-offset,.mdl-cell--1-offset-tablet.mdl-cell--1-offset-tablet{margin-left:calc(12.5% + 8px)}.mdl-grid.mdl-grid--no-spacing>.mdl-cell--1-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--1-offset-tablet.mdl-cell--1-offset-tablet{margin-left:12.5%}.mdl-cell--2-offset,.mdl-cell--2-offset-tablet.mdl-cell--2-offset-tablet{margin-left:calc(25% + 8px)}.mdl-grid.mdl-grid--no-spacing>.mdl-cell--2-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--2-offset-tablet.mdl-cell--2-offset-tablet{margin-left:25%}.mdl-cell--3-offset,.mdl-cell--3-offset-tablet.mdl-cell--3-offset-tablet{margin-left:calc(37.5% + 8px)}.mdl-grid.mdl-grid--no-spacing>.mdl-cell--3-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--3-offset-tablet.mdl-cell--3-offset-tablet{margin-left:37.5%}.mdl-cell--4-offset,.mdl-cell--4-offset-tablet.mdl-cell--4-offset-tablet{margin-left:calc(50% + 8px)}.mdl-grid.mdl-grid--no-spacing>.mdl-cell--4-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--4-offset-tablet.mdl-cell--4-offset-tablet{margin-left:50%}.mdl-cell--5-offset,.mdl-cell--5-offset-tablet.mdl-cell--5-offset-tablet{margin-left:calc(62.5% + 8px)}.mdl-grid.mdl-grid--no-spacing>.mdl-cell--5-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--5-offset-tablet.mdl-cell--5-offset-tablet{margin-left:62.5%}.mdl-cell--6-offset,.mdl-cell--6-offset-tablet.mdl-cell--6-offset-tablet{margin-left:calc(75% + 8px)}.mdl-grid.mdl-grid--no-spacing>.mdl-cell--6-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--6-offset-tablet.mdl-cell--6-offset-tablet{margin-left:75%}.mdl-cell--7-offset,.mdl-cell--7-offset-tablet.mdl-cell--7-offset-tablet{margin-left:calc(87.5% + 8px)}.mdl-grid.mdl-grid--no-spacing>.mdl-cell--7-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--7-offset-tablet.mdl-cell--7-offset-tablet{margin-left:87.5%}}@media (min-width:840px){.mdl-grid{padding:8px}.mdl-cell{margin:8px;width:calc(33.3333333333% - 16px)}.mdl-grid--no-spacing>.mdl-cell{width:33.3333333333%}.mdl-cell--hide-desktop{display:none!important}.mdl-cell--order-1-desktop.mdl-cell--order-1-desktop{-webkit-order:1;-ms-flex-order:1;order:1}.mdl-cell--order-2-desktop.mdl-cell--order-2-desktop{-webkit-order:2;-ms-flex-order:2;order:2}.mdl-cell--order-3-desktop.mdl-cell--order-3-desktop{-webkit-order:3;-ms-flex-order:3;order:3}.mdl-cell--order-4-desktop.mdl-cell--order-4-desktop{-webkit-order:4;-ms-flex-order:4;order:4}.mdl-cell--order-5-desktop.mdl-cell--order-5-desktop{-webkit-order:5;-ms-flex-order:5;order:5}.mdl-cell--order-6-desktop.mdl-cell--order-6-desktop{-webkit-order:6;-ms-flex-order:6;order:6}.mdl-cell--order-7-desktop.mdl-cell--order-7-desktop{-webkit-order:7;-ms-flex-order:7;order:7}.mdl-cell--order-8-desktop.mdl-cell--order-8-desktop{-webkit-order:8;-ms-flex-order:8;order:8}.mdl-cell--order-9-desktop.mdl-cell--order-9-desktop{-webkit-order:9;-ms-flex-order:9;order:9}.mdl-cell--order-10-desktop.mdl-cell--order-10-desktop{-webkit-order:10;-ms-flex-order:10;order:10}.mdl-cell--order-11-desktop.mdl-cell--order-11-desktop{-webkit-order:11;-ms-flex-order:11;order:11}.mdl-cell--order-12-desktop.mdl-cell--order-12-desktop{-webkit-order:12;-ms-flex-order:12;order:12}.mdl-cell--1-col,.mdl-cell--1-col-desktop.mdl-cell--1-col-desktop{width:calc(8.3333333333% - 16px)}.mdl-grid--no-spacing>.mdl-cell--1-col,.mdl-grid--no-spacing>.mdl-cell--1-col-desktop.mdl-cell--1-col-desktop{width:8.3333333333%}.mdl-cell--2-col,.mdl-cell--2-col-desktop.mdl-cell--2-col-desktop{width:calc(16.6666666667% - 16px)}.mdl-grid--no-spacing>.mdl-cell--2-col,.mdl-grid--no-spacing>.mdl-cell--2-col-desktop.mdl-cell--2-col-desktop{width:16.6666666667%}.mdl-cell--3-col,.mdl-cell--3-col-desktop.mdl-cell--3-col-desktop{width:calc(25% - 16px)}.mdl-grid--no-spacing>.mdl-cell--3-col,.mdl-grid--no-spacing>.mdl-cell--3-col-desktop.mdl-cell--3-col-desktop{width:25%}.mdl-cell--4-col,.mdl-cell--4-col-desktop.mdl-cell--4-col-desktop{width:calc(33.3333333333% - 16px)}.mdl-grid--no-spacing>.mdl-cell--4-col,.mdl-grid--no-spacing>.mdl-cell--4-col-desktop.mdl-cell--4-col-desktop{width:33.3333333333%}.mdl-cell--5-col,.mdl-cell--5-col-desktop.mdl-cell--5-col-desktop{width:calc(41.6666666667% - 16px)}.mdl-grid--no-spacing>.mdl-cell--5-col,.mdl-grid--no-spacing>.mdl-cell--5-col-desktop.mdl-cell--5-col-desktop{width:41.6666666667%}.mdl-cell--6-col,.mdl-cell--6-col-desktop.mdl-cell--6-col-desktop{width:calc(50% - 16px)}.mdl-grid--no-spacing>.mdl-cell--6-col,.mdl-grid--no-spacing>.mdl-cell--6-col-desktop.mdl-cell--6-col-desktop{width:50%}.mdl-cell--7-col,.mdl-cell--7-col-desktop.mdl-cell--7-col-desktop{width:calc(58.3333333333% - 16px)}.mdl-grid--no-spacing>.mdl-cell--7-col,.mdl-grid--no-spacing>.mdl-cell--7-col-desktop.mdl-cell--7-col-desktop{width:58.3333333333%}.mdl-cell--8-col,.mdl-cell--8-col-desktop.mdl-cell--8-col-desktop{width:calc(66.6666666667% - 16px)}.mdl-grid--no-spacing>.mdl-cell--8-col,.mdl-grid--no-spacing>.mdl-cell--8-col-desktop.mdl-cell--8-col-desktop{width:66.6666666667%}.mdl-cell--9-col,.mdl-cell--9-col-desktop.mdl-cell--9-col-desktop{width:calc(75% - 16px)}.mdl-grid--no-spacing>.mdl-cell--9-col,.mdl-grid--no-spacing>.mdl-cell--9-col-desktop.mdl-cell--9-col-desktop{width:75%}.mdl-cell--10-col,.mdl-cell--10-col-desktop.mdl-cell--10-col-desktop{width:calc(83.3333333333% - 16px)}.mdl-grid--no-spacing>.mdl-cell--10-col,.mdl-grid--no-spacing>.mdl-cell--10-col-desktop.mdl-cell--10-col-desktop{width:83.3333333333%}.mdl-cell--11-col,.mdl-cell--11-col-desktop.mdl-cell--11-col-desktop{width:calc(91.6666666667% - 16px)}.mdl-grid--no-spacing>.mdl-cell--11-col,.mdl-grid--no-spacing>.mdl-cell--11-col-desktop.mdl-cell--11-col-desktop{width:91.6666666667%}.mdl-cell--12-col,.mdl-cell--12-col-desktop.mdl-cell--12-col-desktop{width:calc(100% - 16px)}.mdl-grid--no-spacing>.mdl-cell--12-col,.mdl-grid--no-spacing>.mdl-cell--12-col-desktop.mdl-cell--12-col-desktop{width:100%}.mdl-cell--1-offset,.mdl-cell--1-offset-desktop.mdl-cell--1-offset-desktop{margin-left:calc(8.3333333333% + 8px)}.mdl-grid.mdl-grid--no-spacing>.mdl-cell--1-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--1-offset-desktop.mdl-cell--1-offset-desktop{margin-left:8.3333333333%}.mdl-cell--2-offset,.mdl-cell--2-offset-desktop.mdl-cell--2-offset-desktop{margin-left:calc(16.6666666667% + 8px)}.mdl-grid.mdl-grid--no-spacing>.mdl-cell--2-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--2-offset-desktop.mdl-cell--2-offset-desktop{margin-left:16.6666666667%}.mdl-cell--3-offset,.mdl-cell--3-offset-desktop.mdl-cell--3-offset-desktop{margin-left:calc(25% + 8px)}.mdl-grid.mdl-grid--no-spacing>.mdl-cell--3-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--3-offset-desktop.mdl-cell--3-offset-desktop{margin-left:25%}.mdl-cell--4-offset,.mdl-cell--4-offset-desktop.mdl-cell--4-offset-desktop{margin-left:calc(33.3333333333% + 8px)}.mdl-grid.mdl-grid--no-spacing>.mdl-cell--4-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--4-offset-desktop.mdl-cell--4-offset-desktop{margin-left:33.3333333333%}.mdl-cell--5-offset,.mdl-cell--5-offset-desktop.mdl-cell--5-offset-desktop{margin-left:calc(41.6666666667% + 8px)}.mdl-grid.mdl-grid--no-spacing>.mdl-cell--5-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--5-offset-desktop.mdl-cell--5-offset-desktop{margin-left:41.6666666667%}.mdl-cell--6-offset,.mdl-cell--6-offset-desktop.mdl-cell--6-offset-desktop{margin-left:calc(50% + 8px)}.mdl-grid.mdl-grid--no-spacing>.mdl-cell--6-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--6-offset-desktop.mdl-cell--6-offset-desktop{margin-left:50%}.mdl-cell--7-offset,.mdl-cell--7-offset-desktop.mdl-cell--7-offset-desktop{margin-left:calc(58.3333333333% + 8px)}.mdl-grid.mdl-grid--no-spacing>.mdl-cell--7-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--7-offset-desktop.mdl-cell--7-offset-desktop{margin-left:58.3333333333%}.mdl-cell--8-offset,.mdl-cell--8-offset-desktop.mdl-cell--8-offset-desktop{margin-left:calc(66.6666666667% + 8px)}.mdl-grid.mdl-grid--no-spacing>.mdl-cell--8-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--8-offset-desktop.mdl-cell--8-offset-desktop{margin-left:66.6666666667%}.mdl-cell--9-offset,.mdl-cell--9-offset-desktop.mdl-cell--9-offset-desktop{margin-left:calc(75% + 8px)}.mdl-grid.mdl-grid--no-spacing>.mdl-cell--9-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--9-offset-desktop.mdl-cell--9-offset-desktop{margin-left:75%}.mdl-cell--10-offset,.mdl-cell--10-offset-desktop.mdl-cell--10-offset-desktop{margin-left:calc(83.3333333333% + 8px)}.mdl-grid.mdl-grid--no-spacing>.mdl-cell--10-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--10-offset-desktop.mdl-cell--10-offset-desktop{margin-left:83.3333333333%}.mdl-cell--11-offset,.mdl-cell--11-offset-desktop.mdl-cell--11-offset-desktop{margin-left:calc(91.6666666667% + 8px)}.mdl-grid.mdl-grid--no-spacing>.mdl-cell--11-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--11-offset-desktop.mdl-cell--11-offset-desktop{margin-left:91.6666666667%}}body{margin:0}.styleguide-demo h1{margin:48px 24px 0}.styleguide-demo h1:after{content:'';display:block;width:100%;border-bottom:1px solid rgba(0,0,0,.5);margin-top:24px}.styleguide-demo{opacity:0;transition:opacity .6s ease}.styleguide-masthead{height:256px;background:#212121;padding:115px 16px 0}.styleguide-container{position:relative;max-width:960px;width:100%}.styleguide-title{color:#fff;bottom:auto;position:relative;font-size:56px;font-weight:300;line-height:1;letter-spacing:-.02em}.styleguide-title:after{border-bottom:0}.styleguide-title span{font-weight:300}.mdl-styleguide .mdl-layout__drawer .mdl-navigation__link{padding:10px 24px}.demosLoaded .styleguide-demo{opacity:1}iframe{display:block;width:100%;border:none}iframe.heightSet{overflow:hidden}.demo-wrapper{margin:24px}.demo-wrapper iframe{border:1px solid rgba(0,0,0,.5)}</style>
 <script></script>


 <script type="text/javascript" data-savepage-src="/lib/idle-timer/idle-timer.js?siteVersion=1.0.6" src=""></script>
 <script type="text/javascript" data-savepage-src="/js/global.js?siteVersion=1.0.6" src=""></script>
 <script type="text/javascript" data-savepage-src="/js/jquery.chunkedReader.js?siteVersion=1.0.6" src=""></script>
 <script type="text/javascript" data-savepage-src="/js/xapi.js?siteVersion=1.0.6" src=""></script>
 <script type="text/javascript" data-savepage-src="/js/fb_check.js?siteVersion=1.0.6" src=""></script>

 <script type="text/javascript"></script>
 <style type="text/css">
 /*savepage-import-url=/css/mdl/root.css.php?siteVersion=1.0.6*/ @charset "utf-8";
 /* CSS Document */
 /*savepage-import-url=global.css?siteVersion=1.0.6*/@charset "utf-8";
 /* CSS Document */

 body, div, dl, dt, dd, ul, ol, li, h1, h2, h3, h4, h5, h6, pre, code, form, fieldset, legend, input, textarea, p, blockquote, th, td {
 	margin: 0;
 	padding: 0;
 }

 body, h1, h2, h3, h4, h5, h6,dl, dt, dd, ul, ol{
 	line-height: 1.22em;
 }

 body, div, dl, dt, dd, ul, ol, li, pre, code, form, fieldset, legend, input, textarea, p, blockquote, th, td {
 	font-size: 12px;
 }

 body {
 	-x-system-font: none;
 	font-family: Roboto,"Helvetica Neue",Helvetica,Arial,clean, sans-serif;
 	font-size: 12px;
 	font-size-adjust: none;
 	font-stretch: normal;
 	font-style: normal;
 	font-variant: normal;
 	font-weight: normal;
 	line-height: 1.22;
 	color: #000;
 	-webkit-text-size-adjust:none;
 }

 img {
 	border: 0px;
 }

 a {
 	text-decoration: none;
 }

 a:hover {
 	text-decoration: underline;
 }

 img {
 	border: 0px;
 }

 li {
 	list-style-image: none;
 	list-style-position: outside;
 	list-style-type: none;
 }

 .table-responsive { min-height: .01%; overflow-x: auto; } @media screen and (max-width: 767px) { .table-responsive { width: 100%; margin-bottom: 15px; overflow-y: hidden; -ms-overflow-style: -ms-autohiding-scrollbar; border: 1px solid #ddd; } .table-responsive>.table { margin-bottom: 0; } .table-responsive>.table>thead>tr>th, .table-responsive>.table>tbody>tr>th, .table-responsive>.table>tfoot>tr>th, .table-responsive>.table>thead>tr>td, .table-responsive>.table>tbody>tr>td, .table-responsive>.table>tfoot>tr>td { white-space: nowrap; } .table-responsive>.table-bordered { border: 0; } .table-responsive>.table-bordered>thead>tr>th:first-child, .table-responsive>.table-bordered>tbody>tr>th:first-child, .table-responsive>.table-bordered>tfoot>tr>th:first-child, .table-responsive>.table-bordered>thead>tr>td:first-child, .table-responsive>.table-bordered>tbody>tr>td:first-child, .table-responsive>.table-bordered>tfoot>tr>td:first-child { border-left: 0; } .table-responsive>.table-bordered>thead>tr>th:last-child, .table-responsive>.table-bordered>tbody>tr>th:last-child, .table-responsive>.table-bordered>tfoot>tr>th:last-child, .table-responsive>.table-bordered>thead>tr>td:last-child, .table-responsive>.table-bordered>tbody>tr>td:last-child, .table-responsive>.table-bordered>tfoot>tr>td:last-child { border-right: 0; } .table-responsive>.table-bordered>tbody>tr:last-child>th, .table-responsive>.table-bordered>tfoot>tr:last-child>th, .table-responsive>.table-bordered>tbody>tr:last-child>td, .table-responsive>.table-bordered>tfoot>tr:last-child>td { border-bottom: 0; } }

 /*savepage-import-url=layout.css?siteVersion=1.0.6*/@charset "utf-8";
 /* CSS Document */

/*
        __  ___   ____    __
       /  |/  /  / __ \  / /
      / /|_/ /  / / / / / /
     / /  / /_ / /_/ / / /____
    /_/  /_/(_)_____(_)_____(_)
    MDL.Layout
    Author: Malek
    Date: 2017-04-09
    Basic Pay layout
    NOTE: THis is trying to confirm with the legacy DOM
    THis may change in the future once all applications are moved over to this
    */


/*
     _  _ ___   _   ___  ___ ___
    | || | __| /_\ |   \| __| _ \
    | __ | _| / _ \| |) | _||   /
    |_||_|___/_/ \_\___/|___|_|_\

    */


    #hd {
    	position: relative;
    	z-index: 5000;
    	width: 100%;
    	color: #000;
    }

    #hd {
    	top: 0px;
    }

    #header {

    	margin: 0px auto;
    	padding: 5px 34px 0 38px;

    }

    #banner {
    	padding: 0;
    	position: relative;
    	padding-bottom: 0px;
    	font-family: "Roboto","Helvetica","Arial",sans-serif !important;
    }

    #navigation {
    	border: none;
    	width: auto;
    	min-width: 410px;
    	margin-top: 2px;
    	display: block;
    }

    #banner h1 {
    	font-size: 34px;
    	line-height: 3em;
    	left: 0;
    	margin: 0;
    	min-height: 24px;
    	padding: 0;
    	top: 0;
    	white-space: nowrap;
    	display: block;
    	font-weight: 300;
    }

    #banner h1 a {
    	margin-right: 10px;
    	min-height: 20px;
    	font-weight: 400;
    }

    #navigation ul {
    	margin: 0;
    	padding: 0;
    	list-style: none;
    	position: relative;
    	font-size: 12px;
    }

    #navigation .nav-list {
    	padding: 2px 0 0 0;
    	white-space: nowrap;
    	max-width: 100%;
    }

    #navigation li {
    	white-space: initial;
    	margin: 0;
    	padding: 0;
    	list-style: none;
    	position: relative;
    	margin-right: 0px;
    	display: inline-block;
    	text-transform: uppercase;
    }

    #navigation  .nav-list > li > a{
    	display: block;
    	text-align: center;
    	line-height: 4em;
    	border-bottom: 3px solid transparent;
    	padding: 0px 2.5em;
    }

/*
     ___ _   _ ___   _  _   ___   __
    / __| | | | _ ) | \| | /_\ \ / /
    \__ \ |_| | _ \ | .` |/ _ \ V /
    |___/\___/|___/ |_|\_/_/ \_\_/

    */


    #navigation ul li div.sub {
    	display: none;
    }

    #navigation ul li div.sub ul li {
    	position: relative;
    	width: 100%;
    	margin-right: 0px;
    	text-align: left;
    }

    #navigation ul li div.sub  ul li a {
    	color: #FFF;
    	padding: 5px 10px;
    	font-weight: bold;
    	min-width: 150px;
    	width: auto;
    	display: block;
    	border-bottom: 1px dotted #7F7F7F;
    	white-space: nowrap;
    	text-align: left;
    	line-height: 2em;
    }


    #navigation ul li div.sub {
    	display: none;
    }

    #navigation ul li div.sub ul li {
    	position: relative;
    	width: 100%;
    	margin-right: 0px;
    	text-align: left;
    }

    #navigation ul li div.sub  ul li a > span {
    	display: block;
    	line-height: 1em;
    	font-size: 0.8em;
    	font-weight: normal;
    }

    #navigation ul li:hover div.sub {
    	display: block;
    }

    #navigation ul li div.sub a:hover {
    	text-decoration: none;
    }

    #navigation ul li div.sub ul {
    	position: absolute;
    	margin-left: 0px;
    }


    #navigation ul li div.sub .mini_view {
    	padding-left: 35px;
    	min-width: 150px;
    	line-height: 1.22em;
    	padding-bottom: 0px;
    }
    #navigation ul li div.sub .site_notify{
    	position: absolute;
    	right: 6px;
    	top: 14px;
    }


    #navigation ul li div.sub .mini_view.roundme img{
    	border-radius: 50%;
    	position: absolute;
    	margin-left: -40px;
    }

    #navigation ul li div.sub .mini_view img {
    	position: absolute;
    	margin-left: -40px;
    }

    #navigation ul li div.sub .mini_view .result_title {
    	display: block;
    }

    #navigation ul li div.sub .mini_view .result_title span {

    	font-size: 10px;
    	display: block;
    	color: #DDD;
    	margin-bottom: 4px;
    }


    #navigation ul li div.sub .mini_view span.fakelink{
    	color: #DDD;
    	font-weight: normal;
    	float: right;
    	margin-left: 5px;
    	font-size: 10px;
    }

/*
      ___ _    ___  ___   _   _      _  _   ___   __
     / __| |  / _ \| _ ) /_\ | |    | \| | /_\ \ / /
    | (_ | |_| (_) | _ \/ _ \| |__  | .` |/ _ \ V /
     \___|____\___/|___/_/ \_\____| |_|\_/_/ \_\_/

     */



     #globalnav {
     	padding-top: 0px;
     	text-align: right;
     	padding-bottom: 1px;
     	white-space: nowrap;
     	font-size: 12px;
     	max-width: 100%;
     }

     #globalnav > li {
     	display: inline-block;
     	text-align: center;
     	vertical-align: top;
     	line-height: 3em;
     	font-size: 12px;
     	padding: 0px 0.8em;
     }

     #globalnav > li a{
     	display: block;
     }

     #globalnav > li img {
     	height: 20px;
     	margin-right: 3px;
     }

     #globalnav > li.lang_holder strong{
     	text-decoration: underline;
     }

/* #globalnav > li:last-child {
    text-align: right;
    padding-right: 0px;
    } */



/*
     ___ ___   ___ _____ ___ ___
    | __/ _ \ / _ \_   _| __| _ \
    | _| (_) | (_) || | | _||   /
    |_| \___/ \___/ |_| |___|_|_\

    */


    #ft {
    	clear: both;
    	padding: 50px 20px 80px;
    	margin-top: 50px;
    	font-family: "Roboto","Helvetica","Arial",sans-serif !important;
    }


    #ft #footernavigation ul li.first {
    	border-left: medium none;
    }

    #ft #footernavigation ul li {
    	display: inline-block;
    	vertical-align: top;
    	line-height: 1.5em;
    	list-style-image: none;
    	list-style-position: outside;
    	list-style-type: none;
    	padding-left: 0.5em;
    	padding-right: 0.1em;
    	opacity: 0.8;
    }


    #ft #footernavigation ul li a {
    	font-weight: normal;
    	padding: 0 4px;
    }
    #ft #footernavigation > a{
    	display: inline-block;
    	font-size: 14px;
    	vertical-align: top;
    	line-height: 1.5em;
    	margin-right: 0.5em;
    }

    #ft #footernavigation ul{
    	display: inline-block;
    	vertical-align: top;
    }


/*
     ___ ___   _   ___  ___ _  _
    / __| __| /_\ | _ \/ __| || |
    \__ \ _| / _ \|   / (__| __ |
    |___/___/_/ \_\_|_\\___|_||_|

    */


    .search-box {
    	position: absolute;
    	right: 0px;
    	top: 2px;
    }

    #search-field {
    	width: 232px;
    	border: 0px;
    	font-size: 20px;
    	height: 25px;
    	position: absolute;
    	right: 25px;
    	top: 0px;
    	padding-left: 4px;
    }

    #search-button {
    	width: 25px;
    	height: 25px;
    	background: #7F7F7F;
    	position: absolute;
    	right: 0px;
    	top: 0px;
    	cursor: pointer;
    }

    #search-button:after {
    	position: absolute;
    	left: 0px;
    	right: 0px;
    	top: 0px;
    	bottom: 0px;
    	display: block;
    	content: "\e90b";
    	font-family: 'zaiko-icons' !important;
    	speak: none;
    	font-style: normal;
    	font-weight: normal;
    	font-variant: normal;
    	text-transform: none;
    	line-height: 1;
    	/* Better Font Rendering =========== */
    	-webkit-font-smoothing: antialiased;
    	-moz-osx-font-smoothing: grayscale;
    	text-align: center;
    	line-height: 25px;
    	font-size: 16px;
    }

    #search-button.selected {
    	background-color: #3274D0;
    }

    #search-button:hover {
    	background-color: #F84713;
    }


/*
     ___ __  __   _   _    _      __  __ ___ _  _ _   _
    / __|  \/  | /_\ | |  | |    |  \/  | __| \| | | | |
    \__ \ |\/| |/ _ \| |__| |__  | |\/| | _|| .` | |_| |
    |___/_|  |_/_/ \_\____|____| |_|  |_|___|_|\_|\___/

    */



    .offtop #hd {
    	position: fixed;
    	box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);
    	height: 60px;
    }

    .offtop #banner h1{
    	font-size: 30px;
    	line-height: 2em;
    	position: absolute;
    	top: -5px;
    	left: 25px;
    }

    .offtop #banner h1 a{
    	font-weight: 400;
    }


    .offtop #navigation{
    	margin-top: -2px;
    }

    @media (min-width: 980px){

    	.offtop #pg{
    		margin-top: 212px;
    	}

    }


    .offtop .nav-list-holder,
    .offtop #globalnav-holder
    {
    	display: block;
    	position: fixed !important;
    	left: -300px;
    	top: 0px;
    	background: #FFF;
    	width: 300px;
    	bottom: 0px;
    	overflow-y: auto;
    	-webkit-overflow-scrolling:touch;
    	transition: 0.3s;
    	z-index: 10;
    }

    .offtop #globalnav-holder,
    .offtop #globalnav
    {
    	left: initial;
    	right: -300px;
    }



    .offtop #navigation li,
    .offtop #globalnav > li{
    	display: block;
    }

    .offtop #navigation li a,
    .offtop #globalnav > li{
    	line-height: 4em;
    	min-height: 2.5em;
    }

    .offtop #globalnav > li{
    	text-align: left;
    	padding: 0px;
    	font-weight: bold;
    }


    .offtop #globalnav > li a{
    	padding: 5px 10px;
    }

    .offtop #globalnav > li.lang_holder  strong{
    	padding: 5px 10px;
    }

    .offtop #navigation ul li div.sub ul{
    	position: relative;
    }

    .offtop #navigation ul li div.sub{
    	display: block;
    }

    .offtop #navigation .nav-list > li > a{
    	border-bottom: 0px;
    	border-right: 5px solid transparent;
    	text-align: left;
    	margin-left: 10px;
    	position: relative;
    }

    .offtop  #navigation .nav-list{
    	padding-top: 0px;
    }

    .offtop #navigation .sub li {
    	padding-left: 3px;
    	box-sizing: border-box;
    	border-left: 10px solid #DDD;
    }

    .offtop #navigation_button {
    	display: inline-block;
    	width: 37px;
    	height: 34px;
    	position: relative;
    	top: 7px;
    	left: -25px;
    }

    .offtop #navigation_button:after {
    	position: absolute;
    	left: 0px;
    	right: 0px;
    	top: 0px;
    	bottom: 0px;
    	display: block;
    	content: "\e5D2";
    	font-family: 'Material Icons';
    	speak: none;
    	font-style: normal;
    	font-weight: normal;
    	font-variant: normal;
    	text-transform: none;
    	line-height: 1;
    	-webkit-font-smoothing: antialiased;
    	-moz-osx-font-smoothing: grayscale;
    	text-align: center;
    	line-height: 34px;
    	font-size: 34px;
    }

    .offtop #banner.active .nav-list-holder{
    	left: 0px; 
    }

    .offtop .active #navigation_button{
    	position: fixed;
    	top: 0px;
    	right: 0px;
    	left: 0px;
    	bottom: 0px;
    	background: rgba(0,0,0,0.8);
    	width: auto;
    	height: auto;
    	z-index: 10;
    }

    .offtop .active #navigation_button:after {
    	display: none;
    }

    .offtop .search-box{
    	display: none;
    }

    .offtop #search_button_holder{
    	display: inline-block;
    	width: 37px;
    	height: 34px;
    	position: absolute;
    	top: 7px;
    	right: 25px;
    	transition:  0.2s;
    }

    .offtop #search_button_holder:after {
    	position: absolute;
    	left: 0px;
    	right: 0px;
    	top: 0px;
    	bottom: 0px;
    	display: block;
    	content: "\e8b6";
    	font-family: 'Material Icons';
    	speak: none;
    	font-style: normal;
    	font-weight: normal;
    	font-variant: normal;
    	text-transform: none;
    	line-height: 1;
    	-webkit-font-smoothing: antialiased;
    	-moz-osx-font-smoothing: grayscale;
    	text-align: center;
    	line-height: 34px;
    	font-size: 34px;
    }

    .offtop #globalnav_button{
    	display: inline-block;
    	width: 37px;
    	height: 34px;
    	position: absolute;
    	top: 12px;
    	right: 10px;
    	transition: 0.2s;
    }

    .offtop #globalnav_button:after {
    	position: absolute;
    	left: 0px;
    	right: 0px;
    	top: 0px;
    	bottom: 0px;
    	display: block;
    	content: "\e5D4";
    	font-family: 'Material Icons';
    	speak: none;
    	font-style: normal;
    	font-weight: normal;
    	font-variant: normal;
    	text-transform: none;
    	line-height: 1;
    	-webkit-font-smoothing: antialiased;
    	-moz-osx-font-smoothing: grayscale;
    	text-align: center;
    	line-height: 34px;
    	font-size: 34px;
    }

    .offtop .globalnav_active #globalnav_button{
    	position: fixed;
    	top: 0px;
    	right: 0px;
    	left: 0px;
    	bottom: 0px;
    	background: rgba(0,0,0,0.8);
    	width: auto;
    	height: auto;
    	z-index: 10;
    }

    .offtop .globalnav_active #globalnav_button:after {
    	display: none;
    }

    .offtop .globalnav_active #globalnav-holder{
    	right: 0px; 
    }

    .offtop #banner.search_active #search_button_holder{
    	background: rgba(0,0,0,0.2);

    }

    .offtop #banner.search_active #search_button_holder:after{
    	color: #FFF;
    	content: "\e5CD";
    }

    .offtop #banner.search_active .search-box{
    	display: block;
    	position: absolute;
    	top: 55px;
    	background: #333;
    	left: -38px;
    	right: -34px;
    	width: auto;
    	height: 50px;
    	box-sizing: border-box;
    	border-right: 50px solid #333;
    }

    .offtop #search-field {
    	font-size: 18px;
    	height: 22px;
    	right: 22px;
    }

    .offtop #search-button {
    	width: 40px;
    	height: 40px;
    	font-size:  14px;
    	right: -45px;
    	top: 5px;
    }
    
    .offtop #search-button:after{
    	line-height: 43px;
    	font-size: 25px;
    }

    .offtop  #search-field{
    	left: 5px;
    	width: 100%;
    	height: 40px;
    	background: #EFEFEF;
    	margin-right: 40px;
    	box-sizing: border-box;
    	top: 5px;
    	font-size: 30px;
    }

    .offtop #navigation ul li div.sub.colors__p, .offtop #navigation ul li div.sub .colors__p{
    	background-color: transparent !important;
    	color: #000 !important;
    }

    .offtop #navigation ul li div.sub ul li a{
    	border-bottom:  none;
    }

    #navigation ul li div.sub ul li a .icon{
    	float:  right;
    	font-size: 1.3em;
    }

    @media only screen and (min-width: 980px){
    	.offtop.no_sitenav #banner h1{
    		left: -10px;
    		margin-top: 2px;
    	}
    }


    .no_sitenav #navigation_button{
    	display: none !important;
    }

    @media only screen and (max-width: 979px){ 

    	#hd {
    		position: fixed;
    		box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);
    		height: 60px;
    	}

    	#banner h1{
    		font-size: 30px;
    		line-height: 2em;
    		position: absolute;
    		top: -5px;
    		left: 25px;
    	}

    	#banner h1 a{
    		font-weight: 400;
    	}

    	.no_sitenav #banner h1{
    		left: -15px;
    		top: -3px;
    	}


    	#navigation{
    		margin-top: -2px;
    	}

    	#pg{
    		margin-top: 60px;
    	}

    	.nav-list-holder,
    	#globalnav-holder
    	{
    		display: block;
    		position: fixed !important;
    		left: -300px;
    		top: 0px;
    		background: #FFF;
    		width: 300px;
    		bottom: 0px;
    		overflow-y: auto;
    		-webkit-overflow-scrolling:touch;
    		transition: 0.3s;
    		z-index: 10;
    	}

    	#globalnav-holder,
    	#globalnav
    	{
    		left: initial;
    		right: -300px;
    	}



    	#navigation li,
    	#globalnav > li{
    		display: block;
    	}

    	#navigation li a,
    	#globalnav > li{
    		line-height: 4em;
    		min-height: 2.5em;
    	}

    	#globalnav > li{
    		text-align: left;
    		padding: 0px;
    		font-weight: bold;
    	}


    	#globalnav > li a{
    		padding: 5px 10px;
    	}

    	#globalnav > li.lang_holder  strong{
    		padding: 5px 10px;
    	}

    	#navigation ul li div.sub ul{
    		position: relative;
    	}

    	#navigation ul li div.sub{
    		display: block;
    	}

    	#navigation .nav-list > li > a{
    		border-bottom: 0px;
    		border-right: 5px solid transparent;
    		text-align: left;
    		margin-left: 10px;
    		position: relative;
    	}

    	#navigation .nav-list{
    		padding-top: 0px;
    	}

    	#navigation .sub li {
    		padding-left: 3px;
    		box-sizing: border-box;
    		border-left: 10px solid #FFF;
    	}

    	#navigation_button {
    		display: inline-block;
    		width: 37px;
    		height: 34px;
    		position: relative;
    		top: 7px;
    		left: -25px;
    	}

    	#navigation_button:after {
    		position: absolute;
    		left: 0px;
    		right: 0px;
    		top: 0px;
    		bottom: 0px;
    		display: block;
    		content: "\e5D2";
    		font-family: 'Material Icons';
    		speak: none;
    		font-style: normal;
    		font-weight: normal;
    		font-variant: normal;
    		text-transform: none;
    		line-height: 1;
    		-webkit-font-smoothing: antialiased;
    		-moz-osx-font-smoothing: grayscale;
    		text-align: center;
    		line-height: 34px;
    		font-size: 34px;
    	}

    	#banner.active .nav-list-holder{
    		left: 0px; 
    	}

    	.active #navigation_button{
    		position: fixed;
    		top: 0px;
    		right: 0px;
    		left: 0px;
    		bottom: 0px;
    		background: rgba(0,0,0,0.8);
    		width: auto;
    		height: auto;
    		z-index: 10;
    	}

    	.active #navigation_button:after {
    		display: none;
    	}

    	.search-box{
    		display: none;
    	}

    	#search_button_holder{
    		display: inline-block;
    		width: 37px;
    		height: 34px;
    		position: absolute;
    		top: 7px;
    		right: 25px;
    		transition:  0.2s;
    	}

    	#search_button_holder:after {
    		position: absolute;
    		left: 0px;
    		right: 0px;
    		top: 0px;
    		bottom: 0px;
    		display: block;
    		content: "\e8b6";
    		font-family: 'Material Icons';
    		speak: none;
    		font-style: normal;
    		font-weight: normal;
    		font-variant: normal;
    		text-transform: none;
    		line-height: 1;
    		-webkit-font-smoothing: antialiased;
    		-moz-osx-font-smoothing: grayscale;
    		text-align: center;
    		line-height: 34px;
    		font-size: 34px;
    	}

    	#globalnav_button{
    		display: inline-block;
    		width: 37px;
    		height: 34px;
    		position: absolute;
    		top: 12px;
    		right: 10px;
    		transition: 0.2s;
    	}

    	#globalnav_button:after {
    		position: absolute;
    		left: 0px;
    		right: 0px;
    		top: 0px;
    		bottom: 0px;
    		display: block;
    		content: "\e5D4";
    		font-family: 'Material Icons';
    		speak: none;
    		font-style: normal;
    		font-weight: normal;
    		font-variant: normal;
    		text-transform: none;
    		line-height: 1;
    		-webkit-font-smoothing: antialiased;
    		-moz-osx-font-smoothing: grayscale;
    		text-align: center;
    		line-height: 34px;
    		font-size: 34px;
    	}

    	.globalnav_active #globalnav_button{
    		position: fixed;
    		top: 0px;
    		right: 0px;
    		left: 0px;
    		bottom: 0px;
    		background: rgba(0,0,0,0.8);
    		width: auto;
    		height: auto;
    		z-index: 10;
    	}

    	.globalnav_active #globalnav_button:after {
    		display: none;
    	}

    	.globalnav_active #globalnav-holder{
    		right: 0px; 
    	}

    	#banner.search_active #search_button_holder{
    		background: rgba(0,0,0,0.2);
    	}

    	#banner.search_active #search_button_holder:after{
    		color: #FFF;
    		content: "\e5CD";
    	}

    	#banner.search_active .search-box{
    		display: block;
    		position: absolute;
    		top: 55px;
    		background: #333;
    		left: -38px;
    		right: -34px;
    		width: auto;
    		height: 50px;
    		box-sizing: border-box;
    		border-right: 50px solid #333;
    	}

    	#search-field {
    		font-size: 18px;
    		height: 22px;
    		right: 22px;
    	}

    	#search-button {
    		width: 40px;
    		height: 40px;
    		font-size:  14px;
    		right: -45px;
    		top: 5px;
    	}

    	#search-button:after{
    		line-height: 43px;
    		font-size: 25px;
    	}

    	#search-field{
    		left: 5px;
    		width: 100%;
    		height: 40px;
    		background: #EFEFEF;
    		margin-right: 40px;
    		box-sizing: border-box;
    		top: 5px;
    		font-size: 30px;
    	}

    }


    @media only screen and (max-width: 663px){

    }

    @media only screen and (max-width: 500px){
    	#banner h1 a{
    		font-size:  25px !important;
    		padding-left: 30px !important;
    		top: -1px !important;
    		position: relative !important;
    	}

    	#banner h1 a:before
    	{
    		left: -7px !important;
    		top: 0px !important;
    		width: 20px !important;
    		font-size: 24px !important;
    		height: 22px !important;
    		padding: 3px !important;
    		padding-left: 2px !important;
    		padding-right: 6px !important;
    	}
    }

    @media only screen and (max-width: 450px){
    	#navigation_button:after, #globalnav_button:after, #search_button_holder:after{
    		font-size: 28px !important;
    	}

    	#navigation_button, #globalnav_button, #search_button_holder{
    		width:  30px !important;
    	}

    	.active #navigation_button,.globalnav_active #globalnav_button{
    		width:  auto !important;
    	}

    	.notifications:after{
    		font-size: 21px !important;
    		right: -2px;
    		top: 1px;
    	}

    	#search_button_holder{
    		right: 10px !important;
    	}

    	#notify_holder{
    		right: 75px !important;
    	}

    	#banner h1 a{
    		font-size:  23px !important;
    		padding-left: 23px !important;
    		top: -1px !important;
    		position: relative !important;
    		left: -5px;
    	}

    	#banner h1 a:before
    	{
    		left: -10px !important;
    		top: -2px !important;
    	}

    }

    @media only screen and (max-width: 400px){

    	NOT(.no_sitenav) #banner h1 a:before{
    		display: none !important;
    	}


    	NOT(.no_sitenav) #banner h1 a{
    		padding-left: 0px !important;
    		font-size: 20px !important;
    		font-weight: 500;
    		top: 0px !important;
    		max-width: 130px;
    		height: 1.5em;
    		display: inline-block;
    		text-overflow: ellipsis;
    		white-space: nowrap;
    		overflow: hidden;
    		line-height: 2em;
    	}

    }

/*
     ___  _   ___ ___   _____ ___ _____ _    ___
    | _ \/_\ / __| __| |_   _|_ _|_   _| |  | __|
    |  _/ _ \ (_ | _|    | |  | |  | | | |__| _|
    |_|/_/ \_\___|___|   |_| |___| |_| |____|___|

    */


    #ttlbar h2{
    	text-align: center;
    	line-height: 64px;
    	font-size: 34px;
    }

    #ttlbar h2 img{
    	margin-right:  0.3em;
    	height: 1.2em;
    }

    @media only screen and (max-width: 979px){ 
    	#ttlbar h2{
    		font-size: 30px;
    	}
    }

    @media only screen and (max-width: 663px){
    	#ttlbar h2{
    		font-size: 28px;
    	}
    }

    @media only screen and (max-width: 500px){
    	#ttlbar h2{
    		font-size: 24px;
    		line-height: 2.4em;
    	}
    }

    @media only screen and (max-width: 450px){

    }

    @media only screen and (max-width: 400px){
    	#ttlbar h2{
    		font-size: 22px;
    		line-height: 2.4em;
    	}
    }


/*
     ___ __  __   _   _    _      _  _ ___   _   ___  ___ ___
    / __|  \/  | /_\ | |  | |    | || | __| /_\ |   \| __| _ \
    \__ \ |\/| |/ _ \| |__| |__  | __ | _| / _ \| |) | _||   /
    |___/_|  |_/_/ \_\____|____| |_||_|___/_/ \_\___/|___|_|_\

    */


    @media only screen and (min-width: 980px){

    	body.smallheader:not(.offtop) #banner h1{
    		font-size: 30px;
    		line-height: 2em;
    		position: absolute;
    		top: 0px;
    		left: 25px;
    	}

    	body.smallheader:not(.offtop) #navigation{
    		margin-top: -2px;
    	}

    	body.smallheader:not(.offtop) #banner h1 a{

    		line-height: 2em;
    	}



    	body.smallheader:not(.offtop)  #navigation{
    		margin-top: -2px;
    	}

    	body.smallheader:not(.offtop) #pg{
    		margin-top: 60px;
    	}
    	body.smallheader:not(.offtop) #globalnav{
    		padding-top: 8px;
    	}

    	body.smallheader:not(.offtop) #hd{
    		height: 60px;
    		position: fixed;
    		border-bottom: 1px solid rgba(100,100,100,0.2);
    	}

    	body.smallheader:not(.offtop) #banner{
    		position: absolute;
    		top: 0px;
    		left: 0px;
    	}

    	body.smallheader:not(.offtop) #banner h1 a:before {
    		top: -2px !important;
    	}

    }

    .smallheader #banner h1 a{
    	font-weight: 200 !important;
    }

    .smallheader.offtop #pg{
    	margin-top: 60px !important;
    }

    .smallheader #banner h1 a:before {
    	background-color: transparent !important;
    	content: '\e962' !important;
    	color: #000 !important;

    }



    body.nosupport_browser #browsersupport{
    	display: block !important;
    	position: fixed;
    	left: 20px;
    	bottom: 0px;
    	right: 20px;
    	z-index: 100000000;
    	background: #D50000;
    	opacity: 0.8;
    	max-width: 100%;
    }

    body.nosupport_browser #nosupport{
    	padding: 10px;
    	font-size: 15px;
    	display: block !important;
    	background: #EFEFEF;
    	margin: 10px;
    	line-height: 1.5em;
    	margin-bottom: 0px;
    }

    body.nosupport_browser #nosupport a{
    	color: #333;
    }

    body.nosupport_browser a strong{
    	font-size: 1.4em;
    }

    body.nosupport_browser #nosupport a p{
    	line-height: 1.6em;
    	padding: 0.2em 0px;
    }



    #partial_alert{
    	position: fixed;
    	z-index: 1000000;
    	background: #D32F2F;
    	color: #FFF;
    	padding: 5px 16px 5px 10px;
    	top: 0px;
    	opacity: 0.9;
    	box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);
    }

    #partial_alert a{
    	color: #333;
    }

    #partial_alert .close{
    	position: absolute;
    	top: 0px;
    	right: 0px;
    	background: #333;
    	color: #FFF;
    	font-size: 10px;
    	cursor: pointer;
    	width: 12px;
    	text-align: center;
    }

    #partial_alert .close:HOVER{
    	background: #000;
    }

    /*savepage-import-url=components.css?siteVersion=1.0.6*/@charset "utf-8";
    /* CSS Document */


/*
     ___ _   _ ___ ___ ___   ___ ___   _   ___  ___ _  _
    / __| | | | _ \ __| _ \ / __| __| /_\ | _ \/ __| || |
    \__ \ |_| |  _/ _||   / \__ \ _| / _ \|   / (__| __ |
    |___/\___/|_| |___|_|_\ |___/___/_/ \_\_|_\\___|_||_|

    */

    #super_search {
    	list-style: none;
    	background: #FFF;
    	width: 236px;
    	position: absolute;
    	right: 25px;
    	top: 25px;
    	padding: 0px;
    	border-top: 0px;
    	border: none;
    	z-index: 501;
    	font-family: Arial, Helvetica, sans-serif;
    	display: none;
    }


    .offtop #super_search {
    	left: 5px;
    	top: 45px;
    }

    #super_search li {
    	padding-left: 40px;
    	position: relative;
    	text-align: left;
    }

    #super_search li.user_block {
    	height: 38px;
    }

    #super_search li.noimg {
    	padding-left: 3px;
    	height: 38px;
    }

    #super_search li a {
    	overflow: hidden;
    }

    #super_search li.noresults{
    	text-align: center;
    	background: #DDD;
    	line-height: 22px;
    	color: #666;
    	padding-left: 0px;
    }

    #super_search li span {
    	display: block;
    	height: 15px;
    	overflow: hidden;
    	font-size: 12px;
    	line-height: 18px;
    }

    #super_search li span.more {
    	color: #999;
    }

    #super_search li.user_block span.more {
    	font-size: 10px;
    }

    #super_search li a img {
    	position: absolute;
    	margin-left: -40px;
    }

    #super_search li a {
    	display: block;
    	height: 35px;
    	padding: 5px;
    	color: #333;
    }

    #super_search li:HOVER {
    	background: #ddd;
    }

    #super_search li.selected {
    	background: #ccc !important;
    }

    #super_search li:HOVER a {
    	text-decoration: none !important;
    }

/*
    online indicator
    */

    #mystatus{
    	position: relative;
    	padding-right: 2.2em;
    }

    #mystatus:after{
    	position: absolute;
    	content:  '\e061';
    	width: 20px;
    	height: 20px;
    	display: block;
    	font-family: 'Material Icons';
    	display: block;
    	height: 12px;
    	right: 0px;
    	top: 0px;
    	font-size: 1.5em;
    }

    .status_online:after {
    	color: #36ce74;
    }

    .status_away:after {
    	color: #ffab03;
    }

/*
     ___ __  __   _   _    _      __  __ ___ _  _ _   _
    / __|  \/  | /_\ | |  | |    |  \/  | __| \| | | | |
    \__ \ |\/| |/ _ \| |__| |__  | |\/| | _|| .` | |_| |
    |___/_|  |_/_/ \_\____|____| |_|  |_|___|_|\_|\___/

    */

    .offtop #mystatus:after{
    	line-height: 3em;
    	right:  10px;
    }

    @media only screen and (max-width: 979px){ 
    	#mystatus:after{
    		line-height: 3em;
    		right:  10px;
    	}
    }


/*
     ___ ___ ___   _   ___     ___ ___ _   _ __  __ ___ ___
    | _ ) _ \ __| /_\ |   \   / __| _ \ | | |  \/  | _ ) __|
    | _ \   / _| / _ \| |) | | (__|   / |_| | |\/| | _ \__ \
    |___/_|_\___/_/ \_\___/   \___|_|_\\___/|_|  |_|___/___/

    */


    #tagscopenav{

    }

    #tagscopenav li{
    	font-size:  16px;
    	display: inline-block;
    	line-height: 3em;
    	vertical-align: top;

    	font-weight: 500;
    	font-family: "Roboto","Helvetica","Arial",sans-serif !important;
    }


    #tagscopenav li li.tag{
    	opacity: 0.5;
    	max-width: 200px;
    	overflow: hidden;
    	white-space: nowrap;
    	text-overflow: ellipsis;
    }

    #tagscopenav li.scope,#tagscopenav li.tag {
    	padding:  0px 10px;
    }

    #tagscopenav li li:before {
    	content: '\E5CC';
    	opacity: 0.7;
    	vertical-align: top;
    	display: inline-block;
    	font-family: 'Material Icons';
    	font-weight: normal;
    	font-style: normal;
    	font-size: 25px;
    	margin: 0 10px 0 0px;
    	-webkit-font-smoothing: antialiased;
    }

    #tagscopenav li li.tag:last-child {
    	opacity: 1;
    }

    #tagscope{
    	max-width: 100%;
    	overflow: hidden;
    	white-space: nowrap;
    	box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);
    	position: relative;
    	z-index: 1;
    	margin-bottom: 20px;
    	-webkit-overflow-scrolling:touch;
    }


    #tagscope:HOVER, .touch_device #tagscope{
    	overflow-x:  auto;
    }

/*
     __  __ ___ ___  ___
    |  \/  |_ _/ __|/ __|
    | |\/| || |\__ \ (__
    |_|  |_|___|___/\___|

    */

    .no_results{
    	font-family: "Roboto","Helvetica","Arial",sans-serif !important;
    	font-size: 30px;
    	min-height: 600px;
    	padding: 30px;
    	color: #CCC;
    	font-weight: 200;
    	line-height: 1.2em;
    	text-align: center;
    }

    .ajaxloadmore{
    	margin: 20px auto;
    	display: block;
    	width: 100px;
    }

    .no_results.no-height{
    	min-height: auto;
    }


/*
     ___  _   ___ ___   _      ___   _____  _   _ _____
    | _ \/_\ / __| __| | |    /_\ \ / / _ \| | | |_   _|
    |  _/ _ \ (_ | _|  | |__ / _ \ V / (_) | |_| | | |
    |_|/_/ \_\___|___| |____/_/ \_\_| \___/ \___/  |_|

    */


    .mdl-grid-full{
    	width: 100%;
    	box-sizing: border-box;
    }

    .fill__cards .mdl-card{
    	width: 100%;
    }

    .noheight__cards .mdl-card{
    	min-height: initial !important;
    }

    .fill__tables .mdl-data-table{
    	width: 100%;
    }

    .fullcard__tables .mdl-data-table{
    	border-width: 0px;
    }

    .color_block{
    	min-height: 20px;
    }


    .bb77-header {
    	min-height: 110px;
    	padding: 20px;
    }

    .bb77-header .info-section{
    	padding-bottom: 60px;
    }

    .bb77-header h3{
    	font-size: 23px;
    }

    .bb77-header h4{
    	font-size: 17px;
    }

    .super__layout__normal .bb77-header-focus{
    	margin-top: -81px;
    }


    .bb77-table .logo-img{
    	margin-right: 0.8em;
    	height: 4em;
    }



    .tagfullpage + .super__layout__normal {
    	margin-top: -20px !important;
    }

    @media (max-width: 1200px){
    	.super__layout__normal .bb77-header-focus{
    		margin-top: 0px;
    	}
    }


    @media (max-width: 839px){
    	.bb77-header-focus{
    		margin-top: 0px;
    	}

    	.bb77-header .info-section{
    		padding-bottom: 20px;
    	}

    	.super__layout__normal .bb77-header-focus{
    		margin-top: 0px;
    	}
    }

    .loadingani{
    	background: /*savepage-url=/img/spinner.svg*/ url() no-repeat center center !important;
    	height: 150px;
    	width: 150px;
    	display: block;
    	text-align: center;
    	color: #999;
    }
    .mi_notice{
    	background-color: #e04838;
    	color: #FFF;
    	margin: 10px 0px;
    	padding: 20px;
    	box-shadow: 0 1px 2px rgba(43,59,93,0.29);
    	border-radius: 3px;
    	font-size: 14px;
    }

    .mi_notice ul{
    	margin: 10px;
    }

    .mi_notice .highlight{
    	display: inline-block;
    	vertical-align: top;
    	margin: 0px 5px 0px 0px;
    	padding: 3px 5px;
    	background: #FFF;
    	color: #000;
    }

    .mi_notice li{
    	margin-bottom: 5px;
    	list-style-type: decimal;
    	list-style-position: inside;;
    }

    .mi__warning a{
    	display: block;
    	border-left: 50px solid #e04838;
    	height: 100px;
    	margin: 5px 5px 10px;
    	background: #FFF;
    	color: #333;
    	text-decoration: none;
    	position: relative;
    	border-radius: 3px;
    }

    .mi__warning a span{
    	display: inline-block;
    	padding: 20px 20px;
    	font-size: 16px;
    	line-height: 1.3em;
    }

    .mi__warning a:HOVER{
    	background: #DDD;
    }

    .mi__warning a svg{
    	position: absolute;
    	left: -40px;
    	top: 50%;
    	margin-top: -15px;
    	fill: #FFF;
    	width: 30px;
    }


    .online {
    	font-size: 10px;
    	color: #999;
    }

    .online span {
    	background: #0C0;
    	padding: 1px 5px;
    	color: #FFF;
    }

    .online span.away {
    	background: #F90;

    }

/*
     _    ___ ___ _____   __  __ ___ _  _ _   _
    | |  | __| __|_   _| |  \/  | __| \| | | | |
    | |__| _|| _|  | |   | |\/| | _|| .` | |_| |
    |____|___|_|   |_|   |_|  |_|___|_|\_|\___/

    */


    .section_links {
    	margin: 13px 0px;

    }

    .section_links li {

    	cursor: pointer;

    }

    .section_links li:HOVER {
    	background: rgba(0,0,0,0.2);
    }

    .section_links li.selected {
    	font-weight: bold;
    	background: #000;
    	color: #FFF;
    }

    .section_links li a {
    	padding: 3px;
    	display: block;
    	color: #000;
    }

    .section_links li.selected a {
    	color: #FFF;
    }

    .section_links li a:HOVER {
    	text-decoration: none;
    }
    /*savepage-import-url=helpers.css?siteVersion=1.0.6*/@charset "utf-8";
    /* CSS Document */

    .hide {
    	display: none;
    }

    .mdl-badge[data-badge=""]:after{
    	display: none !important;
    }

    .mdl-badge[data-badge=""]{
    	display: none !important;
    }

    .group:after {
    	content: "";
    	display: table;
    	clear: both;
    }

    .mdl-badge.mdl-abs{
    	position: absolute;
    	top: 1.2em;
    	right: 0.2em;
    }

    .mdi__after::after{
    	display: block;
    	content: "\e5D2";
    	font-family: 'Material Icons';
    	speak: none;
    	font-style: normal;
    	font-weight: normal;
    	font-variant: normal;
    	text-transform: none;
    	line-height: 1;
    	-webkit-font-smoothing: antialiased;
    	-moz-osx-font-smoothing: grayscale;
    	text-align: center;
    	line-height: 34px;
    	font-size: 34px;
    }

    .mdi__after.mdi__small::after{
    	line-height: 20px;
    	font-size: 20px;
    }

    .noselect {
    	-webkit-touch-callout: none; /* iOS Safari */
    	-webkit-user-select: none; /* Safari */
    	-khtml-user-select: none; /* Konqueror HTML */
    	-moz-user-select: none; /* Firefox */
    	-ms-user-select: none; /* Internet Explorer/Edge */
            user-select: none; /* Non-prefixed version, currently
            supported by Chrome and Opera */
        }


        table .fordownload{
        	display: none;
        }


/*
     ___                     _  _     _
    |_ _|_ __  __ _ __ _ ___| || |___| |_ __  ___ _ _ ___
     | || '  \/ _` / _` / -_) __ / -_) | '_ \/ -_) '_(_-<
    |___|_|_|_\__,_\__, \___|_||_\___|_| .__/\___|_| /__/
                   |___/               |_|
                   */

                   .imgHolder{
                   	width: 100%;
                   	height: 0px;
                   	background-size: cover !important;
                   	background-color: #EFEFEF;
                   	display: inline-block;
                   }

                   .imgHolder.r16_9{
                   	padding-bottom: 56.25%;
                   }

                   .imgHolder.r851_315{
                   	padding-bottom: 37%;
                   }


/*
     ___ ___ ___ ___  ___  _  _ ___ _____   _____
    | _ \ __/ __| _ \/ _ \| \| / __|_ _\ \ / / __|
    |   / _|\__ \  _/ (_) | .` \__ \| | \ V /| _|
    |_|_\___|___/_|  \___/|_|\_|___/___| \_/ |___|

    */

    .fromtablet, .frommobile, .fromthinmobile{
    	display: none;
    }

    @media only screen and (min-width: 980px) { 

    }

    @media only screen and (max-width: 1450px) { 
    	.notthin{
    		display: none !important;
    	}
    	.fromthin{
    		display: initial;
    	}

    	.nomeddesktoppadding{
    		padding: 0px !important;
    	}

    	.ml-table-data td{
    		padding: 1em 1em !important;
    	}


    }

    @media only screen and (max-width: 1200px) { 

    	.ml-table-data td{
    		padding: 0.5em 1em !important;
    		display: block;
    		height: auto !important;
    	}

    	.ml-table-data td:first-child{
    		font-weight: 700;
    		padding-bottom: 0px;
    	}

    	.ml-table-data tr{
    		display: block;
    		height: auto !important;
    	}

    }

    @media only screen and (max-width: 1200px){ 
    	.nosmalldesktoppadding{
    		padding: 0px !important;
    	}
    }

    @media only screen and (max-width: 979px){ 
    	.nottablet{
    		display: none !important;
    	}
    	.fromtablet{
    		display: initial;
    	}
    }

    @media only screen and (max-width: 663px){
    	.notonmobile{
    		display: none !important;
    	}
    	.frommobile{
    		display: initial;
    	}

    	.nomobilepadding{
    		padding: 0px !important;
    	}
    }

    @media only screen and (max-width: 500px){
    	.notthinmobile{
    		display: none !important;
    	}

    	.fromthinmobile{
    		display: initial;
    	}
    }

    @media only screen and (max-width: 450px){

    }

    @media only screen and (max-width: 400px){

    }
    /*savepage-import-url=forms.css?siteVersion=1.0.6*/#saveitem fieldset {
    	border: none;
    }

    button {
    	background-color: transparent;
    }



    #form-confirm-list {
    	margin: 0px;

    }

    #error_caption{
    	background: #D00000;
    	color: #FFF;
    	padding: 1em;
    }

    #error_caption h4{
    	font-size: 14px;
    }

    #error_caption h4 .errors{
    	display: block;
    	border-left: 2px solid #FFF;
    	padding-left: 0.8em;
    	margin: 0.3em;
    	line-height: 1.3em;
    }

    #form-confirm-list > li {
    	margin-bottom: 10px;
    }

    #form-confirm-list *{
    	font-size: 14px;
    	line-height: 1.3em;
    }

    #form-confirm-list > li {
    	padding: 20px 10px;
    	font-size: 14px;
    	transition: background-color 0.3s;
    	position: relative;
    	border-top: 1px solid #EFEFEF;
    }



    #form-confirm-list > li strong {
    	margin-left: 0px;
    	position: relative;
    	background-color: transparent;
    	color: #212121;
    	display: inline-block;
    	vertical-align: top;
    	width: 200px;
    	margin-right: 10px;
    	border: 0px;
    	float: left;
    	text-align: right;
    	padding: 2px 0px;
    	padding-right: 3px;
    }

    #form-confirm-list > li span.formitem {
    	padding: 2px;
    	display: block;
    	border-left: 1px solid #AAA;
    	padding-left: 6px;
    }

    #form-confirm-list > li span.formitem {
    	display: block;
    	vertical-align: top;
    	border-left: 0px;
    	overflow: hidden;
    }

    #form-confirm-list > li span img {
    	margin: 2px;

    }

    .textfield{
    	position: relative;
    }

    .textfield .alt_langs, .textareafield .alt_langs, .richtextnewfield .alt_langs{
    	background: #BBDEFB;
    	font-size: 12px;
    	padding: 0.3em;
    	position: absolute;
    	right: 0px;
    	top: 0px;
    	cursor: pointer;
    	z-index: 10;
    }

    .textfield .alt_langs i, .textareafield .alt_langs i, .richtextnewfield .alt_langs i{
    	font-size: 12px;
    }

    .textfield .alt_langs:HOVER, .textareafield .alt_langs:HOVER, .richtextnewfield .alt_langs:HOVER{
    	color:  #FFF;
    	background: #333;
    }

    .textfield .alt_langs.active, .textareafield .alt_langs.active, .richtextnewfield .alt_langs.active{
    	background: #333;
    	color:  #BBDEFB;
    }

    .textfield .alt_langs.active i, .textareafield .alt_langs.active i, .richtextnewfield .alt_langs.active i{
    	font-size: 16px;
    }

    .hide_with {
    	display: block;
    	background: #CCC;
    	line-height: 2.6em;
    	height: 2.6em;
    	text-align: center;
    	color: #000;
    	margin: 10px auto;
    	padding-left: 0px;
    	width: 150px;
    	cursor: pointer;
    	font-size: 12px;
    	border-radius: 4px;
    	position: relative;
    	box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
    }

    .hide_with a{
    	display: block;
    	color: #000;
    }

    .hide_with:HOVER{
    	background:  #333;
    }

    .hide_with:HOVER a{
    	color: #FFF;
    }

    .hide_with:after{
    	position: absolute;
    	content:  '\e5cf';
    	display: block;
    	font-family: 'Material Icons';
    	display: block;
    	right: 0.3em;
    	top: 2px;
    	font-size: 1.5em;

    }

    .hide_with:HOVER:after{
    	color: #FFF;
    } 

    .hide_with.lang_option{
    	font-size: 10px;
    	background: #BBDEFB;
    	margin-right: 0.3em;
    	width: 120px;
    	margin-bottom: 20px;
    	margin-top: 0px;
    	display: none;
    }

    .left_option .hide_with{
    	margin: 0px auto 10px 0px;
    }

    .hide_wrapper.lang_option{
    	display: none;
    } 

    fieldset.highlight{
    	position: relative;
    	padding-top: 10px !important;
    }

    fieldset.highlight:before {
    	background: #FFFDE7;
    	content: '';
    	position: absolute;
    	left: -89px;
    	top: 0px;
    	right: -10px;
    	bottom: 0px;
    }

    div.field[highlight]:before{
    	content: '';
    	background: #FFFDE7;
    	position: absolute;
    	right: -10px;
    	left: -99px;
    	bottom: 0px;
    	display: block;
    	top: 0px;
    }

    div.field[highlight]{
    	padding:  10px 0px;
    }



    .hide_wrapper.lang_option.active{
    	display: inline-block;
    } 

    .hide_wrapper.lang_option.open.active{
    	display: block;
    }

    #saveitem .hide_wrapper.lang_option.open.active label{
    	background: #bbdefb;
    	min-height: 2.2em;
    }

    .hide_wrapper.lang_option.open.active {
    	display: block;
    	border-left: 3px solid #bbdefb;
    	background: #bbdefb;
    }

    .hide_with.lang_option {
    	display: inline-block;
    }

    .hide_with.lang_option:HOVER{
    	background-color: #333;
    } 

    .fullpage {
    	color: #333333;
    }

    .fullpage #pagetitle #pagetitleContent {
    	background-position: 0 -800px;
    }

    .fullpage #content {
    	background-color: #F2F2F2;
    	margin-left: 60px;
    	margin-right: 50px;
    	_height: 1%;
    }

    .fullpage #content #beError {
    	background-color: #fff;
    	margin-left: 0;
    }

    .fullpage #sidebar {
    	display: none;
    }

    .fullpage .yui-b {
    	margin-right: 0;
    }

    .popup #doc3 {
    	width: 550px;
    	min-width: 550px;
    }

    .popup #doc3.delui-v3 {
    	width: 700px;
    	min-width: 700px;
    }

    .popup #doc3.delui-v4 {
    	width: 680px;
    	min-width: 680px;
    }

    .popup {
    	background-color: #efefef;
    }

    .popup #banner {
    	background-color: white;
    	clear: both;
    	position: static;
    	margin: 0;
    	width: 550px;
    	padding: 8px 0 8px 20px;
    	height: auto;
    	min-height: 2em;
    }

    .popup .delui-v3 #banner {
    	width: 700px;
    }

    .popup .delui-v4 #banner {
    	width: 100%;
    }

    .popup #banner #site_title {
    	position: absolute;
    	top: 5px;
    	left: 10px;
    	margin: 0;
    	padding: 0;
    	width: 6.5em;
    }

    .popup #banner #site_title a {
    	float: left;
    	margin: 0;
    	padding: 0 0 0 28px;
    }

    .popup #banner #page_title {
    	position: absolute;
    	left: 10em;
    	top: .5em;
    	margin: 5px 0 0 0;
    	padding: 0;
    	color: #888;
    	font-size: 115%;
    	line-height: 1em;
    }

    #saveitem {
    	float: none;
    	background-color: #EEE;
    	width: 100%;
    	padding-top: 1px;
    	padding-bottom: 1px;
    }

    #doc3.delui-v4 #saveitem {
    	float: left;
    	background-color: #efefef;
    	width: 65%;
    }

    #saveitem fieldset {
    	margin: 15px 27px 10px 9.6em;
    	padding: 0;
    }

    #saveitem fieldset.related {
    	margin-top: 0px;
    	margin-bottom: 0px;
    }

    #saveitem fieldset:first-of-type.related{
    	margin-top: 15px;
    }

    #saveitem fieldset.full_width{
    	margin-left: 26px;
    }

    #saveitem fieldset legend span {

    	color: #212121;
    	background: transparent;
    	text-align: left;
    	font-size: 25px;

    	width: auto;
    	display: block;
    	border: 0px;
    	border-radius: 0px;
    	padding: 0px;
    	margin: 0px;
    	position: relative;
    	margin-bottom: 4px;
    	margin-top: 10px;
    	display: block;

    }

    #saveitem fieldset:NOT(.full_width) legend span{
    	padding-left: 20px;
    }

    #doc3.delui-v4 #saveitem fieldset {
    	margin: 10px 20px 10px 5.6em;
    }

    #saveitem #savedon {
    	color: #666;
    	font-size: 84%;
    	font-weight: bold;
    	display: block;
    	text-align: right;
    	margin: 0 -6px .4em 0;
    }

    #saveitem #popupDelete {
    	padding-left: .6em;
    }


    #saveitem .field.error{
    	position: relative;
    	margin-bottom: 5px;
    }

    #saveitem .field.error:before {
    	content: '';
    	display: block;
    	left: 0px;
    	right: 0px;
    	bottom: -3px;
    	height: 3px;
    	background: rgba(213, 0, 0, 0.7);
    	z-index: 1000;
    	position: absolute;
    }

    #saveitem .info.error{
    	background: #FFF !important;
    	padding-top: 3px !important;
    	padding-bottom: 3px !important;
    }
    #saveitem .field {
    	position: relative;
    	zoom: 1;
    	height: auto;
    	margin-bottom: 5px;
    }

    #saveitem #deleteconfirmation.deleting span {
    	padding-left: 55px;
    	color: #EFF5FB;
    	background: transparent /*savepage-url=http://l.yimg.com/hr/10363/img/waitingDelicious01.gif*/ url() center left no-repeat;
    }

    #saveitem #deleteconfirmation.deleting a {
    	display: none;
    }

    #saveitem label {
    	color: #333333;
    	display: block;
    	position: absolute;
    	left: -10em;
    	text-align: right;
    	width: 9.4em;
    	padding: 2px 0 0 0;
    }

    #saveitem label strong {
    	font-size: 10px;
    	font-weight: normal;
    	position: relative;
    	top: 0px;
    	color: #212121;
    	right: 5px;
    }

    #saveitem .field .info {
    	display: block;
    	position: relative;
    	/*right: -6px;*/
    	bottom: 0;
    	margin: 0;
    	padding: 2px 3px;
    	width:100%;
    	color:#fff;
    	margin-bottom: 10px;
    }

    #saveitem .field.displaytextfield .info {
    	margin-bottom: 0px;
    	color: #FFF;
    }

    #saveitem .field .info .txt {
    	display: block;
    	font-size: 84%;
    	color: #888;
    }

    #saveitem .field.displaytextfield .info {
    	margin-bottom: 0px;
    }

    #saveitem .field.displaytextfield .info .txt {
    	color: #444;
    }


    #saveitem .field .info.required .txt {
    	display: block;
    	font-size: 84%;
    	color: #c62828;
    }

    #saveitem .field .info .txt .error {
    	background: none;
    	color: #9e1919;
    	padding: 0;
    }

    #saveitem .error label strong{
    	color: #d50000;
    	position: relative;
    }

    #saveitem .field.error:after{
    	content:  '\e000';
    	font-family: 'Material Icons';
    	display: block;
    	position: absolute;
    	right: -24px;
    	top: 0px;
    	width: 20px;
    	height: 20px;
    	color: #d50000;
    	font-size: 16px;
    }



    .is_ie#saveitem .error .info .txt {
    	position: relative;
    	right: -20px;
    	padding-right: 20px;
    	margin-right: 0;
    }

    #saveitem input, #saveitem textarea {
    	position: relative;
    	left: 0;
    	top: 0;
    	width: 100%;
    	padding: 5px;
    	font-size: 20px;
    	border: 1px solid #DDD;
    	margin-bottom: 1px;
    	border-top-width: 0px;
    	border-right-width: 0px;
    	border-left-width: 0px;
    	font-size: 16px;
    	resize: none;
    	box-sizing: border-box;
    	padding: 0.5em;
    }

    #saveitem input:focus, #saveitem textarea:focus{
    	border-bottom: 2px solid #333;
    	margin-bottom: 0px;
    	outline: none;
    	transition: border-bottom-color 0.2s;
    }

    #saveitem textarea:focus{
    	margin-bottom: 1px;
    }

    #saveitem select{
    	border: 1px solid #DDD;
    	border-top:  0px;
    	border-right: 0px;
    	border-left: 0px;
    	margin-bottom: 1px;
    	background: #FFF;
    	font-size: 16px;
    	padding: 0.5em 0.5em 0.4em;
    	margin-top: 0px;
    }

    #saveitem select:focus{
    	outline: none;
    }

    #saveitem #notes {
    	height: 5em;
    }

    #saveitem .submitbttns {
    	padding: 9px 0 0 0;
    	margin: 0;
    	width: 100%;
    }

    #saveitem .submitbttns input.checkbox {
    	width: 1em;
    	padding: none;
    	margin: none;
    }

    #saveitem .share {
    	display: block;
    	float: left;
    }

    #saveitem .share label {
    	width: 10em;
    	position: static;
    	float: none;
    	display: inline;
    	margin: 0;
    	padding: 0 0 0 0.5em;
    	color: #555;
    	font-size: 84%;
    }

    #saveitem .share input {
    	position: static;
    }

    #saveitem input.checkbox {
    	width: 1.25em;
    	display: inline;
    	margin: 0;
    	padding: 0;
    }

    #saveitem .submitbttns .buttons {
    	display: block;
    	float: right;
    	margin-right: -7px;
    	_margin-right: 0;
    }

    #saveitem .submitbttns .buttons .btn {
    	margin-left: 10px;
    }

    .is_ie6 #saveitem .field .info {
    	padding-bottom: 2px;
    }

    #recommendations {
    	clear: both;
    	padding: 0 60px 30px 5.6em;
    	width: auto;
    	_height: 1%;
    }

    #recommendations .tabs li {
    	float: left;
    }

    #recommendations .tabs li a {
    	display: block;
    	color: #FFF;
    	width: 6.5em;
    	padding: .2em 0;
    	text-indent: .5em;
    	font-weight: bold;
    	background: #D2D2D2 /*savepage-url=/img/tracker_sprite_tabs.gif*/ url() no-repeat right -200px;
    	text-decoration: none;
    	margin-right: .4em;
    	text-align: left;
    	font-size: 122%;
    }

    #doc3.delui-v4 #recommendations .tabs li a {
    	font-size: 100%;
    	padding: 0.2em;
    }

    #recommendations .tabs li a:hover {
    	background: #565656 /*savepage-url=/img/tracker_sprite_tabs.gif*/ url() no-repeat right -250px;
    }

    #recommendations .tabs li.selected a {
    	background: #3274D0 /*savepage-url=/img/tracker_sprite_tabs.gif*/ url() no-repeat right -150px;
    }

    #recommendations .tabs li.selected a:hover {
    	background: #3274D0 /*savepage-url=/img/tracker_sprite_tabs.gif*/ url() no-repeat right -150px;
    }

    #recommendations .panel {
    	clear: both;
    	background-color: #fff;
    	border-top: 3px solid #3274D0;
    	width: 410px;
    }

    #recommendations .panel ul {
    	padding: 0 4px .6em 4px;
    }

    #recommendations .panel {
    	width: 100%;
    }

    .popup #recommendations {
    	padding-bottom: 0;
    }

    .popup .delui-v3 #recommendations {
    	display: none;
    }

    .popup .delui-v4 #recommendations {
    	clear: none;
    	float: left;
    	padding: 18px 0 0 5px;
    }

    .popup #recommendations .panel {
    	overflow: auto;
    	min-height: 220px;
    	height: 220px;
    	width: auto;
    }

    .popup .delui-v4 #recommendations .panel {
    	width: 185px;
    	min-height: 200px;
    	height: 260px;
    }

    #recommendations #sortBy {
    	display: none;
    }

    .jsEnabled #recommendations #sortBy {
    	display: block;
    	margin: 0 5px;
    	color: #888;
    	font-size: 92%;
    	text-align: right;
    }

    #recommendations #sortBy p {
    	margin: .6em 0;
    }

    #recommendations #sortBy span.divide {
    	padding: 0 .2em;
    }

    #recommendations #sortBy strong {
    	font-weight: bold;
    	color: #787878;
    }

    #recommendations #sortBy .error {
    	color: maroon;
    }

    #recommendations .panel ul li h3 {
    	margin: 4px 0 0 0;
    	padding: 2px .6em 2px 10px;
    	font-weight: bold;
    	background: #EBEBEB /*savepage-url=/img/tracker_sprite_arrows.gif*/ url() no-repeat 10px -316px;
    }

    #recommendations .panel ul li h3.bundleTitle {
    	background-color: #fff;
    }

    #recommendations .panel ul li h3 span {
    	padding-left: 14px;
    	cursor: pointer;
    	color: #565656;
    }

    #recommendations .panel ul li.off h3 {
    	background-position: 10px -357px;
    }

    #recommendations .panel ul li.off ul {
    	display: none;
    }

    #recommendations .panel ul li .tag-list {
    	padding: 0 10px;
    	margin: .2em 0 .4em -4px;
    }

    #recommendations .panel ul li .tag-list li {
    	display: inline;
    	float: left;
    	padding: 0;
    	line-height: 1.5em;
    	background: #fff;
    	margin: 0 1px 1px 0;
    }

    #recommendations .panel ul li .tag-list li a {
    	text-decoration: none;
    	padding: 0 .4em;
    	display: inline;
    	float: left;
    	background: #fff;
    }

    #recommendations .panel ul li .tag-list li a:hover {
    	background: #565656;
    	color: white;
    }

    #recommendations .network-tags .alias a {
    	border-bottom: 1px solid #bbb;
    	font-weight: bold;
    }

    #recommendations .network-tags .alias.on a {
    	border-bottom: 1px solid white;
    }

    .popup #recommendations .network-tags .alias a {
    	border: none;
    	font-weight: bold;
    }

    #recommendations .panel ul li .tag-list li a em {
    	display: none;
    }

    #recommendations .panel ul li .tag-list li a.size0 {
    	color: #B4D2E4;
    }

    #recommendations .panel ul li .tag-list li a.size1 {
    	color: #73ADFF;
    	color: #3274D0;
    }

    #recommendations .panel ul li .tag-list li.on {
    	display: inline;
    	float: left;
    }

    #recommendations .panel ul li .tag-list li.on a {
    	color: #fff;
    	background: #8F8F8F;
    }

    #recommendations .panel ul li .tag-list li a:hover {
    	background-color: #666;
    	text-decoration: none;
    	color: #fff !important;
    }

    #recommendations .panel ul li#save-bundle-tags ul {
    	padding: 0;
    }

    #recommendations .panel ul li#save-bundle-tags h3 {
    	margin-bottom: .4em;
    }

    #recommendations .panel ul li#save-bundle-tags ul li h3 {
    	margin: 0;
    }

    #recommendations .panel ul li#save-bundle-tags ul li .tag-list {
    	margin: 0 0 0 6px;
    }

    .blankslate {
    	padding-bottom: .4em;
    }

    .blankslate h3 {
    	font-size: 105%;
    	font-weight: bold;
    	padding-top: .75em;
    }

    .blankslate p {
    	font-size: 92%;
    	margin: 0.5em 0;
    }

    .blankslate p.learnmore {
    	text-align: right;
    	padding-right: 20px;
    	font-size: 96%;
    	margin: 0.5em 0;
    	background: transparent
    }

    .btn {
    	height: auto;
    	float: left;
    	overflow: hidden;
    	cursor: pointer;
    	padding-left: 0;
    	padding-right: 0;
    	border-radius: 2px;
    	text-align: center;
    	min-width: 100px;
    	box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
    }

    .btn button {
    	display: block;
    	height: 100%;
    	border: none;
    	color: #FFF;
    	padding: 0px 15px !important;
    	font-weight: bold;
    	cursor: pointer;
    	margin: 0;
    	font-size: 20px;
    	font-family: arial !important;
    	line-height: 34px;
    	text-align: center;
    	min-width: 100px;
    }

    .btn a {
    	display: block;
    	text-decoration: none;
    	color: #FFF;
    	font-weight: bold;
    	line-height: 30px;
    	cursor: pointer;
    	font-family: arial !important;
    	font-size: 20px;
    	text-align: center;
    	padding: 2px 15px !important;
    	background: #CCC;
    }

    .btn.selected a{
    	background: #333 !important;
    }

    .btn.selected a:HOVER{
    	background: #999 !important;
    }


    .btn button:hover, .btn button.iehover, .btn a:hover {
    	color: #000;
    	text-decoration: none;
    	cursor: pointer;
    }

    .is_firefox .btn button {
    	padding-bottom: 1px !important;
    }

    .is_safari3 .btn button {
    	padding-top: 2px !important;
    }

    .btn-blue {
    	background-color: #3274D0;
    	background-position: left -1200px;
    }

    .btn-blue button, .btn-blue a {
    	background-position: right -1250px;
    	padding-right: 15px;
    }

    .btn-blue button {
    	_padding: 0 .5em;
    	*padding: 0 .5em;
    }

    .btn-blue-r {
    	background-color: #3274D0;
    	background-position: left -1200px;
    }

    .btn-blue-r button, .btn-blue-r a {
    	background-position: right -1300px;
    }

    .btn-blue-r button:hover, .btn-blue-arrow button.iehover, .btn-blue-r a:hover {
    	background-position: right -1350px;
    }

    .btn-blue-r-gray {
    	background-color: #3274D0;
    	background-position: left -1600px;
    }

    .btn-blue-r-gray button, .btn-blue-r-gray a {
    	background-position: right -1700px;
    }

    .btn-blue-r-gray button:hover, .btn-blue-r-gray button.iehover, .btn-blue-r-gray a:hover {
    	background-position: right -1750px;
    }

    .btn-blue-blue {
    	background-color: #3274D0;
    	background-position: left -2000px;
    }

    .btn-blue-blue button, .btn-blue-blue a {
    	background-position: right -2050px;
    	padding-right: 15px;
    }

    .btn-blue-blue button {
    	_padding: 0 .5em;
    	*padding: 0 .5em;
    }

    .btn-green {
    	background-color: #7cba0f;
    	background-position: left 0;
    }

    .btn-green button, .btn-green a {
    	background-position: right -50px;
    	padding-right: 15px;
    }

    .btn-green button {
    	_padding: 0 .5em;
    	*padding: 0 .5em;
    }

    .btn-green-gray {

    	background-position: left -400px;
    }

    .btn-green-gray button, .btn-green-gray a {
    	background-position: right -450px;
    	padding-right: 15px;
    	background-color: #4caf50;
    }



    .btn-green-r {
    	background-color: #7cba0f;
    	background-position: left 0;
    }

    .btn-green-r button, .btn-green-r a {
    	background-position: right -100px;
    }

    .btn-green-r button:hover, .btn-green-r a:hover, .btn-green-r button.iehover {
    	background-position: right -150px;
    }

    .btn-green-r-gray {

    	background-position: left -400px;
    }

    .btn-green-r-gray button, .btn-green-r-gray a {
    	background-position: right -500px;
    	background-color: #7cba0f;
    }

    .btn-green-r-gray button:hover, .btn-green-r-gray a:hover, .btn-green-r-gray button.iehover {
    	background-position: right -550px;
    }

    .btn-green-u {

    	background-position: left 0;
    }

    .btn-green-u button, .btn-green-u a {
    	background-position: right -200px;
    	background-color: #7cba0f;
    }

    .btn-green-u button:hover, .btn-green-u a:hover, .btn-green-u button.iehover {
    	background-position: right -250px;
    }

    .btn-gray {
    	background-color: #c1c1c1;
    	background-position: left -2400px;
    }

    .btn-gray button, .btn-gray a {
    	background-position: right -2450px;
    	padding-right: 15px;
    }

    .btn-gray button {
    	_padding: 0 .5em;
    	*padding: 0 .5em;
    }

    .btn-gray-gray {
    	background-color: #c1c1c1;
    }

    .btn-gray-gray button, .btn-gray-gray a {
    	background-position: right -2550px;
    	padding-right: 15px;
    }

    .btn-gray-gray button {
    	_padding: 0 .5em;
    	*padding: 0 .5em;
    }

    .btn-gray-blue {
    	background-color: #c1c1c1;
    	background-position: left -2600px;
    }

    .btn-gray-blue button, .btn-gray-blue a {
    	background-position: right -2650px;
    	padding-right: 15px;
    }

    .btn-gray-blue button {
    	_padding: 0 .5em;
    	*padding: 0 .5em;
    }

    .arrow-blue {
    	padding: 2px 25px 2px 0;
    	background: /*savepage-url=/img/tracker_sprite_arrowButtons.gif*/ url() no-repeat right -100px;
    }

    .arrow-blue:hover {
    	background-position: right -150px;
    }

    #saveitem .checkboxfield {
    	height: auto;
    	margin-bottom: 5px;
    	padding-bottom: 20px;
    }

    #saveitem legend{
    	width:  100%;
    }

    #saveitem fieldset:not(.full_width) legend {
    	margin-left: -100px;
    	width: calc( 100% + 100px);
    }

    #saveitem legend {
    	padding-bottom: 10px;
    	padding-top: 1px;
    	margin-bottom: 22px;
    	border-bottom: 1px solid #EFEFEF;
    	color: #000;
    	background: #f9f9f9;
    	box-sizing: border-box !important;
    }

    #saveitem fieldset.full_width legend span{
    	text-align: center;
    }

    #saveitem .checkboxfield input {
    	width: 10px !important;
    	position: relative;
    	margin-right: 5px;
    	margin-left: 3px;
    }

    #saveitem .checkboxfield .checkboxspan {
    	display: block;
    	border-bottom: 1px solid #EEE;
    	background: #F7F7F7;
    	padding: 4px;
    }

    #saveitem .checkboxfield .checkboxspan:hover {
    	background: #F4F4F4;
    }

    #taskdetailsfield {
    	height: 13em !important;
    }

    #taskdetailsfield textarea {
    	height: 10.1em !important;
    }

    .checkbox_label {
    	display: inline !important;
    	position: inherit !important;
    }

    .imagesfield, .audiofield, .images_newfield {
    	height: auto !important;
    }

    .audioIFrame {
    	width: 100%;
    	border: 1px solid #AAA;
    	background: #FFF;
    	overflow: hidden;
    	height: 90px;
    }

    .imageIFrame {
    	width: 100%;
    	border: 1px solid #AAA;
    	background: #FFF;
    }

    .imageIFrame_new {
    	width: 200px;
    	height: 40px;
    	border: 3px dashed #999;
    	background: #DDD;
    	display: block;
    	margin: 10px;
    }

    .imagesfield .info, .images_newfield .info, .audiofield .info {
    	position: relative !important;
    }

    .sortlist_holder ul li .sortlist_handle {
    	height: 20px;
    	background: /*savepage-url=/img/drag_me.png*/ url() no-repeat 10px center;
    	background-color: #AAAAAA;
    	cursor: move;
    	color: #AAAAAA;
    	font-size: 9px;
    	text-indent: -100px;

    }

    .ui-state-highlight{
    	min-height: 100px;
    	background-color: #2E9AFE;
    	background-image: -webkit-gradient(linear, 0 0, 100% 100%, color-stop(.25, rgba(255, 255, 255, .2)), color-stop(.25, transparent), color-stop(.5, transparent), color-stop(.5, rgba(255, 255, 255, .2)), color-stop(.75, rgba(255, 255, 255, .2)), color-stop(.75, transparent), to(transparent));
    	background-image: -webkit-linear-gradient(-45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);
    	background-image: -moz-linear-gradient(-45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);
    	background-image: -ms-linear-gradient(-45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);
    	background-image: -o-linear-gradient(-45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);
    	background-image: linear-gradient(-45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);
    	background-size: 50px 50px;
    	border: 0px !important;
    }


    .sortlist_handle_dontmove {
    	color: #AAAAAA;
    	font-size: 9px;
    	padding-left: 3px;
    }

    .sortlist_remove {

    	background-color: #000000;
/*    margin-right: -30px;
margin-top: -15px;*/
margin-top: -20px;
background: transparent /*savepage-url=../img/delete.png*/ url() no-repeat scroll center center;
cursor: pointer;
float: right;
height: 20px;
width: 20px;
}

.sortlist_holder ul li {
	border-top: #AAAAAA solid 1px;
	border-left: #AAAAAA solid 2px;
	margin-bottom: 10px;
	position: relative;
}

.content_fieldset {
	font-size: 25px;
	font-weight: normal;
	margin: 0px 0px 10px 0px;
	color: #212121;
	background-color: transparent;
	border-left: 0px;
	padding: 10px 20px;
}

.content_fieldset_divider {
	border-top: 1px dotted #AAAAAA;
	margin-bottom: 5px;
	padding-top: 5px;
	width: 70%;
}

.embedvideofield {
	height: 150px !important;
}

.embedvideofield .embedVideoSectionTextarea {
	width: 300px !important;
	height: 100px;
}

.embedvideofield .embedVideoSectionSubmit {
	width: 100px !important;
	height: auto;
	margin-top: 110px;
	margin-left: 205px;
}

.embedvideofield .embedVideoSectionInfo {
	margin-left: 320px;
	color: #888888;
}

.embedvideofield .embedVideoSectionInfo span {
	font-size: 9px;
	display: block;
	margin: 3px;
}

.embedvideofield .embedVideoSectionRemove {
	background: transparent /*savepage-url=../img/delete.png*/ url() no-repeat scroll 2px center;
	height: 20px;
	width: 120px;
	margin-left: 320px;
	cursor: pointer;
	display: none;
	text-indent: 20px;
	background-color: #DDD;
	padding-top: 4px;
}

.embedvideofield .embedVideoSectionPlayer {
	float: left;
}

.colorpickerfield div img {
	cursor: pointer;
}

.colorpickerfield div {
	cursor: pointer;
	width: 24px;
	height: 24px;
	border: 1px solid #AAAAAA;
	float: left;
	padding: 2px;
	border-right: none;
	border-radius: 0px 0 0 0px;
	position: relative;
}

.colorpickerfield div img {
	opacity: 1;
	width: 15px;
	position: absolute;
	bottom: 2px;
	right: 2px;
}

.colorpickerfield input {
	width: 100px !important;
	position: relative !important;
	padding: 5px 5px 3px !important;
	font-size: 19px !important;
	color: #444;
	background: #CCC;
	border: none !important;
	margin-left: 3px;
}

div.slider {
	width: 200px;
	height: 16px;
	background: #eee;
}

div.slider div.knob {
	background: /*savepage-url=/img/slider/blue.png*/ url() no-repeat center;
	width: 16px;
	height: 16px;
}

div.slider {
	background: /*savepage-url=/img/slider/slider.png*/ url() repeat-x center;
}

.hide_legend{
	x-background: #EFEFEF !important;
	cursor: pointer;
}

.hide_legend span{
	position: relative !important;
}

.hide_legend span:after{
	content: '';
	position: absolute;
	left: 50%;
	width: 150px;
	height: 20px;
	margin-left: -75px;
	background:  #DDD;
	display: block;
	content:  '\e5ce';
	display: block;
	font-family: 'Material Icons';
	text-align: center;
	line-height: 20px;
}

.hide_legend:HOVER{
	background: #DDD !important;
}


.hide_legend span.open:after{
	content:  '\e5cf';
}

.hide_legend:HOVER span:after{
	background:  #333;
	color: #FFF;
}

.

/* .hide_legend span {
    background-image: url(/img/silk_icons/add.png) !important;
    background-position: right !important;
    background-repeat: no-repeat !important;
    cursor: pointer;
    width: 97% !important;
}

.hide_legend span.open {
    background-image: url(/img/silk_icons/arrow_up.png) !important;
    } */

    .calendarfield {
    	height: auto !important;
    }

    .calendarfield .info {
    	position: inherit !important;
    }

    .calendar {
    	background-color: #999;
    	background-position: center bottom;
    	background-repeat: no-repeat;
    	color: #000000;
    	margin: 5px;
    	width: 280px;
    	font-size: 12px !important;
    }

    .calendar table {
    	width: 100%;
    }

    .calendar caption {
    	background-position: center top;
    	font-size: 16px;
    	line-height: 30px;
    	margin: 0 auto;
    	color: #FFF;
    }

    .calendar caption .nextmonth {
    	border-width: 0;
    	float: right;
    	background: #FFF;
    	color: #999 !important;
    }

    .calendar caption .prevmonth {
    	border-width: 0;
    	float: left;
    	background: #FFF;
    	color: #999 !important;
    }

    .calendar caption a {
    	border-color: #EEEEEE;
    	border-style: solid;
    	display: block;
    	font-weight: bold;
    	height: 30px;
    	width: 30px;
    	font-size: 20px;
    	line-height: 29px;
    }


    .calendar caption a:hover {
    	background: rgba(0,0,0,0.1) !important;
    	color: #FFF !important;
    }

    .calendar tbody {
    }

    .calendar thead {
    	background-color: #DDD;
    	color: #333;
    }

    .calendar th, .calendar .today {
    	font-weight: bold;
    }

    .calendar td, .calendar th {
    	height: 24px;
    	text-align: center;
    }

    .calendar td {
    	background-color: #EFEFEF;
    	border-color: #DDDDDD;
    	border-style: solid;
    	border-width: 2px 2px 0 0;
    	font-size: 14px;
    	line-height: 24px;
    }

    .calendar tr td:first-child {
    	border-width: 2px 2px 0;
    }

    .calendar td.lastmonth, .calendar td.nextmonth {
    	background-color: #909090;
    }

    .calendar td.selected {
    	background-color: #FF358B !important;
    }

    .calendar td.lastmonth a, .calendar td.nextmonth a {
    	color: #BBBBBB !important;
    }

    .calendar {
    	color: #666;
    }

    .calendar a {
    	color: #000;
    	display: block;
    	font-size: 14px;
    	height: 24px;
    	line-height: 24px;
    }

    .calendar a:hover {
    	text-decoration: none;
    }

    .calendar td:hover {
    	background-color: #FFFFFF;
    }

    .calendar td:hover a {
    	background-color: #FFFFFF;
    	color: #000000;
    }

    .calendar td:hover a:hover {
    	color: #3274D0;
    }

    .calendar td.today {
    	background-color: #FFFFFF;
    }

    .calendar td.today a {
    	color: #000000;
    }

    .calendar td.weekend {
    	background-color: #666666;
    	font-weight: bold;
    }

    .calendar td.weekend {
    	color: #DDD !important;
    }

    .calendar td.weekend a, .nextmonth, .prevmonth {
    	color: #FFF !important;
    }

    .calendar td.weekend a:hover, .calendar td.weekend:hover a {
    	color: #3274D0 !important;
    }

    .calendar td.holiday {
    	background-color: #AABBAA;
    	font-weight: bold;
    }

    .calendar td.past a {
    	color: #DDDDDD;
    }

    .calendar td.past {
    	background: #999999;
    }

    .calendar .match {
    	background: #8FD400 none repeat scroll 0 0;
    }

    .calendar .match {
    	color: #FFFFFF !important;
    }

    .calendar .match:hover {
    	color: #8FD400 !important;
    }

    .gateway_buttonfield, .gateway_button_newfield {
    	height: auto !important;
    }

    .gateway_buttonfield .info_text, .gateway_button_newfield .info_text {
    	display: block;
    	color: #333;
    	font-size: 14px;
    	font-weight: bold;
    	margin-top: 10px;
    }


    .gateway_buttonfield input, .gateway_button_newfield input {

    	position: relative !important;
    	width: 150px !important;
    }

    .onofffield {
    	height: auto !important;
    }

    .onofffield div {
    	background-color: #DDDDDD;
    	margin: 2px;
    	padding: 4px;
    }

    .onofffield input {
    	width: 20px !important;
    	color: #red;
    	position: relative !important;
    	margin-right: 5px;

    }

    .onofffield .ononfftext {
    	font-weight: bold;
    	font-size: 16px;
    }

    .onofffield .info {
    	position: relative !important;
    }

    .change_passwordfield .maindiv {
    	background-color: rgba(0,0,0,0.1);
    	padding: 10px 0px;
    }

    .change_passwordfield .info {
    	position: relative !important;
    }

    .change_passwordfield {
    	height: auto !important;
    }

    .change_passwordfield input {
    	width: 150px !important;
    	position: relative !important;
    	margin: 8px;
    	display: block;

    }

    .change_passwordfield .passwordnotetop, .change_passwordfield .passwordnote, .change_passwordfield .passwordnoteconfirm {
    	display: block;
    	width: 70px;
    	float: left;
    	clear: both;
    	text-align: right;
    	margin: 2px 10px;
    }

    .change_passwordfield .passwordnotetop {
    	margin-top: 10px;
    }

    .change_passwordfield .passwordnoteconfirm {
    	margin-top: 5px;
    }

    .change_passwordfield table {
    	float: left;
    	margin-right: 5px;

    }

    .change_passwordfield .passwordtable {
    	display: block;
    	color: #999;
    	margin-left: 90px;
    }

    .old_password ~ .new_password_section{
    	margin-top: 20px;
    }

    .new_password_section{
    	position: relative;

    }

    .unmask_pw{
    	position: absolute;
    	top: 10px;
    	right: 20px;
    	font-weight: 500;
    	cursor: pointer;
    	z-index: 10;
    	-moz-user-select: none;
    	-webkit-user-select: none;
    }

    .unmask_pw ul li:first-child{
    	display: block;
    }

    .unmask_pw ul li:last-child{
    	display: none;
    }

    .unmask_pw.show ul li:first-child{
    	display: none;
    }

    .unmask_pw.show ul li:last-child{
    	display: block;
    }

    .unmask_pw ul li{
    	font-weight: 700;
    }

    .unmask_pw ul li svg{
    	position: relative;
    	top: -5px;
    	fill: #333;
    }

    .unmask_pw ul li:HOVER svg{
    	fill: #000;
    }


    .chart_linefield .info {
    	position: relative !important;
    }

    .chart_linefield {
    	height: auto !important;
    }

    .chart_linefield label {
    	display: none !important;
    }

    .purchase_detailsfield {
    	height: auto !important;
    }

    .purchase_detailsfield table {
    	width: 100%;
    	font-size: 14px;
    	margin-bottom: 10px;
    }

    .purchase_detailsfield table thead {
    	font-weight: bold;
    	border-bottom: 1px solid #333;
    	padding: 3px;
    }

    .purchase_detailsfield table thead td {
    	border-bottom: 1px solid #999;
    }

    .purchase_detailsfield table tbody td {
    	background-color: #FFF;
    	padding: 3px;
    }

    .purchase_detailsfield table tfoot td {
    	background-color: #FFF;
    	padding: 3px;
    	font-size: 16px;
    	font-weight: bold;
    }

    .purchase_detailsfield table .order_id_note td {
    	border-top: 1px solid #999;
    	background: #3274D0;
    	color: #FFFFFF;
    	padding: 4px;
    }

    .purchase_detailsfield table .order_id td {
    	font-weight: bold;
    	font-size: 14px;
    }

    .purchase_detailsfield table .start_bank_details td {
    	border-top: 1px solid #999;
    }

    #saveitem .textareafield {
    	height: auto;
    }

    #saveitem .textareafield .info {
    	padding-bottom: 10px;
    }

    .textareafield textarea {
    	height: 200px;
    }

    .purchase_detailsfield #completed_note {
    	margin: 5px 0px;
    	color: #FFF;
    	font-size: 14px;
    	background: #FF358B;
    	padding: 4px;
    }

    #saveitem .richtextfield {
    	height: auto !important;
    }

    .is_ie7#saveitem .richtextfield {
    	height: auto !important;
    }

    .richtextfield .info {
    	position: relative !important;
    }

    #saveitem .richtextfield textarea {
    	height: 11.7em !important;
    }

    .is_ie7#saveitem .richtextfield textarea {
    	height: 12em !important;
    }

    .radio_buttonsfield > div{
    	position: relative;
    }

    .radiobutton {
    	padding: 5px 10px;
    	float: left;
    	margin-right: 10px;
    	border: 0px;
    	border-radius: 2px;
    	background: #DDD;
    	cursor: pointer;
    	font-weight: bold;
    	color: #333;
    	margin-bottom: 8px;
    	padding: 10px 20px;
    	background: rgba(158,158,158,.2);
    	box-shadow: 0 2px 2px 0 rgba(0,0,0,.04), 0 3px 1px -2px rgba(0,0,0,.1), 0 1px 5px 0 rgba(0,0,0,.02);

    }

    .radiobutton:focus{
    	outline: none;
    }


    .sortlist_holder ul li .radiobutton {
    	margin-left: 5px;
    	margin-right: 3px;
    }



    .radiobutton.selected {
    	background: #4caf50;
    	color: #FFF;
    	box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);
    }

    .radio_buttonsfield {
    	height: auto !important;
    }

    .radio_buttonsfield .info {
    	position: relative !important;

    }

    .selecthtml {
    	border: 2px solid #999;
    	padding: 5px 10px;
    	float: left;
    	margin-right: 8px;
    	border-radius: 5px;
    	background: #DDD;
    	cursor: pointer;
    	font-weight: bold;
    	color: #FFF;
    	margin-bottom: 5px;
    }

    .radiobutton:HOVER {
    	background: #333;
    	color: #FFF ;
    }





    .select_htmlfield, .checkbox_htmlfield {
    	height: auto !important;
    }

    .select_htmlfield .info, .checkbox_htmlfield .info {
    	position: relative !important;

    }

    .displaytextfield, .displayhtmlfield{
    	margin-top: 10px !important;
    	height: auto !important;
    	background-color: #FFF9C4;
    	margin-bottom: 20px !important;
    	color: #212121;
    	font-size: 16px;
    	padding: 16px;
    	border-radius: 2px;
    }
    .displaytextfield h3{
    	position: relative;
    	left: -20px;
    	margin-top: -17px;
    	margin-bottom: 4px;
    }

    .displaytextfield h3 div{
    	color: #FFF;
    	border-right: 3px solid #EFEFEF;
    	border-bottom: 3px solid #EFEFEF;
    	position: relative;
    	padding: 5px 8px 3px;
    	background: #E28675;
    	font-size: 14px;
    	display: inline-block;
    	min-width: 200px;
    	text-align: center;
    	font-weight: normal;
    }



    .content_fullpage .displaytextfield,.content_fullpage .displayhtmlfield{
    	margin-left: 0px;
    	margin-right: 0px;
    }

    .displaytextfield label, .displayhtmlfield label{
    	display: none !important;
    }

    .selectHtmlUnit,  .checkboxHtmlUnit, .selectcountHtmlUnit {
    	height: auto !important;
    	background: rgba(158,158,158,.2);
    	padding: 10px;
    	-moz-user-select: none;
    	-webkit-user-select: none;
    	margin-top: 5px !important;
    	margin-bottom: 3px;
    	cursor: pointer;
    	position: relative;
    	box-shadow: 0 2px 2px 0 rgba(0,0,0,.04), 0 3px 1px -2px rgba(0,0,0,.1), 0 1px 5px 0 rgba(0,0,0,.02);
    }
    .selectcountHtmlUnit {
    	cursor: initial;
    }

    .selectHtmlUnit:HOVER:NOT(.disabled), .checkboxHtmlUnit:HOVER:NOT(.disabled) {
    	background-color: #333;
    	color:  #FFF;
    }



    .selectHtmlUnit.selected, .checkboxHtmlUnit.selected, .selectcountHtmlUnit.selected {
    	color: #FFF;
    	background: #4caf50;
    	box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);
    }

    .selectHtmlUnit.disabled, .selectcountHtmlUnit.disabled{
    	opacity: 0.5;
    	cursor: inherit;
    }


    .captchafield {
    	height: 80px !important;
    	background-color: rgba(0,0,0,0.1);
    	padding: 10px;
    	margin-bottom: 20px !important;
    	margin-top: 10px;
    }

    .captchafield input {
    	width: 100px !important;
    	position: relative !important;
    	margin-left: 30px;
    }

    .captchafield img {
    	display: block;
    	width: 105px;
    	margin-left: 30px;
    	margin-bottom: 10px;

    }

/*.displaytextfield .info, .captchafield .info{
    position: relative !important;
}
*/
.displaytextfield span {
	font-size: 15px;
	line-height: 1.7em;
}

.displaytextfield span.notsobig {
	font-size: 12px;
}

.displaytextfield span strong {
	background: #c62828;
	padding: 0px 5px;
	color: #FFF;
	display: inline-block;
	margin: 0px 5px;
	line-height: 1.4em;
}


.location_select_subfield {
	padding-bottom: 8px !important;
}


.removefromlist, .removefrom {
	position: absolute;
	width: 18px;
	height: 18px;
	background: rgba(100,100,100,0.1) /*url('/images/silk_icons/cross.png')*/ center center no-repeat;
	top: 0px;
	right: 3px;
	cursor: pointer;
	z-index: 100;
	border-radius: 5px;
	margin-top: 4px;
}

li:HOVER .removefromlist, li:HOVER .removefrom {
	background-color: #DDD;
}

.removefromlist:HOVER, .removefrom:HOVER {
	background: #CCC /*savepage-url=/img/silk_icons/cross.png*/ url() center center no-repeat;
}

.removeconfirm {
	background: #CCC;
	color: #333;
	position: absolute;
	right: 0px;
	top: 16px;
	min-width: 50px;
	padding: 2px 5px;
	font-size: 12px;
	text-align: center;
	border-radius: 5px 0px 5px 5px;
}

.removeconfirm:HOVER {
	background: #333;
	color: #FFF;
}



body.lightbox_mode .bottombuttons .submitbttns {
	padding-bottom: 50px !important;
	height: 30px;

}

body.lightbox_mode .bottombuttons .submitbttns .buttons {
	display: block !important;
	float: none !important;
	margin-right: 0 !important;
	padding-bottom: 1px;
}

body.lightbox_mode .bottombuttons .submitbttns .buttons .btn {
	height: auto;
	border-radius: 20px;
	background: transparent;
	float: none !important;
	margin: 0px auto !important;
	width: 300px !important;
	margin-bottom: 10px !important;
	text-align: center;
}



body.lightbox_mode  .bottombuttons .submitbttns button{
	color: #333;
	text-align: center;
	font-size: 22px;
	padding-top: 17px !important;
	padding-bottom: 17px !important;
	width: 300px;
	border-radius: 20px;
	float: none;
	line-height: 26px;
}

body.lightbox_mode .bottombuttons .submitbttns button:HOVER{
	background: #3399FF;
	color: #FFF;
}


.float_bottombuttons .bottombuttons{
	position: fixed;
	bottom: 0px;
	background: rgba(0,0,0,0.5);
	left: 0px;
	margin: 0px !important;
	right: 0px;
	padding: 3px 35px 10px !important;
}


#saveitem.float_bottombuttons fieldset {
	margin-bottom: 60px;
}

form#saveitem.has_errors{
	margin-bottom: 20px !important;
}

.textareafield textarea.short_textarea{
	height: 60px;
}

.changelogDiv{
	background: rgba(0,0,0,0.1);
	border-radius: 0px;
	color: #444;
	font-size: 10px !important;
	line-height: 12px !important;
	padding: 5px 10px !important;
}

.changelogDiv .changelog{
	font-size: 12px !important;
	line-height: 16px !important;
	font-family: monospace !important;
}

.displaytextfield.changelogDiv label {
	display: block !important;
}

.fulllistvals li{
	border-bottom: 1px solid #DDD; 
	padding-bottom: 10px;
}

.fulllistvals li span{
	background: #DDD; 
	padding: 3px 10px; 
	line-height: 20px; 
	display: inline-block; 
	margin-right: 3px;
	margin-bottom: 3px;
	vertical-align: top;
	min-height: 20px;
}

.fulllistvals span[data-type='json'], .fulllistvals span[data-type='textarea']{
	white-space: pre;
}

.fulllistvals span[data-type='json']{
	background: #272822;
	color: #e6db74;
}

.fulllistvals span[data-type='select']{
	background-color: #666;
	color: #FFF;
}

.youtube_playlist_content iframe, .soundcloud_user_content iframe,  .spotify_artist_content iframe{
	border: 0px;
	margin: 10px;
	width: 300px;
	height: 170px;
}

.soundcloud_user_content iframe{
	height: 220px;
}

.spotify_artist_content iframe{
	height: 300px;
}


.auto-complete-desc {
	margin: 0;
	padding: 0px;
	background: #FFF;
}

.auto-complete-desc span {
	padding: 5px;
	display: inline-block;
	color: #444;
}

.formautocomplete{
	background-color: #F4C9C9;
}
.formautocomplete.set{
	background-color: #c9dba1;
}

.ui-autocomplete {
	max-height: 200px;
	overflow-y: auto;
	/* prevent horizontal scrollbar */
	overflow-x: hidden;
}
  /* IE 6 doesn't support max-height
   * we use height instead, but this forces the menu to always be this tall
   */
   * html .ui-autocomplete {
   	height: 200px;
   }
   .ui-widget-content a span {
   	color: #999;
   	display: inline-block;
   	margin-left: 8px;
   }

   .lazy__button{
   	cursor: pointer;
   	background: #FF0000;
   	color: #FFF;
   	display: inline-block;
   	border-bottom: 2px solid rgba(100,100,100,0.3);
   	padding: 3px 8px;
   	border-radius: 3px;
   	font-size: 12px;
   }


   .lazy__button:HOVER{
   	background: #333 !important;
   }

   body .bottombuttons .submitbttns .buttons .btn.btn-green-gray, body .bottombuttons .submitbttns button {
   	background-color: #4caf50 !important;
   	border-radius: 3px !important;
   }

   .btn-green-gray {
   	box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);
   	border-bottom: 0px;
   	background-color: #4caf50;
   }

   .btn-gray-gray {
   	border-radius: 3px !important;
   }

   @media only screen and (max-width: 663px) { 

   	.textfield .alt_langs, .textareafield .alt_langs, .richtextnewfield .alt_langs{
   		top: 26px;
   	}

   	#saveitem fieldset {
   		margin: 10px 10px 10px 10px;
   	}

   	#saveitem label {
   		display: block;
   		position: relative;
   		left: 0px;
   		text-align: left;
   		width: 95%;
   		font-size: 16px;
   		margin: 0px 0px 5px;
   	}

   	#saveitem label strong{
   		top: 0px;
   		left: 0px;
   	}

   	#saveitem input, #saveitem textarea {
   		position: relative;
   		left: 0;
   		top: 0;
   		width: 100%;
   		padding: 5px;
   		font-size: 20px;
   		border: 1px solid #DDD;
   	}

   	.captchafield img {
   		float: left;
   		margin-left: 10px;
   	}

   	#saveitem textarea {
   		font-size: 14px;
   	}

   	#saveitem .field {
   		height: auto;
   	}

    /*
    #saveitem .field .info {
        position: relative;
    }

        #saveitem .field .info.required .txt {
    position: relative;
    bottom: 0px;
    right: 0px;
    }
    */

    #saveitem fieldset legend span {
    	width: 200px;
    	display: block;
    	font-size: 20px;
    	padding: 3px 5px;
    	margin-left: 0px;
    }


    #saveitem select {
    	font-size: 18px !important;
    	max-width: 300px;
    }


    #saveitem .textareafield {
    	height: auto !important;
    }

    .datefield {
    	height: auto !important;
    }

    #saveitem fieldset {
    	margin: 10px !important;
    	position: relative !important;
    	width: auto !important;
    	padding: 0px !important;
    	box-sizing: content-box !important;
    }

    #saveitem legend {
    	margin-left: 0px !important;
    	display: block !important;
    	padding-right: 0px !important;
    	margin-right: 0px !important;
    	margin: 10px 0px !important;
    	position: relative !important;
    	width: 100% !important;
    	text-align: center !important;

    }

    #saveitem fieldset legend span {
    	width: auto !important;
    	display: block !important;
    	padding: 3px 5px;
    	margin-left: 0px !important;
    	float: none !important;
    	left: 0px !important;
    	text-align: center !important;
    }

    #saveitem .submitbttns{
    	height: auto;
    	padding: 0px;
    	margin: 0px;
    	text-align: center;
    	padding-bottom: 0px !important;
    	margin-top: 20px;
    }

    .btn-green-r-gray:HOVER, .btn-gray-gray:HOVER{
    	background-color: rgba(158,158,158,.2) !important;
    }

    .bottombuttons .submitbttns button:HOVER {
    	color: #FFF;
    }

    .bottombuttons .submitbttns button {
    	color: #333;
    	text-align: center;
    	font-size: 22px;
    	padding-top: 17px !important;
    	padding-bottom: 17px !important;
    	width: 300px;
    	border-radius: 20px;
    	float: none;
    	line-height: 26px;
    }

    .bottombuttons .submitbttns .buttons .btn {
    	height: auto;
    	border-radius: 20px;
    	background: transparent;
    	float: none !important;
    	margin: 0px auto !important;
    	width: 300px !important;
    	margin-bottom: 10px !important;
    	text-align: center;
    }

    #saveitem .submitbttns .buttons{
    	float:  none;
    }


    .cal_holder:NOT(.icon_open) {
    	margin: 0px auto 10px;
    	display: block !important;
    	width: 290px;
    }

    #saveitem fieldset:NOT(.full_width) legend span{
    	padding:  3px 5px;
    }

    .hide_legend span:after{
    	bottom: -25px;
    }


}


#no-content {
	background-color: #F9F9F9;
	box-shadow: rgba(43, 59, 93, 0.290196) 0px 1px 2px 0px;
	margin: 20px;
}

#no-content .no-content-box {
	padding: 1em;
	margin-top: 1em;
}

#no-content #errbody {
	border-top: 5px solid #E9E9E9;
	margin-top: 5px;
	padding-top: 2px;
	padding: 2px 0.5em;
}

#caption h3 span{
	display: block;
	padding: 0.3em;
	font-weight: 300;
	background: #FFF9C4;
	font-size: 24px;
	padding-left: 15px;
}

.mdl-card #no-content {
	margin: 0px;
	box-shadow: none;
}

.right-options {
	text-align: right;
	margin-right: 1em;
}

.right-options div {
	float: right;
	margin-bottom: 10px;
	position: relative;
	z-index: 10;

}



.cf:before,
.cf:after {
	content: " "; /* 1 */
	display: table; /* 2 */
}

.cf:after {
	clear: both;
}

/**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
 .cf {
 	*zoom: 1;
 }



 .other_play{
 	background-repeat: no-repeat;
 }

 .video_play, .soundcloud_play, .other_play, .all_play, .youtube_play {
 	width: 120px;
 	height: 90px;
 	margin: 3px 0px !important;
 	cursor: pointer;
 	position: relative;
 	background-position: left top;
 	background-repeat: no-repeat;
 }

 .video_play span, .soundcloud_play span, .other_play span, .all_play span, .youtube_play span, .embed_play span {
 	display: block;
 	width: 26px;
 	height: 26px;
 	bottom: 4px;
 	left: 4px;
 	position: absolute;
 	border-radius: 5px;
 	background: #3274D0 /*savepage-url=/img/silk_icons/control_play_blue.png*/ url() center center no-repeat;

 }

 .video_play:HOVER span, .soundcloud_play:HOVER span, .other_play:HOVER span, .all_play:HOVER span, .youtube_play:HOVER span, .embed_play:HOVER span {

 	background: #F84713 /*savepage-url=/img/silk_icons/control_play.png*/ url() center center no-repeat;
 }

 .video_play.opened, .soundcloud_play.opened, .other_play.opened, .all_play.opened, .youtube_play.opened {
 	width: 425px;
 	height: 270px;
 	background: #EFEFEF /*savepage-url=/img/spinner.gif*/ url() no-repeat center center !important;
 }


 @media only screen and (max-width: 663px){



 	#form-confirm-list > li strong{
 		display: block;
 		margin-bottom: 10px;
 		width: auto;
 		text-align: left;
 		margin-right: 0px;
 		float: none;
 	}

 	#form-confirm-list > li span.formitem{
 		padding-left: 0px;
 	}

 	.right-options{
 		margin-right: 10px;
 	}

 	#navigation li{
 		margin-bottom: 3px;
 	}

 }


 .formConfirmModuleSubmit{
 	display: block;
 	background: #FFF;
 	text-align: center;
 	padding: 20px;
 	border-top: 2px solid #DDD;

 }

 .formConfirmModuleSubmit a{
 	background-color: #4caf50 !important;
 	border-radius: 3px !important;
 	text-align: center;
 	font-size: 22px;
 	color: #FFF;
 	line-height: 1.2em;
 	display: inline-block;
 	padding: 0.6em 1em;
 	box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);
 }

 .confirmMsg{
 	padding: 10px;
 	background: #FFEBEE;
 	font-weight: 600;
 	font-size: 11px;
 	line-height: 1.3em;
 }

 .formConfirmModuleSubmit a:HOVER, .formConfirmModuleSubmit a:ACTIVE{
 	background:  #333 !important;
 	text-decoration: none !important;
 }

 .topbuttons .btn a{
 	background-color:  #4caf50;
 }

 .topbuttons .btn a:HOVER{
 	background-color:  #82c584;
 }


 .topbuttons{
 	width: auto;
 	box-sizing: border-box;
 	padding: 10px 20px !important;
 	margin: 0px !important;
 }

 .topbuttons .submitbttns{
 	padding: 0px !important;
 }

 .topbuttons .submitbttns .buttons{
 	margin-right: 0px !important;
 }

 .list_style button {
 	display: block;
 	width: 100%;
 	text-align: left;
 	font-weight: normal;
 	padding: 10px;
 	line-height: 1.4em;
 }

 .list_style button img{
 	float: left;
 	margin-right: 10px;
 }


 .list_style .radiobutton.selected{
 	margin-left: 50px;
 	width: calc( 100% - 50px);
 	position: relative;
 }

 .list_style .radiobutton.selected:before{
 	content: 'check_circle';
 	position: absolute;
 	font-family: 'Material Icons';
 	width: 30px;
 	height: 30px;
 	display: block;
 	left: -54px;
 	top: 0px;
 	border-radius: 50px;
 	font-size: 40px;
 	line-height: 1em;
 	color: #4caf50;
 }

 textarea.tweatlength{
 	height: 5em
 }
 /*savepage-import-url=extended.css?siteVersion=1.0.6*/
 @media (max-width: 1200px) and (min-width:840px) {
 	.mdl-grid {
 		padding:8px
 	}

 	.mdl-cell {
 		margin: 8px;
 		width: calc(33.3333333333% - 16px)
 	}

 	.mdl-grid--no-spacing>.mdl-cell {
 		width: 33.3333333333%
 	}

 	.mdl-cell--hide-smalldesktop {
 		display: none!important
 	}

 	.mdl-cell--order-1-smalldesktop.mdl-cell--order-1-smalldesktop {
 		-webkit-order: 1;
 		-ms-flex-order: 1;
 		order: 1
 	}

 	.mdl-cell--order-2-smalldesktop.mdl-cell--order-2-smalldesktop {
 		-webkit-order: 2;
 		-ms-flex-order: 2;
 		order: 2
 	}

 	.mdl-cell--order-3-smalldesktop.mdl-cell--order-3-smalldesktop {
 		-webkit-order: 3;
 		-ms-flex-order: 3;
 		order: 3
 	}

 	.mdl-cell--order-4-smalldesktop.mdl-cell--order-4-smalldesktop {
 		-webkit-order: 4;
 		-ms-flex-order: 4;
 		order: 4
 	}

 	.mdl-cell--order-5-smalldesktop.mdl-cell--order-5-smalldesktop {
 		-webkit-order: 5;
 		-ms-flex-order: 5;
 		order: 5
 	}

 	.mdl-cell--order-6-smalldesktop.mdl-cell--order-6-smalldesktop {
 		-webkit-order: 6;
 		-ms-flex-order: 6;
 		order: 6
 	}

 	.mdl-cell--order-7-smalldesktop.mdl-cell--order-7-smalldesktop {
 		-webkit-order: 7;
 		-ms-flex-order: 7;
 		order: 7
 	}

 	.mdl-cell--order-8-smalldesktop.mdl-cell--order-8-smalldesktop {
 		-webkit-order: 8;
 		-ms-flex-order: 8;
 		order: 8
 	}

 	.mdl-cell--order-9-smalldesktop.mdl-cell--order-9-smalldesktop {
 		-webkit-order: 9;
 		-ms-flex-order: 9;
 		order: 9
 	}

 	.mdl-cell--order-10-smalldesktop.mdl-cell--order-10-smalldesktop {
 		-webkit-order: 10;
 		-ms-flex-order: 10;
 		order: 10
 	}

 	.mdl-cell--order-11-smalldesktop.mdl-cell--order-11-smalldesktop {
 		-webkit-order: 11;
 		-ms-flex-order: 11;
 		order: 11
 	}

 	.mdl-cell--order-12-smalldesktop.mdl-cell--order-12-smalldesktop {
 		-webkit-order: 12;
 		-ms-flex-order: 12;
 		order: 12
 	}

 	.mdl-cell--1-col,.mdl-cell--1-col-smalldesktop.mdl-cell--1-col-smalldesktop {
 		width: calc(8.3333333333% - 16px)
 	}

 	.mdl-grid--no-spacing>.mdl-cell--1-col,.mdl-grid--no-spacing>.mdl-cell--1-col-smalldesktop.mdl-cell--1-col-smalldesktop {
 		width: 8.3333333333%
 	}

 	.mdl-cell--2-col,.mdl-cell--2-col-smalldesktop.mdl-cell--2-col-smalldesktop {
 		width: calc(16.6666666667% - 16px)
 	}

 	.mdl-grid--no-spacing>.mdl-cell--2-col,.mdl-grid--no-spacing>.mdl-cell--2-col-smalldesktop.mdl-cell--2-col-smalldesktop {
 		width: 16.6666666667%
 	}

 	.mdl-cell--3-col,.mdl-cell--3-col-smalldesktop.mdl-cell--3-col-smalldesktop {
 		width: calc(25% - 16px)
 	}

 	.mdl-grid--no-spacing>.mdl-cell--3-col,.mdl-grid--no-spacing>.mdl-cell--3-col-smalldesktop.mdl-cell--3-col-smalldesktop {
 		width: 25%
 	}

 	.mdl-cell--4-col,.mdl-cell--4-col-smalldesktop.mdl-cell--4-col-smalldesktop {
 		width: calc(33.3333333333% - 16px)
 	}

 	.mdl-grid--no-spacing>.mdl-cell--4-col,.mdl-grid--no-spacing>.mdl-cell--4-col-smalldesktop.mdl-cell--4-col-smalldesktop {
 		width: 33.3333333333%
 	}

 	.mdl-cell--5-col,.mdl-cell--5-col-smalldesktop.mdl-cell--5-col-smalldesktop {
 		width: calc(41.6666666667% - 16px)
 	}

 	.mdl-grid--no-spacing>.mdl-cell--5-col,.mdl-grid--no-spacing>.mdl-cell--5-col-smalldesktop.mdl-cell--5-col-smalldesktop {
 		width: 41.6666666667%
 	}

 	.mdl-cell--6-col,.mdl-cell--6-col-smalldesktop.mdl-cell--6-col-smalldesktop {
 		width: calc(50% - 16px)
 	}

 	.mdl-grid--no-spacing>.mdl-cell--6-col,.mdl-grid--no-spacing>.mdl-cell--6-col-smalldesktop.mdl-cell--6-col-smalldesktop {
 		width: 50%
 	}

 	.mdl-cell--7-col,.mdl-cell--7-col-smalldesktop.mdl-cell--7-col-smalldesktop {
 		width: calc(58.3333333333% - 16px)
 	}

 	.mdl-grid--no-spacing>.mdl-cell--7-col,.mdl-grid--no-spacing>.mdl-cell--7-col-smalldesktop.mdl-cell--7-col-smalldesktop {
 		width: 58.3333333333%
 	}

 	.mdl-cell--8-col,.mdl-cell--8-col-smalldesktop.mdl-cell--8-col-smalldesktop {
 		width: calc(66.6666666667% - 16px)
 	}

 	.mdl-grid--no-spacing>.mdl-cell--8-col,.mdl-grid--no-spacing>.mdl-cell--8-col-smalldesktop.mdl-cell--8-col-smalldesktop {
 		width: 66.6666666667%
 	}

 	.mdl-cell--9-col,.mdl-cell--9-col-smalldesktop.mdl-cell--9-col-smalldesktop {
 		width: calc(75% - 16px)
 	}

 	.mdl-grid--no-spacing>.mdl-cell--9-col,.mdl-grid--no-spacing>.mdl-cell--9-col-smalldesktop.mdl-cell--9-col-smalldesktop {
 		width: 75%
 	}

 	.mdl-cell--10-col,.mdl-cell--10-col-smalldesktop.mdl-cell--10-col-smalldesktop {
 		width: calc(83.3333333333% - 16px)
 	}

 	.mdl-grid--no-spacing>.mdl-cell--10-col,.mdl-grid--no-spacing>.mdl-cell--10-col-smalldesktop.mdl-cell--10-col-smalldesktop {
 		width: 83.3333333333%
 	}

 	.mdl-cell--11-col,.mdl-cell--11-col-smalldesktop.mdl-cell--11-col-smalldesktop {
 		width: calc(91.6666666667% - 16px)
 	}

 	.mdl-grid--no-spacing>.mdl-cell--11-col,.mdl-grid--no-spacing>.mdl-cell--11-col-smalldesktop.mdl-cell--11-col-smalldesktop {
 		width: 91.6666666667%
 	}

 	.mdl-cell--12-col,.mdl-cell--12-col-smalldesktop.mdl-cell--12-col-smalldesktop {
 		width: calc(100% - 16px)
 	}

 	.mdl-grid--no-spacing>.mdl-cell--12-col,.mdl-grid--no-spacing>.mdl-cell--12-col-smalldesktop.mdl-cell--12-col-smalldesktop {
 		width: 100%
 	}

 	.mdl-cell--1-offset,.mdl-cell--1-offset-smalldesktop.mdl-cell--1-offset-smalldesktop {
 		margin-left: calc(8.3333333333% + 8px)
 	}

 	.mdl-grid.mdl-grid--no-spacing>.mdl-cell--1-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--1-offset-smalldesktop.mdl-cell--1-offset-smalldesktop {
 		margin-left: 8.3333333333%
 	}

 	.mdl-cell--2-offset,.mdl-cell--2-offset-smalldesktop.mdl-cell--2-offset-smalldesktop {
 		margin-left: calc(16.6666666667% + 8px)
 	}

 	.mdl-grid.mdl-grid--no-spacing>.mdl-cell--2-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--2-offset-smalldesktop.mdl-cell--2-offset-smalldesktop {
 		margin-left: 16.6666666667%
 	}

 	.mdl-cell--3-offset,.mdl-cell--3-offset-smalldesktop.mdl-cell--3-offset-smalldesktop {
 		margin-left: calc(25% + 8px)
 	}

 	.mdl-grid.mdl-grid--no-spacing>.mdl-cell--3-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--3-offset-smalldesktop.mdl-cell--3-offset-smalldesktop {
 		margin-left: 25%
 	}

 	.mdl-cell--4-offset,.mdl-cell--4-offset-smalldesktop.mdl-cell--4-offset-smalldesktop {
 		margin-left: calc(33.3333333333% + 8px)
 	}

 	.mdl-grid.mdl-grid--no-spacing>.mdl-cell--4-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--4-offset-smalldesktop.mdl-cell--4-offset-smalldesktop {
 		margin-left: 33.3333333333%
 	}

 	.mdl-cell--5-offset,.mdl-cell--5-offset-smalldesktop.mdl-cell--5-offset-smalldesktop {
 		margin-left: calc(41.6666666667% + 8px)
 	}

 	.mdl-grid.mdl-grid--no-spacing>.mdl-cell--5-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--5-offset-smalldesktop.mdl-cell--5-offset-smalldesktop {
 		margin-left: 41.6666666667%
 	}

 	.mdl-cell--6-offset,.mdl-cell--6-offset-smalldesktop.mdl-cell--6-offset-smalldesktop {
 		margin-left: calc(50% + 8px)
 	}

 	.mdl-grid.mdl-grid--no-spacing>.mdl-cell--6-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--6-offset-smalldesktop.mdl-cell--6-offset-smalldesktop {
 		margin-left: 50%
 	}

 	.mdl-cell--7-offset,.mdl-cell--7-offset-smalldesktop.mdl-cell--7-offset-smalldesktop {
 		margin-left: calc(58.3333333333% + 8px)
 	}

 	.mdl-grid.mdl-grid--no-spacing>.mdl-cell--7-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--7-offset-smalldesktop.mdl-cell--7-offset-smalldesktop {
 		margin-left: 58.3333333333%
 	}

 	.mdl-cell--8-offset,.mdl-cell--8-offset-smalldesktop.mdl-cell--8-offset-smalldesktop {
 		margin-left: calc(66.6666666667% + 8px)
 	}

 	.mdl-grid.mdl-grid--no-spacing>.mdl-cell--8-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--8-offset-smalldesktop.mdl-cell--8-offset-smalldesktop {
 		margin-left: 66.6666666667%
 	}

 	.mdl-cell--9-offset,.mdl-cell--9-offset-smalldesktop.mdl-cell--9-offset-smalldesktop {
 		margin-left: calc(75% + 8px)
 	}

 	.mdl-grid.mdl-grid--no-spacing>.mdl-cell--9-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--9-offset-smalldesktop.mdl-cell--9-offset-smalldesktop {
 		margin-left: 75%
 	}

 	.mdl-cell--10-offset,.mdl-cell--10-offset-smalldesktop.mdl-cell--10-offset-smalldesktop {
 		margin-left: calc(83.3333333333% + 8px)
 	}

 	.mdl-grid.mdl-grid--no-spacing>.mdl-cell--10-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--10-offset-smalldesktop.mdl-cell--10-offset-smalldesktop {
 		margin-left: 83.3333333333%
 	}

 	.mdl-cell--11-offset,.mdl-cell--11-offset-smalldesktop.mdl-cell--11-offset-smalldesktop {
 		margin-left: calc(91.6666666667% + 8px)
 	}

 	.mdl-grid.mdl-grid--no-spacing>.mdl-cell--11-offset,.mdl-grid.mdl-grid--no-spacing>.mdl-cell--11-offset-smalldesktop.mdl-cell--11-offset-smalldesktop {
 		margin-left: 91.6666666667%
 	}
 }


 .ml-button--sm {
 	font-size: 12px;
 	line-height: 26px;
 	height: auto;
 	padding: 0 12px
 }

 .ml-table-bordered>tbody>tr>td,.ml-table-bordered>thead>tr>td {
 	border: 1px solid #F0F0F0
 }

 .ml-table-striped thead tr{
 	background: #EEE;
 }


 .ml-table-striped>tbody>tr:nth-of-type(even) {
 	background-color: #f9f9f9
 }

 .ml-table-striped td{
 	border-top:  none;
 	border-bottom:  none;
 }

 .ml-table-striped tbody tr:hover {
 	background-color: #ddd;
 }

 .ml-table-bordered>tbody>tr>td,.ml-table-bordered>thead>tr>td {
 	border: 1px solid #F0F0F0
 }

 .ml-data-table-pager .mdl-button {
 	min-width: auto
 }

 .mdl-components {
 	display: -webkit-flex;
 	display: -ms-flexbox;
 	display: flex
 }

 .mdl-components-index {
 	box-sizing: border-box;
 	position: relative;
 	padding-top: 64px
 }

 .mdl-components-index-text {
 	max-width: 960px;
 	margin: 0
 }

 .mdl-components-index-text .mdl-components-dl {
 	-webkit-justify-content: flex-start;
 	-ms-flex-pack: start;
 	justify-content: flex-start;
 	margin-left: 40px
 }

 .mdl-components-img {
 	position: absolute;
 	margin-top: 24px;
 	left: 40px;
 	height: 90%;
 	width: 90%;
 	max-height: 600px;
 	background: /*savepage-url=../assets/compindex_2x.png*/ url() no-repeat top left/contain
 }

 .mdl-components-text {
 	margin: 0;
 	padding: 0
 }

 mdl-components-dl {
 	padding: .5em;
 	width: 80%
 }

 mdl-components-dt {
 	float: left;
 	clear: left;
 	width: 150px;
 	font-weight: 700
 }

 mdl-components-dd {
 	margin: 0 0 0 150px;
 	padding: 0 0 .5em
 }

 .mdl-components .mdl-components__nav {
 	display: inline-block;
 	background: #fff;
 	width: 200px;
 	box-sizing: border-box;
 	padding: 24px 0;
 	-webkit-flex-shrink: 0;
 	-ms-flex-negative: 0;
 	flex-shrink: 0;
 	z-index: 1
 }

 .mdl-components .mdl-components__pages {
 	display: inline-block;
 	-webkit-flex-grow: 1;
 	-ms-flex-positive: 1;
 	flex-grow: 1;
 	padding-bottom: 120px
 }

 .mdl-components .mdl-components__link {
 	margin: 16px;
 	font-weight: 400;
 	color: rgba(0,0,0,.66);
 	position: relative;
 	padding-left: 72px;
 	min-height: 48px;
 	display: table;
 	line-height: 48px
 }

 .mdl-components .mdl-components__link.is-active {
 	font-weight: 700;
 	color: #c2185b
 }

 .content {
 	background: #fafafa
 }

 .mdl-components .mdl-components__page {
 	display: none;
 	min-height: 1000px
 }

 .mdl-components .docs-toc,.mdl-components .component-title {
 	margin-bottom: 60px;
 	margin-left: 32px
 }

 .mdl-components .component-title {
 	margin-top: 30px
 }

 .mdl-components .component-title h3 {
 	font-size: 16px;
 	font-weight: 500;
 	margin-top: 80px;
 	text-transform: uppercase
 }

 .mdl-components .snippet-code pre.language-markup code {
 	padding-left: 40px
 }

 .mdl-components .mdl-components__page.is-active {
 	display: block
 }

 .mdl-components__link-image {
 	display: inline-block;
 	margin: 0 10px;
 	position: absolute;
 	left: 0;
 	top: 0;
 	background-color: #ddd;
 	background-position: center;
 	background-repeat: no-repeat;
 	background-size: auto 48px;
 	border-radius: 50%;
 	height: 46px;
 	width: 46px
 }

 .mdl-components__warning {
 	width: 100%;
 	max-width: 640px;
 	margin: 0 auto;
 	background-color: #FFF9C4;
 	padding: 16px;
 	border-radius: 2px;
 	color: #212121;
 	box-sizing: border-box;
 }

 .mdl-components__warning.full{
 	max-width: initial;
 }

 .mdl-components__link.is-active .mdl-components__link-image {
 	box-shadow: 0 3px 4px 0 rgba(0,0,0,.14),0 3px 3px -2px rgba(0,0,0,.2),0 1px 8px 0 rgba(0,0,0,.12)
 }

 .docs-readme {
 	width: 100%;
 	max-width: 640px;
 	margin: 0 auto
 }

 .docs-readme .language-markup {
 	width: 100%
 }

 @media (max-width: 850px) {
 	.mdl-components .component-title,.mdl-components .docs-toc {
 		margin-left:0
 	}

 	.mdl-components .snippet-code pre.language-markup code {
 		padding-left: 8px
 	}

 	.docs-layout-content {
 		position: relative;
 		height: 100%;
 		overflow: hidden!important
 	}

 	.mdl-components .mdl-components__nav {
 		position: fixed;
 		top: 64px;
 		left: 0;
 		display: block;
 		-webkit-flex-wrap: nowrap;
 		-ms-flex-wrap: nowrap;
 		flex-wrap: nowrap;
 		width: 100%;
 		height: 120px;
 		overflow-x: auto;
 		overflow-y: hidden;
 		padding: 8px;
 		z-index: 100;
 		white-space: nowrap
 	}

 	.mdl-components-index-text .mdl-components-dl {
 		margin: 0
 	}

 	.mdl-components-img {
 		left: 8px
 	}

 	.components .content .about-panel {
 		padding: 8px
 	}

 	.mdl-components__nav .mdl-components__link {
 		display: inline-block;
 		vertical-align: top;
 		height: 100%;
 		width: auto;
 		min-width: 48px;
 		margin: 0 8px 0 0;
 		padding-left: 0;
 		padding-top: 10px
 	}

 	.mdl-components__nav .mdl-components__link>* {
 		display: block;
 		width: auto;
 		margin: 0 auto;
 		text-align: center
 	}

 	.mdl-components__nav .mdl-components__link>.mdl-components__link-image {
 		position: static;
 		width: 48px
 	}

 	.content {
 		padding-top: 64px!important;
 		padding-left: 0!important;
 		overflow-y: auto;
 		height: 100%
 	}
 }

 .p-2 {
 	padding: 2px
 }

 .p-5 {
 	padding: 5px
 }

 .p-10 {
 	padding: 10px
 }

 .p-15 {
 	padding: 15px
 }

 .p-20 {
 	padding: 20px
 }

 .p-25 {
 	padding: 25px
 }

 .p-30 {
 	padding: 30px
 }

 .p-35 {
 	padding: 35px
 }

 .p-40 {
 	padding: 40px
 }

 .p-45 {
 	padding: 45px
 }

 .p-50 {
 	padding: 50px
 }

 .p-55 {
 	padding: 55px
 }

 .p-60 {
 	padding: 60px
 }

 .p-65 {
 	padding: 65px
 }

 .p-70 {
 	padding: 70px
 }

 .p-75 {
 	padding: 75px
 }

 .p-80 {
 	padding: 80px
 }

 .p-t-5 {
 	padding-top: 5px
 }

 .p-r-5 {
 	padding-right: 5px
 }

 .p-b-5 {
 	padding-bottom: 5px
 }

 .p-l-5 {
 	padding-left: 5px
 }

 .p-v-5 {
 	padding: 5px 0
 }

 .p-h-5 {
 	padding: 0 5px
 }

 .p-t-10 {
 	padding-top: 10px
 }

 .p-r-10 {
 	padding-right: 10px
 }

 .p-b-10 {
 	padding-bottom: 10px
 }

 .p-l-10 {
 	padding-left: 10px
 }

 .p-v-10 {
 	padding: 10px 0
 }

 .p-h-10 {
 	padding: 0 10px
 }

 .p-t-15 {
 	padding-top: 15px
 }

 .p-r-15 {
 	padding-right: 15px
 }

 .p-b-15 {
 	padding-bottom: 15px
 }

 .p-l-15 {
 	padding-left: 15px
 }

 .p-v-15 {
 	padding: 15px 0
 }

 .p-h-15 {
 	padding: 0 15px
 }

 .p-t-20 {
 	padding-top: 20px
 }

 .p-r-20 {
 	padding-right: 20px
 }

 .p-b-20 {
 	padding-bottom: 20px
 }

 .p-l-20 {
 	padding-left: 20px
 }

 .p-v-20 {
 	padding: 20px 0
 }

 .p-h-20 {
 	padding: 0 20px
 }

 .p-t-25 {
 	padding-top: 25px
 }

 .p-r-25 {
 	padding-right: 25px
 }

 .p-b-25 {
 	padding-bottom: 25px
 }

 .p-l-25 {
 	padding-left: 25px
 }

 .p-v-25 {
 	padding: 25px 0
 }

 .p-h-25 {
 	padding: 0 25px
 }

 .p-t-30 {
 	padding-top: 30px
 }

 .p-r-30 {
 	padding-right: 30px
 }

 .p-b-30 {
 	padding-bottom: 30px
 }

 .p-l-30 {
 	padding-left: 30px
 }

 .p-v-30 {
 	padding: 30px 0
 }

 .p-h-30 {
 	padding: 0 30px
 }

 .p-t-35 {
 	padding-top: 35px
 }

 .p-r-35 {
 	padding-right: 35px
 }

 .p-b-35 {
 	padding-bottom: 35px
 }

 .p-l-35 {
 	padding-left: 35px
 }

 .p-v-35 {
 	padding: 35px 0
 }

 .p-h-35 {
 	padding: 0 35px
 }

 .p-t-40,.p-v-40 {
 	padding-top: 40px
 }

 .p-b-40,.p-v-40 {
 	padding-bottom: 40px
 }

 .p-r-40 {
 	padding-right: 40px
 }

 .p-l-40 {
 	padding-left: 40px
 }

 .p-h-40 {
 	padding: 0 40px
 }

 .p-t-45 {
 	padding-top: 45px
 }

 .p-r-45 {
 	padding-right: 45px
 }

 .p-b-45 {
 	padding-bottom: 45px
 }

 .p-l-45 {
 	padding-left: 45px
 }

 .p-v-45 {
 	padding: 45px 0
 }

 .p-h-45 {
 	padding: 0 45px
 }

 .p-t-50 {
 	padding-top: 50px
 }

 .p-r-50 {
 	padding-right: 50px
 }

 .p-b-50 {
 	padding-bottom: 50px
 }

 .p-l-50 {
 	padding-left: 50px
 }

 .p-v-50 {
 	padding: 50px 0
 }

 .p-h-50 {
 	padding: 0 50px
 }

 .p-t-55 {
 	padding-top: 55px
 }

 .p-r-55 {
 	padding-right: 55px
 }

 .p-b-55 {
 	padding-bottom: 55px
 }

 .p-l-55 {
 	padding-left: 55px
 }

 .p-v-55 {
 	padding: 55px 0
 }

 .p-h-55 {
 	padding: 0 55px
 }

 .p-t-60 {
 	padding-top: 60px
 }

 .p-r-60 {
 	padding-right: 60px
 }

 .p-b-60 {
 	padding-bottom: 60px
 }

 .p-l-60 {
 	padding-left: 60px
 }

 .p-v-60 {
 	padding: 60px 0
 }

 .p-h-60 {
 	padding: 0 60px
 }

 .p-t-65 {
 	padding-top: 65px
 }

 .p-r-65 {
 	padding-right: 65px
 }

 .p-b-65 {
 	padding-bottom: 65px
 }

 .p-l-65 {
 	padding-left: 65px
 }

 .p-v-65 {
 	padding: 65px 0
 }

 .p-h-65 {
 	padding: 0 65px
 }

 .p-t-70 {
 	padding-top: 70px
 }

 .p-r-70 {
 	padding-right: 70px
 }

 .p-b-70 {
 	padding-bottom: 70px
 }

 .p-l-70 {
 	padding-left: 70px
 }

 .p-v-70 {
 	padding: 70px 0
 }

 .p-h-70 {
 	padding: 0 70px
 }

 .p-t-75 {
 	padding-top: 75px
 }

 .p-r-75 {
 	padding-right: 75px
 }

 .p-b-75 {
 	padding-bottom: 75px
 }

 .p-l-75 {
 	padding-left: 75px
 }

 .p-v-75 {
 	padding: 75px 0
 }

 .p-h-75 {
 	padding: 0 75px
 }

 .p-t-80 {
 	padding-top: 80px
 }

 .p-r-80 {
 	padding-right: 80px
 }

 .p-b-80 {
 	padding-bottom: 80px
 }

 .p-l-80 {
 	padding-left: 80px
 }

 .p-v-80 {
 	padding: 80px 0
 }

 .p-h-80 {
 	padding: 0 80px
 }

 .no-p,.no-p-h,.no-p-v {
 	padding: 0
 }

 .no-p-t {
 	padding-top: 0
 }

 .no-p-r {
 	padding-right: 0
 }

 .no-p-b {
 	padding-bottom: 0
 }

 .no-p-l {
 	padding-left: 0
 }

 .no-pad-lr,.no-pad-rl,.pad-b-only,.pad-l-only,.pad-t-only {
 	padding-right: 0!important
 }

 .no-pad-lr,.no-pad-rl,.pad-b-only,.pad-r-only,.pad-t-only {
 	padding-left: 0!important
 }

 .no-pad-bt,.no-pad-tb,.pad-b-only,.pad-l-only,.pad-r-only {
 	padding-top: 0!important
 }

 .no-pad-bt,.no-pad-tb,.pad-l-only,.pad-r-only,.pad-t-only {
 	padding-bottom: 0!important
 }

 .m-5 {
 	margin: 5px
 }

 .m-t-5 {
 	margin-top: 5px
 }

 .m-r-5 {
 	margin-right: 5px
 }

 .m-b-5 {
 	margin-bottom: 5px
 }

 .m-l-5 {
 	margin-left: 5px
 }

 .m-v-5 {
 	margin: 5px 0
 }

 .m-h-5 {
 	margin: 0 5px
 }

 .m-10 {
 	margin: 10px
 }

 .m-t-10 {
 	margin-top: 10px
 }

 .m-r-10 {
 	margin-right: 10px
 }

 .m-b-10 {
 	margin-bottom: 10px
 }

 .m-l-10 {
 	margin-left: 10px
 }

 .m-v-10 {
 	margin: 10px 0
 }

 .m-h-10 {
 	margin: 0 10px
 }

 .m-15 {
 	margin: 15px
 }

 .m-t-15 {
 	margin-top: 15px
 }

 .m-r-15 {
 	margin-right: 15px
 }

 .m-b-15 {
 	margin-bottom: 15px
 }

 .m-l-15 {
 	margin-left: 15px
 }

 .m-v-15 {
 	margin: 15px 0
 }

 .m-h-15 {
 	margin: 0 15px
 }

 .m-20 {
 	margin: 20px
 }

 .m-t-20 {
 	margin-top: 20px
 }

 .m-r-20 {
 	margin-right: 20px
 }

 .m-b-20 {
 	margin-bottom: 20px
 }

 .m-l-20 {
 	margin-left: 20px
 }

 .m-v-20 {
 	margin: 20px 0
 }

 .m-h-20 {
 	margin: 0 20px
 }

 .m-25 {
 	margin: 25px
 }

 .m-t-25 {
 	margin-top: 25px
 }

 .m-r-25 {
 	margin-right: 25px
 }

 .m-b-25 {
 	margin-bottom: 25px
 }

 .m-l-25 {
 	margin-left: 25px
 }

 .m-v-25 {
 	margin: 25px 0
 }

 .m-h-25 {
 	margin: 0 25px
 }

 .m-30 {
 	margin: 30px
 }

 .m-t-30 {
 	margin-top: 30px
 }

 .m-r-30 {
 	margin-right: 30px
 }

 .m-b-30 {
 	margin-bottom: 30px
 }

 .m-l-30 {
 	margin-left: 30px
 }

 .m-v-30 {
 	margin: 30px 0
 }

 .m-h-30 {
 	margin: 0 30px
 }

 .m-35 {
 	margin: 35px
 }

 .m-t-35 {
 	margin-top: 35px
 }

 .m-r-35 {
 	margin-right: 35px
 }

 .m-b-35 {
 	margin-bottom: 35px
 }

 .m-l-35 {
 	margin-left: 35px
 }

 .m-v-35 {
 	margin: 35px 0
 }

 .m-h-35 {
 	margin: 0 35px
 }

 .m-40 {
 	margin: 40px
 }

 .m-t-40 {
 	margin-top: 40px
 }

 .m-r-40 {
 	margin-right: 40px
 }

 .m-b-40 {
 	margin-bottom: 40px
 }

 .m-l-40 {
 	margin-left: 40px
 }

 .m-v-40 {
 	margin: 40px 0
 }

 .m-h-40 {
 	margin: 0 40px
 }

 .m-45 {
 	margin: 45px
 }

 .m-t-45 {
 	margin-top: 45px
 }

 .m-r-45 {
 	margin-right: 45px
 }

 .m-b-45 {
 	margin-bottom: 45px
 }

 .m-l-45 {
 	margin-left: 45px
 }

 .m-v-45 {
 	margin: 45px 0
 }

 .m-h-45 {
 	margin: 0 45px
 }

 .m-50 {
 	margin: 50px
 }

 .m-t-50 {
 	margin-top: 50px
 }

 .m-r-50 {
 	margin-right: 50px
 }

 .m-b-50 {
 	margin-bottom: 50px
 }

 .m-l-50 {
 	margin-left: 50px
 }

 .m-v-50 {
 	margin: 50px 0
 }

 .m-h-50 {
 	margin: 0 50px
 }

 .m-55 {
 	margin: 55px
 }

 .m-t-55 {
 	margin-top: 55px
 }

 .m-r-55 {
 	margin-right: 55px
 }

 .m-b-55 {
 	margin-bottom: 55px
 }

 .m-l-55 {
 	margin-left: 55px
 }

 .m-v-55 {
 	margin: 55px 0
 }

 .m-h-55 {
 	margin: 0 55px
 }

 .m-60 {
 	margin: 60px
 }

 .m-t-60 {
 	margin-top: 60px
 }

 .m-r-60 {
 	margin-right: 60px
 }

 .m-b-60 {
 	margin-bottom: 60px
 }

 .m-l-60 {
 	margin-left: 60px
 }

 .m-v-60 {
 	margin: 60px 0
 }

 .m-h-60 {
 	margin: 0 60px
 }

 .m-65 {
 	margin: 65px
 }

 .m-t-65 {
 	margin-top: 65px
 }

 .m-r-65 {
 	margin-right: 65px
 }

 .m-b-65 {
 	margin-bottom: 65px
 }

 .m-l-65 {
 	margin-left: 65px
 }

 .m-v-65 {
 	margin: 65px 0
 }

 .m-h-65 {
 	margin: 0 65px
 }

 .m-70 {
 	margin: 70px
 }

 .m-t-70 {
 	margin-top: 70px
 }

 .m-r-70 {
 	margin-right: 70px
 }

 .m-b-70 {
 	margin-bottom: 70px
 }

 .m-l-70 {
 	margin-left: 70px
 }

 .m-v-70 {
 	margin: 70px 0
 }

 .m-h-70 {
 	margin: 0 70px
 }

 .m-75 {
 	margin: 75px
 }

 .m-t-75 {
 	margin-top: 75px
 }

 .m-r-75 {
 	margin-right: 75px
 }

 .m-b-75 {
 	margin-bottom: 75px
 }

 .m-l-75 {
 	margin-left: 75px
 }

 .m-v-75 {
 	margin: 75px 0
 }

 .m-h-75 {
 	margin: 0 75px
 }

 .m-80 {
 	margin: 80px
 }

 .m-t-80 {
 	margin-top: 80px
 }

 .m-r-80 {
 	margin-right: 80px
 }

 .m-b-80 {
 	margin-bottom: 80px
 }

 .m-l-80 {
 	margin-left: 80px
 }

 .m-v-80 {
 	margin: 80px 0
 }

 .m-h-80 {
 	margin: 0 80px
 }

 .no-m,.no-m-h,.no-m-v {
 	margin: 0
 }

 .no-m-t {
 	margin-top: 0
 }

 .no-m-r {
 	margin-right: 0
 }

 .no-m-b {
 	margin-bottom: 0
 }

 .no-m-l {
 	margin-left: 0
 }

 .m-b-only,.m-l-only,.m-t-only,.no-m-lr,.no-m-rl {
 	margin-right: 0!important
 }

 .m-b-only,.m-r-only,.m-t-only,.no-m-lr,.no-m-rl {
 	margin-left: 0!important
 }

 .m-b-only,.m-l-only,.m-r-only,.no-m-bt,.no-m-tb {
 	margin-top: 0!important
 }

 .m-l-only,.m-r-only,.m-t-only,.no-m-bt,.no-m-tb {
 	margin-bottom: 0!important
 }

 .no-margin {
 	margin: 0!important
 }

 .m-auto {
 	margin-left: auto;
 	margin-right: auto
 }

 .no-padding {
 	padding: 0!important
 }

 .no-border {
 	border: none!important
 }

 .no-border-lt,.no-border-tl {
 	border-top: none!important;
 	border-left: none!important
 }

 .border-dashed,.border-dotted {
 	border-style: dashed!important
 }

 .border-black {
 	border-color: #000
 }

 .border-light-grey {
 	border-color: #e5e5e5
 }

 .border-medium-grey {
 	border-color: #ccc
 }

 .border-grey {
 	border-color: #999
 }

 .border-dark-grey {
 	border-color: #222
 }

 .border-fff,.border-white {
 	border-color: #fff
 }

 .bg-black {
 	background-color: #000
 }

 .bg-light-grey {
 	background-color: #e5e5e5
 }

 .bg-medium-grey {
 	background-color: #ccc
 }

 .bg-grey {
 	background-color: #999
 }

 .bg-dark-grey {
 	background-color: #222
 }

 .bg-fff,.bg-white {
 	background-color: #fff
 }

 .bg-none,.no-bg {
 	background: 0 0
 }

 .bg-facebook {
 	background-color: #47639E
 }

 .bg-twitter {
 	background-color: #02A8F3
 }

 .lh-1 {
 	line-height: 1!important
 }

 .lh-13 {
 	line-height: 1.3!important
 }

 .lh-15 {
 	line-height: 1.5!important
 }

 .bold,.strong {
 	font-weight: 700
 }

 .no-bold,.normal {
 	font-weight: 400
 }

 .em,.italic {
 	font-style: italic
 }

 .strike {
 	text-decoration: line-through
 }

 .underline {
 	text-decoration: underline
 }

 .normal {
 	font-style: normal
 }

 .sans-serif {
 	font-family: "Helvetica Neue",Helvetica,Arial,sans-serif
 }

 .serif {
 	font-family: Georgia,"Times New Romain",serif
 }

 .uppercase {
 	text-transform: uppercase
 }

 .t-right {
 	text-align: right
 }

 .t-center {
 	text-align: center
 }

 .no-mw {
 	max-width: 100%
 }

 .mw400 {
 	max-width: 400px
 }

 .mw500 {
 	max-width: 500px
 }

 .mw600 {
 	max-width: 600px
 }

 .w100 {
 	font-weight: 100
 }

 .w200 {
 	font-weight: 200
 }

 .w300 {
 	font-weight: 300
 }

 .w400 {
 	font-weight: 400
 }

 .w500 {
 	font-weight: 500
 }

 .w600 {
 	font-weight: 600
 }

 .w700 {
 	font-weight: 700
 }

 .w800 {
 	font-weight: 800
 }

 .w900 {
 	font-weight: 900
 }

 .f9 {
 	font-size: 9px
 }

 .f10 {
 	font-size: 10px
 }

 .f11 {
 	font-size: 11px
 }

 .f12 {
 	font-size: 12px
 }

 .f13 {
 	font-size: 13px
 }

 .f14 {
 	font-size: 14px
 }

 .f15 {
 	font-size: 15px
 }

 .f16 {
 	font-size: 16px
 }

 .f17 {
 	font-size: 17px
 }

 .f18 {
 	font-size: 18px;
 	line-height: 30px
 }

 .f19 {
 	font-size: 19px
 }

 .f20 {
 	font-size: 20px
 }

 .f30 {
 	font-size: 30px
 }

 .f40 {
 	font-size: 40px
 }

 .f50 {
 	font-size: 50px
 }

 .f60 {
 	font-size: 60px
 }

 .size-small {
 	font-size: 75%!important
 }

 .size-normal {
 	font-size: 100%!important
 }

 .size-medium {
 	font-size: 125%!important
 }

 .size-big,.size-large {
 	font-size: 150%!important
 }

 .size-huge {
 	font-size: 200%!important
 }

 .inherit {
 	font: inherit
 }

 .no-wrap {
 	white-space: nowrap
 }

 .wrap {
 	white-space: initial;
 }

 .auto-cell-size {
 	white-space: nowrap;
 	width: 1%
 }

 .ls-0 {
 	letter-spacing: -3px;
 	margin-left: 10px;
 	margin-right: 10px;
 	white-space: nowrap
 }

 .no-ul,.no-ul a,.no-ul a:hover,.no-ul:hover {
 	text-decoration: none
 }

 .color-inherit {
 	color: inherit
 }

 .clear:after {
 	display: table;
 	content: " "
 }

 .f-left {
 	float: left
 }

 .f-right {
 	float: right
 }

 .f-none {
 	float: none
 }

 .block {
 	display: block!important
 }

 .inline {
 	display: inline!important
 }

 .table {
 	display: table
 }

 .in-block {
 	display: inline-block!important;
 	zoom:1}

 	.d-none,.hidden,.hide {
 		display: none!important
 	}

 	.rel,.relative {
 		position: relative!important
 	}

 	.abs,.absolute {
 		position: absolute
 	}

 	.static {
 		position: static
 	}

 	.fixed {
 		position: fixed
 	}

 	.t-0 {
 		top: 0
 	}

 	.tb-0 {
 		top: 0
 	}

 	.b-0 {
 		bottom: 0
 	}

 	.l-0{
 		left: 0;
 	}

 	.r-0{
 		right:  0;
 	}

 	.v-m,.v-mid {
 		vertical-align: middle!important
 	}

 	.v-t,.v-top {
 		vertical-align: top!important
 	}

 	.v-b,.v-bottom {
 		vertical-align: bottom!important
 	}

 	.v-super {
 		vertical-align: super!important
 	}

 	.full-input input,.full-input select,.full-input textarea {
 		width: 100%
 	}

 	.full-input input[type=checkbox],.full-input input[type=radio],.normal input,.normal select,.normal textarea {
 		width: auto
 	}

 	.flex1,.fullwidth {
 		width: 100%
 	}

 	.no-shadow {
 		box-shadow: none!important
 	}

 	.no-border-radius {
 		border-radius: 0!important
 	}

 	.overflow-x-scroll {
 		max-width: 100%;
 		overflow-x: auto
 	}

 	.overflow-no {
 		overflow: hidden
 	}

 	.overflow-auto {
 		overflow: auto
 	}

 	.overflow-scroll {
 		overflow: scroll
 	}

 	.pointer {
 		cursor: pointer
 	}

 	.fullheight {
 		height: 100%
 	}

 	.nolist {
 		list-style: none
 	}

 	.text-shadow {
 		text-shadow: 1px 1px 1px rgba(0,0,0,.4)
 	}

 	.radius3 {
 		border-radius: 3px
 	}

 	.radius5 {
 		border-radius: 5px
 	}

 	.radius10 {
 		border-radius: 10px
 	}

 	.text-rgb-2 {
 		color: rgba(0,0,0,.2)
 	}

 	.text-rgb-3 {
 		color: rgba(0,0,0,.3)
 	}

 	.text-rgb-4 {
 		color: rgba(0,0,0,.4)
 	}

 	.text-rgb-5 {
 		color: rgba(0,0,0,.5)
 	}

 	.mh-100 {
 		min-height: 100px
 	}

 	.mh-200 {
 		min-height: 200px
 	}

 	.mh-300 {
 		min-height: 300px
 	}

 	.margin-auto {
 		margin: 0 auto
 	}

 	.img-responsive {
 		display: block;
 		max-width: 100%;
 		height: auto
 	}

 	.flex {
 		display: -webkit-flex;
 		display: -moz-flex;
 		display: -ms-flexbox;
 		display: -o-flex;
 		display: flex
 	}

 	.flex1 {
 		-webkit-flex: 1;
 		-moz-flex: 1;
 		-ms-flex: 1;
 		-o-flex: 1;
 		flex: 1;
 		height: 100%
 	}

 	.flexrow {
 		-webkit-flex-direction: row;
 		-moz-flex-direction: row;
 		-ms-flex-direction: row;
 		-o-flex-direction: row;
 		flex-direction: row
 	}

 	.flex-right {
 		-webkit-flex-direction: row-reverse;
 		-moz-flex-direction: row-reverse;
 		-ms-flex-direction: row-reverse;
 		-o-flex-direction: row-reverse;
 		flex-direction: row-reverse
 	}

 	.text-center {
 		text-align: center !important;
 	}

 	.text-left {
 		text-align: left !important;
 	}

 	.mdl-chip-small .mdl-chip__text{
 		font-size: 10px;
 		line-height: 2em;

 	}
 	.mdl-chip-small{ 
 		font-size: 10px;
 		line-height: 2em;
 		height: 2em;
 		padding: 0 1em;
 	}

 	.mdl-tabs__panel:NOT(.is-active){
 		display: none;
 	}

/* @import url("header.css");
*/

/*
      ___ ___  _    ___  ___  ___
     / __/ _ \| |  / _ \| _ \/ __|
    | (_| (_) | |_| (_) |   /\__ \
     \___\___/|____\___/|_|_\|___/

     */
     .colors__p{
     	background-color: #9e9e9e !important;
     	color: #000 !important;
     }
     .colors__p a{
     	color: #000 !important;
     }
     a.colors__p{
     	background-color: #9e9e9e !important;
     	color: #000 !important;
     }
     .colors__p--text{
     	color: #9e9e9e !important;
     }
     .colors__p--text--hover{
     	color: #9e9e9e !important;
     }
     .colors__p--hover:HOVER{
     	background-color: #9e9e9e !important;
     	color: #000 !important;
     }
     .colors__p--borders{
     	border-color: #9e9e9e !important;
     }
     .colors__p--borders--hover:HOVER{
     	border-color: #9e9e9e !important;
     }
     .colors__p-light{
     	background-color: #cfcfcf !important;
     	color: #000 !important;
     }
     .colors__p-light a{
     	color: #000 !important;
     }
     a.colors__p-light{
     	background-color: #cfcfcf !important;
     	color: #000 !important;
     }
     .colors__p-light--text{
     	color: #cfcfcf !important;
     }
     .colors__p-light--text--hover{
     	color: #cfcfcf !important;
     }
     .colors__p-light--hover:HOVER{
     	background-color: #cfcfcf !important;
     	color: #000 !important;
     }
     .colors__p-light--borders{
     	border-color: #cfcfcf !important;
     }
     .colors__p-light--borders--hover:HOVER{
     	border-color: #cfcfcf !important;
     }
     .colors__p-dark{
     	background-color: #707070 !important;
     	color: #FFF !important;
     }
     .colors__p-dark a{
     	color: #FFF !important;
     }
     a.colors__p-dark{
     	background-color: #707070 !important;
     	color: #FFF !important;
     }
     .colors__p-dark--text{
     	color: #707070 !important;
     }
     .colors__p-dark--text--hover{
     	color: #707070 !important;
     }
     .colors__p-dark--hover:HOVER{
     	background-color: #707070 !important;
     	color: #FFF !important;
     }
     .colors__p-dark--borders{
     	border-color: #707070 !important;
     }
     .colors__p-dark--borders--hover:HOVER{
     	border-color: #707070 !important;
     }
     .colors__iflyer{
     	background-color: #00ADEE !important;
     	color: #FFF !important;
     }
     .colors__iflyer a{
     	color: #FFF !important;
     }
     a.colors__iflyer{
     	background-color: #00ADEE !important;
     	color: #FFF !important;
     }
     .colors__iflyer--text{
     	color: #00ADEE !important;
     }
     .colors__iflyer--text--hover{
     	color: #00ADEE !important;
     }
     .colors__iflyer--hover:HOVER{
     	background-color: #00ADEE !important;
     	color: #FFF !important;
     }
     .colors__iflyer--borders{
     	border-color: #00ADEE !important;
     }
     .colors__iflyer--borders--hover:HOVER{
     	border-color: #00ADEE !important;
     }
     .colors__s{
     	background-color: #448aff !important;
     	color: #000 !important;
     }
     .colors__s a{
     	color: #000 !important;
     }
     a.colors__s{
     	background-color: #448aff !important;
     	color: #000 !important;
     }
     .colors__s--text{
     	color: #448aff !important;
     }
     .colors__s--text--hover{
     	color: #448aff !important;
     }
     .colors__s--hover:HOVER{
     	background-color: #448aff !important;
     	color: #000 !important;
     }
     .colors__s--borders{
     	border-color: #448aff !important;
     }
     .colors__s--borders--hover:HOVER{
     	border-color: #448aff !important;
     }
     .colors__s-light{
     	background-color: #83b9ff !important;
     	color: #000 !important;
     }
     .colors__s-light a{
     	color: #000 !important;
     }
     a.colors__s-light{
     	background-color: #83b9ff !important;
     	color: #000 !important;
     }
     .colors__s-light--text{
     	color: #83b9ff !important;
     }
     .colors__s-light--text--hover{
     	color: #83b9ff !important;
     }
     .colors__s-light--hover:HOVER{
     	background-color: #83b9ff !important;
     	color: #000 !important;
     }
     .colors__s-light--borders{
     	border-color: #83b9ff !important;
     }
     .colors__s-light--borders--hover:HOVER{
     	border-color: #83b9ff !important;
     }
     .colors__s-dark{
     	background-color: #005ecb !important;
     	color: #FFF !important;
     }
     .colors__s-dark a{
     	color: #FFF !important;
     }
     a.colors__s-dark{
     	background-color: #005ecb !important;
     	color: #FFF !important;
     }
     .colors__s-dark--text{
     	color: #005ecb !important;
     }
     .colors__s-dark--text--hover{
     	color: #005ecb !important;
     }
     .colors__s-dark--hover:HOVER{
     	background-color: #005ecb !important;
     	color: #FFF !important;
     }
     .colors__s-dark--borders{
     	border-color: #005ecb !important;
     }
     .colors__s-dark--borders--hover:HOVER{
     	border-color: #005ecb !important;
     }
     .colors__grey-100{
     	background-color: #f5f5f5 !important;
     	color: #000 !important;
     }
     .colors__grey-100 a{
     	color: #000 !important;
     }
     a.colors__grey-100{
     	background-color: #f5f5f5 !important;
     	color: #000 !important;
     }
     .colors__grey-100--text{
     	color: #f5f5f5 !important;
     }
     .colors__grey-100--text--hover{
     	color: #f5f5f5 !important;
     }
     .colors__grey-100--hover:HOVER{
     	background-color: #f5f5f5 !important;
     	color: #000 !important;
     }
     .colors__grey-100--borders{
     	border-color: #f5f5f5 !important;
     }
     .colors__grey-100--borders--hover:HOVER{
     	border-color: #f5f5f5 !important;
     }
     .colors__grey-200{
     	background-color: #eeeeee !important;
     	color: #000 !important;
     }
     .colors__grey-200 a{
     	color: #000 !important;
     }
     a.colors__grey-200{
     	background-color: #eeeeee !important;
     	color: #000 !important;
     }
     .colors__grey-200--text{
     	color: #eeeeee !important;
     }
     .colors__grey-200--text--hover{
     	color: #eeeeee !important;
     }
     .colors__grey-200--hover:HOVER{
     	background-color: #eeeeee !important;
     	color: #000 !important;
     }
     .colors__grey-200--borders{
     	border-color: #eeeeee !important;
     }
     .colors__grey-200--borders--hover:HOVER{
     	border-color: #eeeeee !important;
     }
     .colors__grey-300{
     	background-color: #e0e0e0 !important;
     	color: #000 !important;
     }
     .colors__grey-300 a{
     	color: #000 !important;
     }
     a.colors__grey-300{
     	background-color: #e0e0e0 !important;
     	color: #000 !important;
     }
     .colors__grey-300--text{
     	color: #e0e0e0 !important;
     }
     .colors__grey-300--text--hover{
     	color: #e0e0e0 !important;
     }
     .colors__grey-300--hover:HOVER{
     	background-color: #e0e0e0 !important;
     	color: #000 !important;
     }
     .colors__grey-300--borders{
     	border-color: #e0e0e0 !important;
     }
     .colors__grey-300--borders--hover:HOVER{
     	border-color: #e0e0e0 !important;
     }
     .colors__grey-400{
     	background-color: #bdbdbd !important;
     	color: #000 !important;
     }
     .colors__grey-400 a{
     	color: #000 !important;
     }
     a.colors__grey-400{
     	background-color: #bdbdbd !important;
     	color: #000 !important;
     }
     .colors__grey-400--text{
     	color: #bdbdbd !important;
     }
     .colors__grey-400--text--hover{
     	color: #bdbdbd !important;
     }
     .colors__grey-400--hover:HOVER{
     	background-color: #bdbdbd !important;
     	color: #000 !important;
     }
     .colors__grey-400--borders{
     	border-color: #bdbdbd !important;
     }
     .colors__grey-400--borders--hover:HOVER{
     	border-color: #bdbdbd !important;
     }
     .colors__grey-500{
     	background-color: #9e9e9e !important;
     	color: #000 !important;
     }
     .colors__grey-500 a{
     	color: #000 !important;
     }
     a.colors__grey-500{
     	background-color: #9e9e9e !important;
     	color: #000 !important;
     }
     .colors__grey-500--text{
     	color: #9e9e9e !important;
     }
     .colors__grey-500--text--hover{
     	color: #9e9e9e !important;
     }
     .colors__grey-500--hover:HOVER{
     	background-color: #9e9e9e !important;
     	color: #000 !important;
     }
     .colors__grey-500--borders{
     	border-color: #9e9e9e !important;
     }
     .colors__grey-500--borders--hover:HOVER{
     	border-color: #9e9e9e !important;
     }
     .colors__grey-600{
     	background-color: #757575 !important;
     	color: #FFF !important;
     }
     .colors__grey-600 a{
     	color: #FFF !important;
     }
     a.colors__grey-600{
     	background-color: #757575 !important;
     	color: #FFF !important;
     }
     .colors__grey-600--text{
     	color: #757575 !important;
     }
     .colors__grey-600--text--hover{
     	color: #757575 !important;
     }
     .colors__grey-600--hover:HOVER{
     	background-color: #757575 !important;
     	color: #FFF !important;
     }
     .colors__grey-600--borders{
     	border-color: #757575 !important;
     }
     .colors__grey-600--borders--hover:HOVER{
     	border-color: #757575 !important;
     }
     .colors__grey-700{
     	background-color: #616161 !important;
     	color: #FFF !important;
     }
     .colors__grey-700 a{
     	color: #FFF !important;
     }
     a.colors__grey-700{
     	background-color: #616161 !important;
     	color: #FFF !important;
     }
     .colors__grey-700--text{
     	color: #616161 !important;
     }
     .colors__grey-700--text--hover{
     	color: #616161 !important;
     }
     .colors__grey-700--hover:HOVER{
     	background-color: #616161 !important;
     	color: #FFF !important;
     }
     .colors__grey-700--borders{
     	border-color: #616161 !important;
     }
     .colors__grey-700--borders--hover:HOVER{
     	border-color: #616161 !important;
     }
     .colors__grey-800{
     	background-color: #424242 !important;
     	color: #FFF !important;
     }
     .colors__grey-800 a{
     	color: #FFF !important;
     }
     a.colors__grey-800{
     	background-color: #424242 !important;
     	color: #FFF !important;
     }
     .colors__grey-800--text{
     	color: #424242 !important;
     }
     .colors__grey-800--text--hover{
     	color: #424242 !important;
     }
     .colors__grey-800--hover:HOVER{
     	background-color: #424242 !important;
     	color: #FFF !important;
     }
     .colors__grey-800--borders{
     	border-color: #424242 !important;
     }
     .colors__grey-800--borders--hover:HOVER{
     	border-color: #424242 !important;
     }
     .colors__grey-900{
     	background-color: #212121 !important;
     	color: #FFF !important;
     }
     .colors__grey-900 a{
     	color: #FFF !important;
     }
     a.colors__grey-900{
     	background-color: #212121 !important;
     	color: #FFF !important;
     }
     .colors__grey-900--text{
     	color: #212121 !important;
     }
     .colors__grey-900--text--hover{
     	color: #212121 !important;
     }
     .colors__grey-900--hover:HOVER{
     	background-color: #212121 !important;
     	color: #FFF !important;
     }
     .colors__grey-900--borders{
     	border-color: #212121 !important;
     }
     .colors__grey-900--borders--hover:HOVER{
     	border-color: #212121 !important;
     }
     .colors__facebook{
     	background-color: #3B5998 !important;
     	color: #FFF !important;
     }
     .colors__facebook a{
     	color: #FFF !important;
     }
     a.colors__facebook{
     	background-color: #3B5998 !important;
     	color: #FFF !important;
     }
     .colors__facebook--text{
     	color: #3B5998 !important;
     }
     .colors__facebook--text--hover{
     	color: #3B5998 !important;
     }
     .colors__facebook--hover:HOVER{
     	background-color: #3B5998 !important;
     	color: #FFF !important;
     }
     .colors__facebook--borders{
     	border-color: #3B5998 !important;
     }
     .colors__facebook--borders--hover:HOVER{
     	border-color: #3B5998 !important;
     }
     .colors__twitter{
     	background-color: #00aced !important;
     	color: #FFF !important;
     }
     .colors__twitter a{
     	color: #FFF !important;
     }
     a.colors__twitter{
     	background-color: #00aced !important;
     	color: #FFF !important;
     }
     .colors__twitter--text{
     	color: #00aced !important;
     }
     .colors__twitter--text--hover{
     	color: #00aced !important;
     }
     .colors__twitter--hover:HOVER{
     	background-color: #00aced !important;
     	color: #FFF !important;
     }
     .colors__twitter--borders{
     	border-color: #00aced !important;
     }
     .colors__twitter--borders--hover:HOVER{
     	border-color: #00aced !important;
     }

     /*savepage-import-url=/css/my.css?siteVersion=1.0.6*/ @charset "utf-8";
     /* CSS Document */

/* #user_holder{
    display: none !important;
}
*/

.std-iframe-popup .mfp-iframe-scaler iframe{
	background: #B6B6B6;
}

.std-iframe-popup .mfp-iframe-holder .mfp-content{
	height: 550px;
	max-width: 600px;
}

.offtop .globalnav_active #globalnav-holder{
	background: #333;
}

#facebookuser_holder{
	display: none !important;
}

#youtickets_holder{
	display: none !important;
}

@media only screen and (max-width: 979px){
	.nav-list-holder, #globalnav-holder {
		background: #333;
	}

}

@media only screen and (min-width: 980px){

	body:not(.offtop) #banner h1{
		font-size: 30px;
		line-height: 2em;
		position: absolute;
		top: 0px;
		left: 25px;
	}

	body:not(.offtop) #navigation{
		margin-top: -2px;
	}

	body:not(.offtop) #banner h1 a{

		line-height: 2em;
	}



	body:not(.offtop)  #navigation{
		margin-top: -2px;
	}

	body:not(.offtop) #pg{
		padding-top: 60px;
	}
	body:not(.offtop) #globalnav{
		padding-top: 8px;
	}

	body:not(.offtop) #hd{
		height: 60px;
		position: fixed;
		border-bottom: 1px solid rgba(100,100,100,0.2);
	}

	body:not(.offtop) #banner{
		position: absolute;
		top: 0px;
		left: 0px;
	}

	body:not(.offtop) #banner h1 a:before {
		top: -2px !important;
	}

}

#banner h1 a{
	font-weight: 200 !important;
}

.offtop #pg{
	margin-top: 60px !important;
}

#banner h1 a:before {
	background-color: transparent !important;
	content: '\e962' !important;
	color: #000 !important;

}

#banner h1 a{
	font-weight: 200 !important;
}

.offtop #pg{
	margin-top: 60px !important;
}

#banner h1 a:before {
	background-color: transparent !important;
	content: '\e962' !important;
	color: #000 !important;

}

#banner h1 a{
	color: #FFF !important;
}

#banner h1 a:before{
	color: #FFF !important;
}

#globalnav a, #globalnav strong{
	color: #FFF !important;
}

#hd{
	background-image: linear-gradient(to right bottom, #fc5c7e, #f359a3, #db62c7, #b072e6, #6a82fc);
}

#tagscope{
	background: #F9F9F9 !important;
	box-shadow: none;
}

.featherlight .featherlight-content{
	width: 95% !important;
	max-width: 900px !important;
	height: 95% !important;
	padding: 0px !important;
}

.featherlight .featherlight-inner {
	display: block;
	height: 100%;
}

.featherlight .featherlight-close-icon{
	background-color: #00afee !important;
}

@media only screen and (max-width: 663px){

	.featherlight .featherlight-content{
		width: 95% !important;
		height: 95% !important;
		padding: 0px !important;
	}
}

@media only screen and (max-width: 400px){

	.featherlight .featherlight-content{
		width: 98% !important;
		height: 98% !important;
		padding: 0px !important;
	}
}   

@media only screen and (max-width: 350px){

	.featherlight .featherlight-content{
		width: 100% !important;
		height: 98% !important;
		padding: 0px !important;
	}
}   
/*savepage-import-url=/css/mdl/application.css?siteVersion=1.0.6*/ @charset "utf-8";

/*
       _   ___ _____  __    _   ___ ___ _    ___ ___   _ _____ ___ ___  _  _
      /_\ | _ \ __\ \/ /   /_\ | _ \ _ \ |  |_ _/ __| /_\_   _|_ _/ _ \| \| |
     / _ \|  _/ _| >  <   / _ \|  _/  _/ |__ | | (__ / _ \| |  | | (_) | .` |
    /_/ \_\_| |___/_/\_\ /_/ \_\_| |_| |____|___\___/_/ \_\_| |___\___/|_|\_|

    New clean(er) css for apex
    */


/*
     _  _     _   _  __ _         _   _
    | \| |___| |_(_)/ _(_)__ __ _| |_(_)___ _ _  ___
    | .` / _ \  _| |  _| / _/ _` |  _| / _ \ ' \(_-<
    |_|\_\___/\__|_|_| |_\__\__,_|\__|_\___/_||_/__/

    */

    /* MAKE ROOM */

    #globalnav{
    	margin-right: 40px;
    }

    #globalnav .material-icons{
    	font-size:  1.2em;
    	position: relative;
    	top: 4px;
    	margin-left: 3px;
    }

    .notifications {
    	padding-right: 20px;
    	position: relative;
    	cursor: pointer;
    }

    .notifications:after {
    	content: "\e90a";
    	font-family: 'zaiko-icons' !important;
    	height: 16px;
    	width: 16px;
    	position: absolute;
    	right: 0px;
    	top: 0px;
    	line-height: 1em;
    	font-size: 1.2em;

    }

    .notification_subheader {
    	margin-left: 10px;
    	font-size: 14px;
    	border-bottom: 1px solid #EFEFEF;
    	padding-bottom: 3px;
    }

    .notification_subheader sup {
    	font-size: 8px;
    	background: #999;
    	color: #FFF;
    	padding: 0px 3px;
    	float: right;
    	text-align: center;
    	width: 40px;
    }

    .notification_header {
    	background: #EFEFEF;
    	padding: 10px;
    	margin-left: 5px;
    	margin-top: 10px;
    	height: 54px;
    	margin-bottom: 10px;
    }

    #notify_preview_holder .notification_home{
    	display: block;
    	text-align: center;
    	font-size: 12px;
    }

    .notification_header img {
    	float: left;
    	margin-right: 5px;
    	vertical-align: middle;
    }

    .notification_header div {
    	margin-bottom: 2px;
    }

    .notification_header span {
    	background: #CCC;
    	color: #FFF;
    	display: inline-block;
    	margin-right: 10px;
    	width: 90px;
    	padding: 0px 10px;
    	text-align: right;
    }

    #notify_holder{
    	position: absolute;
    	right: 16px;
    	top: 16px;
    	padding-right: 1.6em;
    }



    #notify_holder:HOVER > .total_notify{
    	opacity: 1;
    }

    #notify_holder > .total_notify.none{
    	display: none !important;
    }


    #notify_preview_holder {
    	position: absolute;
    	z-index: 200;
    	width: 300px;
    	height: 282px;
    	right: 0px;
    	top: 30px;
    	background: #fff;
    	border: 1px solid #ccc;
    	border-color: rgba(0,0,0,.2);
    	box-shadow: 0 2px 10px rgba(0,0,0,.2);
    	display: none;
    }



    #notify_preview_holder:before {
    	top: -16px;
    	height: 0px;
    	width: 0px;
    	right: 0px;
    	margin-left: -20px;
    	border-right: 20px solid transparent;
    	border-bottom: 15px solid rgba(0,0,0,.2);;
    	border-left: 20px solid transparent;
    	position: absolute;
    	background: transparent;
    	content: '';
    } 

    #notify_preview_holder:after {
    	top: -15px;
    	height: 0px;
    	width: 0px;
    	right: 0px;
    	margin-left: -20px;
    	border-right: 20px solid transparent;
    	border-bottom: 15px solid #FFF;
    	border-left: 20px solid transparent;
    	position: absolute;
    	background: transparent;
    	content: '';
    } 

    #notify_holder.toggleon #notify_preview_holder{
    	display: block;
    }

    #notify_preview{
    	margin: 10px;
    	height: 250px;
    	overflow-x: hidden;
    	overflow-y: scroll;
    	width: 286px;
    }

    #notify_preview li {
    	position: relative;
    	display: block;
    	border: 0px;
    	margin-bottom: 5px;
    	width: 260px;
    }



    #notify_preview li a {
    	display: block;
    	padding: 10px;
    	line-height: 20px;
    }

    #notify_preview li a:HOVER {
    	text-decoration: none;
    }

    #notify_preview li.no_notifications{
    	width: 100px;
    	height: 100px;
    	background: /*savepage-url=/img/new_icons/bell.png*/ url() center center no-repeat #DDD;
    	border-radius: 100px;
    	margin: 60px auto;
    }


    #notify_preview li span {

    	padding: 0px 4px;
    	border-radius: 3px;
    	font-size: 12px;
    	position: absolute;
    	top: 10px;
    	right: 10px;

    }

/*
     ___ __  __   _   _    _      __  __ ___ _  _ _   _
    / __|  \/  | /_\ | |  | |    |  \/  | __| \| | | | |
    \__ \ |\/| |/ _ \| |__| |__  | |\/| | _|| .` | |_| |
    |___/_|  |_/_/ \_\____|____| |_|  |_|___|_|\_|\___/

    */


    .offtop #globalnav{
    	margin-right: 0px;
    }

    .offtop #notify_holder{
    	right: 100px;
    	top: 17px;
    	z-index: 9;
    }

    .offtop .notifications:after{
    	font-size: 24px;
    	height: 24px;
    	width: 24px;
    }


    .offtop #notify_preview_holder{
    	top:  40px;   
    }

    @media only screen and (max-width: 979px){ 

    	#globalnav{
    		margin-right: 0px;
    	}

    	#notify_holder{
    		right: 100px;
    		top: 17px;
    		z-index: 9;
    	}

    	.notifications:after{
    		font-size: 24px;
    		height: 24px;
    		width: 24px;
    	}


    	#notify_preview_holder{
    		top:  40px;   
    	}

    }


/*
     _    ___   ___  ___
    | |  / _ \ / __|/ _ \
    | |_| (_) | (_ | (_) |
    |____\___/ \___|\___/

    */

    #banner h1 a{
    	position: relative;
    	padding-left: 50px;
    }

    #banner h1 a:before{
    	content: '\e93c';
    	display: block;
    	position: absolute;
    	left: 0px;
    	line-height: 1em;
    	top: 0px;
    	width: 34px;
    	font-size: 34px;
    	height: 30px;
    	font-family: 'zaiko-icons';
    	font-weight: normal;
    	font-style: normal;
    	background-color: #448aff;
    	padding: 4px;
    	color: #FFF;
    	text-align: center;
    	overflow: hidden;
    	padding-left: 2px;
    }

    .logo_branding #banner h1 a:before{
    	display: none;
    }

    .offtop #banner h1 a{
    	padding-left: 45px;
    }


    .offtop #banner h1 a:before{
    	left: 0px;
    	top: 0px;
    	width: 31px;
    	font-size: 31px;
    	height: 28px;
    	padding: 3px;
    	overflow: hidden;
    	padding-left: 3px;
    }

    @media only screen and (max-width: 979px){ 

    	#banner h1 a{
    		padding-left: 45px;
    	}

    	#banner h1 a:before{
    		left: 0px;
    		top: 0px;
    		width: 31px;
    		font-size: 31px;
    		height: 28px;
    		padding: 3px;
    		overflow: hidden;
    		padding-left: 3px;
    	}

    }


/*
     _____ ___  ___   __  __
    |_   _/ _ \| _ \ |  \/  |___ _ _ _  _
      | || (_) |  _/ | |\/| / -_) ' \ || |
      |_| \___/|_|   |_|  |_\___|_||_\_,_|s

      */

      #youtickets_holder{
      	background-image: linear-gradient(to right bottom, #fc5c7e, #f359a3, #db62c7, #b072e6, #6a82fc);
      	border-radius: 3px;
      	margin-left: 5px;
      }


      #youtickets_holder:HOVER{
      	background-image: none;
      }

      #youadmin_holder{
      	background: #00b5e5;
      	border-radius: 3px;
      	margin-left: 5px;
      }

      #youadmin_holder:HOVER{
      	background-image: none;
      }

      #youadmin_holder a{
      	color: #FFF !important;
      }

      #appstore_holder a{
      	position: relative;
      	padding-right: 2.2em !important;
      }

      #appstore_holder a:after{
      	position: absolute;
      	content:  '\e884';
      	width: 20px;
      	height: 20px;
      	display: block;
      	font-family: 'Material Icons';
      	display: block;
      	height: 12px;
      	right: 0px;
      	top: 0px;
      	font-size: 1.5em;
      }


      #buypromotion_holder a{
      	position: relative;
      	padding-right: 2.2em !important;
      }

      #buypromotion_holder a:after{
      	position: absolute;
      	content:  '\e263';
      	width: 20px;
      	height: 20px;
      	display: block;
      	font-family: 'Material Icons';
      	display: block;
      	height: 12px;
      	right: 0px;
      	top: 0px;
      	font-size: 1.5em;
      }

      .offtop #buypromotion_holder a:after{
      	line-height: 3em;
      	right:  10px;
      }


      #buypromotion_holder a span{
      	display: inline-block;
      	padding: 0px 5px;
      }

      a#list-link-listings , a#list-link-events , a#list-link-stard , a#list-link-reports, a#list-link-mambers , a#list-link-contacts , a#list-link-profile ,a#list-link-profiles , a#list-link-admin, a#list-link-home {
      	padding: 0px 2.5em 0px 2.8em !important;
      }

      #list-item-admin .list-link-sub-theflame, #list-item-admin .list-link-sub-support, #list-item-admin .list-link-sub-thelamppost, #list-item-admin .list-link-sub-corkboard, #list-item-admin .list-link-sub-theswan,#list-item-admin .list-link-sub-newsletter, #list-item-admin .list-link-sub-zaiko, #list-item-mytickets .list-link-sub-zaiko, #list-item-admin .list-link-sub-devtools{
      	padding-left: 2.5em !important;
      }


      #list-link-stard:after , #list-link-reports:after,#list-link-vpaprofile:after, #list-link-members:after, #list-link-contacts:after, #list-link-profile:after,#list-link-profiles:after, #list-link-admin:after,#list-link-home:after, #list-item-admin .list-link-sub-theflame:after, #list-item-admin .list-link-sub-support:after, #list-item-admin .list-link-sub-thelamppost:after, #list-item-admin .list-link-sub-corkboard:after, #list-item-admin .list-link-sub-theswan:after,#list-item-admin .list-link-sub-newsletter:after,#list-item-admin .list-link-sub-zaiko:after ,#list-item-mytickets .list-link-sub-zaiko:after,#list-item-admin .list-link-sub-devtools:after {
      	content: '\e87D';
      	display: block;
      	position: absolute;
      	left: 6px;
      	top: 0px;
      	width: 20px;
      	font-size:  18px;
      	height: 20px;
      	font-family: 'Material Icons';
      	font-weight: normal;
      	font-style: normal;
      }

      #list-link-members:after {
      	content: '\e7ef';
      }

      #list-link-reports:after {
      	content: '\e6e1';
      }

      #list-link-vpaprofile:after {
      	text-transform: none;
      	content: 'change_history';
      }

      #list-link-contacts:after, #list-link-profile:after {
      	content: '\e0ba';
      }

      #list-link-admin:after {
      	content: '\e5c3';
      }

      #list-link-home:after {
      	content: '\e88a';
      }

      #list-link-events:after {
      	content: '\e838';
      	display: block;
      	position: absolute;
      	left: 2px;
      	top: -1px;
      	width: 20px;
      	font-size:  18px;
      	height: 20px;
      	font-family: 'zaiko-icons';
      	font-weight: normal;
      	font-style: normal;
      }

      #list-link-events:after {
      	content: '\e93c';
      }

      #list-link-listings:after {
      	display: block;
      	position: absolute;
      	left: 2px;
      	top: -1px;
      	width: 20px;
      	font-size:  18px;
      	height: 20px;
      	font-weight: normal;
      	font-style: normal;
      	content: 'list_alt';
      	text-transform: initial !important;
      	font-family: 'Material Icons';
      }
      #list-link-mytickets:after {
      	display: block;
      	position: absolute;
      	left: 2px;
      	top: -1px;
      	width: 20px;
      	font-size:  18px;
      	height: 20px;
      	font-weight: normal;
      	font-style: normal;
      	content: 'confirmation_number';
      	text-transform: initial !important;
      	font-family: 'Material Icons';
      }


      #list-link-mytickets {
      	border-color: #fc5c7e !important;
      }


      #list-link-listings:after {
      	content: 'list_alt';
      }


      #list-link-profiles:after {
      	content: '\e886';
      }

      #list-item-admin{
      	background-repeat: no-repeat;
      }

      #list-item-admin .list-link-sub-theflame:after, #list-item-admin .list-link-sub-support:after, #list-item-admin .list-link-sub-thelamppost:after, #list-item-admin .list-link-sub-corkboard:after, #list-item-admin .list-link-sub-theswan:after,#list-item-admin .list-link-sub-newsletter:after,#list-item-admin .list-link-sub-zaiko:after,#list-item-mytickets .list-link-sub-zaiko:after ,#list-item-mytickets .list-link-sub-devtools:after {
      	top: 5px;
      	left: 7px;

      }

/* .list-item.inline-icon .list-link{
    padding-left: 5px !important;
    } */

    .list-item.inline-icon .list-link .material-icons{
    	font-size:  1.3em;
    	text-indent: -20px;
    	margin-right:  0.15em;
    }

    #list-item-admin .list-link-sub-theflame:after{
    	content: '\e80e';
    }

    #list-item-admin .list-link-sub-devtools:after{
    	content: 'settings_input_hdmi';
    	text-transform: initial !important;
    }

    #list-item-admin .list-link-sub-support:after{
    	content: '\e8af';
    }

    #list-item-admin .list-link-sub-thelamppost:after{
    	content: '\e84f';
    }

    #list-item-admin .list-link-sub-corkboard:after{
    	content: '\e871';
    }

    #list-item-admin .list-link-sub-theswan:after{
    	content: '\e262';
    }

    #list-item-admin .list-link-sub-newsletter:after{
    	content: '\e0be';
    }

    #list-item-admin .list-link-sub-zaiko:after{
    	content: '\e961';
    	font-family: 'zaiko-icons';
    }

    #list-item-mytickets .list-link-sub-zaiko:after{
    	content: '\e961';
    	font-family: 'zaiko-icons';
    }

    .navi-partners span{
    	font-weight: normal;
    	font-size: 0.8em;
    }


    .site_notify.active{
    	position: absolute;
    	right: 0px !important;
    	top: 0px !important;
    	z-index: 100;
    	background: #EFDB69 !important;
    	font-size:  10px;
    	width: 20px;
    	text-align: center;
    	border-radius: 20px;
    	display: inline-block;
    	color: #111;
    	line-height: 20px !important;
    } 

    .simpleHeader {
    	background-color: #AAA;
    	color: #111;
    	padding-left: 14px;
    	line-height: 23px;
    	text-align: center;
    	box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);
    	z-index: 10;
    	position: relative;
    	font-size: 30px;
    	font-weight: 200;
    	padding-top: 20px;
    	padding-bottom: 20px;
    }


    /*savepage-import-url=/css/mdl/shared_application.css?siteVersion=1.0.6*/ @charset "utf-8";


/*
     _  _     _     _ ___ _ __   _____ ___                             _
    | \| |___| |_  (_) __| |\ \ / / __| _ \  __ _ ___  _____ _____ _ _| |_ ___
    | .` / _ \  _| | | _|| |_\ V /| _||   / / _` (_-< / -_) V / -_) ' \  _(_-<
    |_|\_\___/\__| |_|_| |____|_| |___|_|_\ \__,_/__/ \___|\_/\___|_||_\__/__/

    */

    #list-link-events:after {
    	content: '\E878';
    	font-family: 'Material Icons';
    }

    #list-link-listings:after {
    	content: 'list_alt';
    	text-transform: initial !important;
    	font-family: 'Material Icons';
    }

    .section_disabled{
    	opacity: 0.3;
    }


/*
     ___             _     _    _    _
    | __|_ _____ _ _| |_  | |  (_)__| |_
    | _|\ V / -_) ' \  _| | |__| (_-<  _|
    |___|\_/\___|_||_\__| |____|_/__/\__|

    */



    .event_list_block, .venue_list_block, .user_list_block, .news_list_block {
    	height: 70px !important;
    	background: none !important;
    	padding-top: 0px !important;
    	color: inherit !important;
    }

    .event_list_block img, .venue_list_block img, .user_list_block img, .news_list_block img{
    	height: 70px;
    	margin: 0px !important;
    	margin-right: 5px !important;
    }

    .user_list_block img{
    	height: 50px;
    }

    .formitem .event_list_block,.formitem  .venue_list_block,.formitem  .user_list_block,.formitem  .news_list_block{
    	margin-bottom: 5px;
    }

    .user_list_block, .news_list_block {
    	height: 50px !important;   
    }

    .venue_list_block.article_list_block {
    	padding-left: 130px !important;
    }

    .selected .event_list_block a, .selected .venue_list_block a, .selected .user_list_block,.selected .news_list_block {
    	color: #000 !important;
    }

    .venue_list_block.article_list_block a{
    	font-weight: normal !important;
    }

    .event_list_block img, .venue_list_block img, .user_list_block img, .news_list_block img {
    	left: 0px !important;
    	top: 0px !important;
    	margin-right: 10px;
    	float:  left;
    }

    .artist-chip .mdl-chip__text{
    	max-width: 200px;
    	overflow:  hidden;
    	text-overflow: ellipsis;
    }


/*
     ___ ___  _____  __
    | __| _ )/ _ \ \/ /
    | _|| _ \ (_) >  <
    |_| |___/\___/_/\_\
Remove when move over to other lightbox
*/


.f_popup{
	display:none; 
	position:fixed; 
	top:50%; left:50%; 
	width:490px; 
	height:430px; 
	padding:0px; 
	margin:-225px 0 0 -255px; 
	border-radius:0px; 
	z-index:25000;
	border: 10px solid rgba(0,0,0,0.5);
	border-radius: 10px;
}

body.platform_ios .f_popup{
	-webkit-overflow-scrolling: touch; 
	overflow:auto;
}

.f_popup iframe{
	background: /*savepage-url=/img/spinner.gif*/ url() center center no-repeat #FFF;
}

#f_closebutton{
	display: none;
	position: absolute;
	background: /*savepage-url=/img/silk_icons/cancel.png*/ url() center center no-repeat rgba(0,0,0,0.5);
	width: 50px;
	height: 50px;
	top: -35px;
	right: -35px;
	border-radius: 50px;
	cursor: pointer;
}

#f_closebutton:HOVER{
	background-color: rgba(255,255,255,0.5);
}

#f_fullbutton{
	background: /*savepage-url=/img/silk_icons/arrow_out.png*/ url() center center no-repeat rgba(0,0,0,0.5);
	position: absolute;
	width: 100px;
	height: 25px;
	top: -35px;
	left: 50%;
	margin-left: -50px;
	border-radius: 15px 15px 0px 0px;
	cursor: pointer;
	display: none;
}

body.lightbox_mode:NOT(.logged_out){
	background: #F9F9F9;
}



#f_fullbutton:HOVER{
	background-color: rgba(255,255,255,0.5);
}

#_f_popup.fullscreen #f_fullbutton{
	background: /*savepage-url=/img/silk_icons/arrow_in.png*/ url() center center no-repeat rgba(0,0,0,0.5);
}

#_f_popup.fullscreen{
	left: 0px !important;
	right: 0px !important;
	top: 30px !important;
	bottom: 0px !important;
	margin: 0px !important;
	width: auto !important;
	height: auto !important;
}

#_f_popup.fullscreen iframe{
	width: 100% !important;
	height: 100% !important;
}

#_f_popup.fullscreen #f_closebutton{
	right: -10px !important;
}

.f_popup iframe::-webkit-scrollbar {
	width: 12px;
}

.f_popup iframe::-webkit-scrollbar-track {
	background-color: #FFF;
	border-radius: 20px;
	border-left: 0px solid #ccc;
}
.f_popup iframe::-webkit-scrollbar-thumb {
	background-color: #ccc;
	border-radius: 20px;
}
.f_popup iframe::-webkit-scrollbar-thumb:hover {
	background-color: #aaa;
}

.f_popup:HOVER iframe::-webkit-scrollbar-track {
	background-color: #F9F9F9;
}

.is_geo_lock{
	font-size: 10px;
	top: 1px;
	position: relative;
	color: #333;
	background: #f9a825;
	border-radius: 20px;
	height: 1.5em;
	line-height: 1.5em;
	width: 1.5em;
	text-align: center;
}

.mdl-lightbox .mfp-iframe-holder .mfp-content{
	max-width: 1200px !important;
}

.mdl-lightbox .mfp-iframe-scaler{
	padding-top: 80% !important;
}

/*
     _    ___   ___ ___
    | |  / _ \ / __/ __|
    | |_| (_) | (_ \__ \
    |____\___/ \___|___/

    */


    .focus_log{
    	background: #F9F9F9;
    	font-size: 14px;
    }

    .focus_log *{
    	font-size:  14px;
    	font-weight: 300;
    }

    .focus_log li:NOT(.subject_post) .profile_preview{
    	display: none;
    }

    .focus_log .profile_preview img{
    	border-radius: 30px;
    	width:  40px;
    	height: 40px;
    	margin-right: 10px;
    }

    .focus_log  .profile_name{
    	font-size: 16px;
    }

    .focus_log .writetowall{
    	background:  #fff9c4;
    	padding:  1em;
    	margin: 0.4em;
    	font-size: 16px;
    	font-weight: 500;
    	border-radius: 2px;
    	position: relative;
    }

    .focus_log .writetowall:before {
    	border-width: 0 20px 20px 0;
    	border-color: transparent transparent #FFF transparent;
    	position: absolute;
    	top: -20px;
    	left: 28px;
    	content: '';
    	width: 0;
    	height: 0;
    	border-style: solid;
    }

    .focus_log .writetowall:after {
    	border-width: 0 15px 15px 0;
    	border-color: transparent transparent #fff9c4 transparent;
    	position: absolute;
    	top: -15px;
    	left: 30px;
    	content: '';
    	width: 0;
    	height: 0;
    	border-style: solid;
    }


    .focus_log li:NOT(.subject_post) .block_main{
    	display: none;
    }

    .focus_log li:NOT(.subject_post) .profile_name{
    	display: none;
    }

    .focus_log .postto{
    	display: none;
    }

    .focus_log li.subject_post > a > img{
    	width:  50px;
    	height: 50px;
    	margin-right: 5px;
    }


    .focus_log li.subject_post time{
    	font-size:  12px;
    	color:  #999;
    }

    .focus_log .postto_profile{
    	display: none;
    }

    .focus_log .block_moreinfo{
    	margin:  5px 0px;
    }

    .focus_log .block_moreinfo .story_text{
    	display: block;
    	margin-top: 5px;
    	font-size:  16px;
    	font-weight: 500;
    }

    .focus_log .thread_list {
    	padding-left: 2em; 
    }

    .focus_log .thread_list .thread{
    	padding:  0.3em 0.6em;
    	background:  #F9F9F9;
    	margin-bottom: 2px;
    	position: relative;
    }

    .focus_log .thread_list .thread *{
    	font-size:  12px;
    }

    .focus_log .thread_list .thread .timestamp{
    	display: block;
    	text-align: right;
    	font-size:  12px;
    }

    .focus_log .block_moreinfo .story_text_sep{
    	display: none;
    }

    .focus_log .feed_showcase_player{
    	display: none;
    }

    .focus_log .thread_type_comment{
    	position: relative;
    }

    .focus_log .thread_list .thread_type_comment p {
    	background: #FFFFFF;
    	display: block;
    	margin: 0px 0px 4px 0px !important;
    	padding: 6px !important;
    	font-size: 12px !important;
    	border-radius: 2px;
    	color: #333;
    	line-height: 16px;
    	padding-right: 18px !important;
    	word-wrap: break-word;
    }

    .focus_log .thread_list .thread_type_comment p .spacer {
    	color: #FFF;
    }

    .thread_list .thread_type_comment p .thread_text {
    	display: block;
    	font-weight: 500;
    }

    .focus_log .thread_list .thread_type_comment {
    	padding: 5px 5px 5px 40px !important;
    	position: relative;
    }

    .focus_log .thread_type_comment img {
    	width: 30px !important;
    	height: 30px !important;
    	position: absolute;
    	left: 5px;
    	top: 5px;
    	border-radius: 30px;
    }

    .focus_log > li{
    	position: relative;
    	padding:  10px;
    	background: #FFF;
    	box-shadow: 0 1px 0 0 rgba(50,50,50,.3);
    	margin-bottom: 5px;
    	font-size: 14px;
    	border-radius: 2px;
    }

    .focus_log .thread_comments:before{
    	width:  20px;
    	height: 20px;
    	content: '\E0B9';
    	font-family: 'Material Icons';
    	display: block;
    	position: absolute;
    	font-size: 20px;
    	color: #999;
    	left: 2px;
    	top: 4px;
    }

    .focus_log .thread_comments {
    	display: block;
    	padding: 5px 5px 5px 25px;
    	margin-left: 2em;
    	position: relative;

    }

    .focus_log .thread_comments span {
    	color: rgb(68,138,255);
    	cursor: pointer;
    }

    .focus_log .thread_comments span:HOVER {
    	text-decoration: underline;
    }

    .focus_log .thread_comments.active {
    	background: #F9F9F9;
    }

    .focus_log .thread_comments.error {
    	border: 1px solid #D00000;
    }

    .focus_log .thread_comments textarea {
    	display: none;
    }

    .focus_log .thread_comments.active textarea {
    	display: block;
    	width:  100%;
    	box-sizing: border-box;
    	padding: 0.5em !important;
    	border:  none;
    }
    .focus_log .thread_comments.active span {
    	display: none;
    }

    .focus_log .thread_list .view_all_thread {

    	display: block;
    	cursor: pointer;
    	text-align: left;
    	text-align: right;
    	padding:  0.2em 0.6em;
    	background:  #FFF;
    	font-size: 10px;
    	margin-bottom: 1px;
    	cursor: pointer;
    	color: rgb(68,138,255);
    }

    .focus_log .thread_list .view_all_thread:HOVER{
    	background: #EFEFEF;
    }

    #status_update textarea{
    	width: 100% !important;
    	box-sizing: border-box;
    }

    #status_update .mdl-textfield{
    	width: auto;
    }

/*
     ___ ___   ___ _   _ ___
    | __/ _ \ / __| | | / __|
    | _| (_) | (__| |_| \__ \
    |_| \___/ \___|\___/|___/

    */

    .focus_on #hd,.focus_on #tagscope,.focus_on #ft,.focus_on .no-focus{
    	display: none !important;
    }

    .focus_on .focus-item{
    	width: 100% !important;
    }

    body:NOT(.focus_on)  .on-focus{
    	display: none;
    }

/*
     ___ ___ ___ _____    ___ _  _ ___ ___
    |   \_ _/ __|_   _|  / __| || |_ _| _ \
    | |) | |\__ \ | |   | (__| __ || ||  _/
    |___/___|___/ |_|    \___|_||_|___|_|

    */



    .dist_chip, .small_chip{
    	background:  #DDD;
    	width:  auto;
    	padding-left: 2.6em;
    	padding-right: 1.5em;
    	font-size:  10px;
    	border-radius: 30px;
    	line-height: 2em;
    	text-align: center;
    	display: inline-block;
    	position: relative;
    	margin-bottom:  0.5em;
    	margin-top: 0.5em;
    	font-weight: 700;
    }


    .dist_chip:before{
    	position: absolute;
    	left: 0px;
    	width: 2em;
    	border-radius: 2em;
    	height: 2em;
    	line-height: 2em;
    	background: #000;
    	color: #FFF;
    	top: 0px;
    	content: 'cloud';
    	font-family: 'Material Icons';
    }

    .dist_fees:before{

    	content: 'account_balance_wallet';
    }



    .dist_chip.public:before{
    	content: 'cloud';
    	background: #4caf50;
    }

    .dist_chip.private:before{
    	content: 'cloud_off';
    	background: #f44336;
    }

    .dist_chip.selective:before{
    	content: 'cloud_done';
    	background: #ff5722;
    }



    .json__link.loading{
    	opacity: 0.5;
    }

    .json__link.error{
    	background: #f44336 !important;
    }

    .json__link.loaded{
    	background: #4caf50 !important;
    }



/*
     ___ __  __   _   _    _      ___  ___ ___ ___ ___ _  _ ___
    / __|  \/  | /_\ | |  | |    / __|/ __| _ \ __| __| \| / __|
    \__ \ |\/| |/ _ \| |__| |__  \__ \ (__|   / _|| _|| .` \__ \
    |___/_|  |_/_/ \_\____|____| |___/\___|_|_\___|___|_|\_|___/

    */

    @media only screen and (max-width: 979px){ 

    }


    @media only screen and (max-width: 839px){ 

    }

    @media only screen and (max-width: 663px){ 


    	.ml-table-mobile-opt.table-responsive{
    		border:  none;
    	}

    	.ml-table-mobile-opt .mdl-data-table{
    		border:  none;
    	}

    	.ml-table-mobile-opt .ml-table-bordered>tbody>tr>td:first-child{
    		border-left:  none;
    	}

    	.ml-table-mobile-opt .ml-table-bordered>tbody>tr>td:last-child{
    		border-right:  none;
    	}
    }

    @media only screen and (max-width: 500px){ 
    	.mobile-grid-full{
    		padding: 0px;
    	}

    	.mdl-cell.mobile-full{
    		margin: -8px;
    		width: calc(100% + 16px) !important;
    	}

    	.mdl-cell.mobile-full .mdl-card{
    		box-shadow: none;
    		border-radius: 0px;
    	}
    }

    ul.showcase_holder li{
    	width: 50%;
    	display: inline-block;
    	vertical-align: top;
    	padding-left: 0px;
    	padding-right: 6px;
    	padding-top: 10px;
    	position: relative;
    	box-sizing: border-box;
    }

    ul.showcase_holder li:nth-of-type(2n){
    	padding-left: 6px;
    	padding-right: 0%;
    }

    .music_holder{
    	width: 100% !important;
    	background: #EFEFEF;
    }

    .music_holder,.video_holder{
    	width: 100%;
    	height: 0px;
    	padding-bottom: 56.25%;
    	display: inline-block;
    	vertical-align: top;
    	position: relative;
    	background: #EFEFEF;
    }


    .video_holder iframe,  .music_holder iframe{
    	position: absolute;
    	top: 0;
    	left: 0;
    	width: 100% !important;
    	height: 100% !important;
    }


    ul.showcase_holder.full li{
    	width: auto;
    	display: block;
    	padding-left: 0px !important;
    	padding-right: 0px !important;
    }

    @media only screen and (max-width: 663px){
    	ul.showcase_holder li{
    		width: auto;
    		display: block;
    		padding-left: 0px !important;
    		padding-right: 0px !important;
    	}

    }
    /*savepage-import-url=/css/application.css?siteVersion=1.0.6*/ @charset "utf-8";

/*
     ____  _   ___ _  _____     ___ ___ ___
    |_  / /_\ |_ _| |/ / _ \   / __/ __/ __|
     / / / _ \ | || ' < (_) | | (__\__ \__ \
    /___/_/ \_\___|_|\_\___/   \___|___/___/

    */


    /* Remove redundent menu items from Zaiko main */

    #globalnav #buypromotion_holder{
    	display: none;
    }

    #notify_holder{
    	display: none;
    }

    #globalnav{
    	margin-right: 0px;
    }

    #banner h1 a:before{
    	background-color: #000;
    	content: '\e962';
    	line-height: 0.95em;
    }

    #banner h1 span{
    	font-size: 0.5em;
    	display: inline-block;
    	margin-left: 0.4em;
    	font-weight: 700;
    	color: #FFF;
    	line-height: 1em;
    	letter-spacing: 0.1em;
    }

    #site_title img{
    	margin-right: 0.3em;
    	height: 40px;
    	vertical-align: text-bottom;
    	margin-left: -50px;
    	position: relative;
    	z-index: 1;
    }

    .offtop #site_title img{
    	height: 37px;
    	margin-left: -45px;
    }

    @media only screen and (max-width: 979px){
    	#site_title img{
    		height: 37px;
    		margin-left: -45px;
    	}
    }

    @media only screen and (max-width: 500px){
    	#site_title img{
    		height: 30px;
    		margin-left: -37px;
    	}
    	.offtop #site_title img{
    		height: 30px;
    		margin-left: -37px;
    	}

    }

    @media only screen and (max-width: 450px){
    	#site_title img{
    		height: 30px;
    		margin-left: -34px;
    	}
    	.offtop #site_title img{
    		height: 30px;
    		margin-left: -34px;
    	}
    }

    @media only screen and (max-width: 400px){
    	#site_title img{
    		display: none;
    	}
    }

    a.list-link-sub-profile_influencer {
    	background-color: #00C853;
    	color: #FFF !important;
    }

    .event_table_unit{
    	max-width: 400px;
    	overflow-x: hidden;
    	white-space: nowrap;
    	line-height: 1.4em;
    	text-overflow: ellipsis;
    	position: relative;
    }

    .chip-status{
    	position: absolute;
    	right: 0px;
    	bottom: 0px;
    }

    .event_table_unit img{
    	float:  left;
    }

    .stats-inchart {
    	background-color: #f3d1dc;
    	min-height: initial;
    }

    #facebookuser_holder{
    	display: none !important;
    }

/*
     _____ ___ ___ _  _____ _____   ___  ___ _____ ___ _    ___   ___  _   ___ ___
    |_   _|_ _/ __| |/ / __|_   _| |   \| __|_   _|_ _| |  / __| | _ \/_\ / __| __|
      | |  | | (__| ' <| _|  | |   | |) | _|  | |  | || |__\__ \ |  _/ _ \ (_ | _|
      |_| |___\___|_|\_\___| |_|   |___/|___| |_| |___|____|___/ |_|/_/ \_\___|___|

      */


      .ticket_list li{
      	position: relative;
      	padding-right: 4em;
      }

      .ticket_list li:HOVER{
      	background: #DDD;
      }

      .ticket_list  li.selected{
      	color: #FFF;
      	background-color: #333;
      }

      .ticket_list  li.selected a{
      	color: #FFF;
      }

      .ticket_list li a{
      	display: block;
      }

      .ticket_list li h6 span.members{
      	color:  #FFF;
      	position: absolute;
      	right: 0px;
      	top: 0px;
      	bottom: 0px;
      	display: block;
      	padding: 0.3em;
      	min-width: 3em;
      	text-align: right;
      }

      .ticket_list .days2go{
      	position: absolute;
      	right: 3px;
      	bottom: 1px;
      	height: 15px;
      }
      .ticket_list .days2go span{
      	line-height: 15px;
      	height: 12px;
      	vertical-align: top;
      	font-size: 10px;
      }


      .days2go.hijacklink{
      	cursor: pointer;
      }

      .days2go.hijacklink:HOVER{
      	background:  #333 !important;
      	color: #FFF !important;
      }


      .json-code-special{
      	background: #212121;
      	color: yellow;
      	position: relative !important;
      	text-align: left !important;
      }

      .json-code-special div {
      	max-width: 90%;
      	overflow: auto;
      	box-sizing: border-box;
      	padding: 10px;
      	word-break: break-all;
      }

      .mover *{
      	position: absolute;
      	right: 0px;
      	top: 0px;
      	width: 30px;
      	height: 30px;
      	font-size: 30px;
      	line-height: 30px;
      	text-align: center;
      	background: #999;
      	color: #FFF;
      	cursor: pointer;
      }

      .reorder{
      	background: #DDD;
      	height: 200px;
      	margin: 20px;
      	border:  5px dashed #999;
      }

      .option-item{
      	position: relative;
      }

      .disabled{
      	position: absolute;
      	left: 0px;
      	right: 0px;
      	top: 0px;
      	bottom: 0px;
      	font-size: 20px;
      	text-align: center;
      	background:  rgba(0,0,0,0.3);
      	color: #FFF;
      	font-weight: 700;
      	line-height: 40px;
      }

      .ticket_table .ticket_design, .ticket_list .ticket_design, .list_design{
      	display: inline-block;
      	font-size:  10px;
      	width: 2em;
      	height: 2em;
      	line-height: 2em;
      	text-align: center;
      	border-radius: 30px;
      	top: -2px;
      	opacity: 0.8;
      	font-weight: 700;
      	vertical-align: top;
      	position: relative;
      	border: 1px solid #000;
      }


      .list_ticket_group{
      	position: relative;
      }

      .showmembers_toggle{
      	display: block;
      	position: absolute;
      	top: 10px;
      	right: 30px;
      }

      .ticket_list .on_hold{
      	position: absolute;
      	top: 14px;
      	left: 0px;
      	line-height: 1em;
      	padding: 0.2em 0.3em;
      }


      .checkin-more{
      	position: absolute;
      	top: 0px;
      	left: 0px;
      	line-height: 1em;
      	padding:  0.2em 0.3em;
      }

      .disabled-unit{
      	opacity: 0.5;
      }

/*
     ___ _   _ ___  ___ _  _   _   ___ ___   _    ___   ___
    | _ \ | | | _ \/ __| || | /_\ / __| __| | |  / _ \ / __|
    |  _/ |_| |   / (__| __ |/ _ \\__ \ _|  | |_| (_) | (_ |
    |_|  \___/|_|_\\___|_||_/_/ \_\___/___| |____\___/ \___|

    */

    .purchase_log {
    	background: #F9F9F9;
    	font-size: 14px;
    }

    .purchase_log *{
    	font-size: 14px;
    }

    .purchase_log li{
    	position: relative;
    	padding: 10px;
    	background: #FFF;
    	box-shadow: 0 1px 0 0 rgba(50,50,50,.3);
    	margin-bottom: 5px;
    	font-size: 14px;
    	border-radius: 2px;
    	position: relative;

    }
    .purchase_log li.purchase{
    	border-left: 5px solid #1e88e5;
    }

    .purchase_log li.action_c_in{
    	border-left: 5px solid #43a047;
    }

    .purchase_log li.action_c_out{
    	border-left: 5px solid #ef5350;
    }

    .purchase_log li .user img{
    	width:  30px;
    	height: 30px;
    	border-radius: 30px;
    	margin-right:  5px;
    }

    .purchase_log li .story_text {
    	display: block;
    	margin-top: 5px;
    	font-size: 16px;
    	font-weight: 500;
    	margin-left: 30px;
    	line-height: 1.3em;
    }

    .purchase_log .list_design{
    	font-size: 10px;
    }

    .purchase_log .timestamp{
    	display: block;
    	text-align: right;
    	font-size:  12px;
    	color: #999;
    	position: absolute;
    	right: 10px;
    	bottom: 5px;
    }

    .purchase_log .payment_type{
    	color: #999;
    	position: absolute;
    	right: 10px;
    	top: 5px;
    	font-size: 12px;
    }

    .purchase_log .gate_checkin,.purchase_log .gate_checkout,.purchase_log .purcahse_summary {
    	font-weight: bold;
    	background: #43a047;
    	color: #FFF;
    	text-align: center;
    	font-size: 12px !important;
    	display: inline-block;
    	width: 2em;
    	height: 2em;
    	margin-right: 5px;
    	vertical-align: top;
    	line-height: 2em;
    	position: absolute;
    	left: 0px;
    	bottom: 0px;
    }

    .purchase_log .gate_checkout {
    	background-color: #ef5350;
    }

    .purchase_log .gate_action{
    	display: inline-block;
    	margin-right:  5px;
    }

    .purchase_log .gate_name,.purchase_log .gate_login{
    	font-size:  12px !important;
    	font-weight: 500;
    }

    .purchase_log .purcahse_summary{
    	background-color: #1e88e5;
    }


/*
     ___  ___   ____  __ ___ _  _ _____ ___
    | _ \/_\ \ / /  \/  | __| \| |_   _/ __|
    |  _/ _ \ V /| |\/| | _|| .` | | | \__ \
    |_|/_/ \_\_| |_|  |_|___|_|\_| |_| |___/

    */

    .eventispaidest{
    	padding: 20px;
    	background-color: #EEE;
    }

    .eventispaid{
    	padding: 20px;
    	background-color: #EEE;
    	font-size: 16px;
    }

    #payment_table .mdl-card__title{
    	white-space: initial;
    }

    .invoice_report th{
    	font-size:  1.3em;
    	font-weight: 300;
    	color: #000;
    }

    .invoice_report td{
    	text-align: left;
    	border: 1px solid rgba(0,0,0,.12);
    }

    .invoice_report td.num{
    	text-align: right;
    }

    .invoice_report .sales_total {
    	color: #d81b60 !important;
    	text-align: right;
    	font-size: 1.3em;
    }

    .invoice_report .sub_header th{
    	text-align: center;
    	background:  #EFEFEF;
    }

    .invoice_report thead th{
    	text-align: center;
    	background: #DDD;
    }

    .invoice_report .totals .num{
    	font-size: 1.5em;
    	font-weight: 700;
    }

    .invoice_report .invoice_report .blankspace{
    	border-top: transparent !important;
    	border-bottom: transparent;
    }

    .invoice_report .sold_0{
    	color: #999;
    }

    .ticket_payment_details{
    	padding: 0px;
    	margin: 0px;
    }

    .ticket_payment_details li{
    	border-bottom: 1px solid #FFF;
    	padding:  4px;
    }

    .ticket_payment_details strong{
    	width:  150px;
    	display: inline-block;
    }

    .invoice_report .nopeople{
    	opacity: 0.5;
    }

/*
     __  __      _ _           __  __           _                 _      _    _
    |  \/  |__ _(_) |___ _ _  |  \/  |___ _ __ | |__  ___ _ _    /_\  __| |__| |
    | |\/| / _` | | / -_) '_| | |\/| / -_) '  \| '_ \/ -_) '_|  / _ \/ _` / _` |
    |_|  |_\__,_|_|_\___|_|   |_|  |_\___|_|_|_|_.__/\___|_|   /_/ \_\__,_\__,_|

    */


    #select_summaryField, #select_summaryXField{
    	background: #FFF;
    	border: 3px #DDD solid !important;
    	box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);
    }

    #select_summaryField dd, #select_summaryXField dd{
    	line-height: 40px;
    }

    #select_summaryField strong, #select_summaryXField strong{
    	color: #444;
    	background: transparent;
    	width: 100px;
    	text-align: right;
    }

    #select_summaryField dl span, #select_summaryXField dl span{
    	font-size: 18px;
    }

    #select_summaryField dl span#selected_total, #select_summaryXField dl span#selected_totalX{
    	color: #FFF;
    	background: #333;
    	padding: 3px 10px;
    }

    #select_summaryField:before, #select_summaryXField:before{
    	content: '';
    	border-right: 50px transparent solid;
    	border-left: 50px transparent solid;
    	border-bottom: 20px #DDD solid !important;
    	top: -20px;
    	left: 5%;
    	position: absolute;
    }

    #select_summaryField:after, #select_summaryXField:after{
    	content: '';
    	border-right: 50px transparent solid;
    	border-left: 50px transparent solid;
    	border-bottom: 20px #FFF solid !important;
    	top: -16px;
    	left: 5%;
    	position: absolute;
    }

    .select_filters span.member_counter, .select_filtersX span.member_counter{
    	background: #FFF;
    	color: #333;
    	position: absolute;
    	right: 0px;
    	line-height: 1.5em;
    	top: 0px;
    	padding: 0px 10px;
    	border-radius: 0px 0 0 0px;
    	text-align: center;
    	width: 50px;
    }

    .select_filters span.already_sent, .select_filtersX span.already_sent{
    	background: #FFF100;
    	color: #333;
    	position: absolute;
    	right: 50%;
    	margin-right: -60px;
    	line-height: 1.5em;
    	top: 0px;
    	padding: 0px 10px;
    	border-radius: 0px 0px 0px 10px;
    	text-align: center;
    	width: 120px;
    }

    .select_filters.all_members, .select_filtersX.all_members {
    	line-height: 30px;
    	font-size: 20px;
    }

    .select_filters.all_members:NOT(.selected), .select_filtersX.all_members:NOT(.selected) {
    	background-color: #BBB;
    }

    .select_filters.am_a_ticket, .select_filtersX.am_a_ticket {
    	border-left: 10px solid #0091ea;
    }

    .select_filters.am_a_list, .select_filtersX.am_a_list {
    	border-left: 10px solid #d81b60;
    }

    .select_filters.am_a_lotterywin, .select_filtersX.am_a_lotterywin{
    	border-left: 10px solid #f9a825;
    }

    .select_filters.am_a_prereg, .select_filtersX.am_a_prereg{
    	border-left: 10px solid #ffc107;
    }

    .dist-offer-sold {
    	background: #ffb300;
    	color: #333 !important;
    	min-width: 22px;
    	height: 22px;
    	right: 0px;
    	border-radius: 30px;
    	text-align: center;
    	font-size: 11px;
    	line-height: 23px;
    	font-weight: 800 !important;
    	display: inline-block;
    	margin-left: 10px;
    	position: relative;
    	margin-right: -6px;
    	vertical-align: middle;
    }


/*
     ___                 ___       _   _
    | __|__ _ ____  _   / _ \ _ __| |_(_)___ _ _  ___
    | _|/ _` (_-< || | | (_) | '_ \  _| / _ \ ' \(_-<
    |___\__,_/__/\_, |  \___/| .__/\__|_\___/_||_/__/
                 |__/        |_|
                 */


                 .easy-option-buttons{
                 	text-align: center;
                 	white-space: nowrap;
                 	overflow-x: auto;
                 	-webkit-overflow-scrolling: touch;
                 }

                 .easy-option-buttons i{
                 	margin-left: 0.5em;
                 }

                 .easy-option-buttons a{
                 	margin: 0px 5px;
                 }

                 @media only screen and (max-width: 500px){
                 	.easy-option-buttons a{
                 		margin: 0px 3px;
                 	}
                 	.easy-option-buttons i{
                 		margin-left: 0em;
                 	}

                 	.easy-option-buttons .mdl-button{
                 		min-width: 20px;
                 		padding: 0px 10px;
                 	}
                 }


                 .scroll_style::-webkit-scrollbar {
                 	width: 12px;
                 }

                 .scroll_style::-webkit-scrollbar-track {
                 	background-color: #FFF;
                 	border-radius: 0px;
                 	border-left: 0px solid #ccc;
                 }

                 .scroll_style::-webkit-scrollbar-thumb {
                 	background-color: #ccc;
                 	border-radius: 0px;
                 }

                 .scroll_style::-webkit-scrollbar-thumb:hover {
                 	background-color: #aaa;
                 }

                 .scroll_style:HOVER::-webkit-scrollbar-track {
                 	background-color: #F9F9F9;
                 }

                 .scroll_style {
                 	scrollbar-base-color:#ccc;scrollbar-3dlight-color: #ccc;
                 	scrollbar-highlight-color:#aaa;scrollbar-track-color:#fff;scrollbar-arrow-color:#ccc;scrollbar-shadow-color:#ccc;}



                 	.scroll_style.dark::-webkit-scrollbar {
                 		width: 12px;
                 	}

                 	.scroll_style.dark::-webkit-scrollbar-track {
                 		background-color: #000;
                 		border-radius: 0px;
                 		border-left: 0px solid #333;
                 	}

                 	.scroll_style.dark::-webkit-scrollbar-thumb {
                 		background-color: #333;
                 		border-radius: 0px;
                 	}

                 	.scroll_style.dark::-webkit-scrollbar-thumb:hover {
                 		background-color: #999;
                 	}

                 	.scroll_style.dark:HOVER::-webkit-scrollbar-track {
                 		background-color: #111;
                 	}

                 	.scroll_style.dark {
                 		scrollbar-base-color:#333;scrollbar-3dlight-color: #333;
                 		scrollbar-highlight-color:#999;scrollbar-track-color:#000;scrollbar-arrow-color:#999;scrollbar-shadow-color:#999;}

                 		.noselect{
                 			-webkit-touch-callout: none;
                 			-webkit-user-select: none;
                 			-khtml-user-select: none;
                 			-moz-user-select: none;
                 			-ms-user-select: none;
                 			user-select: none;
                 		}

/*
     _____   _____ _  _ _____   ___ ___ ___ _____ ___ ___  _  _
    | __\ \ / / __| \| |_   _| / __| __/ __|_   _|_ _/ _ \| \| |
    | _| \ V /| _|| .` | | |   \__ \ _| (__  | |  | | (_) | .` |
    |___| \_/ |___|_|\_| |_|   |___/___\___| |_| |___\___/|_|\_|

    */

    .event-header .main_image{
    	float: left;
    	margin-right: 10px;
    	width: 10em;
    	height: 10em;
    }

    .info-holder{
    	display: table-cell;
    }

    .event_link_section{
    	position: absolute;
    	right: 10px;
    	top: 0px;
    }


    @media only screen and (max-width: 1024px){ 
    	.event-header .info-section{
    		padding-bottom: 20px;
    	}

    }

    @media only screen and (max-width: 839px){ 
    	.event-header .info-section{
    		padding-top: 0px;
    		padding-left: 0px;
    		padding-bottom: 0px;
    	}

    	.event_link_section{
    		position: relative;
    		right: initial;
    		text-align: left;
    	}

    	.event_link_button_set{
    		display: inline-block;
    		margin-right: 10px;
    	}

    }

    @media only screen and (max-width: 663px){ 
    	.event_link_section{
    		padding-bottom: 20px;
    	}
    	.event-header .main_image{
    		width: 50px;
    		height: 50px;
    	}

    	.bb77-header{
    		min-height: initial;
    	}

    	.event-header h3{
    		font-size:  20px;
    	}

    	.event-header h3 .material-icons{
    		font-size: 18px;
    	}


    	.event-header h2{
    		font-size:  26px;
    	}

    	.event-header .info-section{
    		padding-bottom: 0px;
    	}

    	.days2go{
    		position: absolute;
    		top: 6px;
    		right: 3px;
    	}

    	.days2go > span{
    		display: block;
    		margin-bottom: 5px;
    		height: 25px;
    		line-height: 27px !important;
    	}

    	.days2go  .mdl-chip__text{
    		font-size: 10px;
    		line-height: 27px !important;
    	}
    }

    .sub_sections{
    	position: absolute;
    	left: 0px;
    	bottom: -3px;
    	right: 0px;
    	white-space: nowrap;
    	overflow: hidden;
    	-webkit-overflow-scrolling: touch;
    	overflow-x: auto;
    }

    .sub_sections i{
    	position: absolute;
    	right: 10px;
    	top: 7px;
    }

    .sub_sections a{
    	position: relative;
    	width: 16.66%;
    	width: calc(100% / 6);
    	x-min-width: 150px;
    	line-height: 2em;
    	font-size: 20px;
    	text-align: center;
    	color: #FFF;
    	display: inline-block;
    	box-sizing: border-box;
    	border-top: 2px solid #FFF;
    	border-right: 2px solid #FFF;
    	white-space: nowrap;
    	overflow: hidden;
    	text-overflow: ellipsis;
    	vertical-align: top;
    	text-transform: uppercase;
    }

    .sub_sections a:last-of-type{
    	border-right:  0px;
    }


    .sub_sections a.selected{
    	background-color: #42a5f5;
    }

    .sub_sections a:HOVER{
    	background: #333 !important;
    	color: #FFF !important;
    }

    .sub_sections a:before{
    	content: '';
    	display: block;
    	position: absolute;
    	right: 0px;
    	bottom: 0px;
    	width: 0px;
    	top: 0px;
    	transition: 0.1s;
    }

    .sub_sections a span{
    	position: relative;
    	z-index: 100;
    }

    .sub_sections a.istickets:before, .sub_sections a.istickets.selected{
    	background-color: #42a5f5;
    }

    .sub_sections a.isevent:before, .sub_sections a.isevent.selected{
    	background-color: #78909C;
    }

    .sub_sections a.iscommuication:before, .sub_sections a.iscommuication.selected{
    	background-color: #43a047;
    }

    .sub_sections a.islog:before, .sub_sections a.islog.selected{
    	background-color: #FFA726;
    }

    .sub_sections a.ispayments:before, .sub_sections a.ispayments.selected{
    	background-color: #d81b60;
    }

    .sub_sections a.isanalysis:before, .sub_sections a.isanalysis.selected{
    	background-color: #8e24aa;
    }

    .sub_sections a.istickets.selected{
    	transition: 0.3s;
    }

    .sub_sections a.selected:before{
    	width: 100%;
    }

    .sub_sections a:HOVER:before{
    	width: 43px !important;
    }

    .bb77-header.withsubmenu .info-section{
    	padding-bottom: 40px;
    }

    @media only screen and (max-width: 1450px){ 
    	.sub_sections a{
    		font-size: 16px;
    		line-height: 42px;
    	}

    	.sub_sections i {
    		position: absolute;
    		right: 10px;
    		top: 11px;
    		font-size: 20px;
    	}

    	.sub_sections a:HOVER:before{
    		width: 38px !important;
    	}
    }

    @media only screen and (max-width: 1200px){ 
    	.sub_sections a{
    		font-size: 14px;
    	}
    }

    @media only screen and (max-width: 1024px){ 
    	.sub_sections a{
    		height: 60px;
    		line-height: 1.3em;
    		padding-top: 10px;
    	}
    	.sub_sections i {
    		position: absolute;
    		left: 50%;
    		margin-left: -20px;
    		width:  40px;
    		top: initial;
    		bottom: 5px;
    		font-size: 22px;
    	}

    	.sub_sections a:HOVER:before{
    		width: 20px !important;
    	}

    	.bb77-header.withsubmenu .info-section {
    		padding-bottom: 50px;
    	}

    }

    @media only screen and (max-width: 839px){ 
    	.sub_sections a{
    		font-size: 12px;
    	}
    }

    @media only screen and (max-width: 663px){ 
    	.sub_sections a span{
    		white-space: nowrap;
    		display: inline-block;
    		text-overflow: ellipsis;
    		overflow: hidden;
    		width: 90%;
    	}

    	.bb77-header.withsubmenu .info-section{
    		padding-bottom: 30px;
    	}
    }

    @media only screen and (max-width: 500px){
    	.sub_sections a span{
    		font-size: 8px;
    	}
    	.sub_sections a{
    		height: 40px;
    	}

    	.sub_sections a span {
    		font-size: 8px;
    		position: absolute;
    		bottom: 2px;
    		left: 3px;
    		width: 90%;
    	}

    	.sub_sections i{
    		bottom: 14px;
    		font-size: 18px;
    	}

    	.bb77-header.withsubmenu .info-section {
    		padding-bottom: 10px;
    	}

    }
    /*savepage-import-url=/lib/shadowbox/shadowbox.css*/ #sb-title-inner,#sb-info-inner,#sb-loading-inner,div.sb-message{font-family:"HelveticaNeue-Light","Helvetica Neue",Helvetica,Arial,sans-serif;font-weight:200;color:#fff;}
    #sb-container{position:fixed;margin:0;padding:0;top:0;left:0;z-index:99999;text-align:left;visibility:hidden;display:none;}
    #sb-overlay{position:relative;height:100%;width:100%;}
    #sb-wrapper{position:absolute;visibility:hidden;width:100px;}
    #sb-wrapper-inner{position:relative;border:1px solid #303030;overflow:hidden;height:100px;}
    #sb-body{position:relative;height:100%;}
    #sb-body-inner{position:absolute;height:100%;width:100%;}
    #sb-player.html{height:100%;overflow:auto;-webkit-overflow-scrolling: touch;}
    #sb-body img{border:none;}
    #sb-loading{position:relative;height:100%;}
    #sb-loading-inner{position:absolute;font-size:14px;line-height:24px;height:24px;top:50%;margin-top:-12px;width:100%;text-align:center;}
    #sb-loading-inner span{background:/*savepage-url=loading.gif*/url(data:image/gif;base64,R0lGODlhGAAYAPQAAAYGBv///zQ0NAkJCSMjI1JSUhsbG3Nzczo6OmVlZSsrK1lZWUJCQhEREYqKint7e0pKSpiYmAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH+GkNyZWF0ZWQgd2l0aCBhamF4bG9hZC5pbmZvACH5BAAHAAAAIf8LTkVUU0NBUEUyLjADAQAAACwAAAAAGAAYAAAFriAgjiQAQWVaDgr5POSgkoTDjFE0NoQ8iw8HQZQTDQjDn4jhSABhAAOhoTqSDg7qSUQwxEaEwwFhXHhHgzOA1xshxAnfTzotGRaHglJqkJcaVEqCgyoCBQkJBQKDDXQGDYaIioyOgYSXA36XIgYMBWRzXZoKBQUMmil0lgalLSIClgBpO0g+s26nUWddXyoEDIsACq5SsTMMDIECwUdJPw0Mzsu0qHYkw72bBmozIQAh+QQABwABACwAAAAAGAAYAAAFsCAgjiTAMGVaDgR5HKQwqKNxIKPjjFCk0KNXC6ATKSI7oAhxWIhezwhENTCQEoeGCdWIPEgzESGxEIgGBWstEW4QCGGAIJEoxGmGt5ZkgCRQQHkGd2CESoeIIwoMBQUMP4cNeQQGDYuNj4iSb5WJnmeGng0CDGaBlIQEJziHk3sABidDAHBgagButSKvAAoyuHuUYHgCkAZqebw0AgLBQyyzNKO3byNuoSS8x8OfwIchACH5BAAHAAIALAAAAAAYABgAAAW4ICCOJIAgZVoOBJkkpDKoo5EI43GMjNPSokXCINKJCI4HcCRIQEQvqIOhGhBHhUTDhGo4diOZyFAoKEQDxra2mAEgjghOpCgz3LTBIxJ5kgwMBShACREHZ1V4Kg1rS44pBAgMDAg/Sw0GBAQGDZGTlY+YmpyPpSQDiqYiDQoCliqZBqkGAgKIS5kEjQ21VwCyp76dBHiNvz+MR74AqSOdVwbQuo+abppo10ssjdkAnc0rf8vgl8YqIQAh+QQABwADACwAAAAAGAAYAAAFrCAgjiQgCGVaDgZZFCQxqKNRKGOSjMjR0qLXTyciHA7AkaLACMIAiwOC1iAxCrMToHHYjWQiA4NBEA0Q1RpWxHg4cMXxNDk4OBxNUkPAQAEXDgllKgMzQA1pSYopBgonCj9JEA8REQ8QjY+RQJOVl4ugoYssBJuMpYYjDQSliwasiQOwNakALKqsqbWvIohFm7V6rQAGP6+JQLlFg7KDQLKJrLjBKbvAor3IKiEAIfkEAAcABAAsAAAAABgAGAAABbUgII4koChlmhokw5DEoI4NQ4xFMQoJO4uuhignMiQWvxGBIQC+AJBEUyUcIRiyE6CR0CllW4HABxBURTUw4nC4FcWo5CDBRpQaCoF7VjgsyCUDYDMNZ0mHdwYEBAaGMwwHDg4HDA2KjI4qkJKUiJ6faJkiA4qAKQkRB3E0i6YpAw8RERAjA4tnBoMApCMQDhFTuySKoSKMJAq6rD4GzASiJYtgi6PUcs9Kew0xh7rNJMqIhYchACH5BAAHAAUALAAAAAAYABgAAAW0ICCOJEAQZZo2JIKQxqCOjWCMDDMqxT2LAgELkBMZCoXfyCBQiFwiRsGpku0EshNgUNAtrYPT0GQVNRBWwSKBMp98P24iISgNDAS4ipGA6JUpA2WAhDR4eWM/CAkHBwkIDYcGiTOLjY+FmZkNlCN3eUoLDmwlDW+AAwcODl5bYl8wCVYMDw5UWzBtnAANEQ8kBIM0oAAGPgcREIQnVloAChEOqARjzgAQEbczg8YkWJq8nSUhACH5BAAHAAYALAAAAAAYABgAAAWtICCOJGAYZZoOpKKQqDoORDMKwkgwtiwSBBYAJ2owGL5RgxBziQQMgkwoMkhNqAEDARPSaiMDFdDIiRSFQowMXE8Z6RdpYHWnEAWGPVkajPmARVZMPUkCBQkJBQINgwaFPoeJi4GVlQ2Qc3VJBQcLV0ptfAMJBwdcIl+FYjALQgimoGNWIhAQZA4HXSpLMQ8PIgkOSHxAQhERPw7ASTSFyCMMDqBTJL8tf3y2fCEAIfkEAAcABwAsAAAAABgAGAAABa8gII4k0DRlmg6kYZCoOg5EDBDEaAi2jLO3nEkgkMEIL4BLpBAkVy3hCTAQKGAznM0AFNFGBAbj2cA9jQixcGZAGgECBu/9HnTp+FGjjezJFAwFBQwKe2Z+KoCChHmNjVMqA21nKQwJEJRlbnUFCQlFXlpeCWcGBUACCwlrdw8RKGImBwktdyMQEQciB7oACwcIeA4RVwAODiIGvHQKERAjxyMIB5QlVSTLYLZ0sW8hACH5BAAHAAgALAAAAAAYABgAAAW0ICCOJNA0ZZoOpGGQrDoOBCoSxNgQsQzgMZyIlvOJdi+AS2SoyXrK4umWPM5wNiV0UDUIBNkdoepTfMkA7thIECiyRtUAGq8fm2O4jIBgMBA1eAZ6Knx+gHaJR4QwdCMKBxEJRggFDGgQEREPjjAMBQUKIwIRDhBDC2QNDDEKoEkDoiMHDigICGkJBS2dDA6TAAnAEAkCdQ8ORQcHTAkLcQQODLPMIgIJaCWxJMIkPIoAt3EhACH5BAAHAAkALAAAAAAYABgAAAWtICCOJNA0ZZoOpGGQrDoOBCoSxNgQsQzgMZyIlvOJdi+AS2SoyXrK4umWHM5wNiV0UN3xdLiqr+mENcWpM9TIbrsBkEck8oC0DQqBQGGIz+t3eXtob0ZTPgNrIwQJDgtGAgwCWSIMDg4HiiUIDAxFAAoODwxDBWINCEGdSTQkCQcoegADBaQ6MggHjwAFBZUFCm0HB0kJCUy9bAYHCCPGIwqmRq0jySMGmj6yRiEAIfkEAAcACgAsAAAAABgAGAAABbIgII4k0DRlmg6kYZCsOg4EKhLE2BCxDOAxnIiW84l2L4BLZKipBopW8XRLDkeCiAMyMvQAA+uON4JEIo+vqukkKQ6RhLHplVGN+LyKcXA4Dgx5DWwGDXx+gIKENnqNdzIDaiMECwcFRgQCCowiCAcHCZIlCgICVgSfCEMMnA0CXaU2YSQFoQAKUQMMqjoyAglcAAyBAAIMRUYLCUkFlybDeAYJryLNk6xGNCTQXY0juHghACH5BAAHAAsALAAAAAAYABgAAAWzICCOJNA0ZVoOAmkY5KCSSgSNBDE2hDyLjohClBMNij8RJHIQvZwEVOpIekRQJyJs5AMoHA+GMbE1lnm9EcPhOHRnhpwUl3AsknHDm5RN+v8qCAkHBwkIfw1xBAYNgoSGiIqMgJQifZUjBhAJYj95ewIJCQV7KYpzBAkLLQADCHOtOpY5PgNlAAykAEUsQ1wzCgWdCIdeArczBQVbDJ0NAqyeBb64nQAGArBTt8R8mLuyPyEAOwAAAAAAAAAAAA==) no-repeat;padding-left:34px;display:inline-block;}
    #sb-body,#sb-loading{background-color:#060606;}
    #sb-title,#sb-info{position:relative;margin:0;padding:0;overflow:hidden;}
    #sb-title,#sb-title-inner{height:26px;line-height:26px;}
    #sb-title-inner{font-size:16px;}
    #sb-info,#sb-info-inner{height:20px;line-height:20px;}
    #sb-info-inner{font-size:12px;}
    #sb-nav{float:right;height:16px;padding:2px 0;width:45%;}
    #sb-nav a{display:block;float:right;height:16px;width:16px;margin-left:3px;cursor:pointer;background-repeat:no-repeat;}
    #sb-nav-close{background-image:/*savepage-url=close.png*/url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAKFJREFUeNpi/P//PwMlgHHADWAAGYCGo4CYDYs4CMcCMTOyGLqCov8QsB2LIROgcovwGaAFxM+xGALT/A2IPfAZgM2Qybg04zIAZsir/wiAVTM+A0B4KZIBp4GYkxQDYM7+BcQfkLzDSYwB06AafkKdrYvkHQxD0DU3ommGiSMbshqfAXJAfBtHgOlC5SyRxbElZWYg/osj4WLIDXxmAggwAHTlHTWidTHeAAAAAElFTkSuQmCC);}
    #sb-nav-next{background-image:/*savepage-url=/lib/shadowbox/next.png*/url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAJpJREFUeNpi/P//PwMlgJFSAxhABkCxLhKbGHEwRuYsBeIoLIpwiWM14CcQO2ExAJs4GDOh+YgNiNcBsS6R4hgGgAA/EG8HYjlixJlwhK00EO8FYlFC4kx4IkgFiC0JieMzoBKINxEURwttGJhGhDjWaASBjUDMTIQ4VgNOAjEnlnSATRwjHVwGYjcg/o7mZ1zi1MlMFBsAEGAANUZLsB23Wh4AAAAASUVORK5CYII=);}
    #sb-nav-previous{background-image:/*savepage-url=/lib/shadowbox/previous.png*/url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAI9JREFUeNpi/P//PwMlgJFSAxhABuDBKoTE8WlOAOJphMRxafYD4j9YDMAQx6bZDIi//YeAaYTEmdCCRAWItwAxJ5HiKAaIAvF2KM1AhDiGAa5Qm9ABLnGs0dj+HxVMIyCONRDnY1OISxybAcxAvBGLAVjFcaUDTiA+iSUdYIjjS4miQFxESJxxwHMjQIABABQT4TRLh+gpAAAAAElFTkSuQmCC);}
    #sb-nav-play{background-image:/*savepage-url=/lib/shadowbox/play.png*/url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAHVJREFUeNpi/P//PwMlgJFSAxhABmDBZjjEMTAuFyyF0ilA/J0cFyz9DwEngVgCnwsIGQACD4FYlxIDQOAzEHtQYgAI/AHiUkoMgHlHmlwDjmELUGINmA/EbOSGQQW50fgNiEMIpURcEu1AbEBJUqZfbgQIMAD4AkaDGWpXMQAAAABJRU5ErkJggg==);}
    #sb-nav-pause{background-image:/*savepage-url=/lib/shadowbox/pause.png*/url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAD1JREFUeNpi/P//PwMlgHHADWBC40cB8X8krItDDKcBFLtg1ICRaQB6UhYCYkUk/jUg5sQi9n3wZCaAAAMA2eIcETbPY8gAAAAASUVORK5CYII=);}
    #sb-counter{float:left;width:45%;}
    #sb-counter a{padding:0 4px 0 0;text-decoration:none;cursor:pointer;color:#fff;}
    #sb-counter a.sb-counter-current{text-decoration:underline;}
    div.sb-message{font-size:12px;padding:10px;text-align:center;}
    div.sb-message a:link,div.sb-message a:visited{color:#fff;text-decoration:underline;}

        /*savepage-import-url=/lib/featherlight/featherlight.css*/ /**
 * Featherlight – ultra slim jQuery lightbox
 * Version 1.7.13 - http://noelboss.github.io/featherlight/
 *
 * Copyright 2018, Noël Raoul Bossart (http://www.noelboss.com)
 * MIT Licensed.
 **/

 html.with-featherlight {
 	/* disable global scrolling when featherlights are visible */
 	overflow: hidden;
 }

 .featherlight {
 	display: none;

 	/* dimensions: spanning the background from edge to edge */
 	position:fixed;
 	top: 0; right: 0; bottom: 0; left: 0;
 	z-index: 2147483647; /* z-index needs to be >= elements on the site. */

 	/* position: centering content */
 	text-align: center;

 	/* insures that the ::before pseudo element doesn't force wrap with fixed width content; */
 	white-space: nowrap;

 	/* styling */
 	cursor: pointer;
 	background: #333;
 	/* IE8 "hack" for nested featherlights */
 	background: rgba(0, 0, 0, 0);
 }

 /* support for nested featherlights. Does not work in IE8 (use JS to fix) */
 .featherlight:last-of-type {
 	background: rgba(0, 0, 0, 0.8);
 }

 .featherlight:before {
 	/* position: trick to center content vertically */
 	content: '';
 	display: inline-block;
 	height: 100%;
 	vertical-align: middle;
 }

 .featherlight .featherlight-content {
 	/* make content container for positioned elements (close button) */
 	position: relative;

 	/* position: centering vertical and horizontal */
 	text-align: left;
 	vertical-align: middle;
 	display: inline-block;

 	/* dimensions: cut off images */
 	overflow: auto;
 	padding: 25px 25px 0;
 	border-bottom: 25px solid transparent;

 	/* dimensions: handling large content */
 	margin-left: 5%;
 	margin-right: 5%;
 	max-height: 95%;

 	/* styling */
 	background: #fff;
 	cursor: auto;

 	/* reset white-space wrapping */
 	white-space: normal;
 }

 /* contains the content */
 .featherlight .featherlight-inner {
 	/* make sure its visible */
 	display: block;
 }

 /* don't show these though */
 .featherlight script.featherlight-inner,
 .featherlight link.featherlight-inner,
 .featherlight style.featherlight-inner {
 	display: none;
 }

 .featherlight .featherlight-close-icon {
 	/* position: centering vertical and horizontal */
 	position: absolute;
 	z-index: 9999;
 	top: 0;
 	right: 0;

 	/* dimensions: 25px x 25px */
 	line-height: 25px;
 	width: 25px;

 	/* styling */
 	cursor: pointer;
 	text-align: center;
 	font-family: Arial, sans-serif;
 	background: #fff; /* Set the background in case it overlaps the content */
 	background: rgba(255, 255, 255, 0.3);
 	color: #000;
 	border: none;
 	padding: 0;
 }

 /* See http://stackoverflow.com/questions/16077341/how-to-reset-all-default-styles-of-the-html5-button-element */
 .featherlight .featherlight-close-icon::-moz-focus-inner {
 	border: 0;
 	padding: 0;
 }

 .featherlight .featherlight-image {
 	/* styling */
 	width: 100%;
 }


 .featherlight-iframe .featherlight-content {
 	/* removed the border for image croping since iframe is edge to edge */
 	border-bottom: 0;
 	padding: 0;
 	-webkit-overflow-scrolling: touch;
 }

 .featherlight iframe {
 	/* styling */
 	border: none;
 }

 .featherlight * { /* See https://github.com/noelboss/featherlight/issues/42 */
 	-webkit-box-sizing: border-box;
 	-moz-box-sizing: border-box;
 	box-sizing: border-box;
 }

 /* handling phones and small screens */
 @media only screen and (max-width: 1024px) {
 	.featherlight .featherlight-content {
 		/* dimensions: maximize lightbox with for small screens */
 		margin-left: 0;
 		margin-right: 0;
 		max-height: 98%;

 		padding: 10px 10px 0;
 		border-bottom: 10px solid transparent;
 	}
 }

 /* hide non featherlight items when printing */
 @media print {
 	html.with-featherlight > * > :not(.featherlight) {
 		display: none;
 	}
 }

 /*savepage-import-url=/lib/magnific-popup/magnific-popup.css?v=2*/ /* Magnific Popup CSS */
 .mfp-bg {
 	top: 0;
 	left: 0;
 	width: 100%;
 	height: 100%;
 	z-index: 1000042;
 	overflow: hidden;
 	position: fixed !important;
 	background: #0b0b0b;
 	opacity: 0.8;
 	filter: alpha(opacity=80); }

 	.mfp-wrap {
 		top: 0;
 		left: 0;
 		width: 100%;
 		height: 100%;
 		z-index: 1000043;
 		position: fixed;
 		outline: none;
 		-webkit-backface-visibility: hidden; }

 		.mfp-container {
 			text-align: center;
 			position: absolute;
 			width: 100%;
 			height: 100%;
 			left: 0;
 			top: 0;
 			padding: 0 8px;
 			-webkit-box-sizing: border-box;
 			-moz-box-sizing: border-box;
 			box-sizing: border-box; }

 			.mfp-container:before {
 				content: '';
 				display: inline-block;
 				height: 100%;
 				vertical-align: middle; }

 				.mfp-align-top .mfp-container:before {
 					display: none; }

 					.mfp-content {
 						position: relative;
 						display: inline-block;
 						vertical-align: middle;
 						margin: 0 auto;
 						text-align: left;
 						z-index: 1000045; }

 						.mfp-inline-holder .mfp-content, .mfp-ajax-holder .mfp-content {
 							width: 100%;
 							cursor: auto; }

 							.mfp-ajax-cur {
 								cursor: progress; }

 								.mfp-zoom-out-cur, .mfp-zoom-out-cur .mfp-image-holder .mfp-close {
 									cursor: -moz-zoom-out;
 									cursor: -webkit-zoom-out;
 									cursor: zoom-out; }

 									.mfp-zoom {
 										cursor: pointer;
 										cursor: -webkit-zoom-in;
 										cursor: -moz-zoom-in;
 										cursor: zoom-in; }

 										.mfp-auto-cursor .mfp-content {
 											cursor: auto; }

 											.mfp-close, .mfp-arrow, .mfp-preloader, .mfp-counter {
 												-webkit-user-select: none;
 												-moz-user-select: none;
 												user-select: none; }

 												.mfp-loading.mfp-figure {
 													display: none; }

 													.mfp-hide {
 														display: none !important; }

 														.mfp-preloader {
 															color: #cccccc;
 															position: absolute;
 															top: 50%;
 															width: auto;
 															text-align: center;
 															margin-top: -0.8em;
 															left: 8px;
 															right: 8px;
 															z-index: 1000044; }
 															.mfp-preloader a {
 																color: #cccccc; }
 																.mfp-preloader a:hover {
 																	color: white; }

 																	.mfp-s-ready .mfp-preloader {
 																		display: none; }

 																		.mfp-s-error .mfp-content {
 																			display: none; }

 																			button.mfp-close, button.mfp-arrow {
 																				overflow: visible;
 																				cursor: pointer;
 																				background: transparent;
 																				border: 0;
 																				-webkit-appearance: none;
 																				display: block;
 																				outline: none;
 																				padding: 0;
 																				z-index: 1000046;
 																				-webkit-box-shadow: none;
 																				box-shadow: none; }
 																				button::-moz-focus-inner {
 																					padding: 0;
 																					border: 0; }

 																					.mfp-close {
 																						width: 44px;
 																						height: 44px;
 																						line-height: 44px;
 																						position: absolute;
 																						right: 0;
 																						top: 0;
 																						text-decoration: none;
 																						text-align: center;
 																						opacity: 0.65;
 																						filter: alpha(opacity=65);
 																						padding: 0 0 18px 10px;
 																						color: white;
 																						font-style: normal;
 																						font-size: 28px;
 																						font-family: Arial, Baskerville, monospace; }
 																						.mfp-close:hover, .mfp-close:focus {
 																							opacity: 1;
 																							filter: alpha(opacity=100); }
 																							.mfp-close:active {
 																								top: 1px; }

 																								.mfp-close-btn-in .mfp-close {
 																									color: #333333; }

 																									.mfp-image-holder .mfp-close, .mfp-iframe-holder .mfp-close {
 																										color: white;
 																										right: -6px;
 																										text-align: right;
 																										padding-right: 6px;
 																										width: 100%; }

 																										.mfp-counter {
 																											position: absolute;
 																											top: 0;
 																											right: 0;
 																											color: #cccccc;
 																											font-size: 12px;
 																											line-height: 18px; }

 																											.mfp-arrow {
 																												position: absolute;
 																												opacity: 0.65;
 																												filter: alpha(opacity=65);
 																												margin: 0;
 																												top: 50%;
 																												margin-top: -55px;
 																												padding: 0;
 																												width: 90px;
 																												height: 110px;
 																												-webkit-tap-highlight-color: rgba(0, 0, 0, 0); }
 																												.mfp-arrow:active {
 																													margin-top: -54px; }
 																													.mfp-arrow:hover, .mfp-arrow:focus {
 																														opacity: 1;
 																														filter: alpha(opacity=100); }
 																														.mfp-arrow:before, .mfp-arrow:after, .mfp-arrow .mfp-b, .mfp-arrow .mfp-a {
 																															content: '';
 																															display: block;
 																															width: 0;
 																															height: 0;
 																															position: absolute;
 																															left: 0;
 																															top: 0;
 																															margin-top: 35px;
 																															margin-left: 35px;
 																															border: medium inset transparent; }
 																															.mfp-arrow:after, .mfp-arrow .mfp-a {
 																																border-top-width: 13px;
 																																border-bottom-width: 13px;
 																																top: 8px; }
 																																.mfp-arrow:before, .mfp-arrow .mfp-b {
 																																	border-top-width: 21px;
 																																	border-bottom-width: 21px;
 																																	opacity: 0.7; }

 																																	.mfp-arrow-left {
 																																		left: 0; }
 																																		.mfp-arrow-left:after, .mfp-arrow-left .mfp-a {
 																																			border-right: 17px solid white;
 																																			margin-left: 31px; }
 																																			.mfp-arrow-left:before, .mfp-arrow-left .mfp-b {
 																																				margin-left: 25px;
 																																				border-right: 27px solid #3f3f3f; }

 																																				.mfp-arrow-right {
 																																					right: 0; }
 																																					.mfp-arrow-right:after, .mfp-arrow-right .mfp-a {
 																																						border-left: 17px solid white;
 																																						margin-left: 39px; }
 																																						.mfp-arrow-right:before, .mfp-arrow-right .mfp-b {
 																																							border-left: 27px solid #3f3f3f; }

 																																							.mfp-iframe-holder {
 																																								padding-top: 40px;
 																																								padding-bottom: 40px; }
 																																								.mfp-iframe-holder .mfp-content {
 																																									line-height: 0;
 																																									width: 100%;
 																																									max-width: 900px; }
 																																									.mfp-iframe-holder .mfp-close {
 																																										top: -40px; }

 																																										.mfp-iframe-scaler {
 																																											width: 100%;
 																																											height: 0;
 																																											overflow: hidden;
 																																											padding-top: 56.25%; }
 																																											.mfp-iframe-scaler iframe {
 																																												position: absolute;
 																																												display: block;
 																																												top: 0;
 																																												left: 0;
 																																												width: 100%;
 																																												height: 100%;
 																																												box-shadow: 0 0 8px rgba(0, 0, 0, 0.6);
 																																												background: black; }

 																																												/* Main image in popup */
 																																												img.mfp-img {
 																																													width: auto;
 																																													max-width: 100%;
 																																													height: auto;
 																																													display: block;
 																																													line-height: 0;
 																																													-webkit-box-sizing: border-box;
 																																													-moz-box-sizing: border-box;
 																																													box-sizing: border-box;
 																																													padding: 40px 0 40px;
 																																													margin: 0 auto; }

 																																													/* The shadow behind the image */
 																																													.mfp-figure {
 																																														line-height: 0; }
 																																														.mfp-figure:after {
 																																															content: '';
 																																															position: absolute;
 																																															left: 0;
 																																															top: 40px;
 																																															bottom: 40px;
 																																															display: block;
 																																															right: 0;
 																																															width: auto;
 																																															height: auto;
 																																															z-index: -1;
 																																															box-shadow: 0 0 8px rgba(0, 0, 0, 0.6);
 																																															background: #444444; }
 																																															.mfp-figure small {
 																																																color: #bdbdbd;
 																																																display: block;
 																																																font-size: 12px;
 																																																line-height: 14px; }
 																																																.mfp-figure figure {
 																																																	margin: 0; }

 																																																	.mfp-bottom-bar {
 																																																		margin-top: -36px;
 																																																		position: absolute;
 																																																		top: 100%;
 																																																		left: 0;
 																																																		width: 100%;
 																																																		cursor: auto; }

 																																																		.mfp-title {
 																																																			text-align: left;
 																																																			line-height: 18px;
 																																																			color: #f3f3f3;
 																																																			word-wrap: break-word;
 																																																			padding-right: 36px; }

 																																																			.mfp-image-holder .mfp-content {
 																																																				max-width: 100%; }

 																																																				.mfp-gallery .mfp-image-holder .mfp-figure {
 																																																					cursor: pointer; }


 																																																					.mfp-zoom-out-cur {
 																																																						cursor: initial;
 																																																						cursor: initial;
 																																																						cursor: initial; }

 																																																						.mfp-zoom-out-cur .mfp-image-holder .mfp-close{
 																																																							cursor: pointer;
 																																																						}

 																																																						@media screen and (max-width: 800px) and (orientation: landscape), screen and (max-height: 300px) {
  /**
       * Remove all paddings around the image on small screen
       */
       .mfp-img-mobile .mfp-image-holder {
       	padding-left: 0;
       	padding-right: 0; }
       	.mfp-img-mobile img.mfp-img {
       		padding: 0; }
       		.mfp-img-mobile .mfp-figure:after {
       			top: 0;
       			bottom: 0; }
       			.mfp-img-mobile .mfp-figure small {
       				display: inline;
       				margin-left: 5px; }
       				.mfp-img-mobile .mfp-bottom-bar {
       					background: rgba(0, 0, 0, 0.6);
       					bottom: 0;
       					margin: 0;
       					top: auto;
       					padding: 3px 5px;
       					position: fixed;
       					-webkit-box-sizing: border-box;
       					-moz-box-sizing: border-box;
       					box-sizing: border-box; }
       					.mfp-img-mobile .mfp-bottom-bar:empty {
       						padding: 0; }
       						.mfp-img-mobile .mfp-counter {
       							right: 5px;
       							top: 3px; }
       							.mfp-img-mobile .mfp-close {
       								top: 0;
       								right: 0;
       								width: 35px;
       								height: 35px;
       								line-height: 35px;
       								background: rgba(0, 0, 0, 0.6);
       								position: fixed;
       								text-align: center;
       								padding: 0; } }

       								@media all and (max-width: 900px) {
       									.mfp-arrow {
       										-webkit-transform: scale(0.75);
       										transform: scale(0.75); }
       										.mfp-arrow-left {
       											-webkit-transform-origin: 0;
       											transform-origin: 0; }
       											.mfp-arrow-right {
       												-webkit-transform-origin: 100%;
       												transform-origin: 100%; }
       												.mfp-container {
       													padding-left: 6px;
       													padding-right: 6px; } }

       													.mfp-ie7 .mfp-img {
       														padding: 0; }
       														.mfp-ie7 .mfp-bottom-bar {
       															width: 600px;
       															left: 50%;
       															margin-left: -300px;
       															margin-top: 5px;
       															padding-bottom: 5px; }
       															.mfp-ie7 .mfp-container {
       																padding: 0; }
       																.mfp-ie7 .mfp-content {
       																	padding-top: 44px; }
       																	.mfp-ie7 .mfp-close {
       																		top: 0;
       																		right: 0;
       																		padding-top: 0; }

/* 

====== Zoom effect ======

*/
.mfp-zoom-in {
	/* start state */
	/* animate in */
	/* animate out */
}
.mfp-zoom-in .mfp-with-anim {
	opacity: 0;
	transition: all 0.2s ease-in-out;
	transform: scale(0.8);
}
.mfp-zoom-in.mfp-bg {
	opacity: 0;
	transition: all 0.3s ease-out;
}
.mfp-zoom-in.mfp-ready .mfp-with-anim {
	opacity: 1;
	transform: scale(1);
}
.mfp-zoom-in.mfp-ready.mfp-bg {
	opacity: 0.8;
}
.mfp-zoom-in.mfp-removing .mfp-with-anim {
	transform: scale(0.8);
	opacity: 0;
}
.mfp-zoom-in.mfp-removing.mfp-bg {
	opacity: 0;
}



/* 

====== Move-horizontal effect ======

*/
.mfp-move-horizontal {
	/* start state */
	/* animate in */
	/* animate out */
}
.mfp-move-horizontal .mfp-with-anim  {
	opacity: 0;
	transition: all 0.3s;
	transform: translateX(-50px);
}
.mfp-move-horizontal.mfp-bg {
	opacity: 0;
	transition: all 0.3s;
}
.mfp-move-horizontal.mfp-ready.mfp-image-loaded .mfp-with-anim {
	opacity: 1;
	transform: translateX(0);
}

.mfp-move-horizontal.mfp-ready.mfp-bg {
	opacity: 0.8;
}
.mfp-move-horizontal.mfp-removing .mfp-with-anim {
	transform: translateX(50px) !important;
	opacity: 0 !important;
}
.mfp-move-horizontal.mfp-removing.mfp-bg {
	opacity: 0;
}

/* 

====== Move-from-top effect ======

*/
.mfp-move-from-top {
	/* start state */
	/* animate in */
	/* animate out */
}
.mfp-move-from-top .mfp-content {
	vertical-align: top;
}
.mfp-move-from-top .mfp-with-anim {
	opacity: 0;
	transition: all 0.2s;
	transform: translateY(-100px);
}
.mfp-move-from-top.mfp-bg {
	opacity: 0;
	transition: all 0.2s;
}
.mfp-move-from-top.mfp-ready .mfp-with-anim {
	opacity: 1;
	transform: translateY(0);
}
.mfp-move-from-top.mfp-ready.mfp-bg {
	opacity: 0.8;
}
.mfp-move-from-top.mfp-removing .mfp-with-anim {
	transform: translateY(-50px);
	opacity: 0;
}
.mfp-move-from-top.mfp-removing.mfp-bg {
	opacity: 0;
}

/* 

====== 3d unfold ======

*/
.mfp-3d-unfold {
	/* start state */
	/* animate in */
	/* animate out */
}
.mfp-3d-unfold .mfp-content {
	perspective: 2000px;
}
.mfp-3d-unfold .mfp-with-anim {
	opacity: 0;
	transition: all 0.3s ease-in-out;
	transform-style: preserve-3d;
	transform: rotateY(-60deg);
}
.mfp-3d-unfold.mfp-bg {
	opacity: 0;
	transition: all 0.5s;
}
.mfp-3d-unfold.mfp-ready .mfp-with-anim {
	opacity: 1;
	transform: rotateY(0deg);
}
.mfp-3d-unfold.mfp-ready.mfp-bg {
	opacity: 0.8;
}
.mfp-3d-unfold.mfp-removing .mfp-with-anim {
	transform: rotateY(60deg);
	opacity: 0;
}
.mfp-3d-unfold.mfp-removing.mfp-bg {
	opacity: 0;
}

/* 

====== Zoom-out effect ======

*/
.mfp-zoom-out {
	/* start state */
	/* animate in */
	/* animate out */
}
.mfp-zoom-out .mfp-with-anim {
	opacity: 0;
	transition: all 0.3s ease-in-out;
	transform: scale(1.3);
}
.mfp-zoom-out.mfp-bg {
	opacity: 0;
	transition: all 0.3s ease-out;
}
.mfp-zoom-out.mfp-ready .mfp-with-anim {
	opacity: 1;
	transform: scale(1);
}
.mfp-zoom-out.mfp-ready.mfp-bg {
	opacity: 0.8;
}
.mfp-zoom-out.mfp-removing .mfp-with-anim {
	transform: scale(1.3);
	opacity: 0;
}
.mfp-zoom-out.mfp-removing.mfp-bg {
	opacity: 0;
}


.mfp-preloader {
	width: 30px;
	height: 30px;
	background-color: #FFF;
	opacity: 0.65;
	margin: 0 auto;
	-webkit-animation: rotateplane 1.2s infinite ease-in-out;
	-moz-animation: rotateplane 1.2s infinite ease-in-out;
	animation: rotateplane 1.2s infinite ease-in-out;
}

@-webkit-keyframes rotateplane {
	0% { -webkit-transform: perspective(120px); }
	50% { -webkit-transform: perspective(120px) rotateY(180deg); }
	100% { -webkit-transform: perspective(120px) rotateY(180deg)  rotateX(180deg); }
}

@keyframes rotateplane {
	0% { transform: perspective(120px) rotateX(0deg) rotateY(0deg) }
	50% { transform: perspective(120px) rotateX(-180.1deg) rotateY(0deg) }
	100% { transform: perspective(120px) rotateX(-180deg) rotateY(-179.9deg) }
}

</style>
<!-- <script src="/js/prefixfree.min.js"></script> -->


<!-- SERVER: zaiko.io 172.16.5.99-->



<script id="savepage-pageloader" type="application/javascript">
	savepage_PageLoader(5);
	"use strict";function savepage_PageLoader(maxframedepth){var resourceMimeType=new Array();var resourceBase64Data=new Array();var resourceBlobUrl=new Array();window.addEventListener("DOMContentLoaded",function(a){createBlobURLs();replaceReferences(0,document.documentElement)},false);function createBlobURLs(){var i,j,binaryString,blobData;var a=new Array();for(i=0;i<resourceMimeType.length;i++){if(typeof resourceMimeType[i]!="undefined"){binaryString=atob(resourceBase64Data[i]);resourceBase64Data[i]="";a.length=0;for(j=0;j<binaryString.length;j++){a[j]=binaryString.charCodeAt(j)}blobData=new Blob([new Uint8Array(a)],{type:resourceMimeType[i]});resourceMimeType[i]="";resourceBlobUrl[i]=window.URL.createObjectURL(blobData)}}}function replaceReferences(a,b){var i,regex1,regex2,csstext,blobData;regex1=/url\(\s*((?:"[^"]+")|(?:'[^']+')|(?:[^\s)]+))\s*\)/gi;regex2=/data:[^;]*;resource=(\d+);base64,/i;if(b.hasAttribute("style")){csstext=b.style.cssText;b.style.cssText=csstext.replace(regex1,replaceCSSRef)}if(b.localName=="style"){csstext=b.textContent;b.textContent=csstext.replace(regex1,replaceCSSRef)}else if(b.localName=="link"&&(b.rel.toLowerCase()=="icon"||b.rel.toLowerCase()=="shortcut icon")){if(b.href!="")b.href=b.href.replace(regex2,replaceRef)}else if(b.localName=="body"){if(b.background!="")b.background=b.background.replace(regex2,replaceRef)}else if(b.localName=="img"){if(b.src!="")b.src=b.src.replace(regex2,replaceRef)}else if(b.localName=="input"&&b.type.toLowerCase()=="image"){if(b.src!="")b.src=b.src.replace(regex2,replaceRef)}else if(b.localName=="audio"){if(b.src!=""){b.src=b.src.replace(regex2,replaceRef);b.load()}}else if(b.localName=="video"){if(b.src!=""){b.src=b.src.replace(regex2,replaceRef);b.load()}if(b.poster!="")b.poster=b.poster.replace(regex2,replaceRef)}else if(b.localName=="source"){if(b.src!=""){b.src=b.src.replace(regex2,replaceRef);b.parentElement.load()}}else if(b.localName=="track"){if(b.src!="")b.src=b.src.replace(regex2,replaceRef)}else if(b.localName=="object"){if(b.data!="")b.data=b.data.replace(regex2,replaceRef)}else if(b.localName=="embed"){if(b.src!="")b.src=b.src.replace(regex2,replaceRef)}if(b.localName=="iframe"||b.localName=="frame"){if(a<maxframedepth){if(b.hasAttribute("data-savepage-sameorigin")){blobData=new Blob([decodeURIComponent(b.src.substr(29))],{type:"text/html;charset=utf-8"});b.onload=function(){try{if(b.contentDocument.documentElement!=null){replaceReferences(a+1,b.contentDocument.documentElement)}}catch(e){}};b.src=window.URL.createObjectURL(blobData)}}}else{for(i=0;i<b.childNodes.length;i++){if(b.childNodes[i]!=null){if(b.childNodes[i].nodeType==1)replaceReferences(a,b.childNodes[i])}}}}function replaceCSSRef(a,b,c,d){var e=new Array();e=b.match(/data:[^;]*;resource=(\d+);base64,/i);if(e!=null)return"url("+resourceBlobUrl[+e[1]]+")";else return a}function replaceRef(a,b,c,d){return resourceBlobUrl[+b]}
}
</script>
<meta name="savepage-url" content="https://zaiko.io/my/settings.php">
<meta name="savepage-title" content="ZAIKO / Settings">
<meta name="savepage-date" content="Wed May 08 2019 10:08:08 GMT+0900 (Japan Standard Time)">
<meta name="savepage-state" content="Standard Items; Used page loader; Retained cross-origin frames; Removed unsaved URLs; Max frame depth = 5; Max resource size = 50MB; Max resource time = 10s;">
<meta name="savepage-version" content="14.0">
<meta name="savepage-comments" content=""></head>
<body id="_body" class="lang_en   no_sitenav lb_set">
	<div class="mdl-layout__container"><div class="mdl-js-layout is-upgraded" data-upgraded=",MaterialLayout">


		<div id="hd" class="colors__p">
			<div id="header">
				<div id="globalnav_button" class="imset"></div>
				<div id="globalnav-holder">
					<ul id="globalnav">
						<li id="user_holder" class="colors__p-dark--hover">
							<a href="/my/settings.php" id="mystatus" class="status_online">Timothy Lee</a>
						</li><li id="facebookuser_holder" class="colors__facebook colors__p-dark--hover">

							<a href="/includes/facebook_check.php" id="facebook_link_option" popout="scrollbars=1,height=600,width=900" class="pset">
								Link <span>facebook</span>
							</a>
						</li><li id="youadmin_holder" class="colors__p-dark--hover">
							<a href="/admin/" class="youradmin">
								BUSINESS <i class="material-icons">store</i>
							</a>
						</li>            <li class="lang_holder ">
							<strong class="colors__s-dark--text">ENGLISH</strong>
						</li>
						<li class="lang_holder colors__p-dark--hover">
							<a href="?lang=ja">日本語</a>
						</li>
						<li class="lang_holder colors__p-dark--hover">
							<a href="?lang=zh-hant">繁體中文</a>
						</li>
						<li class="lang_holder colors__p-dark--hover">
							<a href="?lang=pt">PORTUGUÊS</a>
						</li>
						<li id="signout_holder" class="colors__s--hover"><a href="?logout=true">Sign Out</a></li>
					</ul>
				</div>
				<div id="banner">
					<div id="navigation_button" class="imset"></div>
					<div id="navigation">
						<h1 id="site_title">
							<a href="/my/">
							ZAIKO            </a>
						</h1>
					</div>
				</div>
			</div>
		</div>



		<div id="pg">


			<div id="ttlbar" class="colors__s">

				<!-- general site option  -->


			</div>

			<div id="bd">
				<div id="yui-main">







					<div id="content" class="content_fullpage"> 


						<section class="super__layout__normal fill__cards fill__tables">
							<div class="mdl-shadow--2dp bb77-header relative clear">
								<div class="p-20 info-section">
									<h3 class="mdl-color-text--white"><i class="material-icons">account_box</i> Account</h3>
									<h4 class="mdl-color-text--grey-100">Manage your account settings</h4>
								</div>
							</div>

							<style>

								body:not(.offtop) #pg{
									padding-top: 0px;
								}

								.bb77-header{
									padding-top: 80px;
									background-image: linear-gradient(to right bottom, #fc5c7e, #f359a3, #db62c7, #b072e6, #6a82fc);
								}

								.ticket_unit{
									display: inline-block;
									width: 24%;
									background: #DDD;
									margin:  0.5%;
									overflow:  hidden;
									vertical-align: top;
									box-sizing: border-box;
									position: relative;
								}

								.ticket_unit a {
									display: block;
									box-sizing: border-box;
									width: 100%;
									overflow: hidden;
								}

								.ticket_top{
									width: 100%;
									height: 0px;
									padding-bottom: 100%;
									background: #DDD;
								}

								.ticket_top:before {
									background: rgba(0,0,0,0.3);
									position: absolute;
									top: 0px;
									bottom: 0px;
									right: 0px;
									left: 0px;
									content: '';
								}

								.ticket_base{
									position:relative;
									height: 80px;
								}

								.ticket_type_ticket .ticket_base{
									border-top:  4px solid #42a5f5;
								}
								.ticket_type_guestlist .ticket_base{
									border-top:  4px solid #F50057;
								}

								.ticket_type_transfer_ticket .ticket_base{
									border-top:  4px solid #999;
								}

								.ticket_type_guest_ticket .ticket_base{
									border-top:  4px solid #BBB;
								}

								.ticket_list{
									padding:  10px;
								}

								#hd{
									background: transparent !important;
									border-bottom: transparent !important;
								}

								.offtop_s #hd{
									background: rgba(0,0,0,0.2) !important;
									transition: 0.3s;
								}


								.ticket_unit a{
									width: 100%;
									position: relative;
									color: #111;
									background: #DDD;
								}

								.ticket_unit a h2{
									position: absolute;
									line-height: 30px;
									font-size: 30px;
									left: 10px;
									right: 0px;
									text-transform: uppercase;
									font-weight: 300;
									white-space: nowrap;
									text-overflow: ellipsis;
									overflow: hidden;
								}

								.ticket_unit a h2 strong{
									font-size: 18px;
									white-space: nowrap;
									text-overflow: ellipsis;
									font-weight: 500;
								}



								.event_info{
									position: relative;
									padding: 10px;
									display: block;
									color: #FFF;
									background: rgba(0,0,0,0.3);
									x-border-bottom: 3px solid #FFF;
									box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);
									text-shadow: 1px 1px rgba(0,0,0,0.4);

								}

								.event_info i{
									position: absolute;
									right: 10px;
									bottom: 3px;
									font-size: 12px;
								}

								.event_info span{
									white-space: nowrap;
									overflow:  hidden;
									text-overflow: ellipsis;
									height: 1em;
									width:  100%;
									display: block;
									font-size: 16px;
									color: #FFF;    
								}

								.event_info span{
									display: block;
								}

								.people{
									position: absolute;
									bottom: 0px;
									font-style: italic;
									font-size:  16px;
									font-weight: 700;
									background: rgba(0,0,0,0.2);
									padding: 4px 1em;
									border-radius: 3px;
									bottom: 8px;
									left: 8px;
								}

								.ticket_base.dark{
									color: #FFF;
								}

								.ticket_base.light{
									color: #000;
								}

								.ticket_brand.light{
									color:  #000;
								}

								.ticket_brand.dark{
									color:  #FFF;
								}

								.ticket_brand{
									padding: 5px;
									position: absolute;
									bottom: 0px;
									right: 0px;
									font-size: 20px;
								}

								.ticket_brand img{
									height: 40px;
								}

								.diff_ticket_status_holder{
									position: absolute;
									top: 0px;
									bottom: 0px;
									left: 0px;
									right: 0px;
									text-align: center;
									color: #FFF;
									background:  rgba(0,0,0,0.7);

									text-align: center;
									vertical-align: middle;
								}

								.diff_ticket_status{
									position: relative;
									top: 50%;
									font-size: 30px;
									font-weight: 700;
									line-height: 30px;
									margin-top: -15px;
								}

								.lightbox_mode .bb77-header{
									padding: 0px !important;
									min-height: initial;
								}

								.lightbox_mode .bb77-header h4{
									display: none;
								}

								.lightbox_mode .my_sections{
									display: none;
								}

								.lightbox_mode  .bb77-header .info-section{
									padding-bottom: 20px;
								}

								@media only screen and (max-width: 900px){

									.ticket_unit{
										width: 49%;
									}

								}

								@media only screen and (max-width: 600px){


									.ticket_unit{
										width: 100%;
										margin: 0px 0px 20px 0px;
									}

								}

								@media only screen and (max-width: 979px){
									#pg{
										margin-top: 0px;
									}
									#hd{
										box-shadow: none;
									}
								}

								.offtop #pg{
									margin-top: 0px !important;
								}


								.t-right.easy-option-buttons{
									text-align: right;
								}

								.ticket_unit:HOVER .ticket_top{
									background-size: 110% 110% !important;
								}

								.hasimg{
									padding-right: 10px;
								}

								.hasimg img{
									border-radius: 100px;
									width: 25px;
									height: 25px;
									margin-left: 5px;
									position: relative;
									top: -1px;
								}
							</style>


							<div class="easy-option-buttons p-10 t-right my_sections">
								<a class="mdl-button mdl-js-button mdl-typography--text-uppercase mdl-color-text--grey-500" href="/my/?show=upcoming" data-upgraded=",MaterialButton">
									Upcoming           <i class="material-icons">event_available</i>
								</a><a class="mdl-button mdl-js-button mdl-typography--text-uppercase mdl-color-text--grey-500" href="/my/?show=past" data-upgraded=",MaterialButton">
									Past           <i class="material-icons">av_timer</i>
								</a><a class="mdl-button mdl-js-button mdl-typography--text-uppercase mdl-color-text--grey-900" href="/my/settings.php" data-upgraded=",MaterialButton">
									Settings           <i class="material-icons">settings</i>
								</a>
							</div>

							<link type="text/css" rel="stylesheet" href="/css/vone/style.css">
							


							<?php include('blocks/settings-details.php') ?>




						</div>

						<div style="clear:both;"></div>

						<div id="ft" class="colors__grey-800">

							<div id="footernavigation">
								<ul>
									<li class="first"><a href="/" rel="">Home</a>                </li>
									<li class=""><a href="/support/" rel="">Support</a>                </li>
									<li class=""><a href="/about/" rel="">About Us</a>                </li>
									<li class=""><a href="/company/" rel="">Our Company</a>                </li>
									<li class=""><a href="https://zaiko.applytojob.com/" rel="">Careers</a>                </li>
									<li class=""><a href="/terms/" rel="">Terms</a>                </li>
									<li>© ZAIKO PTE Ltd. / ZAIKO株式会社 All Rights Reserved.</li>
								</ul>
								<div class="clear">
								</div>


							</div>
						</div>


						<div id="debug"></div>








						<div id="browsersupport" style="display: none;">
							<div id="nosupport" style="display: none;">
								<a href="http://www.google.com/chrome">
									<p>
										<strong>SORRY,</strong> this system is not compatible with your browser. We suggest you use one of the following browsers (in order of recommendation)    </p>
									</a><a href="http://www.google.com/chrome"><strong>Chrome</strong></a>, Apple Safari, Firefox, Microsoft Edge
								</div>
							</div>

							<script></script>


						</div></div>




						<script async="" data-savepage-src="https://www.googletagmanager.com/gtag/js?id=UA-122745985-2" src=""></script>
						<script></script>

						<script type="text/javascript"></script>

						<!--LOADTIME:0.4681990146637-->
						<div id="sb-container"><div id="sb-overlay"></div><div id="sb-wrapper"><div id="sb-title"><div id="sb-title-inner"></div></div><div id="sb-wrapper-inner"><div id="sb-body"><div id="sb-body-inner"></div><div id="sb-loading"><div id="sb-loading-inner"><span>loading</span></div></div></div></div><div id="sb-info"><div id="sb-info-inner"><div id="sb-counter"></div><div id="sb-nav"><a id="sb-nav-close" title="Close" onclick="Shadowbox.close()"></a><a id="sb-nav-next" title="Next" onclick="Shadowbox.next()"></a><a id="sb-nav-play" title="Play" onclick="Shadowbox.play()"></a><a id="sb-nav-pause" title="Pause" onclick="Shadowbox.pause()"></a><a id="sb-nav-previous" title="Previous" onclick="Shadowbox.previous()"></a></div></div></div></div></div><iframe data-savepage-src="https://x-api.zaiko.io/v1.0/../login/fb_check.php" src="" width="1" height="1" style="width:1px; height: 1px;position: fixed; bottom: 0px;border: 0px;opacity: 0;" name="savepage-frame-335526241"></iframe></body></html>