<!DOCTYPE html>
<html lang="en">
<head>
  <title>ZAIKO - PRICING</title>
  <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
  <link rel="icon" href="./img/favicon.ico">
  <link type="text/css" rel="stylesheet" href="css/vone/style.css">
</head>
<body class="zk">
    <div class="zk-table text-center">
      <h3>ユーザー手数料比較 / User Fees Comparison</h3>
        <table class="table table-hover">
            <thead>
              <tr class="active">
                  <th></th>
                  <th><div class="heading-v">E社</div class="heading-v"></th>
                      <th><div class="heading-v">P社</div class="heading-v"></th>
                          <th><div class="heading-v">L社</div class="heading-v"></th>
                              <th><div class="heading-v">ZAIKO</div class="heading-v"></th>
                              </tr>
                          </thead>
                          <tbody>
                            <tr>
                                <td colspan="5" class="heading" style="background: #28a745">発券手数料 / Ticket Distribution Fee</td>
                            </tr>
                            <tr>
                                <td class="heading-h">eチケット / e-Ticket</td>
                                <td>&yen;216</td>
                                <td>&yen;216</td>
                                <td>n/a</td>
                                <td class="zk-cell">&yen;0</td>
                            </tr>
                            <tr>
                                <td class="heading-h">紙チケ（コンビニ）/ Printed in Convenience Store</td>
                                <td>&yen;324</td>
                                <td>&yen;324</td>
                                <td>&yen;108</td>
                                <td class="zk-cell">e-Ticket Only</td>
                            </tr>
                            <tr>
                                <td class="heading-h">紙チケ配送 / Ticket Delivered (Paper)</td>
                                <td>&yen;1080</td>
                                <td>&yen;972</td>
                                <td>&yen;756</td>
                                <td class="zk-cell">e-Ticket Only</td>
                            </tr>
                            <!-- Next Section -->
                            <tr>
                               <td colspan="5" class="heading" style="background: #1dc9b7">購入手数料 / Purchase Fee</td>
                           </tr>
                           <tr>
                            <td class="heading-h">クレジットカード / Credit Card</td>
                            <td>&yen;0</td>
                            <td>&yen;0</td>
                            <td>&yen;216</td>
                            <td class="zk-cell">&yen;230<span class="asterisk">*</span></td>
                        </tr>

                        <tr>
                            <td class="heading-h">コンビニ / Convenience Store</td>
                            <td>&yen;216</td>
                            <td>&yen;216</td>
                            <td>&yen;216</td>
                            <td class="zk-cell">&yen;230+&yen;300</td>
                        </tr>
                        <!-- Next Section -->
                        <tr>
                           <td colspan="5" class="heading" style="background: #007bff">その他 / Others</td>
                       </tr>
                       <tr>
                        <td class="heading-h">先行販売手数料 / Pre-sale Fee</td>
                        <td>&yen;0</td>
                        <td>&yen;0</td>
                        <td>&yen;540</td>
                        <td class="zk-cell">&yen;0</td>
                    </tr>
                    <tr>
                        <td colspan="5"></td>
                    </tr>
                    <!-- Next Section -->
                    <tr class="cta">
                        <td class="heading-h">平均手数料 / Average Fee</td>
                        <td>&yen;684</td>
                        <td>&yen;647</td>
                        <td>&yen;1188</td>
                        <td class="zk-cell">&yen;380</td>
                    </tr>
                    <tr>
                        <td colspan="5"></td>
                    </tr>
                    <!-- Next Section -->
                    <tr>
                        <td class="heading-h">英語対応手数料 / Special Sale Fee (For English)</td>
                        <td>approx. 10%</td>
                        <td>&yen;216</td>
                        <td>Japanese Only</td>
                        <td class="zk-cell">&yen;0</td>
                    </tr>
                </tbody>
            </table>
            <div class='text-left'>
                Ex. Ticket Price: &yen;3,000 <br>  
                <span class="asterisk">* With Exceptions</span><br>
                <div class="text-right">Last Updated 04/2019</div>
            </div>
        </div>
    </div>

</body>
</html>