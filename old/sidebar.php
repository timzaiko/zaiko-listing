<div id="faq-groups">

    <div class="question"> 
        <a class="heading collapsed" data-toggle="collapse" data-target="#faq-group-1" aria-expanded="false" aria-controls="faq-group-1">Group Ticketing<i class="fas fa-chevron-right"></i></a>
        <div id="faq-group-1" class="collapse list-group">
            <a class="list-group-item list-group-item-action" href="#faq-1-1">What is a Group Ticket?</a>
            <a class="list-group-item list-group-item-action" href="#faq-1-2">I bought a Group Ticket, but can my guests and I enter the event separately?</a>
            <a class="list-group-item list-group-item-action" href="#faq-1-3">If I cannot attend the event, how can my guests enter the show?</a>
            <a class="list-group-item list-group-item-action" href="#faq-1-4">Can I add more guests to my Group Ticket?</a>
            <a class="list-group-item list-group-item-action" href="#faq-1-5">My group is more than the maximum number of guests I can sign up for, what can I do?</a>
        </div>
    </div>

    <div class="question">
        <a class="heading collapsed" data-toggle="collapse" data-target="#faq-group-2" aria-expanded="false" aria-controls="faq-group-2">Purchase<i class="fas fa-chevron-right"></i></a>
        <div id="faq-group-2" class="collapse list-group">
            <a class="list-group-item list-group-item-action" href="index-2.php#faq-2-1">How can I buy tickets on ZAIKO?</a>
            <a class="list-group-item list-group-item-action" href="index-2.php#faq-2-2">I am not receiving SMS verification code. What should I do?</a>
            <a class="list-group-item list-group-item-action" href="index-2.php#faq-2-3">Where can I find the seat number of my reserved seat?</a>
        </div>
    </div>


    <div class="question">
        <a class="heading collapsed" data-toggle="collapse" data-target="#faq-group-3" aria-expanded="false" aria-controls="faq-group-3">Payment<i class="fas fa-chevron-right"></i></a>
        <div id="faq-group-3" class="collapse list-group">
            <a class="list-group-item list-group-item-action" href="#faq-3-1">What payment methods do you accept?</a>
            <a class="list-group-item list-group-item-action" href="#faq-3-2">Where can I find the ticket payment record on my credit card statement?</a>
            <a class="list-group-item list-group-item-action" href="#faq-3-3">Where can I find the payment receipt?</a>
            <a class="list-group-item list-group-item-action" href="#faq-3-4">Does ZAIKO store my credit card information?</a>
            <a class="list-group-item list-group-item-action" href="#faq-3-5">For some reason I cannot buy a ticket on ZAIKO, what should I do?</a>
            <a class="list-group-item list-group-item-action" href="#faq-3-6">I received a 1 JPY (or similar) charge to my credit card even though I did not buy any tickets recently. What should I do?</a>
        </div>
    </div>

    <div class="question">
        <a class="heading collapsed" data-toggle="collapse" data-target="#faq-group-4" aria-expanded="false" aria-controls="faq-group-4">Confirmation<i class="fas fa-chevron-right"></i></a>
        <div id="faq-group-4" class="collapse list-group">
            <a class="list-group-item list-group-item-action" href="#faq-4-1">Where can I find my tickets on ZAIKO?</a>
            <a class="list-group-item list-group-item-action" href="#faq-4-2">I cannot find my ticket on ZAIKO, what should I do?</a>
        </div>
    </div>

    <div class="question">
        <a class="heading collapsed" data-toggle="collapse" data-target="#faq-group-5" aria-expanded="false" aria-controls="faq-group-5">Cancellation<i class="fas fa-chevron-right"></i></a>
        <div id="faq-group-5" class="collapse list-group">
            <a class="list-group-item list-group-item-action" href="#faq-5-1">Is it possible to cancel my ticket and receive a refund?</a>
            <a class="list-group-item list-group-item-action" href="#faq-5-2">My event was cancelled. How can I receive refund for my ticket?</a>
        </div>
    </div>

    <div class="question">
        <a class="heading collapsed" data-toggle="collapse" data-target="#faq-group-6" aria-expanded="false" aria-controls="faq-group-6">Modification<i class="fas fa-chevron-right"></i></a>
        <div id="faq-group-6" class="collapse list-group">
            <a class="list-group-item list-group-item-action" href="#faq-6-1">I have mistakenly registered my first and last name in the wrong order. How can I change that?</a>
            <a class="list-group-item list-group-item-action" href="#faq-6-2">How can I assign my ticket to another person?</a>
        </div>
    </div>

    <div class="question">
        <a class="heading collapsed" data-toggle="collapse" data-target="#faq-group-7" aria-expanded="false" aria-controls="faq-group-7">Account<i class="fas fa-chevron-right"></i></a>
        <div id="faq-group-7" class="collapse list-group">
            <a class="list-group-item list-group-item-action" href="#faq-7-1">I forgot my password, what should I do?</a>
            <a class="list-group-item list-group-item-action" href="#faq-7-2">How can I change the email address on my account?</a>
            <a class="list-group-item list-group-item-action" href="#faq-7-3">How do I change my password?</a>
        </div>
    </div>

    <div class="question">
        <a class="heading collapsed" data-toggle="collapse" data-target="#faq-group-8" aria-expanded="false" aria-controls="faq-group-8">Others<i class="fas fa-chevron-right"></i></a>
        <div id="faq-group-8" class="collapse list-group">
            <a class="list-group-item list-group-item-action" href="#faq-8-1">I lost something at the event. Who should I contact?</a>
        </div>
    </div>

</div>