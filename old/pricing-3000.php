<!DOCTYPE html>
<html lang="en">
<head>
  <title>ZAIKO - PRICING</title>
  <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
  <link rel="icon" href="./img/favicon.ico">
  <link type="text/css" rel="stylesheet" href="css/vone/style.css">
</head>
<body class="zk" style="background:rgb(243,243,243)">
    <div class="zk-table text-center">
      <h3>ユーザー手数料比較</h3>
      <table class="table table-hover">
        <thead>
          <tr class="active">
              <th></th>
              <th><div class="heading-v">e+ / イープラス</div class="heading-v"></th>
                  <th><div class="heading-v">PIA / ぴあ</div class="heading-v"></th>
                    <th><div class="heading-v">Lawson / ローソン</div class="heading-v"></th>
                              <th><div class="heading-v">ZAIKO</div class="heading-v"></th>
                              </tr>
                          </thead>
                          <tbody>
                            <tr>
                                <td colspan="5" class="heading" style="background: #28a745; color: #fff">発券手数料</td>
                            </tr>
                            <tr>
                                <td class="heading-h">eチケット</td>
                                <td>&yen;216</td>
                                <td>&yen;216</td>
                                <td>n/a</td>
                                <td class="zk-cell">&yen;0</td>
                            </tr>
                            <tr>
                                <td class="heading-h">紙チケ（コンビニ）</td>
                                <td>&yen;324</td>
                                <td>&yen;324</td>
                                <td>&yen;108</td>
                                <td class="zk-cell">e-Ticketのみ</td>
                            </tr>
                            <tr>
                                <td class="heading-h">紙チケ配送</td>
                                <td>&yen;1080</td>
                                <td>&yen;972</td>
                                <td>&yen;n/a</td>
                                <td class="zk-cell">e-Ticketのみ</td>
                            </tr>
                            <!-- Next Section -->
                            <tr>
                               <td colspan="5" class="heading" style="background: #1dc9b7; color: #fff">購入手数料</td>
                           </tr>
                           <tr>
                            <td class="heading-h">クレジットカード</td>
                            <td>&yen;0</td>
                            <td>&yen;0</td>
                            <td>&yen;216</td>
                            <td class="zk-cell">&yen;230*</span></td>
                        </tr>

                        <tr>
                            <td class="heading-h">コンビニ</td>
                            <td>&yen;216</td>
                            <td>&yen;216</td>
                            <td>&yen;216</td>
                            <td class="zk-cell">&yen;230+300</td>
                        </tr>
                        <!-- Next Section -->
                        <tr>
                           <td colspan="5" class="heading" style="background: #007bff; color: #fff">その他</td>
                       </tr>
                       <tr>
                        <td class="heading-h">先行販売手数料</td>
                        <td>&yen;0</td>
                        <td>&yen;0</td>
                        <td>&yen;540</td>
                        <td class="zk-cell">&yen;0</td>
                    </tr>
                    <tr>
                        <td colspan="5"></td>
                    </tr>
                    <!-- Next Section -->
                    <tr class="cta">
                        <td class="heading-h">平均手数料</td>
                        <td>&yen;684</td>
                        <td>&yen;647</td>
                        <td>&yen;1188</td>
                        <td class="zk-cell">&yen;380</td>
                    </tr>
                    <tr>
                        <td colspan="5"></td>
                    </tr>
                    <!-- Next Section -->
                    <tr>
                        <td class="heading-h">英語対応手数料</td>
                        <td>10%</td>
                        <td>&yen;216</td>
                        <td>日本語のみ</td>
                        <td class="zk-cell">&yen;0</td>
                    </tr>
                </tbody>
            </table>
            <div class='text-left'>
                チケット代 &yen;3,000の場合<br>  
                <!-- 他社平均値: &yen;864<br> -->
                 *一部除く
            </div>
        </div>
    </div>


    <style>

        .cta {
            border: 5px solid red;
            font-size: 1.5rem;
        }
        .zk-table {
            padding: 10rem;
        }
        .zk-cell {
            background: #abdcc1;
        }
        .table {
            border: 1px solid #dee2e6;
            -webkit-box-shadow: 0 2px 8px 0 rgba(0, 0, 0, 0.05);
            box-shadow: 0 2px 8px 0 rgba(0, 0, 0, 0.05);

        }
        .zk-cell {
            font-weight: bold;
        }
    </style>
</body>
</html>