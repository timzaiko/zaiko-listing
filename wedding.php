<!DOCTYPE html>
<html>
<head>
	<title>Mayumi & Tim - March 29, 2020</title>
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
	<link href="https://fonts.googleapis.com/css?family=Raleway:300,400|Noto+Serif+JP:300,400,500,700&display=swap" rel="stylesheet">
</head>
<body>

	<style>

		html {
			scroll-behavior: smooth;
		}

		html, body {
			overflow-x: hidden;
		}

		body {
			font-family: 'Noto Serif JP','Raleway', sans-serif;
			font-size: 16px;
		}

		p, li {
			font-weight: 300;
		}

		a {
			color: black;
		}

		h1, h2, h3, h4, h5, h6 {
			font-family: 'Noto Serif JP','Raleway', sans-serif;
			font-weight: 300;
		}

		.btn {
			border-radius: 50px;
			padding: 1em 2em;
			font-weight: bold;
			background: black;
		}

		.box {
			background: white;
			color: black;
			padding: 10px 15px;
			display: inline-block;
		}

		.full-h {
			background: linear-gradient(to left, #ffffff, #ffffff, #afafaf);;
			position: absolute;
			width: 100%;
		}

		.full-h > div {
			z-index: 99;
			position: relative;
		}

		a:hover {
			text-decoration: none;
		}

		ul {
			list-style: none;
			
		}

		ul > li {
			margin-bottom: 20px; 
			line-height: 1.5;
		}

		.mock img {
			display: block;
			margin: 0 auto;
		}

		.parallax {
			height: 300px;
			background-size: cover!important;
		}

		.mock .bg-leaf-1 {
			background: url(/wedding/leaf-1.png);
			background-size: cover;
			width: 400px;
			height: 253px;
			position: absolute;
			z-index: -1;
			left: -150px;
			top: -100px;
		}

		.mock .bg-leaf-2 {
			background: url(/wedding/leaf-2.png);
			background-size: cover;
			width: 400px;
			height: 328px;
			position: absolute;
			z-index: -1;
			bottom: 0px;
			right: -150px;
		}

		.mt-logo svg {
			animation: spin 10s linear infinite;
		}

		.mt-logo svg path {
			transition: all 200ms ease-in-out;
		}

		.mt-logo.background--dark svg path{
			fill: white!important;
		}

		.text-hello {
			color:black;
			text-align: right;
		}


		.text-hello.background--dark  {
			color: white;
		}

		.dark-mode {
			background: black;
			color: white;
		}

		.dark-mode .blackbox {
			background: white;
			color: #666!important;
		}

		.blackbox {
			padding: 1px; 
			background: #666; 
			color: white;
		}

		ul.timeline {
			list-style-type: none;
			position: relative;
			margin-left: -20px;
		}
		ul.timeline:before {
			content: ' ';
			background: #d4d9df;
			display: inline-block;
			position: absolute;
			left: 29px;
			width: 2px;
			height: 100%;
			z-index: 400;
		}
		ul.timeline > li {
			margin: 20px 0;
			padding-left: 20px;
		}
		ul.timeline > li:before {
			content: ' ';
			background: white;
			display: inline-block;
			position: absolute;
			border-radius: 50%;
			border: 3px solid #22c0e8;
			left: 20px;
			width: 20px;
			height: 20px;
			z-index: 400;
		}

		

		@keyframes spin {
			0% {
				transform: rotateZ(0);
			}
			100% {
				transform: rotateZ(360deg);
			}
		}

		@media (min-width: 768px) {
			.full-h {
				height: 100vh;	
			}
			.mock {
				margin-left: 15%;
			}
			.mock img {
				height: 80vh;
			}
			ul {
				padding-right: 100px;
			}

			.parallax {
				/* Set a specific height */
				height: 500px;

				/* Create the parallax scrolling effect */
				background-attachment: fixed!important;
				background-position: center!important;
				background-repeat: no-repeat!important;
				background-size: cover!important;
			}
		}

		@media (max-width: 767px) {
			body {
				overflow-y:scroll;
			}
			h1 {
				font-size: 30px;
			}

			h2,  h3 {
				font-size: 24px;
			}

			h4, h5   {
				font-size: 18px;
			}
			.full-h:after {
				position: fixed;
			}
			.mock img {
				width: 60%;
			}
			.btn {
				margin-bottom: 50px;
			}
		}

	</style>

	<header>
		<nav class="fixed-top d-flex p-4 justify-content-between align-items-center">
			<div class="mt-logo">
				<svg width="100" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 145.08 142.03">
					<path d="M637.8,334.8l-6.5,13.9-3.25-1.52,3.58-7.66,0,0-5.21,3.79-2.1-1-.43-6.42,0,0-3.59,7.65L617,342l6.5-13.89,3.3,1.54.39,9.16.06,0,7.25-5.58Z" transform="translate(-527.65 -328.11)" style="fill:#010101"/><path d="M641.11,353.56,637,350l-2.47,1.5-2.85-2.47L646,341.12l2.61,2.27-5.76,15.3L640,356.22Zm1.25-3,2.1-5,0,0-4.65,2.83Z" transform="translate(-527.65 -328.11)" style="fill:#010101"/><path d="M661.46,358.2l-9.94,1.32-5.29,4.08L644,360.71l5.29-4.09,3.79-9.27,2.53,3.27-2.38,5.24,0,.05,5.65-1Z" transform="translate(-527.65 -328.11)" style="fill:#010101"/><path d="M651.18,371.23a5.29,5.29,0,0,1,1.71-4.37,8.33,8.33,0,0,1,2.39-1.57l8.17-3.67,1.49,3.33-8,3.56a5.07,5.07,0,0,0-1.15.71,3.72,3.72,0,0,0-.79.91,2.25,2.25,0,0,0-.34,1.07,2.37,2.37,0,0,0,.23,1.18,2.44,2.44,0,0,0,1.65,1.43,3.88,3.88,0,0,0,2.63-.34l7.95-3.56,1.49,3.31-8.17,3.66a7.76,7.76,0,0,1-3.48.76,5.24,5.24,0,0,1-3-1.05,6.85,6.85,0,0,1-2.16-2.82A7.25,7.25,0,0,1,651.18,371.23Z" transform="translate(-527.65 -328.11)" style="fill:#010101"/><path d="M672.73,394.4l-15.18,2.2-.52-3.55,8.37-1.22v0l-6-2.27-.33-2.28,5.12-3.89v0l-8.36,1.21-.52-3.55,15.19-2.2.52,3.6-7.44,5.36v.07l8.65,3Z" transform="translate(-527.65 -328.11)" style="fill:#010101"/><path d="M672.32,399.64l-.22,3.64-15.32-.93.22-3.65Z" transform="translate(-527.65 -328.11)" style="fill:#010101"/><path d="M654.23,417.46v0a6,6,0,0,1,.12-3.9,5.59,5.59,0,0,1,1.38-2.26,4.6,4.6,0,0,1,4.78-1.08,5,5,0,0,1,3.4,4l2.49-.84a4.79,4.79,0,0,1,2.5,2.49,4.7,4.7,0,0,1,.12,3.54,5.27,5.27,0,0,1-4.73,3.78l-.1-2.93c.22,0,.42-.09.6-.14a3.08,3.08,0,0,0,.55-.23,2,2,0,0,0,.5-.42,2.07,2.07,0,0,0,.35-.64,2,2,0,0,0,.1-.7,1.27,1.27,0,0,0-.16-.57,1.84,1.84,0,0,0-.63-.68l-6.81,2.29v0a5,5,0,0,0,1.61.89l-.91,3a9.88,9.88,0,0,1-2.22-1.08,7.38,7.38,0,0,1-1.84-1.68h0L652,421.38l1.17-3.56Zm3.9-3.92a2.27,2.27,0,0,0-1,1.29,3.51,3.51,0,0,0-.13,1.67h0l4.09-1.39a2.11,2.11,0,0,0-1.46-1.7A1.86,1.86,0,0,0,658.13,413.54Z" transform="translate(-527.65 -328.11)" style="fill:#010101"/><path d="M657,436.14l2.38-3.27,2.61,1.89-6.87,9.46-2.61-1.89,2.37-3.28-9.8-7.12L647.2,429Z" transform="translate(-527.65 -328.11)" style="fill:#010101"/><path d="M652.45,446.87l-2.58,2.58L639,438.6l2.58-2.59Z" transform="translate(-527.65 -328.11)" style="fill:#010101"/><path d="M633,461.87,624.61,449l3-2,4.64,7.07,0,0-.49-6.43,1.93-1.26,5.69,3,0,0-4.64-7.07,3-2,8.42,12.83-3.05,2-8-4.48,0,0,.94,9.1Z" transform="translate(-527.65 -328.11)" style="fill:#010101"/><path d="M600.77,470.13l2.35-15.91,3.26-.52,4.12,9.58h0l1.06-10.39,3.26-.52L622,466.78l-3.8.6-4-8.9h0l-.87,9.68-3.76.6-3.79-8.94h0l-1.09,9.71Z" transform="translate(-527.65 -328.11)" style="fill:#010101"/><path d="M595.74,469.4l-10-1.27.41-3.2,6.43.82.37-2.92-5.08-.65.4-3.15,5.08.65.35-2.77-6.88-.87.4-3.19,10.4,1.32Z" transform="translate(-527.65 -328.11)" style="fill:#010101"/><path d="M580.61,467l-5-1.85a9.09,9.09,0,0,1-2.88-1.69,7,7,0,0,1-1.8-2.36,6.91,6.91,0,0,1-.64-2.84,8.47,8.47,0,0,1,.56-3.11,8.64,8.64,0,0,1,1.6-2.71,6.91,6.91,0,0,1,2.29-1.73,6.83,6.83,0,0,1,2.88-.59,9.42,9.42,0,0,1,3.31.64l5,1.88ZM580,453.84a4.77,4.77,0,0,0-1.45-.31,4.07,4.07,0,0,0-1.36.17,4.24,4.24,0,0,0-2.15,1.5,5.09,5.09,0,0,0-.68,1.26,4.74,4.74,0,0,0-.3,2.33,3.9,3.9,0,0,0,.82,2,4,4,0,0,0,1.77,1.29l1.72.64,3.12-8.33Z" transform="translate(-527.65 -328.11)" style="fill:#010101"/><path d="M563.67,459.9l-4.35-3a9.17,9.17,0,0,1-2.37-2.36,7.09,7.09,0,0,1-1.15-2.74,7,7,0,0,1,.1-2.91,8.35,8.35,0,0,1,1.32-2.87,8.75,8.75,0,0,1,2.23-2.23,6.69,6.69,0,0,1,5.59-.95,9.32,9.32,0,0,1,3,1.46l4.4,3.08ZM566.42,447a4.72,4.72,0,0,0-1.33-.66,4.13,4.13,0,0,0-3.82.74,4.83,4.83,0,0,0-1,1,4.78,4.78,0,0,0-.88,2.18,4.06,4.06,0,0,0,.29,2.16,4,4,0,0,0,1.4,1.69l1.5,1,5.11-7.29Z" transform="translate(-527.65 -328.11)" style="fill:#010101"/><path d="M549.78,449.1l-2.56-2.6,10.93-10.77,2.56,2.6Z" transform="translate(-527.65 -328.11)" style="fill:#010101"/><path d="M536.37,432.05l12.7-8.59,2.09,3.08-4.35,10,0,0,7.61-5.15,2,3L543.75,443l-2-3,4.49-10.17,0,0L538.39,435Z" transform="translate(-527.65 -328.11)" style="fill:#010101"/><path d="M546.53,417.33a7.25,7.25,0,0,1-.22,2.38,7.08,7.08,0,0,1-.94,2.12,7.27,7.27,0,0,1-1.7,1.77,9.52,9.52,0,0,1-5.57,1.78,7.55,7.55,0,0,1-2.86-.61,6.8,6.8,0,0,1-2.39-1.77,8.74,8.74,0,0,1-2.08-6.32,7.53,7.53,0,0,1,1-3.32l2.89,1.7a5,5,0,0,0-.24,3.95,3.79,3.79,0,0,0,.79,1.35,3.59,3.59,0,0,0,1.24.91,4,4,0,0,0,1.64.35,5.68,5.68,0,0,0,3.47-1.16,4.51,4.51,0,0,0,1.09-1.23,3.82,3.82,0,0,0,.49-1.55,4.16,4.16,0,0,0-.23-1.76,5.4,5.4,0,0,0-1.51-2.26l-1.64.57,1,2.78-2.88,1-2.1-6.06,6.52-2.25a8.08,8.08,0,0,1,2.16,2.11,11.28,11.28,0,0,1,1.6,3A9.36,9.36,0,0,1,546.53,417.33Z" transform="translate(-527.65 -328.11)" style="fill:#010101"/><path d="M536.17,391.89a4,4,0,0,1,2.45-.79,4.74,4.74,0,0,1,1.72.28,3.77,3.77,0,0,1,1.37.89,4.11,4.11,0,0,1,.92,1.53,6.66,6.66,0,0,1,.35,2.14l.11,6.73-15.34.24-.09-5.79a9,9,0,0,1,.21-2.23,4.32,4.32,0,0,1,.72-1.59,3,3,0,0,1,1.16-.95,3.71,3.71,0,0,1,1.58-.34,4.53,4.53,0,0,1,1,.1,3.5,3.5,0,0,1,.94.36,3.18,3.18,0,0,1,.86.68,3.67,3.67,0,0,1,.64,1h0A4.07,4.07,0,0,1,536.17,391.89Zm-2.36,7.38,0-1.7a2.38,2.38,0,0,0-.4-1.39,1.23,1.23,0,0,0-1.1-.53,1.29,1.29,0,0,0-1.07.52,2.49,2.49,0,0,0-.36,1.57l0,1.58Zm5.92-3.09a2.72,2.72,0,0,0-.31-.71,1.46,1.46,0,0,0-.51-.45,1.37,1.37,0,0,0-.71-.15,1.4,1.4,0,0,0-1.17.58,2.74,2.74,0,0,0-.42,1.7l0,2.07,3.24-.05,0-2.07A4.19,4.19,0,0,0,539.73,396.18Z" transform="translate(-527.65 -328.11)" style="fill:#010101"/><path d="M542.38,380.9l-1.21,5.26,2.48,1.48-.85,3.68-13.7-8.92.78-3.38L546.1,377l-.84,3.68Zm-3.2.28-5.43.49v0l4.67,2.79Z" transform="translate(-527.65 -328.11)" style="fill:#010101"/><path d="M547,374.71a8.15,8.15,0,0,1-2.82,1.61l-1.44-3.07a6.19,6.19,0,0,0,2-1,4.24,4.24,0,0,0,1.21-1.44,2.54,2.54,0,0,0,.28-1,1.19,1.19,0,0,0-.16-.71,1.1,1.1,0,0,0-.5-.42,1.17,1.17,0,0,0-1.09,0A6.58,6.58,0,0,0,543,370c-.28.29-.53.53-.76.74a8,8,0,0,1-.8.66,7.94,7.94,0,0,1-.86.57,6.24,6.24,0,0,1-.9.38,3.44,3.44,0,0,1-1,.18,3.75,3.75,0,0,1-1-.09,4.34,4.34,0,0,1-1.13-.39,4.15,4.15,0,0,1-1.77-1.6,4.66,4.66,0,0,1-.67-2.36,5.52,5.52,0,0,1,.55-2.55,8,8,0,0,1,4.13-3.93l1.52,3a5.47,5.47,0,0,0-2.72,2.21,1.56,1.56,0,0,0-.16,1.05,1.14,1.14,0,0,0,.62.73,1,1,0,0,0,1,0,8.79,8.79,0,0,0,1.54-1.23,21.12,21.12,0,0,1,2.16-1.86,3.92,3.92,0,0,1,2.6-.82,4.61,4.61,0,0,1,1.57.48,4.85,4.85,0,0,1,1.56,1.14,4.08,4.08,0,0,1,.86,1.43,5.2,5.2,0,0,1,.27,1.59,6.28,6.28,0,0,1-.17,1.58,7.31,7.31,0,0,1-.53,1.44A7.59,7.59,0,0,1,547,374.71Z" transform="translate(-527.65 -328.11)" style="fill:#010101"/><path d="M539.71,356.51l2.28-2.84,5,4,3.39-4.21-5-4,2.29-2.83,11.94,9.63-2.28,2.84-4.5-3.63-3.39,4.21,4.49,3.62-2.28,2.83Z" transform="translate(-527.65 -328.11)" style="fill:#010101"/>
				</svg>
			</div>

			
			<div id="dateLeft" class="text-hello mr-3" target="_blank"></div>

		</div>
	</nav>
</header>

<section class="d-md-flex align-items-center py-5 my-5">

	<div class="mock wow fadeIn position-relative" data-wow-duration="1.5s">
		<div class="bg-leaf-1"></div>
		<div class="bg-leaf-2"></div>
		<img src="/wedding/top.jpg" alt="" style="transform: rotate(-5deg); box-shadow:0 0 20px rgba(0,0,0,.15); border:10px solid white">
	</div>
	<div class="text-dark px-4 pl-md-5 pr-md-4 py-5 my-0">
		<div class="wow fadeIn" data-wow-duration="2s" data-wow-delay="0.5s">
			<h4>2020年3月29日(日)に、東京にて結婚式とパーティーを行います。<br>お時間ご都合よろしければ、ぜひお気軽にお越しください。</h4>
			<br>
			<h3>On March 29, 2020, We invite you to our wedding celebration in Tokyo, Japan.</h3>
			<br>
		</div>
		<div class="wow fadeIn " data-wow-duration="2s" data-wow-delay="1s">
			<h5>当日の簡単な流れ<br>Here is the quick rundown.</h5>
			<ul class="timeline">
				<li>
					<a href="#church">
						2:30 ~ 3:30PM <br>教会にて結婚式 / Church Ceremony
					</a>
				</li>
				<li>
					<a href="#boat">
						7:00 ~ 9:00PM <br>ウェディングレセプションパーティー / Wedding Reception
					</a>
				</li>
			</ul>

		</div>
	</div>
</section>

<section class="d-md-flex align-items-center py-5 my-5" id="church">

	<div class="text-dark px-4 px-md-5 py-5 my-0">
		<img src="/wedding/leaf-3.png" alt="">
		<div class="wow fadeIn" data-wow-duration="2s" data-wow-delay="0s">
			<h3>2:30 ~ 3:30PM</h3>
			<h4>東京カテドラル聖マリア大聖堂にて、結婚式</h4>
			<h4>					Wedding Ceremony<br>@&nbsp;St.&nbsp;Mary's Cathedral</span></h4>
		</div>
		<div class="wow fadeIn" data-wow-duration="2s" data-wow-delay="1s">
			<br>
			<p>結婚式は、日本語で執り行われます。受付等はございませんので、お気軽にお越しください。</p>
			<p>The ceremony will be conducted in Japanese. Attendance is not required but always welcomed.</p>
		</div>
	</div>
	<div class="wow fadeIn" data-wow-duration="2s" data-wow-delay="1.5s">
		<img class="w-100" src="/wedding/mary-2.jpg" alt="">
	</div>
</section>

<section class="container mb-5">
	<div class="col-md-10 mx-auto">
		<h2><span class="blackbox">交通アクセス / Direction</span></h2>
		<p>JRの場合：<br>
			山手線目白駅下車後、都営バス白61系統　新宿駅西口行きにて「ホテル椿山荘前」で下車。
			<br>
			メトロの場合：<br>
		有楽町線江戸川橋駅（出口1a）より徒歩約15分</p>
		<p><strong>By Train:</strong><br>
			1. Take Yamanote Line arriving at Mejiro Station<br>
			2. From Mejiro Station, take Bus #61 Westbound arriving at Hotel Chinzanso bus stop (6 stops).
		</p>
		<p><strong>By Taxi:</strong><br>
			You can tell driver to bring you to Hotel Chinzanso in Mejiro, as the church is literally across from it.
		</p>
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3239.49359879413!2d139.7239407152537!3d35.714077380186644!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60188d043a76155f%3A0x790bd9ecd638d809!2sSt.%20Mary%20Cathedral%20%2F%20Kenzo%20Tange!5e0!3m2!1sen!2sjp!4v1578026455461!5m2!1sen!2sjp" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
	</div>
</section>


<section class="parallax" style="background:url('/wedding/mary.jpg')">
</section>


<section class="d-md-flex align-items-center py-5 dark-mode" id="boat">

	<div class="text-white px-4 px-md-5 py-5 my-0">
		<img src="/wedding/leaf-3.png" alt="">
		<div class="wow fadeIn" data-wow-duration="2s" data-wow-delay="0s">
			<h3>7:00 ~ 9:00PM<h3>
				<h4>ホタルナ船上レセプションパーティー</h4><h4>Wedding Reception<br>@&nbsp;Hotaluna</h4>
				<br>
			</div>
			<div class="wow fadeIn" data-wow-duration="2s" data-wow-delay="1s">
				<p>船は、日の出桟橋より7:00PMに出航し、東京湾を2時間ほどクルーズする予定です。
					軽食とドリンクを船内にてご用意しております。<br><br>
				受付があるため、6:30PMを目安に乗船場所へお越しください。</p>
				<p>The boat will depart at 7:00PM and cruise around the Tokyo Bay, including Odaiba. Dinner Buffet and Drinks will be served inside the Boat Cruise.</p>
				<p>Please arrive at least 30 minutes prior to boat departure.</p>
			</div>
		</div>
		<div class="wow fadeIn" data-wow-duration="2s" data-wow-delay="0.5s">
			<img class="w-100"src="/wedding/boat.jpg" alt="">
		</div>
	</section>

	<section class="container-fluid pb-5 dark-mode">
		<div class="container">
			<div class="col-md-10 mx-auto">	
				<h2><span class="blackbox">交通アクセス / Direction</span></h2>
				<p>ゆりかもめの場合：<br>
				日の出駅で下車後、徒歩約5分</p>
				<p>
					JRの場合：<br>
				浜松町駅より徒歩約10分</p>
				<p><strong>By Train:</strong><br>
					Take Yurikamome Line to Hinode Station, then walk 5 minutes to the Pier.<br>
					Or take Keihin-Tōhoku or Yamanote Line to Hamamatsucho Station, then walk 10 minutes to the Pier.
				</p>
				<p><strong>By Taxi:</strong><br>
					Hinode Pier
				</p>
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3242.068133743792!2d139.75828511497397!3d35.650692939254164!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60188bcdc945dac1%3A0xb4f1613cb80a56ce!2z44Ob44K_44Or44OKIEhvdGFsdW5h!5e0!3m2!1sen!2sjp!4v1578028389375!5m2!1sen!2sjp" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
			</div>
		</div>
	</section>

	<section class="parallax dark-mode" style="background:url('/wedding/boat-1.jpg')">
	</section>


	<section class="d-md-flex align-items-center py-5 my-5">

		<div class="mock wow fadeIn position-relative" data-wow-duration="1.5s">
			<div class="bg-leaf-1"></div>
			<div class="bg-leaf-2"></div>
			<img src="/wedding/bottom.jpg" alt="" style="transform: rotate(-5deg); box-shadow:0 0 20px rgba(0,0,0,.15); border:10px solid white">
		</div>
		<div class="text-dark px-4 px-md-5 py-5 my-0">
			<div class="wow fadeIn" data-wow-duration="2s" data-wow-delay="0.5s">
				<h4 style="color:red">お手数ですが、船上パーティーへの出欠をお知らせください。<br><br>Please confirm your attendance at our Wedding Reception in the evening.</h4>
				<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSfOk4HOEAaBVxUPKJHtmQDJalyG1M7LcxUan-lS8l3a2QWc2g/viewform?embedded=true" width="100%" height="600" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>
			</div>

		</div>

	</section>

	<h2 class="pb-5 mb-5 px-5 text-center">We look forward to seeing you there!</h2>



	<script src="https://code.jquery.com/jquery-3.4.1.js" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
	<script src="http://www.kennethcachia.com/background-check/scripts/background-check.min.js"></script>

	<script>
		new WOW().init();

		BackgroundCheck.init({
			targets: '.mt-logo, .text-hello'
		});

		today = new Date();

		xmas = new Date("March 29, 2020");
		msPerDay = 24 * 60 * 60 * 1000 ;
		timeLeft = (xmas.getTime() - today.getTime());


		e_daysLeft = timeLeft / msPerDay;
		daysLeft = Math.floor(e_daysLeft);

		document.getElementById("dateLeft").innerHTML = daysLeft + " Days to go";


		$(document).on('click', 'a[href^="#"]', function (e) {
			e.preventDefault();
			$('html, body').stop().animate({
				scrollTop: $($(this).attr('href')).offset().top
			}, 0, 'linear');
		});

	</script>

</body>
</html>