    <?php include('header.php'); ?>

    <section class="banner bg-zaiko-overlay h-100 text-center">
      <div class="text-block">
        <h1>Frequently Asked Questions</h1>
        <p>What do you want to know today?</p>
    </section>

    <div class="text-block">

        <div class="container-fluid border-bottom faq__btn-group">
            <div class="row p-5">
                <div class="col-md-3 my-3">
                    <a href="#faq-topic-1">
                        <div class="faq__btn faq__btn-1">
                            <i class="fas fa-users"></i>
                            <span>Group Ticketing</span>
                            <i class="fas fa-chevron-right"></i>

                        </div>
                    </a>
                </div>
                <div class="col-md-3 my-3">
                    <a href="#faq-topic-2">
                        <div class="faq__btn faq__btn-2">
                            <i class="fas fa-credit-card"></i>
                            <span>Payment</span>
                            <i class="fas fa-chevron-right"></i>
                        </div>

                    </a>
                </div>
                <div class="col-md-3 my-3">
                    <a href="#faq-topic-3" >
                        <div class="faq__btn faq__btn-3">
                            <i class="fas fa-shopping-cart"></i>
                            <span>Purchase</span>
                            <i class="fas fa-chevron-right"></i>
                        </div>
                        
                    </a>
                </div>
                <div class="col-md-3 my-3">
                    <a href="#">
                        <div class="faq__btn faq__btn-4">
                            <i class="fas fa-check-circle"></i>
                            <span>Confirmation</span>
                            <i class="fas fa-chevron-right"></i>
                        </div>
                    </a>
                    
                </div>
                <div class="col-md-3 my-3">
                    <a href="#">
                        <div class="faq__btn faq__btn-5">
                            <i class="fas fa-ban"></i>
                            <span>Cancellation</span>
                            <i class="fas fa-chevron-right"></i>
                        </div>
                    </a>
                    
                </div>
                <div class="col-md-3 my-3">
                    <a href="#">
                        <div class="faq__btn faq__btn-6">
                            <i class="fas fa-edit"></i>
                            <span>Modification</span>
                            <i class="fas fa-chevron-right"></i>
                        </div>
                    </a>
                    
                </div>
                <div class="col-md-3 my-3">
                    <a href="#">
                        <div class="faq__btn faq__btn-7">
                            <i class="fas fa-user-circle"></i>
                            <span>Account</span>
                            <i class="fas fa-chevron-right"></i>
                        </div>
                    </a>

                </div>
                <div class="col-md-3 my-3">
                    <a href="#">
                        <div class="faq__btn faq__btn-8">
                            <i class="fas fa-cogs"></i>
                            <span>Others</span>
                            <i class="fas fa-chevron-right"></i>
                        </div>
                    </a>
                    
                </div>

            </div>
        </div>
    </div>

</div>

<div class="container-fluid">
    <div class="row faq">
        <div class="col-12 col-md-3 col-xl-3 faq__sidebar d-none d-sm-block">

            <div id="fag-groups">
                <form action="">
                    <input type="text" class="form-control" id="faqSearch" placeholder="Search">
                </form>

                <!-- One Group -->
                <div class="question"> 
                    <a class="heading collapsed" data-toggle="collapse" data-target="#faq-group-1" aria-expanded="false" aria-controls="faq-group-1">Group Ticketing<i class="fas fa-chevron-right"></i></a>
                    <div id="faq-group-1" class="collapse list-group">
                        <a class="list-group-item list-group-item-action" href="#faq-1-1">What is Group Tickets?</a>
                        <a class="list-group-item list-group-item-action" href="#faq-1-2">I bought a group ticket, but can we enter separately?</a>
                        <a class="list-group-item list-group-item-action" href="#faq-1-3">If the buyer cannot join the event, how other guests can get in?</a>
                        <a class="list-group-item list-group-item-action" href="#faq-1-4">Sub-heading</a>
                    </div>
                </div>

                <!-- Another Group -->
                <div class="question">
                    <a class="heading collapsed" data-toggle="collapse" data-target="#faq-group-2" aria-expanded="false" aria-controls="faq-group-2">Payment<i class="fas fa-chevron-right"></i></a>
                    <div id="faq-group-2" class="collapse list-group">
                        <a class="list-group-item list-group-item-action" href="#faq-2-1">Payment Question 1</a>
                        <a class="list-group-item list-group-item-action" href="#faq-2-2">Payment Question 2</a>
                        <a class="list-group-item list-group-item-action" href="#faq-2-3">Payment Question 3</a>
                        <a class="list-group-item list-group-item-action" href="#faq-2-4">Payment Question 4</a>
                    </div>
                </div>

                <!-- etc -->

                <div class="question">
                    <a class="heading collapsed" data-toggle="collapse" data-target="#faq-group-#" aria-expanded="false" aria-controls="faq-group-#">Purchase<i class="fas fa-chevron-right"></i></a>
                    <div id="faq-group-3" class="collapse list-group">
                       <!-- Questions -->
                   </div>
               </div>
               <div class="question">
                <a class="heading collapsed" data-toggle="collapse" data-target="#faq-group-#" aria-expanded="false" aria-controls="faq-group-#">Confirmation<i class="fas fa-chevron-right"></i></a>
                <div id="faq-group-4" class="collapse list-group">
                   <!-- Questions -->
               </div>
           </div>
           <div class="question">
            <a class="heading collapsed" data-toggle="collapse" data-target="#faq-group-#" aria-expanded="false" aria-controls="faq-group-#">Cancellation<i class="fas fa-chevron-right"></i></a>
            <div id="faq-group-5" class="collapse list-group">
               <!-- Questions -->
           </div>
       </div>
       <div class="question">
        <a class="heading collapsed" data-toggle="collapse" data-target="#faq-group-#" aria-expanded="false" aria-controls="faq-group-#">Modification<i class="fas fa-chevron-right"></i></a>
        <div id="faq-group-6" class="collapse list-group">
           <!-- Questions -->
       </div>
   </div>
   <div class="question">
    <a class="heading collapsed" data-toggle="collapse" data-target="#faq-group-#" aria-expanded="false" aria-controls="faq-group-#">Account<i class="fas fa-chevron-right"></i></a>
    <div id="faq-group-" class="collapse list-group">
       <!-- Questions -->
   </div>
</div>
<div class="question">
    <a class="heading collapsed" data-toggle="collapse" data-target="#faq-group-#" aria-expanded="false" aria-controls="faq-group-#">Others<i class="fas fa-chevron-right"></i></a>
    <div id="faq-group-8" class="collapse list-group">
       <!-- Questions -->
   </div>
</div>

</div>
</div>    
<div class="col-12 col-md-9 col-xl-9 faq__content" data-spy="scroll" data-target="fag-groups"data-offset="0">
    <!-- Answer 1 -->
    <div class="answers">
        <h1 id="faq-topic-1" class="heading">Group Ticketing</h1>
        <div class="" data-spy="scroll" data-target="#faq-group-1" data-offset="0">
            <div class="text-block" id="faq-1-1">
                <h3 class="subheading">What is Group Tickets?</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut <a href="">labore et dolore magna aliqua.</a> Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <img src="./img/faq-1.jpg" class="img-fluid img-faq" alt="">
                <ul>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat facere, quis et voluptatum laudantium aliquid sapiente amet aperiam, cum officiis sit repellendus odit voluptatibus necessitatibus, aut velit minus illo facilis. </li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat facere, quis et voluptatum laudantium aliquid sapiente amet aperiam, cum officiis sit repellendus odit voluptatibus necessitatibus, aut velit minus illo facilis. </li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat facere, quis et voluptatum laudantium aliquid sapiente amet aperiam, cum officiis sit repellendus odit voluptatibus necessitatibus, aut velit minus illo facilis. </li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat facere, quis et voluptatum laudantium aliquid sapiente amet aperiam, cum officiis sit repellendus odit voluptatibus necessitatibus, aut velit minus illo facilis. </li>
                </ul>
                <a href="#" class="btn back-to-top">Back to Top     <i class="fas fa-chevron-up"></i>    </a>
            </div>
            <div class="text-block" id="faq-1-2">
                <h3 class="subheading">I bought a group ticket, but can we enter separately?</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <a href="#" class="btn back-to-top">Back to Top     <i class="fas fa-chevron-up"></i>    </a>
            </div>
            <div class="text-block" id="faq-1-3">
                <h3 class="subheading">If the buyer cannot join the event, how other guests can get in?</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <p>Be sure to add aria-expanded to the control element. This attribute explicitly conveys </p>
                <a href="#" class="btn back-to-top">Back to Top     <i class="fas fa-chevron-up"></i>    </a>
            </div>

            <div class="text-block" id="faq-1-4">
                <h3 class="subheading">Sub-heading</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <a href="#" class="btn back-to-top">Back to Top     <i class="fas fa-chevron-up"></i>    </a>
            </div>

        </div>
    </div>

    <!-- Answer 2 -->
    <div class="answers">
        <h1 id="faq-topic-2" class="heading">Payment</h1>
        <div class="" data-spy="scroll" data-target="#faq-group-2" data-offset="0">
            <div class="text-block" id="faq-2-1">
                <h3 class="subheading">Payment Question 1</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <a href="#" class="btn back-to-top">Back to Top     <i class="fas fa-chevron-up"></i>    </a>

            </div>
            <div class="text-block" id="faq-2-2">
                <h3 class="subheading">Payment Question 2?</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <a href="#" class="btn back-to-top">Back to Top     <i class="fas fa-chevron-up"></i>    </a>
            </div>
            <div class="text-block" id="faq-2-3">
                <h3 class="subheading">Payment Question 3</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <p>Be sure to add aria-expanded to the control element. This attribute explicitly conveys </p>
                <a href="#" class="btn back-to-top">Back to Top     <i class="fas fa-chevron-up"></i>    </a>
            </div>

            <div class="text-block" id="faq-2-4">
                <h3 class="subheading">Payment Question 4</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <a href="#" class="btn back-to-top">Back to Top     <i class="fas fa-chevron-up"></i>    </a>
            </div>

        </div>
    </div>
</div>
</div>        
</div>

<?php include('footer.php'); ?>