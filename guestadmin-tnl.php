<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
	<link type="text/css" rel="stylesheet" href="/css/vone/app.css">

</head>
<body class="guest-admin">

	<header class="topbar p-4" style="background:#153b5f">
		<img src="img/guestadmin/logo-tnl.png" alt="">
	</header>

	<nav class="bg-dark p-3 text-white">
		<div class="px-5">
			HOME | LISTINGS | TICKETS | SUPPORT | SUBSCRIBE | ADD EVENT | <a href="guestadmin.php" style="color: white">YOUR ADMIN</a>&nbsp;<a href="guestadmin.php#gaNotification"><sup class="badge badge-pill badge-danger">3</sup></a>
		</div>
	</nav>

<section class="container pt-5">
	<div class="row">
		<div class="col-md-12 d-flex justify-content-between align-items-center">
			<h3>Overview</h3>
			<span>
				<a href="guestadmin-intro.php" class="btn btn-secondary">What is ZAIKO Lite?</a>
				<a href="#" class="btn btn-outline-brand" data-toggle="modal" data-target="#modal-youtube"><i class="fas fa-ticket-alt"></i>&nbsp;&nbsp;How to Check-in</a>
			</span>
		</div>
	</div>

	<div class="alert alert-success alert-hidden alert-submit fade show">
		You have submitted your event <strong>Fyre Festival</strong> to TimeOut Tokyo. You will be notified when they approve or reject your request.
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<div class="alert alert-success alert-hidden alert-addbank fade show">
		You have successfully added your bank account information.
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

		<div class="alert alert-success alert-hidden alert-payout fade show">
		You have successfully requested for payout on <strong>Fyre Festival</strong>. Please expect the bank transfer on the specified date.
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>


	<div class="row">
		<div class="col-md-4">
			<div class="card py-4">
				<div class="text-block text-center">
					<h5 class="mb-1 font-weight-normal">Active Tickets Sold</h5>
					<h2 class="text-brand mb-2">1,088</h2>
					<a href="#" class="btn btn-outline-brand" data-toggle="modal" data-target="#modal-agree">Add An Event</a>
				</div>
			</div>

		</div>
		<div class="col-md-4">
			<div class="card py-4">
				<div class="text-block text-center">
					<h5 class="mb-1 font-weight-normal">Total Members</h5>
					<h2 class="text-brand mb-2">348</h2>
					<a href="purchases.php" class="btn btn-outline-brand">View All Members</a>
				</div>
			</div>

		</div>
		<div class="col-md-4">
			<div class="card py-4">
				<div class="text-block text-center">
					<h5 class="mb-1 font-weight-normal">Sales Proceeds</h5>
					<h2 class="text-brand mb-2" style="white-space:nowrap;">¥ 2,012,091</h2>
					<a href="guestadmin-all.php" class="btn btn-outline-brand">View Report</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="d-flex justify-content-between align-items-center">
				<h3 class="pt-3">Your Events</h3>
			</div>
			<div class="card table-responsive">
				<table class="table table-hover">
					<thead class="thead-light text-center">
						<tr>
							<th scope="col">Event Name</th>
							<th scope="col">Event Date</th>							
							<th scope="col">Tickets Sold</th>
							<th scope="col">Status</th>
							<th scope="col"></th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<td><a href="event-display.php" target="_blank">Event #1</a></td>
							<td>2020-09-01</td>
							<td><a class="font-weight-bold" href="purchases-1.php">122</a> / 300</td>
							<td><a href="event-approved.php" class="badge badge-success">Active</a></td>

							<td>
								<a href="#" data-toggle="modal" data-target="#modal-promote" class="btn btn-brand">Promote</a>
							</td>
						</tr>
						<tr>
							<td><a href="event-display.php" target="_blank">Fyre Festival</a></td>
							<td>2020-08-08</td>
							<td><a class="font-weight-bold" href="purchases-1.php">84</a> / 500</td>
							<td><a href="event-approved.php" class="badge badge-success">Active</a></td>
							<td>
								<a href="#" data-toggle="modal" data-target="#modal-promote" class="btn btn-brand">Promote</a>
							</td>
						</tr>
						<tr>
							<td><a href="event-display.php" target="_blank">Test Event</a></td>
							<td>2020-07-21</td>
							<td>-</td>
							<td><a href="event-pending.php" class="badge badge-secondary">Pending</a></td>
							<td>
								<a href="#" class="btn btn-brand disabled" aria-disabled="true">Promote</a>
							</td>
						</tr>
						<tr>
							<td><a href="event-display.php" target="_blank">Tech Conf</a></td>
							<td>2020-06-15</td>
							<td>-</td>
							<td><a href="event-rejected.php" class="badge badge-danger">Rejected</a></td>
							<td>
								<a href="#" class="btn btn-brand disabled" aria-disabled="true">Promote</a>
							</td>
						</tr>
						<tr>
							<td><a href="event-display.php" target="_blank">ZAIKO Office Party</a></td>
							<td>2020-05-01</td>
							<td><a class="font-weight-bold" href="purchases-1.php">50</a> / 50</td>
							<td><a href="event-over.php" class="badge badge-info">Completed</a></td>
							<td><a href="#" class="btn btn-brand disabled" aria-disabled="true">Promote</a></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<nav aria-label="">
		<ul class="pagination justify-content-end">
			<li class="page-item disabled">
				<a class="page-link" href="#" tabindex="-1">Prev</a>
			</li>
			<li class="page-item active"><a class="page-link" href="#">1</a></li>
			<li class="page-item"><a class="page-link" href="#">2</a></li>
			<li class="page-item"><a class="page-link" href="#">3</a></li>
			<li class="page-item">
				<a class="page-link" href="#">Next</a>
			</li>
		</ul>
	</nav>
</section>

<!-- Modal -->
<div id="modal-youtube" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg modal-dialog-centered">
		<div class="modal-content">
			<div class="embed-responsive embed-responsive-16by9">
				<iframe width="100%" src="https://www.youtube.com/embed/nzjuiyJhY0o" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
		</div>
	</div>
</div>

<section class="container">
	<div class="row">
		<div class="col-md-12">
			<h3 id="gaNotification" class="pt-3">Notifications</h3>
			<div class="card">
				<ul class="notification-list mb-0">
					<li class="p-4 position-relative">
						<a href="event-over.php">
							<p>Your event <strong>ZAIKO Office Party</strong> is now over. <strong>Please apply for payout.</strong><span class="badge badge-pill badge-danger">NEW</span></p>
						</a>
						<div class="text-med-grey"><i class="fas fa-clock"></i>2 days ago</div>
						<i class="fas fa-chevron-right"></i>
					</li>

					<li class="p-4 position-relative">
						<a href="event-approved.php">
							<p>
								TimeOut Tokyo has approved your request to edit event <strong>Fyre Festival</strong>. Click here for details.<span class="badge badge-pill badge-danger">NEW</span>
							</p>
							<div class="text-med-grey"><i class="fas fa-clock"></i>22 hours ago</div>
							<i class="fas fa-chevron-right"></i>
						</a>
					</li>
					<li class="p-4 position-relative">
						<a href="purchases-1.php">
							<p>
								5 People have purchased your event <strong>Event #1</strong> in the past 24 hours. See the attendees list.<span class="badge badge-pill badge-danger">NEW</span>
							</p>
						</a>
						<div class="text-med-grey"><i class="fas fa-clock"></i>1 day ago</div>
						<i class="fas fa-chevron-right"></i>
					</li>
					<li class="p-4 position-relative">
						<a href="event-rejected.php">
							<p>
								Your event <strong>Tech Conf</strong> has been rejected. Click here for details.
							</p>
						</a>
						<div class="text-med-grey"><i class="fas fa-clock"></i>2 days ago</div>
						<i class="fas fa-chevron-right"></i>
					</li>
					<li class="p-4 position-relative">
						<a href="event-approved.php">
							<p>
								Your event <strong>Fyre Festival</strong> has been approved. Click here for details.
							</p>
						</a>
						<div class="text-med-grey"><i class="fas fa-clock"></i>2 days ago</div>
						<i class="fas fa-chevron-right"></i>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>


<div id="modal-agree" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg modal-dialog-centered">
		<div class="modal-content p-5 text-center">
			<h3>TimeOut Tokyo Ticket Rate</h3>
			<p>By accessing this admin you will be able to request that your events, tickets and items be listed on this service.</p>
			<h3 class="text-brand">10% + System Fee (4.98%+ 98 JPY)</h3>

			<div class="alert alert-warning">
				Please understand that the listing is at our discretion and even though an event or ticket is submitted we have the right to approve or reject it.
			</div>

			<span>
				<a href="addevent-2.php" class="btn btn-brand btn-lg">I hereby agree to the rate</a>
				<a href="#" class="btn btn-outline-brand btn-lg" data-dismiss="modal" aria-label="Close">Cancel</a>
			</span>
		</div>
	</div>
</div>

<?php include('guestadmin-modal-promote.php'); ?>

<?php include('guestadmin-footer.php'); ?>


