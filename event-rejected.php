<?php include('guestadmin-header.php'); ?>

<section class="container-fluid">
	<div class="text-center pt-5">
		<div class="text-danger">
			<i class="fas fa-exclamation-circle h1"></i>
			<h4 class="fa-step--text">Your Event has been rejected</h4>
		</div>
	</div>
	<p class="text-center text-dark-grey">The Media has rejected your request to publish the event <strong>Tech Conf</strong> on their listing. Please view their reason below.</p>
</section>

<section class="container p-5">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<ul class="status-details">
				<div class="alert alert-danger">
					<strong>Reason for Rejection</strong><br>
					[Reason for Rejection from selected list]
				</div>
			</ul>
			<div class="text-center">
				<p class="py-4">You can edit and re-submit your event to the media for review. However, you can only do this once. If rejected again you can no longer create or submit your event, and your account will be reviewed. <strong>
				If you wish to edit the event before re-submitting, please create a new event instead.</strong>
				</p>
				<a href="guestadmin.php" class="btn btn-brand btn-lg">Return to Home Page</a>
				<a href="#" data-toggle="modal" data-target="#modal-resubmit"><button class="btn btn-secondary btn-lg">Re-submit Event</button></a>
			</div>
		</div>
	</div>		
</section>

<div id="modal-resubmit" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg modal-dialog-centered">
		<div class="modal-content queue p-5 text-center">
			<h3>Are you sure you want to re-submit this event?</h3>
			<label>Enter your explanation for this event to be approved.</label>
			<div class="form-row">
				<div class="form-group col-md-12">
					<textarea row="5" class="form-control"></textarea>
					<small id="companyHelp" class="form-text text-muted"></small>
				</div>
			</div>
			<span>
				<a href="guestadmin.php" class="btn btn-brand btn-lg">Re-submit this event</a>
				<a href="#" data-dismiss="modal" aria-label="Close" class="btn btn-secondary btn-lg">Cancel</a>
			</span>
		</div>
	</div>
</div>

<?php include('guestadmin-footer.php'); ?>


