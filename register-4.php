<?php include('header.php'); ?>

<style>
	header, footer {
		display: none;
	}
</style>

<section class="bg-overlay opacity-40 bg-zaiko full-h d-flex justify-content-center align-items-center flex-column">

	<div class="container">
		<div class="row">
			<div class="col-md-6 mx-auto card card-dark p-5">
				<a href="/"><img class="img-zaiko d-block mx-auto mb-3" src="/img/ZAIKO-logo-text-white.svg" alt=""></a>

				<?php include('register/block-4.php') ?>

			</div>
		</div>
	</div>

</section>

<?php include('footer.php'); ?>