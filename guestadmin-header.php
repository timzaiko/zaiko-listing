<?php include('header.php') ?>

<div class="guestadmin-nav sticky-top navbar-expand-md">
	<button type="button" class="navbar-toggle navbar-toggle-guestadmin d-md-none" data-toggle="collapse" data-target="#guestadmin-navbar" aria-expanded="true" aria-controls="navbar">
		<i class="fas fa-ellipsis-v"></i>&nbsp;&nbsp;ZAIKO Lite
	</button>

	<nav id="guestadmin-navbar" class="navbar-collapse px-3 text-med-grey bg-dark-grey collapse">
		<ul class="m-0 p-0">
			<a href="#" class="text-med-grey d-md-inline-block"><li class="p-3">Home</li></a>
			<a href="#" class="text-med-grey d-md-inline-block"><li class="p-3">Listings</li></a>
			<a href="#" class="text-med-grey d-md-inline-block"><li class="p-3">Tickets</li></a>
			<a href="#" class="text-med-grey d-md-inline-block"><li class="p-3">Support</li></a>
			<a href="#" class="text-med-grey d-md-inline-block"><li class="p-3">Subscribe</li></a>
			<a href="#" class="text-med-grey d-md-inline-block"><li class="p-3">Your Admin</li></a>
		</ul>
	</nav>
</div>

<div id="guestadmin">

	<style>
		.header--transparent {
			padding: 1rem;
			z-index: 100;
			border-bottom: 1px solid rgba(0,0,0,0.2);
		}

		.navbar-toggle-guestadmin { /*toggle button*/
			background:#333;
			color: white;
			top: 0;
			z-index: 9999;
			border: none;
			width: 100%;
			padding: 10px;
		}
	</style>