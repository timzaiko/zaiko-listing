<div class="listing-group d-flex justify-content-between flex-wrap">
	<?php 
	for($i= 0; $i < 12; $i++) {
		?>
		<a href="#" class="card listing-group-upcoming">
			<img src="https://designshack.net/wp-content/uploads/placeholder-image.png" alt="">
			<div class="event p-3">
				<p class="h5">{{ Event Name }}</p>
				<span>{{ Event Date }}</span>
				<div class="event event-location text-right">{{City}}, {{Country }}</div>
				<div class="text-right">
					<span class="badge badge-primary">{{ Category }}</span>
					<!-- if past event -->
					<span class="badge badge-danger">Past Event</span>
					<!-- if advertisment -->
					<span class="badge badge-secondary">Sponsored</span>
				</div>
			</div>
		</a>
	<?php } ?>
</div>
