<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><meta name="x-apple-disable-message-reformatting"><meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>{{ Email Name }}</title>

  <style>
  a { 
    text-decoration: none; 
    outline: none; 
  }
  @media (max-width: 480px) {
    .zmail_col-full { max-width: 100% !important; }
    .zmail_col-half { max-width: 50% !important; }
    .zmail_hide-lg { display: inline-block !important; font-size: inherit !important; max-height: none !important; line-height: inherit !important; overflow: visible !important; width: auto !important; visibility: visible !important; }
    .zmail_hide-xs, .zmail_hide-xs.zmail_col_i { display: none !important; font-size: 0 !important; max-height: 0 !important; width: 0 !important; line-height: 0 !important; overflow: hidden !important; visibility: hidden !important; height: 0 !important; }
    .zmail_xs-center { text-align: center !important; }
    .zmail_xs-left { text-align: left !important; }
    .zmail_xs-right { text-align: left !important; }
    table.zmail_xs-left { margin-left: 0 !important; margin-right: auto !important; float: none !important; }
    table.zmail_xs-right { margin-left: auto !important; margin-right: 0 !important; float: none !important; }
    table.zmail_xs-center { margin-left: auto !important; margin-right: auto !important; float: none !important; }
    h1.zmail_heading { font-size: 32px !important; line-height: 41px !important; }
    h2.zmail_heading { font-size: 26px !important; line-height: 37px !important; }
    h3.zmail_heading { font-size: 20px !important; line-height: 30px !important; }
    .zmail_xs-py-md { padding-top: 24px !important; padding-bottom: 24px !important; }
    .zmail_xs-pt-xs { padding-top: 8px !important; }
    .zmail_xs-pb-xs { padding-bottom: 8px !important; }
  }
  @media screen {
    .zmail_sans, .zmail_heading { font-family: Helvetica, sans-serif !important; }
    .zmail_heading, strong, b { font-weight: 700 !important; }
    a[x-apple-data-detectors] { color: inherit !important; text-decoration: none !important; }
  }
  #canvas td.zmail_hide, #canvas td.zmail_hide div { display: block!important; font-family: Helvetica, sans-serif; font-size: 16px!important; color: #000; font-size: inherit!important; max-height: none!important; width: auto!important; line-height: inherit!important; visibility: visible!important;}
</style>


</head>
<body marginwidth="0" marginheight="0" style="margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;" offset="0" topmargin="0" leftmargin="0">

  <table data-module="header-primary-link0" data-thumb="" width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" class="">

    <tbody>
      <tr>
        <td class="zmail_bg-light zmail_px-xs zmail_pt-lg zmail_xs-pt-xs" align="center" data-bgcolor="Bg Light" style="background-color: #f4f4f4;padding-left: 8px;padding-right: 8px;padding-top: 32px;">
          <!--[if mso]><table width="455" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td><![endif]-->
            <table class="zmail_block-xs" width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" style="max-width: 455px;margin: 0 auto;">

              <tbody>
                <tr>
                  <td class="zmail_re zmail_bg-primary zmail_px zmail_pb-md zmail_br-t" align="center" data-bgcolor="Bg Primary" style="font-size: 0;vertical-align: top;background-color: #fc5c7e;border-radius: 4px 4px 0px 0px;padding-left: 16px;padding-right: 16px;padding-bottom: 24px;">
                    <!--[if mso]><table cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td width="200" align="left" valign="top" style="padding:0px 8px;"><![endif]-->
                      <div class="zmail_col zmail_col-2" style="display: inline-block;vertical-align: top;width: 100%;max-width: 200px;">
                        <div style="font-size: 24px; line-height: 24px; height: 24px;">&nbsp; </div>
                        <div class="zmail_px-xs zmail_sans zmail_text zmail_left zmail_xs-center" data-size="Text Default" data-min="12" data-max="20" style="font-family: Helvetica, Arial, sans-serif;margin-top: 0px;margin-bottom: 0px;font-size: 16px;line-height: 24px;text-align: left;padding-left: 8px;padding-right: 8px;">
                          <p style="margin-top: 0px;margin-bottom: 0px;"><a class="zmail_text-white" href="https://example.com/" data-color="White" style="text-decoration: none;outline: none;color: #ffffff;"><img src="https://d38fgd7fmrcuct.cloudfront.net/1_3qyeas6lhgsw57d2tygit.png" width="136" height="36" alt="ZAIKO Logo" style="max-width: 136px;-ms-interpolation-mode: bicubic;vertical-align: middle;border: 0;line-height: 100%;height: auto;outline: none;text-decoration: none;" class="image_target"></a></p>
                        </div>
                      </div>
                      <!--[if mso]></td><td width="200" align="right" valign="top" style="padding:0px 8px;"><![endif]-->
                        <div class="zmail_col zmail_col-2" style="display: inline-block;vertical-align: top;width: 100%;max-width: 200px;">
                          <div style="font-size: 22px; line-height: 22px; height: 22px;">&nbsp; </div>
                          <div class="zmail_px-xs" style="padding-left: 8px;padding-right: 8px;">
                            
                          </div>
                        </div>
                        <!--[if mso]></td></tr></table><![endif]-->
                      </td>
                    </tr>

                  </tbody></table>
                  <!--[if mso]></td></tr></table><![endif]-->
                </td>
              </tr>

            </tbody></table>
            <!--[if mso]></td></tr></table><![endif]-->
          </td>
        </tr>

      </tbody></table>

      <table data-module="spacer0" data-thumb="" width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" class="">

        <tbody><tr>
          <td class="zmail_bg-light zmail_px-xs" align="center" data-bgcolor="Bg Light" style="background-color: #f4f4f4;padding-left: 8px;padding-right: 8px;">
            <!--[if mso]><table width="455" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td><![endif]-->
              <table class="zmail_block-xs" width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" style="max-width: 455px;margin: 0 auto;">

                <tbody><tr>
                  <td class="zmail_bg-white" style="font-size: 24px;line-height: 24px;height: 24px;background-color: #ffffff;" data-bgcolor="Bg White">&nbsp; </td>
                </tr>

              </tbody></table>
              <!--[if mso]></td></tr></table><![endif]-->
            </td>
          </tr>

        </tbody></table><table data-module="content0" data-thumb="" width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" class="">

          <tbody><tr>
            <td class="zmail_bg-light zmail_px-xs" align="center" data-bgcolor="Bg Light" style="background-color: #f4f4f4;padding-left: 8px;padding-right: 8px;">
              <!--[if mso]><table width="455" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td><![endif]-->
                <table class="zmail_block-xs" width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" style="max-width: 455px;margin: 0 auto;">

                  <tbody><tr>
                    <td class="zmail_bg-white zmail_px-md zmail_py zmail_sans zmail_text zmail_text-secondary" data-bgcolor="Bg White" data-color="Secondary" data-size="Text Default" data-min="12" data-max="20" style="font-family: Helvetica, Arial, sans-serif;margin-top: 0px;margin-bottom: 0px;font-size: 16px;line-height: 24px;background-color: #ffffff;color: #424651;padding-left: 24px;padding-right: 24px;padding-top: 16px;padding-bottom: 16px;">
                      <p style="margin-top: 0px;margin-bottom: 0px;">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus expedita officiis amet sit nostrum ducimus accusamus deserunt, consequuntur a maiores, natus repellat pariatur. Quo quaerat asperiores laborum tempora veniam, temporibus.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id fugit dolores, doloremque nobis laborum aliquam blanditiis tenetur, harum illum enim, sint. Nemo maxime beatae sapiente consequuntur eveniet ipsa laboriosam impedit.</p>
                    </td>
                  </tr>

                </tbody></table>
                <!--[if mso]></td></tr></table><![endif]-->
              </td>
            </tr>

          </tbody></table><table data-module="button-primary0" data-thumb="" width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" class="">

            <tbody><tr>
              <td class="zmail_bg-light zmail_px-xs" align="center" data-bgcolor="Bg Light" style="background-color: #f4f4f4;padding-left: 8px;padding-right: 8px;">
                <!--[if mso]><table width="455" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td><![endif]-->
                  <table class="zmail_block-xs" width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" style="max-width: 455px;margin: 0 auto;">

                    <tbody><tr>
                      <td class="zmail_bg-white zmail_px-md zmail_py-xs" align="center" data-bgcolor="Bg White" style="background-color: #ffffff;padding-left: 24px;padding-right: 24px;padding-top: 8px;padding-bottom: 8px;">
                        <table align="center" cellspacing="0" cellpadding="0" border="0" role="presentation">

                          <tbody><tr>
                            <td width="300" class="zmail_btn zmail_bg-primary zmail_br zmail_heading zmail_text" align="center" data-bgcolor="Bg Primary" data-size="Text Default" data-min="12" data-max="20" style="font-family: Helvetica, Arial, sans-serif;font-weight: bold;margin-top: 0px;margin-bottom: 0px;font-size: 16px;line-height: 24px;mso-padding-alt: 12px 24px;background-color: #fc5c7e;border-radius: 4px;">
                              <a class="zmail_text-white" href="https://example.com/" data-color="White" style="text-decoration: none;outline: none;color: #ffffff;display: block;padding: 12px 24px;mso-text-raise: 3px;">View Message Thread</a>
                            </td>
                          </tr>

                        </tbody></table>
                      </td>
                    </tr>

                  </tbody></table>
                  <!--[if mso]></td></tr></table><![endif]-->
                </td>
              </tr>

            </tbody></table><table data-module="spacer-lg0" data-thumb="" width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" class="">

              <tbody><tr>
                <td class="zmail_bg-light zmail_px-xs" align="center" data-bgcolor="Bg Light" style="background-color: #f4f4f4;padding-left: 8px;padding-right: 8px;">
                  <!--[if mso]><table width="455" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td><![endif]-->
                    <table class="zmail_block-xs" width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" style="max-width: 455px;margin: 0 auto;">

                      <tbody><tr>
                        <td class="zmail_bg-white" style="font-size: 48px;line-height: 48px;height: 48px;background-color: #ffffff;" data-bgcolor="Bg White">&nbsp; </td>
                      </tr>

                    </tbody></table>
                    <!--[if mso]></td></tr></table><![endif]-->
                  </td>
                </tr>

              </tbody></table><table data-module="footer-light-3cols0" data-thumb="" width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" class="">

                <tbody><tr>
                  <td class="zmail_bg-light zmail_px-xs zmail_pb-lg zmail_xs-pb-xs" align="center" data-bgcolor="Bg Light" style="background-color: #f4f4f4;padding-left: 8px;padding-right: 8px;padding-bottom: 32px;">
                    <!--[if mso]><table width="455" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td><![endif]-->
                      <table class="zmail_block-xs" width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" style="max-width: 455px;margin: 0 auto;">

                        <tbody><tr>
                          <td class="zmail_bg-white zmail_br-b zmail_sans" style="font-size: 8px;line-height: 8px;height: 8px;font-family: Helvetica, Arial, sans-serif;margin-top: 0px;margin-bottom: 0px;background-color: #ffffff;border-radius: 0px 0px 4px 4px;" data-bgcolor="Bg White">&nbsp; </td>
                        </tr>
                        <tr>
                          <td class="zmail_re zmail_px zmail_pb-lg" align="center" style="font-size: 0;vertical-align: top;padding-left: 16px;padding-right: 16px;padding-bottom: 32px;">
                            <!--[if mso]><table cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td width="100" align="center" valign="top" style="padding:0px 8px;"><![endif]-->
                              <div class="zmail_col zmail_col-1 zmail_col-full" style="display: inline-block;vertical-align: top;width: 100%;max-width: 100px;">
                                <div style="font-size: 32px; line-height: 32px; height: 32px;">&nbsp; </div>
                                <div class="zmail_px-xs zmail_sans zmail_text-xs zmail_center" data-size="Text XS" data-min="10" data-max="18" style="font-family: Helvetica, Arial, sans-serif;margin-top: 0px;margin-bottom: 0px;font-size: 14px;line-height: 21px;text-align: center;padding-left: 8px;padding-right: 8px;">
                                  <p style="margin-top: 0px;margin-bottom: 0px;"><a class="zmail_text-light" href="https://example.com/" data-color="Light" style="text-decoration: none;outline: none;color: #82899a;"><strong data-color="Light" style="color: #82899a;">Support</strong></a></p>
                                </div>
                              </div>
                              <!--[if mso]></td><td width="100" align="center" valign="top" style="padding:0px 8px;"><![endif]-->
                                <div class="zmail_col zmail_col-1 zmail_col-full" style="display: inline-block;vertical-align: top;width: 100%;max-width: 100px;">
                                  <div style="font-size: 32px; line-height: 32px; height: 32px;">&nbsp; </div>
                                  <div class="zmail_px-xs zmail_sans zmail_text-xs zmail_center" data-size="Text XS" data-min="10" data-max="18" style="font-family: Helvetica, Arial, sans-serif;margin-top: 0px;margin-bottom: 0px;font-size: 14px;line-height: 21px;text-align: center;padding-left: 8px;padding-right: 8px;">
                                    <p style="margin-top: 0px;margin-bottom: 0px;"><a class="zmail_text-light" href="https://example.com/" data-color="Light" style="text-decoration: none;outline: none;color: #82899a;"><strong data-color="Light" style="color: #82899a;">Preferences</strong></a></p>
                                  </div>
                                </div>
                                <!--[if mso]></td></tr></table><![endif]-->
                              </td>
                            </tr>
                            <tr>
                              <td class="zmail_px-md zmail_pb-lg zmail_sans zmail_text-xs zmail_text-light" align="center" data-color="Light" data-size="Text XS" data-min="10" data-max="18" style="font-family: Helvetica, Arial, sans-serif;margin-top: 0px;margin-bottom: 0px;font-size: 14px;line-height: 21px;color: #82899a;padding-left: 24px;padding-right: 24px;padding-bottom: 32px;">

                                <p class="zmail_mb-xs" style="margin-top: 0px;margin-bottom: 8px;">© ZAIKO PTE Ltd. / ZAIKO株式会社 <br>
                                  2603 Woodridge Lane, Memphis, TN 38104, USA
                                </p>
                                <p style="margin-top: 0px;margin-bottom: 0px;">
                                  <a class="zmail_text-xxs zmail_text-light zmail_underline" href="https://example.com/" data-color="Light" data-size="Text XXS" data-min="8" data-max="16" style="text-decoration: underline;outline: none;font-size: 12px;line-height: 19px;color: #82899a;">Unsubscribe</a>
                                </p>
                              </td>
                            </tr>

                          </tbody>
                        </table>
                        <!--[if mso]></td></tr></table><![endif]-->
                        <div class="zmail_hide-xs" style="font-size: 64px; line-height: 64px; height: 64px;">&nbsp; </div>
                      </td>
                    </tr>

                  </tbody>
                </table>
                <div id="edit_link" class="hidden" style="display: none; left: 226px; top: 156px;">




                </div>
              </body>
              </html>