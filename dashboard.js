 var e = document.getElementById("chart_daily_sales");
 if(e) {
    var a= {
        labels:["Label 1",
        "Label 2",
        "Label 3",
        "Label 4",
        "Label 5",
        "Label 6",
        "Label 7",
        "Label 8",
        "Label 9",
        "Label 10",
        "Label 11",
        "Label 12",
        "Label 13",
        "Label 14",
        "Label 15",
        "Label 16"],
        datasets:[ {
            backgroundColor: "#0abb87", data: [15, 20, 25, 30, 25, 20, 15, 20, 25, 30, 25, 20, 15, 10, 15, 20]
        }
        ,
        {
            backgroundColor: "#f3f3fb", data: [15, 20, 25, 30, 25, 20, 15, 20, 25, 30, 25, 20, 15, 10, 15, 20]
        }
        ]
    }
    ;
    new Chart(e, {
        type:"bar", data:a, options: {
            title: {
                display: !1
            }
            , tooltips: {
                intersect: !1, mode: "nearest", xPadding: 10, yPadding: 10, caretPadding: 10
            }
            , legend: {
                display: !1
            }
            , responsive:!0, maintainAspectRatio:!1, barRadius:4, scales: {
                xAxes:[ {
                    display: !1, gridLines: !1, stacked: !0
                }
                ], yAxes:[ {
                    display: !1, stacked: !0, gridLines: !1
                }
                ]
            }
            , layout: {
                padding: {
                    left: 0, right: 0, top: 0, bottom: 0
                }
            }
        }
    }
    )
}


if(document.getElementById("chart_profit_share")) {
    var e= {
        type:"doughnut",
        data: {
            datasets:[ {
                data: [35, 30, 35], backgroundColor: ["#0abb87", "#fd397a", "#17a2b8"]
            }
            ],
            labels:["Angular",
            "CSS",
            "HTML"]
        }
        ,
        options: {
            cutoutPercentage:75,
            responsive:!0,
            maintainAspectRatio:!1,
            legend: {
                display: !1, position: "top"
            }
            ,
            title: {
                display: !1, text: "Technology"
            }
            ,
            animation: {
                animateScale: !0, animateRotate: !0
            }
            ,
            tooltips: {
                enabled: !0, intersect: !1, mode: "nearest", bodySpacing: 5, yPadding: 10, xPadding: 10, caretPadding: 0, displayColors: !1, backgroundColor: "#17a2b8", titleFontColor: "#ffffff", cornerRadius: 4, footerSpacing: 0, titleSpacing: 0
            }
        }
    }
    ,
    a=document.getElementById("chart_profit_share").getContext("2d");
    new Chart(a, e)
}

if(document.getElementById("chart_sales_stats")) {
    var e= {
        type:"line",
        data: {
            labels:["January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December",
            "January",
            "February",
            "March",
            "April"],
            datasets:[ {
                label: "Sales Stats", borderColor: '#17a2b8', borderWidth: 2, backgroundColor: '#17a2b8', pointBackgroundColor: Chart.helpers.color("#ffffff").alpha(0).rgbString(), pointBorderColor: Chart.helpers.color("#ffffff").alpha(0).rgbString(), pointHoverBackgroundColor: "#fd397a", pointHoverBorderColor: Chart.helpers.color("#fd397a").alpha(.2).rgbString(), data: [10, 20, 16, 18, 12, 40, 35, 30, 33, 34, 45, 40, 60, 55, 70, 65, 75, 62]
            }
            ]
        }
        ,
        options: {
            title: {
                display: !1
            }
            ,
            tooltips: {
                intersect: !1, mode: "nearest", xPadding: 10, yPadding: 10, caretPadding: 10
            }
            ,
            legend: {
                display:!1,
                labels: {
                    usePointStyle: !1
                }
            }
            ,
            responsive:!0,
            maintainAspectRatio:!1,
            hover: {
                mode: "index"
            }
            ,
            scales: {
                xAxes:[ {
                    display:!1,
                    gridLines:!1,
                    scaleLabel: {
                        display: !0, labelString: "Month"
                    }
                }
                ],
                yAxes:[ {
                    display:!1,
                    gridLines:!1,
                    scaleLabel: {
                        display: !0, labelString: "Value"
                    }
                }
                ]
            }
            ,
            elements: {
                point: {
                    radius: 3, borderWidth: 0, hoverRadius: 8, hoverBorderWidth: 2
                }
            }
        }
    }
    ;
    new Chart(document.getElementById("chart_sales_stats"), e)
}