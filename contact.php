<?php include('header.php'); ?>

<section class="banner bg-zaiko bg-overlay">
	<div class="text-block banner-narrow text-center pb-4">
		<h3 class="px-5 text-center">
			Interested about ZAIKO? We would love to hear from you.
		</h3>
		<p class="lead">Please enter your information below and we will be in touch.</p>
	</div>
	<div class="container">
		<div class="row">
			<div class="card card-dark card-login p-5 col-md-9 mx-auto mb-5">
				<form class="needs-validation" action="confirm.php" novalidate>
					<div class="form-group">
						<label>What are you organizing?</label>
						<select class="form-control">
							<option value="-">Please select below</option>
							<option value="concert">Live Concerts</option>
							<option value="festival">Festivals (Music, Arts, Design)</option>
							<option value="conference">Conferences</option>
							<option value="fan">Fan Interaction</option>
							<option value="others">Others</option>
						</select>
					</div>  

					<div class="form-group">
						<label>How many people attending your event?</label>
						<select class="form-control">
							<option value="-">Please select below</option>
							<option value="1">1-100</option>
							<option value="2">101-1000</option>
							<option value="3">Over 1000</option>
							<option value="others">Not sure</option>
						</select>
					</div>  

					<div class="form-group">
						<label>Estimated Ticket Price?</label>
						<select class="form-control">
							<option value="-">Please select below</option>
							<option value="1">Free Event</option>
							<option value="2">Under 2,000 JPY</option>
							<option value="2">2,000 - 5,000 JPY</option>
							<option value="3">Over 5,000 JPY</option>
							<option value="others">Not sure</option>
						</select>
					</div>  

					<hr class="my-4">

					<label>Your Full Name*</label>
					<div class="form-row">
						<div class="form-group col-md-6">
							<input type="text" class="form-control" placeholder="First name" required>
							<div class="invalid-feedback">
								Please enter your first name
							</div>
						</div>
						<div class="form-group col-md-6">
							<input type="text" class="form-control" placeholder="Last name" required>
							<div class="invalid-feedback">
								Please enter your last name
							</div>
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-6">
							<label>Your Email*</label>
							<input type="email" class="form-control" placeholder="Email Address" required>
							<div class="invalid-feedback">
								Please enter correct email address
							</div>
						</div>  

						<div class="form-group col-md-6">
							<label>Your Contact Number</label>
							<input type="phone" class="form-control" placeholder="Telephone Number">
						</div>  
					</div>

					<div class="form-group">
						<label>Anything else you would like to tell us?</label>
						<textarea class="form-control" rows="5" data-field="extra_message"></textarea>
					</div>  


					<div class="text-center">
						<button class="btn btn-default btn-lg btn-block" type="submit">Submit</button>
					</div>

					<p class="text-danger pt-2">*Required Fields</p>

				</form>
			</div>
		</div>
	</div>
</section>


<?php include('footer.php'); ?>


<script>
	// Example starter JavaScript for disabling form submissions if there are invalid fields
	(function() {
		'use strict';
		window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
    	form.addEventListener('submit', function(event) {
    		if (form.checkValidity() === false) {
    			event.preventDefault();
    			event.stopPropagation();
    		}
    		form.classList.add('was-validated');
    	}, false);
    });
}, false);
	})();
</script>