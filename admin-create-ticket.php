<?php include('header-admin-profile.php'); ?>

		<?php include('admin-aside-event.php') ?>

		<main class="admin-main pb-5">
			<?php include('admin-breadcrumbs.php') ?>

			<section class="container-fluid px-5">

				<div class="progress-bar d-xs-none py-5">
					<ol class="clearfix">
						<li class="active">
							<div class="progress-status">
								<i class="fas fa-wrench"></i>
							</div>
							<h5 class="fa-step--text">1. Basic Information</h5>
						</li>
						<li class="">
							<div class="progress-status">
								<i class="fas fa-ticket-alt fa-step"></i>					
							</div>
							<h5 class="fa-step--text">2. Ticket Details</h5>
						</li>
					</ol>
				</div>

				<div class="row">
					<div class="col-md-12 mx-auto">
						<h3 class="font-weight-normal mb-2">Add Your Ticket</h3>
						<p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
						<form class="needs-validation" action="admin-ticket-settings.php" novalidate>

							<div class="card p-4">

								<?php include('guestadmin/add-ticket-1.php'); ?>


							</div>

							<div class="text-center mt-4">
								<button class="btn btn-default " type="submit">Next Step</button>
								<a href="addevent-1.php"><button class="btn btn-secondary ">Cancel</button></a>
							</div>
						</form>
					</div>
				</div>
			</section>

		</main>


<!-- modal -->
<div id="modal-addticket" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content queue p-5 text-center">
			<h4>Please select your ticket type</h4>
			<a href="admin-create-ticket.php" class="btn btn-primary btn-lg btn-block">Paid Ticket</a>
			<a href="admin-create-ticket.php" class="btn btn-default btn-lg btn-block">Free Ticket*</a>
			<div class="mt-2">*While these tickets are for free events, organizers will be charged 50¥ for each ticket “purchased”.</div>
		</div>


	</div>
</div>

<?php include('footer.php'); ?>


