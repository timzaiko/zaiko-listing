<?php include('subscribe-header.php') ?>

<div id="modal-auto" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg modal-dialog-centered">
		<div class="modal-content p-5">

			<div class="text-center">
				<h3>Registration for Labyrinth Presale</h3>
				<p>Verify your account with your mobile phone</p>
			</div>

			<div class="progress my-3">
				<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 50%"></div>
			</div>					

			<form action="subscribe-3.php">

				<label>Enter your mobile phone</label>
				<div class="input-group">
					<div class="input-group-prepend">
						<button class="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Japan (+81)</button>
						<div class="dropdown-menu">
							<a class="dropdown-item" href="#">U.S. (+1)</a>
							<a class="dropdown-item" href="#">Country (+00)</a>
							<a class="dropdown-item" href="#">Country (+01)</a>
							<a class="dropdown-item" href="#">Country (+02)</a>
						</div>
					</div>
					<input type="tel" class="form-control" placeholder="">

				</div>

				<div class="text-center mt-2">
					<button class="btn btn-primary" type="button" id="requestCode">Request SMS Code [Code Sent]</button>
				</div>

				<div class="text-center mt-5">
					<div class="form-group">
						<input type="text" class="form-control form-sms" placeholder="Please enter the SMS code" maxlength="6">

					</div>
				</div>

				<div class="text-center">
					<button class="btn btn-primary btn-lg btn-block disabled btn-submit" type="submit" disabled="true">Verify</button>
				</div>

				<div class="text-center pt-3">      
					<a href="subscribe-3.php">I will add this later</a>
				</div>
			</form>

			<div class="text-center mt-4">
				Powered By&nbsp;&nbsp;
				<img width="100" src="img/zaiko-logo-text.svg" alt="">
			</div>
		</div>
	</div>
</div>

<?php include('subscribe-footer.php') ?>

<script>
	$('#requestCode').on('click', function() {
		$(this).addClass('disabled');
		$('.btn-submit').removeClass('disabled').removeAttr('disabled');
	});
</script>