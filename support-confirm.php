<?php include('header.php'); ?>

<section class="banner bg-zaiko">
    <div class="text-block banner-narrow text-center">
        <div class="container">
            <h2>Help Center</h2>
        </div>
    </div>
</section>

<section class="container p-5rem">
    <div class="row">
        <div class="col-md-10 mx-auto">

            <div class="alert alert-warning">
                <i class="fas fa-exclamation-triangle"></i>&nbsp;&nbsp;To help serve you better, <a href="#">please log in to your ZAIKO account</a>
            </div>

            <div class="alert alert-info p-3">
                * Our support hours are <strong>Monday - Friday 10 am - 7 pm (JST)</strong>. Please allow for up to 2 business days for a response.<br>
                * We are sorry that we might be slower in response outside of normal support hours and on national holidays.<br>
                * Please make sure to allow your email client (phone settings etc) to accept emails from @zaiko.io and @support.zaiko.io.<br>
                <strong>* Please note that we might take longer time to reply or not be able to reply depending on the type of requests.</strong><br>
                * Please visit our FAQ page for answers to common ticketing questions. 
                <div class="text-center mt-3">
                    <a href="#" class="btn btn-secondary btn-lg">Browse Full FAQ</a>
                </div>
            </div>

            <div class="my-5 text-center">
                <div class="text-success">
                    <i class="fas fa-check-circle display-4 mb-3"></i>
                    <h4 class="fa-step--text">Your support request has been received</h4>
                    <p class="text-dark-grey">Thank you for your submission. Please allow for up to 2 business days for a response.</p>
                </div>
                <a href="#" class="btn btn-default btn-xl">Return to Home Page</a>
            </div>
        </div>
    </div>
</section>

<?php include('footer.php'); ?>

<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
            }
            form.classList.add('was-validated');
        }, false);
    });
}, false);
    })();
</script>