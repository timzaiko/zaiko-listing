<?php include('header-beatink.php'); ?>

<div class="microsite">
	<?php include('admin-breadcrumbs.php') ?>
	
	<section class="container">
		<div class="row">


			<div class="col-md-8 col-sm-12 order-md-first py-3">
				<div class="swiper-container mb-3">
					<div class="swiper-wrapper">
						<div class="swiper-slide">
							<img class="w-100" src="https://d38fgd7fmrcuct.cloudfront.net/1_3t0wkgd2krfe4wuskvot3.jpg" alt="">
						</div>
						<!-- Add Pagination -->
						<div class="swiper-pagination"></div>
					</div>
				</div>
				<h2>【奨学金】日本学生支援機構奨学金 2020年3月満期対象者(大学院生)満期説明会について</h2>
				<h4 class="font-weight-light">{{ Event Subtitle }}</h4>

				<div class="event-desc">
					<div><p>現在、日本学生支援機構奨学金を貸与されている方で、2020年3月貸与予定の大学院生は、下記日程を確認し、自身が該当する日時の説明会に必ず参加してください。返還の手続きや卒業後の返還制度について説明を行います。</p>
						<p>※今回対象となる大学院生には自宅宛に「満期説明会および必要手続きのご案内」を送付しています。詳細については送られてきた封筒をご確認ください。<br>
						※過去に学生支援機構の奨学金を途中辞退した学生で、2020年3月卒業見込みの人も説明会に参加できます。参加希望の場合は、事前に学生課奨学金係へ連絡してください。</p>
						<p>日程：10月9日（水）<br>
							時間：12:30-<br>
							場所:9-102教室<br>
						持参物：1.「リレー口座加入申込書控」のコピー 2.学生証</p>
						<p>＜注意＞<br>
							・銀行窓口で「リレー口座申し込み手続き」を行い「口座加入申込書控」をコピーして説明会当日に提出してください。<br>
							・この説明会は必ず学生本人が出席してください。保護者の出席は認められません。<br>
							・やむを得ない理由で欠席の場合は、事前に必ず学生課、奨学金係へ連絡してください。無断欠席の場合は、11月からの奨学金が停止となります。<br>
						・奨学金の途中辞退を希望する場合は、説明会当日までに印鑑を持参の上、学生課奨学金係で辞退手続きを行ってください。</p>
					</div>
				</div>
			</div>


			<div class="col-md-4 col-sm-12 pt-3">
				<div class="card sticky-top mb-0">
									<a class="twitter-timeline" data-height="500" href="https://twitter.com/beatink_jp?ref_src=twsrc%5Etfw">Tweets by beatink_jp</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


				</div>
			</div>


		</div>
	</section> 

</div>

<div id="modal-login" class="modal fade show" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<?php include('modal-login.php') ?>
	</div>
</div>

<?php include('footer.php'); ?>

<script>
	var swiper = new Swiper('.swiper-container', {
		loop: true,
		speed: 700,
		dynamicBullets: true,
		effect: 'fade',
		autoplay: {
			delay: 4000,
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
	});
</script>
