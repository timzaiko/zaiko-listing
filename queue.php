<!DOCTYPE html>
<html>
<head>
	<title></title>

	<!-- Bootstrap is used for modal + other UI -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

</head>
<body>

	<style>
		body {
			overflow: hidden;
		}

		h3 {
			margin-bottom: 1rem;
		}

		.bg-image {
			width: 100%;
			height: 100vh;
		}

		.queue-number {
			color: #eb5baf;
			font-size: 6rem;
			font-weight: bold;
			line-height: 1;
			margin-bottom: 1rem;
		}
	</style>

	<div class="bg-image" style="background:url('/img/screen-timeout.jpg') no-repeat center center / cover"> <!-- Use static image as modal background to prevent inspect element hack -->

		<div id="modal-queue" class="modal fade" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-lg modal-dialog-centered">
				<div class="modal-content queue p-5 text-center">
					<h3>You are now in queue for<br>EDC Japan 2020</h3>
					<p>Due to overwhelming number of purchasers, you have been assigned in queue to purchase tickets for EDC Japan 2020. When it is your turn, you will have access to the ticket purchase page.</p>

					<div class="queue-number">
							<div class="queue-tbd">
								<div></div>
							</div>
							<style type="text/css">
								@keyframes queue-tbd {
									0% {
										-webkit-transform: translate(-50%, -50%) rotate(0deg);
										transform: translate(-50%, -50%) rotate(0deg);
									}
									100% {
										-webkit-transform: translate(-50%, -50%) rotate(360deg);
										transform: translate(-50%, -50%) rotate(360deg);
									}
								}
								@-webkit-keyframes queue-tbd {
									0% {
										-webkit-transform: translate(-50%, -50%) rotate(0deg);
										transform: translate(-50%, -50%) rotate(0deg);
									}
									100% {
										-webkit-transform: translate(-50%, -50%) rotate(360deg);
										transform: translate(-50%, -50%) rotate(360deg);
									}
								}
								.queue-tbd {
									position: relative;
									margin: 0 auto;
								}
								.queue-tbd div,
								.queue-tbd div:after {
									position: absolute;
									width: 120px;
									height: 120px;
									border: 20px solid #eb5baf;
									border-top-color: transparent;
									border-radius: 50%;
								}
								.queue-tbd div {
									-webkit-animation: queue-tbd 1s linear infinite;
									animation: queue-tbd 1s linear infinite;
									top: 100px;
									left: 100px;
								}
								.queue-tbd div:after {
									-webkit-transform: rotate(90deg);
									transform: rotate(90deg);
								}
								.queue-tbd {
									width: 200px !important;
									height: 200px !important;
									-webkit-transform: translate(-100px, -100px) scale(1) translate(100px, 100px);
									transform: translate(-100px, -100px) scale(1) translate(100px, 100px);
								}
							</style>
						<h5>Number of people ahead of you</h5>
					</div>

					<div class="alert alert-warning mb-3" role="alert">
						Estimated time of waiting: <strong>15 minutes</strong><br>
						Even if you leave this page, you will not lose your spot in the queue. Refreshing the page too many times might result in having yourself removed in the queue without notice.
					</div>

                <!-- <label style="text-align:left">Notify me by email when it is my turn</label>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Enter your email" aria-describedby>
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button" id="button-addon2">Submit</button>
                    </div>
                </div> -->

                <span><a href="#" class="btn btn-primary mt-4">Return to Previous Page</a></span>
                <a href="#" class="mt-3">Remove myself from the queue</a>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

<script type="text/javascript">
	$(window).on('load',function(){
		$('#modal-queue').modal({
			show: true,
			backdrop: 'static',
			keyboard: false
		});
	});
</script>

</body>
</html>

