<?php include('header.php'); ?>

<style>
	header,footer {
		display: none;
	}
</style>

<section class="bg-overlay opacity-40 bg-zaiko full-h d-flex justify-content-center align-items-center flex-column">

	<div class="container-fluid">
		<div class="mx-auto card card-login">
			<div class="d-flex justify-content-between align-items-center card-header">
				<a href="/">
					<img class="img-header d-block mx-auto" src="https://d38fgd7fmrcuct.cloudfront.net/1_3srrgnchq4ywmry64ua57.png" alt="">
				</a>
				<p class="m-0">Reset your password</p>
			</div>

			<div class="card-body p-4">
				<p>Please check your email for a reset password link.</p>		
				<a href="forgot-1.php" class="text-muted">{{ Reset Page UI }}</a>		
			</div>
			<div class="card-footer d-flex justify-content-between align-items-center">
				<p class="m-0">Did not receive email?</p>
				<a href="/forgot.php" class="btn btn-outline-dark">Start Over</a>
			</div>

		</div>
	</div>

</section>

<?php include('footer.php'); ?>