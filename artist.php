<?php include('header.php'); ?>

<div class="artist">
    <section class="banner artist-banner bg-overlay opacity-40" style="background: url('https://d38fgd7fmrcuct.cloudfront.net/1_3tbxi519bcfacifx1hd4e.jpg') no-repeat center center / cover">
        <div class="container">
            <div class="row text-block banner-narrow wow fadeIn">
                <div class="col-md-10 col-sm-12 mx-auto p-5 text-center">
                    <h2>ZAIKO For&nbsp;<span class="box">Artists</span></h2>
                    <p class="lead">Leverage the power of your brand. Get the most out of your fame.</p>
                    <a href="/my"
                    class="btn btn-xl btn-default my-4">Sign Up Now</a>
                </div>
            </div>
        </div>
    </section>

    <section class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-6 p-0 artist-slide position-relative">
                <img src="https://lh5.googleusercontent.com/u9lAnLvarim-rYcnQJO5xZrOVazvLP7cU1Q15mIU4Nda7ZZFkJQYuwqjNO_tX9viNIlGP4__BJvzoUNwybBBsFo7NMhr2OoWnbFUQTB8BCpqGgy3208mau-Bc80j5tDFAov-0Dcyip-XfA" alt="">
                <a href="#" class="text-block">
                    <span class="text-white">
                        <h5 class="mb-2">Akane</h5>
                    </span>
                </a>
            </div>
            <div class="col-md-3 col-6 p-0 artist-slide position-relative">
                <img src="https://lh3.googleusercontent.com/RXS25m-Y1PNBdKtWH_VPMFDou83qUvnEx01jfyt8fgzJi2axLT7_uC1cfweQENr6UBchc6uTLGz092yGzV9YTqrf1QvCAWjNrFfWLriPgA5-K4QcsbZ5uEN-ZlmNoTnj36OjoeMSyCuqPQ" alt="">
                <a href="#" class="text-block">
                    <span class="text-white">
                        <h5 class="mb-2">AMARYLLIS BOMB</h5>
                    </span>
                </a>
            </div>
            <div class="col-md-3 col-6 p-0 artist-slide position-relative">
                <img src="https://lh3.googleusercontent.com/2wHvDjKwyT6yYc_j4zGPmDDmB6zt0mx2bEhQBcb4KPuxR-TFDzPD15Msxeo-Sa3BaanjK4y0hWl3UDBxrlkbbDrJVgtlwkHVEKwR2xsDaVOHr8F8WJNyaB3lbKrE8Yf75zHcZUeBHijypA" alt="">
                <a href="#" class="text-block">
                    <span class="text-white">
                        <h5 class="mb-2">KOHH</h5>
                        <p class="mb-0">Untitled Tickets</p>
                    </span>
                </a>
            </div>
            <div class="col-md-3 col-6 p-0 artist-slide position-relative">
                <img src="https://lh6.googleusercontent.com/3WqmKkx9z9o4eXUFVlqJdX9Ig-ppcaQ2got-YbTLtPgX57o_EGo7zXYGlXitWMlG6ffsunPDF6Noti7epu66DLPucazeCu-fDGSqeLvgDHyUZH6v_Gzw6hDc2aQ6qYDe0oRspuQYM-4JwQ" alt="">
                <a href="#" class="text-block">
                    <span class="text-white">
                        <h5 class="mb-2">MITSUME</h5>
                    </span>
                </a>
            </div>
            <div class="col-md-3 col-6 p-0 artist-slide position-relative">
                <img src="https://lh3.googleusercontent.com/iBFVij7eODueebZ_VK15NTnZlcafML0moeGN6AIukdXNqrgkLUKMueKcrjpHN-Sg2mPJhbK4vuidDvGYeYyU69SrKnPsYnZbWZ29J44Hap7fT67FsrZBpnCIJ7HjxUrb846DSGrwznJv4g" alt="">
                <a href="#" class="text-block">
                    <span class="text-white">
                        <h5 class="mb-2">Shurkn Pap</h5>
                        <p class="mb-0">Island State Tickets</p>
                    </span>
                </a>
            </div>
            <div class="col-md-3 col-6 p-0 artist-slide position-relative">
                <img src="https://lh4.googleusercontent.com/HXd9UJZqMRQX_xmK1oY9QeTOU039JTf8RwGP2CkkrsRUUUbqH0rZKnwoFFfLYacvO-46DopoXZEYEX7Tp46esbPkMYfh1jkpcGfCqoeudpv6Di0LpMjHw0xicRgjdg-JJjjgx8O543t7ow" alt="">
                <a href="#" class="text-block">
                    <span class="text-white">
                        <h5 class="mb-2">KOWICHI</h5>
                        <p class="mb-0">FLY BOY RECORDS</p>
                    </span>
                </a>
            </div>
            <div class="col-md-3 col-6 p-0 artist-slide position-relative">
                <img src="https://d38fgd7fmrcuct.cloudfront.net/1_3tbyt5sa6ogs9ctpntw1v.jpg" alt="">
                <a href="#" class="text-block">
                    <span class="text-white">
                        <h5 class="mb-2">LITE, The fin.</h5>
                        <p class="mb-0">Friendship</p>
                    </span>
                </a>
            </div>
            <div class="col-md-3 col-6 p-0 artist-slide position-relative">
                <img src="https://lh5.googleusercontent.com/RPxtvry54oDd-dJTc_CugvVLXYkUa6q5UFR2tGZJAFhv200fC_rSVKEOdqnCIXp3y5b-7dpgJ4j8n_fDiBnTBuDySuu8ZJdrhiqZszULM8Ma_RIakEzqJhO_omiWSZdr1-7HRnTXPgE72w" alt="">
                <a href="#" class="text-block">
                    <span class="text-white">
                        <h5 class="mb-2">おとぼけビ～バ～</h5>
                        <p class="mb-0">JET TICKET</p>
                    </span>
                </a>
            </div>
        </div>       
    </section>

    <section class="container-fluid text-white bg-overlay" style="background:url(https://d38fgd7fmrcuct.cloudfront.net/1_3t8f3k6p0r71piqf90anw.jpg)">
        <div class="row align-items-center">
            <div class="col-md-1"></div>
            <div class="col-md-5 p-5 pl-md-0">
                <h2>Brand Building</h2>
                <p class="lead">オリジナルのチケットプラットフォームでアーティストブランディングを強める</p>

                <p class="lead">従来のセルフサービス型の電子チケットサービスとは異なり、あなただけのオリジナルのマイクロサイトで、カラーイメージや写真を設定し、思うがままにチケット販売をすることが可能に。<br><br>

                自分だけのプレイガイド=チケット販売プラットフォームで、チケット販売をアーティスト活動のブランディングにつなげる。
            </p>

        </div>
        <div class="col-md-6 pt-5 wow fadeIn">
            <img class="w-100" src="https://d38fgd7fmrcuct.cloudfront.net/1_3tbxw3xy0vgxqrpyg7n9f.png" alt="">
        </div>
    </div>
</section>


<section class="text-white pt-md-5 bg-black">
    <div class="container p-5">
        <div class="row">
            <div class="col-md-10 col-sm-10 mx-auto text-center">
               <h2 class="box">Digital Possibilities</h2>
               <p class="lead">自由自在のデータ管理、豊富な機能で
           アーティストにもファンにも便利なチケット販売を実現</p>

               <p class="lead">ZAIKOを利用すれば、各プレイガイドに配券を行う従来のチケッティングでうやむやにされていた券売状況や購入者データにいつでもアクセス可能。<br><br>

                抽選チケットや座席指定などはもちろん、ファンクラブ認証必須の限定チケットやグッズ付きチケットの販売など、どんなアーティスト・イベント内容にも対応できるユニークな機能も充実。
                
            </p>
        </div>
    </div>
</div>

<div class="swiper-container wow fadeIn" style="margin-bottom: -200px">
    <div class="swiper-wrapper">
        <div class="swiper-slide px-5"><img class="w-100" src="https://d38fgd7fmrcuct.cloudfront.net/1_3tc0bua5g07a2bcgn3p3u.png" alt=""></div>
        <div class="swiper-slide px-5"><img class="w-100" src="https://d38fgd7fmrcuct.cloudfront.net/1_3tc0gclfkwwl6cpk76n97.png" alt=""></div>
        <div class="swiper-slide px-5"><img class="w-100" src="https://d38fgd7fmrcuct.cloudfront.net/1_3tc0h9oyatm1ip8w0bck0.png" alt=""></div>
        <div class="swiper-slide px-5"><img class="w-100" src="https://d38fgd7fmrcuct.cloudfront.net/1_3tc0hlubfhylt86vlvt74.png" alt=""></div>
        <div class="swiper-slide px-5"><img class="w-100" src="https://d38fgd7fmrcuct.cloudfront.net/1_3tc0gckb4tklfodnxh6ht.png" alt=""></div>

        <div class="swiper-slide px-5"><img class="w-100" src="https://d38fgd7fmrcuct.cloudfront.net/1_3tc0i6sn0f9e326mh095b.png" alt=""></div>
    </div>
    <!-- Add Pagination -->
    <div class="swiper-pagination"></div>
</div>

</section>

<section class="container-fluid text-white bg-overlay" style="background:url(https://d38fgd7fmrcuct.cloudfront.net/1_3t8f3k6p0r71piqf90anw.jpg); z-index:9">
    <div class="row align-items-center">
        <div class="col-md-1"></div>
        <div class="col-md-5 p-5 pl-md-0">
            <h2>Get a<br>deeper,<br>wider view</h2>
            <p class="lead">ファンのことをより深く知りアーティスト活動を世界へ</p>

            <p class="lead">ファンのデータは、継続的なチケット販売で増えていく。居住地や年齢だけでなく、好きなアーティストの情報など、ZAIKOを通じてあなたのファンのことをもっと深く理解できる。<br><br>


                そしてZAIKOはインバウンドはもちろん、海外公演チケットの販売も簡単。多くのプレイガイドがブロックしている海外発行クレジットカード所持者、コンビニ発券の複雑なシステムが使えないオーディエンスにもアプローチしよう。
            </p>

        </div>
        <div class="col-md-6 text-right pr-md-0 py-5"><img class="w-100" src="https://d38fgd7fmrcuct.cloudfront.net/1_3tc0lchfemnahkfmi9wt4.jpg" alt=""></div>
        
    </div>
</section>

<section style="background:url(https://d38fgd7fmrcuct.cloudfront.net/1_3tc0kjfelrpxgcxel8i6y.jpg)">
    <div class="container text-white">
        <div class="row py-5">
            <div class="col-md-10 col-sm-12 mx-auto text-center">
                <h2 class="p-5">多くのアーティストが、世界で活躍できる可能性を秘めています。
                今すぐ <span class="box">ZAIKO for Artists</span> を活用しませんか？</h2>
                <a href="/my"
                class="btn btn-xl btn-default mb-5">Sign Up Now</a>
            </div>
        </div>
    </div>
</section>
</div>

<?php include('footer.php'); ?>

<script>
 var swiper = new Swiper('.swiper-container', {
        effect: 'coverflow',
        grabCursor: true,
        speed: 800,
        loop: true,
        centeredSlides: true,
        autoplay: {
            delay: 1500,
            disableOnInteraction: false,
        },
        coverflowEffect: {
            rotate: 30,
            stretch: 0,
            modifier: 1,
        },
        breakpoints: {
            320: {
                slidesPerView: 1
            },
            640: {
                slidesPerView: 3
            },
            1080: {
                slidesPerView: 4
            }
        }
    });
</script>