<?php include('header-admin-profile.php'); ?>

		<?php include('admin-aside-event.php') ?>

		<main class="admin-main pb-5">
			<?php include('admin-breadcrumbs.php') ?>

			<section class="container-fluid pt-4 px-5">
				<div class="row justify-content-between align-items-center my-3">
					<div>
						<h3 class="font-weight-normal mb-2">Edit Event</h3>
						<p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. </p>
					</div>
				</div>
			</section>

			<?php include('module-event-name.php') ?>

			<section class="container-fluid px-5">

				<div class="row">
					<div class="w-100">

						<form class="needs-validation" action="admin-event-settings.php" novalidate>

							<div class="card p-4">

								<?php include('guestadmin/add-1.php'); ?>



								
							</div>

							<div class="text-center mt-4">
								<button class="btn btn-default " type="submit">Update</button>
								<a href="addevent-1.php"><button class="btn btn-secondary ">Cancel</button></a>
							</div>
						</form>
					</div>
				</div>
			</section>

		</main>


<!-- modal -->
<div id="modal-addticket" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content queue p-5 text-center">
			<h4>Please select your ticket type</h4>
			<a href="register-1.php" class="btn btn-primary btn-lg btn-block">Paid Ticket</a>
			<a href="register-1.php" class="btn btn-default btn-lg btn-block">Free Ticket*</a>
			<div class="mt-2">*While these tickets are for free events, organizers will be charged 50¥ for each ticket “purchased”.</div>
		</div>


	</div>
</div>

<?php include('footer.php'); ?>


