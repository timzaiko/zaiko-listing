<?php include('header.php'); ?>

<style>
	header, footer {
		display: none;
	}
</style>

<section class="bg-overlay bg-overlay-login bg-overlay-zaiko full-h d-flex justify-content-center align-items-center flex-column modal">

	<a href="/"><img class="img-zaiko mb-3" src="/img/ZAIKO-logo-text-white.svg" alt=""></a>


	<div class="container">
		<div class="row">
			<div class="col-md-6 mx-auto">

				<?php include('register/block-3.php') ?>

			</div>
		</div>
	</div>

	<a href="login.php" class="btn btn-outline-light">Already using ZAIKO?</a>

</section>

<?php include('footer.php'); ?>