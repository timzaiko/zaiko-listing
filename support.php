<?php include('header.php'); ?>

<section class="banner bg-zaiko">
	<div class="text-block banner-narrow text-center">
		<div class="container">
			<h2>Help Center</h2>
		</div>
	</div>
</section>

<section class="container p-5">
	<div class="row">
		<div class="col-md-10 mx-auto">
			<div class="alert alert-warning">
				<i class="fas fa-exclamation-triangle"></i>&nbsp;&nbsp;To help serve you better, <a href="#">please log in to your ZAIKO account</a>
			</div>

			<div class="alert alert-info p-3">
				* Our support hours are <strong>Monday - Friday 10 am - 7 pm (JST)</strong>. Please allow for up to 2 business days for a response.<br>
				* We are sorry that we might be slower in response outside of normal support hours and on national holidays.<br>
				* Please make sure to allow your email client (phone settings etc) to accept emails from @zaiko.io and @support.zaiko.io.<br>
				<strong>* Please note that we might take longer time to reply or not be able to reply depending on the type of requests.</strong><br>
				* Please visit our FAQ page for answers to common ticketing questions. 
				<div class="text-center mt-3">
					<a href="#" class="btn btn-secondary btn-lg">Browse Full FAQ</a>
				</div>
			</div>

			<form class="needs-validation" action="support-confirm.php" novalidate>
				<div class="form-group">
					<label>Your Name*</label>
					<input type="text" class="form-control" placeholder="First name" required>
					<div class="invalid-feedback">
						Please enter your name
					</div>
				</div>

				<div class="form-group">
					<label>Your Email*</label>
					<input type="email" class="form-control" placeholder="Email Address" required>
					<div class="invalid-feedback">
						Please enter correct email address
					</div>
				</div>  

				<div class="form-group">
					<label>Your Contact Number</label>
					<input type="phone" class="form-control" placeholder="Telephone Number">
				</div> 


				<div class="form-group">
					<label>Ticket Affected</label>

					<div class="mb-2">
						<div class="form-check">
							<input class="form-check-input" type="checkbox" name="" id="" value="option1" aria-label="">
							<label class="form-check-label">
								<strong>1 x TimTam Band 2020</strong> - GA - 2019-12-31 
							</label>
						</div>
					</div>

					<div class="mb-2">
						<div class="form-check">
							<input class="form-check-input" type="checkbox" name="" id="" value="option1" aria-label="">
							<label class="form-check-label">
								<strong>2 x TimTam Band 2020</strong> - VIP - 2019-12-31
							</label>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label>Issues*</label>
					<textarea class="form-control" rows="10" data-field="extra_message" placeholder="Please be as detailed as possible." required></textarea>
					<div class="invalid-feedback">
						Please list your issues
					</div>
				</div>  


				<div class="text-center">
					<button class="btn btn-default btn-lg btn-block" type="submit">Submit</button>
				</div>

				<p class="text-danger pt-2">*Required Fields</p>

			</form>

		</div>
	</div>
</section>

<?php include('footer.php'); ?>

<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
    	'use strict';
    	window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
    	form.addEventListener('submit', function(event) {
    		if (form.checkValidity() === false) {
    			event.preventDefault();
    			event.stopPropagation();
    		}
    		form.classList.add('was-validated');
    	}, false);
    });
}, false);
    })();
</script>