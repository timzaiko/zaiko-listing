<div class="container-fluid border-bottom bg-light-grey px-5 d-xs-none">
	<div class="breadcrumb row justify-content-between">
		<div class="text-med-grey">
			<a href="admin-home.php">Home</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;›&nbsp;&nbsp;&nbsp;&nbsp;Breadcrumb 1&nbsp;&nbsp;&nbsp;&nbsp;›&nbsp;&nbsp;&nbsp;&nbsp;Breadcrumb 2&nbsp;&nbsp;&nbsp;&nbsp;›&nbsp;&nbsp;&nbsp;&nbsp;<span class="text-dark">Breadcrumb 3</span>
		</div>
		<div class="">
			<span class="typcn typcn-zoom-outline pr-1" style=""></span>
			<span class="text-muted">Search</span>
		</div>
	</div>
</div>