<?php include('header-iflyer.php'); ?>

<div class="zaiko-listing">
	<section class="container-fluid">
		<div class="row bg-overlay" style="background:url('/img/listing/event-edc.jpg') center / cover">
			<figure class="banner event-banner w-100 swiper-banner swiper-no-swiping m-0">
				<div class="bg-gradient bg-gradient-top"></div>

				<div class="swiper-wrapper">
					<img class="w-100 swiper-slide" src="/img/listing/event-edc.jpg" alt="">

				</div>
				<div class="swiper-pagination"></div>
			</figure>
		</div>
	</section>

	<section class="container mt-3">
		<div class="row">

			<div class="col-lg-8">
				<div class="card p-4">
					<div class="row">
						<div class="col-md-12">
							<h4>Use Existing Card</h4>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-md-6 ">
							<label>Card Number</label> 
							<p>**** **** **** 6274</p>
						</div> 
						<div class="form-group col-md-6 ">
							<label>Name on card</label> 
							<p>Timothy Lee</p>	
						</div> 
						<!-- <div class="form-group col-md-12 ">
							<label>Your E-mail</label> 
							<input id="event-name" type="email" placeholder="yourname@email.com" required="required" class="form-control">
						</div>  -->

					</div>

	
				</div>
				<div class="text-center">
					<a href="payment-2.php" class="btn btn-lg btn-pink text-center btn-inline-block">Complete Order</a>
					<a href="listing-display.php" class="btn btn-lg btn-secondary text-center btn-inline-block">Payment Options</a>
				</div>
			</div> 

			<div class="card-order col-md-4">
				<div class="card p-3 p-md-4">

					<h4 class="text-center text-md-left mb-3">Your Order</h4>

					<div class="" id="prices">

						<div class="ticket-price single d-flex justify-content-between align-items-center py-2">
							<span>						
								<p>早割チケット</p>
								<h5 class="mb-1 text-primary">￥2,500</h5>
							</span>

							<p class="text-muted">1 x</p>		
						</div>

						<div class="ticket-price single d-flex justify-content-between align-items-center py-2">
							<span>						
								<p>		前売りチケット</p>
								<h5 class="mb-1 text-primary"> ￥3,000</h5>
							</span>

							<p class="text-muted">2 x</p>		
						</div>

						<div class="ticket-price single d-flex justify-content-between align-items-center py-2">
							<span>						
								<p>Handling Fee</p>
								<p class="small" data-toggle="tooltip" data-placement="bottom" title="this is extra charge on your ticket price">what is this?</p>
							</span>

							<a href="#" class="text-muted">￥800</a>		
						</div>

						<div class="ticket-price d-flex justify-content-between align-items-center pt-3">
							<h4>Total</h4>
							<h4 class="text-primary">￥9,300</h4>
						</div>

						<div class="alert alert-secondary">Your credit card will be charged for the amount indicated.</div>

						

					</div>


				</div>
			</div>

		</div>



	</div>
</section> 

</div>

<div id="modal-login" class="modal fade show" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<?php include('modal-login.php') ?>
	</div>
</div>

<?php include('footer.php'); ?>

<script>

</script>
