<!DOCTYPE html>
<html lang="en">
<head>
	<title>ZAIKO</title>
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
	<link rel="icon" href="/img/favicon.ico">

	<!-- swiper css -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css">

	<link type="text/css" rel="stylesheet" href="/css/vone/app.css">
	<!-- <link type="text/css" rel="stylesheet" href="/vue/my-project/src/static/app.css"> -->
</head>

<body class>

	<header>
	<nav class="header--transparent navbar navbar-expand-lg navbar-light">
        <a href="/"><img class="img-header img-fluid" src="https://zaiko.io/img/ZAIKO-logo-text-white.svg"></a>

        <a class="navbar-toggler navbar-toggler-zaiko" data-toggle="collapse" data-target="#navbar-user" aria-controls="navbar-zaiko" aria-expanded="false" aria-label="Toggle navigation">
            <span></span>
            <span></span>
            <span></span>
        </a>

        <div class="admin-navbar d-flex flex-grow-1 flex-lg-row-reverse justify-content-between align-items-center">
            <ul class="collapse navbar-collapse navbar-collapse-zaiko menu menu-right flex-lg-row-reverse" id="navbar-user">
                <li class="dropdown-user d-flex flex-column align-items-center">
    <div class="d-flex align-items-center justify-lg-content-end w-100">
        <img class="img-profile mr-2" src=" https://d38fgd7fmrcuct.cloudfront.net/bw_300/bh_300/pf_1/1_3r2xttwhvbo3aq0p0bkqp" alt="">
        <span class="welcome d-block pr-2">User Name</span>
    </div>
    <ul class="sub-menu sub-menu--user right w-100 py-3 px-0">
        <li>
            <a class="" href="https://zaiko.io/legacy_support/view_my_ticket">View My Tickets</a>
        </li>
        <li>
            <a class="" href="https://zaiko.io/legacy_support/settings">Account Settings</a>
        </li>

                <li class="mx-lg-3">
            <a class="btn btn-default d-block text-white" href="https://zaiko.io/legacy_support/admin">
                Business
            </a>
        </li>
        
        <li>
            <a dusk="logout_link" href="https://zaiko.io/legacy_support/logout_link" class="">Sign Out</a>
        </li>
    </ul>

    </li><li class="dropdown-lang d-flex align-items-center">
    <div class="dropdown">
        <span class="dropdown-toggle" id="langButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="flag-icon flag-icon-en"></i>
            <span class="ml-2">English</span>
        </span>
        <ul class="dropdown-menu sub-menu sub-menu--user right w-100 py-3 px-0 " aria-labelledby="langButton">
                                                            <li>
                <a href="https://zaiko.io/?lang=ja"><i class="mr-2 flag-icon flag-icon-ja"></i>
                    <span>日本語</span>
                </a>
            </li>
                                                <li>
                <a href="https://zaiko.io/?lang=ko"><i class="mr-2 flag-icon flag-icon-ko"></i>
                    <span>한국어</span>
                </a>
            </li>
                                                <li>
                <a href="https://zaiko.io/?lang=zh-hans"><i class="mr-2 flag-icon flag-icon-zh-hans"></i>
                    <span>简体中文</span>
                </a>
            </li>
                                                <li>
                <a href="https://zaiko.io/?lang=zh-hant"><i class="mr-2 flag-icon flag-icon-zh-hant"></i>
                    <span>繁體中文</span>
                </a>
            </li>
                                </ul>
    </div>
</li>
<hr class="d-lg-none">            </ul>
        </div>
    </nav>
	</header>