<?php include('guestadmin-header.php'); ?>

<section class="container">
	<div class="row p-5rem">
		<div class="col-md-10 mx-auto">
			<div class="text-center">
				<h4 class="text-center">
					Please enter your bank or PayPal information below for payout purposes
				</h4>
			</div>

			<div class="justify-content-center my-3">
				<span class="nav justify-content-center">
					<a href="#addBankinfo" class="btn btn-brand btn-lg" role="tab" data-toggle="tab" aria-selected="true" aria-controls=""><i class="fas fa-university"></i>&nbsp;&nbsp;Bank</a>
					<a href="#addPaypal" class="btn btn-primary btn-lg" role="tab" data-toggle="tab" aria-selected="false" aria-controls=""><i class="fab fa-paypal"></i>&nbsp;&nbsp;PayPal</a>
				</span>
			</div>
			<form class="needs-validation" action="guestadmin-addbank.php" novalidate>
				<div class="tab-content">
					<div class="tab-pane fade show active" id="addBankinfo" role="tabpanel" aria-labelledby="">

						<label>Company Name</label>
						<div class="form-row">
							<div class="form-group col-md-12">
								<input type="text" class="form-control">
								<small id="companyHelp" class="form-text text-muted">Please fill out the full, LEGAL NAME of your organization, or leave blank if you are working as an individual.</small>
							</div>
						</div>

						<label>Contact Name*</label>
						<div class="form-row">
							<div class="form-group col-md-6">
								<input type="text" class="form-control" placeholder="First name" required>
								<div class="invalid-feedback">
									Please enter contact name
								</div>
							</div>
							<div class="form-group col-md-6">
								<input type="text" class="form-control" placeholder="Last name" required>
								<div class="invalid-feedback">
									Please enter your last name
								</div>
							</div>
						</div>

						<div class="form-row">
							<div class="form-group col-md-6">
								<label>Contact Email*</label>
								<input type="email" class="form-control" placeholder="Email Address" required>
								<div class="invalid-feedback">
									Please enter correct email address
								</div>
							</div>  

							<div class="form-group col-md-6">
								<label>Contact Phone Number</label>
								<input type="phone" class="form-control" placeholder="Telephone Number">
							</div>  
						</div>

						<h4>Bank Details</h4>

						<label>Bank Name*</label>
						<div class="form-row">
							<div class="form-group col-md-12">
								<input type="text" class="form-control" placeholder="Bank name" required>
								<div class="invalid-feedback">
									Please enter bank name
								</div>
							</div>
						</div>

						<label>Branch Name and Code*</label>
						<div class="form-row">
							<div class="form-group col-md-8">
								<input type="text" class="form-control" placeholder="Branch name" required>
								<div class="invalid-feedback">
									Please enter branch name
								</div>
							</div>
							<div class="form-group col-md-4">
								<input type="text" class="form-control" maxlength="3" placeholder="3-digit code" required>
								<div class="invalid-feedback">
									Please enter branch code
								</div>
							</div>
						</div>

						<div class="form-group">
							<label>Account Type*</label>
							<select class="form-control">
								<option value="-">Please select below</option>
								<option value="savings">Savings</option>
								<option value="checking">Checking</option>
							</select>
						</div>  

						<div class="form-group">
							<label for="">Account Number*</label>
							<input type="number" maxlength="7" class="form-control" placeholder="7-digit maximum">
						</div>

						<div class="form-group">
							<label for="">Account Name*</label>
							<input type="text" class="form-control" placeholder="in KANA. See the inside of the front cover of the bank book">
						</div>

						
					</div>
					<div class="tab-pane fade" id="addPaypal" role="tabpanel" aria-labelledby="">
						<label>Paypal Email</label>
						<div class="form-row">
							<div class="form-group col-md-12">
								<input type="email" class="form-control">
								<small id="companyHelp" class="form-text text-muted">Please enter the registerd email address of the PayPal account to which the payout is being transferred.</small>
							</div>
						</div>
					</div>

				</div>
				<div class="text-center">
					<button class="btn btn-brand btn-lg btn-block" type="submit">Submit</button>
				</div>

				<p class="text-danger pt-2">*Required Fields</p>

			</form>
		</div>
	</div>
</section>

<?php include('guestadmin-footer.php'); ?>


<script>
	// Example starter JavaScript for disabling form submissions if there are invalid fields
	(function() {
		'use strict';
		window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
    	form.addEventListener('submit', function(event) {
    		if (form.checkValidity() === false) {
    			event.preventDefault();
    			event.stopPropagation();
    		}
    		form.classList.add('was-validated');
    	}, false);
    });
}, false);
	})();
</script>